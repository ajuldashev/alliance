#include "ABase.hpp"

#ifdef DEBUG_ABASE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_abase(AString com) {
	#ifdef DEBUG_AENCRYPTION
	if (com == "aencryption") {
		if (debug_aencryption()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AIO
	if (com == "aio") {
		if (debug_aio()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ALIBS
	if (com == "alibs") {
		if (debug_alibs()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMATH
	if (com == "amath") {
		if (debug_amath()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRUCT
	if (com == "astruct") {
		if (debug_astruct()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATYPES
	if (com == "atypes") {
		if (debug_atypes()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_abase() {
	AString text;
	#ifdef DEBUG_AENCRYPTION
	text += "d) aencryption\n";
	#endif
	#ifdef DEBUG_AIO
	text += "d) aio\n";
	#endif
	#ifdef DEBUG_ALIBS
	text += "d) alibs\n";
	#endif
	#ifdef DEBUG_AMATH
	text += "d) amath\n";
	#endif
	#ifdef DEBUG_ASTRUCT
	text += "d) astruct\n";
	#endif
	#ifdef DEBUG_ATYPES
	text += "d) atypes\n";
	#endif
	return text;
}

#endif
