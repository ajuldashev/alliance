#include "ABase.exception.hpp"

ABaseException::ABaseException() : AllianceException() {
	exception = "Alliance-Exception-ABase";
}

ABaseException::ABaseException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ABase");
}

ABaseException::ABaseException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ABase");
}

ABaseException::ABaseException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ABase");
}

ABaseException::ABaseException(const ABaseException & obj) : AllianceException(obj) {
	
}

ABaseException::~ABaseException() {

}

