#ifndef HPP_ABASEEXCEPTION
#define HPP_ABASEEXCEPTION
#include <alliance.exception.hpp>

class ABaseException : public AllianceException {
public:
	ABaseException();
	ABaseException(std::string text);
	ABaseException(AllianceException & e);
	ABaseException(AllianceException & e, std::string text);
	ABaseException(const ABaseException & obj);
	~ABaseException();
};

#endif

