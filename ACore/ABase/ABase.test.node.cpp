#include "ABase.hpp"

#ifdef TEST_ABASE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_abase(bool is_soft) {
	#ifdef TEST_AENCRYPTION
	if (!test_aencryption(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AENCRYPTION );
		return is_soft;
	}
	#endif
	#ifdef TEST_AIO
	if (!test_aio(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AIO );
		return is_soft;
	}
	#endif
	#ifdef TEST_ALIBS
	if (!test_alibs(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ALIBS );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMATH
	if (!test_amath(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMATH );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRUCT
	if (!test_astruct(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRUCT );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATYPES
	if (!test_atypes(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATYPES );
		return is_soft;
	}
	#endif
	return true;
}
#endif
