#include "ACrc16.hpp"

#ifdef DEBUG_ACRC16
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_acrc16() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ACRC16 );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_acrc16());
            AIO::writeln("1) help");
            AIO::writeln("2) get_hash");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "get_hash") {
            A8Bits data(AIO::get_word());
            AIO::writeln(ACrc16::get_hash(data).to_text());
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_acrc16(com)) {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ACRC16 );
    AIO::write_div_line();
    return false;
}
#endif

