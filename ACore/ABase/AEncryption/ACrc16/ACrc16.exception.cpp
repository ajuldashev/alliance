#include "ACrc16.exception.hpp"

ACrc16Exception::ACrc16Exception() : AllianceException() {
	exception = "Alliance-Exception-ACrc16";
}

ACrc16Exception::ACrc16Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACrc16");
}

ACrc16Exception::ACrc16Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACrc16");
}

ACrc16Exception::ACrc16Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACrc16");
}

ACrc16Exception::ACrc16Exception(const ACrc16Exception & obj) : AllianceException(obj) {
	
}

ACrc16Exception::~ACrc16Exception() {

}

