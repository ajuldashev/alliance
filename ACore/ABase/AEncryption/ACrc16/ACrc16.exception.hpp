#ifndef HPP_ACRC16EXCEPTION
#define HPP_ACRC16EXCEPTION
#include <alliance.exception.hpp>

class ACrc16Exception : public AllianceException {
public:
	ACrc16Exception();
	ACrc16Exception(std::string text);
	ACrc16Exception(AllianceException & e);
	ACrc16Exception(AllianceException & e, std::string text);
	ACrc16Exception(const ACrc16Exception & obj);
	~ACrc16Exception();
};

#endif

