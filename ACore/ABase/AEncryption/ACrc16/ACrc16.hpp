#ifndef HPP_ACRC16
#define HPP_ACRC16
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/AStruct/AVector/AVector.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bits.hpp>
#include "ACrc16.exception.hpp"

class ACrc16 {
public:
    ACrc16();
    static A8Bits get_hash(A8Bits);
    ~ACrc16();
};

#endif