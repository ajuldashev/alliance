#include "ACrc32.exception.hpp"

ACrc32Exception::ACrc32Exception() : AllianceException() {
	exception = "Alliance-Exception-ACrc32";
}

ACrc32Exception::ACrc32Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACrc32");
}

ACrc32Exception::ACrc32Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACrc32");
}

ACrc32Exception::ACrc32Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACrc32");
}

ACrc32Exception::ACrc32Exception(const ACrc32Exception & obj) : AllianceException(obj) {
	
}

ACrc32Exception::~ACrc32Exception() {

}

