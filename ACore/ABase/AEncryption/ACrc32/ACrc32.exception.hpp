#ifndef HPP_ACRC32EXCEPTION
#define HPP_ACRC32EXCEPTION
#include <alliance.exception.hpp>

class ACrc32Exception : public AllianceException {
public:
	ACrc32Exception();
	ACrc32Exception(std::string text);
	ACrc32Exception(AllianceException & e);
	ACrc32Exception(AllianceException & e, std::string text);
	ACrc32Exception(const ACrc32Exception & obj);
	~ACrc32Exception();
};

#endif

