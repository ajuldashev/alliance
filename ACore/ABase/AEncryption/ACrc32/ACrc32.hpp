#ifndef HPP_ACRC32
#define HPP_ACRC32
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/AStruct/AVector/AVector.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bits.hpp>
#include "ACrc32.exception.hpp"

class ACrc32 {
public:
    ACrc32();
    static A8Bits get_hash(A8Bits);
    ~ACrc32();
};

#endif