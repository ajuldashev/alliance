#include "ACrc64.exception.hpp"

ACrc64Exception::ACrc64Exception() : AllianceException() {
	exception = "Alliance-Exception-ACrc64";
}

ACrc64Exception::ACrc64Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACrc64");
}

ACrc64Exception::ACrc64Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACrc64");
}

ACrc64Exception::ACrc64Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACrc64");
}

ACrc64Exception::ACrc64Exception(const ACrc64Exception & obj) : AllianceException(obj) {
	
}

ACrc64Exception::~ACrc64Exception() {

}

