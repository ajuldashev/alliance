#ifndef HPP_ACRC64EXCEPTION
#define HPP_ACRC64EXCEPTION
#include <alliance.exception.hpp>

class ACrc64Exception : public AllianceException {
public:
	ACrc64Exception();
	ACrc64Exception(std::string text);
	ACrc64Exception(AllianceException & e);
	ACrc64Exception(AllianceException & e, std::string text);
	ACrc64Exception(const ACrc64Exception & obj);
	~ACrc64Exception();
};

#endif

