#ifndef HPP_ACRC64
#define HPP_ACRC64
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/AStruct/AVector/AVector.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bits.hpp>
#include "ACrc64.exception.hpp"

class ACrc64 {
public:
    ACrc64();
    static A8Bits get_hash(u64int, A8Bits, u64int);
    ~ACrc64();
};

#endif