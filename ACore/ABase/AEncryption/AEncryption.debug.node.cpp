#include "AEncryption.hpp"

#ifdef DEBUG_AENCRYPTION
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aencryption(AString com) {
	#ifdef DEBUG_ACRC16
	if (com == "acrc16") {
		if (debug_acrc16()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACRC32
	if (com == "acrc32") {
		if (debug_acrc32()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACRC64
	if (com == "acrc64") {
		if (debug_acrc64()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASHA1
	if (com == "asha1") {
		if (debug_asha1()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASHA256
	if (com == "asha256") {
		if (debug_asha256()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASHA3
	if (com == "asha3") {
		if (debug_asha3()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASHA512
	if (com == "asha512") {
		if (debug_asha512()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aencryption() {
	AString text;
	#ifdef DEBUG_ACRC16
	text += "d) acrc16\n";
	#endif
	#ifdef DEBUG_ACRC32
	text += "d) acrc32\n";
	#endif
	#ifdef DEBUG_ACRC64
	text += "d) acrc64\n";
	#endif
	#ifdef DEBUG_ASHA1
	text += "d) asha1\n";
	#endif
	#ifdef DEBUG_ASHA256
	text += "d) asha256\n";
	#endif
	#ifdef DEBUG_ASHA3
	text += "d) asha3\n";
	#endif
	#ifdef DEBUG_ASHA512
	text += "d) asha512\n";
	#endif
	return text;
}

#endif
