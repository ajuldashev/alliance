#include "AEncryption.exception.hpp"

AEncryptionException::AEncryptionException() : AllianceException() {
	exception = "Alliance-Exception-AEncryption";
}

AEncryptionException::AEncryptionException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AEncryption");
}

AEncryptionException::AEncryptionException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AEncryption");
}

AEncryptionException::AEncryptionException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AEncryption");
}

AEncryptionException::AEncryptionException(const AEncryptionException & obj) : AllianceException(obj) {
	
}

AEncryptionException::~AEncryptionException() {

}

