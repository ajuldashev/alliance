#ifndef HPP_AENCRYPTIONEXCEPTION
#define HPP_AENCRYPTIONEXCEPTION
#include <alliance.exception.hpp>

class AEncryptionException : public AllianceException {
public:
	AEncryptionException();
	AEncryptionException(std::string text);
	AEncryptionException(AllianceException & e);
	AEncryptionException(AllianceException & e, std::string text);
	AEncryptionException(const AEncryptionException & obj);
	~AEncryptionException();
};

#endif

