#include "AEncryption.hpp"

#ifdef TEST_AENCRYPTION
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aencryption(bool is_soft) {
	#ifdef TEST_ACRC16
	if (!test_acrc16(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACRC16 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACRC32
	if (!test_acrc32(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACRC32 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACRC64
	if (!test_acrc64(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACRC64 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASHA1
	if (!test_asha1(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASHA1 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASHA256
	if (!test_asha256(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASHA256 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASHA3
	if (!test_asha3(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASHA3 );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASHA512
	if (!test_asha512(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASHA512 );
		return is_soft;
	}
	#endif
	return true;
}
#endif
