#include "ASha1.hpp"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "ASha1.test.cpp"
#include "ASha1.test.node.cpp"
#include "ASha1.debug.cpp"
#include "ASha1.debug.node.cpp"
#include "ASha1.exception.cpp"

u32int ASha1::lrot(u32int x, s32int bits) {
	return (x<<bits) | (x>>(32 - bits));
}

A8Bits ASha1::store_big_endian_u32int(A8Bits byte, s32int pos, u32int num) {
	byte[pos] = (u8int)(num >> 24);
	byte[pos + 1] = (u8int)(num >> 16);
	byte[pos + 2] = (u8int)(num >> 8);
	byte[pos + 3] = (u8int)num;
	return byte;
}

ASha1::ASha1() {
	if (sizeof(u32int) == 4) {
		H0 = 0x67452301;
		H1 = 0xefcdab89;
		H2 = 0x98badcfe;
		H3 = 0x10325476;
		H4 = 0xc3d2e1f0;
		unprocessedBytes = 0;
		size = 0;
		bytes.clear();
		bytes = A8Bits(64, 0);
	}
}

ASha1::~ASha1() {
	H0 = H1 = H2 = H3 = H4 = 0;
	bytes.clear();
	bytes = A8Bits(64, 0);
	unprocessedBytes = size = 0;
}

void ASha1::process() {
	if (unprocessedBytes == 64) {
		s32int t;
		u32int a, b, c, d, e, K, f, W[80];
		a = H0;
		b = H1;
		c = H2;
		d = H3;
		e = H4;
		for (t = 0; t < 16; t += 1) {
			W[t] = (bytes[t * 4] << 24) + (bytes[t * 4 + 1] << 16)
					+ (bytes[t * 4 + 2] << 8) + bytes[t * 4 + 3];
		}
		for (; t< 80; t += 1 ) {
			W[t] = lrot( W[t-3]^W[t-8]^W[t-14]^W[t-16], 1);
		}
		u32int temp;
		for (t = 0; t < 80; t += 1) {
			if (t < 20) {
				K = 0x5a827999;
				f = (b & c) | ((b ^ 0xFFFFFFFF) & d);
			} else {
				if (t < 40) {
					K = 0x6ed9eba1;
					f = b ^ c ^ d;
				} else {
					if (t < 60) {
						K = 0x8f1bbcdc;
						f = (b & c) | (b & d) | (c & d);
					} else {
						K = 0xca62c1d6;
						f = b ^ c ^ d;
					}
				}
			}
			temp = lrot(a,5) + f + e + W[t] + K;
			e = d;
			d = c;
			c = lrot(b,30);
			b = a;
			a = temp;
		}
		H0 += a;
		H1 += b;
		H2 += c;
		H3 += d;
		H4 += e;
		unprocessedBytes = 0;
	}
}

void ASha1::add_bytes(A8Bits data, u32int num) {
	if (data.length() != 0) {
		if (num > 0) {
			size += num;
			s32int pos = 0;
			while (num > 0) {
				s32int needed = 64 - unprocessedBytes;
				if (needed > 0) {
					s32int toCopy = (num < needed) ? num : needed;
					for (u64int i = 0; i < (u64int) toCopy; i += 1) {
						bytes[unprocessedBytes + i] = data[pos + i];
					}
					num -= toCopy;
					pos += toCopy;
					unprocessedBytes += toCopy;
					if (unprocessedBytes == 64) {
						process();
					}
				}
			}
		}
	}
}

A8Bits ASha1::get_digest() {
	u32int totalBitsL = size << 3;
	u32int totalBitsH = size >> 29;
	add_bytes(A8Bits("\x80"), 1);
	A8Bits footer(64, 0);
	if (unprocessedBytes > 56) {
		add_bytes(footer, 64 - unprocessedBytes);
	}
	if (unprocessedBytes <= 56) {
		s32int neededZeros = 56 - unprocessedBytes;
		footer = store_big_endian_u32int(footer, neededZeros, totalBitsH);
		footer = store_big_endian_u32int(footer, neededZeros + 4, totalBitsL);
		add_bytes(footer, neededZeros + 8);
	}
	A8Bits digest(20, 0);
	digest = store_big_endian_u32int(digest, 0, H0);
	digest = store_big_endian_u32int(digest, 4, H1);
	digest = store_big_endian_u32int(digest, 8, H2);
	digest = store_big_endian_u32int(digest, 12, H3);
	digest = store_big_endian_u32int(digest, 16, H4);
	return digest;
}

A8Bits ASha1::get_hash(A8Bits data) {
	if (data.length() == 0) {
		return A8Bits(AString(""));
	}
	ASha1 sha1;
	sha1.add_bytes(data, (u32int) data.length());
	A8Bits res = sha1.get_digest();
	return res;
}