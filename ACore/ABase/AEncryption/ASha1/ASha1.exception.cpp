#include "ASha1.exception.hpp"

ASha1Exception::ASha1Exception() : AllianceException() {
	exception = "Alliance-Exception-ASha1";
}

ASha1Exception::ASha1Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ASha1");
}

ASha1Exception::ASha1Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ASha1");
}

ASha1Exception::ASha1Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ASha1");
}

ASha1Exception::ASha1Exception(const ASha1Exception & obj) : AllianceException(obj) {
	
}

ASha1Exception::~ASha1Exception() {

}

