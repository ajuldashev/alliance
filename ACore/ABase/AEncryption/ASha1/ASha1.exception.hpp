#ifndef HPP_ASHA1EXCEPTION
#define HPP_ASHA1EXCEPTION
#include <alliance.exception.hpp>

class ASha1Exception : public AllianceException {
public:
	ASha1Exception();
	ASha1Exception(std::string text);
	ASha1Exception(AllianceException & e);
	ASha1Exception(AllianceException & e, std::string text);
	ASha1Exception(const ASha1Exception & obj);
	~ASha1Exception();
};

#endif

