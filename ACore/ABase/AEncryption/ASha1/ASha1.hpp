#ifndef HPP_ASHA1
#define HPP_ASHA1
#include <alliance.config.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bits.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include "ASha1.exception.hpp"

class ASha1 {
    private:
		u32int H0, H1, H2, H3, H4;
		A8Bits bytes;
		s32int unprocessedBytes;
		u32int size;
		void process();
		void add_bytes(A8Bits, u32int);
		A8Bits get_digest();
		u32int lrot(u32int, s32int);
		A8Bits store_big_endian_u32int(A8Bits, s32int, u32int);
	public:
		ASha1();
		~ASha1();
		static A8Bits get_hash(A8Bits);
};

#endif