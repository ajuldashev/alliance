#include "ASha256.hpp"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "ASha256.test.cpp"
#include "ASha256.test.node.cpp"
#include "ASha256.debug.cpp"
#include "ASha256.debug.node.cpp"
#include "ASha256.exception.cpp"

static u32int DIGEST_SIZE = (256/8);
static const u32int SHA224_256_BLOCK_SIZE = (512/8);

const u32int sha256_k[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
	0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
	0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
	0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
	0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
	0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

ASha256::ASha256() {
    m_block = A8Bits(2*SHA224_256_BLOCK_SIZE, 0);
    m_h[0] = 0x6a09e667;
    m_h[1] = 0xbb67ae85;
    m_h[2] = 0x3c6ef372;
    m_h[3] = 0xa54ff53a;
    m_h[4] = 0x510e527f;
    m_h[5] = 0x9b05688c;
    m_h[6] = 0x1f83d9ab;
    m_h[7] = 0x5be0cd19;
    m_len = 0;
    m_tot_len = 0;
}

ASha256::~ASha256() {
    m_block = A8Bits(2*SHA224_256_BLOCK_SIZE, 0);
}

void ASha256::transform(A8Bits message, u32int block_nb) {
    u32int w[64];
    u32int wv[8];
    u32int t1, t2;
    A8Bits sub_block;
    s32int i;
    s32int j;
    s32int idx = 0;
    for (i = 0; i < (s32int) block_nb; i++) {
        sub_block.clear();
        for (s32int k = i << 6; k < message.length(); k += 1) {
            sub_block.push_end(message[k]);
        }
        for (j = 0; j < 16; j++) {
            w[j] = ((u32int) sub_block[(j << 2) + 3])
            | ((u32int) sub_block[(j << 2) + 2] <<  8)
            | ((u32int) sub_block[(j << 2) + 1] << 16)
            | ((u32int) sub_block[j << 2] << 24);
        }
        for (j = 16; j < 64; j++) {
            w[j] =  SHA256_F4(w[j -  2]) + w[j -  7] + SHA256_F3(w[j - 15]) + w[j - 16];
        }
        for (j = 0; j < 8; j++) {
            wv[j] = m_h[j];
        }
        for (j = 0; j < 64; j++) {
            t1 = wv[7] + SHA256_F2(wv[4]) + SHA2_CH(wv[4], wv[5], wv[6])
                + sha256_k[j] + w[j];
            t2 = SHA256_F1(wv[0]) + SHA2_MAJ(wv[0], wv[1], wv[2]);
            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3] + t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1 + t2;
        }
        for (j = 0; j < 8; j++) {
            m_h[j] += wv[j];
        }
    }
}

void ASha256::update(A8Bits message, u32int len) {
    u32int block_nb;
    u32int new_len, rem_len, tmp_len;
    A8Bits shifted_message;
    tmp_len = SHA224_256_BLOCK_SIZE - m_len;
    rem_len = len < tmp_len ? len : tmp_len;
    for (u64int i = 0; i < (u64int) rem_len; i += 1) {
        m_block[m_len + i] = message[i];
    }
    if (m_len + len < SHA224_256_BLOCK_SIZE) {
        m_len += len;
        return;
    }
    new_len = len - rem_len;
    block_nb = new_len / SHA224_256_BLOCK_SIZE;
    shifted_message.clear();
    for (s32int k = rem_len; k < message.length(); k += 1) {
        shifted_message.push_end(message[k]);
    }
    transform(m_block, 1);
    transform(shifted_message, block_nb);
    rem_len = new_len % SHA224_256_BLOCK_SIZE;
    for (u64int i = 0; i < (u64int) rem_len; i += 1) {
        m_block[i] = shifted_message[(block_nb << 6) + i];
    }
    m_len = rem_len;
    m_tot_len += (block_nb + 1) << 6;
}

A8Bits ASha256::final(A8Bits digest) {
    u32int block_nb;
    u32int pm_len;
    u32int len_b;
    s32int i;
    block_nb = (1 + ((SHA224_256_BLOCK_SIZE - 9) < (m_len % SHA224_256_BLOCK_SIZE)));
    len_b = (m_tot_len + m_len) << 3;
    pm_len = block_nb << 6;
    for (u64int i = 0; i < (u64int) (pm_len - m_len); i += 1) {
        m_block[m_len + i] = 0;
    }
    m_block[m_len] = 0x80;
    m_block[pm_len - 1] = (u8int) (len_b);
    m_block[pm_len - 2] = (u8int) (len_b >>  8);
    m_block[pm_len - 3] = (u8int) (len_b >> 16);
    m_block[pm_len - 4] = (u8int) (len_b >> 24);  
    transform(m_block, block_nb);
    for (i = 0 ; i < 8; i++) {
        digest[(i << 2) + 3] = (u8int) (m_h[i]);
        digest[(i << 2) + 2] = (u8int) (m_h[i] >>  8);
        digest[(i << 2) + 1] = (u8int) (m_h[i] >> 16);
        digest[i << 2] = (u8int) (m_h[i] >> 24);  
    }
    return digest;
}

A8Bits ASha256::get_hash(A8Bits data) {
    A8Bits digest(DIGEST_SIZE, 0);
    ASha256 ctx = ASha256();
    ctx.update(data, data.length());
    digest = ctx.final(digest);
    return digest;
}