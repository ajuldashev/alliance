#include "ASha256.hpp"

#ifdef DEBUG_ASHA256
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_asha256() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASHA256 );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_asha256());
            AIO::writeln("1) help");
            AIO::writeln("2) get_hash'");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "get_hash") {
            AIO::writeln(ASha256::get_hash(A8Bits(AIO::get_word())).to_text());
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_asha256(com)) {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASHA256 );
    AIO::write_div_line();
    return false;
}
#endif

