#include "ASha256.exception.hpp"

ASha256Exception::ASha256Exception() : AllianceException() {
	exception = "Alliance-Exception-ASha256";
}

ASha256Exception::ASha256Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ASha256");
}

ASha256Exception::ASha256Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ASha256");
}

ASha256Exception::ASha256Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ASha256");
}

ASha256Exception::ASha256Exception(const ASha256Exception & obj) : AllianceException(obj) {
	
}

ASha256Exception::~ASha256Exception() {

}

