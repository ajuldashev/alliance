#ifndef HPP_ASHA256EXCEPTION
#define HPP_ASHA256EXCEPTION
#include <alliance.exception.hpp>

class ASha256Exception : public AllianceException {
public:
	ASha256Exception();
	ASha256Exception(std::string text);
	ASha256Exception(AllianceException & e);
	ASha256Exception(AllianceException & e, std::string text);
	ASha256Exception(const ASha256Exception & obj);
	~ASha256Exception();
};

#endif

