#include "ASha3.hpp"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "ASha3.test.cpp"
#include "ASha3.test.node.cpp"
#include "ASha3.debug.cpp"
#include "ASha3.debug.node.cpp"
#include "ASha3.exception.cpp"

/*enum Bits { Bits224 = 224, Bits256 = 256, Bits384 = 384, Bits512 = 512 };
enum { StateSize    = 1600 / (8 * 8),
        MaxBlockSize =  200 - 2 * (224 / 8) };
  static u64int m_hash[StateSize];
  static u64int m_numBytes;
  static Bits     m_bits  = Bits256;
  static size_t   m_blockSize = 200 - 2 * (m_bits / 8);
  static size_t   m_bufferSize;
  static u8int  m_buffer[MaxBlockSize];

ASha3::ASha3() {

}

void ASha3::reset()
{
  for (size_t i = 0; i < StateSize; i++) {
    m_hash[i] = 0;
  }
  m_numBytes   = 0;
  m_bufferSize = 0;
}

namespace
{
  const u32int Rounds = 24;
  const u64int XorMasks[Rounds] =
  {
    0x0000000000000001ULL, 0x0000000000008082ULL, 0x800000000000808aULL,
    0x8000000080008000ULL, 0x000000000000808bULL, 0x0000000080000001ULL,
    0x8000000080008081ULL, 0x8000000000008009ULL, 0x000000000000008aULL,
    0x0000000000000088ULL, 0x0000000080008009ULL, 0x000000008000000aULL,
    0x000000008000808bULL, 0x800000000000008bULL, 0x8000000000008089ULL,
    0x8000000000008003ULL, 0x8000000000008002ULL, 0x8000000000000080ULL,
    0x000000000000800aULL, 0x800000008000000aULL, 0x8000000080008081ULL,
    0x8000000000008080ULL, 0x0000000080000001ULL, 0x8000000080008008ULL
  };

  inline u64int rotateLeft(u64int x, u8int numBits)
  {
    return (x << numBits) | (x >> (64 - numBits));
  }

  inline u64int swap(u64int x)
  {
#if defined(__GNUC__) || defined(__clang__)
    return __builtin_bswap64(x);
#endif
#ifdef _MSC_VER
    return _byteswap_uint64(x);
#endif
    return  (x >> 56) |
           ((x >> 40) & 0x000000000000FF00ULL) |
           ((x >> 24) & 0x0000000000FF0000ULL) |
           ((x >>  8) & 0x00000000FF000000ULL) |
           ((x <<  8) & 0x000000FF00000000ULL) |
           ((x << 24) & 0x0000FF0000000000ULL) |
           ((x << 40) & 0x00FF000000000000ULL) |
            (x << 56);
  }

  u32int mod5(u32int x)
  {
    if (x < 5)
      return x;
    return x - 5;
  }
}

void ASha3::processBlock(const void* data)
{
#if defined(__BYTE_ORDER) && (__BYTE_ORDER != 0) && (__BYTE_ORDER == __BIG_ENDIAN)
#define LITTLEENDIAN(x) swap(x)
#else
#define LITTLEENDIAN(x) (x)
#endif
  const u64int* data64 = (const u64int*) data;

  for (u32int i = 0; i < m_blockSize / 8; i++)
    m_hash[i] ^= LITTLEENDIAN(data64[i]);

  for (u32int round = 0; round < Rounds; round++)
  {

    u64int coefficients[5];
    for (u32int i = 0; i < 5; i++)
      coefficients[i] = m_hash[i] ^ m_hash[i + 5] ^ m_hash[i + 10] ^ m_hash[i + 15] ^ m_hash[i + 20];
    for (u32int i = 0; i < 5; i++)
    {
      u64int one = coefficients[mod5(i + 4)] ^ rotateLeft(coefficients[mod5(i + 1)], 1);
      m_hash[i     ] ^= one;
      m_hash[i +  5] ^= one;
      m_hash[i + 10] ^= one;
      m_hash[i + 15] ^= one;
      m_hash[i + 20] ^= one;
    }

    u64int one;

    u64int last = m_hash[1];
    one = m_hash[10]; m_hash[10] = rotateLeft(last,  1); last = one;
    one = m_hash[ 7]; m_hash[ 7] = rotateLeft(last,  3); last = one;
    one = m_hash[11]; m_hash[11] = rotateLeft(last,  6); last = one;
    one = m_hash[17]; m_hash[17] = rotateLeft(last, 10); last = one;
    one = m_hash[18]; m_hash[18] = rotateLeft(last, 15); last = one;
    one = m_hash[ 3]; m_hash[ 3] = rotateLeft(last, 21); last = one;
    one = m_hash[ 5]; m_hash[ 5] = rotateLeft(last, 28); last = one;
    one = m_hash[16]; m_hash[16] = rotateLeft(last, 36); last = one;
    one = m_hash[ 8]; m_hash[ 8] = rotateLeft(last, 45); last = one;
    one = m_hash[21]; m_hash[21] = rotateLeft(last, 55); last = one;
    one = m_hash[24]; m_hash[24] = rotateLeft(last,  2); last = one;
    one = m_hash[ 4]; m_hash[ 4] = rotateLeft(last, 14); last = one;
    one = m_hash[15]; m_hash[15] = rotateLeft(last, 27); last = one;
    one = m_hash[23]; m_hash[23] = rotateLeft(last, 41); last = one;
    one = m_hash[19]; m_hash[19] = rotateLeft(last, 56); last = one;
    one = m_hash[13]; m_hash[13] = rotateLeft(last,  8); last = one;
    one = m_hash[12]; m_hash[12] = rotateLeft(last, 25); last = one;
    one = m_hash[ 2]; m_hash[ 2] = rotateLeft(last, 43); last = one;
    one = m_hash[20]; m_hash[20] = rotateLeft(last, 62); last = one;
    one = m_hash[14]; m_hash[14] = rotateLeft(last, 18); last = one;
    one = m_hash[22]; m_hash[22] = rotateLeft(last, 39); last = one;
    one = m_hash[ 9]; m_hash[ 9] = rotateLeft(last, 61); last = one;
    one = m_hash[ 6]; m_hash[ 6] = rotateLeft(last, 20); last = one;
                      m_hash[ 1] = rotateLeft(last, 44);

    for (u32int j = 0; j < 25; j += 5)
    {

      u64int one = m_hash[j];
      u64int two = m_hash[j + 1];
      m_hash[j]     ^= m_hash[j + 2] & ~two;
      m_hash[j + 1] ^= m_hash[j + 3] & ~m_hash[j + 2];
      m_hash[j + 2] ^= m_hash[j + 4] & ~m_hash[j + 3];
      m_hash[j + 3] ^=      one      & ~m_hash[j + 4];
      m_hash[j + 4] ^=      two      & ~one;
    }

    m_hash[0] ^= XorMasks[round];
  }
}

void ASha3::add(const void* data, size_t numBytes)
{
  const u8int* current = (const u8int*) data;

  if (m_bufferSize > 0)
  {
    while (numBytes > 0 && m_bufferSize < m_blockSize)
    {
      m_buffer[m_bufferSize++] = *current++;
      numBytes--;
    }
  }

  if (m_bufferSize == m_blockSize)
  {
    processBlock((void*)m_buffer);
    m_numBytes  += m_blockSize;
    m_bufferSize = 0;
  }

  if (numBytes == 0)
    return;

  while (numBytes >= m_blockSize)
  {
    processBlock(current);
    current    += m_blockSize;
    m_numBytes += m_blockSize;
    numBytes   -= m_blockSize;
  }

  while (numBytes > 0)
  {
    m_buffer[m_bufferSize++] = *current++;
    numBytes--;
  }
}

void ASha3::processBuffer()
{

  size_t offset = m_bufferSize;

  m_buffer[offset++] = 0x06;

  while (offset < m_blockSize)
    m_buffer[offset++] = 0;

  m_buffer[offset - 1] |= 0x80;
  processBlock(m_buffer);
}

A8Bits ASha3::get_hash(A8Bits data) {
  reset();
  add(data.to_string().c_str(), data.length());
  processBuffer();

  static const char dec2hex[16 + 1] = "0123456789abcdef";

  u32int hashLength = m_bits / 64;
  std::string result;
  result.reserve(m_bits / 4);
  for (u32int i = 0; i < hashLength; i++)
    for (u32int j = 0; j < 8; j++) 
    {

      u8int oneByte = (u8int) (m_hash[i] >> (8 * j));
      result += dec2hex[oneByte >> 4];
      result += dec2hex[oneByte & 15];
    }

  u32int remainder = m_bits - hashLength * 64;
  u32int processed = 0;
  while (processed < remainder)
  {

    u8int oneByte = (u8int) (m_hash[hashLength] >> processed);
    result += dec2hex[oneByte >> 4];
    result += dec2hex[oneByte & 15];
    processed += 8;
  }
  A8Bits res(result.c_str());
  return res;
}*/

/*enum Bits { Bits224 = 224, Bits256 = 256, Bits384 = 384, Bits512 = 512 };
enum { StateSize    = 1600 / (8 * 8),
        MaxBlockSize =  200 - 2 * (224 / 8) };
  static u64int m_hash[StateSize];
  static u64int m_numBytes;
  static Bits     m_bits  = Bits256;
  static size_t   m_blockSize = 200 - 2 * (m_bits / 8);
  static size_t   m_bufferSize;
  static A8Bits  m_buffer(MaxBlockSize, 0);

ASha3::ASha3() {

}

void ASha3::reset()
{
  for (size_t i = 0; i < StateSize; i++) {
    m_hash[i] = 0;
  }
  m_numBytes   = 0;
  m_bufferSize = 0;
}

namespace
{
  const u32int Rounds = 24;
  const u64int XorMasks[Rounds] =
  {
    0x0000000000000001ULL, 0x0000000000008082ULL, 0x800000000000808aULL,
    0x8000000080008000ULL, 0x000000000000808bULL, 0x0000000080000001ULL,
    0x8000000080008081ULL, 0x8000000000008009ULL, 0x000000000000008aULL,
    0x0000000000000088ULL, 0x0000000080008009ULL, 0x000000008000000aULL,
    0x000000008000808bULL, 0x800000000000008bULL, 0x8000000000008089ULL,
    0x8000000000008003ULL, 0x8000000000008002ULL, 0x8000000000000080ULL,
    0x000000000000800aULL, 0x800000008000000aULL, 0x8000000080008081ULL,
    0x8000000000008080ULL, 0x0000000080000001ULL, 0x8000000080008008ULL
  };

  inline u64int rotateLeft(u64int x, u8int numBits)
  {
    return (x << numBits) | (x >> (64 - numBits));
  }

  inline u64int swap(u64int x)
  {
#if defined(__GNUC__) || defined(__clang__)
    return __builtin_bswap64(x);
#endif
#ifdef _MSC_VER
    return _byteswap_uint64(x);
#endif
    return  (x >> 56) |
           ((x >> 40) & 0x000000000000FF00ULL) |
           ((x >> 24) & 0x0000000000FF0000ULL) |
           ((x >>  8) & 0x00000000FF000000ULL) |
           ((x <<  8) & 0x000000FF00000000ULL) |
           ((x << 24) & 0x0000FF0000000000ULL) |
           ((x << 40) & 0x00FF000000000000ULL) |
            (x << 56);
  }

  u32int mod5(u32int x)
  {
    if (x < 5)
      return x;
    return x - 5;
  }
}

void ASha3::processBlock(A8Bits data)
{
#if defined(__BYTE_ORDER) && (__BYTE_ORDER != 0) && (__BYTE_ORDER == __BIG_ENDIAN)
#define LITTLEENDIAN(x) swap(x)
#else
#define LITTLEENDIAN(x) (x)
#endif
  APtr<u64int> data64((const u64int*) data);
  for (u32int i = 0; i < m_blockSize / 8; i++)
    m_hash[i] ^= LITTLEENDIAN(data64[i]);

  for (u32int round = 0; round < Rounds; round++)
  {

    u64int coefficients[5];
    for (u32int i = 0; i < 5; i++)
      coefficients[i] = m_hash[i] ^ m_hash[i + 5] ^ m_hash[i + 10] ^ m_hash[i + 15] ^ m_hash[i + 20];
    for (u32int i = 0; i < 5; i++)
    {
      u64int one = coefficients[mod5(i + 4)] ^ rotateLeft(coefficients[mod5(i + 1)], 1);
      m_hash[i     ] ^= one;
      m_hash[i +  5] ^= one;
      m_hash[i + 10] ^= one;
      m_hash[i + 15] ^= one;
      m_hash[i + 20] ^= one;
    }

    u64int one;

    u64int last = m_hash[1];
    one = m_hash[10]; m_hash[10] = rotateLeft(last,  1); last = one;
    one = m_hash[ 7]; m_hash[ 7] = rotateLeft(last,  3); last = one;
    one = m_hash[11]; m_hash[11] = rotateLeft(last,  6); last = one;
    one = m_hash[17]; m_hash[17] = rotateLeft(last, 10); last = one;
    one = m_hash[18]; m_hash[18] = rotateLeft(last, 15); last = one;
    one = m_hash[ 3]; m_hash[ 3] = rotateLeft(last, 21); last = one;
    one = m_hash[ 5]; m_hash[ 5] = rotateLeft(last, 28); last = one;
    one = m_hash[16]; m_hash[16] = rotateLeft(last, 36); last = one;
    one = m_hash[ 8]; m_hash[ 8] = rotateLeft(last, 45); last = one;
    one = m_hash[21]; m_hash[21] = rotateLeft(last, 55); last = one;
    one = m_hash[24]; m_hash[24] = rotateLeft(last,  2); last = one;
    one = m_hash[ 4]; m_hash[ 4] = rotateLeft(last, 14); last = one;
    one = m_hash[15]; m_hash[15] = rotateLeft(last, 27); last = one;
    one = m_hash[23]; m_hash[23] = rotateLeft(last, 41); last = one;
    one = m_hash[19]; m_hash[19] = rotateLeft(last, 56); last = one;
    one = m_hash[13]; m_hash[13] = rotateLeft(last,  8); last = one;
    one = m_hash[12]; m_hash[12] = rotateLeft(last, 25); last = one;
    one = m_hash[ 2]; m_hash[ 2] = rotateLeft(last, 43); last = one;
    one = m_hash[20]; m_hash[20] = rotateLeft(last, 62); last = one;
    one = m_hash[14]; m_hash[14] = rotateLeft(last, 18); last = one;
    one = m_hash[22]; m_hash[22] = rotateLeft(last, 39); last = one;
    one = m_hash[ 9]; m_hash[ 9] = rotateLeft(last, 61); last = one;
    one = m_hash[ 6]; m_hash[ 6] = rotateLeft(last, 20); last = one;
                      m_hash[ 1] = rotateLeft(last, 44);

    for (u32int j = 0; j < 25; j += 5)
    {

      u64int one = m_hash[j];
      u64int two = m_hash[j + 1];
      m_hash[j]     ^= m_hash[j + 2] & ~two;
      m_hash[j + 1] ^= m_hash[j + 3] & ~m_hash[j + 2];
      m_hash[j + 2] ^= m_hash[j + 4] & ~m_hash[j + 3];
      m_hash[j + 3] ^=      one      & ~m_hash[j + 4];
      m_hash[j + 4] ^=      two      & ~one;
    }

    m_hash[0] ^= XorMasks[round];
  }
}

void ASha3::add(A8Bits data, size_t numBytes)
{
  A8Bits current = data;
  s32int idx = 0;
  if (m_bufferSize > 0)
  {
    while (numBytes > 0 && m_bufferSize < m_blockSize)
    {
      m_buffer[m_bufferSize++] = current[idx];
      idx += 1;
      numBytes--;
    }
  }
  if (m_bufferSize == m_blockSize)
  {
    /*m_buffer = *//*processBlock(m_buffer);
    m_numBytes  += m_blockSize;
    m_bufferSize = 0;
  }
  if (numBytes == 0) {
    return;
  }
  while (numBytes >= m_blockSize)
  {*/
    /*current = *//*processBlock(current);
    idx += m_blockSize;
    m_numBytes += m_blockSize;
    numBytes   -= m_blockSize;
  }
  while (numBytes > 0)
  {
    m_buffer[m_bufferSize] = current[idx];
    m_bufferSize += 1;
    idx += 1;
    numBytes--;
  }
}

void ASha3::processBuffer()
{

  size_t offset = m_bufferSize;

  m_buffer[offset++] = 0x06;

  while (offset < m_blockSize)
    m_buffer[offset++] = 0;

  m_buffer[offset - 1] |= 0x80;*/
  /*m_buffer = *//*processBlock(m_buffer);
}

A8Bits ASha3::get_hash(A8Bits data) {
  reset();
  add(data, data.length());
  processBuffer();

  static const char dec2hex[16 + 1] = "0123456789abcdef";

  u32int hashLength = m_bits / 64;
  std::string result;
  result.reserve(m_bits / 4);
  for (u32int i = 0; i < hashLength; i++)
    for (u32int j = 0; j < 8; j++) 
    {

      u8int oneByte = (u8int) (m_hash[i] >> (8 * j));
      result += dec2hex[oneByte >> 4];
      result += dec2hex[oneByte & 15];
    }

  u32int remainder = m_bits - hashLength * 64;
  u32int processed = 0;
  while (processed < remainder)
  {

    u8int oneByte = (u8int) (m_hash[hashLength] >> processed);
    result += dec2hex[oneByte >> 4];
    result += dec2hex[oneByte & 15];
    processed += 8;
  }
  A8Bits res(result.c_str());
  return res;
}*/

ASha3::~ASha3() {

}