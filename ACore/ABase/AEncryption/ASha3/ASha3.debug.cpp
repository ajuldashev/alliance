#include "ASha3.hpp"

#ifdef DEBUG_ASHA3
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_asha3() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASHA3 );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_asha3());
            AIO::writeln("1) help");
            AIO::writeln("2) get_hash'");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "get_hash") {
            /*A8Bits data(AIO::get_word());
            AIO::writeln(ASha3::get_hash(data).to_string());*/
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_asha3(com)) {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASHA3 );
    AIO::write_div_line();
    return false;
}
#endif

