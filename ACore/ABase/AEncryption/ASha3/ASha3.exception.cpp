#include "ASha3.exception.hpp"

ASha3Exception::ASha3Exception() : AllianceException() {
	exception = "Alliance-Exception-ASha3";
}

ASha3Exception::ASha3Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ASha3");
}

ASha3Exception::ASha3Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ASha3");
}

ASha3Exception::ASha3Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ASha3");
}

ASha3Exception::ASha3Exception(const ASha3Exception & obj) : AllianceException(obj) {
	
}

ASha3Exception::~ASha3Exception() {

}

