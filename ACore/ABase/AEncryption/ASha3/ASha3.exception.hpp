#ifndef HPP_ASHA3EXCEPTION
#define HPP_ASHA3EXCEPTION
#include <alliance.exception.hpp>

class ASha3Exception : public AllianceException {
public:
	ASha3Exception();
	ASha3Exception(std::string text);
	ASha3Exception(AllianceException & e);
	ASha3Exception(AllianceException & e, std::string text);
	ASha3Exception(const ASha3Exception & obj);
	~ASha3Exception();
};

#endif

