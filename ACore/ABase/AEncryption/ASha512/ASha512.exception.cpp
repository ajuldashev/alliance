#include "ASha512.exception.hpp"

ASha512Exception::ASha512Exception() : AllianceException() {
	exception = "Alliance-Exception-ASha512";
}

ASha512Exception::ASha512Exception(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ASha512");
}

ASha512Exception::ASha512Exception(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ASha512");
}

ASha512Exception::ASha512Exception(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ASha512");
}

ASha512Exception::ASha512Exception(const ASha512Exception & obj) : AllianceException(obj) {
	
}

ASha512Exception::~ASha512Exception() {

}

