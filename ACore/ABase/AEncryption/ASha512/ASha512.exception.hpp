#ifndef HPP_ASHA512EXCEPTION
#define HPP_ASHA512EXCEPTION
#include <alliance.exception.hpp>

class ASha512Exception : public AllianceException {
public:
	ASha512Exception();
	ASha512Exception(std::string text);
	ASha512Exception(AllianceException & e);
	ASha512Exception(AllianceException & e, std::string text);
	ASha512Exception(const ASha512Exception & obj);
	~ASha512Exception();
};

#endif

