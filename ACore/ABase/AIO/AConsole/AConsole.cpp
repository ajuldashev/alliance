#include "AConsole.hpp"
#include "AConsole.test.cpp"
#include "AConsole.test.node.cpp"
#include "AConsole.debug.cpp"
#include "AConsole.debug.node.cpp"
#include "AConsole.exception.cpp"

AConsoleStyle::AConsoleStyle() {
		style = AConsoleStyleText::normal;
		color = AConsoleColor::white;
		color_bg = AConsoleColorBG::black;
}

AConsoleStyle::AConsoleStyle(const char * style_text_i, const char * color_i, const char * color_bg_i) {
		style    = to_style(AString(style_text_i));
		color    = to_color(AString(color_i));
		color_bg = to_color_bg(AString(color_bg_i));
}

AConsoleStyle::AConsoleStyle(AString style_text_i, AString color_i, AString color_bg_i) {
		style    = to_style(style_text_i);
		color    = to_color(color_i);
		color_bg = to_color_bg(color_bg_i);
}

AString AConsoleStyle::get_style() {
	return AStringLibs::to_string(static_cast<int>(style));
}

AString AConsoleStyle::get_color() {
	return AStringLibs::to_string(static_cast<int>(color));
}

AString AConsoleStyle::get_color_bg() {
	return AStringLibs::to_string(static_cast<int>(color_bg));
}

AConsoleStyle AConsoleStyle::get_normal() {
	AConsoleStyle style;
	style.style = AConsoleStyleText::normal;
	style.color = AConsoleColor::white;
	style.color_bg = AConsoleColorBG::none;
	return style;
}

AConsoleStyle AConsoleStyle::get_success() {
	AConsoleStyle style;
	style.style = AConsoleStyleText::normal;
	style.color = AConsoleColor::green;
	style.color_bg = AConsoleColorBG::none;
	return style;
}

AConsoleStyle AConsoleStyle::get_warning() {
	AConsoleStyle style;
	style.style = AConsoleStyleText::normal;
	style.color = AConsoleColor::yellow;
	style.color_bg = AConsoleColorBG::none;
	return style;
}

AConsoleStyle AConsoleStyle::get_error() {
	AConsoleStyle style;
	style.style = AConsoleStyleText::normal;
	style.color = AConsoleColor::red;
	style.color_bg = AConsoleColorBG::none;
	return style;
}

AConsoleStyleText AConsoleStyle::to_style(AString str) {
	if (str == "normal") {
		return AConsoleStyleText::normal;
	}
	if (str == "bold") {
		return AConsoleStyleText::bold;
	}
	if (str == "underline") {
		return AConsoleStyleText::underline;
	}
	if (str == "blink") {
		return AConsoleStyleText::blink;
	}
	if (str == "invert") {
		return AConsoleStyleText::invert;
	}
	if (str == "invis") {
		return AConsoleStyleText::invis;
	}
	return AConsoleStyleText::normal;
}

AConsoleColor AConsoleStyle::to_color(AString str) {
	if (str == "black") {
		return AConsoleColor::black;
	}
	if (str == "red") {
		return AConsoleColor::red;
	}
	if (str == "green") {
		return AConsoleColor::green;
	}
	if (str == "yellow") {
	return AConsoleColor::yellow;
	}
	if (str == "blue") {
		return AConsoleColor::blue;
	}
	if (str == "purple") {
		return AConsoleColor::purple;
	}
	if (str == "azure") {
		return AConsoleColor::azure;
	}
	if (str == "white") {
		return AConsoleColor::white;
	}
	return AConsoleColor::white;
}

AConsoleColorBG AConsoleStyle::to_color_bg(AString str) {
	if (str == "black") {
		return AConsoleColorBG::black;
	}
	if (str == "red") {
		return AConsoleColorBG::red;
	}
	if (str == "green") {
		return AConsoleColorBG::green;
	}
	if (str == "yellow") {
	return AConsoleColorBG::yellow;
	}
	if (str == "blue") {
		return AConsoleColorBG::blue;
	}
	if (str == "purple") {
		return AConsoleColorBG::purple;
	}
	if (str == "azure") {
		return AConsoleColorBG::azure;
	}
	if (str == "white") {
		return AConsoleColorBG::white;
	}
	return AConsoleColorBG::black;
}

void AConsole::write(const char * str) {
	AString text(str);
	write(text);
}

void AConsole::write(AString str, AConsoleStyle style) {
	std::string text;
	text += "\x1b[";
	text += style.get_style().to_cstring().const_data();
	text += ";" ;
	text += style.get_color().to_cstring().const_data();
	if (style.color_bg != AConsoleColorBG::none) { 
		text += ";";
		text += style.get_color_bg().to_cstring().const_data();
	}
	text += "m";
	text += str.to_cstring().const_data();
	text += "\x1b[0m" ;
	std::cout << text;
}

void AConsole::write_stream_error(AString str, AConsoleStyle style) {
	std::string text;
	text += "\x1b[";
	text += style.get_style().to_cstring().const_data();
	text += ";" ;
	text += style.get_color().to_cstring().const_data();
	if (style.color_bg != AConsoleColorBG::none) { 
		text += ";";
		text += style.get_color_bg().to_cstring().const_data();
	}
	text += "m";
	text += str.to_cstring().const_data();
	text += "\x1b[0m" ;
	std::cerr << text;
}

void AConsole::write(AString str) {
	//AConsoleStyle style = AConsoleStyle::get_normal();
	//write(str, style);
	std::cout << str.to_cstring().const_data();
}

void AConsole::write_warning(AString str) {
	AConsoleStyle style = AConsoleStyle::get_warning();
	write_stream_error(str, style);
}

void AConsole::write_error(AString str) {
	AConsoleStyle style = AConsoleStyle::get_error();
	write_stream_error(str, style);
}

void AConsole::writeln(const char * str) {
	AString text(str);
	writeln(text);
}

void AConsole::writeln(AString str, AConsoleStyle style) {
	std::string text;
	text += "\x1b[";
	text += style.get_style().to_cstring().const_data();
	text += ";" ;
	text += style.get_color().to_cstring().const_data();
	if (style.color_bg != AConsoleColorBG::none) { 
		text += ";";
		text += style.get_color_bg().to_cstring().const_data();
	}
	text += "m";
	text += str.to_cstring().const_data();
	text += "\x1b[0m" ;
	std::cout << text << std::endl;
}

void AConsole::writeln_stream_error(AString str, AConsoleStyle style) {
	std::string text;
	text += "\x1b[";
	text += style.get_style().to_cstring().const_data();
	text += ";" ;
	text += style.get_color().to_cstring().const_data();
	if (style.color_bg != AConsoleColorBG::none) { 
		text += ";";
		text += style.get_color_bg().to_cstring().const_data();
	}
	text += "m";
	text += str.to_cstring().const_data();
	text += "\x1b[0m" ;
	std::cerr << text << std::endl;
}

void AConsole::writeln(AString str) {
	//AConsoleStyle style = AConsoleStyle::get_normal();
	//writeln(str, style);
	std::cout << str.to_cstring().const_data() << std::endl;
}

void AConsole::writeln_warning(AString str) {
	AConsoleStyle style = AConsoleStyle::get_warning();
	writeln_stream_error(str, style);
}

void AConsole::writeln_error(AString str) {
	AConsoleStyle style = AConsoleStyle::get_error();
	writeln_stream_error(str, style);
}

void AConsole::write(char sym) {
	AString text;
	write(text + sym);
}

void AConsole::writeln(char sym) {
	AString text;
	writeln(text + sym);
}

void AConsole::write_warning(char sym) {
	AString text;
	write_warning(text + sym);
}

void AConsole::writeln_warning(char sym) {
	AString text;
	writeln_warning(text + sym);
}

void AConsole::write_error(char sym) {
	AString text;
	write_error(text + sym);
}

void AConsole::writeln_error(char sym) {
	AString text;
	writeln_error(text + sym);
}

void AConsole::write(s16int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(s16int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(s16int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(s16int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(s16int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(s16int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(s32int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(s32int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(s32int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(s32int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(s32int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(s32int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(s64int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(s64int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(s64int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(s64int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(s64int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(s64int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(float num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(float num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(float num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(float num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(float num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(float num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(double num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(double num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(double num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(double num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(double num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(double num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(u8int sym) {
	AString text;
	write(text + sym);
}

void AConsole::writeln(u8int sym) {
	AString text;
	writeln(text + sym);
}

void AConsole::write_warning(u8int sym) {
	AString text;
	write_warning(text + sym);
}

void AConsole::writeln_warning(u8int sym) {
	AString text;
	writeln_warning(text + sym);
}

void AConsole::write_error(u8int sym) {
	AString text;
	write_error(text);
}

void AConsole::writeln_error(u8int sym) {
	AString text;
	writeln_error(text + sym);
}

void AConsole::write(u16int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(u16int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(u16int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(u16int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(u16int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(u16int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(u32int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(u32int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(u32int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(u32int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(u32int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(u32int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(u64int num) {
	write(AStringLibs::to_string(num));
}

void AConsole::writeln(u64int num) {
	writeln(AStringLibs::to_string(num));
}

void AConsole::write_warning(u64int num) {
	write_warning(AStringLibs::to_string(num));
}

void AConsole::writeln_warning(u64int num) {
	writeln_warning(AStringLibs::to_string(num));
}

void AConsole::write_error(u64int num) {
	write_error(AStringLibs::to_string(num));
}

void AConsole::writeln_error(u64int num) {
	writeln_error(AStringLibs::to_string(num));
}

void AConsole::write(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	write(text);
}

void AConsole::writeln(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	writeln(text);
}

void AConsole::write_warning(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	write_warning(text);
}

void AConsole::writeln_warning(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	writeln_warning(text);
}

void AConsole::write_error(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	write_error(text);
}

void AConsole::writeln_error(bool f) {
	AString text;
	if (f) {
		text = "TRUE";
	} else {
		text = "FALSE";
	}
	writeln_error(text);
}

AString AConsole::get_word() {
	AConsoleIBuffer & buff = AConsoleIBuffer::buff();
	if (!buff.is_empty()) {
		AString str = buff.pop();
		std::cout << std::endl;
		return str;
	}
	char str[256];
	std::cin >> str;
	AString res(str);
	return res;
}

AString AConsole::get_line() {
	AConsoleIBuffer & buff = AConsoleIBuffer::buff();
	if (!buff.is_empty()) {
		AString str = buff.pop();
		return str;
	}
	char _str[256];
	std::cin.ignore();
	std::cin.getline(_str, 256, '\n');
	AString str(_str);
	return str;
}

char AConsole::get_char() {
	return AStringLibs::to_int(get_word());
}

s32int AConsole::get_int() {
	return AStringLibs::to_int(get_word());
}

s64int AConsole::get_long() {
	return AStringLibs::to_long(get_word());
}

float AConsole::get_float() {
	return AStringLibs::to_float(get_word());
}

double AConsole::get_double() {
	return AStringLibs::to_double(get_word());
}