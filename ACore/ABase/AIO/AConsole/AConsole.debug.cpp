#include "AConsole.hpp"

#ifdef DEBUG_ACONSOLE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aconsole() {
    AConsole console;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ACONSOLE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aconsole());
            AIO::writeln(" 1) help");
            AIO::writeln(" 3) write_char");
            AIO::writeln(" 4) write_astring");
            AIO::writeln(" 5) write_astring_decor");
            AIO::writeln(" 6) write");
            AIO::writeln(" 7) write_warning");
            AIO::writeln(" 8) write_error");
            AIO::writeln(" 9) get_word");
            AIO::writeln("10) get_line");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        #ifdef DEBUG_ACONSOLEIBUFFER
        if (com == "aconsoleibuffer") {
            if (debug_aconsoleibuffer()) {
                return true;
            }
        }
        #endif
        if (com == "write_char") {
            AIO::writeln((AIO::get_word()).to_cstring().const_data());
        }
        if (com == "write_astring") {
            AIO::writeln(AIO::get_word());
        }
        if (com == "write_astring_decor") {
            AString str1 			= AIO::get_word(); 
            AString style_text_w 	= AIO::get_word(); 
            AString color_w 		= AIO::get_word(); 
            AString color_bg_w 		= AIO::get_word();
            AConsoleStyle style(style_text_w, color_w, color_bg_w);
            console.writeln(str1, style);
        }
        if (com == "write") {
            console.writeln(AIO::get_word());
        }
        if (com == "write_warning") {
            console.writeln_warning(AIO::get_word());
        }
        if (com == "write_error") {
            console.writeln_error(AIO::get_word());
        }
        if (com == "get_word") {
            console.write("str: ");
            console.writeln(console.get_word());
        }
        if (com == "get_line") {
            console.write("str: ");
            console.writeln(console.get_line());
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aconsole(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ACONSOLE );
    AIO::write_div_line();
    return false;
}
#endif

