#include "AConsole.hpp"

#ifdef DEBUG_ACONSOLE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aconsole(AString com) {
	#ifdef DEBUG_ACONSOLEIBUFFER
	if (com == "aconsoleibuffer") {
		if (debug_aconsoleibuffer()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aconsole() {
	AString text;
	#ifdef DEBUG_ACONSOLEIBUFFER
	text += "d) aconsoleibuffer\n";
	#endif
	return text;
}

#endif
