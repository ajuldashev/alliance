#include "AConsole.exception.hpp"

AConsoleException::AConsoleException() : AllianceException() {
	exception = "Alliance-Exception-AConsole";
}

AConsoleException::AConsoleException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AConsole");
}

AConsoleException::AConsoleException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AConsole");
}

AConsoleException::AConsoleException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AConsole");
}

AConsoleException::AConsoleException(const AConsoleException & obj) : AllianceException(obj) {
	
}

AConsoleException::~AConsoleException() {

}

