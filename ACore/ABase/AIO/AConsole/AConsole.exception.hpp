#ifndef HPP_ACONSOLEEXCEPTION
#define HPP_ACONSOLEEXCEPTION
#include <alliance.exception.hpp>

class AConsoleException : public AllianceException {
public:
	AConsoleException();
	AConsoleException(std::string text);
	AConsoleException(AllianceException & e);
	AConsoleException(AllianceException & e, std::string text);
	AConsoleException(const AConsoleException & obj);
	~AConsoleException();
};

#endif

