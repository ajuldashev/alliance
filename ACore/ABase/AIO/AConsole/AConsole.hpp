#ifndef HPP_ACONSOLE
#define HPP_ACONSOLE
#include <stdio.h>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include "AConsoleIBuffer/AConsoleIBuffer.hpp"
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "AConsole.exception.hpp"

enum class AConsoleStyleText {
    normal    = 0,
    bold      = 1, 
    underline = 4, 
    blink     = 5, 
    invert    = 7,
    invis     = 8
};

enum class AConsoleColor {
    black  = 30,
    red    = 31,
    green  = 32,
    yellow = 33,
    blue   = 34,
    purple = 35,
    azure  = 36,
    white  = 37
};

enum class AConsoleColorBG { 
    black  = 40,
    red    = 41,
    green  = 42,
    yellow = 43,
    blue   = 44,
    purple = 45,
    azure  = 46,
    white  = 47,
    none   = -1
};

class AConsoleStyle {
public:
    AConsoleStyle();
    AConsoleStyle(const char *, const char *, const char *);
    AConsoleStyle(AString, AString, AString);
    AConsoleStyleText style;
    AConsoleColor     color;
    AConsoleColorBG   color_bg;
    AString get_style();
    AString get_color();
    AString get_color_bg();
    static AConsoleStyle get_normal ();
    static AConsoleStyle get_success();
    static AConsoleStyle get_warning();
    static AConsoleStyle get_error  ();
    static AConsoleStyleText to_style   (AString);
    static AConsoleColor     to_color   (AString);
    static AConsoleColorBG   to_color_bg(AString);
};

class AConsole {
public:
    void write(const char *);
    void write(AString);
    void write(AString, AConsoleStyle);
    void write_stream_error(AString, AConsoleStyle);
    void write_warning(AString);
    void write_error  (AString);
    void writeln(const char *);
    void writeln(AString);
    void writeln(AString, AConsoleStyle);
    void writeln_stream_error(AString, AConsoleStyle);
    void writeln_warning(AString);
    void writeln_error  (AString);
    
    void write(char);
    void writeln(char);
    void write_warning(char);
    void writeln_warning(char);
    void write_error(char);
    void writeln_error(char);
    
    void write(s16int);
    void writeln(s16int);
    void write_warning(s16int);
    void writeln_warning(s16int);
    void write_error(s16int);
    void writeln_error(s16int);
    
    void write(s32int);
    void writeln(s32int);
    void write_warning(s32int);
    void writeln_warning(s32int);
    void write_error(s32int);
    void writeln_error(s32int);

    void write(s64int);
    void writeln(s64int);
    void write_warning(s64int);
    void writeln_warning(s64int);
    void write_error(s64int);
    void writeln_error(s64int);
    
    void write(float);
    void writeln(float);
    void write_warning(float);
    void writeln_warning(float);
    void write_error(float);
    void writeln_error(float);
    
    void write(double);
    void writeln(double);
    void write_warning(double);
    void writeln_warning(double);
    void write_error(double);
    void writeln_error(double);

    void write(u8int);
    void writeln(u8int);
    void write_warning(u8int);
    void writeln_warning(u8int);
    void write_error(u8int);
    void writeln_error(u8int);
    
    void write(u16int);
    void writeln(u16int);
    void write_warning(u16int);
    void writeln_warning(u16int);
    void write_error(u16int);
    void writeln_error(u16int);
    
    void write(u32int);
    void writeln(u32int);
    void write_warning(u32int);
    void writeln_warning(u32int);
    void write_error(u32int);
    void writeln_error(u32int);

    void write(u64int);
    void writeln(u64int);
    void write_warning(u64int);
    void writeln_warning(u64int);
    void write_error(u64int);
    void writeln_error(u64int);

    void write(bool);
    void writeln(bool);
    void write_warning(bool);
    void writeln_warning(bool);
    void write_error(bool);
    void writeln_error(bool);

    AString get_word();
    AString get_line();
    char    get_char();
    s32int  get_int();
    s64int  get_long();
    float   get_float();
    double  get_double();
};

#endif