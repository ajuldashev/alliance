#include "AConsole.hpp"

#ifdef TEST_ACONSOLE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aconsole(bool is_soft) {
	#ifdef TEST_ACONSOLEIBUFFER
	if (!test_aconsoleibuffer(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACONSOLEIBUFFER );
		return is_soft;
	}
	#endif
	return true;
}
#endif
