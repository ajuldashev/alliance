#include "AConsoleIBuffer.hpp"
#include "AConsoleIBuffer.test.cpp"
#include "AConsoleIBuffer.test.node.cpp"
#include "AConsoleIBuffer.debug.cpp"
#include "AConsoleIBuffer.debug.node.cpp"
#include "AConsoleIBuffer.exception.cpp"

APtr<AConsoleIBuffer> AConsoleIBuffer::obj;

AConsoleIBuffer::AConsoleIBuffer() {

}

AConsoleIBuffer & AConsoleIBuffer::buff() {
    if (AConsoleIBuffer::obj.is_empty()) {
        APtr<AConsoleIBuffer> ptr(new AConsoleIBuffer);
        AConsoleIBuffer::obj = ptr;
    }
    return *AConsoleIBuffer::obj;
}

void AConsoleIBuffer::init(s32int argc, char ** argv) {
    if (argc > 1) {
        for (s32int i = 1; i < argc; i += 1) {
            buff().push(argv[i]);
        }
    }
}

AString & AConsoleIBuffer::at(u64int pos) {
    return buffer.at(pos);
}

void AConsoleIBuffer::push(AString str) {
    buffer.push_beg(str);
}

AString AConsoleIBuffer::pop() {
    return buffer.pop_end();
}

bool AConsoleIBuffer::is_empty() {
    return buffer.length() <= 0;
}

u64int AConsoleIBuffer::length() {
    return buffer.length();
}

u64int AConsoleIBuffer::size() {
    u64int res = 0;
    for (u64int i = 0; i < buffer.length(); i += 1) {
        res += buffer.at(i).length();
    }
    return res;
}

AConsoleIBuffer::~AConsoleIBuffer() {
    buffer.clear();
}