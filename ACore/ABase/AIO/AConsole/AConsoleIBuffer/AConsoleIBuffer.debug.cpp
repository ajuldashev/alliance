#include "AConsoleIBuffer.hpp"

#ifdef DEBUG_ACONSOLEIBUFFER
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aconsoleibuffer() {
    AConsoleIBuffer & buff = AConsoleIBuffer::buff();
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ACONSOLEIBUFFER );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aconsoleibuffer());
            AIO::writeln("1) help");
            AIO::writeln("2) at");
            AIO::writeln("3) push");
            AIO::writeln("4) pop");
            AIO::writeln("5) is_empty");
            AIO::writeln("6) length");
            AIO::writeln("7) size");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aconsoleibuffer(com)) {
			return true;
		}
        if (com == "at") {
            AString str(buff.at(AIO::get_s32int()));
            AIO::writeln(str);
        }
        if (com == "push") {
            buff.push(AIO::get_word());
        }
        if (com == "pop") {
            AIO::writeln(buff.pop());
        }
        if (com == "is_empty") {
            AIO::writeln(buff.is_empty());
        }
        if (com == "length") {
            AIO::writeln(buff.length());
        }
        if (com == "size") {
            AIO::writeln(buff.size());
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ACONSOLEIBUFFER );
    AIO::write_div_line();
    return false;
}
#endif

