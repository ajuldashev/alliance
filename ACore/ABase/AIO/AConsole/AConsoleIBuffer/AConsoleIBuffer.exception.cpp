#include "AConsoleIBuffer.exception.hpp"

AConsoleIBufferException::AConsoleIBufferException() : AllianceException() {
	exception = "Alliance-Exception-AConsoleIBuffer";
}

AConsoleIBufferException::AConsoleIBufferException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AConsoleIBuffer");
}

AConsoleIBufferException::AConsoleIBufferException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AConsoleIBuffer");
}

AConsoleIBufferException::AConsoleIBufferException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AConsoleIBuffer");
}

AConsoleIBufferException::AConsoleIBufferException(const AConsoleIBufferException & obj) : AllianceException(obj) {
	
}

AConsoleIBufferException::~AConsoleIBufferException() {

}

