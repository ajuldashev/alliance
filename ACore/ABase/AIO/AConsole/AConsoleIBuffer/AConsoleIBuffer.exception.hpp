#ifndef HPP_ACONSOLEIBUFFEREXCEPTION
#define HPP_ACONSOLEIBUFFEREXCEPTION
#include <alliance.exception.hpp>

class AConsoleIBufferException : public AllianceException {
public:
	AConsoleIBufferException();
	AConsoleIBufferException(std::string text);
	AConsoleIBufferException(AllianceException & e);
	AConsoleIBufferException(AllianceException & e, std::string text);
	AConsoleIBufferException(const AConsoleIBufferException & obj);
	~AConsoleIBufferException();
};

#endif

