#ifndef HPP_ACONSOLEIBUFFER
#define HPP_ACONSOLEIBUFFER

#include <alliance.config.hpp>
#include <stdio.h>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AStruct/AArray/AArray.hpp>
#include "AConsoleIBuffer.exception.hpp"

class AConsoleIBuffer {
private:
    AArray<AString> buffer;
    static APtr<AConsoleIBuffer> obj;
    AConsoleIBuffer();
public:
    static AConsoleIBuffer & buff();
    static void init(s32int argc, char ** argv);
    AString & at(u64int);
    void push(AString);
    AString pop();
    bool is_empty();
    u64int length();
    u64int size();
    ~AConsoleIBuffer();
};

#endif