#include "ADir.hpp"
#include "ADir.test.cpp"
#include "ADir.test.node.cpp"
#include "ADir.debug.cpp"
#include "ADir.debug.node.cpp"
#include "ADir.exception.cpp"


ADir::ADir() {
    dir = NULL;
}

ADir::ADir(AString name) {
    dir = NULL;
    info.full_name(name);
}

AString ADir::get_name() {
    return info.get_name();
}

AString ADir::get_path() {
    return info.get_path();
}

AString ADir::full_name() {
    return info.full_name();
}

void ADir::get_name(AString name) {
    info.set_name(name);
}

void ADir::get_path(AString path) {
    info.set_path(path);
}

void ADir::full_name(AString name) {
    info.full_name(name);
}

void ADir::set_name(AString name) {
    info.full_name(name);
}

void ADir::open() {
    list.clear();
    if (AFileInfo::is_exist(info.full_name())) {
        dir = opendir(info.full_name().to_cstring().const_data());
        if (!dir) {
            return;
        }
        struct dirent * pdir = readdir(dir);
        while (pdir) {
            AString entry_name = pdir->d_name;
            if (
                entry_name == "."  ||
                entry_name == ".."
            ) {
                pdir = readdir(dir);
                continue;
            }
            AFileInfo file(info.full_name() + "/" + entry_name);
            file.open();
            list.push_end(file.get_file());
            pdir = readdir(dir);      
        }
        closedir(dir);
    } else {
        AString msg = "open() : Error open dir ";
        msg = msg + info.full_name();
        throw ADirException(msg.to_cstring().const_data());
    }
}

AList<AFile> & ADir::dirs() {
    return list;
}

void ADir::sort() {
    list.sort();
}

bool ADir::make(AString dir) {
    return mkdir(dir.to_cstring().const_data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0;
}

ADir::~ADir() {
	
}