#include "ADir.hpp"

#ifdef DEBUG_ADIR
#include <ACore/ABase/AIO/AIO.hpp>

void debug_print(ADir & dir) {
    AIO::writeln();
    AIO::format(".%107[_]cc.\n",'_');
    for (s32int i = 0; i < dir.dirs().length(); i += 1) {
        AFile file = dir.dirs()[i];
        AString type = "Unknown";
        if (file.is_fifo()) {
            type = "FIFO";
        } else
        if (file.is_reg()) {
            type = "FILE";
        } else
        if (file.is_dir()) {
            type = "DIR";
        } else
        if (file.is_chr()) {
            type = "CHR";
        } else
        if (file.is_blk()) {
            type = "BLK";
        } else
        if (file.is_lnk()) {
            type = "LNK";
        } else
        if (file.is_sock()) {
            type = "SOCK";
        }
        AIO::format("|%5[_]ss|%70[_]ss|%30[_]ss|\n", type, file.full_name(), file.get_name());
        
    }
    AIO::format("|%107[#]cc|\n",'#');
}

bool debug_adir() {
    ADir dir;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ADIR );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_adir());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) full_name");
            AIO::writeln(" 3) open");
            AIO::writeln(" 4) sort");
            AIO::writeln(" 5) make");
            AIO::writeln(" 6) status");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "exit") {  
            return true;
        }
        if (debug_node_adir(com)) {
			return true;
		}
        if (com == "full_name") {
            dir.full_name(AIO::get_word());
        }
        if (com == "open") {
            dir.open();
        }
        if (com == "sort") {
            dir.sort();
        }
        if (com == "status") {
            debug_print(dir);
        }
        if (com == "make") {
            AIO::writeln(ADir::make(AIO::get_word().to_cstring().const_data()));
        }
        AIO::write_div_line();
        com = AIO::get_command();
   }
   AIO::writeln("End debug the module " MODULE_DEBUG_ADIR );
   AIO::write_div_line();
   return false;
}
#endif

