#include "ADir.exception.hpp"

ADirException::ADirException() : AllianceException() {
	exception = "Alliance-Exception-ADir";
}

ADirException::ADirException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ADir");
}

ADirException::ADirException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ADir");
}

ADirException::ADirException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ADir");
}

ADirException::ADirException(const ADirException & obj) : AllianceException(obj) {
	
}

ADirException::~ADirException() {

}

