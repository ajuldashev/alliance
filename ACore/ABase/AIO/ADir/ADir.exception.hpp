#ifndef HPP_ADIREXCEPTION
#define HPP_ADIREXCEPTION
#include <alliance.exception.hpp>

class ADirException : public AllianceException {
public:
	ADirException();
	ADirException(std::string text);
	ADirException(AllianceException & e);
	ADirException(AllianceException & e, std::string text);
	ADirException(const ADirException & obj);
	~ADirException();
};

#endif

