#ifndef HPP_ADIR
#define HPP_ADIR
#include <alliance.config.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <ACore/ABase/AIO/AFileInfo/AFileInfo.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include "ADir.exception.hpp"

class ADir {
private:
    AFile info;
    AList<AFile> list;
    DIR * dir;
public:
    ADir();
    ADir(AString);

    AString get_name();
    AString get_path();
    AString full_name();

    void get_name(AString);
    void get_path(AString);
    void full_name(AString);

    void set_name(AString);
    void open();

    AList<AFile> & dirs();
    
    void sort();

    static bool make(AString dir);
    
    ~ADir();
};

#endif