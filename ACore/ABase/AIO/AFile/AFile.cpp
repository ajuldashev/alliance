#include "AFile.hpp"
#include "AFile.test.cpp"
#include "AFile.test.node.cpp"
#include "AFile.debug.cpp"
#include "AFile.debug.node.cpp"
#include "AFile.exception.cpp"

AFile::AFile() {
	name = "";
	path = "";
	type = Type::UNKNOWN;
	size = 0;
}

AString AFile::get_name() {
	return name;
}

AString AFile::get_path() {
	return path;
}

AFile::Type AFile::get_type() {
	return type;
}

u64int AFile::get_size() {
	return size;
}

AString AFile::full_name() {
	AString res;
	if (!path.is_empty()) {
		res = path;
	}
	res = res + name;
	return res;
}

void AFile::set_name(AString name) {
	this->name = name;
}

void AFile::set_path(AString path) {
	this->path = path;
}

void AFile::set_type(Type type) {
	this->type = type;
}

void AFile::set_size(s64int size) {
	this->size = size;
} 

void AFile::full_name(AString string) {
	if (string.is_empty()) {
		return;
	}
	s64int pos = string.length() - 1;
	while (pos >= 0) {
		if (string[pos] == '/') {
			break;
		}
		pos -= 1;
	}
	if (pos == -1) {
		name = string;
		path = "";
		return;
	}
	path = string.get(0, pos + 1);
	if (string.length() - (pos + 1) > 0) {
		name = string.get((pos + 1), string.length() - (pos + 1));
	} else {
		name = "";
	}
} 

bool AFile::is_exist() {
	struct stat info;
    return lstat(full_name().to_cstring().const_data(), &info) == 0;
}

bool AFile::is_file() {
    return get_type() == AFile::Type::REG;
}

bool AFile::is_fifo() {
    return get_type() == AFile::Type::FIFO;
}

bool AFile::is_chr() {
    return get_type() == AFile::Type::CHR;
}

bool AFile::is_dir() {
    return get_type() == AFile::Type::DIR;
}

bool AFile::is_blk() {
    return get_type() == AFile::Type::DIR;
}

bool AFile::is_reg() {
    return get_type() == AFile::Type::REG;
}

bool AFile::is_lnk() {
    return get_type() == AFile::Type::LNK;
}

bool AFile::is_sock() {
    return get_type() == AFile::Type::SOCK;
}

bool AFile::operator < (AFile file) {
	return full_name() < file.full_name();
}

bool AFile::operator > (AFile file) {
	return full_name() > file.full_name();
}

bool AFile::operator == (AFile file) {
	return full_name() == file.full_name();
}

bool AFile::operator != (AFile file) {
	return full_name() != file.full_name();
}

bool AFile::operator <= (AFile file) {
	return full_name() <= file.full_name();
}

bool AFile::operator >= (AFile file) {
	return full_name() >= file.full_name();
}

AFile::~AFile() {

}