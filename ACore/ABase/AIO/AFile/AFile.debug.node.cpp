#include "AFile.hpp"

#ifdef DEBUG_AFILE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_afile(AString com) {
	#ifdef DEBUG_AIFILE
	if (com == "aifile") {
		if (debug_aifile()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AOFILE
	if (com == "aofile") {
		if (debug_aofile()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_afile() {
	AString text;
	#ifdef DEBUG_AIFILE
	text += "d) aifile\n";
	#endif
	#ifdef DEBUG_AOFILE
	text += "d) aofile\n";
	#endif
	return text;
}

#endif
