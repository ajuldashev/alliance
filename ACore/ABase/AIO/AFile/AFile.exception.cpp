#include "AFile.exception.hpp"

AFileException::AFileException() : AllianceException() {
	exception = "Alliance-Exception-AFile";
}

AFileException::AFileException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AFile");
}

AFileException::AFileException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AFile");
}

AFileException::AFileException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AFile");
}

AFileException::AFileException(const AFileException & obj) : AllianceException(obj) {
	
}

AFileException::~AFileException() {

}

