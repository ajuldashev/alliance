#ifndef HPP_AFILEEXCEPTION
#define HPP_AFILEEXCEPTION
#include <alliance.exception.hpp>

class AFileException : public AllianceException {
public:
	AFileException();
	AFileException(std::string text);
	AFileException(AllianceException & e);
	AFileException(AllianceException & e, std::string text);
	AFileException(const AFileException & obj);
	~AFileException();
};

#endif

