#ifndef HPP_AFILE
#define HPP_AFILE
#include <alliance.config.hpp>
#include <stdio.h>
#include <sys/stat.h>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include "AFile.exception.hpp"

class   AFile {
public:
    enum class Type;
private:
    AString name;
    AString path;
    Type    type;
    s64int  size;
public:
    AFile();
    AString get_name();
    AString get_path();
    Type    get_type();
    u64int  get_size();
    AString full_name();
    void set_name(AString);
    void set_path(AString);
    void set_type(Type);
    void set_size(s64int);
    void full_name(AString);

    bool is_exist();
    bool is_file();
    bool is_fifo();
    bool is_chr();
    bool is_dir();
    bool is_blk();
    bool is_reg();
    bool is_lnk();
    bool is_sock();

    bool operator <  (AFile);
    bool operator >  (AFile);
    bool operator == (AFile);
    bool operator != (AFile);
    bool operator <= (AFile);
    bool operator >= (AFile);

    ~AFile();
};

enum class AFile::Type {
    UNKNOWN = 0,
    FIFO = 1,
    CHR = 2,
    DIR = 4,
    BLK = 6,
    REG = 8,
    LNK = 10,
    SOCK = 12
};

#endif