#include "AFile.hpp"

#ifdef TEST_AFILE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
        AFile file;
        file.full_name("ACore");
        if (!(
            file.get_name()  == "ACore" &&
            file.get_path()  == "" &&
            file.full_name() == "ACore" 
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
        AFile file;
        file.full_name("ACore/ABase");
        if (!(
            file.get_name()  == "ABase" &&
            file.get_path()  == "ACore/" &&
            file.full_name() == "ACore/ABase"
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
        AFile file;
        file.full_name("/ABase");
        if (!(
            file.get_name()  == "ABase" &&
            file.get_path()  == "/" &&
            file.full_name() == "/ABase"
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
        AFile file;
        file.full_name("/");
        if (!(
            file.get_name()  == "" &&
            file.get_path()  == "/" &&
            file.full_name() == "/"
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
        AFile file;
        file.full_name("ACore/");
        if (!(
            file.get_name()  == "" &&
            file.get_path()  == "ACore/" &&
            file.full_name() == "ACore/"
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
        AFile file;
        file.full_name("ACore/ABase/AStruct/AStruct.hpp");
        if (!(
            file.get_name()  == "AStruct.hpp" &&
            file.get_path()  == "ACore/ABase/AStruct/" &&
            file.full_name() == "ACore/ABase/AStruct/AStruct.hpp"
        )) {
            /** <doc-test-info>
             *
             */
            TEST_ALERT(false, 1)
        }
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END



TEST_FUNCTION_MAIN_BEGIN(afile)

	TEST_CALL(1)
    TEST_CALL(2)
    TEST_CALL(3)
    TEST_CALL(4)
    TEST_CALL(5)
    TEST_CALL(6)

TEST_FUNCTION_MAIN_END(AFILE)

#endif