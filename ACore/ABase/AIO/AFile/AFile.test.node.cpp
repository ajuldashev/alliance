#include "AFile.hpp"

#ifdef TEST_AFILE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_afile(bool is_soft) {
	#ifdef TEST_AIFILE
	if (!test_aifile(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AIFILE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AOFILE
	if (!test_aofile(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AOFILE );
		return is_soft;
	}
	#endif
	return true;
}
#endif
