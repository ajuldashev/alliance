#include "AIFile.hpp"
#include "AIFile.test.cpp"
#include "AIFile.test.node.cpp"
#include "AIFile.debug.cpp"
#include "AIFile.debug.node.cpp"
#include "AIFile.exception.cpp"


AIFile::AIFile() {
    name = "";
}

AIFile::AIFile(AString n) {
    set_name(n);
    open();
}

void AIFile::open() {
    if (file) {
        file.close();
    }
    file.open(name.to_cstring().const_data());
}

void AIFile::open(AString n) {
    set_name(n);
    open();
}

void AIFile::set_name(AString n) {
    name = AString(n.to_cstring().const_data());
}

AString AIFile::get_line() {
    AString ares;
    if (file) {
        std::string res;
        std::getline(file, res);
        ares.copy(res.c_str());
    }
    return ares;
}

AString AIFile::get_text() {
    AString ares;
    if (file) {
        while (!is_eof()) {
            ares = ares + get_line() + '\n';
        }
    }
    return ares;
}
bool AIFile::is_file() {
    return (file)? true : false;
}

bool AIFile::is_eof() {
    return file.eof();
}

AString AIFile::get_name() {
    return name;
}

void AIFile::close() {
    file.close();
}

AIFile::~AIFile() {
    if (file) {
        file.close();
    }
}