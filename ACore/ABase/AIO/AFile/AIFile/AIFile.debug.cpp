#include "AIFile.hpp"

#ifdef DEBUG_AIFILE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aifile() {
    AIFile file;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AIFILE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aifile());
            AIO::writeln("1) help");
            AIO::writeln("2) set_name");
            AIO::writeln("3) get_name");
            AIO::writeln("4) open");
            AIO::writeln("5) get_line");
            AIO::writeln("6) get_text");
            AIO::writeln("7) is_file");
            AIO::writeln("8) is_eof");
            AIO::writeln("9) close");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aifile(com)) {
			return true;
		}
        if  (com == "set_name") {
            AString name = AIO::get_word();
            file.set_name(name);
        }
        if  (com == "get_name") {
            AIO::writeln(file.get_name());
        }
        if  (com == "open") {
            file.open();
        }
        if (com == "get_line") {
            AIO::writeln(file.get_line());
        }
        if (com == "get_text") {
            AIO::writeln(file.get_text());
        }
        if (com == "is_file") {
            AIO::write("Is file : ");
            AIO::writeln(file.is_file());
        }
        if (com == "is_eof") {
            AIO::write("Is eof : ");
            AIO::writeln(file.is_eof());
        }
        if (com == "close") {
            file.close();
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AIFILE );
    AIO::write_div_line();
    return false;
}
#endif

