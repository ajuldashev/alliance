#include "AIFile.exception.hpp"

AIFileException::AIFileException() : AllianceException() {
	exception = "Alliance-Exception-AIFile";
}

AIFileException::AIFileException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AIFile");
}

AIFileException::AIFileException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AIFile");
}

AIFileException::AIFileException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AIFile");
}

AIFileException::AIFileException(const AIFileException & obj) : AllianceException(obj) {
	
}

AIFileException::~AIFileException() {

}

