#ifndef HPP_AIFILEEXCEPTION
#define HPP_AIFILEEXCEPTION
#include <alliance.exception.hpp>

class AIFileException : public AllianceException {
public:
	AIFileException();
	AIFileException(std::string text);
	AIFileException(AllianceException & e);
	AIFileException(AllianceException & e, std::string text);
	AIFileException(const AIFileException & obj);
	~AIFileException();
};

#endif

