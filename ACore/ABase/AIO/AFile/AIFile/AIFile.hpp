#ifndef HPP_AIFILE
#define HPP_AIFILE
#include <alliance.config.hpp>
#include <fstream>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AIFile.exception.hpp"


class AIFile {
private:
    AString name;
    std::ifstream file;
public:
    AIFile();
    AIFile(AString);
    void open();
    void open(AString);
    void set_name(AString);
    AString get_line();
    AString get_text();
    bool is_file();
    bool is_eof();
    void close();
    AString get_name();
    ~AIFile();
};

#endif