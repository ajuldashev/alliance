#include "AOFile.hpp"
#include <ACore/ABase/AIO/AIO.hpp>
#include "AOFile.test.cpp"
#include "AOFile.test.node.cpp"
#include "AOFile.debug.cpp"
#include "AOFile.debug.node.cpp"
#include "AOFile.exception.cpp"

AOFile::AOFile() {

}

AOFile::AOFile(AString name) {
    this->name = name;
}

void AOFile::open() {
    close();
    file.open(name.to_cstring().const_data());
}

void AOFile::open(AString name) {
    set_name(name);
    open();
}

void AOFile::set_name(AString name) {
    this->name = name;
}

void AOFile::close() {
    file.close();
}

AString AOFile::get_name() {
    return name;
}

bool AOFile::is_open() {
    return file.is_open();
}

void AOFile::write(AString text) {
    file << text.to_cstring().const_data();
}

AOFile::~AOFile() {

}
