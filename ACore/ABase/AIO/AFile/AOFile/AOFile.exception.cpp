#include "AOFile.exception.hpp"

AOFileException::AOFileException() : AllianceException() {
	exception = "Alliance-Exception-AOFile";
}

AOFileException::AOFileException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AOFile");
}

AOFileException::AOFileException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AOFile");
}

AOFileException::AOFileException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AOFile");
}

AOFileException::AOFileException(const AOFileException & obj) : AllianceException(obj) {
	
}

AOFileException::~AOFileException() {

}

