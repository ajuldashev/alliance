#ifndef HPP_AOFILEEXCEPTION
#define HPP_AOFILEEXCEPTION
#include <alliance.exception.hpp>

class AOFileException : public AllianceException {
public:
	AOFileException();
	AOFileException(std::string text);
	AOFileException(AllianceException & e);
	AOFileException(AllianceException & e, std::string text);
	AOFileException(const AOFileException & obj);
	~AOFileException();
};

#endif

