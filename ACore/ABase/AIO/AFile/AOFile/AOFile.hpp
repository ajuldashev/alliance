#ifndef HPP_AOFILE
#define HPP_AOFILE
#include <alliance.config.hpp>
#include <fstream>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "AOFile.exception.hpp"

class AOFile {
private:
    AString name;
    std::ofstream file;
public:
    AOFile();
    AOFile(AString name);
    void open();
    void open(AString name);
    void set_name(AString name);
    void close();
    AString get_name();
    bool is_open();
    void write(AString text);
    ~AOFile();
};

#endif