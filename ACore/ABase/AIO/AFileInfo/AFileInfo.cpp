#include "AFileInfo.hpp"
#include "AFileInfo.test.cpp"
#include "AFileInfo.test.node.cpp"
#include "AFileInfo.debug.cpp"
#include "AFileInfo.debug.node.cpp"
#include "AFileInfo.exception.cpp"

AFileInfo::AFileInfo() {
    file.set_name("");
    file.set_type(AFile::Type::UNKNOWN);
}

AFileInfo::AFileInfo(AString name) {
    file.full_name(name);
    file.set_type(AFile::Type::UNKNOWN);
}

void AFileInfo::set_name(AString name) {
    file.full_name(name);
}

bool AFileInfo::open() {
    lstat(file.full_name().to_cstring().const_data(), &info);
    file.set_size(info.st_size);
    if (S_ISFIFO(info.st_mode)) {
        file.set_type(AFile::Type::FIFO);
    }
    else if (S_ISCHR(info.st_mode)) {
        file.set_type(AFile::Type::CHR);
    }
    else if (S_ISDIR(info.st_mode)) {
        file.set_type(AFile::Type::DIR);
    }
    else if (S_ISBLK(info.st_mode)) {
        file.set_type(AFile::Type::BLK);
    }
    else if (S_ISREG(info.st_mode)) {
        file.set_type(AFile::Type::REG);
    }
    else if (S_ISLNK(info.st_mode)) {
        file.set_type(AFile::Type::LNK);
    }
    else if (S_ISSOCK(info.st_mode)) {
        file.set_type(AFile::Type::SOCK);
    } else {
        file.set_type(AFile::Type::UNKNOWN);
        return false;
    }
    return true;
}

bool AFileInfo::open(AString name) {
    file.set_name(name);
    return open();
}

bool AFileInfo::is_exist() { 
    return lstat(file.full_name().to_cstring().const_data(), &info) == 0;
}

bool AFileInfo::is_file() {
    return file.get_type() == AFile::Type::REG;
}

bool AFileInfo::is_fifo() {
    return file.get_type() == AFile::Type::FIFO;
}

bool AFileInfo::is_chr() {
    return file.get_type() == AFile::Type::CHR;
}

bool AFileInfo::is_dir() {
    return file.get_type() == AFile::Type::DIR;
}

bool AFileInfo::is_blk() {
    return file.get_type() == AFile::Type::DIR;
}

bool AFileInfo::is_reg() {
    return file.get_type() == AFile::Type::REG;
}

bool AFileInfo::is_lnk() {
    return file.get_type() == AFile::Type::LNK;
}

bool AFileInfo::is_sock() {
    return file.get_type() == AFile::Type::SOCK;
}

AFile::Type AFileInfo::get_type() {
    return file.get_type();
}

u64int AFileInfo::get_size() {
    return file.get_size();
}

bool AFileInfo::is_exist(AString path) {
    AFileInfo info(path);
    return info.is_exist();
}

bool AFileInfo::is_file(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_file();
}

bool AFileInfo::is_fifo(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_fifo();
}

bool AFileInfo::is_chr(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_chr();
}

bool AFileInfo::is_dir(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_dir();
}

bool AFileInfo::is_blk(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_blk();
}

bool AFileInfo::is_reg(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_reg();
}

bool AFileInfo::is_lnk(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_lnk();
}

bool AFileInfo::is_sock(AString path) {
    AFileInfo info(path);
    info.open();
    return info.is_sock();
}

AFile::Type AFileInfo::get_type(AString path) {
    AFileInfo info(path);
    info.open();
    return info.get_type();
}

u64int AFileInfo::get_size(AString path) {
    AFileInfo info(path);
    info.open();
    return info.get_size();
} 

AFile AFileInfo::get_file() {
    return file;
}

AFileInfo::~AFileInfo() {

}