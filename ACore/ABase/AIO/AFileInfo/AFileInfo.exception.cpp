#include "AFileInfo.exception.hpp"

AFileInfoException::AFileInfoException() : AllianceException() {
	exception = "Alliance-Exception-AFileInfo";
}

AFileInfoException::AFileInfoException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AFileInfo");
}

AFileInfoException::AFileInfoException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AFileInfo");
}

AFileInfoException::AFileInfoException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AFileInfo");
}

AFileInfoException::AFileInfoException(const AFileInfoException & obj) : AllianceException(obj) {
	
}

AFileInfoException::~AFileInfoException() {

}

