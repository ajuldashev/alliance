#ifndef HPP_AFILEINFOEXCEPTION
#define HPP_AFILEINFOEXCEPTION
#include <alliance.exception.hpp>

class AFileInfoException : public AllianceException {
public:
	AFileInfoException();
	AFileInfoException(std::string text);
	AFileInfoException(AllianceException & e);
	AFileInfoException(AllianceException & e, std::string text);
	AFileInfoException(const AFileInfoException & obj);
	~AFileInfoException();
};

#endif

