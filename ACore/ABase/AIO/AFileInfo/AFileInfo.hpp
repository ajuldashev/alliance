#ifndef HPP_AFILEINFO
#define HPP_AFILEINFO
#include <alliance.config.hpp>
#include <stdio.h>
#include <sys/stat.h>
#include <ACore/ABase/AIO/AFile/AFile.hpp>
#include "AFileInfo.exception.hpp"

class AFileInfo {
private:
    AFile file;
    struct stat info;
public:
    AFileInfo();
    AFileInfo(AString name);
    
    void set_name(AString name);

    bool open();
    bool open(AString name);

    bool is_exist();
    bool is_file();
    bool is_fifo();
    bool is_chr();
    bool is_dir();
    bool is_blk();
    bool is_reg();
    bool is_lnk();
    bool is_sock();

    AFile::Type get_type();
    u64int get_size();

    static bool is_exist(AString);
    static bool is_file(AString);
    static bool is_fifo(AString);
    static bool is_chr(AString);
    static bool is_dir(AString);
    static bool is_blk(AString);
    static bool is_reg(AString);
    static bool is_lnk(AString);
    static bool is_sock(AString);

    static AFile::Type get_type(AString);
    static u64int get_size(AString);

    AFile get_file();

    ~AFileInfo();
};

#endif