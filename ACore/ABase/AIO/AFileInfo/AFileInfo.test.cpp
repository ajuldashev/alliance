#include "AFileInfo.hpp"

#ifdef TEST_AFILEINFO
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(AFileInfo::is_dir("ACore"), 1)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(AFileInfo::is_file("alliance.hpp"), 2)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(AFileInfo::is_file("ACore/ACore.hpp"), 3)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(!AFileInfo::is_exist("ACore/ACore.h"), 4)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(afileinfo)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AFILEINFO)

#endif

