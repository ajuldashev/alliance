#include "AIO.hpp"
#include "AIO.test.cpp"
#include "AIO.test.node.cpp"
#include "AIO.debug.cpp"
#include "AIO.debug.node.cpp"
#include "AIO.exception.cpp"

AConsole AIO::console;

AString AIO::get_word() {
	return console.get_word();
}

AString AIO::get_line() {
	return console.get_line();
}

AString AIO::get_command() {
	AConsoleIBuffer & buff = AConsoleIBuffer::buff();
	if (!buff.is_empty()) {
		AString str = buff.pop();
		return str;
	}
	write_cursor();
	return console.get_word();
}

bool AIO::get_bool() {
	return console.get_word() == "true" || console.get_word() == "TRUE";
}

char AIO::get_char() {
	return console.get_word()[0];
}

s8int AIO::get_s8int() {
	return console.get_int();
}

u8int AIO::get_u8int() {
	return console.get_int();
}

s16int AIO::get_s16int() {
	return console.get_int();
}

u16int AIO::get_u16int() {
	return console.get_int();
}

s32int AIO::get_s32int() {
	return console.get_int();
}

u32int AIO::get_u32int() {
	return console.get_int();
}

s64int AIO::get_s64int() {
	return console.get_long();
}

u64int AIO::get_u64int() {
	return console.get_long();
}

float AIO::get_float() {
	return console.get_float();
}

double AIO::get_double() {
	return console.get_double();
}

void AIO::write() {

}

void AIO::writeln() {
	console.write("\n");
}

void AIO::write(bool f) {
	console.write(f);
}

void AIO::writeln(bool f) {
	console.writeln(f);
}

void AIO::write(s8int num) {
	console.write(num);
}

void AIO::writeln(s8int num) {
	console.writeln(num);
}

void AIO::write(u8int num) {
	console.write(num);
}

void AIO::writeln(u8int num) {
	console.writeln(num);
}

void AIO::write(s16int num) {
	console.write(num);
}

void AIO::writeln(s16int num) {
	console.writeln(num);
}

void AIO::write(u16int num) {
	console.write(num);
}

void AIO::writeln(u16int num) {
	console.writeln(num);
}

void AIO::write(s32int num) {
	console.write(num);
}

void AIO::writeln(s32int num) {
	console.writeln(num);
}

void AIO::write(u32int num) {
	console.write(num);
}

void AIO::writeln(u32int num) {
	console.writeln(num);
}

void AIO::write(s64int num) {
	console.write(num);
}

void AIO::writeln(s64int num) {
	console.writeln(num);
}

void AIO::write(u64int num) {
	console.write(num);
}

void AIO::writeln(u64int num) {
	console.writeln(num);
}

void AIO::write(float num) {
	console.write(num);
}

void AIO::writeln(float num) {
	console.writeln(num);
}

void AIO::write(double num) {
	console.write(num);
}

void AIO::writeln(double num) {
	console.writeln(num);
}

void AIO::write(const char * str) {
	std::cout << ((str == NULL) ? "\0" : str);
}

void AIO::writeln(const char * str) {
	std::cout << ((str == NULL) ? "\0" : str) << std::endl;
}

void AIO::write(AString str) {
	std::cout << ((str.to_cstring().const_data() == NULL) ? "\0" : str.to_cstring().const_data());
}

void AIO::writeln(AString str) {
	std::cout << ((str.to_cstring().const_data() == NULL) ? "\0" : str.to_cstring().const_data()) << std::endl;
}

void AIO::write_success(const char * cstr) {
	AString str(cstr);
	console.write(str, AConsoleStyle::get_success());
}

void AIO::write_warning(const char * cstr) {
	AString str(cstr);
	console.write(str, AConsoleStyle::get_warning());
}

void AIO::write_error(const char * cstr) {
	AString str(cstr);
	console.write(str, AConsoleStyle::get_error());
}

void AIO::write_success(AString str) {
	console.write(str, AConsoleStyle::get_success());
}

void AIO::write_warning(AString str) {
	console.write_warning(str);
}

void AIO::write_error(AString str) {
	console.write_error(str);
}

void AIO::writeln_success(AString str) {
	console.writeln(str, AConsoleStyle::get_success());
}

void AIO::writeln_warning(AString str) {
	console.writeln_warning(str);
}

void AIO::writeln_error(AString str) {
	console.writeln_error(str);
}

AString AIO::from_file(AString file_name) {
	AString res;
	std::ifstream file(file_name.to_cstring().const_data());
	std::string str;
	if (file) {
		getline(file, str, '\0');
		res = str.c_str();
	}
	return res;
}

bool AIO::to_file(AString str, AString file_name) {
	std::ofstream file(file_name.to_cstring().const_data(), std::ofstream::out | std::ofstream::trunc);
	if (!file) {
		return false;
	}
	file << str.to_cstring().const_data();;
	return true;
}

void AIO::format(AString string, ...) {
	std::va_list ap; 									  
    va_start(ap,string);
    string = AStringFormat::format_base(string, ap);
    va_end(ap);
    write(string);
}

void AIO::write_div_line() {
	AIO::writeln("--------------------------------------");
}

void AIO::write_cursor() {
	AIO::write(">>> ");
}