#include "AIO.hpp"

#ifdef DEBUG_AIO
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aio() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AIO );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aio());
            AIO::writeln("1) help");
            AIO::writeln("6) to_file");
            AIO::writeln("7) from_file");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aio(com)) {
			return true;
		}
        if (com == "to_file") {
            AString text = AIO::get_word();
            AString file = AIO::get_word();
            AIO::to_file(text, file);
        }
        if (com == "from_file") {
            AString file = AIO::get_word();
            AIO::writeln(AIO::from_file(file));
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AIO );
    AIO::write_div_line();
    return false;
}
#endif

