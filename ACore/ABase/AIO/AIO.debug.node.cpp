#include "AIO.hpp"

#ifdef DEBUG_AIO
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aio(AString com) {
	#ifdef DEBUG_ACONSOLE
	if (com == "aconsole") {
		if (debug_aconsole()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ADIR
	if (com == "adir") {
		if (debug_adir()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AFILE
	if (com == "afile") {
		if (debug_afile()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AFILEINFO
	if (com == "afileinfo") {
		if (debug_afileinfo()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aio() {
	AString text;
	#ifdef DEBUG_ACONSOLE
	text += "d) aconsole\n";
	#endif
	#ifdef DEBUG_ADIR
	text += "d) adir\n";
	#endif
	#ifdef DEBUG_AFILE
	text += "d) afile\n";
	#endif
	#ifdef DEBUG_AFILEINFO
	text += "d) afileinfo\n";
	#endif
	return text;
}

#endif
