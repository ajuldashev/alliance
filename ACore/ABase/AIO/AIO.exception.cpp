#include "AIO.exception.hpp"

AIOException::AIOException() : AllianceException() {
	exception = "Alliance-Exception-AIO";
}

AIOException::AIOException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AIO");
}

AIOException::AIOException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AIO");
}

AIOException::AIOException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AIO");
}

AIOException::AIOException(const AIOException & obj) : AllianceException(obj) {
	
}

AIOException::~AIOException() {

}

