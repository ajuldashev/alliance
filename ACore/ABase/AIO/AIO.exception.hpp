#ifndef HPP_AIOEXCEPTION
#define HPP_AIOEXCEPTION
#include <alliance.exception.hpp>

class AIOException : public AllianceException {
public:
	AIOException();
	AIOException(std::string text);
	AIOException(AllianceException & e);
	AIOException(AllianceException & e, std::string text);
	AIOException(const AIOException & obj);
	~AIOException();
};

#endif

