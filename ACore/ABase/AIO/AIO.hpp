#ifndef HPP_AIO
#define HPP_AIO
#include <ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.hpp>
#include <ACore/ABase/AIO/AConsole/AConsole.hpp>
#include <ACore/ABase/AIO/AFile/AFile.hpp>
#include <iostream>
#include <fstream>
#include "AIO.exception.hpp"

class AIO {
private :
    static AConsole console;
public :
    static AString get_word();
    static AString get_line();
    static AString get_command();

    static bool   get_bool   ();
    static char   get_char   ();
    static s8int  get_s8int  ();
    static u8int  get_u8int  ();
    static s16int get_s16int ();
    static u16int get_u16int ();
    static s32int get_s32int ();
    static u32int get_u32int ();
    static s64int get_s64int ();
    static u64int get_u64int ();
    static float  get_float  ();
    static double get_double ();

    static void write  ();
    static void writeln();

    static void write  (bool);
    static void writeln(bool);

    static void write  (s8int);
    static void writeln(s8int);
    static void write  (u8int);
    static void writeln(u8int);
    static void write  (s16int);
    static void writeln(s16int);
    static void write  (u16int);
    static void writeln(u16int);
    static void write  (s32int);
    static void writeln(s32int);
    static void write  (u32int);
    static void writeln(u32int);
    static void write  (s64int);
    static void writeln(s64int);
    static void write  (u64int);
    static void writeln(u64int);

    static void write  (float);
    static void writeln(float);
    static void write  (double);
    static void writeln(double);

    static void write  (const char *);
    static void writeln(const char *);
    static void write  (AString);
    static void writeln(AString);

    static void write_success(const char *);
    static void write_warning(const char *);
    static void write_error(const char *);

    static void write_success(AString);
    static void write_warning(AString);
    static void write_error(AString);

    static void writeln_success(AString);
    static void writeln_warning(AString);
    static void writeln_error(AString);

    static AString from_file(AString);
    static bool to_file(AString, AString);

    static void format(AString, ...);

    static void write_div_line();
    static void write_cursor();
};

#endif
