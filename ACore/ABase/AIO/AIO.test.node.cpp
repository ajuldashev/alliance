#include "AIO.hpp"

#ifdef TEST_AIO
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aio(bool is_soft) {
	#ifdef TEST_ACONSOLE
	if (!test_aconsole(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACONSOLE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ADIR
	if (!test_adir(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ADIR );
		return is_soft;
	}
	#endif
	#ifdef TEST_AFILE
	if (!test_afile(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AFILE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AFILEINFO
	if (!test_afileinfo(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AFILEINFO );
		return is_soft;
	}
	#endif
	return true;
}
#endif
