#include "A16BitsIterator.exception.hpp"

A16BitsIteratorException::A16BitsIteratorException() : AllianceException() {
	exception = "Alliance-Exception-A16BitsIterator";
}

A16BitsIteratorException::A16BitsIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A16BitsIterator");
}

A16BitsIteratorException::A16BitsIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A16BitsIterator");
}

A16BitsIteratorException::A16BitsIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A16BitsIterator");
}

A16BitsIteratorException::A16BitsIteratorException(const A16BitsIteratorException & obj) : AllianceException(obj) {
	
}

A16BitsIteratorException::~A16BitsIteratorException() {

}

