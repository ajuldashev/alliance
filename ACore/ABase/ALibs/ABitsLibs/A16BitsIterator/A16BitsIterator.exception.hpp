#ifndef HPP_A16BITSITERATOREXCEPTION
#define HPP_A16BITSITERATOREXCEPTION
#include <alliance.exception.hpp>

class A16BitsIteratorException : public AllianceException {
public:
	A16BitsIteratorException();
	A16BitsIteratorException(std::string text);
	A16BitsIteratorException(AllianceException & e);
	A16BitsIteratorException(AllianceException & e, std::string text);
	A16BitsIteratorException(const A16BitsIteratorException & obj);
	~A16BitsIteratorException();
};

#endif

