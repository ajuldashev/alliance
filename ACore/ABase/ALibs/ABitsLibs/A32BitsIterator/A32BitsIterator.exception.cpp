#include "A32BitsIterator.exception.hpp"

A32BitsIteratorException::A32BitsIteratorException() : AllianceException() {
	exception = "Alliance-Exception-A32BitsIterator";
}

A32BitsIteratorException::A32BitsIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A32BitsIterator");
}

A32BitsIteratorException::A32BitsIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A32BitsIterator");
}

A32BitsIteratorException::A32BitsIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A32BitsIterator");
}

A32BitsIteratorException::A32BitsIteratorException(const A32BitsIteratorException & obj) : AllianceException(obj) {
	
}

A32BitsIteratorException::~A32BitsIteratorException() {

}

