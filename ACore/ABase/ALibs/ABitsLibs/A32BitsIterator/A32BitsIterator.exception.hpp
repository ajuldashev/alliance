#ifndef HPP_A32BITSITERATOREXCEPTION
#define HPP_A32BITSITERATOREXCEPTION
#include <alliance.exception.hpp>

class A32BitsIteratorException : public AllianceException {
public:
	A32BitsIteratorException();
	A32BitsIteratorException(std::string text);
	A32BitsIteratorException(AllianceException & e);
	A32BitsIteratorException(AllianceException & e, std::string text);
	A32BitsIteratorException(const A32BitsIteratorException & obj);
	~A32BitsIteratorException();
};

#endif

