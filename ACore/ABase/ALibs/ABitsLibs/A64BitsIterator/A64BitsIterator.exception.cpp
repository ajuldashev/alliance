#include "A64BitsIterator.exception.hpp"

A64BitsIteratorException::A64BitsIteratorException() : AllianceException() {
	exception = "Alliance-Exception-A64BitsIterator";
}

A64BitsIteratorException::A64BitsIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A64BitsIterator");
}

A64BitsIteratorException::A64BitsIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A64BitsIterator");
}

A64BitsIteratorException::A64BitsIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A64BitsIterator");
}

A64BitsIteratorException::A64BitsIteratorException(const A64BitsIteratorException & obj) : AllianceException(obj) {
	
}

A64BitsIteratorException::~A64BitsIteratorException() {

}

