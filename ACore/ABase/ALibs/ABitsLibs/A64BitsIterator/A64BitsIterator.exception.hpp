#ifndef HPP_A64BITSITERATOREXCEPTION
#define HPP_A64BITSITERATOREXCEPTION
#include <alliance.exception.hpp>

class A64BitsIteratorException : public AllianceException {
public:
	A64BitsIteratorException();
	A64BitsIteratorException(std::string text);
	A64BitsIteratorException(AllianceException & e);
	A64BitsIteratorException(AllianceException & e, std::string text);
	A64BitsIteratorException(const A64BitsIteratorException & obj);
	~A64BitsIteratorException();
};

#endif

