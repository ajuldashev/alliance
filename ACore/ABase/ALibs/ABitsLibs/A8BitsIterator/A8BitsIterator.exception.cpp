#include "A8BitsIterator.exception.hpp"

A8BitsIteratorException::A8BitsIteratorException() : AllianceException() {
	exception = "Alliance-Exception-A8BitsIterator";
}

A8BitsIteratorException::A8BitsIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A8BitsIterator");
}

A8BitsIteratorException::A8BitsIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A8BitsIterator");
}

A8BitsIteratorException::A8BitsIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A8BitsIterator");
}

A8BitsIteratorException::A8BitsIteratorException(const A8BitsIteratorException & obj) : AllianceException(obj) {
	
}

A8BitsIteratorException::~A8BitsIteratorException() {

}

