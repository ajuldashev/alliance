#ifndef HPP_A8BITSITERATOREXCEPTION
#define HPP_A8BITSITERATOREXCEPTION
#include <alliance.exception.hpp>

class A8BitsIteratorException : public AllianceException {
public:
	A8BitsIteratorException();
	A8BitsIteratorException(std::string text);
	A8BitsIteratorException(AllianceException & e);
	A8BitsIteratorException(AllianceException & e, std::string text);
	A8BitsIteratorException(const A8BitsIteratorException & obj);
	~A8BitsIteratorException();
};

#endif

