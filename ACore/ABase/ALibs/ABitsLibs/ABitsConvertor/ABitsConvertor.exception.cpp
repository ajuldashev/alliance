#include "ABitsConvertor.exception.hpp"

ABitsConvertorException::ABitsConvertorException() : AllianceException() {
	exception = "Alliance-Exception-ABitsConvertor";
}

ABitsConvertorException::ABitsConvertorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ABitsConvertor");
}

ABitsConvertorException::ABitsConvertorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ABitsConvertor");
}

ABitsConvertorException::ABitsConvertorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ABitsConvertor");
}

ABitsConvertorException::ABitsConvertorException(const ABitsConvertorException & obj) : AllianceException(obj) {
	
}

ABitsConvertorException::~ABitsConvertorException() {

}

