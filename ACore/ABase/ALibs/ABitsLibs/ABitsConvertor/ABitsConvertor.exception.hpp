#ifndef HPP_ABITSCONVERTOREXCEPTION
#define HPP_ABITSCONVERTOREXCEPTION
#include <alliance.exception.hpp>

class ABitsConvertorException : public AllianceException {
public:
	ABitsConvertorException();
	ABitsConvertorException(std::string text);
	ABitsConvertorException(AllianceException & e);
	ABitsConvertorException(AllianceException & e, std::string text);
	ABitsConvertorException(const ABitsConvertorException & obj);
	~ABitsConvertorException();
};

#endif

