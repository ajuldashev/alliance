#include "ABitsLibs.hpp"

#ifdef DEBUG_ABITSLIBS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_abitslibs(AString com) {
	#ifdef DEBUG_A16BITSITERATOR
	if (com == "a16bitsiterator") {
		if (debug_a16bitsiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A32BITSITERATOR
	if (com == "a32bitsiterator") {
		if (debug_a32bitsiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A64BITSITERATOR
	if (com == "a64bitsiterator") {
		if (debug_a64bitsiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A8BITSITERATOR
	if (com == "a8bitsiterator") {
		if (debug_a8bitsiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ABITSCONVERTOR
	if (com == "abitsconvertor") {
		if (debug_abitsconvertor()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_abitslibs() {
	AString text;
	#ifdef DEBUG_A16BITSITERATOR
	text += "d) a16bitsiterator\n";
	#endif
	#ifdef DEBUG_A32BITSITERATOR
	text += "d) a32bitsiterator\n";
	#endif
	#ifdef DEBUG_A64BITSITERATOR
	text += "d) a64bitsiterator\n";
	#endif
	#ifdef DEBUG_A8BITSITERATOR
	text += "d) a8bitsiterator\n";
	#endif
	#ifdef DEBUG_ABITSCONVERTOR
	text += "d) abitsconvertor\n";
	#endif
	return text;
}

#endif
