#include "ABitsLibs.exception.hpp"

ABitsLibsException::ABitsLibsException() : AllianceException() {
	exception = "Alliance-Exception-ABitsLibs";
}

ABitsLibsException::ABitsLibsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ABitsLibs");
}

ABitsLibsException::ABitsLibsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ABitsLibs");
}

ABitsLibsException::ABitsLibsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ABitsLibs");
}

ABitsLibsException::ABitsLibsException(const ABitsLibsException & obj) : AllianceException(obj) {
	
}

ABitsLibsException::~ABitsLibsException() {

}

