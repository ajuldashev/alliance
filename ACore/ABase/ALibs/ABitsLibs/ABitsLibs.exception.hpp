#ifndef HPP_ABITSLIBSEXCEPTION
#define HPP_ABITSLIBSEXCEPTION
#include <alliance.exception.hpp>

class ABitsLibsException : public AllianceException {
public:
	ABitsLibsException();
	ABitsLibsException(std::string text);
	ABitsLibsException(AllianceException & e);
	ABitsLibsException(AllianceException & e, std::string text);
	ABitsLibsException(const ABitsLibsException & obj);
	~ABitsLibsException();
};

#endif

