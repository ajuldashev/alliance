#include "ABitsLibs.hpp"

#ifdef TEST_ABITSLIBS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_abitslibs(bool is_soft) {
	#ifdef TEST_A16BITSITERATOR
	if (!test_a16bitsiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A16BITSITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_A32BITSITERATOR
	if (!test_a32bitsiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A32BITSITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_A64BITSITERATOR
	if (!test_a64bitsiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A64BITSITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_A8BITSITERATOR
	if (!test_a8bitsiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A8BITSITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_ABITSCONVERTOR
	if (!test_abitsconvertor(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ABITSCONVERTOR );
		return is_soft;
	}
	#endif
	return true;
}
#endif
