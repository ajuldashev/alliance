#include "ALibs.hpp"

#ifdef DEBUG_ALIBS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_alibs(AString com) {
	#ifdef DEBUG_ABITSLIBS
	if (com == "abitslibs") {
		if (debug_abitslibs()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEX
	if (com == "aregex") {
		if (debug_aregex()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRINGLIBS
	if (com == "astringlibs") {
		if (debug_astringlibs()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_alibs() {
	AString text;
	#ifdef DEBUG_ABITSLIBS
	text += "d) abitslibs\n";
	#endif
	#ifdef DEBUG_AREGEX
	text += "d) aregex\n";
	#endif
	#ifdef DEBUG_ASTRINGLIBS
	text += "d) astringlibs\n";
	#endif
	return text;
}

#endif
