#include "ALibs.exception.hpp"

ALibsException::ALibsException() : AllianceException() {
	exception = "Alliance-Exception-ALibs";
}

ALibsException::ALibsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ALibs");
}

ALibsException::ALibsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ALibs");
}

ALibsException::ALibsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ALibs");
}

ALibsException::ALibsException(const ALibsException & obj) : AllianceException(obj) {
	
}

ALibsException::~ALibsException() {

}

