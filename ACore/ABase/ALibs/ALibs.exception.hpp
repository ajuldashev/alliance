#ifndef HPP_ALIBSEXCEPTION
#define HPP_ALIBSEXCEPTION
#include <alliance.exception.hpp>

class ALibsException : public AllianceException {
public:
	ALibsException();
	ALibsException(std::string text);
	ALibsException(AllianceException & e);
	ALibsException(AllianceException & e, std::string text);
	ALibsException(const ALibsException & obj);
	~ALibsException();
};

#endif

