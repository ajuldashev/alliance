#include "ALibs.hpp"

#ifdef TEST_ALIBS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_alibs(bool is_soft) {
	#ifdef TEST_ABITSLIBS
	if (!test_abitslibs(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ABITSLIBS );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEX
	if (!test_aregex(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEX );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRINGLIBS
	if (!test_astringlibs(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGLIBS );
		return is_soft;
	}
	#endif
	return true;
}
#endif
