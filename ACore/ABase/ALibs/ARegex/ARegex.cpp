#include "ARegex.hpp"
#include "ARegex.test.cpp"
#include "ARegex.test.node.cpp"
#include "ARegex.debug.cpp"
#include "ARegex.debug.node.cpp"
#include "ARegex.exception.cpp"

bool ARegex::match(AString pattern, AString string) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.match(string);
    } catch (AllianceException & e) {
        throw ARegexException("match(AString pattern, AString string)");
    }
}

bool ARegex::match(AString pattern, AStringIterator & iterator) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.match(iterator);
    } catch (AllianceException & e) {
        throw ARegexException("match(AString pattern, AStringIterator & iterator)");
    }
}

APtr<AStringList> ARegex::matches(AString pattern, AString string) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.matches(string);
    } catch (AllianceException & e) {
        throw ARegexException("matches(AString pattern, AString string)");
    }
}

APtr<AStringList> ARegex::matches(AString pattern, AStringIterator & iterator) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.matches(iterator);
    } catch (AllianceException & e) {
        throw ARegexException("matches(AString pattern, AStringIterator & iterator)");
    }
}

APtr<AStringList> ARegex::find_first(AString pattern, AString string) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.find_first(string);
    } catch (AllianceException & e) {
        throw ARegexException("find_first(AString pattern, AString string)");
    }
}

APtr<AList<APtr<AStringList>>> ARegex::find_all(AString pattern, AString string) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.find_all(string);
    } catch (AllianceException & e) {
        throw ARegexException("find_all(AString pattern, AString string)");
    }
}

AString ARegex::replace_first(AString pattern, AString string, AString substring) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.replace_first(string, substring);
    } catch (AllianceException & e) {
        throw ARegexException("replace_first(AString pattern, AString string, AString substring)");
    }
}

AString ARegex::replace_all(AString pattern, AString string, AString substring) {
    try {
        ARegexPattern regex_pattern(pattern);
        return regex_pattern.replace_all(string, substring);
    } catch (AllianceException & e) {
        throw ARegexException("replace_all(AString pattern, AString string, AString substring)");
    }
}