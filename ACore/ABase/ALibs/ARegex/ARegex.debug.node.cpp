#include "ARegex.hpp"

#ifdef DEBUG_AREGEX
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregex(AString com) {
	#ifdef DEBUG_AREGEXCONSTRUCTOR
	if (com == "aregexconstructor") {
		if (debug_aregexconstructor()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXPATTERN
	if (com == "aregexpattern") {
		if (debug_aregexpattern()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregex() {
	AString text;
	#ifdef DEBUG_AREGEXCONSTRUCTOR
	text += "d) aregexconstructor\n";
	#endif
	#ifdef DEBUG_AREGEXPATTERN
	text += "d) aregexpattern\n";
	#endif
	return text;
}

#endif
