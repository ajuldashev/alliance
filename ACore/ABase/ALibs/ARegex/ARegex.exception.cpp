#include "ARegex.exception.hpp"

ARegexException::ARegexException() : AllianceException() {
	exception = "Alliance-Exception-ARegex";
}

ARegexException::ARegexException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegex");
}

ARegexException::ARegexException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegex");
}

ARegexException::ARegexException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegex");
}

ARegexException::ARegexException(const ARegexException & obj) : AllianceException(obj) {
	
}

ARegexException::~ARegexException() {

}

