#ifndef HPP_AREGEXEXCEPTION
#define HPP_AREGEXEXCEPTION
#include <alliance.exception.hpp>

class ARegexException : public AllianceException {
public:
	ARegexException();
	ARegexException(std::string text);
	ARegexException(AllianceException & e);
	ARegexException(AllianceException & e, std::string text);
	ARegexException(const ARegexException & obj);
	~ARegexException();
};

#endif

