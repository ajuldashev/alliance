#ifndef HPP_AREGEX
#define HPP_AREGEX

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.hpp>

#include "ARegex.exception.hpp"

class ARegex {
public:
    static bool match(AString pattern, AString string);
    static bool match(AString pattern, AStringIterator & iterator);

    static APtr<AStringList> matches(AString pattern, AString string);
    static APtr<AStringList> matches(AString pattern, AStringIterator & iterator);

    static APtr<AStringList> find_first(AString pattern, AString string);

    static APtr<AList<APtr<AStringList>>> find_all(AString pattern, AString string);

    static AString replace_first(AString pattern, AString string, AString substring);

    static AString replace_all(AString pattern, AString string, AString substring);
};

#endif