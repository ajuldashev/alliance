#include "ARegex.hpp"

#ifdef TEST_AREGEX
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(!ARegex::match("[0-9]+", ""), 1)
      /** <doc-test-info>
       * 
       */
      TEST_ALERT( ARegex::match("[0-9]+", "1234"), 2)
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(!ARegex::match("[0-9]+", "1234ads"), 3)
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregex)

   TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AREGEX)

#endif