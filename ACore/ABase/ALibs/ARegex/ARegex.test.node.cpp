#include "ARegex.hpp"

#ifdef TEST_AREGEX
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregex(bool is_soft) {
	#ifdef TEST_AREGEXCONSTRUCTOR
	if (!test_aregexconstructor(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXCONSTRUCTOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXPATTERN
	if (!test_aregexpattern(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXPATTERN );
		return is_soft;
	}
	#endif
	return true;
}
#endif
