#include "ARegexConstructor.hpp"
#include "ARegexConstructor.test.cpp"
#include "ARegexConstructor.test.node.cpp"
#include "ARegexConstructor.debug.cpp"
#include "ARegexConstructor.debug.node.cpp"
#include "ARegexConstructor.exception.cpp"

ARegexConstructor::ARegexConstructor() {

}

ARegexConstructor ARegexConstructor::make() {
    ARegexConstructor constructor;
    return constructor;
}

ARegexConstructor & ARegexConstructor::group_begin() {
    pattern_string += "(";
    return *this;
}

ARegexConstructor & ARegexConstructor::group_or() {
    pattern_string += "|";
    return *this;
}

ARegexConstructor & ARegexConstructor::group_end() {
    pattern_string += ")";
    return *this;
}

ARegexConstructor & ARegexConstructor::unknown() {
    pattern_string += "?:";
    return *this;
}

ARegexConstructor & ARegexConstructor::advanced_check_positive() {
    pattern_string += "?=";
    return *this;
}

ARegexConstructor & ARegexConstructor::advanced_check_negative() {
    pattern_string += "?!";
    return *this;
}

ARegexConstructor & ARegexConstructor::retrospective_check_positive() {
    pattern_string += "?<=";
    return *this;
}

ARegexConstructor & ARegexConstructor::retrospective_check_negative() {
    pattern_string += "?<!";
    return *this;
}

ARegexConstructor & ARegexConstructor::begin() {
    pattern_string += "^";
    return *this;
}

ARegexConstructor & ARegexConstructor::end() {
    pattern_string += "$";
    return *this;
}

ARegexConstructor & ARegexConstructor::text(AString string) {
    ARegexParserToken token;
    AStringIterator iterator(string);
    while (iterator.can_get()) {
        iterator = token.set(iterator);
        pattern_string += token.to_string();
    }
    return *this;
}

ARegexConstructor & ARegexConstructor::regex(AString string) {
    pattern_string += string;
    return *this;
}

ARegexConstructor & ARegexConstructor::regex_class(AString string) {
    pattern_string += "[";
    pattern_string += string;
    pattern_string += "]";
    return *this;
}

ARegexConstructor & ARegexConstructor::symbol(ASymbol symbol) {
    ARegexParserToken token;
    AStringIterator iterator(symbol.to_string());
    if (iterator.can_get()) {
        iterator = token.set(iterator);
        pattern_string += token.to_string();
    }
    return *this;
}

ARegexConstructor & ARegexConstructor::digital() {
    pattern_string += "\\d";
    return *this;
}

ARegexConstructor & ARegexConstructor::space() {
    pattern_string += "\\s";
    return *this;
}

ARegexConstructor & ARegexConstructor::word() {
    pattern_string += "\\w";
    return *this;
}

ARegexConstructor & ARegexConstructor::not_digital() {
    pattern_string += "\\D";
    return *this;
}

ARegexConstructor & ARegexConstructor::not_space() {
    pattern_string += "\\S";
    return *this;
}

ARegexConstructor & ARegexConstructor::not_word() {
    pattern_string += "\\W";
    return *this;
}

ARegexConstructor & ARegexConstructor::any() {
    pattern_string += ".";
    return *this;
}

ARegexConstructor & ARegexConstructor::count(s64int min, s64int max) {
    pattern_string += "{";
    if (min >= 0) {
        pattern_string += AStringLibs::to_string(min);
    }
    pattern_string += ",";
    if (max >= 0) {
        pattern_string += AStringLibs::to_string(max);
    }
    pattern_string += "}";
    return *this;
}

ARegexConstructor & ARegexConstructor::count(u64int count) {
    pattern_string += "{";
    pattern_string += AStringLibs::to_string(count);
    pattern_string += "}";
    return *this;
}

ARegexConstructor & ARegexConstructor::may_be_present() {
    pattern_string += "?";
    return *this;
}

ARegexConstructor & ARegexConstructor::at_least_one() {
    pattern_string += "+";
    return *this;
}

ARegexConstructor & ARegexConstructor::any_number() {
    pattern_string += "*";
    return *this;
}

ARegexConstructor & ARegexConstructor::refs(u64int index) {
    pattern_string += "\\";
    pattern_string += AStringLibs::to_string(index);
    return *this;
}

AString ARegexConstructor::to_string() {
    return pattern_string;
}

ARegexConstructor::~ARegexConstructor() {

}