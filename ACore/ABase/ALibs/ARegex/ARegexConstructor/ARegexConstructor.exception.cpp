#include "ARegexConstructor.exception.hpp"

ARegexConstructorException::ARegexConstructorException() : AllianceException() {
	exception = "Alliance-Exception-ARegexConstructor";
}

ARegexConstructorException::ARegexConstructorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexConstructor");
}

ARegexConstructorException::ARegexConstructorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexConstructor");
}

ARegexConstructorException::ARegexConstructorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexConstructor");
}

ARegexConstructorException::ARegexConstructorException(const ARegexConstructorException & obj) : AllianceException(obj) {
	
}

ARegexConstructorException::~ARegexConstructorException() {

}

