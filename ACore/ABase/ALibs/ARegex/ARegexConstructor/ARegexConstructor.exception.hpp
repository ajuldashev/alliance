#ifndef HPP_AREGEXCONSTRUCTOREXCEPTION
#define HPP_AREGEXCONSTRUCTOREXCEPTION
#include <alliance.exception.hpp>

class ARegexConstructorException : public AllianceException {
public:
	ARegexConstructorException();
	ARegexConstructorException(std::string text);
	ARegexConstructorException(AllianceException & e);
	ARegexConstructorException(AllianceException & e, std::string text);
	ARegexConstructorException(const ARegexConstructorException & obj);
	~ARegexConstructorException();
};

#endif

