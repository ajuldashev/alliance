#ifndef HPP_AREGEXCONSTRUCTOR
#define HPP_AREGEXCONSTRUCTOR

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.hpp>

#include "ARegexConstructor.exception.hpp"

class ARegexConstructor {
private:
    AString pattern_string;
public:
    ARegexConstructor();

    static ARegexConstructor make(); 

    ARegexConstructor & group_begin();
    ARegexConstructor & group_or();
    ARegexConstructor & group_end();

    ARegexConstructor & unknown();

    ARegexConstructor & advanced_check_positive();
    ARegexConstructor & advanced_check_negative();

    ARegexConstructor & retrospective_check_positive();
    ARegexConstructor & retrospective_check_negative();

    ARegexConstructor & begin();
    ARegexConstructor & end();

    ARegexConstructor & text(AString string);
    ARegexConstructor & regex(AString string);
    ARegexConstructor & regex_class(AString string);

    ARegexConstructor & symbol(ASymbol symbol);
    
    ARegexConstructor & digital();
    ARegexConstructor & space();
    ARegexConstructor & word();

    ARegexConstructor & not_digital();
    ARegexConstructor & not_space();
    ARegexConstructor & not_word();

    ARegexConstructor & any();

    ARegexConstructor & count(s64int min, s64int max);
    ARegexConstructor & count(u64int count);

    ARegexConstructor & may_be_present();
    ARegexConstructor & at_least_one();
    ARegexConstructor & any_number();

    ARegexConstructor & refs(u64int index);
    
    AString to_string();

    ~ARegexConstructor();
};

#endif