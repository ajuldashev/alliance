#include "ARegexConstructor.hpp"

#ifdef TEST_AREGEXCONSTRUCTOR
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * паттерн опсания нода {a, b} 
       */
      AString string_pattern = ARegexConstructor::make()
         // Должно начинаться с открывающий фигурной скобки 
         .regex('{')
            // возможны пробелы
            .space().any_number()
            // целое число a, может отсуствовать {,b}
            .group_begin()
               .digital().any_number()
            .group_end()
            // возможны пробелы
            .space().any_number()
            // может не присутсвовать если нод вида {a} а не {a, b}
            .group_begin()
               .symbol(',')
            .group_end().may_be_present()
            // возможны пробелы
            .space().any_number()
            // целое число b, может отсуствовать {a,}
            .group_begin()
               .digital().any_number()
            .group_end()
            // возможны пробелы
            .space().any_number()
         // Должно заканчиваться закрывающей фигурной скобкой 
         .regex('}')
         .to_string();
      TEST_ALERT(string_pattern == "{\\s*(\\d*)\\s*(,)?\\s*(\\d*)\\s*}", 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info>
       * паттерн описание номера телефона 
       */
      AString string_pattern = ARegexConstructor::make()
         
         .group_begin()
            .regex("\\+7")
         .group_or()
            .regex("8")
         .group_end()

         .group_begin()
            .regex("\\(")
            .digital().count(3)
            .regex("\\)")
         .group_or()
            .digital().count(3)
         .group_end()
         
         .symbol('-').may_be_present()
         .digital().count(3)
         .symbol('-').may_be_present()
         .digital().count(2)
         .symbol('-').may_be_present()
         .digital().count(2)

         .to_string();

      TEST_ALERT(string_pattern == "(\\+7|8)(\\(\\d{3}\\)|\\d{3})-?\\d{3}-?\\d{2}-?\\d{2}", 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info>
       * паттерн описание номера телефона 
       */
      AString string_pattern = ARegexConstructor::make()
         
         .group_begin()
            .text("+7")
         .group_or()
            .text("8")
         .group_end()

         .group_begin()
            .text("(")
            .digital().count(3)
            .text(")")
         .group_or()
            .digital().count(3)
         .group_end()
         
         .symbol('-').may_be_present()
         .digital().count(3)
         .symbol('-').may_be_present()
         .digital().count(2)
         .symbol('-').may_be_present()
         .digital().count(2)

         .to_string();

      TEST_ALERT(string_pattern == "(\\+7|8)(\\(\\d{3}\\)|\\d{3})-?\\d{3}-?\\d{2}-?\\d{2}", 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info>
       * 
       */
      AString string_pattern = ARegexConstructor::make()
         
         .text("abc")

         .group_begin()
            .retrospective_check_positive()
            .text("abc")
         .group_end()

         .to_string();

      TEST_ALERT(string_pattern == "abc(?<=abc)", 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(5)
   {
      /** <doc-test-info>
       * 
       */
      AString string_pattern = ARegexConstructor::make()
         
         .text("\\1234")
         .to_string();
      
      TEST_ALERT(string_pattern == "\\\\1234", 1)
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(aregexconstructor)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)

TEST_FUNCTION_MAIN_END(AREGEXCONSTRUCTOR)

#endif

