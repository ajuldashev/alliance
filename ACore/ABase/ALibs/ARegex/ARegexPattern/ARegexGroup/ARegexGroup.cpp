#include "ARegexGroup.hpp"
#include "ARegexGroup.test.cpp"
#include "ARegexGroup.test.node.cpp"
#include "ARegexGroup.debug.cpp"
#include "ARegexGroup.debug.node.cpp"
#include "ARegexGroup.exception.cpp"

ARegexGroup::ARegexGroup() {
    is_complated = false;
}

ARegexGroup::ARegexGroup(APtr<ARegexGroup> group, APtr<ARegexGroup> next) {
    is_complated = false;
    this->group = group;
    this->next = next;
}

AStringIterator ARegexGroup::execute(AStringIterator iterator) {
    try {
        AStringIterator local_iterator = iterator;
        iterator_begin = iterator;
        bool restart = true;
        group->reset_behavior();
        while (restart) {
            is_complated = false;
            iterator_end = iterator;
            local_iterator = group->execute(local_iterator);
            if (!group->is_complate()) {
                while (group->can_change_behavior()) {
                    group->change_behavior();
                    local_iterator = group->execute(local_iterator);
                    if (group->is_complate()) {
                        is_complated = true;
                        break;
                    }
                }
            } else {
                is_complated = true;
            }
            
            if (!is_complated) {
                return iterator;
            }

            restart = false;
            iterator_end = local_iterator;
            if (next) {
                next->reset_behavior();
                local_iterator = next->execute(local_iterator);
                is_complated = is_complated && next->is_complate();
                if (!is_complated && group->can_change_behavior()) {
                    group->change_behavior();
                    restart = true;
                }
            }
        }
        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexGroup::can_change_behavior() {
    return false;
}

void ARegexGroup::change_behavior() {

}

void ARegexGroup::reset_behavior() {
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

bool ARegexGroup::is_complate() {
    return is_complated;
}

void ARegexGroup::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

void ARegexGroup::set_next(APtr<ARegexGroup> next) {
    this->next = next;
}

AString ARegexGroup::to_text() {
    AString text;
    if (group) {
        text += "(";
        text += group->to_text();
        text += ")";
    }
    if (next) {
        text += next->to_text();
    }
    return text;
}

AStringIterator ARegexGroup::get_begin() {
    return iterator_begin;
}

AStringIterator ARegexGroup::get_end() {
    return iterator_end;
}

AString ARegexGroup::get_result() {
    return iterator_begin.get_between(iterator_end);
}

ARegexGroup::~ARegexGroup() {

}