#include "ARegexGroup.hpp"

#ifdef DEBUG_AREGEXGROUP
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexgroup(AString com) {
	#ifdef DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE
	if (com == "aregexgroupadvancedchecknegative") {
		if (debug_aregexgroupadvancedchecknegative()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXGROUPADVANCEDCHECKPOSITIVE
	if (com == "aregexgroupadvancedcheckpositive") {
		if (debug_aregexgroupadvancedcheckpositive()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
	if (com == "aregexgroupretrospectivechecknegative") {
		if (debug_aregexgroupretrospectivechecknegative()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
	if (com == "aregexgroupretrospectivecheckpositive") {
		if (debug_aregexgroupretrospectivecheckpositive()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXGROUPTREE
	if (com == "aregexgrouptree") {
		if (debug_aregexgrouptree()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexgroup() {
	AString text;
	#ifdef DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE
	text += "d) aregexgroupadvancedchecknegative\n";
	#endif
	#ifdef DEBUG_AREGEXGROUPADVANCEDCHECKPOSITIVE
	text += "d) aregexgroupadvancedcheckpositive\n";
	#endif
	#ifdef DEBUG_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
	text += "d) aregexgroupretrospectivechecknegative\n";
	#endif
	#ifdef DEBUG_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
	text += "d) aregexgroupretrospectivecheckpositive\n";
	#endif
	#ifdef DEBUG_AREGEXGROUPTREE
	text += "d) aregexgrouptree\n";
	#endif
	return text;
}

#endif
