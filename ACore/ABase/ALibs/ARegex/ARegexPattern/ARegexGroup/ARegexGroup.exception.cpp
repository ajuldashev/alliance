#include "ARegexGroup.exception.hpp"

ARegexGroupException::ARegexGroupException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroup";
}

ARegexGroupException::ARegexGroupException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroup");
}

ARegexGroupException::ARegexGroupException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroup");
}

ARegexGroupException::ARegexGroupException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroup");
}

ARegexGroupException::ARegexGroupException(const ARegexGroupException & obj) : AllianceException(obj) {
	
}

ARegexGroupException::~ARegexGroupException() {

}

