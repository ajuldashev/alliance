#ifndef HPP_AREGEXGROUPEXCEPTION
#define HPP_AREGEXGROUPEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupException : public AllianceException {
public:
	ARegexGroupException();
	ARegexGroupException(std::string text);
	ARegexGroupException(AllianceException & e);
	ARegexGroupException(AllianceException & e, std::string text);
	ARegexGroupException(const ARegexGroupException & obj);
	~ARegexGroupException();
};

#endif

