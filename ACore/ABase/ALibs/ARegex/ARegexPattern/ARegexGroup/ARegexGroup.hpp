#ifndef HPP_AREGEXGROUP
#define HPP_AREGEXGROUP
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include "ARegexGroup.exception.hpp"

class ARegexGroup {
private:
    AStringIterator iterator_begin;
    AStringIterator iterator_end;
    APtr<ARegexGroup> group;
protected:
    APtr<ARegexGroup> next;
    bool is_complated;
public:
    ARegexGroup();
    ARegexGroup(APtr<ARegexGroup> group, APtr<ARegexGroup> next);
    virtual AStringIterator execute(AStringIterator iterator);
    virtual bool can_change_behavior();
    virtual void change_behavior();
    virtual void reset_behavior();
    bool is_complate();
    void set_group(APtr<ARegexGroup> group);
    void set_next(APtr<ARegexGroup> next);
    AStringIterator get_begin();
    AStringIterator get_end();
    virtual AString to_text();
    AString get_result();
    virtual ~ARegexGroup();
};

#endif