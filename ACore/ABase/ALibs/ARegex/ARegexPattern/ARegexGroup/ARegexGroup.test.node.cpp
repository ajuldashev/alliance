#include "ARegexGroup.hpp"

#ifdef TEST_AREGEXGROUP
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexgroup(bool is_soft) {
	#ifdef TEST_AREGEXGROUPADVANCEDCHECKNEGATIVE
	if (!test_aregexgroupadvancedchecknegative(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUPADVANCEDCHECKNEGATIVE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXGROUPADVANCEDCHECKPOSITIVE
	if (!test_aregexgroupadvancedcheckpositive(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUPADVANCEDCHECKPOSITIVE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
	if (!test_aregexgroupretrospectivechecknegative(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
	if (!test_aregexgroupretrospectivecheckpositive(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXGROUPTREE
	if (!test_aregexgrouptree(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUPTREE );
		return is_soft;
	}
	#endif
	return true;
}
#endif
