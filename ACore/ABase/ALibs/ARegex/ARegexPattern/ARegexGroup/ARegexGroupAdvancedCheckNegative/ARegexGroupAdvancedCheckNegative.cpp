#include "ARegexGroupAdvancedCheckNegative.hpp"
#include "ARegexGroupAdvancedCheckNegative.test.cpp"
#include "ARegexGroupAdvancedCheckNegative.test.node.cpp"
#include "ARegexGroupAdvancedCheckNegative.debug.cpp"
#include "ARegexGroupAdvancedCheckNegative.debug.node.cpp"
#include "ARegexGroupAdvancedCheckNegative.exception.cpp"

ARegexGroupAdvancedCheckNegative::ARegexGroupAdvancedCheckNegative() {

}

ARegexGroupAdvancedCheckNegative::ARegexGroupAdvancedCheckNegative(const ARegexGroupAdvancedCheckNegative & group) {
    this->group = group.group;
    this->next = group.next;
}

AStringIterator ARegexGroupAdvancedCheckNegative::execute(AStringIterator iterator) {
    try {
        is_complated = true;
        
        APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
        
        local_group->set_group(group);
        AStringIterator local_iterator = iterator.get_second_part();
        for (; local_iterator.can_get(); local_iterator.next()) {
            local_group->reset_behavior();
            local_group->execute(local_iterator);
            if (local_group->is_complate()) {
                is_complated = false;
                break;        
            }
        }
        
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupAdvancedCheckNegativeException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexGroupAdvancedCheckNegative::reset_behavior() {
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexGroupAdvancedCheckNegative::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexGroupAdvancedCheckNegative::to_text() {
    AString text;
    if (group) {
        text += "(?!";
        text += group->to_text();
        text += ")";
    }
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexGroupAdvancedCheckNegative::~ARegexGroupAdvancedCheckNegative() {

}
