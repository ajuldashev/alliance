#include "ARegexGroupAdvancedCheckNegative.hpp"

#ifdef DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aregexgroupadvancedchecknegative() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aregexgroupadvancedchecknegative());
            AIO::writeln("1) help");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aregexgroupadvancedchecknegative(com)) {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE );
    AIO::write_div_line();
    return false;
}
#endif

