#include "ARegexGroupAdvancedCheckNegative.exception.hpp"

ARegexGroupAdvancedCheckNegativeException::ARegexGroupAdvancedCheckNegativeException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroupAdvancedCheckNegative";
}

ARegexGroupAdvancedCheckNegativeException::ARegexGroupAdvancedCheckNegativeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckNegative");
}

ARegexGroupAdvancedCheckNegativeException::ARegexGroupAdvancedCheckNegativeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckNegative");
}

ARegexGroupAdvancedCheckNegativeException::ARegexGroupAdvancedCheckNegativeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckNegative");
}

ARegexGroupAdvancedCheckNegativeException::ARegexGroupAdvancedCheckNegativeException(const ARegexGroupAdvancedCheckNegativeException & obj) : AllianceException(obj) {
	
}

ARegexGroupAdvancedCheckNegativeException::~ARegexGroupAdvancedCheckNegativeException() {

}

