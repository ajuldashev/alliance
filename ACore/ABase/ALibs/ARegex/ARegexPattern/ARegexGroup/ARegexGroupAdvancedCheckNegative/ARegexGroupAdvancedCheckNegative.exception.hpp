#ifndef HPP_AREGEXGROUPADVANCEDCHECKNEGATIVEEXCEPTION
#define HPP_AREGEXGROUPADVANCEDCHECKNEGATIVEEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupAdvancedCheckNegativeException : public AllianceException {
public:
	ARegexGroupAdvancedCheckNegativeException();
	ARegexGroupAdvancedCheckNegativeException(std::string text);
	ARegexGroupAdvancedCheckNegativeException(AllianceException & e);
	ARegexGroupAdvancedCheckNegativeException(AllianceException & e, std::string text);
	ARegexGroupAdvancedCheckNegativeException(const ARegexGroupAdvancedCheckNegativeException & obj);
	~ARegexGroupAdvancedCheckNegativeException();
};

#endif

