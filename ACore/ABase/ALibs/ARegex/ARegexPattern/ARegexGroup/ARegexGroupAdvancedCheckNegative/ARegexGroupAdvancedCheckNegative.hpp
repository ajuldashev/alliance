#ifndef HPP_AREGEXGROUPADVANCEDCHECKNEGATIVE
#define HPP_AREGEXGROUPADVANCEDCHECKNEGATIVE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexGroupAdvancedCheckNegative.exception.hpp"

class ARegexGroupAdvancedCheckNegative : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
public:
    ARegexGroupAdvancedCheckNegative();
    ARegexGroupAdvancedCheckNegative(const ARegexGroupAdvancedCheckNegative & group);
    AStringIterator execute(AStringIterator iterator);
    void reset_behavior();
    bool is_complate();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexGroupAdvancedCheckNegative();
};

#endif