#include "ARegexGroupAdvancedCheckPositive.hpp"
#include "ARegexGroupAdvancedCheckPositive.test.cpp"
#include "ARegexGroupAdvancedCheckPositive.test.node.cpp"
#include "ARegexGroupAdvancedCheckPositive.debug.cpp"
#include "ARegexGroupAdvancedCheckPositive.debug.node.cpp"
#include "ARegexGroupAdvancedCheckPositive.exception.cpp"

ARegexGroupAdvancedCheckPositive::ARegexGroupAdvancedCheckPositive() {

}

ARegexGroupAdvancedCheckPositive::ARegexGroupAdvancedCheckPositive(const ARegexGroupAdvancedCheckPositive & group) {
    this->group = group.group;
    this->next = group.next;
}

AStringIterator ARegexGroupAdvancedCheckPositive::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
        
        local_group->set_group(group);
        AStringIterator local_iterator = iterator.get_second_part();
        for (; local_iterator.can_get(); local_iterator.next()) {
            local_group->reset_behavior();
            local_group->execute(local_iterator);
            if (local_group->is_complate()) {
                is_complated = true;
                break;        
            }
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupAdvancedCheckPositiveException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexGroupAdvancedCheckPositive::reset_behavior() {
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexGroupAdvancedCheckPositive::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexGroupAdvancedCheckPositive::to_text() {
    AString text;
    if (group) {
        text += "(?=";
        text += group->to_text();
        text += ")";
    }
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexGroupAdvancedCheckPositive::~ARegexGroupAdvancedCheckPositive() {

}
