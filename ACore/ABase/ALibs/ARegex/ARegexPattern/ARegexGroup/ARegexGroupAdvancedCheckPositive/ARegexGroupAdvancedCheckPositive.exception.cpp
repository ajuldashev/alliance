#include "ARegexGroupAdvancedCheckPositive.exception.hpp"

ARegexGroupAdvancedCheckPositiveException::ARegexGroupAdvancedCheckPositiveException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroupAdvancedCheckPositive";
}

ARegexGroupAdvancedCheckPositiveException::ARegexGroupAdvancedCheckPositiveException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckPositive");
}

ARegexGroupAdvancedCheckPositiveException::ARegexGroupAdvancedCheckPositiveException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckPositive");
}

ARegexGroupAdvancedCheckPositiveException::ARegexGroupAdvancedCheckPositiveException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroupAdvancedCheckPositive");
}

ARegexGroupAdvancedCheckPositiveException::ARegexGroupAdvancedCheckPositiveException(const ARegexGroupAdvancedCheckPositiveException & obj) : AllianceException(obj) {
	
}

ARegexGroupAdvancedCheckPositiveException::~ARegexGroupAdvancedCheckPositiveException() {

}

