#ifndef HPP_AREGEXGROUPADVANCEDCHECKPOSITIVEEXCEPTION
#define HPP_AREGEXGROUPADVANCEDCHECKPOSITIVEEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupAdvancedCheckPositiveException : public AllianceException {
public:
	ARegexGroupAdvancedCheckPositiveException();
	ARegexGroupAdvancedCheckPositiveException(std::string text);
	ARegexGroupAdvancedCheckPositiveException(AllianceException & e);
	ARegexGroupAdvancedCheckPositiveException(AllianceException & e, std::string text);
	ARegexGroupAdvancedCheckPositiveException(const ARegexGroupAdvancedCheckPositiveException & obj);
	~ARegexGroupAdvancedCheckPositiveException();
};

#endif

