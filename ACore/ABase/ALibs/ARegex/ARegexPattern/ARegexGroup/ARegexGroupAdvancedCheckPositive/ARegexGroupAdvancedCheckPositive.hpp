#ifndef HPP_AREGEXGROUPADVANCEDCHECKPOSITIVE
#define HPP_AREGEXGROUPADVANCEDCHECKPOSITIVE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexGroupAdvancedCheckPositive.exception.hpp"

class ARegexGroupAdvancedCheckPositive : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
public:
    ARegexGroupAdvancedCheckPositive();
    ARegexGroupAdvancedCheckPositive(const ARegexGroupAdvancedCheckPositive & group);
    AStringIterator execute(AStringIterator iterator);
    void reset_behavior();
    bool is_complate();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexGroupAdvancedCheckPositive();
};

#endif