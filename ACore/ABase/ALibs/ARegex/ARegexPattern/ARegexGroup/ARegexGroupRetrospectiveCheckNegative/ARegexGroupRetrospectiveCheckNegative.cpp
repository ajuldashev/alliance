#include "ARegexGroupRetrospectiveCheckNegative.hpp"
#include "ARegexGroupRetrospectiveCheckNegative.test.cpp"
#include "ARegexGroupRetrospectiveCheckNegative.test.node.cpp"
#include "ARegexGroupRetrospectiveCheckNegative.debug.cpp"
#include "ARegexGroupRetrospectiveCheckNegative.debug.node.cpp"
#include "ARegexGroupRetrospectiveCheckNegative.exception.cpp"

ARegexGroupRetrospectiveCheckNegative::ARegexGroupRetrospectiveCheckNegative() {

}

ARegexGroupRetrospectiveCheckNegative::ARegexGroupRetrospectiveCheckNegative(const ARegexGroupRetrospectiveCheckNegative & group) {
    this->group = group.group;
    this->next = group.next;
}

AStringIterator ARegexGroupRetrospectiveCheckNegative::execute(AStringIterator iterator) {
    try {
        is_complated = true;
        
        APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
        
        local_group->set_group(group);
        
        local_group->set_group(group);
        AStringIterator local_iterator = iterator.get_first_part();
        for (; local_iterator.can_get(); local_iterator.next()) {
            local_group->reset_behavior();
            local_group->execute(local_iterator);
            if (local_group->is_complate()) {
                is_complated = false;
                break;        
            }
        }
        
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupRetrospectiveCheckNegativeException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexGroupRetrospectiveCheckNegative::reset_behavior() {
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexGroupRetrospectiveCheckNegative::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexGroupRetrospectiveCheckNegative::to_text() {
    AString text;
    if (group) {
        text += "(?<!";
        text += group->to_text();
        text += ")";
    }
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexGroupRetrospectiveCheckNegative::~ARegexGroupRetrospectiveCheckNegative() {

}
