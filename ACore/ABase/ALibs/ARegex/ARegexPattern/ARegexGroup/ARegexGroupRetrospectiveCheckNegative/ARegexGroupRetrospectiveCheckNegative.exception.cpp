#include "ARegexGroupRetrospectiveCheckNegative.exception.hpp"

ARegexGroupRetrospectiveCheckNegativeException::ARegexGroupRetrospectiveCheckNegativeException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroupRetrospectiveCheckNegative";
}

ARegexGroupRetrospectiveCheckNegativeException::ARegexGroupRetrospectiveCheckNegativeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckNegative");
}

ARegexGroupRetrospectiveCheckNegativeException::ARegexGroupRetrospectiveCheckNegativeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckNegative");
}

ARegexGroupRetrospectiveCheckNegativeException::ARegexGroupRetrospectiveCheckNegativeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckNegative");
}

ARegexGroupRetrospectiveCheckNegativeException::ARegexGroupRetrospectiveCheckNegativeException(const ARegexGroupRetrospectiveCheckNegativeException & obj) : AllianceException(obj) {
	
}

ARegexGroupRetrospectiveCheckNegativeException::~ARegexGroupRetrospectiveCheckNegativeException() {

}

