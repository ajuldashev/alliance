#ifndef HPP_AREGEXGROUPRETROSPECTIVECHECKNEGATIVEEXCEPTION
#define HPP_AREGEXGROUPRETROSPECTIVECHECKNEGATIVEEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupRetrospectiveCheckNegativeException : public AllianceException {
public:
	ARegexGroupRetrospectiveCheckNegativeException();
	ARegexGroupRetrospectiveCheckNegativeException(std::string text);
	ARegexGroupRetrospectiveCheckNegativeException(AllianceException & e);
	ARegexGroupRetrospectiveCheckNegativeException(AllianceException & e, std::string text);
	ARegexGroupRetrospectiveCheckNegativeException(const ARegexGroupRetrospectiveCheckNegativeException & obj);
	~ARegexGroupRetrospectiveCheckNegativeException();
};

#endif

