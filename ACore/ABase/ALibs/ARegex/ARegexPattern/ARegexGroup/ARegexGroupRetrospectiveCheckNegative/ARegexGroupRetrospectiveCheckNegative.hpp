#ifndef HPP_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
#define HPP_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexGroupRetrospectiveCheckNegative.exception.hpp"

class ARegexGroupRetrospectiveCheckNegative : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
public:
    ARegexGroupRetrospectiveCheckNegative();
    ARegexGroupRetrospectiveCheckNegative(const ARegexGroupRetrospectiveCheckNegative & group);
    AStringIterator execute(AStringIterator iterator);
    void reset_behavior();
    bool is_complate();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexGroupRetrospectiveCheckNegative();
};
#endif