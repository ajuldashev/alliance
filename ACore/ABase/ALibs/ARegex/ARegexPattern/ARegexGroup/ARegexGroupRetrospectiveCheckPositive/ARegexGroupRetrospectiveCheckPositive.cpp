#include "ARegexGroupRetrospectiveCheckPositive.hpp"
#include "ARegexGroupRetrospectiveCheckPositive.test.cpp"
#include "ARegexGroupRetrospectiveCheckPositive.test.node.cpp"
#include "ARegexGroupRetrospectiveCheckPositive.debug.cpp"
#include "ARegexGroupRetrospectiveCheckPositive.debug.node.cpp"
#include "ARegexGroupRetrospectiveCheckPositive.exception.cpp"


ARegexGroupRetrospectiveCheckPositive::ARegexGroupRetrospectiveCheckPositive() {

}

ARegexGroupRetrospectiveCheckPositive::ARegexGroupRetrospectiveCheckPositive(const ARegexGroupRetrospectiveCheckPositive & group) {
    this->group = group.group;
    this->next = group.next;
}

AStringIterator ARegexGroupRetrospectiveCheckPositive::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
        
        local_group->set_group(group);
        
        local_group->set_group(group);
        AStringIterator local_iterator = iterator.get_first_part();
        for (; local_iterator.can_get(); local_iterator.next()) {
            local_group->reset_behavior();
            local_group->execute(local_iterator);
            if (local_group->is_complate()) {
                is_complated = true;
                break;        
            }
        }
        
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupRetrospectiveCheckPositiveException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexGroupRetrospectiveCheckPositive::reset_behavior() {
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexGroupRetrospectiveCheckPositive::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexGroupRetrospectiveCheckPositive::to_text() {
    AString text;
    if (group) {
        text += "(?<=";
        text += group->to_text();
        text += ")";
    }
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexGroupRetrospectiveCheckPositive::~ARegexGroupRetrospectiveCheckPositive() {

}
