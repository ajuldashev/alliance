#include "ARegexGroupRetrospectiveCheckPositive.exception.hpp"

ARegexGroupRetrospectiveCheckPositiveException::ARegexGroupRetrospectiveCheckPositiveException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroupRetrospectiveCheckPositive";
}

ARegexGroupRetrospectiveCheckPositiveException::ARegexGroupRetrospectiveCheckPositiveException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckPositive");
}

ARegexGroupRetrospectiveCheckPositiveException::ARegexGroupRetrospectiveCheckPositiveException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckPositive");
}

ARegexGroupRetrospectiveCheckPositiveException::ARegexGroupRetrospectiveCheckPositiveException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroupRetrospectiveCheckPositive");
}

ARegexGroupRetrospectiveCheckPositiveException::ARegexGroupRetrospectiveCheckPositiveException(const ARegexGroupRetrospectiveCheckPositiveException & obj) : AllianceException(obj) {
	
}

ARegexGroupRetrospectiveCheckPositiveException::~ARegexGroupRetrospectiveCheckPositiveException() {

}

