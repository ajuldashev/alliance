#ifndef HPP_AREGEXGROUPRETROSPECTIVECHECKPOSITIVEEXCEPTION
#define HPP_AREGEXGROUPRETROSPECTIVECHECKPOSITIVEEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupRetrospectiveCheckPositiveException : public AllianceException {
public:
	ARegexGroupRetrospectiveCheckPositiveException();
	ARegexGroupRetrospectiveCheckPositiveException(std::string text);
	ARegexGroupRetrospectiveCheckPositiveException(AllianceException & e);
	ARegexGroupRetrospectiveCheckPositiveException(AllianceException & e, std::string text);
	ARegexGroupRetrospectiveCheckPositiveException(const ARegexGroupRetrospectiveCheckPositiveException & obj);
	~ARegexGroupRetrospectiveCheckPositiveException();
};

#endif

