#ifndef HPP_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
#define HPP_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexGroupRetrospectiveCheckPositive.exception.hpp"

class ARegexGroupRetrospectiveCheckPositive : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
public:
    ARegexGroupRetrospectiveCheckPositive();
    ARegexGroupRetrospectiveCheckPositive(const ARegexGroupRetrospectiveCheckPositive & group);
    AStringIterator execute(AStringIterator iterator);
    void reset_behavior();
    bool is_complate();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexGroupRetrospectiveCheckPositive();
};

#endif