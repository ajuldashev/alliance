#include "ARegexGroupTree.hpp"
#include "ARegexGroupTree.test.cpp"
#include "ARegexGroupTree.test.node.cpp"
#include "ARegexGroupTree.debug.cpp"
#include "ARegexGroupTree.debug.node.cpp"
#include "ARegexGroupTree.exception.cpp"

ARegexGroupTree::ARegexGroupTree() {
    pointer_behavior = 0;
}

AStringIterator ARegexGroupTree::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
        APtr<ARegexGroup> group = groups.get(pointer_behavior);
        group->reset_behavior();
        
        local_group->set_group(group);
        local_group->set_next(next);

        iterator = local_group->execute(iterator);

        is_complated = local_group->is_complate();

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexGroupTreeException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexGroupTree::can_change_behavior() {
    return pointer_behavior < groups.length() - 1; 
}

void ARegexGroupTree::change_behavior() {
    pointer_behavior += 1;
}

void ARegexGroupTree::reset_behavior() {
    pointer_behavior = 0;
    for (u64int i = 0; i < groups.length(); i += 1) {
        groups.at(i)->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexGroupTree::push_group(APtr<ARegexGroup> group) {
    groups.push_end(group);
}

AString ARegexGroupTree::to_text() {
    AString text = "(";
    if (groups.length() > 0) {
        text += groups.get(0)->to_text();
    }
    for (u64int i = 1; i < groups.length(); i += 1) {
        text += "|";
        text += groups.get(i)->to_text();
    }
    text += ")";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexGroupTree::~ARegexGroupTree() {

}