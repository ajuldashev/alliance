#include "ARegexGroupTree.exception.hpp"

ARegexGroupTreeException::ARegexGroupTreeException() : AllianceException() {
	exception = "Alliance-Exception-ARegexGroupTree";
}

ARegexGroupTreeException::ARegexGroupTreeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexGroupTree");
}

ARegexGroupTreeException::ARegexGroupTreeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexGroupTree");
}

ARegexGroupTreeException::ARegexGroupTreeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexGroupTree");
}

ARegexGroupTreeException::ARegexGroupTreeException(const ARegexGroupTreeException & obj) : AllianceException(obj) {
	
}

ARegexGroupTreeException::~ARegexGroupTreeException() {

}

