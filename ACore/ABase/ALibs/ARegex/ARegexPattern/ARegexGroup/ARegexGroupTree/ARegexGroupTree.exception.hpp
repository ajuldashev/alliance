#ifndef HPP_AREGEXGROUPTREEEXCEPTION
#define HPP_AREGEXGROUPTREEEXCEPTION
#include <alliance.exception.hpp>

class ARegexGroupTreeException : public AllianceException {
public:
	ARegexGroupTreeException();
	ARegexGroupTreeException(std::string text);
	ARegexGroupTreeException(AllianceException & e);
	ARegexGroupTreeException(AllianceException & e, std::string text);
	ARegexGroupTreeException(const ARegexGroupTreeException & obj);
	~ARegexGroupTreeException();
};

#endif

