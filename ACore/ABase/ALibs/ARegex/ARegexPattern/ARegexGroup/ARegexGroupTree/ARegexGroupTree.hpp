#ifndef HPP_AREGEXGROUPTREE
#define HPP_AREGEXGROUPTREE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include "ARegexGroupTree.exception.hpp"

class ARegexGroupTree : public ARegexGroup {
private:
    AList<APtr<ARegexGroup>> groups;
    u64int pointer_behavior;
public:
    ARegexGroupTree();
    AStringIterator execute(AStringIterator iterator);
    bool can_change_behavior();
    void change_behavior();
    void reset_behavior();
    bool is_complate();
    void push_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexGroupTree();
};

#endif