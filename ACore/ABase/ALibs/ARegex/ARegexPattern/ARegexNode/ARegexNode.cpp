#include "ARegexNode.hpp"
#include "ARegexNode.test.cpp"
#include "ARegexNode.test.node.cpp"
#include "ARegexNode.debug.cpp"
#include "ARegexNode.debug.node.cpp"
#include "ARegexNode.exception.cpp"

ARegexNode::ARegexNode() {
    min_count_nodes = 0;
    max_count_nodes = ATypes<u64int>::max();
}

ARegexNode::ARegexNode(const ARegexNode & node) {
    this->group = node.group;
    this->next = node.next;
}

AStringIterator ARegexNode::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        AStringIterator local_iterator = iterator;
        if (stack.length() == 0) {
            stack.push(local_iterator);
            group->reset_behavior();
            s64int count_nodes = 0;
            APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
            local_group->set_group(group);
            local_iterator = local_group->execute(local_iterator);
            while (local_group->is_complate() && count_nodes < max_count_nodes) {
                count_nodes += 1;
                stack.push(local_iterator);
                local_iterator = local_group->execute(local_iterator);
            }
            if (count_nodes < min_count_nodes) {
                is_complated = false;
                return iterator;
            }
        }

        iterator = stack.get();
        
        is_complated = stack.length() > min_count_nodes;

        if (!is_complated) {
            return iterator;
        }
        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexNode::can_change_behavior() {
    return stack.length() > min_count_nodes && stack.length() > 1;
}

void ARegexNode::change_behavior() {
    stack.pop();
}

void ARegexNode::reset_behavior() {
    stack.clear();
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexNode::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

void ARegexNode::set_min_count_nodes(u64int count) {
    min_count_nodes = count;
}

void ARegexNode::set_max_count_nodes(u64int count) {
    max_count_nodes = count;
}

AString ARegexNode::to_text() {
    AString text;
    text += group->to_text();
    text += "{";
    if (min_count_nodes > 0) {
        text += AStringLibs::to_string(min_count_nodes);
    }
    if (min_count_nodes != max_count_nodes) {
        text += ",";
        if (max_count_nodes != ATypes<u64int>::max()) {
            text += AStringLibs::to_string(max_count_nodes);
        }
    }
    text += "}";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexNode::~ARegexNode() {

}