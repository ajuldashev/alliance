#include "ARegexNode.hpp"

#ifdef DEBUG_AREGEXNODE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexnode(AString com) {
	#ifdef DEBUG_AREGEXNODEANYNUMBER
	if (com == "aregexnodeanynumber") {
		if (debug_aregexnodeanynumber()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXNODEATLEASTONE
	if (com == "aregexnodeatleastone") {
		if (debug_aregexnodeatleastone()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXNODEBEGINLINE
	if (com == "aregexnodebeginline") {
		if (debug_aregexnodebeginline()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXNODEENDLINE
	if (com == "aregexnodeendline") {
		if (debug_aregexnodeendline()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXNODEMAYBEPRESENT
	if (com == "aregexnodemaybepresent") {
		if (debug_aregexnodemaybepresent()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexnode() {
	AString text;
	#ifdef DEBUG_AREGEXNODEANYNUMBER
	text += "d) aregexnodeanynumber\n";
	#endif
	#ifdef DEBUG_AREGEXNODEATLEASTONE
	text += "d) aregexnodeatleastone\n";
	#endif
	#ifdef DEBUG_AREGEXNODEBEGINLINE
	text += "d) aregexnodebeginline\n";
	#endif
	#ifdef DEBUG_AREGEXNODEENDLINE
	text += "d) aregexnodeendline\n";
	#endif
	#ifdef DEBUG_AREGEXNODEMAYBEPRESENT
	text += "d) aregexnodemaybepresent\n";
	#endif
	return text;
}

#endif
