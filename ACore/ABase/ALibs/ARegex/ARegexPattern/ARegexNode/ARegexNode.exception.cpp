#include "ARegexNode.exception.hpp"

ARegexNodeException::ARegexNodeException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNode";
}

ARegexNodeException::ARegexNodeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNode");
}

ARegexNodeException::ARegexNodeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNode");
}

ARegexNodeException::ARegexNodeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNode");
}

ARegexNodeException::ARegexNodeException(const ARegexNodeException & obj) : AllianceException(obj) {
	
}

ARegexNodeException::~ARegexNodeException() {

}

