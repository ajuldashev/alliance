#ifndef HPP_AREGEXNODEEXCEPTION
#define HPP_AREGEXNODEEXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeException : public AllianceException {
public:
	ARegexNodeException();
	ARegexNodeException(std::string text);
	ARegexNodeException(AllianceException & e);
	ARegexNodeException(AllianceException & e, std::string text);
	ARegexNodeException(const ARegexNodeException & obj);
	~ARegexNodeException();
};

#endif

