#ifndef HPP_AREGEXNODE
#define HPP_AREGEXNODE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/AStruct/AStack/AStack.hpp>
#include "ARegexNode.exception.hpp"

class ARegexNode : public ARegexGroup {
private:
    AStack<AStringIterator> stack;
    APtr<ARegexGroup> group;
    u64int min_count_nodes;
    u64int max_count_nodes;
public:
    ARegexNode();
    ARegexNode(const ARegexNode & node);

    AStringIterator execute(AStringIterator iterator);
    bool can_change_behavior();
    void change_behavior();
    void reset_behavior();
    void set_group(APtr<ARegexGroup> group);

    void set_min_count_nodes(u64int count);
    void set_max_count_nodes(u64int count);

    AString to_text();

    ~ARegexNode();
};

#endif