#include "ARegexNode.hpp"

#ifdef TEST_AREGEXNODE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexnode(bool is_soft) {
	#ifdef TEST_AREGEXNODEANYNUMBER
	if (!test_aregexnodeanynumber(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODEANYNUMBER );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXNODEATLEASTONE
	if (!test_aregexnodeatleastone(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODEATLEASTONE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXNODEBEGINLINE
	if (!test_aregexnodebeginline(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODEBEGINLINE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXNODEENDLINE
	if (!test_aregexnodeendline(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODEENDLINE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXNODEMAYBEPRESENT
	if (!test_aregexnodemaybepresent(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODEMAYBEPRESENT );
		return is_soft;
	}
	#endif
	return true;
}
#endif
