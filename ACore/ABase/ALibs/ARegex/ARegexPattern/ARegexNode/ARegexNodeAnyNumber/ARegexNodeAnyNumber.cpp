#include "ARegexNodeAnyNumber.hpp"
#include "ARegexNodeAnyNumber.test.cpp"
#include "ARegexNodeAnyNumber.test.node.cpp"
#include "ARegexNodeAnyNumber.debug.cpp"
#include "ARegexNodeAnyNumber.debug.node.cpp"
#include "ARegexNodeAnyNumber.exception.cpp"

ARegexNodeAnyNumber::ARegexNodeAnyNumber() {

}

ARegexNodeAnyNumber::ARegexNodeAnyNumber(const ARegexNodeAnyNumber & node) {
    this->group = node.group;
    this->next = node.next;
}

AStringIterator ARegexNodeAnyNumber::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (stack.length() == 0) {
            stack.push(iterator);
            group->reset_behavior();
            APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
            local_group->set_group(group);
            iterator = local_group->execute(iterator);
            while (local_group->is_complate()) {
                stack.push(iterator);
                iterator = local_group->execute(iterator);
            }
        } else {
            iterator = stack.get();
        }
        
        is_complated = true;

        if (!is_complated) {
            return iterator;
        }

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeAnyNumberException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexNodeAnyNumber::can_change_behavior() {
    return stack.length() > 1;
}

void ARegexNodeAnyNumber::change_behavior() {
    stack.pop();
}

void ARegexNodeAnyNumber::reset_behavior() {
    stack.clear();
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexNodeAnyNumber::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexNodeAnyNumber::to_text() {
    AString text;
    text += group->to_text();
    text += "*";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexNodeAnyNumber::~ARegexNodeAnyNumber() {

}
