#include "ARegexNodeAnyNumber.exception.hpp"

ARegexNodeAnyNumberException::ARegexNodeAnyNumberException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNodeAnyNumber";
}

ARegexNodeAnyNumberException::ARegexNodeAnyNumberException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNodeAnyNumber");
}

ARegexNodeAnyNumberException::ARegexNodeAnyNumberException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNodeAnyNumber");
}

ARegexNodeAnyNumberException::ARegexNodeAnyNumberException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNodeAnyNumber");
}

ARegexNodeAnyNumberException::ARegexNodeAnyNumberException(const ARegexNodeAnyNumberException & obj) : AllianceException(obj) {
	
}

ARegexNodeAnyNumberException::~ARegexNodeAnyNumberException() {

}

