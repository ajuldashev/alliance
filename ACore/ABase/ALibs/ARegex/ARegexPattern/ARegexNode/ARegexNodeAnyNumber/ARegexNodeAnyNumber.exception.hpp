#ifndef HPP_AREGEXNODEANYNUMBEREXCEPTION
#define HPP_AREGEXNODEANYNUMBEREXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeAnyNumberException : public AllianceException {
public:
	ARegexNodeAnyNumberException();
	ARegexNodeAnyNumberException(std::string text);
	ARegexNodeAnyNumberException(AllianceException & e);
	ARegexNodeAnyNumberException(AllianceException & e, std::string text);
	ARegexNodeAnyNumberException(const ARegexNodeAnyNumberException & obj);
	~ARegexNodeAnyNumberException();
};

#endif

