#ifndef HPP_AREGEXNODEANYNUMBER
#define HPP_AREGEXNODEANYNUMBER

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/AStruct/AStack/AStack.hpp>

#include "ARegexNodeAnyNumber.exception.hpp"

class ARegexNodeAnyNumber : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
    AStack<AStringIterator> stack;
public:
    ARegexNodeAnyNumber();
    ARegexNodeAnyNumber(const ARegexNodeAnyNumber & node);
    AStringIterator execute(AStringIterator iterator);
    bool can_change_behavior();
    void change_behavior();
    void reset_behavior();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexNodeAnyNumber();
};

#endif