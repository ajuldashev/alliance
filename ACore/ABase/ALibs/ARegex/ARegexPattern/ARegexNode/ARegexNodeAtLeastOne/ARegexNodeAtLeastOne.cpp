#include "ARegexNodeAtLeastOne.hpp"
#include "ARegexNodeAtLeastOne.test.cpp"
#include "ARegexNodeAtLeastOne.test.node.cpp"
#include "ARegexNodeAtLeastOne.debug.cpp"
#include "ARegexNodeAtLeastOne.debug.node.cpp"
#include "ARegexNodeAtLeastOne.exception.cpp"

ARegexNodeAtLeastOne::ARegexNodeAtLeastOne() {

}

ARegexNodeAtLeastOne::ARegexNodeAtLeastOne(const ARegexNodeAtLeastOne & node) {
    this->group = node.group;
    this->next = node.next;
}

AStringIterator ARegexNodeAtLeastOne::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (stack.length() == 0) {
            group->reset_behavior();
            APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
            local_group->set_group(group);
            iterator = local_group->execute(iterator);
            while (local_group->is_complate()) {
                stack.push(iterator);
                iterator = local_group->execute(iterator);
            }
            if (stack.length() == 0) {
                return iterator;
            }
        } else {
            iterator = stack.get();
        }
        
        is_complated = true;

        if (!is_complated) {
            return iterator;
        }

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeAtLeastOneException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexNodeAtLeastOne::can_change_behavior() {
    return stack.length() > 1;
}

void ARegexNodeAtLeastOne::change_behavior() {
    stack.pop();
}

void ARegexNodeAtLeastOne::reset_behavior() {
    stack.clear();
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexNodeAtLeastOne::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexNodeAtLeastOne::to_text() {
    AString text;
    text += group->to_text();
    text += "*";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexNodeAtLeastOne::~ARegexNodeAtLeastOne() {

}
