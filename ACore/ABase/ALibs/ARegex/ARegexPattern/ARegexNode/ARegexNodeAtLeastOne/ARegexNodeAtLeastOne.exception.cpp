#include "ARegexNodeAtLeastOne.exception.hpp"

ARegexNodeAtLeastOneException::ARegexNodeAtLeastOneException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNodeAtLeastOne";
}

ARegexNodeAtLeastOneException::ARegexNodeAtLeastOneException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNodeAtLeastOne");
}

ARegexNodeAtLeastOneException::ARegexNodeAtLeastOneException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNodeAtLeastOne");
}

ARegexNodeAtLeastOneException::ARegexNodeAtLeastOneException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNodeAtLeastOne");
}

ARegexNodeAtLeastOneException::ARegexNodeAtLeastOneException(const ARegexNodeAtLeastOneException & obj) : AllianceException(obj) {
	
}

ARegexNodeAtLeastOneException::~ARegexNodeAtLeastOneException() {

}

