#ifndef HPP_ARegexNodeAtLeastOneEXCEPTION
#define HPP_ARegexNodeAtLeastOneEXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeAtLeastOneException : public AllianceException {
public:
	ARegexNodeAtLeastOneException();
	ARegexNodeAtLeastOneException(std::string text);
	ARegexNodeAtLeastOneException(AllianceException & e);
	ARegexNodeAtLeastOneException(AllianceException & e, std::string text);
	ARegexNodeAtLeastOneException(const ARegexNodeAtLeastOneException & obj);
	~ARegexNodeAtLeastOneException();
};

#endif

