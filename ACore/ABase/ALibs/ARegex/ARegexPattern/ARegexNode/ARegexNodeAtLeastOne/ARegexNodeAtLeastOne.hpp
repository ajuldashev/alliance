#ifndef HPP_AREGEXNODEATLEASTONE
#define HPP_AREGEXNODEATLEASTONE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/AStruct/AStack/AStack.hpp>

#include "ARegexNodeAtLeastOne.exception.hpp"

class ARegexNodeAtLeastOne : public ARegexGroup {
    private:
    APtr<ARegexGroup> group;
    AStack<AStringIterator> stack;
public:
    ARegexNodeAtLeastOne();
    ARegexNodeAtLeastOne(const ARegexNodeAtLeastOne & node);
    AStringIterator execute(AStringIterator iterator);
    bool can_change_behavior();
    void change_behavior();
    void reset_behavior();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexNodeAtLeastOne();
};

#endif