#include "ARegexNodeBeginLine.hpp"
#include "ARegexNodeBeginLine.test.cpp"
#include "ARegexNodeBeginLine.test.node.cpp"
#include "ARegexNodeBeginLine.debug.cpp"
#include "ARegexNodeBeginLine.debug.node.cpp"
#include "ARegexNodeBeginLine.exception.cpp"

ARegexNodeBeginLine::ARegexNodeBeginLine() {

}

ARegexNodeBeginLine::ARegexNodeBeginLine(const ARegexNodeBeginLine & node) {

}

AStringIterator ARegexNodeBeginLine::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (!iterator.is_first()) {
            return iterator;
        }

        is_complated = true;
        
        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeBeginLineException(e, "execute(AStringIterator iterator)");
    }
}

AString ARegexNodeBeginLine::to_text() {
    AString text = "^";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexNodeBeginLine::~ARegexNodeBeginLine() {

}