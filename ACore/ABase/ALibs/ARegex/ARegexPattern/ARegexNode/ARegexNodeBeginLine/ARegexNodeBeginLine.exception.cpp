#include "ARegexNodeBeginLine.exception.hpp"

ARegexNodeBeginLineException::ARegexNodeBeginLineException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNodeBeginLine";
}

ARegexNodeBeginLineException::ARegexNodeBeginLineException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNodeBeginLine");
}

ARegexNodeBeginLineException::ARegexNodeBeginLineException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNodeBeginLine");
}

ARegexNodeBeginLineException::ARegexNodeBeginLineException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNodeBeginLine");
}

ARegexNodeBeginLineException::ARegexNodeBeginLineException(const ARegexNodeBeginLineException & obj) : AllianceException(obj) {
	
}

ARegexNodeBeginLineException::~ARegexNodeBeginLineException() {

}

