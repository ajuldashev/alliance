#ifndef HPP_AREGEXNODEBEGINLINEEXCEPTION
#define HPP_AREGEXNODEBEGINLINEEXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeBeginLineException : public AllianceException {
public:
	ARegexNodeBeginLineException();
	ARegexNodeBeginLineException(std::string text);
	ARegexNodeBeginLineException(AllianceException & e);
	ARegexNodeBeginLineException(AllianceException & e, std::string text);
	ARegexNodeBeginLineException(const ARegexNodeBeginLineException & obj);
	~ARegexNodeBeginLineException();
};

#endif

