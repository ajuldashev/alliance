#ifndef HPP_AREGEXNODEBEGINLINE
#define HPP_AREGEXNODEBEGINLINE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexNodeBeginLine.exception.hpp"

class ARegexNodeBeginLine : public ARegexGroup {
public:
    ARegexNodeBeginLine();
    ARegexNodeBeginLine(const ARegexNodeBeginLine & node);
    AStringIterator execute(AStringIterator iterator);
    AString to_text();
    ~ARegexNodeBeginLine();
};

#endif