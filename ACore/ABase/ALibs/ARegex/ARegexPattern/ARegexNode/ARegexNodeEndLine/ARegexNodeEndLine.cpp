#include "ARegexNodeEndLine.hpp"
#include "ARegexNodeEndLine.test.cpp"
#include "ARegexNodeEndLine.test.node.cpp"
#include "ARegexNodeEndLine.debug.cpp"
#include "ARegexNodeEndLine.debug.node.cpp"
#include "ARegexNodeEndLine.exception.cpp"

ARegexNodeEndLine::ARegexNodeEndLine() {

}

ARegexNodeEndLine::ARegexNodeEndLine(const ARegexNodeEndLine & node) {

}

AStringIterator ARegexNodeEndLine::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (iterator.can_get()) {
            return iterator;
        }
        is_complated = true;

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeEndLineException(e, "execute(AStringIterator iterator)");
    }
}

AString ARegexNodeEndLine::to_text() {
    AString text = "$";
    if (next) {
        text = next->to_text();
    }
    return text;
}

ARegexNodeEndLine::~ARegexNodeEndLine() {

}