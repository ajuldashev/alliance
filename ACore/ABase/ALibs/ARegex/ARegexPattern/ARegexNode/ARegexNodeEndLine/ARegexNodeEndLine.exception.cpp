#include "ARegexNodeEndLine.exception.hpp"

ARegexNodeEndLineException::ARegexNodeEndLineException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNodeEndLine";
}

ARegexNodeEndLineException::ARegexNodeEndLineException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNodeEndLine");
}

ARegexNodeEndLineException::ARegexNodeEndLineException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNodeEndLine");
}

ARegexNodeEndLineException::ARegexNodeEndLineException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNodeEndLine");
}

ARegexNodeEndLineException::ARegexNodeEndLineException(const ARegexNodeEndLineException & obj) : AllianceException(obj) {
	
}

ARegexNodeEndLineException::~ARegexNodeEndLineException() {

}

