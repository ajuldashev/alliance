#ifndef HPP_AREGEXNODEENDLINEEXCEPTION
#define HPP_AREGEXNODEENDLINEEXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeEndLineException : public AllianceException {
public:
	ARegexNodeEndLineException();
	ARegexNodeEndLineException(std::string text);
	ARegexNodeEndLineException(AllianceException & e);
	ARegexNodeEndLineException(AllianceException & e, std::string text);
	ARegexNodeEndLineException(const ARegexNodeEndLineException & obj);
	~ARegexNodeEndLineException();
};

#endif

