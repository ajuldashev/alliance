#ifndef HPP_ARegexNodeEndLine
#define HPP_ARegexNodeEndLine

 #include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexNodeEndLine.exception.hpp"

class ARegexNodeEndLine : public ARegexGroup {
public : 
    ARegexNodeEndLine();
    ARegexNodeEndLine(const ARegexNodeEndLine & node);
    AStringIterator execute(AStringIterator iterator);
    AString to_text();
    ~ARegexNodeEndLine();
};

#endif