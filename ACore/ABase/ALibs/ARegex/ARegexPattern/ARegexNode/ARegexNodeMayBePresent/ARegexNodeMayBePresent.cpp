#include "ARegexNodeMayBePresent.hpp"
#include "ARegexNodeMayBePresent.test.cpp"
#include "ARegexNodeMayBePresent.test.node.cpp"
#include "ARegexNodeMayBePresent.debug.cpp"
#include "ARegexNodeMayBePresent.debug.node.cpp"
#include "ARegexNodeMayBePresent.exception.cpp"

ARegexNodeMayBePresent::ARegexNodeMayBePresent() {

}

ARegexNodeMayBePresent::ARegexNodeMayBePresent(const ARegexNodeMayBePresent & node) {
    this->group = node.group;
    this->next = node.next;
}

AStringIterator ARegexNodeMayBePresent::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (is_present) {
            group->reset_behavior();
            APtr<ARegexGroup> local_group = APtr<ARegexGroup>::make();
            local_group->set_group(group);
            iterator = local_group->execute(iterator);
            is_complated = local_group->is_complate();
        } else {
            is_complated = true;
        }
        
        if (!is_complated) {
            return iterator;
        }

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexNodeMayBePresentException(e, "execute(AStringIterator iterator)");
    }
}

bool ARegexNodeMayBePresent::can_change_behavior() {
    return is_present;
}

void ARegexNodeMayBePresent::change_behavior() {
    is_present = false;
}

void ARegexNodeMayBePresent::reset_behavior() {
    is_present = true;
    if (group) {
        group->reset_behavior();
    }
    if (next) {
        next->reset_behavior();
    }
}

void ARegexNodeMayBePresent::set_group(APtr<ARegexGroup> group) {
    this->group = group;
}

AString ARegexNodeMayBePresent::to_text() {
    AString text;
    text += group->to_text();
    text += "?";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexNodeMayBePresent::~ARegexNodeMayBePresent() {

}
    