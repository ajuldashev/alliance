#include "ARegexNodeMayBePresent.exception.hpp"

ARegexNodeMayBePresentException::ARegexNodeMayBePresentException() : AllianceException() {
	exception = "Alliance-Exception-ARegexNodeMayBePresent";
}

ARegexNodeMayBePresentException::ARegexNodeMayBePresentException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexNodeMayBePresent");
}

ARegexNodeMayBePresentException::ARegexNodeMayBePresentException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexNodeMayBePresent");
}

ARegexNodeMayBePresentException::ARegexNodeMayBePresentException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexNodeMayBePresent");
}

ARegexNodeMayBePresentException::ARegexNodeMayBePresentException(const ARegexNodeMayBePresentException & obj) : AllianceException(obj) {
	
}

ARegexNodeMayBePresentException::~ARegexNodeMayBePresentException() {

}

