#ifndef HPP_AREGEXNODEMAYBEPRESENTEXCEPTION
#define HPP_AREGEXNODEMAYBEPRESENTEXCEPTION
#include <alliance.exception.hpp>

class ARegexNodeMayBePresentException : public AllianceException {
public:
	ARegexNodeMayBePresentException();
	ARegexNodeMayBePresentException(std::string text);
	ARegexNodeMayBePresentException(AllianceException & e);
	ARegexNodeMayBePresentException(AllianceException & e, std::string text);
	ARegexNodeMayBePresentException(const ARegexNodeMayBePresentException & obj);
	~ARegexNodeMayBePresentException();
};

#endif

