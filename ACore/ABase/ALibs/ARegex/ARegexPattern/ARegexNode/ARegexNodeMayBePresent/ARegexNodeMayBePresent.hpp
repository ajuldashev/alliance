#ifndef HPP_AREGEXNODEMAYBEPRESET
#define HPP_AREGEXNODEMAYBEPRESET

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>

#include "ARegexNodeMayBePresent.exception.hpp"

class ARegexNodeMayBePresent : public ARegexGroup {
private:
    APtr<ARegexGroup> group;
    bool is_present;
public:
    ARegexNodeMayBePresent();
    ARegexNodeMayBePresent(const ARegexNodeMayBePresent & node);
    AStringIterator execute(AStringIterator iterator);
    bool can_change_behavior();
    void change_behavior();
    void reset_behavior();
    void set_group(APtr<ARegexGroup> group);
    AString to_text();
    ~ARegexNodeMayBePresent();
};

#endif