#include "ARegexParser.hpp"
#include "ARegexParser.test.cpp"
#include "ARegexParser.test.node.cpp"
#include "ARegexParser.debug.cpp"
#include "ARegexParser.debug.node.cpp"
#include "ARegexParser.exception.cpp"

APtr<ARegexGroup> ARegexParser::get_next_group(AStringIterator & iterator) {
    try {

        APtr<ARegexGroup> result;
        APtr<ARegexGroup> next;
        
        if (!iterator.can_get()) {
            return result;
        }

        ARegexParserToken token;
        AStringIterator local_iterator = iterator;
        local_iterator = token.set(local_iterator);
        
        switch (token.get_token()) {
            case ARegexParserToken::Token::group_end :
            case ARegexParserToken::Token::group_or  :
                return result;
            default :
                iterator = local_iterator;
        }
        
        result = switch_base_token(token, iterator);

        result = switch_node_token(token, iterator, result);

        next = get_next_group(iterator);
        
        result->set_next(next);

        return result;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_next_group(AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::switch_base_token(ARegexParserToken token, AStringIterator & iterator) {
    try {
        APtr<ARegexGroup> result;
        switch (token.get_token()) {
            case ARegexParserToken::Token::symbol : {
                result = get_symbol(token);
                break;               
            }
            case ARegexParserToken::Token::symbol_any : {
                result = get_symbol_any(); 
                break;              
            }
            case ARegexParserToken::Token::symbol_class_begin : {
                result = get_symbol_class(iterator);
                break;
            }
            case ARegexParserToken::Token::symbol_class_invert_begin : {
                result = get_symbol_class_invert(iterator);
                break;
            }
            case ARegexParserToken::Token::symbol_digital : {
                result = get_symbol_digital();
                break;
            }
            case ARegexParserToken::Token::symbol_space : {
                result = get_symbol_space();
                break;
            }
            case ARegexParserToken::Token::symbol_word : {
                result = get_symbol_word();
                break;
            }
            case ARegexParserToken::Token::symbol_not_digital : {
                result = get_symbol_not_digital();
                break;
            }
            case ARegexParserToken::Token::symbol_not_space : {
                result = get_symbol_not_space();
                break;
            }
            case ARegexParserToken::Token::symbol_not_word : {
                result = get_symbol_not_word();
                break;
            }
            case ARegexParserToken::Token::group_begin : {
                u64int index_group = groups->length();
                groups->push_end(result);
                result = APtr<ARegexGroup>::make();
                result->set_group(get_group(iterator));
                groups->at(index_group) = result;
                break;
            }
            case ARegexParserToken::Token::group_unknown : {
                result = APtr<ARegexGroup>::make();
                result->set_group(get_group(iterator));
                break;
            }
            case ARegexParserToken::Token::group_advanced_check_positive : {
                APtr<ARegexGroupAdvancedCheckPositive> local_group = APtr<ARegexGroupAdvancedCheckPositive>::make();
                local_group->set_group(get_group(iterator));
                result = APtr<ARegexGroup>::make();
                result->set_group(aptr_cast<ARegexGroupAdvancedCheckPositive, ARegexGroup>(local_group));
                break;
            }
            case ARegexParserToken::Token::group_advanced_check_negative : {
                APtr<ARegexGroupAdvancedCheckNegative> local_group = APtr<ARegexGroupAdvancedCheckNegative>::make();
                local_group->set_group(get_group(iterator));
                result = APtr<ARegexGroup>::make();
                result->set_group(aptr_cast<ARegexGroupAdvancedCheckNegative, ARegexGroup>(local_group));
                break;
            }
            case ARegexParserToken::Token::group_retrospective_check_positive : {
                APtr<ARegexGroupRetrospectiveCheckPositive> local_group = APtr<ARegexGroupRetrospectiveCheckPositive>::make();
                local_group->set_group(get_group(iterator));
                result = APtr<ARegexGroup>::make();
                result->set_group(aptr_cast<ARegexGroupRetrospectiveCheckPositive, ARegexGroup>(local_group));
                break;
            }
            case ARegexParserToken::Token::group_retrospective_check_negative : {
                APtr<ARegexGroupRetrospectiveCheckNegative> local_group = APtr<ARegexGroupRetrospectiveCheckNegative>::make();
                local_group->set_group(get_group(iterator));
                result = APtr<ARegexGroup>::make();
                result->set_group(aptr_cast<ARegexGroupRetrospectiveCheckNegative, ARegexGroup>(local_group));
                break;
            }
            case ARegexParserToken::Token::node_begin_line : {
                result = aptr_make<ARegexGroup, ARegexNodeBeginLine>();
                break;
            }
            case ARegexParserToken::Token::node_end_line : {
                result = aptr_make<ARegexGroup, ARegexNodeEndLine>();
                break;
            }
            case ARegexParserToken::Token::refs : {
                result = get_refs(iterator);
                break;
            }
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "switch_base_token(ARegexParserToken token, AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::switch_node_token(ARegexParserToken token, AStringIterator & iterator, APtr<ARegexGroup> result) {
    try {
        if (iterator.can_get()) {
            AStringIterator local_iterator = token.set(iterator);
            switch (token.get_token()) {
                case ARegexParserToken::Token::node_may_be_present : {
                    result = get_node_may_be_present(result);
                    iterator = local_iterator;
                    break;
                }
                case ARegexParserToken::Token::node_any_number : {
                    result = get_node_any_namber(result);
                    iterator = local_iterator;
                    break;
                }
                case ARegexParserToken::Token::node_at_least_one : {
                    result = get_node_at_least_one(result);
                    iterator = local_iterator;
                    break;
                }
                case ARegexParserToken::Token::node : {
                    result = get_node(result, local_iterator);
                    iterator = local_iterator;
                    break;
                }   
            }
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "switch_next_token(ARegexParserToken token, AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol(ARegexParserToken & token) {
    try {
        APtr<ARegexSymbol> symbol = APtr<ARegexSymbol>::make();
        symbol->set_symbol(token.get_symbol());
        return aptr_cast<ARegexSymbol, ARegexGroup>(symbol);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol(ARegexParserToken & token)");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_any() {
    try {
        APtr<ARegexSymbolAny> symbol_any = APtr<ARegexSymbolAny>::make();
        return aptr_cast<ARegexSymbolAny, ARegexGroup>(symbol_any);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_any()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_digital() {
    try {
        APtr<ARegexSymbolDigital> symbol_digital = APtr<ARegexSymbolDigital>::make();
        return aptr_cast<ARegexSymbolDigital, ARegexGroup>(symbol_digital);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_digital()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_space() {
    try {
        APtr<ARegexSymbolSpace> symbol_space = APtr<ARegexSymbolSpace>::make();
        return aptr_cast<ARegexSymbolSpace, ARegexGroup>(symbol_space);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_space()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_word() {
    try {
        APtr<ARegexSymbolWord> symbol_word = APtr<ARegexSymbolWord>::make();
        return aptr_cast<ARegexSymbolWord, ARegexGroup>(symbol_word);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_word()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_not_digital() {
    try {
        APtr<ARegexSymbolNotDigital> symbol_not_digital = APtr<ARegexSymbolNotDigital>::make();
        return aptr_cast<ARegexSymbolNotDigital, ARegexGroup>(symbol_not_digital);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_not_digital()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_not_space() {
    try {
        APtr<ARegexSymbolNotSpace> symbol_not_space = APtr<ARegexSymbolNotSpace>::make();
        return aptr_cast<ARegexSymbolNotSpace, ARegexGroup>(symbol_not_space);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_not_space()");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_not_word() {
    try {
        APtr<ARegexSymbolNotWord> symbol_not_word = APtr<ARegexSymbolNotWord>::make();
        return aptr_cast<ARegexSymbolNotWord, ARegexGroup>(symbol_not_word);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_not_word()");
    }
}

APtr<ARegexGroup> ARegexParser::get_group(AStringIterator & iterator) {
    try {
        ARegexParserToken token;
        APtr<ARegexGroup> group;
        group = get_next_group(iterator);
        iterator = token.set(iterator);
        if (token.get_token() == ARegexParserToken::Token::group_or) {
            APtr<ARegexGroupTree> tree = APtr<ARegexGroupTree>::make();
            while (token.get_token() == ARegexParserToken::Token::group_or) {
                tree->push_group(group);
                group = get_next_group(iterator);
                iterator = token.set(iterator);
            }
            tree->push_group(group);
            group = aptr_cast<ARegexGroupTree, ARegexGroup>(tree);
        }
        if (token.get_token() != ARegexParserToken::Token::group_end) {
            throw ARegexParserException("get_group(AStringIterator & iterator) : Not found ')'");
        }
        return group;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_group(AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::get_main_group(AStringIterator & iterator) {
    try {
        APtr<ARegexGroup> main_group;
        APtr<ARegexGroup> group;
        ARegexParserToken token;
        groups = APtr<AList<APtr<ARegexGroup>>>::make();
        
        if (!iterator.can_get()) {
            return main_group;
        }

        main_group = APtr<ARegexGroup>::make();

        group = get_next_group(iterator);
        
        if (iterator.can_get()) {
            iterator = token.set(iterator);
            if (token.get_token() == ARegexParserToken::Token::group_or) {
                APtr<ARegexGroupTree> tree = APtr<ARegexGroupTree>::make();
                while (token.get_token() == ARegexParserToken::Token::group_or) {
                    tree->push_group(group);
                    group = get_next_group(iterator);
                    if (iterator.can_get()) { 
                        iterator = token.set(iterator);
                    } else {
                        break;
                    }
                }
                tree->push_group(group);
                group = aptr_cast<ARegexGroupTree, ARegexGroup>(tree);
            }
        }

        main_group->set_group(group);
        groups->push_beg(main_group);
        
        return main_group;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_main_group(AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_class(AStringIterator & iterator) {
    try {
        ARegexParserToken token;
        APtr<ARegexGroup> group;

        AString text_class = get_body_symbol_class(iterator);

        APtr<ARegexSymbolClass> symbol_class = APtr<ARegexSymbolClass>::make();
        symbol_class->parse(text_class);
        group = aptr_cast<ARegexSymbolClass, ARegexGroup>(symbol_class);

        return group;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_class(AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::get_symbol_class_invert(AStringIterator & iterator) {
    try {
        ARegexParserToken token;
        APtr<ARegexGroup> group;
        
        AString text_class = "^";
        text_class += get_body_symbol_class(iterator);

        APtr<ARegexSymbolClass> symbol_class = APtr<ARegexSymbolClass>::make();
        symbol_class->parse(text_class);
        group = aptr_cast<ARegexSymbolClass, ARegexGroup>(symbol_class);

        return group;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_symbol_class_invert(AStringIterator & iterator)");
    }
}

AString ARegexParser::get_body_symbol_class(AStringIterator & iterator) {
    try {
        ARegexParserToken token;
        AString text_class;

        iterator = token.set(iterator);

        if (token.get_token() == ARegexParserToken::Token::symbol_class_end) {
            throw ARegexParserException("get_body_symbol_class(AStringIterator & iterator) : class is empty");
        }

        while (token.get_token() != ARegexParserToken::Token::symbol_class_end) {
            if (token.get_token() == ARegexParserToken::Token::symbol) {
                text_class += token.get_symbol().to_string();
            } else {
                throw ARegexParserException("get_body_symbol_class(AStringIterator & iterator) : error class");
            }
            iterator = token.set(iterator);
        }

        return text_class;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_body_symbol_class(AStringIterator & iterator)");
    }
}

APtr<ARegexGroup> ARegexParser::get_node_may_be_present(APtr<ARegexGroup> result) {
    APtr<ARegexGroup> group = APtr<ARegexGroup>::make();
    APtr<ARegexNodeMayBePresent> node = APtr<ARegexNodeMayBePresent>::make();
    node->set_group(result);
    group->set_group(aptr_cast<ARegexNodeMayBePresent, ARegexGroup>(node));
    return group;
}

APtr<ARegexGroup> ARegexParser::get_node_at_least_one(APtr<ARegexGroup> result) {
    APtr<ARegexGroup> group = APtr<ARegexGroup>::make();
    APtr<ARegexNodeAtLeastOne> node = APtr<ARegexNodeAtLeastOne>::make();
    node->set_group(result);
    group->set_group(aptr_cast<ARegexNodeAtLeastOne, ARegexGroup>(node));
    return group;
}

APtr<ARegexGroup> ARegexParser::get_node_any_namber(APtr<ARegexGroup> result) {
    APtr<ARegexGroup> group = APtr<ARegexGroup>::make();
    APtr<ARegexNodeAnyNumber> node = APtr<ARegexNodeAnyNumber>::make();
    node->set_group(result);
    group->set_group(aptr_cast<ARegexNodeAnyNumber, ARegexGroup>(node));
    return group;
}

APtr<ARegexGroup> ARegexParser::get_node(APtr<ARegexGroup> result, AStringIterator & iterator) {
    APtr<ARegexGroup> group = APtr<ARegexGroup>::make();
    APtr<ARegexNode> node = APtr<ARegexNode>::make();
    node->set_group(result);

    ARegexParser mini_parser("\\s*(\\d*)\\s*(,?)\\s*(\\d*)\\s*}");
    APtr<ARegexGroup> group_node = mini_parser.parse();

    iterator = group_node->execute(iterator);

    AString string_number_min = mini_parser.get_groups()->get(1)->get_result();
    AString string_number_max = mini_parser.get_groups()->get(3)->get_result();

    bool is_single = mini_parser.get_groups()->get(2)->get_result().length() == 0;
    u64int min_count_node = 0;
    u64int max_count_node = 0;

    if (is_single && string_number_min.length() != 0) { 
        min_count_node = AStringLibs::to_ulong(string_number_min);
        node->set_min_count_nodes(min_count_node);
        node->set_max_count_nodes(min_count_node);
    } else
    if (string_number_min.length() != 0 && string_number_max.length() != 0) {
        min_count_node = AStringLibs::to_ulong(string_number_min);
        max_count_node = AStringLibs::to_ulong(string_number_max);
        node->set_min_count_nodes(min_count_node);
        node->set_max_count_nodes(max_count_node);
    } else
    if (string_number_min.length() != 0 && string_number_max.length() == 0) {
        min_count_node = AStringLibs::to_ulong(string_number_min);
        node->set_min_count_nodes(min_count_node);
    } else
    if (string_number_min.length() == 0 && string_number_max.length() != 0) {
        max_count_node = AStringLibs::to_ulong(string_number_max);
        node->set_max_count_nodes(max_count_node);
    }

    group->set_group(aptr_cast<ARegexNode, ARegexGroup>(node));
    return group;
}

APtr<ARegexGroup> ARegexParser::get_refs(AStringIterator & iterator) {
    try {
        APtr<ARegexGroup> result;
        if (!iterator.can_get()) {
            return result;
        }

        APtr<ARegexGroup> group = APtr<ARegexGroup>::make();
        APtr<ARegexNodeAtLeastOne> node = APtr<ARegexNodeAtLeastOne>::make();
        APtr<ARegexSymbolDigital> digital = APtr<ARegexSymbolDigital>::make();

        node->set_group(aptr_cast<ARegexSymbolDigital, ARegexGroup>(digital));
        group->set_group(aptr_cast<ARegexNodeAtLeastOne, ARegexGroup>(node));

        iterator = group->execute(iterator);

        AString string_number = group->get_result();

        u64int index = AStringLibs::to_ulong(string_number);

        APtr<ARegexRefs> refs = APtr<ARegexRefs>::make();
        refs->set_refs(groups);
        refs->set_index(index);

        result = aptr_cast<ARegexRefs, ARegexGroup>(refs);

        return result;
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "get_refs(AStringIterator & iterator)");
    }
}

ARegexParser::ARegexParser() {

}

ARegexParser::ARegexParser(AString pattern) {
    try {
        set(pattern);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "ARegexParser(AString pattern)");
    }
}

void ARegexParser::set(AString pattern) {
    this->pattern = pattern;
}

APtr<ARegexGroup> ARegexParser::parse() {
    try {
        groups.reset();
        AStringIterator regex_iterator(pattern);
        return get_main_group(regex_iterator);
    } catch (AllianceException & e) {
        throw ARegexParserException(e, "parse()");
    }
}

APtr<AList<APtr<ARegexGroup>>> ARegexParser::get_groups() {
    return groups;
}

ARegexParser::~ARegexParser() {

}