#include "ARegexParser.hpp"

#ifdef DEBUG_AREGEXPARSER
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexparser(AString com) {
	#ifdef DEBUG_AREGEXPARSERTOKEN
	if (com == "aregexparsertoken") {
		if (debug_aregexparsertoken()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexparser() {
	AString text;
	#ifdef DEBUG_AREGEXPARSERTOKEN
	text += "d) aregexparsertoken\n";
	#endif
	return text;
}

#endif
