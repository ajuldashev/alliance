#include "ARegexParser.exception.hpp"

ARegexParserException::ARegexParserException() : AllianceException() {
	exception = "Alliance-Exception-ARegexParser";
}

ARegexParserException::ARegexParserException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexParser");
}

ARegexParserException::ARegexParserException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexParser");
}

ARegexParserException::ARegexParserException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexParser");
}

ARegexParserException::ARegexParserException(const ARegexParserException & obj) : AllianceException(obj) {
	
}

ARegexParserException::~ARegexParserException() {

}

