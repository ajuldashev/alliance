#ifndef HPP_AREGEXPARSEREXCEPTION
#define HPP_AREGEXPARSEREXCEPTION
#include <alliance.exception.hpp>

class ARegexParserException : public AllianceException {
public:
	ARegexParserException();
	ARegexParserException(std::string text);
	ARegexParserException(AllianceException & e);
	ARegexParserException(AllianceException & e, std::string text);
	ARegexParserException(const ARegexParserException & obj);
	~ARegexParserException();
};

#endif

