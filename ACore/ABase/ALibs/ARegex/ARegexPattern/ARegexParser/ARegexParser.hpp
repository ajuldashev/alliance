#ifndef HPP_AREGEXPARSER
#define HPP_AREGEXPARSER

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.hpp>

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.hpp>

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.hpp>

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.hpp>

#include<ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.hpp>

#include "ARegexParserToken/ARegexParserToken.hpp"

#include "ARegexParser.exception.hpp"

class ARegexParser {
private:
    AString pattern;

    APtr<AList<APtr<ARegexGroup>>> groups;

    APtr<ARegexGroup> get_next_group(AStringIterator & iterator);

    APtr<ARegexGroup> switch_base_token(ARegexParserToken token, AStringIterator & iterator);
    APtr<ARegexGroup> switch_node_token(ARegexParserToken token, AStringIterator & iterator, APtr<ARegexGroup> result);

    APtr<ARegexGroup> get_group(AStringIterator & iterator);

    APtr<ARegexGroup> get_main_group(AStringIterator & iterator);

    APtr<ARegexGroup> get_symbol(ARegexParserToken & token);
    APtr<ARegexGroup> get_symbol_any();
    APtr<ARegexGroup> get_symbol_class(AStringIterator & iterator);
    APtr<ARegexGroup> get_symbol_class_invert(AStringIterator & iterator);

    APtr<ARegexGroup> get_symbol_digital();
    APtr<ARegexGroup> get_symbol_space();
    APtr<ARegexGroup> get_symbol_word();

    APtr<ARegexGroup> get_symbol_not_digital();
    APtr<ARegexGroup> get_symbol_not_space();
    APtr<ARegexGroup> get_symbol_not_word();

    AString get_body_symbol_class(AStringIterator & iterator);

    APtr<ARegexGroup> get_node_may_be_present(APtr<ARegexGroup> result);
    APtr<ARegexGroup> get_node_at_least_one(APtr<ARegexGroup> result);
    APtr<ARegexGroup> get_node_any_namber(APtr<ARegexGroup> result);

    APtr<ARegexGroup> get_node(APtr<ARegexGroup> result, AStringIterator & iterator);

    APtr<ARegexGroup> get_refs(AStringIterator & iterator);

public:
    ARegexParser();
    ARegexParser(AString pattern);

    void set(AString pattern);

    APtr<ARegexGroup> parse();

    APtr<AList<APtr<ARegexGroup>>> get_groups();

    ~ARegexParser();
};

#endif