#include "ARegexParser.hpp"

#ifdef TEST_AREGEXPARSER
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexparser(bool is_soft) {
	#ifdef TEST_AREGEXPARSERTOKEN
	if (!test_aregexparsertoken(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXPARSERTOKEN );
		return is_soft;
	}
	#endif
	return true;
}
#endif
