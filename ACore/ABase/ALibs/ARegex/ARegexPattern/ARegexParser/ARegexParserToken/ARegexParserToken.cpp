#include "ARegexParserToken.hpp"
#include "ARegexParserToken.test.cpp"
#include "ARegexParserToken.test.node.cpp"
#include "ARegexParserToken.debug.cpp"
#include "ARegexParserToken.debug.node.cpp"
#include "ARegexParserToken.exception.cpp"

ARegexParserToken::ARegexParserToken() {
    token = Token::undefined;
}

ARegexParserToken::ARegexParserToken(const ARegexParserToken & token) {
    this->token = token.token;
    this->symbol = token.symbol;
}

AStringIterator ARegexParserToken::set(AStringIterator iterator) {
    try {
        return parse_symbol_base(iterator);
    } catch (AllianceException & e) {
        throw ARegexParserTokenException(e, "set(AStringIterator iterator)");
    }
}

AStringIterator ARegexParserToken::parse_symbol_base(AStringIterator iterator) {
    try {
        if (!iterator.can_get()) {
            return iterator;
        }
        ASymbol local_symbol = iterator.get();
        if (local_symbol == '^') {
            token = Token::node_begin_line;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '$') {
            token = Token::node_end_line;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '?') {
            token = Token::node_may_be_present;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '+') {
            token = Token::node_at_least_one;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '*') {
            token = Token::node_any_number;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '.') {
            token = Token::symbol_any;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '[') {
            token = Token::symbol_class_begin;
            return parse_symbol_class(iterator);
        }
        if (local_symbol == ']') {
            token = Token::symbol_class_end;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '(') {
            token = Token::group_begin;
            return parse_group_custom(iterator);
        }
        if (local_symbol == ')') {
            token = Token::group_end;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '{') {
            token = Token::node;
            iterator.next();
            return iterator;
        }
        if (local_symbol ==  '|') {
            token = Token::group_or;
            iterator.next();
            return iterator;
        }
        if (local_symbol == '\\') {
            token = Token::symbol;
            return parse_symbol_custom(iterator);
        }
        token = Token::symbol;
        symbol = local_symbol;
        iterator.next();
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexParserTokenException(e, "parse_symbol_base(AStringIterator iterator)");
    }
}

AStringIterator ARegexParserToken::parse_symbol_custom(AStringIterator iterator) {
    try {
        AStringIterator local_iterator = iterator;
        local_iterator.next();

        ARegexSymbolDigital digital;
        digital.execute(local_iterator);
        if (digital.is_complate()) {
            token = Token::refs;
            iterator.next();
            return iterator;
        }
        ASymbol local_symbol = local_iterator.get();
        if (local_symbol == 'd') {
            token = Token::symbol_digital;
            local_iterator.next();
            return local_iterator;
        }
        if (local_symbol == 's') {
            token = Token::symbol_space;
            local_iterator.next();
            return local_iterator;
        }
        if (local_symbol == 'w') {
            token = Token::symbol_word;
            local_iterator.next();
            return local_iterator;
        }
        if (local_symbol == 'D') {
            token = Token::symbol_not_digital;
            local_iterator.next();
            return local_iterator;
        }
        if (local_symbol == 'S') {
            token = Token::symbol_not_space;
            local_iterator.next();
            return local_iterator;
        }
        if (local_symbol == 'W') {
            token = Token::symbol_not_word;
            local_iterator.next();
            return local_iterator;
        }
        token = Token::symbol;
        symbol = local_symbol;
        local_iterator.next();
        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexParserTokenException(e, "parse_symbol_custom(AStringIterator iterator)");
    }
}

AStringIterator ARegexParserToken::parse_symbol_class(AStringIterator iterator) {
    try {
        AStringIterator local_iterator = iterator;
        local_iterator.next();
        if (!local_iterator.can_get()) {
            return iterator;
        }
        if (local_iterator.get() == '^') {
            token = Token::symbol_class_invert_begin;
            local_iterator.next();
            return local_iterator;
        } else {
            iterator.next();
            return iterator;
        }
    } catch (AllianceException & e) {
        throw ARegexParserTokenException(e, "parse_symbol_class(AStringIterator iterator)");
    }
}

AStringIterator ARegexParserToken::parse_group_custom(AStringIterator iterator) {
    try {
        AStringIterator local_iterator = iterator;
        local_iterator.next();
        if (!local_iterator.can_get()) {
            iterator.next();
            return iterator;
        }
        ASymbol symbol_group = local_iterator.get();
        if (symbol_group == '?') {
            local_iterator.next();
            if (!iterator.can_get()) {
                return iterator;
            }
            ASymbol symbol_group_check = local_iterator.get();
            if (symbol_group_check == ':' ) {       
                token = Token::group_unknown;
                local_iterator.next();
                return local_iterator;
            }
            if (symbol_group_check == '=' ) {       
                token = Token::group_advanced_check_positive;
                local_iterator.next();
                return local_iterator;
            }
            if (symbol_group_check == '!' ) {       
                token = Token::group_advanced_check_negative;
                local_iterator.next();
                return local_iterator;
            }
            if (symbol_group_check == '<' ) {
                local_iterator.next();
                if (!local_iterator.can_get()) {
                    return iterator;
                }
                ASymbol symbol_group_retrospective_check = local_iterator.get();
                if  (symbol_group_retrospective_check == '=') {
                    token = Token::group_retrospective_check_positive;
                    local_iterator.next();
                    return local_iterator;
                } 
                if  (symbol_group_retrospective_check == '!') {
                    token = Token::group_retrospective_check_negative;
                    local_iterator.next();
                    return local_iterator;
                } 
                return local_iterator;
            }                
            return local_iterator;
        }
        iterator.next();
        return iterator;
    } catch (AllianceException & e) {
        throw ARegexParserTokenException(e, "parse_group_custom(AStringIterator iterator)");
    }
}

ARegexParserToken::Token ARegexParserToken::get_token() {
    return token;
}

ASymbol ARegexParserToken::get_symbol() {
    return symbol;
}

AString ARegexParserToken::to_string() {
    AString result;
    switch (token) {
        case Token::undefined : {
            return result;
        }
        case Token::node_begin_line: {
            result = "\\^";
            return result;
        }
        case Token::node_end_line: {
            result = "\\$";
            return result;
        }
        case Token::node_may_be_present: {
            result = "\\?";
            return result;
        }
        case Token::node_at_least_one: {
            result = "\\+";
            return result;
        }
        case Token::node_any_number: {
            result = "\\*";
            return result;
        }
        case Token::node: {
            result = "\\{";
            return result;
        }
        case Token::symbol: {
            result = symbol.to_string();
            return result;
        }
        case Token::symbol_any: {
            result = "\\.";
            return result;
        }
        case Token::symbol_class_begin: {
            result = "\\[";
            return result;
        }
        case Token::symbol_class_invert_begin: {
            result = "\\[\\^";
            return result;
        }
        case Token::symbol_class_end: {
            result = "\\]";
            return result;
        }
        case Token::symbol_digital: {
            result = "\\\\d";
            return result;
        }
        case Token::symbol_space: {
            result = "\\\\s";
            return result;
        }
        case Token::symbol_word: {
            result = "\\\\w";
            return result;
        }
        case Token::symbol_not_digital: {
            result = "\\\\D";
            return result;
        }
        case Token::symbol_not_space: {
            result = "\\\\S";
            return result;
        }
        case Token::symbol_not_word: {
            result = "\\\\W";
            return result;
        }
        case Token::group_begin: {
            result = "\\(";
            return result;
        }
        case Token::group_end: {
            result = "\\)";
            return result;
        }
        case Token::group_or: {
            result = "\\|";
            return result;
        }
        case Token::group_advanced_check_positive: {
            result = "\\(\\?=";
            return result;
        }
        case Token::group_advanced_check_negative: {
            result = "\\(\\?!";
            return result;
        }
        case Token::group_retrospective_check_positive: {
            result = "\\(\\?<=";
            return result;
        }
        case Token::group_retrospective_check_negative: {
            result = "\\(\\?<!";
            return result;
        }
        case Token::group_unknown: {
            result = "\\(\\?:";
            return result;
        }
        case Token::refs: {
            result = "\\\\";
            return result;
        }
        case Token::error: {
            return result;
        }
    }
    return result;
}

ARegexParserToken::~ARegexParserToken() {

}