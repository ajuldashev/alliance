#include "ARegexParserToken.exception.hpp"

ARegexParserTokenException::ARegexParserTokenException() : AllianceException() {
	exception = "Alliance-Exception-ARegexParserToken";
}

ARegexParserTokenException::ARegexParserTokenException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexParserToken");
}

ARegexParserTokenException::ARegexParserTokenException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexParserToken");
}

ARegexParserTokenException::ARegexParserTokenException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexParserToken");
}

ARegexParserTokenException::ARegexParserTokenException(const ARegexParserTokenException & obj) : AllianceException(obj) {
	
}

ARegexParserTokenException::~ARegexParserTokenException() {

}

