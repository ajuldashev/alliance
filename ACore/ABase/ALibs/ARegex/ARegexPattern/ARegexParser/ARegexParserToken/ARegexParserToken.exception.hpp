#ifndef HPP_AREGEXPARSERTOKENEXCEPTION
#define HPP_AREGEXPARSERTOKENEXCEPTION
#include <alliance.exception.hpp>

class ARegexParserTokenException : public AllianceException {
public:
	ARegexParserTokenException();
	ARegexParserTokenException(std::string text);
	ARegexParserTokenException(AllianceException & e);
	ARegexParserTokenException(AllianceException & e, std::string text);
	ARegexParserTokenException(const ARegexParserTokenException & obj);
	~ARegexParserTokenException();
};

#endif

