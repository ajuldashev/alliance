#ifndef HPP_AREGEXPARSERTOKEN
#define HPP_AREGEXPARSERTOKEN
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include "ARegexParserToken.exception.hpp"

class ARegexParserToken {
public:
    enum class Token {
        undefined,
        node_begin_line,
        node_end_line,
        node_may_be_present,
        node_at_least_one,
        node_any_number,
        node,
        symbol,
        symbol_any,
        symbol_class_begin,
        symbol_class_invert_begin,
        symbol_class_end,
        symbol_digital,
        symbol_space,
        symbol_word,
        symbol_not_digital,
        symbol_not_space,
        symbol_not_word,
        group_begin,
        group_end,
        group_or,
        group_advanced_check_positive,
        group_advanced_check_negative,
        group_retrospective_check_positive,
        group_retrospective_check_negative,
        group_unknown,
        refs,
        error
    };
private:
    Token token;
    ASymbol symbol;

    AStringIterator parse_symbol_base(AStringIterator iterator);
    AStringIterator parse_symbol_custom(AStringIterator iterator);
    AStringIterator parse_symbol_class(AStringIterator iterator);
    AStringIterator parse_group_custom(AStringIterator iterator);

public:
    ARegexParserToken();
    ARegexParserToken(const ARegexParserToken & token);

    AStringIterator set(AStringIterator iterator);

    Token    get_token();
    ASymbol  get_symbol();

    AString to_string();

    ~ARegexParserToken();
};

#endif