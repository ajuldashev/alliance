#include "ARegexParserToken.hpp"

#ifdef TEST_AREGEXPARSERTOKEN
#include <ACore/ABase/AIO/AIO.hpp>


/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(1)
   {
      AString string = "a";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>
       * Конекретный символ
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 1);
      TEST_ALERT(token.get_symbol() == 'a', 2);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(2)
   {
      AString string = ".";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>
       * Любой символ
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol_any, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(3)
   {
      AString string = "^";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>
       * Символ начала строки
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::node_begin_line, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(4)
   {
      AString string = "$";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ конца строки
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::node_end_line, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(5)
   {
      AString string = "[";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Начало символьного класса
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol_class_begin, 1);
   }
   {
      AString string = "[^";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Начало инвертированного символьного класса
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol_class_invert_begin, 2);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(6)
   {
      AString string = "]";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Конец символьного класса
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol_class_end, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(7)
   {
      AString string = "(";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Начало группы символов
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_begin, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(8)
   {
      AString string = ")";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Конец группы символов
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_end, 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(9)
   {
      AString string = "\\.";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ .
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 1);
      TEST_ALERT(token.get_symbol() == '.', 2);
   }
   {
      AString string = "\\^";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ ^
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 3);
      TEST_ALERT(token.get_symbol() == '^', 4);
   }
   {
      AString string = "\\$";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ $
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 5);
      TEST_ALERT(token.get_symbol() == '$', 6);
   }
   {
      AString string = "\\[";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ [
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 7);
      TEST_ALERT(token.get_symbol() == '[', 8);
   }
   {
      AString string = "\\]";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ ]
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 9);
      TEST_ALERT(token.get_symbol() == ']', 10);
   }
   {
      AString string = "\\(";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ (
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 11);
      TEST_ALERT(token.get_symbol() == '(', 12);
   }
   {
      AString string = "\\)";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * Символ )
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::symbol, 13);
      TEST_ALERT(token.get_symbol() == ')', 14);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * AStringIterator set(AStringIterator iterator);
 */
TEST_FUNCTION_BEGIN(10)
   {
      AString string = "(";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_begin, 1);
   }
   {
      AString string = "(?:";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_unknown, 1);
   }
   {
      AString string = "(?=";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_advanced_check_positive, 1);
   }
   {
      AString string = "(?!";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_advanced_check_negative, 1);
   }
   {
      AString string = "(?<=";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_retrospective_check_positive, 1);
   }
   {
      AString string = "(?<!";
      AStringIterator iterator(string);
      ARegexParserToken token;
      token.set(iterator);
      /** <doc-test-info>Любой символ символ
       * 
       */
      TEST_ALERT(token.get_token() == ARegexParserToken::Token::group_retrospective_check_negative, 1);
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(aregexparsertoken)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)
   TEST_CALL(7)
   TEST_CALL(8)
   TEST_CALL(9)
   TEST_CALL(10)

TEST_FUNCTION_MAIN_END(AREGEXPARSERTOKEN)

#endif

