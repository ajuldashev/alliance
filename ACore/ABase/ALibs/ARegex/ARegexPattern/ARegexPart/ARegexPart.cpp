#include "ARegexPart.hpp"
#include "ARegexPart.test.cpp"
#include "ARegexPart.test.node.cpp"
#include "ARegexPart.debug.cpp"
#include "ARegexPart.debug.node.cpp"
#include "ARegexPart.exception.cpp"


ARegexPart::ARegexPart() {

}

ARegexPart::ARegexPart(const ARegexPart & part) {
    this->start = part.start;
    this->finish = part.finish;
    this->matches = part.matches;
}

void ARegexPart::set_start(AStringIterator start) {
    this->start = start;
}

void ARegexPart::set_finish(AStringIterator finish) {
    this->finish = finish;
}

void ARegexPart::set_matches(APtr<AStringList> matches) {
    this->matches = matches;
}

AStringIterator ARegexPart::get_start() {
    return start;
}

AStringIterator ARegexPart::get_finish() {
    return finish;
}

APtr<AStringList> ARegexPart::get_matches() {
    return matches;
}

ARegexPart::~ARegexPart() {

}