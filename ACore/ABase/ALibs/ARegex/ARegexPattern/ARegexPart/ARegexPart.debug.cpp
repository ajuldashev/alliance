#include "ARegexPart.hpp"

#ifdef DEBUG_AREGEXPART
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aregexpart() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_AREGEXPART );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_aregexpart());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_aregexpart(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_AREGEXPART );
	AIO::write_div_line();
	return false;
}
#endif
