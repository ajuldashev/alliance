#include "ARegexPart.exception.hpp"

ARegexPartException::ARegexPartException() : AllianceException() {
	exception = "Alliance-Exception-ARegexPart";
}

ARegexPartException::ARegexPartException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexPart");
}

ARegexPartException::ARegexPartException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexPart");
}

ARegexPartException::ARegexPartException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexPart");
}

ARegexPartException::ARegexPartException(const ARegexPartException & obj) : AllianceException(obj) {

}

ARegexPartException::~ARegexPartException() {

}
