#ifndef HPP_AREGEXPARTEXCEPTION
#define HPP_AREGEXPARTEXCEPTION
#include <alliance.exception.hpp>

class ARegexPartException : public AllianceException {
public:
	ARegexPartException();
	ARegexPartException(std::string text);
	ARegexPartException(AllianceException & e);
	ARegexPartException(AllianceException & e, std::string text);
	ARegexPartException(const ARegexPartException & obj);
	~ARegexPartException();
};

#endif
