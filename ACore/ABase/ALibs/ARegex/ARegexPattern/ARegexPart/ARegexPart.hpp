#ifndef HPP_AREGEXPART
#define HPP_AREGEXPART
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.hpp>
#include "ARegexPart.exception.hpp"

class ARegexPart {
private:
    AStringIterator start;
    AStringIterator finish;
    APtr<AStringList> matches;
public:
    ARegexPart();
    ARegexPart(const ARegexPart & part);

    void set_start(AStringIterator start);
    void set_finish(AStringIterator finish);
    void set_matches(APtr<AStringList> matches);

    AStringIterator get_start();
    AStringIterator get_finish();
    APtr<AStringList> get_matches();

    ~ARegexPart();
};

#endif
