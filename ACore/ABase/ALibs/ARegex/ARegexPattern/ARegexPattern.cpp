#include "ARegexPattern.hpp"
#include "ARegexPattern.test.cpp"
#include "ARegexPattern.test.node.cpp"
#include "ARegexPattern.debug.cpp"
#include "ARegexPattern.debug.node.cpp"
#include "ARegexPattern.exception.cpp"

AString ARegexPattern::insert_refs(AString string) {
    try {
        if (groups) {
            ARegexPattern pattern("(?<!\\\\$)\\\\(\\d+)");
            APtr<AList<APtr<AStringList>>> list = pattern.find_all(string);
            for (u64int i = 0; list && i < list->length(); i += 1) {
                ARegexPattern replace_pattern("(?<!\\\\$)\\" + list->at(i)->at(0));
                AString substring = get(AStringLibs::to_ulong(list->at(i)->at(1)));
                string = replace_pattern.replace_first(string, substring);
            }
        }
        return string;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "insert_refs(AString stirng)");
    }
}

ARegexPattern::ARegexPattern() {
    
}

ARegexPattern::ARegexPattern(AString pattern) {
    try {
        set(pattern);
        compile();
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "ARegexPattern(AString pattern)");
    }
}

ARegexPattern::ARegexPattern(const ARegexPattern & pattern) {
    try {
        this->groups  = pattern.groups;
        this->group   = pattern.group;
        this->pattern = pattern.pattern;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "ARegexPattern(const ARegexPattern & pattern)");
    }
}

void ARegexPattern::set(AString pattern) {
    this->pattern = pattern;
}

void ARegexPattern::compile() {
    try {
        ARegexParser parser(pattern);
        group = parser.parse();
        groups = parser.get_groups();
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "compile()");
    }
}

bool ARegexPattern::match(AString string) {
    try {
        if (group) {
            AStringIterator iterator(string);
            return match(iterator) && !iterator.has_next();
        }
        return false;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "match(AString string)");
    }
}

bool ARegexPattern::match(AStringIterator & iterator) {
    try {
        if (group) {
            iterator = group->execute(iterator);
            return group->is_complate();
        }
        return false;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "match(AStringIterator iterator)");
    }
}

APtr<AStringList> ARegexPattern::matches(AString string) {
    try {
        APtr<AStringList> result;
        if (group) {
            AStringIterator iterator(string);
            result = matches(iterator);
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "matches(AString string)");
    }
}

APtr<AStringList> ARegexPattern::matches(AStringIterator & iterator) {
    try {
        APtr<AStringList> result;
        if (group) {
            iterator = group->execute(iterator);
            
            if (!group->is_complate() || iterator.has_next()) {
                return result;
            }
            result = APtr<AStringList>::make();
            for (u64int i = 0; i < groups->length(); i += 1) {
                result->push_end(groups->at(i)->get_result());
            }
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "matches(AStringIterator & iterator)");
    }
}

ARegexPart ARegexPattern::matches_part(AString string) {
    try {
        ARegexPart result;
        if (group) {
            AStringIterator iterator(string);
            result = matches_part(iterator);
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "matches_part(AString string)");
    }
}

ARegexPart ARegexPattern::matches_part(AStringIterator iterator) {
    try {
        ARegexPart result;
        if (group) {
            AStringIterator finish = group->execute(iterator);
            
            result.set_start(iterator);
            result.set_finish(finish);

            if (!group->is_complate()) {
                return result;
            }
            result.set_matches(APtr<AStringList>::make());
            for (u64int i = 0; i < groups->length(); i += 1) {
                result.get_matches()->push_end(groups->at(i)->get_result());
            }
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "matches_part(AStringIterator & iterator)");
    }
}

APtr<AStringList> ARegexPattern::find_first(AString string) {
    try {
        APtr<AStringList> result;
        if (group) {
            AStringIterator iterator(string);
            iterator = group->execute(iterator);
            while (!group->is_complate() || iterator.has_next()) {
                iterator.next();
                iterator = group->execute(iterator);
            }
            result = APtr<AStringList>::make();
            for (u64int i = 0; i < groups->length(); i += 1) {
                result->push_end(groups->at(i)->get_result());
            }
        }
        return result;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "find_first(AString string)");
    }
}

APtr<AList<APtr<AStringList>>> ARegexPattern::find_all(AString string) { 
    try {
        APtr<AList<APtr<AStringList>>> list;
        if (group) {
            AStringIterator iterator(string);
            while (iterator.can_get()) {
                iterator = group->execute(iterator);
                if (group->is_complate()) {
                    APtr<AStringList> result = APtr<AStringList>::make();
                    for (u64int i = 0; i < groups->length(); i += 1) {
                        result->push_end(groups->at(i)->get_result());
                    }
                    if (!list) {
                        list = APtr<AList<APtr<AStringList>>>::make();
                    }
                    list->push_end(result);
                } else {
                    iterator.next();
                }
            }
        }
        return list;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "find_all(AString string)");
    }
}

AString ARegexPattern::replace_first(AString string, AString substring) {
    try {
        if (group) {
            AStringIterator iterator(string);
            while (iterator.can_get()) {
                AStringIterator local_iterator = group->execute(iterator);
                if (group->is_complate()) {
                    return iterator.replace_between(local_iterator, insert_refs(substring));
                }
                iterator.next();
            }
        }
        return string;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "replace_first(AString string, AString substring)");
    }
}

AString ARegexPattern::replace_all(AString string, AString substring) {
    try {
        if (group) {
            AStringIterator iterator(string);
            while (iterator.can_get()) {
                AStringIterator local_iterator = group->execute(iterator);
                if (group->is_complate()) {
                    string = iterator.replace_between(local_iterator, insert_refs(substring));
                    iterator = local_iterator;
                } else {
                    iterator.next();
                }
            }
        }
        return string;
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "replace_all(AString string, AString substring)");
    }
}

AString ARegexPattern::to_text() {
    AString text;
    if (group) {
        text = group->to_text();
    }
    return text;
}

u64int ARegexPattern::length() {
    return groups->length();
}

AString ARegexPattern::get(u64int index) {
    try {
        return groups->at(index)->get_result();
    } catch (AllianceException & e) {
        throw ARegexPatternException(e, "get(u64int index)");
    }
}

ARegexPattern::~ARegexPattern() {
    
}