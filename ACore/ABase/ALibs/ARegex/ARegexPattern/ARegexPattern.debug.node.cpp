#include "ARegexPattern.hpp"

#ifdef DEBUG_AREGEXPATTERN
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexpattern(AString com) {
	#ifdef DEBUG_AREGEXGROUP
	if (com == "aregexgroup") {
		if (debug_aregexgroup()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXNODE
	if (com == "aregexnode") {
		if (debug_aregexnode()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXPARSER
	if (com == "aregexparser") {
		if (debug_aregexparser()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXPART
	if (com == "aregexpart") {
		if (debug_aregexpart()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXREFS
	if (com == "aregexrefs") {
		if (debug_aregexrefs()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOL
	if (com == "aregexsymbol") {
		if (debug_aregexsymbol()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexpattern() {
	AString text;
	#ifdef DEBUG_AREGEXGROUP
	text += "d) aregexgroup\n";
	#endif
	#ifdef DEBUG_AREGEXNODE
	text += "d) aregexnode\n";
	#endif
	#ifdef DEBUG_AREGEXPARSER
	text += "d) aregexparser\n";
	#endif
	#ifdef DEBUG_AREGEXPART
	text += "d) aregexpart\n";
	#endif
	#ifdef DEBUG_AREGEXREFS
	text += "d) aregexrefs\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOL
	text += "d) aregexsymbol\n";
	#endif
	return text;
}

#endif
