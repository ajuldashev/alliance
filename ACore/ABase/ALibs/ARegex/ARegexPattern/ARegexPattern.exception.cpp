#include "ARegexPattern.exception.hpp"

ARegexPatternException::ARegexPatternException() : AllianceException() {
	exception = "Alliance-Exception-ARegexPattern";
}

ARegexPatternException::ARegexPatternException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexPattern");
}

ARegexPatternException::ARegexPatternException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexPattern");
}

ARegexPatternException::ARegexPatternException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexPattern");
}

ARegexPatternException::ARegexPatternException(const ARegexPatternException & obj) : AllianceException(obj) {
	
}

ARegexPatternException::~ARegexPatternException() {

}

