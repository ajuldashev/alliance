#ifndef HPP_AREGEXPATTERNEXCEPTION
#define HPP_AREGEXPATTERNEXCEPTION
#include <alliance.exception.hpp>

class ARegexPatternException : public AllianceException {
public:
	ARegexPatternException();
	ARegexPatternException(std::string text);
	ARegexPatternException(AllianceException & e);
	ARegexPatternException(AllianceException & e, std::string text);
	ARegexPatternException(const ARegexPatternException & obj);
	~ARegexPatternException();
};

#endif

