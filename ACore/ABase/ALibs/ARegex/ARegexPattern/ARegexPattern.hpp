#ifndef HPP_AREGEXPATTERN
#define HPP_AREGEXPATTERN

#include <ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.hpp>
#include "ARegexParser/ARegexParser.hpp"
#include "ARegexPart/ARegexPart.hpp"
#include "ARegexPattern.exception.hpp"

class ARegexPattern {
private:
    APtr<AList<APtr<ARegexGroup>>> groups;
    APtr<ARegexGroup> group;
    AString pattern;

    AString insert_refs(AString stirng);

public:
    ARegexPattern();
    ARegexPattern(AString pattern);
    ARegexPattern(const ARegexPattern & pattern);
    void set(AString pattern);
    void compile();

    bool match(AString string);
    bool match(AStringIterator & iterator);

    APtr<AStringList> matches(AString string);
    APtr<AStringList> matches(AStringIterator & iterator);

    ARegexPart matches_part(AString string);
    ARegexPart matches_part(AStringIterator iterator);

    APtr<AStringList> find_first(AString string);

    APtr<AList<APtr<AStringList>>> find_all(AString string);

    AString replace_first(AString string, AString substring);

    AString replace_all(AString string, AString substring);

    AString to_text();

    u64int length();
    AString get(u64int index);

    ~ARegexPattern();
};

#endif