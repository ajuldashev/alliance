#include "ARegexPattern.hpp"

#ifdef TEST_AREGEXPATTERN
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("abc");
      TEST_ALERT(pattern.match("abc"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("abc");
      TEST_ALERT(!pattern.match("abd"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a[abc]c");
      TEST_ALERT(pattern.match("abc"), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a[abc][abc]");
      TEST_ALERT(pattern.match("abc"), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a[b]c");
      TEST_ALERT(pattern.match("abc"), 5)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a[^b]c");
      TEST_ALERT(!pattern.match("abc"), 6)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a[^b]+c");
      TEST_ALERT(pattern.match("a123c"), 7)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(pattern.match("abc"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(pattern.match("aac"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(pattern.match("acc"), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(!pattern.match("aAc"), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(!pattern.match("aBc"), 5)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(a|b|c)c");
      TEST_ALERT(!pattern.match("aCc"), 6)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(13|22|34|45)");
      TEST_ALERT(pattern.match("34"), 7)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\+7(923|913)..[^1-389]9");
      TEST_ALERT(pattern.match("+79238779"), 8)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("((1|2)|(3|4))");
      TEST_ALERT(pattern.match("2"), 9)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(((1|2)|(3|4))((5|6)|(7|8)))");
      TEST_ALERT(pattern.match("27"), 10)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^123$");
      TEST_ALERT(pattern.match("123"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^123$");
      TEST_ALERT(!pattern.match("1234"), 2)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("12?3");
      TEST_ALERT(pattern.match("13"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("12?3");
      TEST_ALERT(pattern.match("123"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("12?3");
      TEST_ALERT(!pattern.match("143"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("1(2222)?3");
      TEST_ALERT(pattern.match("122223"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("1(2222)?3");
      TEST_ALERT(pattern.match("13"), 2)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(5)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab*c$");
      TEST_ALERT(pattern.match("abbbbbbc"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab*(cddd|bcd*)e$");
      TEST_ALERT(pattern.match("abbbbbbcdde"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^a(1|2)*e$");
      TEST_ALERT(pattern.match("a121222121212121212e"), 3)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(6)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab+c$");
      TEST_ALERT(pattern.match("abbbbbbc"), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab+(cddd|bcd+)e$");
      TEST_ALERT(pattern.match("abbbbbbcdde"), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^a+$");
      TEST_ALERT(!pattern.match(""), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^a+$");
      TEST_ALERT(pattern.match("a"), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^a+$");
      TEST_ALERT(pattern.match("aaaaaaaaaaaa"), 5)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab+c$");
      TEST_ALERT(!pattern.match("ac"), 6)
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^ab+c$");
      TEST_ALERT(pattern.match("abc"), 7)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(7)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?=a)b.");
      TEST_ALERT(pattern.match("aba"), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?=a)b");
      TEST_ALERT(!pattern.match("ab"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?=a)..");
      TEST_ALERT(pattern.match("ab"), 3);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".*(?=a).*");
      TEST_ALERT(pattern.match("jnfsdliugrbtnfaglisdfgjsbldf"), 4);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?=a).*");
      TEST_ALERT(!pattern.match("jnfsdliugrbtnfglisdfgjsbldf"), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(8)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?!a)b");
      TEST_ALERT(!pattern.match("aba"), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?!a)b");
      TEST_ALERT(pattern.match("ab"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?!a)..");
      TEST_ALERT(!pattern.match("ab"), 3);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?!a).*");
      TEST_ALERT(!pattern.match("jnfsdliugrbtnfaglisdfgjsbldf"), 4);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".*(?!a).*");
      TEST_ALERT(pattern.match("jnfsdliugrbtnfglisdfgjsbldf"), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(9)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?<=a)b.");
      TEST_ALERT(pattern.match("aba"), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?<=a)ab.");
      TEST_ALERT(!pattern.match("aba"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".b(?<=a)ab");
      TEST_ALERT(pattern.match("abab"), 3);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".b(?<=a)ab");
      TEST_ALERT(!pattern.match("bbab"), 4);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("..(?<=b)ab");
      TEST_ALERT(pattern.match("bcab"), 5);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("..(?<=b$)ab");
      TEST_ALERT(!pattern.match("bcab"), 6);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("..(?<=^b)ab");
      TEST_ALERT(!pattern.match("cbcab"), 7);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("..(?<=^b)ab");
      TEST_ALERT(pattern.match("bcab"), 8);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(10)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("a(?<!a)b");
      TEST_ALERT(!pattern.match("aba"), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?<!a)ab.");
      TEST_ALERT(pattern.match("aba"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".b(?<!a)ab");
      TEST_ALERT(!pattern.match("abab"), 3);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".b(?<!a)ab");
      TEST_ALERT(pattern.match("bbab"), 4);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern(".b(?<!a$)ab");
      TEST_ALERT(pattern.match("bbab"), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(11)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("123|abc|AB(C|F)");
      TEST_ALERT(pattern.match("123"), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("123|abc|AB(C|F)");
      TEST_ALERT(pattern.match("abc"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("123|abc|AB(C|F)");
      TEST_ALERT(pattern.match("ABC"), 3);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("123|abc|AB(C|F)");
      TEST_ALERT(pattern.match("ABF"), 4);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("123|abc|AB(C|F)");
      TEST_ALERT(!pattern.match("AB4"), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(12)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\D\\d\\D");
      TEST_ALERT(pattern.match("F2 "), 1);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\sA\\s\\S");
      TEST_ALERT(pattern.match(" A\t7"), 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\W\\w\\w\\w\\W");
      TEST_ALERT(pattern.match(" H_2 "), 3);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(13)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(?:123)(4(5)6)(789)((ab)c)");
      TEST_ALERT(pattern.match("123456789abc"), 1);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.get(0) == "123456789abc", 2);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.get(1) == "456", 3);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.get(2) == "5", 4);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.get(3) == "789", 5);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.get(4) == "abc", 6);
      /** <doc-test-info>
       *  Тута пока не робит (
       */
      TEST_ALERT(pattern.get(5) == "ab", 7);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(14)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(12(3))\\2\\1");
      TEST_ALERT(pattern.match("1233123"), 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(15)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^9{1,3}$");
      TEST_ALERT(!pattern.match(""), 1);
      TEST_ALERT( pattern.match("9"), 2);
      TEST_ALERT( pattern.match("99"), 3);
      TEST_ALERT( pattern.match("999"), 4);
      TEST_ALERT(!pattern.match("9999"), 5);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^9{2}$");
      TEST_ALERT(!pattern.match(""), 6);
      TEST_ALERT(!pattern.match("9"), 7);
      TEST_ALERT( pattern.match("99"), 8);
      TEST_ALERT(!pattern.match("999"), 9);
      TEST_ALERT(!pattern.match("9999"), 10);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^9{,2}$");
      TEST_ALERT( pattern.match(""), 11);
      TEST_ALERT( pattern.match("9"), 12);
      TEST_ALERT( pattern.match("99"), 13);
      TEST_ALERT(!pattern.match("999"), 14);
      TEST_ALERT(!pattern.match("9999"), 15);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^9{2,}$");
      TEST_ALERT(!pattern.match(""), 16);
      TEST_ALERT(!pattern.match("9"), 17);
      TEST_ALERT( pattern.match("99"), 18);
      TEST_ALERT( pattern.match("999"), 19);
      TEST_ALERT( pattern.match("9999"), 20);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("^9{}$");
      TEST_ALERT( pattern.match(""), 21);
      TEST_ALERT( pattern.match("9"), 22);
      TEST_ALERT( pattern.match("99"), 23);
      TEST_ALERT( pattern.match("999"), 24);
      TEST_ALERT( pattern.match("9999"), 25);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(16)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(((.)(.))((.)(.)))");
      APtr<AStringList> matches = pattern.matches("1234");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches, 1);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->length() == 8, 2);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(0) == "1234", 3);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(1) == "1234", 5);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(2) == "12", 6);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(3) == "1", 7);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(4) == "2", 8);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(5) == "34", 9);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(6) == "3", 10);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(7) == "4", 11);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(17)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\d");
      AString string = "1a2b3c4d5";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "1a2b3c4d5", 1);

      string = pattern.replace_first(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a2b3c4d5", 2);

      string = pattern.replace_first(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b3c4d5", 3);

      string = pattern.replace_first(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b+c4d5", 4);

      string = pattern.replace_first(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b+c+d5", 5);

      string = pattern.replace_first(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b+c+d+", 6);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(18)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\d");
      AString string = "1a2b3c4d5";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "1a2b3c4d5", 1);

      string = pattern.replace_all(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b+c+d+", 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\\d+");
      AString string = "123a2b3908c4d51234";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "123a2b3908c4d51234", 1);

      string = pattern.replace_all(string, "+");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "+a+b+c+d+", 2);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(19)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(\\d)(\\w)");
      AString string = "1a";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "1a", 1);

      string = pattern.replace_all(string, "\\2\\1");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "a1", 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(\\d)(\\w)");
      AString string = "1a2b";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "1a2b", 3);
      string = pattern.replace_all(string, "\\2\\1");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "a1b2", 4);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(20)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("(\\d)(\\w)");
      AString string = "1a2b3c4d5";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "1a2b3c4d5", 1);

      string = pattern.replace_all(string, "\\2\\1");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(string == "a1b2c3d45", 2);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(21)
   {
      ARegexPattern pattern("^$");
      AString string = "";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.match(string), 1);
   }
   {
      ARegexPattern pattern("^a($|\\d)");
      AString string = "a1";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.match(string), 2);
   }
   {
      ARegexPattern pattern("^a($|\\d)");
      AString string = "a";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.match(string), 3);
   }
   {
      ARegexPattern pattern("^a($|\\d)");
      AString string = "af";
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(!pattern.match(string), 4);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(22)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("1234|123");
      APtr<AStringList> matches = pattern.matches("1234");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches, 1);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(0) == "1234", 2);
   }
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("1234|123");
      APtr<AStringList> matches = pattern.matches("123");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches, 3);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(matches->get(0) == "123", 4);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(23)
   {
      /** <doc-test-info>
       * 
       */
      ARegexPattern pattern("\"(([^\"]|(?<=\\\\(\\\\\\\\)*$)\")*)\".*");
      APtr<AStringList> matches = pattern.matches("\"Hello worlds \\\"HI\\\\\\\" \"");
      TEST_ALERT(matches->get(1) == "Hello worlds \\\"HI\\\\\\\" ", 1);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(24)
   {
      ARegexPattern pattern("123");
      ARegexPart part = pattern.matches_part("123456");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_matches()->get(0) == "123", 1);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_start().get_index() == 0, 2);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_finish().get_index() == 3, 3);
      pattern = ARegexPattern("456");
      part = pattern.matches_part(part.get_finish());
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_matches()->get(0) == "456", 4);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_start().get_index() == 3, 5);
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(part.get_finish().get_index() == 6, 6);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(25)
   {
      ARegexPattern pattern("\\*");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.match("*"), 1);
   }
   {
      ARegexPattern pattern("[\\*]");
      /** <doc-test-info>
       * 
       */
      TEST_ALERT(pattern.match("*"), 2);
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexpattern)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)
   TEST_CALL(7)
   TEST_CALL(8)
   TEST_CALL(9)
   TEST_CALL(10)
   TEST_CALL(11)
   TEST_CALL(12)
   TEST_CALL(13)
   TEST_CALL(14)
   TEST_CALL(15)
   TEST_CALL(16)
   TEST_CALL(17)
   TEST_CALL(18)
   TEST_CALL(19)
   TEST_CALL(20)
   TEST_CALL(21)
   TEST_CALL(22)
   TEST_CALL(23)
   TEST_CALL(24)
   TEST_CALL(25)

TEST_FUNCTION_MAIN_END(AREGEXPATTERN)
#endif

