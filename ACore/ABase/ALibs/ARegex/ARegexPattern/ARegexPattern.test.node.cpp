#include "ARegexPattern.hpp"

#ifdef TEST_AREGEXPATTERN
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexpattern(bool is_soft) {
	#ifdef TEST_AREGEXGROUP
	if (!test_aregexgroup(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXGROUP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXNODE
	if (!test_aregexnode(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXNODE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXPARSER
	if (!test_aregexparser(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXPARSER );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXPART
	if (!test_aregexpart(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXPART );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXREFS
	if (!test_aregexrefs(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXREFS );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOL
	if (!test_aregexsymbol(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOL );
		return is_soft;
	}
	#endif
	return true;
}
#endif
