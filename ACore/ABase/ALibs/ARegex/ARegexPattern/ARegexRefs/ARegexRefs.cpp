#include "ARegexRefs.hpp"
#include "ARegexRefs.test.cpp"
#include "ARegexRefs.test.node.cpp"
#include "ARegexRefs.debug.cpp"
#include "ARegexRefs.debug.node.cpp"
#include "ARegexRefs.exception.cpp"

ARegexRefs::ARegexRefs() {
    index = 0;
}

AStringIterator ARegexRefs::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        APtr<ARegexGroup> group = refs->get(index);
        
        AString substring = group->get_result();

        APtr<ARegexGroup> local_group = parse_string(substring);
        
        iterator = local_group->execute(iterator);

        is_complated = local_group->is_complate();

        if (!is_complated) {
            return iterator;
        }

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexRefsException(e, "execute(AStringIterator iterator)");
    }
}

APtr<ARegexGroup> ARegexRefs::parse_string(AString string) {
    AStringIterator iterator(string);
    return get_next_symbol(iterator);
}

APtr<ARegexGroup> ARegexRefs::get_next_symbol(AStringIterator iterator) {
    try {

        APtr<ARegexGroup> group;
        if (!iterator.can_get()) {
            return group;
        }
        APtr<ARegexSymbol> symbol = APtr<ARegexSymbol>::make();
        
        symbol->set_symbol(iterator.get());

        iterator.next();
        symbol->set_next(get_next_symbol(iterator));

        group = aptr_cast<ARegexSymbol, ARegexGroup>(symbol);

        return group;
    } catch (AllianceException & e) {
        throw ARegexRefsException(e, "get_next_symbol(AStringIterator iterator)");
    }
}

bool ARegexRefs::can_change_behavior() {
    return false; 
}

void ARegexRefs::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

void ARegexRefs::set_refs(APtr<AList<APtr<ARegexGroup>>> refs) {
    this->refs = refs;
}

void ARegexRefs::set_index(u64int index) {
    this->index = index;
}

AString ARegexRefs::to_text() {
    AString text = "\\";
    text += AStringLibs::to_string(index);
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexRefs::~ARegexRefs() {

}