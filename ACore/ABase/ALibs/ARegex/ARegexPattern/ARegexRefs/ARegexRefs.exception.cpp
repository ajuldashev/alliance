#include "ARegexRefs.exception.hpp"

ARegexRefsException::ARegexRefsException() : AllianceException() {
	exception = "Alliance-Exception-ARegexRefs";
}

ARegexRefsException::ARegexRefsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexRefs");
}

ARegexRefsException::ARegexRefsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexRefs");
}

ARegexRefsException::ARegexRefsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexRefs");
}

ARegexRefsException::ARegexRefsException(const ARegexRefsException & obj) : AllianceException(obj) {
	
}

ARegexRefsException::~ARegexRefsException() {

}

