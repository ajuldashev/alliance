#ifndef HPP_AREGEXREFSEXCEPTION
#define HPP_AREGEXREFSEXCEPTION
#include <alliance.exception.hpp>

class ARegexRefsException : public AllianceException {
public:
	ARegexRefsException();
	ARegexRefsException(std::string text);
	ARegexRefsException(AllianceException & e);
	ARegexRefsException(AllianceException & e, std::string text);
	ARegexRefsException(const ARegexRefsException & obj);
	~ARegexRefsException();
};

#endif

