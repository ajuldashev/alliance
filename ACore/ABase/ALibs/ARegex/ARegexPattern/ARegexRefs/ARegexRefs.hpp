#ifndef HPP_AREGEXREFS
#define HPP_AREGEXREFS

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include "ARegexRefs.exception.hpp"

class ARegexRefs : public ARegexGroup {
private:
    APtrWeak<AList<APtr<ARegexGroup>>> refs;
    u64int index;
public:
    ARegexRefs();
    AStringIterator execute(AStringIterator iterator);
    APtr<ARegexGroup> parse_string(AString string);
    APtr<ARegexGroup> get_next_symbol(AStringIterator iterator);
    bool can_change_behavior();
    void reset_behavior();
    bool is_complate();
    void set_refs(APtr<AList<APtr<ARegexGroup>>> refs);
    void set_index(u64int index);
    AString to_text();
    ~ARegexRefs();
};

#endif