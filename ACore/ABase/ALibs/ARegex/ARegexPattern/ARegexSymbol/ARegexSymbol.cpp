#include "ARegexSymbol.hpp"
#include "ARegexSymbol.test.cpp"
#include "ARegexSymbol.test.node.cpp"
#include "ARegexSymbol.debug.cpp"
#include "ARegexSymbol.debug.node.cpp"
#include "ARegexSymbol.exception.cpp"

ARegexSymbol::ARegexSymbol() {

}

AStringIterator ARegexSymbol::execute(AStringIterator iterator) {
    try {
        is_complated = false;

        if (!iterator.can_get()) {
            return iterator;
        }   
        AStringIterator local_iterator = iterator;
        if (symbol != local_iterator.get()) {
            return iterator;
        }

        local_iterator.next();
        is_complated = true;
        
        if (next) {
            local_iterator = next->execute(local_iterator);
            is_complated = is_complated && next->is_complate();
        }

        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbol::set_symbol(ASymbol symbol) {
    this->symbol = symbol;
}

AString ARegexSymbol::to_text() {
    AString text = symbol.to_string();
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbol::~ARegexSymbol() {

}