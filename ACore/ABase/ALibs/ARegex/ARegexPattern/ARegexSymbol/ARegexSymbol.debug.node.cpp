#include "ARegexSymbol.hpp"

#ifdef DEBUG_AREGEXSYMBOL
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexsymbol(AString com) {
	#ifdef DEBUG_AREGEXSYMBOLANY
	if (com == "aregexsymbolany") {
		if (debug_aregexsymbolany()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLCLASS
	if (com == "aregexsymbolclass") {
		if (debug_aregexsymbolclass()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexsymbol() {
	AString text;
	#ifdef DEBUG_AREGEXSYMBOLANY
	text += "d) aregexsymbolany\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLCLASS
	text += "d) aregexsymbolclass\n";
	#endif
	return text;
}

#endif
