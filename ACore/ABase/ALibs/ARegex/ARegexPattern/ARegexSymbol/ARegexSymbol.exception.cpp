#include "ARegexSymbol.exception.hpp"

ARegexSymbolException::ARegexSymbolException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbol";
}

ARegexSymbolException::ARegexSymbolException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbol");
}

ARegexSymbolException::ARegexSymbolException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbol");
}

ARegexSymbolException::ARegexSymbolException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbol");
}

ARegexSymbolException::ARegexSymbolException(const ARegexSymbolException & obj) : AllianceException(obj) {
	
}

ARegexSymbolException::~ARegexSymbolException() {

}

