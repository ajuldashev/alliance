#ifndef HPP_AREGEXSYMBOLEXCEPTION
#define HPP_AREGEXSYMBOLEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolException : public AllianceException {
public:
	ARegexSymbolException();
	ARegexSymbolException(std::string text);
	ARegexSymbolException(AllianceException & e);
	ARegexSymbolException(AllianceException & e, std::string text);
	ARegexSymbolException(const ARegexSymbolException & obj);
	~ARegexSymbolException();
};

#endif

