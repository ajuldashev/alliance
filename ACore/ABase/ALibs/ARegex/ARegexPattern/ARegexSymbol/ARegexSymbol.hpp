#ifndef HPP_AREGEXSYMBOL
#define HPP_AREGEXSYMBOL
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp>
#include <ACore/ABase/ATypes/ASymbol/ASymbol.hpp>
#include "ARegexSymbol.exception.hpp"

class ARegexSymbol : public ARegexGroup {
private:
    ASymbol symbol;
public:
    ARegexSymbol();
    AStringIterator execute(AStringIterator iterator);
    void set_symbol(ASymbol symbol);
    AString to_text();
    ~ARegexSymbol();
};

#endif