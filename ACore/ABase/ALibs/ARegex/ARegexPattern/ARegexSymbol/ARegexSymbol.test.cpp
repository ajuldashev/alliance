#include "ARegexSymbol.hpp"

#ifdef TEST_AREGEXSYMBOL
#include <ACore/ABase/AIO/AIO.hpp>


/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * Сравниваем 'a' [a]
       */
      APtr<ARegexSymbol> ptr_symbol = APtr<ARegexSymbol>::make();
      ptr_symbol->set_symbol('a');
      AString a = "a";
      AStringIterator iterator_a(a);
      ptr_symbol->execute(iterator_a);
      TEST_ALERT(ptr_symbol->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info>
       * Сравниваем 'b' [a]
       */
      APtr<ARegexSymbol> ptr_symbol = APtr<ARegexSymbol>::make();
      ptr_symbol->set_symbol('b');
      AString a = "a";
      AStringIterator iterator_a(a);
      ptr_symbol->execute(iterator_a);
      TEST_ALERT(!ptr_symbol->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info>
       * Сравниваем 'ba' [ab]
       */
      APtr<ARegexSymbol> ptr_symbol_1 = APtr<ARegexSymbol>::make();
      ptr_symbol_1->set_symbol('a');
      APtr<ARegexSymbol> ptr_symbol_2 = APtr<ARegexSymbol>::make();
      ptr_symbol_2->set_symbol('b'); 
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbol, ARegexGroup>(ptr_symbol_1));
      AString ab = "ab";
      AStringIterator iterator_ab(ab);
      ptr_symbol_2->execute(iterator_ab);
      TEST_ALERT(!ptr_symbol_2->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info>
       * Сравниваем 'ab' [ab]
       */
      APtr<ARegexSymbol> ptr_symbol_1 = APtr<ARegexSymbol>::make();
      ptr_symbol_1->set_symbol('b');
      APtr<ARegexSymbol> ptr_symbol_2 = APtr<ARegexSymbol>::make();
      ptr_symbol_2->set_symbol('a');
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbol, ARegexGroup>(ptr_symbol_1));
      AString ab = "ab";
      AStringIterator iterator_ab(ab);
      ptr_symbol_2->execute(iterator_ab);
      TEST_ALERT(ptr_symbol_2->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(5)
   {
      APtr<ARegexSymbol> ptr_symbol_1 = APtr<ARegexSymbol>::make();
      ptr_symbol_1->set_symbol('b');
      APtr<ARegexSymbol> ptr_symbol_2 = APtr<ARegexSymbol>::make();
      ptr_symbol_2->set_symbol('a');
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbol, ARegexGroup>(ptr_symbol_1));
      AString abb = "abb";
      /** <doc-test-info>
       * Сравниваем 'ab' [ab]b
       */  
      AStringIterator iterator_abb(abb);
      ptr_symbol_2->execute(iterator_abb);
      TEST_ALERT(ptr_symbol_2->is_complate(), 1)
      /** <doc-test-info>
       * Сравниваем 'ab' a[bb]
       */
      iterator_abb.next();
      ptr_symbol_2->execute(iterator_abb);
      TEST_ALERT(!ptr_symbol_2->is_complate(), 2)
      /** <doc-test-info>
       * Сравниваем 'ab' ab[b ]
       */
      iterator_abb.next();
      ptr_symbol_2->execute(iterator_abb);
      TEST_ALERT(!ptr_symbol_2->is_complate(), 3)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(6)
   {
      APtr<ARegexSymbol> ptr_symbol_1 = APtr<ARegexSymbol>::make();
      ptr_symbol_1->set_symbol('c');
      APtr<ARegexSymbol> ptr_symbol_2 = APtr<ARegexSymbol>::make();
      ptr_symbol_2->set_symbol('b');
      APtr<ARegexSymbol> ptr_symbol_3 = APtr<ARegexSymbol>::make();
      ptr_symbol_3->set_symbol('a');

      ptr_symbol_3->set_next(aptr_cast<ARegexSymbol, ARegexGroup>(ptr_symbol_2));
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbol, ARegexGroup>(ptr_symbol_1));
      
      AString abc = "abc";

      AStringIterator iterator_abc(abc);

      /** <doc-test-info>
       * Сравниваем 'abc' [adc]
       */
      ptr_symbol_3->execute(iterator_abc);
      TEST_ALERT(ptr_symbol_3->is_complate(), 1)
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexsymbol)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOL)

#endif

