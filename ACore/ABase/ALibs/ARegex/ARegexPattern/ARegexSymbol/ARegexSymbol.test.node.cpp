#include "ARegexSymbol.hpp"

#ifdef TEST_AREGEXSYMBOL
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexsymbol(bool is_soft) {
	#ifdef TEST_AREGEXSYMBOLANY
	if (!test_aregexsymbolany(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLANY );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLCLASS
	if (!test_aregexsymbolclass(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLCLASS );
		return is_soft;
	}
	#endif
	return true;
}
#endif
