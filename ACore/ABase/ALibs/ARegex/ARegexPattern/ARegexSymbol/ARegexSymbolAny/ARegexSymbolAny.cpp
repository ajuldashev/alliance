#include "ARegexSymbolAny.hpp"
#include "ARegexSymbolAny.test.cpp"
#include "ARegexSymbolAny.test.node.cpp"
#include "ARegexSymbolAny.debug.cpp"
#include "ARegexSymbolAny.debug.node.cpp"
#include "ARegexSymbolAny.exception.cpp"

ARegexSymbolAny::ARegexSymbolAny() {

}

ARegexSymbolAny::ARegexSymbolAny(APtr<ARegexGroup> next) {
    is_complated = false;
    this->next = next;
}

AStringIterator ARegexSymbolAny::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        AStringIterator local_iterator = iterator;
        local_iterator.next();
        is_complated = true;

        if (next) {
            local_iterator = next->execute(local_iterator);
            is_complated = is_complated && next->is_complate();
        }

        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolAnyException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolAny::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolAny::to_text() {
    AString text = ".";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolAny::~ARegexSymbolAny() {

}
