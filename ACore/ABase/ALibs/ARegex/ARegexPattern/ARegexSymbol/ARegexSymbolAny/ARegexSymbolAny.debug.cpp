#include "ARegexSymbolAny.hpp"

#ifdef DEBUG_AREGEXSYMBOLANY
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aregexsymbolany() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AREGEXSYMBOLANY );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aregexsymbolany());
            AIO::writeln("1) help");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aregexsymbolany(com)) {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AREGEXSYMBOLANY );
    AIO::write_div_line();
    return false;
}
#endif

