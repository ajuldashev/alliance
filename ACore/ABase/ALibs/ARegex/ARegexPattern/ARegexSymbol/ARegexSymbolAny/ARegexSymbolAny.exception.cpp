#include "ARegexSymbolAny.exception.hpp"

ARegexSymbolAnyException::ARegexSymbolAnyException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolAny";
}

ARegexSymbolAnyException::ARegexSymbolAnyException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolAny");
}

ARegexSymbolAnyException::ARegexSymbolAnyException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolAny");
}

ARegexSymbolAnyException::ARegexSymbolAnyException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolAny");
}

ARegexSymbolAnyException::ARegexSymbolAnyException(const ARegexSymbolAnyException & obj) : AllianceException(obj) {
	
}

ARegexSymbolAnyException::~ARegexSymbolAnyException() {

}

