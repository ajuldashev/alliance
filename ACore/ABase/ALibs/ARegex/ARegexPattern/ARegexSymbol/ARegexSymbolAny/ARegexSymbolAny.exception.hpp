#ifndef HPP_AREGEXSYMBOLANYEXCEPTION
#define HPP_AREGEXSYMBOLANYEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolAnyException : public AllianceException {
public:
	ARegexSymbolAnyException();
	ARegexSymbolAnyException(std::string text);
	ARegexSymbolAnyException(AllianceException & e);
	ARegexSymbolAnyException(AllianceException & e, std::string text);
	ARegexSymbolAnyException(const ARegexSymbolAnyException & obj);
	~ARegexSymbolAnyException();
};

#endif

