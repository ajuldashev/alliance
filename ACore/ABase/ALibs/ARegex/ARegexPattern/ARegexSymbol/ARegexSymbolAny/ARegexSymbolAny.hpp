#ifndef HPP_AREGEXSYMBOLANY
#define HPP_AREGEXSYMBOLANY
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.hpp>
#include "ARegexSymbolAny.exception.hpp"

class ARegexSymbolAny : public ARegexSymbol {
public:
    ARegexSymbolAny();
    ARegexSymbolAny(APtr<ARegexGroup> next);
    AStringIterator execute(AStringIterator iterator);
    void reset_behavior();
    AString to_text();
    ~ARegexSymbolAny();
};

#endif