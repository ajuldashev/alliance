#include "ARegexSymbolAny.hpp"

#ifdef TEST_AREGEXSYMBOLANY
#include <ACore/ABase/AIO/AIO.hpp>
/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * Сравниваем '.' []
       */
      APtr<ARegexSymbolAny> ptr_symbol = APtr<ARegexSymbolAny>::make();
      AStringIterator iterator;
      ptr_symbol->execute(iterator);
      TEST_ALERT(!ptr_symbol->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info>
       * Сравниваем '.' [a]
       */
      APtr<ARegexSymbolAny> ptr_symbol = APtr<ARegexSymbolAny>::make();
      AString a = "a";
      AStringIterator iterator_a(a);
      ptr_symbol->execute(iterator_a);
      TEST_ALERT(ptr_symbol->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info>
       * Сравниваем '..' [ab]
       */
      APtr<ARegexSymbolAny> ptr_symbol_1 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_2 = APtr<ARegexSymbolAny>::make();
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_1));
      AString ab = "ab";
      AStringIterator iterator_ab(ab);
      ptr_symbol_2->execute(iterator_ab);
      TEST_ALERT(ptr_symbol_2->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info>
       * Сравниваем '..' [ba]
       */
      APtr<ARegexSymbolAny> ptr_symbol_1 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_2 = APtr<ARegexSymbolAny>::make();
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_1));
      AString ab = "ba";
      AStringIterator iterator_ab(ab);
      ptr_symbol_2->execute(iterator_ab);
      TEST_ALERT(ptr_symbol_2->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(5)
   {
      /** <doc-test-info>
       * Сравниваем '...' [abc]
       */
      APtr<ARegexSymbolAny> ptr_symbol_1 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_2 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_3 = APtr<ARegexSymbolAny>::make();
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_1));
      ptr_symbol_3->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_2));
      AString abc = "abc";
      AStringIterator iterator_abc(abc);
      ptr_symbol_3->execute(iterator_abc);
      TEST_ALERT(ptr_symbol_3->is_complate(), 1)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(6)
   {
      /** <doc-test-info>
       * Сравниваем '...' [ab]
       */
      APtr<ARegexSymbolAny> ptr_symbol_1 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_2 = APtr<ARegexSymbolAny>::make();
      APtr<ARegexSymbolAny> ptr_symbol_3 = APtr<ARegexSymbolAny>::make();
      ptr_symbol_2->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_1));
      ptr_symbol_3->set_next(aptr_cast<ARegexSymbolAny, ARegexGroup>(ptr_symbol_2));
      AString ab = "ab";
      AStringIterator iterator_ab(ab);
      ptr_symbol_3->execute(iterator_ab);
      TEST_ALERT(!ptr_symbol_3->is_complate(), 1)
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexsymbolany)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLANY)
#endif

