#include "ARegexSymbolClass.hpp"
#include "ARegexSymbolClass.test.cpp"
#include "ARegexSymbolClass.test.node.cpp"
#include "ARegexSymbolClass.debug.cpp"
#include "ARegexSymbolClass.debug.node.cpp"
#include "ARegexSymbolClass.exception.cpp"


ARegexSymbolClass::Interval::Interval() {
    symbol_left = '\0';
    symbol_right = '\0';
}

ARegexSymbolClass::Interval::Interval(ASymbol symbol_left, ASymbol symbol_right) {
    set(symbol_left, symbol_right);
}

void ARegexSymbolClass::Interval::set(ASymbol symbol_left, ASymbol symbol_right) {
    this->symbol_left = symbol_left;
    this->symbol_right = symbol_right;
}

bool ARegexSymbolClass::Interval::has(ASymbol symbol) {
    return symbol_left <= symbol && symbol <= symbol_right;
}

bool ARegexSymbolClass::Interval::validate(ASymbol symbol_left, ASymbol symbol_right) {
    return symbol_left <= symbol_right;
}

ASymbol ARegexSymbolClass::Interval::get_right() {
    return symbol_right;
}

ASymbol ARegexSymbolClass::Interval::get_left() {
    return symbol_left;
}

ARegexSymbolClass::Interval::~Interval() {
    
}

ARegexSymbolClass::ARegexSymbolClass() {
    inverse = false;
    is_complated = false;
}

ARegexSymbolClass::ARegexSymbolClass(AString string) {
    try {
        parse(string);
        is_complated = false;
    } catch (AllianceException & e) {
        throw ARegexSymbolClassException(e, "ARegexSymbolClass(AString string)");
    }
}

ARegexSymbolClass::ARegexSymbolClass(AString string, APtr<ARegexGroup> next) {
    try {
        parse(string);
        is_complated = false;
        this->next = next;
    } catch (AllianceException & e) {
        throw ARegexSymbolClassException(e, "ARegexSymbolClass(AString string, APtr<ARegexSymbol> next)");
    }
}

AStringIterator ARegexSymbolClass::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        AStringIterator local_iterator = iterator;

        if (!local_iterator.can_get()) {
            return iterator;
        }   

        bool has_symbol = has(local_iterator.get());
        
        if (((inverse)? has_symbol : !has_symbol)) {
            return iterator; 
        }

        is_complated = true;
        local_iterator.next();

        if (next) {
            local_iterator = next->execute(local_iterator);
            is_complated = is_complated && next->is_complate();
        }

        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolClassException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolClass::set_class(AString string) {
    try {
        parse(string);
        is_complated = false;
    } catch (AllianceException & e) {
        throw ARegexSymbolClassException(e, "set_class(AString string)");
    }
}

AStringIterator ARegexSymbolClass::is_inverse(AStringIterator iterator) {
    AStringIterator local_iterator = iterator;
    if (local_iterator.get() == '^') {
        inverse = true;
        local_iterator.next();
        return local_iterator;
    }
    inverse = false;
    return iterator;
}

ASymbol ARegexSymbolClass::get_next_symbol(AStringIterator iterator) {
    if (iterator.get() == '\\') {
        iterator.next();
        if (!iterator.can_get()) {
            throw ARegexSymbolClassException("get_next_symbol(AStringIterator iterator)");
        }
        ASymbol local_symbol = iterator.get();
        if (local_symbol == 't') {
            return '\t';
        }
        if (local_symbol == 'n') {
            return '\n';
        }
        if (local_symbol == 'r') {
            return '\r';
        }
        if (local_symbol == '[') {
            return '[';
        }
        if (local_symbol == ']') {
            return ']';
        }
        if (local_symbol == '\\') {
            return '\\';
        }
        if (local_symbol == '-') {
            return '-';
        }
        if (local_symbol == '^') {
            return '^';
        }
    }
    return iterator.get();
}

AStringIterator ARegexSymbolClass::next_interval(AStringIterator iterator) {
    try {
        AStringIterator local_iterator = iterator;
        ASymbol symbol = get_next_symbol(local_iterator);
        
        local_iterator.next();

        if (!local_iterator.can_get()) {
            Interval interval(symbol, symbol);
            intervals.push_end(interval);
            return local_iterator;
        }
        
        if (local_iterator.get() != '-') {
            Interval interval(symbol, symbol);
            intervals.push_end(interval);
            return iterator;
        }
        
        if (!local_iterator.can_get()) {
            throw ARegexSymbolClassException("next_interval(AStringIterator iterator) : Not found symbol after '-' ");
        }
        
        local_iterator.next();
        ASymbol symbol_2 = get_next_symbol(local_iterator);
        
        if (!Interval::validate(symbol, symbol_2)) {
            throw ARegexSymbolClassException("next_interval(AStringIterator iterator) : validation fail");
        }

        Interval interval(symbol, symbol_2);
        intervals.push_end(interval);
    
        return local_iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolClassException(e, "next_interval(AStringIterator iterator)");
    }
}

void ARegexSymbolClass::parse(AString string) {
    AStringIterator iterator(string);
    if (iterator.can_get()) {
        iterator = is_inverse(iterator);
        while (iterator.can_get()) {
            iterator = next_interval(iterator);
            iterator.next();
        }
    }    
}

bool ARegexSymbolClass::has(ASymbol symbol) {
    if (intervals.length() == 0) {
        return false;
    }
    for (u64int i = 0; i < intervals.length(); i += 1) {
        if (intervals.get(i).has(symbol)) {
            return true;
        }
    }
    return false;
}

void ARegexSymbolClass::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

void ARegexSymbolClass::clear() {
    intervals.clear();
}

AString ARegexSymbolClass::to_text() {
    AString text = "[";
    if (inverse) {
        text += "^";
    }
    for (u64int  i = 0; i < intervals.length(); i += 1) {
        Interval interval = intervals.get(i);
        ASymbol symbol_right = interval.get_right();
        ASymbol symbol_left = interval.get_left();
        if (symbol_right == symbol_left) {
            text += symbol_right.to_string();
        } else {
            text += symbol_left.to_string();
            text += "-";
            text += symbol_right.to_string();
        }
    }
    text += "]";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolClass::~ARegexSymbolClass() {
  
}