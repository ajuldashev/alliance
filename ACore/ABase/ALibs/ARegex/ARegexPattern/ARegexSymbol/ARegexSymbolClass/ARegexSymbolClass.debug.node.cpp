#include "ARegexSymbolClass.hpp"

#ifdef DEBUG_AREGEXSYMBOLCLASS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aregexsymbolclass(AString com) {
	#ifdef DEBUG_AREGEXSYMBOLDIGITAL
	if (com == "aregexsymboldigital") {
		if (debug_aregexsymboldigital()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTDIGITAL
	if (com == "aregexsymbolnotdigital") {
		if (debug_aregexsymbolnotdigital()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTSPACE
	if (com == "aregexsymbolnotspace") {
		if (debug_aregexsymbolnotspace()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTWORD
	if (com == "aregexsymbolnotword") {
		if (debug_aregexsymbolnotword()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLSPACE
	if (com == "aregexsymbolspace") {
		if (debug_aregexsymbolspace()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AREGEXSYMBOLWORD
	if (com == "aregexsymbolword") {
		if (debug_aregexsymbolword()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aregexsymbolclass() {
	AString text;
	#ifdef DEBUG_AREGEXSYMBOLDIGITAL
	text += "d) aregexsymboldigital\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTDIGITAL
	text += "d) aregexsymbolnotdigital\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTSPACE
	text += "d) aregexsymbolnotspace\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLNOTWORD
	text += "d) aregexsymbolnotword\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLSPACE
	text += "d) aregexsymbolspace\n";
	#endif
	#ifdef DEBUG_AREGEXSYMBOLWORD
	text += "d) aregexsymbolword\n";
	#endif
	return text;
}

#endif
