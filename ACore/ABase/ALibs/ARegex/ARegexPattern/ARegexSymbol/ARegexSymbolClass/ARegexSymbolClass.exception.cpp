#include "ARegexSymbolClass.exception.hpp"

ARegexSymbolClassException::ARegexSymbolClassException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolClass";
}

ARegexSymbolClassException::ARegexSymbolClassException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolClass");
}

ARegexSymbolClassException::ARegexSymbolClassException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolClass");
}

ARegexSymbolClassException::ARegexSymbolClassException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolClass");
}

ARegexSymbolClassException::ARegexSymbolClassException(const ARegexSymbolClassException & obj) : AllianceException(obj) {
	
}

ARegexSymbolClassException::~ARegexSymbolClassException() {

}

