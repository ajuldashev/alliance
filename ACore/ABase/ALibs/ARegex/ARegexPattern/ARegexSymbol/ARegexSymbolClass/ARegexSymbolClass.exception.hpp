#ifndef HPP_AREGEXSYMBOLCLASSEXCEPTION
#define HPP_AREGEXSYMBOLCLASSEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolClassException : public AllianceException {
public:
	ARegexSymbolClassException();
	ARegexSymbolClassException(std::string text);
	ARegexSymbolClassException(AllianceException & e);
	ARegexSymbolClassException(AllianceException & e, std::string text);
	ARegexSymbolClassException(const ARegexSymbolClassException & obj);
	~ARegexSymbolClassException();
};

#endif

