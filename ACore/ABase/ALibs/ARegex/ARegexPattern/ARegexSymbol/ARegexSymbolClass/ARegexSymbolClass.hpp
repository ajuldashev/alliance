#ifndef HPP_AREGEXSYMBOLCLASS
#define HPP_AREGEXSYMBOLCLASS
#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include "ARegexSymbolClass.exception.hpp"

class ARegexSymbolClass : public ARegexGroup {
private:
    class Interval;
    AList<Interval> intervals;
    bool inverse;

    AStringIterator next_interval(AStringIterator iterator);
    AStringIterator is_inverse(AStringIterator iterator);
    ASymbol get_next_symbol(AStringIterator iterator);

    bool has(ASymbol symbol);

public:
    ARegexSymbolClass();
    ARegexSymbolClass(AString string);
    ARegexSymbolClass(AString string, APtr<ARegexGroup> next);
    
    AStringIterator execute(AStringIterator iterator);
    
    void set_class(AString string);
    
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolClass();
};

class ARegexSymbolClass::Interval {
private:
    ASymbol symbol_left;
    ASymbol symbol_right;
public:
    Interval();
    Interval(ASymbol symbol_left, ASymbol symbol_right);

    void set(ASymbol symbol_left, ASymbol symbol_right);

    bool has(ASymbol symbol);

    static bool validate(ASymbol symbol_left, ASymbol symbol_right);

    ASymbol get_right();
    ASymbol get_left();

    ~Interval();
};

#endif