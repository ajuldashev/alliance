#include "ARegexSymbolClass.hpp"

#ifdef TEST_AREGEXSYMBOLCLASS
#include <ACore/ABase/AIO/AIO.hpp>
/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * символ 'a' входит в класс [abc]
       */
      AString string = "a";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("abc");
      symbol_class.execute(iterator);

      TEST_ALERT(symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'b' входит в класс [abc]
       */
      AString string = "b";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 2);
   }
   {
      /** <doc-test-info>
       * символ 'c входит в класс [abc]
       */
      AString string = "c";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 3);
   }
   {
      /** <doc-test-info>
       * символ '1' не входит в класс [abc]
       */
      AString string = "1";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 4);
   }
   {
      /** <doc-test-info>
       * символ 'A' не входит в класс [abc]
       */
      AString string = "A";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info>
       * символ 'a' входит в класс [^abc]
       */
      AString string = "a";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^abc");

      symbol_class.execute(iterator);

      TEST_ALERT(!symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'b' входит в класс [^abc]
       */
      AString string = "b";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 2);
   }
   {
      /** <doc-test-info>
       * символ 'c входит в класс [^abc]
       */
      AString string = "c";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 3);
   }
   {
      /** <doc-test-info>
       * символ '1' не входит в класс [^abc]
       */
      AString string = "1";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 4);
   }
   {
      /** <doc-test-info>
       * символ 'A' не входит в класс [^abc]
       */
      AString string = "A";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^abc");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 5);
   }
TEST_FUNCTION_END


/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info>
       * символ 'B' входит в класс [A-Da-d0-4]
       */
      AString string = "B";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);

      TEST_ALERT(symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'b' входит в класс [A-Da-d0-4]
       */
      AString string = "b";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 2);
   }
   {
      /** <doc-test-info>
       * символ '3 входит в класс [A-Da-d0-4]
       */
      AString string = "3";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 3);
   }
   {
      /** <doc-test-info>
       * символ 'F' не входит в класс [A-Da-d0-4]
       */
      AString string = "F";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 4);
   }
   {
      /** <doc-test-info>
       * символ 'f' не входит в класс [A-Da-d0-4]
       */
      AString string = "f";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 5);
   }
   {
      /** <doc-test-info>
       * символ '9' не входит в класс [A-Da-d0-4]
       */
      AString string = "9";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info>
       * символ 'B' входит в класс [^A-Da-d0-4]
       */
      AString string = "B";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);

      TEST_ALERT(!symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'b' входит в класс [^A-Da-d0-4]
       */
      AString string = "b";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 2);
   }
   {
      /** <doc-test-info>
       * символ '3 входит в класс [^A-Da-d0-4]
       */
      AString string = "3";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(!symbol_class.is_complate(), 3);
   }
   {
      /** <doc-test-info>
       * символ 'F' не входит в класс [^A-Da-d0-4]
       */
      AString string = "F";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 4);
   }
   {
      /** <doc-test-info>
       * символ 'f' не входит в класс [^A-Da-d0-4]
       */
      AString string = "f";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 5);
   }
   {
      /** <doc-test-info>
       * символ '9' не входит в класс [^A-Da-d0-4]
       */
      AString string = "9";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("^A-Da-d0-4");

      symbol_class.execute(iterator);
      
      TEST_ALERT(symbol_class.is_complate(), 5);
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test exceute(AStringIterator iterator)
 */
TEST_FUNCTION_BEGIN(5)
   {
      /** <doc-test-info>
       * символ 'B' входит в класс [A-D\\-^]
       */
      AString string = "B";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-D\\-^");

      symbol_class.execute(iterator);

      TEST_ALERT(symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'B' входит в класс [A-D\\-^]
       */
      AString string = "-";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-D\\-^");

      symbol_class.execute(iterator);

      TEST_ALERT(symbol_class.is_complate(), 1);
   }
   {
      /** <doc-test-info>
       * символ 'B' входит в класс [A-D\\-^]
       */
      AString string = "^";
      AStringIterator iterator(string);
      ARegexSymbolClass symbol_class("A-D\\-^");

      symbol_class.execute(iterator);

      TEST_ALERT(symbol_class.is_complate(), 1);
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexsymbolclass)

   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLCLASS)

#endif

