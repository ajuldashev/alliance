#include "ARegexSymbolClass.hpp"

#ifdef TEST_AREGEXSYMBOLCLASS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aregexsymbolclass(bool is_soft) {
	#ifdef TEST_AREGEXSYMBOLDIGITAL
	if (!test_aregexsymboldigital(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLDIGITAL );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLNOTDIGITAL
	if (!test_aregexsymbolnotdigital(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLNOTDIGITAL );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLNOTSPACE
	if (!test_aregexsymbolnotspace(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLNOTSPACE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLNOTWORD
	if (!test_aregexsymbolnotword(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLNOTWORD );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLSPACE
	if (!test_aregexsymbolspace(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLSPACE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AREGEXSYMBOLWORD
	if (!test_aregexsymbolword(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AREGEXSYMBOLWORD );
		return is_soft;
	}
	#endif
	return true;
}
#endif
