#include "ARegexSymbolDigital.hpp"
#include "ARegexSymbolDigital.test.cpp"
#include "ARegexSymbolDigital.test.node.cpp"
#include "ARegexSymbolDigital.debug.cpp"
#include "ARegexSymbolDigital.debug.node.cpp"
#include "ARegexSymbolDigital.exception.cpp"


ARegexSymbolDigital::ARegexSymbolDigital() {
    is_complated = false;
    symbol_class.set_class("0-9");
}

ARegexSymbolDigital::ARegexSymbolDigital(const ARegexSymbolDigital & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolDigital::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolDigitalException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolDigital::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolDigital::to_text() {
    AString text = "\\d";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolDigital::~ARegexSymbolDigital() {
  
}