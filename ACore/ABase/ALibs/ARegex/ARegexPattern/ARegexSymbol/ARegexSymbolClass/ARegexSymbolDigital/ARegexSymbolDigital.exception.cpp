#include "ARegexSymbolDigital.exception.hpp"

ARegexSymbolDigitalException::ARegexSymbolDigitalException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolDigital";
}

ARegexSymbolDigitalException::ARegexSymbolDigitalException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolDigital");
}

ARegexSymbolDigitalException::ARegexSymbolDigitalException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolDigital");
}

ARegexSymbolDigitalException::ARegexSymbolDigitalException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolDigital");
}

ARegexSymbolDigitalException::ARegexSymbolDigitalException(const ARegexSymbolDigitalException & obj) : AllianceException(obj) {
	
}

ARegexSymbolDigitalException::~ARegexSymbolDigitalException() {

}

