#ifndef HPP_AREGEXSYMBOLDIGITALEXCEPTION
#define HPP_AREGEXSYMBOLDIGITALEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolDigitalException : public AllianceException {
public:
	ARegexSymbolDigitalException();
	ARegexSymbolDigitalException(std::string text);
	ARegexSymbolDigitalException(AllianceException & e);
	ARegexSymbolDigitalException(AllianceException & e, std::string text);
	ARegexSymbolDigitalException(const ARegexSymbolDigitalException & obj);
	~ARegexSymbolDigitalException();
};

#endif

