#ifndef HPP_AREGEXSYMBOLDIGITAL
#define HPP_AREGEXSYMBOLDIGITAL

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolDigital.exception.hpp"

class ARegexSymbolDigital : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolDigital();
    ARegexSymbolDigital(const ARegexSymbolDigital & symbol);
    
    AStringIterator execute(AStringIterator iterator);
        
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolDigital();
};

#endif