#include "ARegexSymbolDigital.hpp"

#ifdef TEST_AREGEXSYMBOLDIGITAL
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolDigital> digital = APtr<ARegexSymbolDigital>::make();
      AString string = "0";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolDigital> digital = APtr<ARegexSymbolDigital>::make();
      AString string = "5";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolDigital> digital = APtr<ARegexSymbolDigital>::make();
      AString string = "9";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolDigital> digital = APtr<ARegexSymbolDigital>::make();
      AString string = "A";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(!digital->is_complate(), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolDigital> digital_1 = APtr<ARegexSymbolDigital>::make();
      APtr<ARegexSymbolDigital> digital_2 = APtr<ARegexSymbolDigital>::make();
      digital_1->set_next(aptr_cast<ARegexSymbolDigital, ARegexGroup>(digital_2));
      AString string = "89";
      AStringIterator iterator(string);
      digital_1->execute(iterator);
      TEST_ALERT(digital_1->is_complate(), 5)
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(aregexsymboldigital)

   TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLDIGITAL)

#endif

