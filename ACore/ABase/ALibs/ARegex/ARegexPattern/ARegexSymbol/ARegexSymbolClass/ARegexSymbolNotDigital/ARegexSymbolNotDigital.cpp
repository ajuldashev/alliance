#include "ARegexSymbolNotDigital.hpp"
#include "ARegexSymbolNotDigital.test.cpp"
#include "ARegexSymbolNotDigital.test.node.cpp"
#include "ARegexSymbolNotDigital.debug.cpp"
#include "ARegexSymbolNotDigital.debug.node.cpp"
#include "ARegexSymbolNotDigital.exception.cpp"


ARegexSymbolNotDigital::ARegexSymbolNotDigital() {
    is_complated = false;
    symbol_class.set_class("^0-9");
}

ARegexSymbolNotDigital::ARegexSymbolNotDigital(const ARegexSymbolNotDigital & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolNotDigital::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolNotDigitalException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolNotDigital::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolNotDigital::to_text() {
    AString text = "\\D";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolNotDigital::~ARegexSymbolNotDigital() {
  
}