#include "ARegexSymbolNotDigital.exception.hpp"

ARegexSymbolNotDigitalException::ARegexSymbolNotDigitalException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolNotDigital";
}

ARegexSymbolNotDigitalException::ARegexSymbolNotDigitalException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolNotDigital");
}

ARegexSymbolNotDigitalException::ARegexSymbolNotDigitalException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolNotDigital");
}

ARegexSymbolNotDigitalException::ARegexSymbolNotDigitalException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolNotDigital");
}

ARegexSymbolNotDigitalException::ARegexSymbolNotDigitalException(const ARegexSymbolNotDigitalException & obj) : AllianceException(obj) {
	
}

ARegexSymbolNotDigitalException::~ARegexSymbolNotDigitalException() {

}

