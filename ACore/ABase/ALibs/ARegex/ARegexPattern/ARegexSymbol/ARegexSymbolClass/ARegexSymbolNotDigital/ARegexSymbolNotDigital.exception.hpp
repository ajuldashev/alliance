#ifndef HPP_AREGEXSYMBOLNOTDIGITALEXCEPTION
#define HPP_AREGEXSYMBOLNOTDIGITALEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolNotDigitalException : public AllianceException {
public:
	ARegexSymbolNotDigitalException();
	ARegexSymbolNotDigitalException(std::string text);
	ARegexSymbolNotDigitalException(AllianceException & e);
	ARegexSymbolNotDigitalException(AllianceException & e, std::string text);
	ARegexSymbolNotDigitalException(const ARegexSymbolNotDigitalException & obj);
	~ARegexSymbolNotDigitalException();
};

#endif

