#ifndef HPP_AREGEXSYMBOLNOTDIGITAL
#define HPP_AREGEXSYMBOLNOTDIGITAL

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolNotDigital.exception.hpp"

class ARegexSymbolNotDigital : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolNotDigital();
    ARegexSymbolNotDigital(const ARegexSymbolNotDigital & symbol);
    
    AStringIterator execute(AStringIterator iterator);
        
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolNotDigital();
};

#endif