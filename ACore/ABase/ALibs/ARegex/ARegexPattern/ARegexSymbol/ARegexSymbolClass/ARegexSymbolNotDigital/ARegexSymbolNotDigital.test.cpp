#include "ARegexSymbolNotDigital.hpp"

#ifdef TEST_AREGEXSYMBOLNOTDIGITAL
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotDigital> digital = APtr<ARegexSymbolNotDigital>::make();
      AString string = "0";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(!digital->is_complate(), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotDigital> digital = APtr<ARegexSymbolNotDigital>::make();
      AString string = "5";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(!digital->is_complate(), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotDigital> digital = APtr<ARegexSymbolNotDigital>::make();
      AString string = "9";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(!digital->is_complate(), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotDigital> digital = APtr<ARegexSymbolNotDigital>::make();
      AString string = "A";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 4)
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexsymbolnotdigital)

   TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLNOTDIGITAL)

#endif

