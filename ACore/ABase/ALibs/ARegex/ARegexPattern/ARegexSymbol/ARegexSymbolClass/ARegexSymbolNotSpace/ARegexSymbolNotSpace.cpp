#include "ARegexSymbolNotSpace.hpp"
#include "ARegexSymbolNotSpace.test.cpp"
#include "ARegexSymbolNotSpace.test.node.cpp"
#include "ARegexSymbolNotSpace.debug.cpp"
#include "ARegexSymbolNotSpace.debug.node.cpp"
#include "ARegexSymbolNotSpace.exception.cpp"


ARegexSymbolNotSpace::ARegexSymbolNotSpace() {
    is_complated = false;
    symbol_class.set_class("^ \t\v\r\n\f");
}

ARegexSymbolNotSpace::ARegexSymbolNotSpace(const ARegexSymbolNotSpace & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolNotSpace::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolNotSpaceException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolNotSpace::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolNotSpace::to_text() {
    AString text = "\\S";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolNotSpace::~ARegexSymbolNotSpace() {
  
}