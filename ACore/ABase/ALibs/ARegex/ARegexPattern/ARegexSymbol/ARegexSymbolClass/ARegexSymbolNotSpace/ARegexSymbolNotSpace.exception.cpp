#include "ARegexSymbolNotSpace.exception.hpp"

ARegexSymbolNotSpaceException::ARegexSymbolNotSpaceException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolNotSpace";
}

ARegexSymbolNotSpaceException::ARegexSymbolNotSpaceException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolNotSpace");
}

ARegexSymbolNotSpaceException::ARegexSymbolNotSpaceException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolNotSpace");
}

ARegexSymbolNotSpaceException::ARegexSymbolNotSpaceException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolNotSpace");
}

ARegexSymbolNotSpaceException::ARegexSymbolNotSpaceException(const ARegexSymbolNotSpaceException & obj) : AllianceException(obj) {
	
}

ARegexSymbolNotSpaceException::~ARegexSymbolNotSpaceException() {

}

