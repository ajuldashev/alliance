#ifndef HPP_AREGEXSYMBOLNOTSPACEEXCEPTION
#define HPP_AREGEXSYMBOLNOTSPACEEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolNotSpaceException : public AllianceException {
public:
	ARegexSymbolNotSpaceException();
	ARegexSymbolNotSpaceException(std::string text);
	ARegexSymbolNotSpaceException(AllianceException & e);
	ARegexSymbolNotSpaceException(AllianceException & e, std::string text);
	ARegexSymbolNotSpaceException(const ARegexSymbolNotSpaceException & obj);
	~ARegexSymbolNotSpaceException();
};

#endif

