#ifndef HPP_AREGEXSYMBOLNOTSPACE
#define HPP_AREGEXSYMBOLNOTSPACE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolNotSpace.exception.hpp"

class ARegexSymbolNotSpace : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolNotSpace();
    ARegexSymbolNotSpace(const ARegexSymbolNotSpace & symbol);
    
    AStringIterator execute(AStringIterator iterator);
        
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolNotSpace();
};

#endif