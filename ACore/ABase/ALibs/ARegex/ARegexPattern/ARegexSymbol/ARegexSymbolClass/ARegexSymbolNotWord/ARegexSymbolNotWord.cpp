#include "ARegexSymbolNotWord.hpp"
#include "ARegexSymbolNotWord.test.cpp"
#include "ARegexSymbolNotWord.test.node.cpp"
#include "ARegexSymbolNotWord.debug.cpp"
#include "ARegexSymbolNotWord.debug.node.cpp"
#include "ARegexSymbolNotWord.exception.cpp"


ARegexSymbolNotWord::ARegexSymbolNotWord() {
    is_complated = false;
    symbol_class.set_class("^A-Za-z0-9_");
}

ARegexSymbolNotWord::ARegexSymbolNotWord(const ARegexSymbolNotWord & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolNotWord::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolNotWordException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolNotWord::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolNotWord::to_text() {
    AString text = "\\W";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolNotWord::~ARegexSymbolNotWord() {
  
}