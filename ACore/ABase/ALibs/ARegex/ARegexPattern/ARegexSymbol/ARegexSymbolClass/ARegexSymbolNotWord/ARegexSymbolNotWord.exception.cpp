#include "ARegexSymbolNotWord.exception.hpp"

ARegexSymbolNotWordException::ARegexSymbolNotWordException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolNotWord";
}

ARegexSymbolNotWordException::ARegexSymbolNotWordException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolNotWord");
}

ARegexSymbolNotWordException::ARegexSymbolNotWordException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolNotWord");
}

ARegexSymbolNotWordException::ARegexSymbolNotWordException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolNotWord");
}

ARegexSymbolNotWordException::ARegexSymbolNotWordException(const ARegexSymbolNotWordException & obj) : AllianceException(obj) {
	
}

ARegexSymbolNotWordException::~ARegexSymbolNotWordException() {

}

