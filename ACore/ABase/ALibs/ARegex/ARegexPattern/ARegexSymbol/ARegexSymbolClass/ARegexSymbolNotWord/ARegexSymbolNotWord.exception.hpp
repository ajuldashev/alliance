#ifndef HPP_AREGEXSYMBOLNOTWORDEXCEPTION
#define HPP_AREGEXSYMBOLNOTWORDEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolNotWordException : public AllianceException {
public:
	ARegexSymbolNotWordException();
	ARegexSymbolNotWordException(std::string text);
	ARegexSymbolNotWordException(AllianceException & e);
	ARegexSymbolNotWordException(AllianceException & e, std::string text);
	ARegexSymbolNotWordException(const ARegexSymbolNotWordException & obj);
	~ARegexSymbolNotWordException();
};

#endif

