#ifndef HPP_AREGEXSYMBOLNOTWORD
#define HPP_AREGEXSYMBOLNOTWORD

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolNotWord.exception.hpp"

class ARegexSymbolNotWord : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolNotWord();
    ARegexSymbolNotWord(const ARegexSymbolNotWord & symbol);
    
    AStringIterator execute(AStringIterator iterator);
        
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolNotWord();
};

#endif