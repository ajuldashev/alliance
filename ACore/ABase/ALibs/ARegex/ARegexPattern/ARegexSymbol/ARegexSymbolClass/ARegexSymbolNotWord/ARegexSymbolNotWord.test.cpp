#include "ARegexSymbolNotWord.hpp"

#ifdef TEST_AREGEXSYMBOLNOTWORD
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "A";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "D";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "Z";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "a";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "d";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 5)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "z";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 6)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "0";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 7)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "5";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 8)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "9";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 9)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = "_";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(!word->is_complate(), 10)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolNotWord> word = APtr<ARegexSymbolNotWord>::make();
      AString string = " ";
      AStringIterator iterator(string);
      word->execute(iterator);
      TEST_ALERT(word->is_complate(), 11)
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(aregexsymbolnotword)

   TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLNOTWORD)

#endif

