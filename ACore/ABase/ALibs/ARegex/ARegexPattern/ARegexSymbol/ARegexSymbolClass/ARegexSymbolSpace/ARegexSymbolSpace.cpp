#include "ARegexSymbolSpace.hpp"
#include "ARegexSymbolSpace.test.cpp"
#include "ARegexSymbolSpace.test.node.cpp"
#include "ARegexSymbolSpace.debug.cpp"
#include "ARegexSymbolSpace.debug.node.cpp"
#include "ARegexSymbolSpace.exception.cpp"


ARegexSymbolSpace::ARegexSymbolSpace() {
    is_complated = false;
    symbol_class.set_class(" \t\v\r\n\f");
}

ARegexSymbolSpace::ARegexSymbolSpace(const ARegexSymbolSpace & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolSpace::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolSpaceException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolSpace::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolSpace::to_text() {
    AString text = "\\s";;
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolSpace::~ARegexSymbolSpace() {
  
}