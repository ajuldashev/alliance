#include "ARegexSymbolSpace.exception.hpp"

ARegexSymbolSpaceException::ARegexSymbolSpaceException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolSpace";
}

ARegexSymbolSpaceException::ARegexSymbolSpaceException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolSpace");
}

ARegexSymbolSpaceException::ARegexSymbolSpaceException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolSpace");
}

ARegexSymbolSpaceException::ARegexSymbolSpaceException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolSpace");
}

ARegexSymbolSpaceException::ARegexSymbolSpaceException(const ARegexSymbolSpaceException & obj) : AllianceException(obj) {
	
}

ARegexSymbolSpaceException::~ARegexSymbolSpaceException() {

}

