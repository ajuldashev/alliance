#ifndef HPP_AREGEXSYMBOLSPACEEXCEPTION
#define HPP_AREGEXSYMBOLSPACEEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolSpaceException : public AllianceException {
public:
	ARegexSymbolSpaceException();
	ARegexSymbolSpaceException(std::string text);
	ARegexSymbolSpaceException(AllianceException & e);
	ARegexSymbolSpaceException(AllianceException & e, std::string text);
	ARegexSymbolSpaceException(const ARegexSymbolSpaceException & obj);
	~ARegexSymbolSpaceException();
};

#endif

