#ifndef HPP_AREGEXSYMBOLSPACE
#define HPP_AREGEXSYMBOLSPACE

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolSpace.exception.hpp"

class ARegexSymbolSpace : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolSpace();
    ARegexSymbolSpace(const ARegexSymbolSpace & symbol);
    
    AStringIterator execute(AStringIterator iterator);
        
    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolSpace();
};

#endif