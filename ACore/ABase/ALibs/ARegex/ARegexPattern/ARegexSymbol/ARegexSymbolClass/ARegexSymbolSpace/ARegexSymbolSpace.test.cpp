#include "ARegexSymbolSpace.hpp"

#ifdef TEST_AREGEXSYMBOLSPACE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = " ";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 1)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "\t";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 2)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "\v";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 3)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "\r";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 4)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "\n";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 5)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "\f";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(digital->is_complate(), 6)
   }
   {
      /** <doc-test-info>
       * 
       */
      APtr<ARegexSymbolSpace> digital = APtr<ARegexSymbolSpace>::make();
      AString string = "_";
      AStringIterator iterator(string);
      digital->execute(iterator);
      TEST_ALERT(!digital->is_complate(), 7)
   }
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aregexsymbolspace)

   TEST_CALL(1)

TEST_FUNCTION_MAIN_END(AREGEXSYMBOLSPACE)

#endif

