#include "ARegexSymbolWord.hpp"
#include "ARegexSymbolWord.test.cpp"
#include "ARegexSymbolWord.test.node.cpp"
#include "ARegexSymbolWord.debug.cpp"
#include "ARegexSymbolWord.debug.node.cpp"
#include "ARegexSymbolWord.exception.cpp"


ARegexSymbolWord::ARegexSymbolWord() {
    is_complated = false;
    symbol_class.set_class("A-Za-z0-9_");
}

ARegexSymbolWord::ARegexSymbolWord(const ARegexSymbolWord & symbol) {
    is_complated = symbol.is_complated;
}

AStringIterator ARegexSymbolWord::execute(AStringIterator iterator) {
    try {
        is_complated = false;
        
        if (!iterator.can_get()) {
            return iterator;
        }   
        
        iterator = symbol_class.execute(iterator);

        is_complated = symbol_class.is_complate();

        if (next) {
            iterator = next->execute(iterator);
            is_complated = is_complated && next->is_complate();
        }

        return iterator;
    } catch (AllianceException & e) {
        throw ARegexSymbolWordException(e, "execute(AStringIterator iterator)");
    }
}

void ARegexSymbolWord::reset_behavior() {
    if (next) {
        next->reset_behavior();
    }
}

AString ARegexSymbolWord::to_text() {
    AString text = "\\w";
    if (next) {
        text += next->to_text();
    }
    return text;
}

ARegexSymbolWord::~ARegexSymbolWord() {
  
}