#include "ARegexSymbolWord.exception.hpp"

ARegexSymbolWordException::ARegexSymbolWordException() : AllianceException() {
	exception = "Alliance-Exception-ARegexSymbolWord";
}

ARegexSymbolWordException::ARegexSymbolWordException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ARegexSymbolWord");
}

ARegexSymbolWordException::ARegexSymbolWordException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ARegexSymbolWord");
}

ARegexSymbolWordException::ARegexSymbolWordException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ARegexSymbolWord");
}

ARegexSymbolWordException::ARegexSymbolWordException(const ARegexSymbolWordException & obj) : AllianceException(obj) {
	
}

ARegexSymbolWordException::~ARegexSymbolWordException() {

}

