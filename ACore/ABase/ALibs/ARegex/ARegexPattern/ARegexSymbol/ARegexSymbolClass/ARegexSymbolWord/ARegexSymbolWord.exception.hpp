#ifndef HPP_AREGEXSYMBOLWORDEXCEPTION
#define HPP_AREGEXSYMBOLWORDEXCEPTION
#include <alliance.exception.hpp>

class ARegexSymbolWordException : public AllianceException {
public:
	ARegexSymbolWordException();
	ARegexSymbolWordException(std::string text);
	ARegexSymbolWordException(AllianceException & e);
	ARegexSymbolWordException(AllianceException & e, std::string text);
	ARegexSymbolWordException(const ARegexSymbolWordException & obj);
	~ARegexSymbolWordException();
};

#endif

