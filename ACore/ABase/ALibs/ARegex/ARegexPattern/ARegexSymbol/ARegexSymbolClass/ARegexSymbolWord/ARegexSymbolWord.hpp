#ifndef HPP_AREGEXSYMBOLWORD
#define HPP_AREGEXSYMBOLWORD

#include <ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp>

#include "ARegexSymbolWord.exception.hpp"

class ARegexSymbolWord : public ARegexGroup {
private:
    ARegexSymbolClass symbol_class;
public:
    ARegexSymbolWord();
    ARegexSymbolWord(const ARegexSymbolWord & symbol);
    
    AStringIterator execute(AStringIterator iterator);

    void parse(AString string);
    void reset_behavior();
    void clear();

    AString to_text();

    ~ARegexSymbolWord();
};

#endif