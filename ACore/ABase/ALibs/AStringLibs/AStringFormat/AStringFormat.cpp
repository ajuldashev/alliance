#include "AStringFormat.hpp"
#include "AStringFormat.test.cpp"
#include "AStringFormat.test.node.cpp"
#include "AStringFormat.debug.cpp"
#include "AStringFormat.debug.node.cpp"
#include "AStringFormat.exception.cpp"

s64int AStringFormat::get_next_pos_escape(AString string, s64int pos) {
    for (u64int i = pos; i < string.length() - 1; i += 1) {
        if (string[i] == '%' && string[i + 1] == '%') {
            i += 1;
            continue;
        }
        if (string[i] == '%') {
            return i;
        }
    }
    return -1;
}

s64int AStringFormat::get_next_end_escape(AString string, s64int pos){
    for (u64int i = pos; i < string.length() - 1; i += 1) {
        if ((string[i] == 'c' && string[i + 1] == 's') ||
            (string[i] == 'c' && string[i + 1] == 'c') ||
            (string[i] == 's' && string[i + 1] == 's') ||
            (string[i] == 'b' && string[i + 1] == 'b') ||
            (string[i] == 's' && string[i + 1] == 'b') ||
            (string[i] == 'u' && string[i + 1] == 'b') ||
            (string[i] == 's' && string[i + 1] == 's') ||
            (string[i] == 'u' && string[i + 1] == 's') ||
            (string[i] == 's' && string[i + 1] == 'i') ||
            (string[i] == 'u' && string[i + 1] == 'i') ||
            (string[i] == 's' && string[i + 1] == 'l') ||
            (string[i] == 'u' && string[i + 1] == 'l') ||
            (string[i] == 'f' && string[i + 1] == 'f') ||
            (string[i] == 'd' && string[i + 1] == 'd')) {
            return i;
        }
    }
    return -1;
}

AString AStringFormat::get_next_escape(AString string, s64int pos) {
    s64int beg = get_next_pos_escape(string, pos);
    s64int end = get_next_end_escape(string, pos) + 2;
    return string.get(beg, end - beg);
}

AString AStringFormat::remove_escape(AString string, s64int pos) {
    s64int beg = get_next_pos_escape(string, pos);
    s64int end = get_next_end_escape(string, pos) + 2;
    string.remove(beg, end - beg);
    return string;
}

AString AStringFormat::get_type_from_escape(AString string) {
    return string.get(string.length() - 2, 2);
}

AString AStringFormat::format_string(AString text,
                                    s64int length,  
                                    AString background = " ",                    
                                    Align align = Align::left, 
                                    Resize lock = Resize::unlock) {
    AString res;
    if (length <= 0 || background.is_empty()) {
        return res;
    }
    while (res.length() < length) {
        res = res + background;
    }
    if (lock == Resize::lock) {
        res = res.get(0, length);
    }
    if (res.length() <= text.length()) {
        if (lock == Resize::lock) {
            if (align == Align::right) {
                res = text.get(text.length() - res.length(), length);  
            } else 
            if (align == Align::center) {
                res = text.get((text.length() - res.length()) / 2, length);   
            } else {
                res = text.get(0, length);
            }
        } else {
            res = text;
        }
        return res;
    }
    s64int index_in_res = 0;
    switch (align) {
        case Align::left:
            index_in_res = 0;
            break;
        case Align::center:
            index_in_res = (res.length() - text.length()) / 2;
            break;
        case Align::right:
            index_in_res = res.length() - text.length();
            break;
    }
    for (s64int i = 0; i < text.length(); i += 1) {
        res[i + index_in_res] = text[i];
    }
    return res;
}

AString AStringFormat::format_bool(bool f, 
                                    s64int length,  
                                    AString background = " ",                    
                                    Align align = Align::left, 
                                    Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(f),
                            length, 
                            background,
                            align,
                            resize);

}

AString AStringFormat::format_s8int(s8int num, 
                                    s64int length,  
                                    AString background = " ",                    
                                    Align align = Align::left, 
                                    Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);

}

AString AStringFormat::format_u8int(u8int num, 
                                    s64int length,  
                                    AString background = " ",                    
                                    Align aligan = Align::left, 
                                    Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            aligan,
                            resize);
}

AString AStringFormat::format_s16int(s16int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_u16int(u16int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_s32int(s32int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_u32int(u32int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_s64int(s64int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_u64int(u64int num, 
                                        s64int length,  
                                        AString background = " ",                    
                                        Align align = Align::left, 
                                        Resize resize = Resize::unlock) {
    return format_string(AStringLibs::to_string(num),
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_float(float num, 
                                    s16int correct,
                                    s64int length, 
                                    AString background, 
                                    Align align, 
                                    Resize resize) {
    AString text = AStringLibs::to_string(num, correct);
    return format_string(text,
                            length, 
                            background,
                            align,
                            resize);
}

AString AStringFormat::format_double(double num, 
                                    s16int correct,
                                    s64int length, 
                                    AString background, 
                                    Align align, 
                                    Resize resize) {
    AString text = AStringLibs::to_string(num, correct);
    return format_string(text,
                            length, 
                            background,
                            align,
                            resize);
}

void AStringFormat::test_escape(AString escape, 
                                    s16int & correct, 
                                    s64int & length, 
                                    AString & background, 
                                    Align & align, 
                                    Resize & resize) {
    correct = 2;
    length = 1;
    background = " ";
    align = AStringFormat::Align::left;
    resize = AStringFormat::Resize::unlock;
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    if (escape.is_empty()) {
        return;
    }
    if (escape[0] == '-') {
        align = AStringFormat::Align::left;
        escape.remove(0, 1);
    } else
    if (escape[0] == '+') {
        align = AStringFormat::Align::right;
        escape.remove(0, 1);
    } else
    if (escape[0] == '=') {
        align = AStringFormat::Align::center;
        escape.remove(0, 1);
    }
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    if (escape.is_empty()) {
        return;
    }
    s64int pos = 0;
    for (; pos < escape.length() && escape[pos] >= '0' && escape[pos] <= '9'; pos += 1);
    AString num = escape.get(0, pos);
    if (AStringLibs::test_int(num)) {
        length = AStringLibs::to_long(num);
        escape.remove(0, pos);
    }
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    if (escape.is_empty()) {
        return;
    }
    if (escape[0] == '.') {
        escape.remove(0,1);
        s64int pos = 0;
        for (; pos < escape.length() && escape[pos] >= '0' && escape[pos] <= '9'; pos += 1);
        AString num = escape.get(0, pos);
        if (AStringLibs::test_int(num)) {
            correct = AStringLibs::to_int(num);
            escape.remove(0, pos);
        }
    }
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    if (escape.is_empty()) {
        return;
    }
    if (escape[0] == 'L') {
        resize = AStringFormat::Resize::lock;
        escape.remove(0, 1);
    }
    while (!escape.is_empty() && escape[0] == ' ') {
        escape.remove(0, 1);
    }
    if (escape.is_empty()) {
        return;
    }
    if (escape[0] == '[') {
        escape.remove(0,1);
        s64int pos = 0;
        s64int qty = 0;
        for (; pos < escape.length() && qty == 0; pos += 1) {
            if (escape[pos] == '[') {
                qty += 1;
            }
            if (escape[pos] == ']') {
                qty -= 1;
            }
        }
        AString text = escape.get(0, pos - 1);
        if (qty == -1) {
            background = text;
        }
    }
}


AString AStringFormat::format(AString string, ...) {
    std::va_list ap; 									  
    va_start(ap,string);
    string = format_base(string, ap);
    va_end(ap);
    return string;
}

AString AStringFormat::format_base(AString string, std::va_list & ap) {								  
    s64int pos_next_escape = get_next_pos_escape(string, 0);
    while (pos_next_escape != -1) {
        s64int end_next_escape = get_next_end_escape(string, pos_next_escape);
        if (end_next_escape == -1) {
            break;
        }
        AString escape = get_next_escape(string, pos_next_escape);
        string = remove_escape(string, pos_next_escape);
        AString type = get_type_from_escape(escape);
        AString text;
        s64int  length = 1;
        AString background = " ";
        AStringFormat::Align align = AStringFormat::Align::left;
        AStringFormat::Resize resize = AStringFormat::Resize::unlock;
        s16int correct = 2;
        if (escape.length() > 3) {
            escape = escape.get(1, escape.length() - 3);
            test_escape(escape, 
                        correct, 
                        length, 
                        background,
                        align,
                        resize);
        }
        if (type == "ss") {
            AString ss = va_arg(ap, AString);
            text = format_string(ss, length, background, align, resize);
        } else
        if (type == "cs") {
            AString cs = va_arg(ap, const char *);
            text = format_string(cs, length, background, align, resize);
        } else
        if (type == "cc") {
            char cc = va_arg(ap, s32int);
            AString sc(cc);
            text = format_string(sc, length, background, align, resize);
        } else 
        if (type == "bb") {
            bool f = va_arg(ap, s32int);
            text = format_bool(f, length, background, align, resize);
        } else 
        if (type == "sb") {
            s8int num = va_arg(ap, s32int);
            text = format_s8int(num, length, background, align, resize);
        } else
        if (type == "ub") {
            u8int num = va_arg(ap, u32int);
            text = format_u8int(num, length, background, align, resize);
        } else
        if (type == "ss") {
            s16int num = va_arg(ap, s32int);
            text = format_s16int(num, length, background, align, resize);
        } else
        if (type == "us") {
            u16int num = va_arg(ap, u32int);
            text = format_u16int(num, length, background, align, resize);
        } else
        if (type == "si") {
            s32int num = va_arg(ap,s32int);
            text = format_s32int(num, length, background, align, resize);
        } else
        if (type == "ui") {
            u32int num = va_arg(ap, u32int);
            text = format_u32int(num, length, background, align, resize);
        } else
        if (type == "sl") {
            s64int num = va_arg(ap, s64int);
            text = format_s64int(num, length, background, align, resize);
        } else
        if (type == "ul") {
            u64int num = va_arg(ap, u64int);
            text = format_u64int(num, length, background, align, resize);
        } else
        if (type == "ff") {
            float num = va_arg(ap, double);
            text = format_float(num,  correct, length, background, align, resize);
        } else
        if (type == "dd") {
            double num = va_arg(ap, double);
            text = format_double(num, correct, length, background, align, resize);
        }
        string.insert(text, pos_next_escape);
        end_next_escape = pos_next_escape + text.length();
        pos_next_escape = get_next_pos_escape(string, end_next_escape);
    }
    return string;
}