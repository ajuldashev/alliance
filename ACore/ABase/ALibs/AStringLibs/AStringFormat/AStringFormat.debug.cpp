#include "AStringFormat.hpp"

#ifdef DEBUG_ASTRINGFORMAT
#include <ACore/ABase/AIO/AIO.hpp>

AStringFormat::Align debug_aligan(AString aligan) {
    if (aligan == "left") {
        return AStringFormat::Align::left;
    } 
    if (aligan == "center") {
        return AStringFormat::Align::center;
    } 
    return AStringFormat::Align::right;
}

AStringFormat::Resize debug_locksize(AString lock) {
    if (lock == "lock") {
        return AStringFormat::Resize::lock;
    } 
    return AStringFormat::Resize::unlock;
}

bool debug_astringformat() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRINGFORMAT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astringformat());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) format_string");
            AIO::writeln(" 3) format_s8int");
            AIO::writeln(" 4) format_u8int");
            AIO::writeln(" 5) format_s16int");
            AIO::writeln(" 6) format_u16int");
            AIO::writeln(" 7) format_s32int");
            AIO::writeln(" 8) format_u32int");
            AIO::writeln(" 9) format_s64int");
            AIO::writeln("10) format_u64int");
            AIO::writeln("11) format_float");
            AIO::writeln("12) format_double");
            AIO::writeln("13) get_next_pos_escape");
            AIO::writeln("14) get_next_end_escape");
            AIO::writeln("15) get_next_escape");
            AIO::writeln("16) remove_escape");
            AIO::writeln("17) get_type_from_escape");
            AIO::writeln("18) format");            
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astringformat(com)) {
			return true;
		}
        if (com == "format_string") {
            AIO::writeln("text, length, background, align(left, center, right), resize(lock, unlock)");
            AString text;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            text = AIO::get_word();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_string(text, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_s8int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)"); 
            s8int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_s8int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_u8int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");             u8int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_u8int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_s16int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            s16int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_s16int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_u16int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            u16int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_u16int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_s32int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            s32int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_s32int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_u32int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            u32int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_u32int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_s64int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            s64int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_s64int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_u64int") {
            AIO::writeln("num, length, background, align(left, center, right), resize(lock, unlock)");
            u64int num;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_u64int(num, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_float") {
            AIO::writeln("num, correct, length, background, align(left, center, right), resize(lock, unlock)");
            float num;
            s16int correct;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_float();
            correct = AIO::get_s32int();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_float(num, correct, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "format_double") {
            AIO::writeln("num, correct, length, background, align(left, center, right), resize(lock, unlock)");
            double num;
            s16int correct;
            s64int length;
            AString background;
            AStringFormat::Align align;
            AStringFormat::Resize lock;
            num = AIO::get_s32int();
            correct = AIO::get_double();
            length = AIO::get_s32int();
            background = AIO::get_word();
            align = debug_aligan(AIO::get_word());
            lock = debug_locksize(AIO::get_word());
            AString res = AStringFormat::format_double(num, correct, length, background, align, lock);
            AIO::write("Format : ");
            AIO::writeln(res);
        }
        if (com == "get_next_pos_escape") {
            AString escape = AIO::get_word();
            s64int pos = AIO::get_s32int();
            AIO::writeln(AStringFormat::get_next_pos_escape(escape, pos));
        }
        if (com == "get_next_end_escape") {
            AString escape = AIO::get_word();
            s64int pos = AIO::get_s32int();
            AIO::writeln(AStringFormat::get_next_end_escape(escape, pos));
        }
        if (com == "get_next_escape") {
            AString escape = AIO::get_word();
            s64int pos = AIO::get_s32int();
            AIO::writeln(AStringFormat::get_next_escape(escape, pos));
        }
        if (com == "remove_escape") {
            AString escape = AIO::get_word();
            s64int pos = AIO::get_s32int();
            AIO::writeln(AStringFormat::remove_escape(escape, pos));
        }
        if (com == "get_type_from_escape") {
            AString escape = AIO::get_word();
            AIO::writeln(AStringFormat::get_type_from_escape(escape));
        }
        if (com == "format") {
            AString escape = AIO::get_word();
            AIO::writeln(AStringFormat::format(escape));
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRINGFORMAT );
    AIO::write_div_line();
    return false;
}
#endif

