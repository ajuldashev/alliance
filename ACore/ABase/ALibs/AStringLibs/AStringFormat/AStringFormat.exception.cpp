#include "AStringFormat.exception.hpp"

AStringFormatException::AStringFormatException() : AllianceException() {
	exception = "Alliance-Exception-AStringFormat";
}

AStringFormatException::AStringFormatException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringFormat");
}

AStringFormatException::AStringFormatException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringFormat");
}

AStringFormatException::AStringFormatException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringFormat");
}

AStringFormatException::AStringFormatException(const AStringFormatException & obj) : AllianceException(obj) {
	
}

AStringFormatException::~AStringFormatException() {

}

