#ifndef HPP_ASTRINGFORMATEXCEPTION
#define HPP_ASTRINGFORMATEXCEPTION
#include <alliance.exception.hpp>

class AStringFormatException : public AllianceException {
public:
	AStringFormatException();
	AStringFormatException(std::string text);
	AStringFormatException(AllianceException & e);
	AStringFormatException(AllianceException & e, std::string text);
	AStringFormatException(const AStringFormatException & obj);
	~AStringFormatException();
};

#endif

