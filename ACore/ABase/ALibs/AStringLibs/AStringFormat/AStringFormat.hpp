#ifndef HPP_ASTRINGFORMAT
#define HPP_ASTRINGFORMAT
#include "../AStringLibs.hpp"
#include <cstdarg>
#include "AStringFormat.exception.hpp"

class AStringFormat {
private:
 	
public:
	enum class Align {
        left, 
        center, 
        right
    };
    enum class Resize {
        unlock,
		lock
    };

	static s64int get_next_pos_escape(AString, s64int);
	static s64int get_next_end_escape(AString, s64int);
	static AString get_next_escape(AString, s64int);
	static AString remove_escape(AString, s64int);
	static AString get_type_from_escape(AString);

    static AString format_string(AString, s64int, AString, Align, Resize);
	
	static AString format_bool(bool, s64int, AString, Align, Resize);
	static AString format_s8int(s8int, s64int, AString, Align, Resize);
	static AString format_u8int(u8int, s64int, AString, Align, Resize);
	static AString format_s16int(s16int, s64int, AString, Align, Resize);
	static AString format_u16int(u16int, s64int, AString, Align, Resize);
	static AString format_s32int(s32int, s64int, AString, Align, Resize);
	static AString format_u32int(u32int, s64int, AString, Align, Resize);
	static AString format_s64int(s64int, s64int, AString, Align, Resize);
	static AString format_u64int(u64int, s64int, AString, Align, Resize);

	static AString format_float(float, s16int, s64int, AString, Align, Resize);
	static AString format_double(double, s16int, s64int, AString, Align, Resize);
	
	static void test_escape(AString, s16int &, s64int &, AString &, Align &, Resize &);

	static AString format(AString, ...);
	static AString format_base(AString, std::va_list &);
};

#endif