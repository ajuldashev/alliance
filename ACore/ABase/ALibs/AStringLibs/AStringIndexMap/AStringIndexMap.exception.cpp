#include "AStringIndexMap.exception.hpp"

AStringIndexMapException::AStringIndexMapException() : AllianceException() {
	exception = "Alliance-Exception-AStringIndexMap";
}

AStringIndexMapException::AStringIndexMapException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringIndexMap");
}

AStringIndexMapException::AStringIndexMapException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringIndexMap");
}

AStringIndexMapException::AStringIndexMapException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringIndexMap");
}

AStringIndexMapException::AStringIndexMapException(const AStringIndexMapException & obj) : AllianceException(obj) {
	
}

AStringIndexMapException::~AStringIndexMapException() {

}

