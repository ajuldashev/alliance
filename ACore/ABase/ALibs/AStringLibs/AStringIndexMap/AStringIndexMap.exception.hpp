#ifndef HPP_ASTRINGINDEXMAPEXCEPTION
#define HPP_ASTRINGINDEXMAPEXCEPTION
#include <alliance.exception.hpp>

class AStringIndexMapException : public AllianceException {
public:
	AStringIndexMapException();
	AStringIndexMapException(std::string text);
	AStringIndexMapException(AllianceException & e);
	AStringIndexMapException(AllianceException & e, std::string text);
	AStringIndexMapException(const AStringIndexMapException & obj);
	~AStringIndexMapException();
};

#endif

