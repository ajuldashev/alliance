#ifndef HPP_ASTRINGINDEXMAP
#define HPP_ASTRINGINDEXMAP
#include <ACore/ABase/AMath/AMath.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include "AStringIndexMap.exception.hpp"

class AStringIndexMap {
public:
    class Node;
private:
    Node * root;
    bool add_rec(Node *, AString &, s64int);
    s64int find_rec(Node *, AString &, s64int, s64int);
    AString ftab(s64int);
    bool test_left(Node *);
    bool test_right(Node *);
    void rotation_left(Node **);
    void rotation_right(Node **);
    void update_length(Node *);
public:
    AStringIndexMap();
    bool add(AString &, s64int = 0);
    char get(s64int);
    s64int find(AString &, s64int offset = 0, s64int pos = 0);
    void print(Node * node = NULL, s64int tab = 0);
    s64int size();
    s64int length();
    ~AStringIndexMap();
};

class AStringIndexMap::Node {
public:
    Node();
    Node * left;
    Node * right;
    Node * parent;
    AStringIndexMap map;
    s64int  size;
    s64int length;
    char data;
    bool is_end;
    ~Node();
};

AStringIndexMap::Node::Node() {
    left = NULL;
    right = NULL;
    parent = NULL;
    size = 0;
    length = 0;
    is_end = false;
}

AStringIndexMap::Node::~Node() {
    if (left) {
        delete left;
    }
    if (right) {
        delete right;
    }
    size = 0;
}

bool AStringIndexMap::add_rec(Node * node, AString & data, s64int pos) {
    if (node) {
        if (node->data > data[pos]) {
            if (node->left == NULL) {
                node->left = new Node();
                node->size += 1;
                node->left->size = 1;
                node->left->data = data[pos];
                node->left->parent = node;
                node->left->length += 1;
                if (pos == data.length() - 1) {
                    node->length += 1;
                    node->left->is_end = true;
                    return true;
                }
                bool f = true;
                if (pos < data.length()) {
                    f = node->left->map.add(data, pos + 1);
                    if (f) {
                        node->length += 1;
                    }
                }
                return f;
            }
            bool f = add_rec(node->left, data, pos);
            if (f) {
                node->size += 1;
                node->length += 1;
                if (test_left(node)) {
                    if (node == root) {
                        rotation_left(&node);
                        root = node;
                    } else {
                        rotation_left(&node);
                    }
                } else
                if (test_right(node)) {
                    if (node == root) {
                        rotation_right(&node);
                        root = node;
                    } else {
                        rotation_right(&node);
                    }
                }
            }
            return f;
        }
        if (node->data < data[pos]) {
            if (node->right == NULL) {
                node->right = new Node();
                node->size += 1;
                node->right->size = 1;
                node->right->data = data[pos];
                node->right->parent = node;
                node->right->length += 1;
                if (pos == data.length() - 1) {
                    node->length += 1;
                    node->right->is_end = true;
                    return true;
                }
                bool f = true;
                if (pos < data.length()) {
                    f = node->right->map.add(data, pos + 1);
                    if (f) {
                        node->length += 1;
                    }
                }
                return f;
            }
            bool f = add_rec(node->right, data, pos);
            if (f) {
                node->size += 1;
                node->length += 1;
                if (test_left(node)) {
                    if (node == root) {
                        rotation_left(&node);
                        root = node;
                    } else {
                        rotation_left(&node);
                    }
                } else
                if (test_right(node)) {
                    if (node == root) {
                        rotation_right(&node);
                        root = node;
                    } else {
                        rotation_right(&node);
                    }
                }
            }
            return f;
        }
        if (pos < data.length()) {
            if (pos == data.length() - 1) {
                if (node->is_end) {
                    return false;
                }
                node->length += 1;
                node->is_end = true; 
            } else {
                bool f = node->map.add(data, pos + 1);
                if (f) {
                    node->length += 1;
                } 
                return f; 
            }
            return true;
        }
    }
    return false;
}

s64int AStringIndexMap::find_rec(Node * node, AString & data, s64int offset,s64int pos) {
    if (node) {
        if (node->data < data[offset]) {
            pos += node->length;
            if (node->right) {
                pos -= node->right->length;
            }
            return find_rec(node->right, data, offset, pos);
        }
        if (node->data > data[offset]) {
            return find_rec(node->left, data, offset, pos);
        }
        if (node->left) {
            pos += node->left->length;
        }
        if (offset == data.length() - 1) {
            return (node->is_end && (node->data == data[offset]))? pos : -1;
        }
        return node->map.find(data, offset + 1, pos);
    }
    return -1;
}

char AStringIndexMap::get(s64int pos) {
    char res;
    if (root) {
        if (pos >= 0 && pos < root->size) {
            Node * node = root;
            if (pos == 0 && !node->left) {
                return node->data;
            }
            while (pos >= 0) {
                if (node->left && pos == node->left->size) {
                    return node->data;
                }
                if (node->left && pos < node->left->size) {
                    node = node->left;
                    continue;
                }
                if (node->left && node->right) {
                    pos -= node->left->size + 1;
                    node = node->right;
                    continue;
                }
                if (pos == 0 && !node->left) {
                    return node->data;
                }
                if (node->right) {
                    pos -= 1;
                    node = node->right;
                    continue;
                }
                return node->data;
            }
        }
    }    
    return res;
}

s64int AStringIndexMap::find(AString & data, s64int offset, s64int pos) {
    return find_rec(root, data, offset, pos);
}

AString AStringIndexMap::ftab(s64int tab) {
    AString res = "";
    for (s64int i = 0; i < tab; i += 1) {
        res = res + "  ";
    }
    return res;
}

bool AStringIndexMap::test_left(Node * node) {
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->right && node->right->right) {
            new_right = node->right->right->size;
        }
        new_left = node->size - 1 - new_right;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

bool AStringIndexMap::test_right(Node *node) { 
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->left && node->left->left) {
            new_left = node->left->left->size;
        }
        new_right = node->size - 1 - new_left;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

void AStringIndexMap::rotation_left(Node ** node) {
    if (node && *node) {
        Node * pnode = *node; 
        if (pnode->right) {
            s64int size = pnode->size;
            Node * rnode = pnode->right;
            if (rnode->left) {
                rnode->left->parent = pnode;
            }
            pnode->right = rnode->left;
            rnode->left = pnode;
            rnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = rnode;
                } else {
                    pnode->parent->right = rnode;
                }
            }
            pnode->parent = rnode;
            pnode = rnode;
            pnode->size = size;
            pnode->left->size = size - 1;
            update_length(pnode->left);
            update_length(pnode);
            if (pnode->right) {
                pnode->left->size -= pnode->right->size;
            }
            (*node) = pnode;
        }
    }
}

void AStringIndexMap::rotation_right(Node ** node) {
    if (node && *node) {
        Node * pnode = *node; 
        if (pnode->left) {
            s64int size = pnode->size;
            Node * lnode = pnode->left;
            if (lnode->right) {
                lnode->right->parent = pnode;
            }
            pnode->left = lnode->right;
            lnode->right = pnode;
            lnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = lnode;
                } else {
                    pnode->parent->right = lnode;
                }
            }
            pnode->parent = lnode;
            pnode = lnode;
            pnode->size = size;
            pnode->right->size = size - 1;
            update_length(pnode->right);
            update_length(pnode);
            if (pnode->left) {
                pnode->right->size -= pnode->left->size;
            }
            (*node) = pnode;
        }
    }
}

void AStringIndexMap::update_length(Node * node) {
    if (node) {
        s64int length_map = node->map.length();
        s64int length_left = (node->left)? node->left->length : 0;
        s64int length_right = (node->right)? node->right->length : 0;;
        s64int length_is_end = node->is_end;
        node->length = length_map + length_left + length_right + length_is_end;
    }
}

AStringIndexMap::AStringIndexMap() {
    root = NULL;
}

bool AStringIndexMap::add(AString & data, s64int pos) {
    if (root == NULL && pos < data.length()) {
        root = new Node();
        root->size = 1;
        root->data = data[pos];
        root->length = 1;
        if (data.length() - 1 == pos) {
            root->is_end = true;
        } 
        if (pos < data.length()) { 
            root->map.add(data, pos + 1);
        }
        return true;
    }
    return add_rec(root, data, pos);
}

void AStringIndexMap::print(Node * node, s64int tab) {
    if (!node) {
        node = root;
    }
    if (node) {
        AString data = node->data;
        AIO::write(ftab(tab)); 
        AIO::write(data);
        AIO::write(" ");
        AIO::write(node->is_end); 
        AIO::write(" ");
        AIO::write(node->length);
        AIO::writeln();
        AIO::write(ftab(tab + 2)); 
        AIO::write("-----"); 
        AIO::writeln(); 
        node->map.print(NULL, tab + 2);
        AIO::write(ftab(tab + 2)); 
        AIO::write("-----"); 
        AIO::writeln(); 
        if (node->left || node->right) {
            if (node->left) {
                AIO::write(ftab(tab + 1));
                AIO::write("L "); 
                AIO::writeln();
                print(node->left, tab + 1);
            }
            if (node->right) {
                AIO::write(ftab(tab + 1));
                AIO::write("R "); 
                AIO::writeln();
                print(node->right, tab + 1);
            }
        }
        return;
    } 
}

s64int AStringIndexMap::size() {
    if (root) {
        return root->size;
    }
    return 0;
}

s64int AStringIndexMap::length() {
    if (root) {
        return root->length;
    }
    return 0;
}

AStringIndexMap::~AStringIndexMap() {
    if (root) {
        delete root;
    }
}

#endif