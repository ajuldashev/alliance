#include "AStringIndexMap.hpp"

#ifdef TEST_ASTRINGINDEXMAP
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
      AStringIndexMap tree;
      s32int N = 1000;
      AString buff;
      {
         for (s32int i = N/10; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            tree.add(buff);
         }
      }
      {
         for (s32int i = N/10; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            if (tree.find(buff) != i - N/10) {
               /** <doc-test-info>
                *
                */             
               TEST_ALERT(false, 1)
            }
         }
      }
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(astringindexmap)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ASTRINGINDEXMAP)

#endif