#include "AStringIterator.hpp"
#include "AStringIterator.test.cpp"
#include "AStringIterator.test.node.cpp"
#include "AStringIterator.debug.cpp"
#include "AStringIterator.debug.node.cpp"
#include "AStringIterator.exception.cpp"

AStringIterator::AStringIterator() {
    this->index = 0;
}

AStringIterator::AStringIterator(AString string) {
    this->string = string;
    this->index = 0;
}

AStringIterator::AStringIterator(const AStringIterator & iterator) {
    this->string = iterator.string;
    this->index = iterator.index;
}

bool AStringIterator::has_next() {
    return index < string.length() - 1;
}

ASymbol AStringIterator::get_next() {
    try {
        ASymbol symbol;
        symbol.set(string, index + ASymbol::test_length_symbol(string, index));
        return symbol;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get_next()");
    }
}

void AStringIterator::next() {
    index += (index == -1)? 1 :
        ASymbol::test_length_symbol(string, index);
}

bool AStringIterator::has_prev() {
    return index > 0;
}

ASymbol AStringIterator::get_prev() {
    try {
        ASymbol symbol;
        symbol.set(string, index - 1);
        return symbol;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get_prev()");
    }
}

void AStringIterator::prev() {
   index -= 1;
}

bool AStringIterator::is_first() {
    return index == 0;
}

bool AStringIterator::is_last() {
    return string.length() == (index + ASymbol::test_length_symbol(string, index));
}

bool AStringIterator::can_get() {
    return index >= 0 
        && index < string.length() 
        && !string.is_empty(); 
}

bool AStringIterator::is_empty() {
    return string.length() == 0;
}

ASymbol AStringIterator::get() {
    try {
        ASymbol symbol;
        index = symbol.set(string, index);
        return symbol;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get()");
    }
}

void AStringIterator::set(AString string) {
    this->string = string;
    reset();
}

void AStringIterator::reset(Position position) {
    index = (position == Position::begin)? 0 : string.length() - 1;
}

void AStringIterator::clear() {
    string = "";
    index = 0;
}

s64int AStringIterator::get_index() {
    return index;
}

AString AStringIterator::get_string() {
    return string;
}

s64int AStringIterator::get_length() {
    return string.length();
}

AStringIterator AStringIterator::get_first_part() {
    try {
        AStringIterator iterator;
        if (index != 0) {
            iterator.string = string.get(0, index);
        }
        iterator.reset();
        return iterator;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get_first_part()");
    }
}

AStringIterator AStringIterator::get_second_part() {
    try {
        AStringIterator iterator;
        if (index != string.length()) {
            iterator.string = string.get(index, string.length() - index);
        }
        iterator.reset();
        return iterator;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get_second_part()");
    }
}

AString AStringIterator::get_between(AStringIterator iterator) {
    try {
        s64int min_index = (index < iterator.index)?
            index : iterator.index;
        s64int max_index = (index >= iterator.index)?
            index : iterator.index;    
        
        s64int length = max_index - min_index;

        if (length == 0) {
            return AString();
        }

        return string.get(min_index, length);
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "get_between(AStringIterator iterator)");
    }
}

AString AStringIterator::remove_between(AStringIterator & iterator) {
    try {
        s64int min_index = (index < iterator.index)?
            index : iterator.index;
        s64int max_index = (index >= iterator.index)?
            index : iterator.index;    
        
        s64int length = max_index - min_index;

        if (length == 0) {
            return AString();
        }

        string.remove(min_index, length);

        iterator.index = min_index;
        iterator.string = string;

        return string;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "remove_between(AStringIterator & iterator)");
    }
}

AString AStringIterator::replace_between(AStringIterator & iterator, AString substring) {
    try {
        s64int min_index = (index < iterator.index)?
            index : iterator.index;
        s64int max_index = (index >= iterator.index)?
            index : iterator.index;    
        
        s64int length = max_index - min_index;

        if (length == 0) {
            return AString();
        }

        string.remove(min_index, length);
        string.insert(substring, min_index);
        
        iterator.index = min_index + substring.length();
        iterator.string = string;
        
        return string;
    } catch (AllianceException & e) {
        throw AStringIteratorException(e, "replace_between(AStringIterator & iterator, AString substring)");
    }

    return AString();
}

AStringIterator & AStringIterator::operator=(AStringIterator iterator) {
    this->string = iterator.string;
    this->index = iterator.index;
    return *this;
}

AStringIterator::~AStringIterator() {
    
}