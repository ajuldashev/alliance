#include "AStringIterator.exception.hpp"

AStringIteratorException::AStringIteratorException() : AllianceException() {
	exception = "Alliance-Exception-AStringIterator";
}

AStringIteratorException::AStringIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringIterator");
}

AStringIteratorException::AStringIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringIterator");
}

AStringIteratorException::AStringIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringIterator");
}

AStringIteratorException::AStringIteratorException(const AStringIteratorException & obj) : AllianceException(obj) {
	
}

AStringIteratorException::~AStringIteratorException() {

}

