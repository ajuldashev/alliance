#ifndef HPP_ASTRINGITERATOREXCEPTION
#define HPP_ASTRINGITERATOREXCEPTION
#include <alliance.exception.hpp>

class AStringIteratorException : public AllianceException {
public:
	AStringIteratorException();
	AStringIteratorException(std::string text);
	AStringIteratorException(AllianceException & e);
	AStringIteratorException(AllianceException & e, std::string text);
	AStringIteratorException(const AStringIteratorException & obj);
	~AStringIteratorException();
};

#endif

