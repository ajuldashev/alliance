#ifndef HPP_ASTRINGITERATOR
#define HPP_ASTRINGITERATOR
#include <ACore/ABase/ATypes/ASymbol/ASymbol.hpp>
#include "AStringIterator.exception.hpp"

class AStringIterator {
public:
    enum class Position {
        begin, 
        end
    };
private:
    AString string;
    s64int index;
public:
    AStringIterator();
    AStringIterator(AString string);
    AStringIterator(const AStringIterator & iterator);
    
    bool has_next();
    ASymbol get_next();
    void next();

    bool has_prev();
    ASymbol get_prev();
    void prev();
    
    bool is_first();
    bool is_last();

    bool can_get();

    bool is_empty();

    ASymbol get();
    void set(AString string);
    void reset(Position position = Position::begin);
    void clear();

    s64int  get_index();
    AString get_string();
    s64int  get_length();

    AStringIterator get_first_part();
    AStringIterator get_second_part();

    AString get_between(AStringIterator iterator);
    AString remove_between(AStringIterator & iterator);
    AString replace_between(AStringIterator & iterator, AString substring);

    AStringIterator & operator = (AStringIterator iterator);

    ~AStringIterator();
};

#endif