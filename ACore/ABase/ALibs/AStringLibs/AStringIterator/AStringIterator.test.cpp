#include "AStringIterator.hpp"

#ifdef TEST_ASTRINGITERATOR
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		AStringIterator iterator("");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_next(), 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_prev(), 2)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_length() == 0, 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_string() == "", 4)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 0, 5)
		
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
		AStringIterator iterator("1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_prev(), 2)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_length() == 4, 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_string() == "1234", 4)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 0, 5)

		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 6)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_prev(), 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 0, 8)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '1', 9)

		iterator.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 10)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 11)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 1, 12)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '2', 13)

		iterator.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 14)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 15)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 2, 16)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '3', 17)

		iterator.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_next(), 18)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 19)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 3, 20)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '4', 21)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
		AStringIterator iterator("1234");
		iterator.reset(AStringIterator::Position::end);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_next(), 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 2)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_length() == 4, 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_string() == "1234", 4)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 3, 5)

		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_next(), 6)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 3, 8)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '4', 9)

		iterator.prev();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 10)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 11)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 2, 12)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '3', 13)

		iterator.prev();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 14)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_prev(), 15)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 1, 16)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '2', 17)

		iterator.prev();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.has_next(), 18)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(!iterator.has_prev(), 19)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_index() == 0, 20)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '1', 21)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
		AStringIterator iterator("1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '1', 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_next() == '2', 2)
		
		iterator.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '2', 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_next() == '3', 4)
		
		iterator.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '3', 5)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_next() == '4', 6)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_prev() == '2', 7)

		iterator.prev();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get() == '2', 8)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_next() == '3', 9)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator.get_prev() == '1', 10)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_between(iterator_2) == "1234", 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 2)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_between(iterator_2) == "12", 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 4)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_between(iterator_2) == "34", 5)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 6)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_between(iterator_2) == "23", 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 8)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.remove_between(iterator_2) == "", 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 2)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.remove_between(iterator_2) == "34", 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 4)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.remove_between(iterator_2) == "12", 5)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 6)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.remove_between(iterator_2) == "14", 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 8)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(7)
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.replace_between(iterator_2, "ab") == "ab", 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 2)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_index() == 0, 3)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_2.get_index() == 2, 4)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");

		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.replace_between(iterator_2, "abcd") == "abcd34", 5)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 6)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_index() == 0, 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_2.get_index() == 4, 8)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.replace_between(iterator_2, "abcd") == "12abcd", 9)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 10)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_index() == 2, 11)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_2.get_index() == 6, 12)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.replace_between(iterator_2, "abcd") == "1abcd4", 13)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 14)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_index() == 1, 15)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_2.get_index() == 5, 16)
	}
	{
		AStringIterator iterator_1("1234");
		AStringIterator iterator_2("1234");
		iterator_1.next();

		iterator_2.next();
		iterator_2.next();
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.replace_between(iterator_2, "a") == "1a34", 17)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_string() == iterator_2.get_string(), 18)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_1.get_index() == 1, 19)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(iterator_2.get_index() == 2, 20)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(8)
   {
		AStringIterator first;
		AStringIterator second;
		AStringIterator iterator("adsb");

		/** <doc-test-info>
		 * Первая половина []
		 * Вторая половина [adsb]
		 */
		first = iterator.get_first_part();
		second = iterator.get_second_part();
		TEST_ALERT(first.get_string() == "", 1)
		TEST_ALERT(second.get_string() == "adsb", 2)

		/** <doc-test-info>
		 * Первая половина [a]
		 * Вторая половина [dsb]
		 */
		iterator.next();
		first = iterator.get_first_part();
		second = iterator.get_second_part();
		TEST_ALERT(first.get_string() == "a", 3)
		TEST_ALERT(second.get_string() == "dsb", 4)

		/** <doc-test-info>
		 * Первая половина [ad]
		 * Вторая половина [sb]
		 */
		iterator.next();
		first = iterator.get_first_part();
		second = iterator.get_second_part();
		TEST_ALERT(first.get_string() == "ad", 5)
		TEST_ALERT(second.get_string() == "sb", 6)

		/** <doc-test-info>
		 * Первая половина [ads]
		 * Вторая половина [b]
		 */
		iterator.next();
		first = iterator.get_first_part();
		second = iterator.get_second_part();
		TEST_ALERT(first.get_string() == "ads", 7)
		TEST_ALERT(second.get_string() == "b", 8)

		/** <doc-test-info>
		 * Первая половина [adsb]
		 * Вторая половина []
		 */
		iterator.next();
		first = iterator.get_first_part();
		second = iterator.get_second_part();
		TEST_ALERT(first.get_string() == "adsb", 9)
		TEST_ALERT(second.get_string() == "", 10)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(9)
   {
		AStringIterator iterator("чфйц");
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ч", 1)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ф", 2)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "й", 3)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ц", 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(10)
   {
		AStringIterator iterator("1фaц");
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "1", 1)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ф", 2)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "a", 3)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ц", 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(11)
   {
		AStringIterator iterator("1фaц");
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "1", 1)
		TEST_ALERT(iterator.is_first(), 2)
		TEST_ALERT(!iterator.is_last(), 3)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ф", 4)
		TEST_ALERT(!iterator.is_first(), 5)
		TEST_ALERT(!iterator.is_last(), 6)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "a", 7)
		TEST_ALERT(!iterator.is_first(), 8)
		TEST_ALERT(!iterator.is_last(), 9)

		iterator.next();
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.get() == "ц", 10)
		TEST_ALERT(!iterator.is_first(), 11)
		TEST_ALERT(iterator.is_last(), 12)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 * 
 */
TEST_FUNCTION_BEGIN(12)
   {
		AStringIterator iterator;
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(iterator.is_empty(), 1)
   }
   {
		AStringIterator iterator("1фaц");
		/** <doc-test-info>
		 * 
		 */
		TEST_ALERT(!iterator.is_empty(), 2)
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(astringiterator)

	TEST_CALL(1)
	TEST_CALL(2)
	TEST_CALL(3)
	TEST_CALL(4)
	TEST_CALL(5)
	TEST_CALL(6)
	TEST_CALL(7)
	TEST_CALL(8)
	TEST_CALL(9)
	TEST_CALL(10)
	TEST_CALL(11)
	TEST_CALL(12)

TEST_FUNCTION_MAIN_END(ASTRINGITERATOR)

#endif
