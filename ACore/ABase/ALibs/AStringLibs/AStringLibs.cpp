#include "AStringLibs.hpp"
#include "AStringLibs.test.cpp"
#include "AStringLibs.test.node.cpp"
#include "AStringLibs.debug.cpp"
#include "AStringLibs.debug.node.cpp"
#include "AStringLibs.exception.cpp"

AStringLibs::AStringLibs() {

}

AStringLibs::AStringLibs(AString s) {
    string = s;
}

AStringLibs::AStringLibs(const AStringLibs & obj) {
    string = obj.string;
}

void AStringLibs::set(AString s) {
    string = s;
}

AString AStringLibs::get() {
    return string;
}

AArray<AString> AStringLibs::split(char) {
    AArray<AString> arr;

    return arr;
}

AString AStringLibs::to_string(bool f) {
    AString res;
    to_string(res, f);
    return res;
}

void AStringLibs::to_string(AString & buff, bool f) {
    if (f) {
        buff = "TRUE";
    } else {
        buff = "FALSE";
    }
}

AString AStringLibs::to_string(s16int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, s16int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    bool sign = false;
    if (num < 0) {
        num = -num;
        sign = true;
    }
    s16int size = sign;
    s16int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    if (sign) {
        buff[0] = '-';
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(u16int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, u16int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    u16int size = 0;
    u16int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(s32int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, s32int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    bool sign = false;
    if (num < 0) {
        num = -num;
        sign = true;
    }
    s32int size = sign;
    s32int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    if (sign) {
        buff[0] = '-';
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(u32int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, u32int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    u32int size = 0;
    u32int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(s64int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, s64int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    bool sign = false;
    if (num < 0) {
        num = -num;
        sign = true;
    }
    s64int size = sign;
    s64int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    if (sign) {
        buff[0] = '-';
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(u64int num) {
    AString res;
    to_string(res, num);
    return res;
}

void AStringLibs::to_string(AString & buff, u64int num) {
    AString res;
    if (num == 0) {
        buff = "0";
        return;
    }
    u64int size = 0;
    u64int temp = num;
    while (temp > 0) {
        temp = temp / 10;
        size += 1;
    }
    if (buff.length()  != size) {
        buff = AString::make(size);
    }
    while (num > 0) {
        s16int a = num % 10;
        num = num / 10;
        size -= 1;
        buff[size] = a + '0';
    }
}

AString AStringLibs::to_string(float num) {
    return to_string(num, 7);
}

AString AStringLibs::to_string(float num, s16int correct) {
    AString res;
    s16int i, j, a;
    i = correct;
    j = 0;
    if (num < 0) { // определяем знак числа
        num = -num;
        res = res.insert('-', j);
        j -= 1;
    }
    do { // преобразуем к нормированной форме (если не меньше 1)
        num = num / 10;
        i += 1;
    } while ((s32int)num > 0);
    num = num * 10;
    while (i > 0) {
        a = (s16int)num; // выделяем цифру, соответствующую целой части
        res = res.insert(a + '0', j);
        i -= 1;
        j += 1;
        if (i == correct) { // определяем положение десятичного разделителя
            res = res.insert('.', j);
            j += 1;
        }
        num = num - (float)a; // вычитаем из числа целую часть
        num = num * 10; // переходим к следующему разряду
    }
    return res;
}

AString AStringLibs::to_string(double num) {
    return to_string(num, 15);
}

AString AStringLibs::to_string(double num, s16int correct) { 
    AString res;
    s16int i, j, a;
    i = correct;
    j = 0;
    if (num < 0) { // определяем знак числа
        num = -num;
        res = res.insert('-', j);
        j += 1;
    }
    do { // преобразуем к нормированной форме (если не меньше 1)
        num = num / 10;
        i += 1;
    } while ((s32int)num > 0);
    num = num * 10;
    while (i > 0)
    {
        a = (s16int)num; // выделяем цифру, соответствующую целой части
        res = res.insert(a + '0', j);
        i -= 1;
        j += 1;
        if (i == correct) { // определяем положение десятичного разделителя
            res = res.insert('.', j);
            j += 1;
        }
        num = num - (double)a; // вычитаем из числа целую часть
        num = num * 10; // переходим к следующему разряду
    } 
    return res;
}

char  AStringLibs::to_char() {
    return to_char(string);
}

char  AStringLibs::to_char(AString str) {
    return to_char(str.to_cstring().const_data());
}

char  AStringLibs::to_char(char const * str) {
    char c;
    if (AString::length(str) > 1 || AString::length(str) < 1) {
        return c;
    }
    c = str[0];
    return c;
}

s32int  AStringLibs::to_int() {
    return to_int(string);
}

s32int  AStringLibs::to_int(AString str) {
    return to_int(str.to_cstring().const_data());
}

s32int  AStringLibs::to_int(char const * str) {
    if (test_int(str)) {
        s32int res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        res = (sign)? -res : res;
        return res;
    }
    if (test_hex(str)) {
        s32int res = 0;
        s32int pos = 2;
        while (str[pos] != '\0') {
            res = res*16 + from_hex_num(str[pos]);
            pos += 1;
        }
        return res;
    }
    return 0;
}

s64int AStringLibs::to_long() {
    return to_long(string);
}

s64int AStringLibs::to_long(AString str) {
    return to_long(str.to_cstring().const_data());
}   

s64int AStringLibs::to_long(char const * str) {
    if (test_int(str)) {
        s64int res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        res = (sign)? -res : res;
        return res;
    }
    if (test_hex(str)) {
        s64int res = 0;
        s32int pos = 2;
        while (str[pos] != '\0') {
            res = res*16 + from_hex_num(str[pos]);
            pos += 1;
        }
        return res;
    }
    return 0;
}

float AStringLibs::to_float() {
    return to_float(string);
}

float AStringLibs::to_float(AString str) {
    return to_float(str.to_cstring().const_data());
}   

float AStringLibs::to_float(char const * str) {
    if (test_float(str)) {
        float res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0' && str[pos] != '.') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        if (str[pos] == '.') {
            pos += 1;
            float a = 1;
            while (str[pos] != '\0' && str[pos] != '.') {
                res = res + a/10*(str[pos] - '0');
                a /= 10;
                pos += 1;
            }
        }
        return res;
    }
    return 0;
}

double AStringLibs::to_double() {
    return to_double(string);
}

double AStringLibs::to_double(AString str) {
    return to_double(str.to_cstring().const_data());
}   

double AStringLibs::to_double(char const * str) {
    if (test_float(str)) {
        double res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0' && str[pos] != '.') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        if (str[pos] == '.') {
            pos += 1;
            double a = 1;
            while (str[pos] != '\0' && str[pos] != '.') {
                res = res + a/10*(str[pos] - '0');
                a /= 10;
                pos += 1;
            }
        }
        return res;
    }
    return 0;
}

u8int  AStringLibs::to_uchar() {
    return to_uchar(string);
}

u8int  AStringLibs::to_uchar(AString str) {
    return to_uchar(str.to_cstring().const_data());
}

u8int  AStringLibs::to_uchar(char const * str) {
    u8int c;
    if (AString::length(str) > 1 || AString::length(str) < 1) {
        return c;
    }
    c = str[0];
    return c;
}

u32int  AStringLibs::to_uint() {
    return to_uint(string);
}

u32int  AStringLibs::to_uint(AString str) {
    return to_uint(str.to_cstring().const_data());
}

u32int  AStringLibs::to_uint(char const * str) {
    if (test_int(str)) {
        u32int res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        res = (sign)? -res : res;
        return res;
    }
    if (test_hex(str)) {
        u32int res = 0;
        s32int pos = 2;
        while (str[pos] != '\0') {
            res = res*16 + from_hex_num(str[pos]);
            pos += 1;
        }
        return res;
    }
    return 0;
}

u64int AStringLibs::to_ulong() {
    return to_ulong(string);
}

u64int AStringLibs::to_ulong(AString str) {
    return to_ulong(str.to_cstring().const_data());
}   

u64int AStringLibs::to_ulong(char const * str) {
    if (test_int(str)) {
        u64int res = 0;
        bool sign = false;
        s32int pos = 0;
        if (str[0] == '-') {
            pos = 1;
            sign = true;
        }
        if (str[0] == '+') {
            pos = 1;
        }
        while (str[pos] != '\0') {
            res = res*10 + str[pos] - '0';
            pos += 1;
        }
        res = (sign)? -res : res;
        return res;
    }
    if (test_hex(str)) {
        u64int res = 0;
        s32int pos = 2;
        while (str[pos] != '\0') {
            res = res*16 + from_hex_num(str[pos]);
            pos += 1;
        }
        return res;
    }
    return 0;
}

char AStringLibs::to_hex_num(char num) {
    if (num >= 0 && num < 10) {
        return num + '0';
    }
    if (num >= 10 && num < 16) {
        return num + 'A' - 10;
    }
    return '0';
}

AString AStringLibs::to_hex(s16int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

AString AStringLibs::to_hex(s32int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

AString AStringLibs::to_hex(s64int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

AString AStringLibs::to_hex(u16int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

AString AStringLibs::to_hex(u32int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

AString AStringLibs::to_hex(u64int num) {
    AString res;
    while (num >= 16) {
        res = AString(to_hex_num(num % 16)) + res;
        num/=16;
    }
    res = AString(to_hex_num(num)) + res;
    return res;
}

char AStringLibs::from_hex_num(char num) {
    if (num >= '0' && num <= '9') {
        return num - '0';
    }
    if (num >= 'A' && num <= 'F') {
        return num - 'A' + 10;
    }
    if (num >= 'a' && num <= 'f') {
        return num - 'a' + 10;
    }
    return 0;
}

bool AStringLibs::test_int() {
    return test_int(string);
}

bool AStringLibs::test_int(char const * str) {
    if (str == NULL) {
        return false;
    }
    s32int i = 0;
    s32int len = AString::length(str);
    if (len > 1 && (str[0] == '+' || str[0] == '-')) {
        i = 1;
    }
    for (; i < len; ++i) {
        if (str[i] < '0' || str[i] > '9') {
            return false;
        }
    }
    return true;
}

bool AStringLibs::test_int(AString str) {
    return test_int(str.to_cstring().const_data());
}

bool AStringLibs::test_hex() {
    return test_hex(string);
}

bool AStringLibs::test_hex(char const * str) {
    if (str == NULL) {
        return false;
    }
    s32int len = AString::length(str);
    if (len < 3) {
        return false;
    }
    if (str[0] != '0' || str[1] !=  'x' && str[1] !=  'X') {
        return false;
    }
    for (s32int i = 2; i < len; ++i) {
        if ((str[i] < '0' || str[i] > '9')
            && (str[i] < 'A' || str[i] > 'F')
            && (str[i] < 'a' || str[i] > 'f')) 
        {
            return false;
        }
    }
    return true;
}

bool AStringLibs::test_hex(AString str) {
    return test_hex(str.to_cstring().const_data());
}

bool AStringLibs::test_float() {
    return test_float(string);
}

bool AStringLibs::test_float(char const * str) {
    if (str == NULL) {
        return false;
    }
    s32int qty = 0;
    s32int i = 0;
    s32int len = AString::length(str);
    if (len > 1 && (str[0] == '+' || str[0] == '-')) {
        i = 1;
    }
    for (; i < len; ++i) {
        if ((str[i] < '0' || str[i] > '9') && str[i] != '.') {
            return false;
        }
        if (str[i] == '.') {
            qty += 1;
        }
        if (qty > 1) {
            return false;
        }
    }
    return true;
}

bool AStringLibs::test_float(AString str) {
    return test_float(str.to_cstring().const_data());
}

AStringLibs & AStringLibs::operator=(AStringLibs obj) {
    string = obj.string;
    return *this;
}

AStringLibs::~AStringLibs() {

}