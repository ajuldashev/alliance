#include "AStringLibs.hpp"

#ifdef DEBUG_ASTRINGLIBS
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_astringlibs() {
   AString com;
   AIO::write_div_line();
   AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRINGLIBS );
   com = AIO::get_command();
   while (com != "stop") {
       if  (com == "help") {
           AIO::write(debug_list_node_astringlibs());
			AIO::writeln(" 1) help");
            AIO::writeln(" 6) to_string_bool");
            AIO::writeln(" 7) to_string_s8int");
            AIO::writeln(" 8) to_string_u8int");
            AIO::writeln(" 9) to_string_s16int");
            AIO::writeln("10) to_string_u16int");
            AIO::writeln("11) to_string_s32int");
            AIO::writeln("12) to_string_u32int");
            AIO::writeln("13) to_string_s64int");
            AIO::writeln("14) to_string_u64int");
			AIO::writeln(" 0) stop");
			AIO::writeln(" e) exit");
		}
		if (com == "exit") {
			return true;
		}
        if (debug_node_astringlibs(com)) {
			return true;
		}
        if (com == "to_string_bool") {
            bool f = false;
            AIO::writeln(AStringLibs::to_string(f));
        }
        if (com == "to_string_s8int") {
            s8int num = AIO::get_s8int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_u8int") {
            u8int num = AIO::get_u8int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_s16int") {
            s16int num = AIO::get_s16int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_u16int") {
            u16int num = AIO::get_u16int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_s32int") {
            s32int num = AIO::get_s32int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_u32int") {
            u32int num = AIO::get_u32int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_s64int") {
            s64int num = AIO::get_s64int();
            AIO::writeln(AStringLibs::to_string(num));
        }
        if (com == "to_string_u64int") {
            u64int num = AIO::get_u64int();
            AIO::writeln(AStringLibs::to_string(num));
        }
       AIO::write_div_line();
       com = AIO::get_command();
   }
   AIO::writeln("End debug the module " MODULE_DEBUG_ASTRINGLIBS );
   AIO::write_div_line();
   return false;
}
#endif

