#include "AStringLibs.hpp"

#ifdef DEBUG_ASTRINGLIBS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_astringlibs(AString com) {
	#ifdef DEBUG_ASTRINGFORMAT
	if (com == "astringformat") {
		if (debug_astringformat()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRINGINDEXMAP
	if (com == "astringindexmap") {
		if (debug_astringindexmap()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRINGITERATOR
	if (com == "astringiterator") {
		if (debug_astringiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRINGLIST
	if (com == "astringlist") {
		if (debug_astringlist()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRINGMAP
	if (com == "astringmap") {
		if (debug_astringmap()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_astringlibs() {
	AString text;
	#ifdef DEBUG_ASTRINGFORMAT
	text += "d) astringformat\n";
	#endif
	#ifdef DEBUG_ASTRINGINDEXMAP
	text += "d) astringindexmap\n";
	#endif
	#ifdef DEBUG_ASTRINGITERATOR
	text += "d) astringiterator\n";
	#endif
	#ifdef DEBUG_ASTRINGLIST
	text += "d) astringlist\n";
	#endif
	#ifdef DEBUG_ASTRINGMAP
	text += "d) astringmap\n";
	#endif
	return text;
}

#endif
