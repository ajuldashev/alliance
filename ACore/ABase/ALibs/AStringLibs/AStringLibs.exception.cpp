#include "AStringLibs.exception.hpp"

AStringLibsException::AStringLibsException() : AllianceException() {
	exception = "Alliance-Exception-AStringLibs";
}

AStringLibsException::AStringLibsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringLibs");
}

AStringLibsException::AStringLibsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringLibs");
}

AStringLibsException::AStringLibsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringLibs");
}

AStringLibsException::AStringLibsException(const AStringLibsException & obj) : AllianceException(obj) {
	
}

AStringLibsException::~AStringLibsException() {

}

