#ifndef HPP_ASTRINGLIBSEXCEPTION
#define HPP_ASTRINGLIBSEXCEPTION
#include <alliance.exception.hpp>

class AStringLibsException : public AllianceException {
public:
	AStringLibsException();
	AStringLibsException(std::string text);
	AStringLibsException(AllianceException & e);
	AStringLibsException(AllianceException & e, std::string text);
	AStringLibsException(const AStringLibsException & obj);
	~AStringLibsException();
};

#endif

