#ifndef HPP_ASTRINGLIBS
#define HPP_ASTRINGLIBS
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AStruct/AArray/AArray.hpp>
#include "AStringLibs.exception.hpp"

class AStringLibs {
private:
    AString string;
public:
    AStringLibs();
    AStringLibs(AString);
    AStringLibs(const AStringLibs &);
    void set(AString);
    AString get();
    
    AArray<AString> split(char);

    static AString  to_string(bool);
    static void     to_string(AString &, bool);
    static AString  to_string(s16int);
    static void     to_string(AString &, s16int);
    static AString  to_string(u16int);
    static void     to_string(AString &, u16int);
    static AString  to_string(s32int);
    static void     to_string(AString &, s32int);
    static AString  to_string(u32int);
    static void     to_string(AString &, u32int);
    static AString  to_string(s64int);
    static void     to_string(AString &, s64int);  
    static AString  to_string(u64int);
    static void     to_string(AString &, u64int);

    static AString  to_string(float);
    static AString  to_string(float, s16int);
    static AString  to_string(double);
    static AString  to_string(double, s16int);  

    char to_char();
    static char to_char(AString);
    static char to_char(char const *);

    s32int to_int();
    static s32int to_int(AString);
    static s32int to_int(char const *);

    s64int to_long();
    static s64int to_long(AString);    
    static s64int to_long(char const *);

    float to_float();
    static float to_float(AString);
    static float to_float(char const *);

    double to_double();
    static double to_double(AString);    
    static double to_double(char const *);

    u8int to_uchar();
    static u8int to_uchar(AString);
    static u8int to_uchar(char const *);

    u32int to_uint();
    static u32int to_uint(AString);
    static u32int to_uint(char const *);

    u64int to_ulong();
    static u64int to_ulong(AString);    
    static u64int to_ulong(char const *);

    static char to_hex_num(char);
    static char from_hex_num(char);

    static AString to_hex(s16int);
    static AString to_hex(s32int);
    static AString to_hex(s64int);
    static AString to_hex(u16int);
    static AString to_hex(u32int);
    static AString to_hex(u64int);

    bool test_int();
    static bool test_int(char const *);
    static bool test_int(AString);

    bool test_hex();
    static bool test_hex(char const *);
    static bool test_hex(AString);

    bool test_float();
    static bool test_float(char const *);
    static bool test_float(AString);

    AStringLibs & operator=(AStringLibs);
    ~AStringLibs();
};

#endif