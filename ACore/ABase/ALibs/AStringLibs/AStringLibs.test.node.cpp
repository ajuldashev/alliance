#include "AStringLibs.hpp"

#ifdef TEST_ASTRINGLIBS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_astringlibs(bool is_soft) {
	#ifdef TEST_ASTRINGFORMAT
	if (!test_astringformat(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGFORMAT );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRINGINDEXMAP
	if (!test_astringindexmap(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGINDEXMAP );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRINGITERATOR
	if (!test_astringiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRINGLIST
	if (!test_astringlist(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGLIST );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRINGMAP
	if (!test_astringmap(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGMAP );
		return is_soft;
	}
	#endif
	return true;
}
#endif
