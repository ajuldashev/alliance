#include "AStringList.hpp"
#include "AStringList.test.cpp"
#include "AStringList.test.node.cpp"
#include "AStringList.debug.cpp"
#include "AStringList.debug.node.cpp"
#include "AStringList.exception.cpp"

AStringList::AStringList() {

}

AStringList::AStringList(const AStringList & obj) {
    AList<AString> new_arr(obj.list);
    list = new_arr;
}

void AStringList::copy(AStringList & obj) {
    AList<AString> new_arr(obj.list);
    list = new_arr;
}

u64int AStringList::length() {
    return list.length();
}

void AStringList::insert(AString str, s64int pos) {
    list.insert(str, pos);
}
void AStringList::insert(AStringList & l , s64int pos) {
    AList<AString> new_list(l.list);
    list.insert(new_list, pos);
}

void AStringList::push_beg(AString str) {
    list.push_beg(str);
} 

void AStringList::push_end(AString str) {
    list.push_end(str);
}

AString & AStringList::at(s64int pos) {
    return list.at(pos);
}

AString AStringList::get(s64int pos) {
    return list.get(pos);
}

AString AStringList::pop(s64int pos) {
    return list.pop(pos);
}

AStringList AStringList::get(s64int pos, s64int length) {
    AList<AString> l(list.get(pos, length));
    AStringList res;
    res.list = l;
    return res;
}

AStringList AStringList::sub(s64int pos, s64int length) {
    AList<AString> l(list.sub(pos, length));
    AStringList res;
    res.list = l;
    return res;
}

void AStringList::remove(s64int pos, s64int length) {
    list.remove(pos, length);
}

AString AStringList::pop_beg() {
    return pop_beg();
}

AString AStringList::pop_end() {
    return pop_end();
}

AString & AStringList::operator[](s64int pos) {
    return list.at(pos);
}

AStringList & AStringList::operator=(AStringList & obj) {
    copy(obj);
    return *this;
}

void AStringList::clear() {
    list.clear();
}

AStringList::~AStringList() {
    clear();
}