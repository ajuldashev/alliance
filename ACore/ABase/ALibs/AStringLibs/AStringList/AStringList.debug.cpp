#include "AStringList.hpp"

#ifdef DEBUG_ASTRINGLIST
#include <ACore/ABase/AIO/AIO.hpp>

void write_list(AStringList & list) {
    AConsole console;
    console.writeln("List : ");
    for (u64int i = 0; i < list.length(); i += 1) {
        console.writeln(list[i]);
    }
}

bool debug_astringlist() {
    AStringList list;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRINGLIST );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astringlist());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) length");
            AIO::writeln(" 3) insert");
            AIO::writeln(" 4) insert_list");
            AIO::writeln(" 5) push_beg");
            AIO::writeln(" 6) push_end");
            AIO::writeln(" 7) at");
            AIO::writeln(" 8) get");
            AIO::writeln(" 9) pop");
            AIO::writeln("10) get_list");
            AIO::writeln("11) sub_list");
            AIO::writeln("12) remove");
            AIO::writeln("13) pop_beg");
            AIO::writeln("14) pop_end");
            AIO::writeln("15) clear");
            AIO::writeln("16) =");
            AIO::writeln("17) status");
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astringlist(com)) {
			return true;
		}
        if (com == "length") {
            AIO::writeln(list.length());
        }
        if (com == "insert") {
            AString str = AIO::get_word();
            u64int pos  = AIO::get_s32int();
            list.insert(str, pos);
        }
        if (com == "insert_arr") {
            AStringList list_s;
            u64int size = AIO::get_s32int();
            for (u64int i = 0; i < size; i += 1) {
                list_s.push_end(AIO::get_word());
            }
            u64int pos = AIO::get_s32int();
            list.insert(list_s, pos);
        }
        if (com == "push_beg") {
            list.push_beg(AIO::get_word());
        }
        if (com == "push_end") {
            list.push_end(AIO::get_word());
        }
        if (com == "at") {
            AIO::writeln(list.at(AIO::get_s32int()));
        }
        if (com == "get") {
            AIO::writeln(list.get(AIO::get_s32int()));
        }
        if (com == "pop") {
            AIO::writeln(list.pop(AIO::get_s32int()));
        }
        if (com == "get_arr") {
            u64int pos = AIO::get_s32int();
            u64int length = AIO::get_s32int();
            AStringList a = list.get(pos, length);
            write_list(a);
        }
        if (com == "sub_arr") {
            u64int pos = AIO::get_s32int();
            u64int length = AIO::get_s32int();
            AStringList a = list.sub(pos, length);
            write_list(a);
        }
        if (com == "remove") {
            u64int pos = AIO::get_s32int();
            u64int length = AIO::get_s32int();
            list.remove(pos, length);
        }
        if (com == "pop_beg") {
            AIO::writeln(list.pop_beg());
        }
        if (com == "pop_end") {
            AIO::writeln(list.pop_end());
        }
        if (com == "clear") {
            list.clear();
        }
        if (com == "=") {
            AStringList a;
            list = a;
        }
        if (com == "status") {
            write_list(list);
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRINGLIST );
    AIO::write_div_line();
    return false;
}
#endif

