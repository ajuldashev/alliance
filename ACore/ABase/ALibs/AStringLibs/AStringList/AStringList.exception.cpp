#include "AStringList.exception.hpp"

AStringListException::AStringListException() : AllianceException() {
	exception = "Alliance-Exception-AStringList";
}

AStringListException::AStringListException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringList");
}

AStringListException::AStringListException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringList");
}

AStringListException::AStringListException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringList");
}

AStringListException::AStringListException(const AStringListException & obj) : AllianceException(obj) {
	
}

AStringListException::~AStringListException() {

}

