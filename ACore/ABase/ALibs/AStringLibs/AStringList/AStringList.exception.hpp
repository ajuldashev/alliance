#ifndef HPP_ASTRINGLISTEXCEPTION
#define HPP_ASTRINGLISTEXCEPTION
#include <alliance.exception.hpp>

class AStringListException : public AllianceException {
public:
	AStringListException();
	AStringListException(std::string text);
	AStringListException(AllianceException & e);
	AStringListException(AllianceException & e, std::string text);
	AStringListException(const AStringListException & obj);
	~AStringListException();
};

#endif

