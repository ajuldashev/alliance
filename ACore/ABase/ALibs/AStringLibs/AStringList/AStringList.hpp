#ifndef HPP_ASTRINGLIST
#define HPP_ASTRINGLIST
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include <ACore/ABase/AStruct/AArray/AArray.hpp>
#include "AStringList.exception.hpp"

class AStringList {
private:
    AList<AString> list;
public:
    AStringList();
    AStringList(const AStringList &);
    void copy(AStringList &);
    u64int length();

    void insert(AString, s64int);
	void insert(AStringList & , s64int);
	void push_beg(AString); 
	void push_end(AString);

	AString & at(s64int);
	AString get(s64int);
	AString pop(s64int);
	AStringList get(s64int, s64int);
	AStringList sub(s64int, s64int);
	void remove(s64int, s64int);

	AString pop_beg();
	AString pop_end();

    AString & operator[](s64int);
	AStringList & operator=(AStringList&);

    void clear();
    ~AStringList();
};

#endif