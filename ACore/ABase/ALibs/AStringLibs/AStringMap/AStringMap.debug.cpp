#include "AStringMap.hpp"

#ifdef DEBUG_ASTRINGMAP
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_astringmap() {
    AStringMap<s64int> map;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRINGMAP );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astringmap());
            AIO::writeln("1) help");
            AIO::writeln("2) add");
            AIO::writeln("3) get");
            AIO::writeln("4) find");
            AIO::writeln("5) print");
            AIO::writeln("0) stop");	
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astringmap(com)) {
			return true;
		}
        if (com == "add") {
            AString num = AIO::get_word();
            map.add(num, 0);
        }
        if (com == "get") {
            s64int num = AIO::get_s64int();
            AIO::writeln(map.get(num).to_string());
        }
        if (com == "find") {
            AString num = AIO::get_word();
            AIO::writeln(map.find(num));
        }
        if (com == "print") {
            map.print();
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRINGMAP );
    AIO::write_div_line();
    return false;
}
#endif

