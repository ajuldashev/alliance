#include "AStringMap.exception.hpp"

AStringMapException::AStringMapException() : AllianceException() {
	exception = "Alliance-Exception-AStringMap";
}

AStringMapException::AStringMapException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringMap");
}

AStringMapException::AStringMapException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringMap");
}

AStringMapException::AStringMapException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringMap");
}

AStringMapException::AStringMapException(const AStringMapException & obj) : AllianceException(obj) {
	
}

AStringMapException::~AStringMapException() {

}

