#ifndef HPP_ASTRINGMAPEXCEPTION
#define HPP_ASTRINGMAPEXCEPTION
#include <alliance.exception.hpp>

class AStringMapException : public AllianceException {
public:
	AStringMapException();
	AStringMapException(std::string text);
	AStringMapException(AllianceException & e);
	AStringMapException(AllianceException & e, std::string text);
	AStringMapException(const AStringMapException & obj);
	~AStringMapException();
};

#endif

