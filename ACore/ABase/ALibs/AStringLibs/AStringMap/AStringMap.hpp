#ifndef HPP_ASTRINGMAP
#define HPP_ASTRINGMAP
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/AMath/AMath.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include "AStringMap.exception.hpp"

template<typename T>
class AStringMap {
public:
    class Node;
private:
    Node * root;
private:
    bool add_rec(Node * node, AStringIterator key, T & data);
    bool add_left (Node * node, AStringIterator key, T & data);
    bool add_left_next (Node * node, AStringIterator key, T & data);
    bool add_right(Node * node, AStringIterator key, T & data);
    bool add_right_next(Node * node, AStringIterator key, T & data);
    bool add_next(Node * node, AStringIterator key, T & data);
    bool try_rotation_if_added(Node * node, bool f);
    bool add(AStringIterator key, T data);
public:
    bool add(AString key, T data);
private:
    s64int find(Node * node, AStringIterator key, s64int pos);
    s64int find(AStringIterator key, s64int pos);
public:
    s64int find(AString key);
private:
    APtr<T> get(Node * node, AStringIterator key, s64int pos);
    APtr<T> get(AStringIterator key, s64int pos);
public:
    APtr<T> get(AString key);
private:
    AString get_key(Node * node, u64int index, s64int pos);
    AString get_key(u64int index, s64int pos);
public:
    AString get_key(u64int index);
private:    
    bool test_left(Node *);
    bool test_right(Node *);
    void rotation_left(Node **);
    void rotation_right(Node **);
    void update_count(Node *);
    s64int size();
public:
    AStringMap();

    ASymbol get(s64int);

    s64int count();

    //AString get_key (u64int index);
    APtr<T> get_data(u64int index);

    bool remove(AString key);

    AString ftab(s64int);
    void print(Node * node = NULL, s64int tab = 0);

    ~AStringMap();
};

template<typename T>
class AStringMap<T>::Node {
public:
    Node();

    Node * left;
    Node * right;
    Node * parent;

    AStringMap map;

    s64int  size;

    s64int count;

    ASymbol symbol;

    APtr<T> data;

    ~Node();
};

template<typename T>
AStringMap<T>::Node::Node() {
    left = NULL;
    right = NULL;
    parent = NULL;
    size = 0;
    count = 0;
}

template<typename T>
AStringMap<T>::Node::~Node() {
    if (left) {
        delete left;
    }
    if (right) {
        delete right;
    }
    size = 0;
}

template<typename T>
bool AStringMap<T>::add_rec(Node * node, AStringIterator key, T & data) {
    if (node) {
        if (node->symbol > key.get()) {
            return add_left(node, key, data);
        }
        if (node->symbol < key.get()) {
            return add_right(node, key, data);
        }
        if (key.has_next() || key.is_last()) {
            return add_next(node, key, data);
        }
    }
    return false;
}


template<typename T>
bool AStringMap<T>::add_left(Node * node, AStringIterator key, T & data) {
    if (node->left == NULL) {
        node->left = new Node();
        node->size += 1;
        node->left->size = 1;
        node->left->symbol = key.get();
        node->left->parent = node;
        node->left->count += 1;
        if (key.is_last()) {
            node->count += 1;
            node->left->data = APtr<T>::make(); 
            node->left->data.update(data);
            return true;
        }
        return add_left_next(node, key, data);
    }
    return try_rotation_if_added(node, add_rec(node->left, key, data));
}

template<typename T>
bool AStringMap<T>::add_left_next(Node * node, AStringIterator key, T & data) {
    key.next();
    if (node->left->map.add(key, data)) {
        node->count += 1;
        return true;
    }
    return false;
}

template<typename T>
bool AStringMap<T>::add_right(Node * node, AStringIterator key, T & data) {
    if (node->right == NULL) {
        node->right = new Node();
        node->size += 1;
        node->right->size = 1;
        node->right->symbol = key.get();
        node->right->parent = node;
        node->right->count += 1;
        if (key.is_last()) {
            node->count += 1;
            node->right->data = APtr<T>::make(); 
            node->right->data.update(data);
            return true;
        }
        return add_right_next(node, key, data);
    }
    return try_rotation_if_added(node, add_rec(node->right, key, data));
}

template<typename T>
bool AStringMap<T>::add_right_next(Node * node, AStringIterator key, T & data) {
    key.next();
    if (node->right->map.add(key, data)) {
        node->count += 1;
        return true;
    }
    return false;
}

template<typename T>
bool AStringMap<T>::add_next(Node * node, AStringIterator key, T & data) {
    if (key.is_last()) {
        if (node->data) {
            return false;
        }
        node->count += 1;
        node->data = APtr<T>::make(); 
        node->data.update(data);
    } else {
        key.next();
        bool f = node->map.add(key, data);
        if (f) {
            node->count += 1;
        } 
        return f; 
    }
    return true;
}

template<typename T>
bool AStringMap<T>::try_rotation_if_added(Node * node, bool added) {
    if (added) {
        node->size += 1;
        node->count += 1;
        if (test_left(node)) {
            if (node == root) {
                rotation_left(&node);
                root = node;
            } else {
                rotation_left(&node);
            }
        } else
        if (test_right(node)) {
            if (node == root) {
                rotation_right(&node);
                root = node;
            } else {
                rotation_right(&node);
            }
        }
    }
    return added;
}

template<typename T>
bool AStringMap<T>::add(AStringIterator key, T data) {
    if (root == NULL 
        && (
            key.has_next() || 
            key.is_last()
        )
    ) {
        root = new Node();
        root->size = 1;
        root->symbol = key.get();
        root->count = 1;
        if (key.is_last()) {
            root->data = APtr<T>::make(); 
            root->data.update(data);
        } 
        if (key.has_next()) {
            key.next();
            root->map.add(key, data);
        }
        return true;
    }
    return add_rec(root, key, data);
}

template<typename T>
bool AStringMap<T>::add(AString key, T data) {
    AStringIterator iterator(key);
    return add(iterator, data);
}

template<typename T>
s64int AStringMap<T>::find(Node * node, AStringIterator key, s64int pos) {
    if (node) {
        if (node->symbol < key.get()) {
            pos += node->count;
            if (node->right) {
                pos -= node->right->count;
            }
            return find(node->right, key, pos);
        }
        if (node->symbol > key.get()) {
            return find(node->left, key, pos);
        }
        if (node->left) {
            pos += node->left->count;
        }
        if (!key.has_next()) {
            return (node->data && (node->symbol == key.get()))? pos : -1;
        }
        key.next();
        return node->map.find(key, pos);
    }
    return -1;
}

template<typename T>
s64int AStringMap<T>::find(AStringIterator key, s64int pos) {
    return find(root, key, pos);
}

template<typename T>
s64int AStringMap<T>::find(AString key) {
    AStringIterator iterator(key);
    return find(iterator, 0);
}

template<typename T>
APtr<T> AStringMap<T>::get(Node * node, AStringIterator key, s64int pos) {
    if (node) {
        if (node->symbol < key.get()) {
            pos += node->count;
            if (node->right) {
                pos -= node->right->count;
            }
            return get(node->right, key, pos);
        }
        if (node->symbol > key.get()) {
            return get(node->left, key, pos);
        }
        if (node->left) {
            pos += node->left->count;
        }
        if (!key.has_next()) {
            return (node->data && (node->symbol == key.get()))? node->data : APtr<T>();
        }
        key.next();
        return node->map.get(key, pos);
    }
    return APtr<T>();
}

template<typename T>
APtr<T> AStringMap<T>::get(AStringIterator key, s64int pos) {
    return get(root, key, pos);
}

template<typename T>
APtr<T> AStringMap<T>::get(AString key) {
    AStringIterator iterator(key);
    return get(iterator, 0);
}

template<typename T>
AString AStringMap<T>::get_key(Node * node, u64int index, s64int pos) {
    if (node) {
        // if (node->symbol < key.get()) {
        //     pos += node->count;
        //     if (node->right) {
        //         pos -= node->right->count;
        //     }
        //     return get_key(node->right, index, pos);
        // }
        // if (node->symbol > key.get()) {
        //     return get_key(node->left, index, pos);
        // }
        // if (node->left) {
        //     pos += node->left->count;
        // }
        // if (!key.has_next()) {
        //     return (node->data && (index == pos))? node->symbol.to_string() : AString();
        // }
        // key.next();
        return node->symbol.to_string() + node->map.get_key(index, pos);
    }
    return AString();
}

template<typename T>
AString AStringMap<T>::get_key(u64int index, s64int pos) {
    return get_key(root, index, pos);
}

template<typename T>
AString AStringMap<T>::get_key(u64int index) {
    return get_key(index, 0);
}


template<typename T>
ASymbol AStringMap<T>::get(s64int pos) {
    if (root) {
        if (pos >= 0 && pos < root->size) {
            Node * node = root;
            if (pos == 0 && !node->left) {
                return node->symbol;
            }
            while (pos >= 0) {
                if (node->left && pos == node->left->size) {
                    return node->symbol;
                }
                if (node->left && pos < node->left->size) {
                    node = node->left;
                    continue;
                }
                if (node->left && node->right) {
                    pos -= node->left->size + 1;
                    node = node->right;
                    continue;
                }
                if (pos == 0 && !node->left) {
                    return node->symbol;
                }
                if (node->right) {
                    pos -= 1;
                    node = node->right;
                    continue;
                }
                return node->symbol;
            }
        }
    }    
    return ASymbol();
}

template<typename T>
bool AStringMap<T>::test_left(Node * node) {
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->right && node->right->right) {
            new_right = node->right->right->size;
        }
        new_left = node->size - 1 - new_right;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template<typename T>
bool AStringMap<T>::test_right(Node *node) { 
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->left && node->left->left) {
            new_left = node->left->left->size;
        }
        new_right = node->size - 1 - new_left;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template<typename T>
void AStringMap<T>::rotation_left(Node ** node) {
    if (node && *node) {
        Node * pnode = *node; 
        if (pnode->right) {
            s64int size = pnode->size;
            Node * rnode = pnode->right;
            if (rnode->left) {
                rnode->left->parent = pnode;
            }
            pnode->right = rnode->left;
            rnode->left = pnode;
            rnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = rnode;
                } else {
                    pnode->parent->right = rnode;
                }
            }
            pnode->parent = rnode;
            pnode = rnode;
            pnode->size = size;
            pnode->left->size = size - 1;
            update_count(pnode->left);
            update_count(pnode);
            if (pnode->right) {
                pnode->left->size -= pnode->right->size;
            }
            (*node) = pnode;
        }
    }
}

template<typename T>
void AStringMap<T>::rotation_right(Node ** node) {
    if (node && *node) {
        Node * pnode = *node; 
        if (pnode->left) {
            s64int size = pnode->size;
            Node * lnode = pnode->left;
            if (lnode->right) {
                lnode->right->parent = pnode;
            }
            pnode->left = lnode->right;
            lnode->right = pnode;
            lnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = lnode;
                } else {
                    pnode->parent->right = lnode;
                }
            }
            pnode->parent = lnode;
            pnode = lnode;
            pnode->size = size;
            pnode->right->size = size - 1;
            update_count(pnode->right);
            update_count(pnode);
            if (pnode->left) {
                pnode->right->size -= pnode->left->size;
            }
            (*node) = pnode;
        }
    }
}

template<typename T>
void AStringMap<T>::update_count(Node * node) {
    if (node) {
        s64int count_map = node->map.count();
        s64int count_left = (node->left)? node->left->count : 0;
        s64int count_right = (node->right)? node->right->count : 0;;
        s64int count_is_end = node->data.is_present();
        node->count = count_map + count_left + count_right + count_is_end;
    }
}

template<typename T>
AStringMap<T>::AStringMap() {
    root = NULL;
}

template<typename T>
s64int AStringMap<T>::size() {
    if (root) {
        return root->size;
    }
    return 0;
}

template<typename T>
s64int AStringMap<T>::count() {
    if (root) {
        return root->count;
    }
    return 0;
}

template<typename T>
AString AStringMap<T>::ftab(s64int tab) {
    AString res = "";
    for (s64int i = 0; i < tab; i += 1) {
        res = res + "  ";
    }
    return res;
}

template<typename T>
void AStringMap<T>::print(Node * node, s64int tab) {
    if (!node) {
        node = root;
    }
    if (node) {
        AString data = node->symbol.to_string();
        AIO::write(ftab(tab)); 
        AIO::write(data);
        AIO::write(" ");
        AIO::write(node->data.is_present()); 
        AIO::write(" ");
        AIO::write(node->count);
        AIO::writeln();
        AIO::write(ftab(tab + 2)); 
        AIO::write("-----"); 
        AIO::writeln(); 
        node->map.print(NULL, tab + 2);
        AIO::write(ftab(tab + 2)); 
        AIO::write("-----"); 
        AIO::writeln(); 
        if (node->left || node->right) {
            if (node->left) {
                AIO::write(ftab(tab + 1));
                AIO::write("L "); 
                AIO::writeln();
                print(node->left, tab + 1);
            }
            if (node->right) {
                AIO::write(ftab(tab + 1));
                AIO::write("R "); 
                AIO::writeln();
                print(node->right, tab + 1);
            }
        }
        return;
    } 
}

template<typename T>
AStringMap<T>::~AStringMap() {
    if (root) {
        delete root;
    }
}

#endif