#include "AStringMap.hpp"

#ifdef TEST_ASTRINGMAP
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
      AStringMap<s64int> map;
      s32int N = 1000;
      AString buff;
      {
         for (s32int i = N/10; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            map.add(buff, 0);
         }
      }
      {
         for (s32int i = N/10; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            if (map.find(buff) != i - N/10) {
               /** <doc-test-info>
                *
                */
               TEST_ALERT(false, 1)
            }
         }
      }
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
      AStringMap<AString> map;
      
      map.add("key 1", "data 1");

      APtr<AString> data = map.get("key 1");

      /** <doc-test-info>
		 *
		 */
      TEST_ALERT(data, 1)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(*data == "data 1", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
      AStringMap<AString> map;
      
      map.add("key 1", "data 1");
      map.add("key 2", "data 2");
      map.add("key 3", "data 3");

      APtr<AString> data = map.get("key 2");

      /** <doc-test-info>
		 *
		 */
      TEST_ALERT(data, 1)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(*data == "data 2", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
      AStringMap<AString> map;
      
      map.add("key 1", "data 1");
      map.add("key 2", "data 2");
      map.add("key 3", "data 3");

      APtr<AString> data = map.get("key 4");

      /** <doc-test-info>
		 *
		 */
      TEST_ALERT(!data, 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
		/** <doc-test-info>
		 *
		 */
      AStringMap<AString> map;
      s32int N = 1000;
      AString buff;
      {
         for (s32int i = 0; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            map.add(buff + "_key", buff + "_data");
         }
      }
      {
         for (s32int i = 0; i < N; i += 1) {
            AStringLibs::to_string(buff, i);
            if (*map.get(buff + "_key") != buff + "_data") {
               /** <doc-test-info>
                *
                */
               TEST_ALERT(false, 1)
            }
         }
      }
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(astringmap)

	TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)

TEST_FUNCTION_MAIN_END(ASTRINGMAP)

#endif

