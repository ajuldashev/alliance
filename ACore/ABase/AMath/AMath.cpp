#include "AMath.hpp"
#include "AMath.test.cpp"
#include "AMath.test.node.cpp"
#include "AMath.debug.cpp"
#include "AMath.debug.node.cpp"
#include "AMath.exception.cpp"

namespace cstdlib {
    double (*_cos)(double) = cos;
    double (*_sin)(double) = sin;
    double (*_tan)(double) = tan;
    double (*_acos)(double) = acos;
    double (*_asin)(double) = asin;
    double (*_atan)(double) = atan;
    double (*_exp)(double) = exp;
    double (*_log)(double) = log;
    double (*_pow)(double, double) = pow;
    double (*_sqrt)(double) = sqrt;
    double (*_ceil)(double) = ceil;
    double (*_floor)(double) = floor;
};

double AMath::cos(double value) {
	return cstdlib::_cos(value);
}

double AMath::sin(double value) {
	return cstdlib::_sin(value);
}

double AMath::tan(double value) {
	return cstdlib::_tan(value);
}

double AMath::acos(double value) {
	return cstdlib::_acos(value);
}

double AMath::asin(double value) {
	return cstdlib::_asin(value);
}

double AMath::atan(double value) {
	return cstdlib::_atan(value);
}

double AMath::exp(double value) {
	return cstdlib::_exp(value);
}

double AMath::log(double value) {
	return cstdlib::_log(value);
}

double AMath::pow(double value1, double value2) {
	return cstdlib::_pow(value1, value2);
}

double AMath::sqrt(double value) {
	return cstdlib::_sqrt(value);
}
template<typename T>
T AMath::abs(T value) {
	return (value < 0)? -value : value;
}

double AMath::ceil(double value) {
	return cstdlib::_ceil(value);
}

double AMath::floor(double value) {
	return cstdlib::_floor(value);
}

double AMath::max(double value1, double value2) {
	if(value1 > value2) {
		return value1;
	} else {
		return value2; 
	}
}

double AMath::min(double value1, double value2) {
	if(value1 < value2) {
		return value1;
	} else {
		return value2; 
	}
}

double AMath::random() {
	long last = time(NULL);
	last ^= (last << 21);
	last ^= (last >> 35);
	last ^= (last << 4);
	double out = (double) ((int) last % 100)/100;     
	return (out < 0) ? -out : out;
}

s32int AMath::random_int(s32int max) {
	long last = time(NULL);
	last ^= (last << 21);
	last ^= (last >> 35);
	last ^= (last << 4);
	int out = (int) last % max;     
	return (out < 0) ? -out : out;
}
