#include "AMath.hpp"

#ifdef DEBUG_AMATH
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_amath() {
   AString com;
   AIO::write_div_line();
   AIO::writeln("Begin debug the module " MODULE_DEBUG_AMATH );
   com = AIO::get_command();
   while (com != "stop") {
       if  (com == "help") {
           AIO::write(debug_list_node_amath());
            AIO::writeln("1) help");
            AIO::writeln("2) cos");
            AIO::writeln("3) sin");
            AIO::writeln("4) tan");
            AIO::writeln("5) acos");
			AIO::writeln("6) asin");
			AIO::writeln("7) atan");
			AIO::writeln("8) exp");
			AIO::writeln("9) log");
			AIO::writeln("10) pow");
			AIO::writeln("11) sqrt");
			AIO::writeln("12) abs");
			AIO::writeln("13) ceil");
			AIO::writeln("14) floor");
			AIO::writeln("15) max");
			AIO::writeln("16) min");
			AIO::writeln("17) random");
			AIO::writeln("18) random_int");
            AIO::writeln("0) stop");
			AIO::writeln("e) exit");	
        }
		if (com == "exit") {
			return true;
        }
        if (debug_node_amath(com)) {
			return true;
		}
        if (com == "cos") {
			AIO::write("value: ");
            AIO::writeln(AMath::cos(AIO::get_double()));
        }
        if (com == "sin") {
            AIO::write("value: ");
            AIO::writeln(AMath::sin(AIO::get_double()));
        }
		if (com == "tan") {
            AIO::write("value: ");
            AIO::writeln(AMath::tan(AIO::get_double()));
        }
		if (com == "acos") {
            AIO::write("value: ");
            AIO::writeln(AMath::acos(AIO::get_double()));
        }
		if (com == "asin") {
            AIO::write("value: ");
            AIO::writeln(AMath::asin(AIO::get_double()));
        }
		if (com == "atan") {
            AIO::write("value: ");
            AIO::writeln(AMath::atan(AIO::get_double()));
        }
		if (com == "exp") {
            AIO::write("value: ");
            AIO::writeln(AMath::exp(AIO::get_double()));
        }
		if (com == "log") {
            AIO::write("value: ");
            AIO::writeln(AMath::log(AIO::get_double()));
        }
		if (com == "pow") {
            AIO::write("value: ");
            AIO::writeln(AMath::pow(AIO::get_double(), AIO::get_double()));
        }
        if (com == "sqrt") {
            AIO::write("value: ");
            AIO::writeln(AMath::sqrt(AIO::get_double()));
        }
		if (com == "abs") {
            AIO::write("value: ");
            AIO::writeln(AMath::abs(AIO::get_s32int()));
        }
		if (com == "ceil") {
            AIO::write("value: ");
            AIO::writeln(AMath::ceil(AIO::get_double()));
        }
		if (com == "floor") {
            AIO::write("value: ");
            AIO::writeln(AMath::floor(AIO::get_double()));
        }
		if (com == "max") {
            AIO::write("value: ");
            AIO::writeln(AMath::max(AIO::get_double(), AIO::get_double()));
        }
		if (com == "min") {
            AIO::write("value: ");
            AIO::writeln(AMath::min(AIO::get_double(), AIO::get_double()));
        }
		if (com == "random") {
            AIO::write("value: ");
            AIO::writeln(AMath::random());
        }
		if (com == "random_int") {
            AIO::write("value: ");
            AIO::writeln(AMath::random_int(AIO::get_s32int()));
        }
		AIO::write_div_line();
		com = AIO::get_command();
   }
   AIO::writeln("End debug the module " MODULE_DEBUG_AMATH );
   AIO::write_div_line();
   return false;
}
#endif

