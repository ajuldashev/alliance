#include "AMath.exception.hpp"

AMathException::AMathException() : AllianceException() {
	exception = "Alliance-Exception-AMath";
}

AMathException::AMathException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AMath");
}

AMathException::AMathException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AMath");
}

AMathException::AMathException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AMath");
}

AMathException::AMathException(const AMathException & obj) : AllianceException(obj) {
	
}

AMathException::~AMathException() {

}

