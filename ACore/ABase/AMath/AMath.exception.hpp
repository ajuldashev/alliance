#ifndef HPP_AMATHEXCEPTION
#define HPP_AMATHEXCEPTION
#include <alliance.exception.hpp>

class AMathException : public AllianceException {
public:
	AMathException();
	AMathException(std::string text);
	AMathException(AllianceException & e);
	AMathException(AllianceException & e, std::string text);
	AMathException(const AMathException & obj);
	~AMathException();
};

#endif

