#ifndef HPP_AMATH
#define HPP_AMATH
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <math.h>
#include "AMath.exception.hpp"

class AMath {
private:
    
public:
    static constexpr double pi = 3.141592653589793;
    static constexpr double e = 2.718281828459045;

    static double cos(double value);

    static double sin(double value);

    static double tan(double value);

    static double acos(double value);

    static double asin(double value);

    static double atan(double value);

    static double exp(double value);

    static double log(double value);

    static double pow(double value1, double value2);

    static double sqrt(double value);

    template <typename T>
    static T abs(T value);

    static double ceil(double value);

    static double floor(double value);

    static double max(double value1, double value2);

    static double min(double value1, double value2);

    static double random();

    static s32int random_int(s32int max);
};

#endif
