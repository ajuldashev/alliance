#include "AArray.hpp"

#ifdef DEBUG_AARRAY
#include <ACore/ABase/AIO/AIO.hpp>

void write_array(AArray<s32int> array) {
    
}

bool debug_aarray() {
    AArray<s32int> array;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AARRAY );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aarray());
            AIO::writeln(" 1) help");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aarray(com)) {
            return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AARRAY );
    AIO::write_div_line();
    return false;
}
#endif

