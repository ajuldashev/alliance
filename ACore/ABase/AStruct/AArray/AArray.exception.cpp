#include "AArray.exception.hpp"

AArrayException::AArrayException() : AllianceException() {
	exception = "Alliance-Exception-AArray";
}

AArrayException::AArrayException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AArray");
}

AArrayException::AArrayException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AArray");
}

AArrayException::AArrayException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AArray");
}

AArrayException::AArrayException(const AArrayException & obj) : AllianceException(obj) {
	
}

AArrayException::~AArrayException() {

}

