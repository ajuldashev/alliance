#ifndef HPP_AARRAYEXCEPTION
#define HPP_AARRAYEXCEPTION
#include <alliance.exception.hpp>

class AArrayException : public AllianceException {
public:
	AArrayException();
	AArrayException(std::string text);
	AArrayException(AllianceException & e);
	AArrayException(AllianceException & e, std::string text);
	AArrayException(const AArrayException & obj);
	~AArrayException();
};

#endif

