#ifndef HPP_AARRAY
#define HPP_AARRAY
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtrArray/APtrArray.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <iostream>
#include "AArray.exception.hpp"

template<typename T> 
class AArray { 
private:
    APtrArray<T> array;
	void insert_before(T);
	void insert_before(AArray<T>);
	void insert_in(T, s64int);
	void insert_in(AArray<T>, s64int);
	void insert_after(T);
	void insert_after(AArray<T>);
public:
	AArray();
    AArray(s64int);
	AArray(const AArray<T> &);
	static AArray<T> make(s64int);
	void copy(AArray<T> &);

	s64int length() const;

	void insert(T, s64int);
	void insert(AArray<T> & , s64int);
	void push_beg(T);
	void push_beg(AArray<T> &); 
	void push_end(T); 
	void push_end(AArray<T> &);

	T & at(s64int);
	T get(s64int);
	T pop(s64int);
	AArray<T> get(s64int, s64int);
	AArray<T> sub(s64int, s64int);
	void remove(s64int, s64int);

	T pop_beg();
	T pop_end();

    T & operator[](s64int);
	AArray<T> & operator=(AArray<T>&);

	void clear();

	~AArray();
};

template<typename T> 
AArray<T>::AArray() {

}

template<typename T> 
AArray<T>::AArray(s64int size) {
	try {
		array = APtrArray<T>::make(size);
	} catch (APtrArrayException e) {
		throw AArrayException(e, "AArray(s64int size) : Error create Array");
	}
}

template<typename T>
AArray<T>::AArray(const AArray<T> & arr) {
	array = arr.array;
}

template<typename T>
AArray<T> AArray<T>::make(s64int size) {
	AArray<T> res(size);
	return res;
}

template<typename T>
void AArray<T>::copy(AArray<T> & arr) {
	if (arr.length() == 0) {
		return;
	}
	array = APtrArray<T>::make(arr.length());
	for (s64int i = 0; i < length(); i += 1) {
		array[i] = arr[i];
	}
}

template<typename T>
s64int AArray<T>::length() const {
    return array.length();
}

template<typename T>
void AArray<T>::insert_before(T obj) {
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + 1);
	new_array[0] = obj;
	for (s64int i = 1; i < new_array.length() ; i += 1) {
		new_array[i] = array[i - 1];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert_before(AArray<T> arr) {
	if (arr.length() == 0) {
		return;
	}
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + arr.length());
	s64int pos = 0;
	for (s64int i = 0; i < arr.length() ; i += 1, pos += 1) {
		new_array[i] = arr[pos];
	}
	for (s64int i = arr.length(); i < new_array.length() ; i += 1, pos += 1) {
		new_array[i] = array[pos];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert_in(T obj, s64int pos) {
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + 1);
	new_array[pos] = obj;
	for (s64int i = 0; i < pos; i += 1) {
		new_array[i] = array[i];
	}
	for (s64int i = pos; i < array.length(); i += 1) {
		new_array[i + 1] = array[i];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert_in(AArray<T> arr, s64int pos) {
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + 1);
	s64int pos_in_array = 0;
	for (s64int i = 0; i < pos; i += 1, pos += 1) {
		new_array[i] = array[pos_in_array];
	}
	for (s64int i = 0; i < arr.length(); i += 1) {
		new_array[i + pos] = arr[i];
	}
	for (s64int i = pos + arr.length(); i < array.length(); i += 1, pos += 1) {
		new_array[i] = array[pos_in_array];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert_after(T obj) {
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + 1);
	new_array[array.length()] = obj;
	for (s64int i = 0; i < array.length() ; i += 1) {
		new_array[i] = array[i];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert_after(AArray<T> arr) {
	if (arr.length() == 0) {
		return;
	}
	APtrArray<T> new_array = APtrArray<T>::make(array.length() + arr.length());
	s64int pos = 0;
	for (s64int i = 0; i < array.length() ; i += 1, pos += 1) {
		new_array[i] = array[pos];
	}
	for (s64int i = array.length(); i < new_array.length() ; i += 1, pos += 1) {
		new_array[i] = arr[pos];
	}
	array = new_array;
}

template<typename T>
void AArray<T>::insert(T obj, s64int pos) {
	if (pos == 0) {
		insert_before(obj);
		return;
	}
	if (pos == array.length()) {
		insert_after(obj);
		return;
	}
	insert_in(obj, pos);
}

template<typename T>
void AArray<T>::insert(AArray<T> & arr, s64int pos) {
    if (array.length() == 0) {
        copy(arr);
        return;
    }
    if (pos <= 0) {
		insert_before(arr);
		return;
	}
	if (pos >= array.length()) {
		insert_after(arr);
		return;
	}
	insert_in(arr ,pos);
}

template<typename T>
void AArray<T>::push_beg(T obj) {
	insert(obj, 0);
}

template<typename T>
void AArray<T>::push_beg(AArray<T> & arr) {
	insert(arr, 0);
}

template<typename T>
void AArray<T>::push_end(T obj) {
	insert(obj, array.length());
}

template<typename T>
void AArray<T>::push_end(AArray<T> & arr) {
	insert(arr, array.length());
}

template<typename T>
T & AArray<T>::at(s64int pos) {
	try {
		if (!array.is_unique()) {
			array = array.clone();
		}
		return array[pos];
	} catch (APtrArrayException e) {
		throw AArrayException(e, "at(s64int pos) : Error at by index");
	}
}

template<typename T>
T AArray<T>::get(s64int pos) {
	try {
		return array[pos];
	} catch (APtrArrayException e) {
		throw AArrayException(e, "get(s64int pos) : Error at by index");
	}
}

template<typename T>
T AArray<T>::pop(s64int pos) {
	if (array.length() == 0) {
		throw AArrayException("pop(s64int pos) : Array is empty");
	}
	s64int i;
	if (pos < 0) {
		T res = array[0];
		if (array.length() == 1) {
			array.reset();
			return res;
		}
		APtrArray<T> new_array = APtrArray<T>::make(array.length() - 1);
		for (s64int i = 1; i < array.length(); i += 1) {
			new_array[i] = array[i - 1];
		}
		array = new_array;
		return res;
	}
	if (pos >= array.length()) { 
		T res = array[array.length() - 1];
		if (array.length() == 1) {
			array.reset();
			return res;
		}
		APtrArray<T> new_array = APtrArray<T>::make(array.length() - 1);
		for (s64int i = 0; i < array.length() - 1; i += 1) {
			new_array[i] = array[i];
		}
		array = new_array;
		return res;
	}
	T res = array[pos];
	APtrArray<T> new_array = APtrArray<T>::make(array.length() - 1);
	for (s64int i = 0; i < pos; i += 1)	 {
		new_array[i] = array[i];
	}
	for (s64int i = pos; i < array.length(); i += 1)	 {
		new_array[i - 1] = array[i];
	}
	array = new_array;
	return res;
}

template<typename T>
AArray<T> AArray<T>::sub(s64int pos, s64int len) {
	try {
		AArray<T> res = get(pos, len);
		remove(pos, len);
		return res;
	} catch (AArrayException e) {
		throw AArrayException(e, "sub(s64int pos, s64int len)");
	}
}

template<typename T>
AArray<T> AArray<T>::get(s64int pos, s64int len) {
	if (pos == 0 && len == array.length()) {
		AArray<T> res;
		res.copy(*this);
		return res;
	}
	if (pos < 0) {
		throw AArrayException("get(s64int pos, s64int len) : Error position < 0");
	}
	if (pos > array.length()) {
		throw AArrayException("get(s64int pos, s64int len) : Error position > array.length()");
	}
	if (len <= 0) {
		throw AArrayException("get(s64int pos, s64int len) : Error length <= 0");
	}
	if  (pos + len > array.length()) {
		throw AArrayException("get(s64int pos, s64int len) : Error position + length > array.length()");
	}
	AArray<T> res(len);
	for (s64int i = pos; i < pos + len; i += 1) {
		res[i - pos] = array[i];
	}
	return res;
}

template<typename T>
void AArray<T>::remove(s64int pos, s64int len) {
	if (pos == 0 && len == array.length()) {
		clear();
		return;
	}
	if (pos < 0) {
		throw AArrayException("remove(s64int pos, s64int len) : Error position < 0");
	}
	if (pos > array.length()) {
		throw AArrayException("remove(s64int pos, s64int len) : Error position > array.length()");
	}
	if (len <= 0) {
		throw AArrayException("remove(s64int pos, s64int len) : Error length <= 0");
	}
	if  (pos + len > array.length()) {
		throw AArrayException("remove(s64int pos, s64int len) : Error position + length > array.length()");
	}
	APtrArray<T> new_array = APtrArray<T>::make(length() - len);
	for (s64int i = 0; i < pos; i += 1) {
		new_array[i] = array[i];
	}
	for (s64int i = pos + len; i < length(); i += 1) {
		new_array[i - (pos + len)] = array[i];
	}
	array = new_array;
	return;
}

template<typename T>
T AArray<T>::pop_beg() {
	return pop(0);
}

template<typename T>
T AArray<T>::pop_end() {
	return pop(length());
}

template<typename T>
T & AArray<T>::operator[](s64int pos) {
	try {
		if (!array.is_unique()) {
			array = array.clone();
		}
		return array[pos];
	} catch (APtrArrayException e) {
		throw AArrayException(e, "operator[] : Error at by index");
	}
}

template<typename T>
AArray<T> & AArray<T>::operator=(AArray<T> & arr) {
	copy(arr);
	return *this;
}

template<typename T>
void AArray<T>::clear() {
	array.reset();
}

template<typename T>
AArray<T>::~AArray() {

}

#endif
