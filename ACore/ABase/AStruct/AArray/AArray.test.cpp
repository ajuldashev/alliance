#include "AArray.hpp"

#ifdef TEST_AARRAY
#include <ACore/ABase/AIO/AIO.hpp>

AArray<s32int> test_array(s64int len) {
   AArray<s32int> res(len);
   for (s64int i = 0, k = 1; i < res.length(); i += 1) {
      res[i] = k;
      k = k * 2;
   }
   return res;
}

bool test_equal(AArray<s32int> & arr1, AArray<s32int> & arr2) {
   if (arr1.length() != arr2.length()) {
      return false;
      for (s32int i = 0; i < arr1.length(); i += 1) {
         if (arr1[i] != arr2[i]) {
            return false;
         }
      }
   }
   return true;
}

void test_print(AArray<s32int> & arr) {
   for (s64int i = 0; i < arr.length(); i += 1) {
      AIO::format("%si ", arr.at(i));
   }
   AIO::writeln();
}

bool test_aarray(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_AARRAY );
   
   if (!test_node_aarray(is_soft)) {
      return is_soft;
   }

   {
      AIO::writeln_warning("Test AArray()");
      AArray<s32int> array;
      if (array.length() != 0) {
         test_print(array);
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test AArray(s64int) 1 : 1");
      AArray<s32int> array(1);
      if (array.length() != 1) {
         test_print(array);
         return is_soft;
      }
      array[0] = 1;
      if (array[0] != 1) {
         test_print(array);
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test AArray(s64int) 10 : 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024");
      AArray<s32int> array = test_array(10);
      if (array.length() != 10) {
         test_print(array);
         return is_soft;
      }
      for (s64int i = 0, k = 1; i < 10; i += 1) {
         if (array[i] != k) {
            test_print(array);
            return is_soft;
         }
         k = k * 2;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test copy");
      AArray<s32int> array = test_array(10);
      if (array.length() != 10) {
         test_print(array);
         return is_soft;
      }
      for (s64int i = 0, k = 1; i < 10; i += 1) {
         if (array[i] != k) {
            test_print(array);
            return is_soft;
         }
         k = k * 2;
      }
      AArray<s32int> array_copy;
      array_copy.copy(array);
      if (!test_equal(array, array_copy)) {
         test_print(array);
         test_print(array_copy);
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Insert(T, s64int)");
      AArray<s32int> array;
      AIO::writeln_warning("Insert(1, 0)");
      array.insert(1, 0);
      s64int mass_1[] = {1};
      for (s64int i = 0; i < 1; i += 1) {
         if (mass_1[i] != array[i]) {
            test_print(array);
            return is_soft;
         }
      }
      test_print(array);
      AIO::writeln_warning("Insert(2, 0)");
      array.insert(2, 0);
      s64int mass_2[] = {2, 1};
      for (s64int i = 0; i < 2; i += 1) {
         if (mass_2[i] != array[i]) {
            test_print(array);
            return is_soft;
         }
      }
      test_print(array);
      AIO::writeln_warning("Insert(3, 1)");
      array.insert(3, 1);
      s64int mass_3[] = {2, 3, 1};
      for (s64int i = 0; i < 3; i += 1) {
         if (mass_3[i] != array[i]) {
            test_print(array);
            return is_soft;
         }
      }
      test_print(array);
      AIO::writeln_warning("Insert(4, 3)");
      array.insert(4, 3);
      s64int mass_4[] = {2, 3, 1, 4};
      for (s64int i = 0; i < 4; i += 1) {
         if (mass_4[i] != array[i]) {
            test_print(array);
            return is_soft;
         }
      }
      test_print(array);
      AIO::writeln_warning("Insert(5, 1)");
      array.insert(5, 1);
      s64int mass_5[] = {2, 5, 3, 1, 4};
      for (s64int i = 0; i < 5; i += 1) {
         if (mass_5[i] != array[i]) {
            test_print(array);
            return is_soft;
         }
      }
      test_print(array);
      AIO::writeln_success("Done");
   }
   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_AARRAY );
   return true;
}
#endif

