#include "AList.hpp"

#ifdef DEBUG_ALIST
#include <ACore/ABase/AIO/AIO.hpp>

void debug_print(AList<s32int> list) {
    AIO::writeln("List : ");
    for (s64int i = 0; i < list.length(); i += 1) {
        AIO::format("%+3sl) : %si\n", i, list.at(i));
    }
}

AList<s32int> debug_list() {
    AList<s32int> list;
    s64int length = AIO::get_s64int();
    for (s64int i = 0; i < length; i += 1) {
        list.push_end(AIO::get_s32int());
    }
    return list;
}

bool debug_alist() {
    AList<s32int> list;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ALIST );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_alist());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) push_beg");
            AIO::writeln(" 3) push_end");
            AIO::writeln(" 4) insert");
            AIO::writeln(" 5) insert_list");
            AIO::writeln(" 6) at");
            AIO::writeln(" 7) get");
            AIO::writeln(" 8) get_list");
            AIO::writeln(" 9) sub_list");
            AIO::writeln("10) remove_list");
            AIO::writeln("11) pop");
            AIO::writeln("12) pop_beg");
            AIO::writeln("13) pop_end");
            AIO::writeln("14) next");
            AIO::writeln("15) prev");
            AIO::writeln("16) goto_beg");
            AIO::writeln("17) goto_end");
            AIO::writeln("18) status");
            AIO::writeln("19) clear");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_alist(com)) {
			return true;
		}
        if (com == "push_beg") {
            s32int value = AIO::get_s32int();
            list.push_beg(value);
        }
        if (com == "push_end") {
            s32int value = AIO::get_s32int();
            list.push_end(value);
        }
        if (com == "insert") {
            s32int value = AIO::get_s32int();
            list.insert(value, AIO::get_s32int());
        }
        if (com == "insert_list") {
            s64int pos = AIO::get_s64int();
            AList<s32int> l = debug_list();
            list.insert(l, pos);
        }
        if (com == "at") {
            s32int value = list.at(AIO::get_s32int());
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "get") {
            s32int value = list.get(AIO::get_s32int());
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "get_list") {
            s64int pos = AIO::get_s64int();
            s64int length = AIO::get_s64int();
            AList<s32int> l = list.get(pos, length);
            debug_print(l);
        }
        if (com == "sub_list") {
            s64int pos = AIO::get_s64int();
            s64int length = AIO::get_s64int();
            AList<s32int> l = list.sub(pos, length);
            debug_print(l);
        }
        if (com == "remove_list") {
            s64int pos = AIO::get_s64int();
            s64int length = AIO::get_s64int();
            list.remove(pos, length);
        }
        if (com == "pop") {
            s32int value = list.pop(AIO::get_s32int());
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "pop_beg") {
            s32int value = list.pop_beg();
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "pop_end") {
            s32int value = list.pop_end();
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "next") {
            list.next();
        }
        if (com == "prev") {
            list.prev();
        }
        if (com == "goto_beg") {
            list.goto_beg();
        }
        if (com == "goto_end") {
            list.goto_end();
        }
        if (com == "status") {
            for(s64int idx = 0; idx < list.length(); idx += 1) {
                AIO::format("%+3sl) : %si\n", idx, list.at(idx));
            }
        }
        if (com == "clear") {
            list.clear();
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ALIST );
    AIO::write_div_line();
    return false;
}
#endif

