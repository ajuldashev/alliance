#include "AList.hpp"

#ifdef DEBUG_ALIST
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_alist(AString com) {
	#ifdef DEBUG_ALISTITERATOR
	if (com == "alistiterator") {
		if (debug_alistiterator()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_alist() {
	AString text;
	#ifdef DEBUG_ALISTITERATOR
	text += "d) alistiterator\n";
	#endif
	return text;
}

#endif
