#include "AList.exception.hpp"

AListException::AListException() : AllianceException() {
	exception = "Alliance-Exception-AList";
}

AListException::AListException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AList");
}

AListException::AListException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AList");
}

AListException::AListException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AList");
}

AListException::AListException(const AListException & obj) : AllianceException(obj) {
	
}

AListException::~AListException() {

}

