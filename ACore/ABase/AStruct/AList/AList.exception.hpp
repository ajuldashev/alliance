#ifndef HPP_ALISTEXCEPTION
#define HPP_ALISTEXCEPTION
#include <alliance.exception.hpp>

class AListException : public AllianceException {
public:
	AListException();
	AListException(std::string text);
	AListException(AllianceException & e);
	AListException(AllianceException & e, std::string text);
	AListException(const AListException & obj);
	~AListException();
};

#endif

