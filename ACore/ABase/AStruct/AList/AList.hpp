#ifndef HPP_ALIST
#define HPP_ALIST
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "AList.exception.hpp"

template<typename T> 
class AList {
private:
	class Node;
	mutable APtr<Node> beg;
	mutable APtr<Node> end;
	mutable APtr<Node> cur;
	s64int pos_cur;
	s64int length_list;
public:
	AList();
	AList(const AList &);
	AList copy();
	
	s64int length();
	void insert(T, s64int);
	void insert(AList &, s64int);
	void push_beg(T); 
	void push_end(T);
	void set(T, s64int);

	T & at(s64int);
	T get(s64int);
	T pop(s64int);

	T pop_beg();
	T pop_end();

	AList get(s64int, s64int);
	AList sub(s64int, s64int);
	void remove(s64int, s64int);

	void next();
	void prev();

	void goto_beg();
	void goto_end();

	void sort();

	T & operator[](s64int);
	AList<T> & operator=(AList<T>);
	void clear();
	~AList();
};

template <typename T> 
class AList<T>::Node {
public:
	mutable APtr<Node> prev;
	mutable APtr<Node> next;
	T obj;
};

template<typename T> 
AList<T>::AList() {
	pos_cur = -1;
	length_list = 0;
}

template<typename T> 
AList<T>::AList(const AList & list) {
	pos_cur = -1;
	length_list = 0;
	for (APtr<Node> node = list.beg; node; node = node->next) {
		push_end(node->obj);
	}
}

template<typename T> 
AList<T> AList<T>::copy() {
	AList<T> list;
	for (s64int i = 0; i < length(); i += 1) {
		list.push_end(at(i));
	}
	return list;
}

template <typename T>
s64int AList<T>::length() {
	return length_list;
}

template<typename T> 
void AList<T>::insert(T obj, s64int pos) {
	if (length_list == 0){
		APtr<Node> node(new Node);
		node->obj = obj;
		node->next.reset();
		node->prev.reset();
		beg = node;
		end = node;
		cur = node;
		pos_cur = 0;
		length_list = 1;
		return;
	}
	if (length_list == 1 && pos > 0){
		APtr<Node> node(new Node);
		node->obj = obj;
		node->next.reset();
		node->prev = beg;
		beg->next = node;
		end = node;
		cur = node;
		pos_cur = 1;
		length_list = 2;
		return;
	}
	at(pos);
	if (cur == beg){
		APtr<Node> node(new Node);
		node->obj = obj;
		node->next = beg;
		node->prev.reset();
		beg->prev = node;
		beg = node;
		cur = node;
		length_list += 1;
		return;
	}
	if (cur == end && pos >= length_list){
		APtr<Node> node(new Node);
		node->obj = obj;
		node->next.reset();
		node->prev = end;
		end->next = node;
		end = node;
		cur = node;
		length_list  += 1;
		pos_cur += 1;
		return;
	}
	APtr<Node> node(new Node);
	node->obj = obj;
	node->next = cur;
	node->prev = cur->prev;
	cur->prev->next = node;
	cur->prev = node;
	cur = node;
	length_list += 1;
}

template<typename T> 
void AList<T>::insert(AList<T> & list, s64int pos) {
	if (pos < 0) {
		pos = 0;
	}
	if (pos > length_list) {
		pos = length_list;
	}
	for (s64int i = 0; i < list.length(); i += 1) {
		insert(list.at(i), pos + i);
	}
}

template<typename T> 
void AList<T>::push_beg(T obj) {
	insert(obj, 0);
} 

template<typename T> 
void AList<T>::push_end(T obj) {
	insert(obj, length_list);
}

template<typename T> 
void AList<T>::set(T obj, s64int pos) {
	at(pos) = obj;
}

template<typename T> 
T & AList<T>::at(s64int pos) {
	if (length_list == 0){
		throw AListException("Error at pos, list empty");
	}
	if (pos <= 0){
		cur = beg;
		pos_cur = 0;
		return beg->obj;
	}
	if (pos >= length_list - 1){
		cur = end;
		pos_cur = length_list - 1;
		return end->obj;
	}
	if (pos == pos_cur){
		return cur->obj;
	}
	if (pos < pos_cur){
		// beg up
		s32int du = pos;
		// cur down
		s32int cd = pos_cur - pos;
		if (du < cd) {
			cur = beg;
			pos_cur = 0;
			for (; pos_cur < pos; pos_cur += 1, cur = cur->next);
			return cur->obj;
		} else {
			for (; pos_cur > pos; pos_cur -= 1, cur = cur->prev);
			return cur->obj;
		}
	}
	// cur up
	s32int cu = pos - pos_cur;
	// end down
	s32int ed = length_list - pos;
	if (ed < cu) {
		cur = end;
		pos_cur = length_list - 1;
		for (; pos_cur > pos; pos_cur -= 1, cur = cur->prev);
		return cur->obj;
	} else {
		for (; pos_cur < pos; pos_cur += 1, cur = cur->next);
		return cur->obj;
	}
}

template<typename T> 
T AList<T>::pop(s64int pos) {
	if (length_list == 0){
		throw AListException("Error pop pos, list empty");
	}
	if (length_list == 1){
		APtr<Node> node;
		node = beg;
		beg.reset();
		end.reset();
		cur.reset();
		length_list = 0;
		pos_cur -= 1;
		return  node->obj;
	}
	if (pos <= 0){
		APtr<Node> node;
		node = beg;
		beg->next->prev.reset();
		beg = beg->next;
		cur = beg;
		pos_cur = 0;
		length_list -= 1;
		return node->obj;
	}
	if (pos >= length_list - 1){
		APtr<Node> node;
		node = end;
		end->prev->next.reset();
		end = end->prev;
		cur = end;
		length_list -= 1;
		pos_cur = length_list - 1;
		return node->obj;
	}
	at(pos);
	APtr<Node> node;
	node = cur;
	cur->prev->next = cur->next;
	cur->next->prev = cur->prev;
	cur = cur->next;
	length_list -= 1;
	return node->obj;
}

template<typename T> 
T AList<T>::get(s64int pos) {
	if (length_list == 0){
		throw AListException("Error get pos, list empty");
	}
	if (length_list == 1){
		return  beg->obj;
	}
	if (pos <= 0){
		return beg->obj;
	}
	if (pos >= length_list - 1){
		return end->obj;
	}
	at(pos);
	return cur->obj;
}

template<typename T> 
T AList<T>::pop_beg() {
	return pop(0);
} 

template<typename T> 
T AList<T>::pop_end() {
	return pop(length_list - 1);
}

template<typename T> 
AList<T> AList<T>::get(s64int pos, s64int length_sublist) {
	AList<T> res;
	if (pos < 0) {
		pos = 0;
	}
	if (pos > length_list) {
		return res;
	}
	for (s64int i = 0; i < length_sublist && (i + pos < length_list); i += 1) {
		res.push_end(at(i + pos));
	}
	return res;
}

template<typename T> 
AList<T> AList<T>::sub(s64int pos, s64int length_sublist) {
	AList<T> res;
	if (pos < 0) {
		pos = 0;
	}
	if (pos > length_list) {
		return res;
	}
	for (s64int i = 0; i < length_sublist && (pos < length_list); i += 1) {
		res.push_end(pop(pos));
	}
	return res;
}

template<typename T> 
void AList<T>::remove(s64int pos, s64int length_sublist) {
	if (pos < 0) {
		pos = 0;
	}
	if (pos > length_list) {
		return;
	}
	for (s64int i = 0; i < length_sublist && (pos < length_list); i += 1) {
		pop(pos);
	}
}

template<typename T> 
void AList<T>::next(){
	at(pos_cur + 1);
}

template<typename T> 
void AList<T>::prev() {
	at(pos_cur - 1);
}

template<typename T> 
void AList<T>::goto_beg() {
	at(0);
}

template<typename T> 
void AList<T>::goto_end() {
	at(length_list - 1);
}

template<typename T> 
void AList<T>::sort() {
	AList<T> list_sorted;
	for (s64int i = 0, j = 0; i < length(); i += 1) {
		for (j = 0; j < list_sorted.length(); j += 1) {
			if (at(i) < list_sorted[j]) {
				list_sorted.insert(at(i), j);
				break;
			}
		}
		if (j == list_sorted.length()) {
			list_sorted.push_end(at(i));
		}
	}
	*this = list_sorted;
}

template<typename T>
T & AList<T>::operator[](s64int pos) {
	return at(pos);
}

template<typename T>
AList<T> & AList<T>::operator=(AList<T> list) {
	clear();
	pos_cur = -1;
	length_list = 0;
	for (APtr<Node> node = list.beg; node; node = node->next) {
		push_end(node->obj);
	}
	return *this;
}

template<typename T> 
void AList<T>::clear() {
	while (length_list > 0){
		pop_beg();
	}
}

template<typename T> 
AList<T>::~AList() {
	while (length_list > 0){
		pop_beg();
	}
}

#endif
