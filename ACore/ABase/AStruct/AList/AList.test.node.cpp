#include "AList.hpp"

#ifdef TEST_ALIST
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_alist(bool is_soft) {
	#ifdef TEST_ALISTITERATOR
	if (!test_alistiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ALISTITERATOR );
		return is_soft;
	}
	#endif
	return true;
}
#endif
