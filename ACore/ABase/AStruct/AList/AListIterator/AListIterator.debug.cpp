#include "AListIterator.hpp"

#ifdef DEBUG_ALISTITERATOR
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_alistiterator() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ALISTITERATOR );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_alistiterator());           
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_alistiterator(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ALISTITERATOR );
    AIO::write_div_line();
    return false;
}
#endif

