#include "AListIterator.exception.hpp"

AListIteratorException::AListIteratorException() : AllianceException() {
	exception = "Alliance-Exception-AListIterator";
}

AListIteratorException::AListIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AListIterator");
}

AListIteratorException::AListIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AListIterator");
}

AListIteratorException::AListIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AListIterator");
}

AListIteratorException::AListIteratorException(const AListIteratorException & obj) : AllianceException(obj) {
	
}

AListIteratorException::~AListIteratorException() {

}

