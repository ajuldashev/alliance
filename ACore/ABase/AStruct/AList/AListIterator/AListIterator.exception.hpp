#ifndef HPP_ALISTITERATOREXCEPTION
#define HPP_ALISTITERATOREXCEPTION
#include <alliance.exception.hpp>

class AListIteratorException : public AllianceException {
public:
	AListIteratorException();
	AListIteratorException(std::string text);
	AListIteratorException(AllianceException & e);
	AListIteratorException(AllianceException & e, std::string text);
	AListIteratorException(const AListIteratorException & obj);
	~AListIteratorException();
};

#endif

