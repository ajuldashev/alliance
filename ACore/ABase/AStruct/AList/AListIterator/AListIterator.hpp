#ifndef HPP_ALISTITERATOR
#define HPP_ALISTITERATOR
#include <ACore/ABase/AStruct/AStructIterator/AStructIterator.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include "AListIterator.exception.hpp"

template <typename T>
class AListIterator : public AStructIterator<T> {
private:
    APtrWeak<AList<T>> list_weak;
    s64int pos;
public:
    AListIterator(AList<T> & list);
    static bool vfunc_has_next_alistiterator(AStructIterator<T> & i);
    static T    vfunc_get_next_alistiterator(AStructIterator<T> & i);
    static void vfunc_reset_alistiterator(AStructIterator<T> & i);
    static void vfunc_clear_alistiterator(AStructIterator<T> & i);
    ~AListIterator();
};

template <typename T>
static bool func_has_next_alistiterator(AStructIterator<T> & i) {
    return AListIterator<T>::vfunc_has_next_alistiterator(i);
}

template <typename T>
static T func_get_next_alistiterator(AStructIterator<T> & i) {
    return AListIterator<T>::vfunc_get_next_alistiterator(i);
}

template <typename T>
static void func_reset_alistiterator(AStructIterator<T> & i) {
    AListIterator<T>::vfunc_reset_alistiterator(i);
}

template <typename T>
static void func_clear_alistiterator(AStructIterator<T> & i) {
    AListIterator<T>::vfunc_clear_alistiterator(i);
}

template <typename T>
bool AListIterator<T>::vfunc_has_next_alistiterator(AStructIterator<T> & i) {
    try {
        AListIterator<T> & li = (AListIterator<T>&)i;
        return li.list_weak && li.pos < li.list_weak->length();
    } catch (AllianceException & e) {
        throw AListIteratorException(e, "has_next()");
    }
}

template <typename T>
T AListIterator<T>::vfunc_get_next_alistiterator(AStructIterator<T> & i) {
    try {
        AListIterator<T> & li = (AListIterator<T>&)i;
        li.pos += 1;
        return li.list_weak->get(li.pos - 1);
    } catch (AllianceException & e) {
        throw AListIteratorException(e, "get_nex()");
    }
}

template <typename T>
void AListIterator<T>::vfunc_reset_alistiterator(AStructIterator<T> & i) {
    try {
        AListIterator<T> & li = (AListIterator<T>&)i;
        li.pos = 0;
        li.list_weak->at(li.pos);
    } catch (AllianceException & e) {
        throw AListIteratorException(e, "reset()");
    }
}

template <typename T>
void AListIterator<T>::vfunc_clear_alistiterator(AStructIterator<T> & i) {
    try {
        AListIterator<T> & li = (AListIterator<T>&)i;
        li.list_weak.reset();
    } catch (AllianceException & e) {
        throw AListIteratorException(e, "clear()");
    }
}

template <typename T>
AListIterator<T>::AListIterator(AList<T> & list) {
    pos = 0;
    list_weak = &list;
    list_weak->at(pos);
    this->vfunc_has_next = func_has_next_alistiterator<T>;
    this->vfunc_get_next = func_get_next_alistiterator<T>;
    this->vfunc_reset = func_reset_alistiterator<T>;
    this->vfunc_clear = func_clear_alistiterator<T>;
}

template <typename T>
AListIterator<T>::~AListIterator() {

}

#endif