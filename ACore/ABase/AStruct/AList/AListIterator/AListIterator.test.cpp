#include "AListIterator.hpp"

#ifdef TEST_ALISTITERATOR
#include <ACore/ABase/AIO/AIO.hpp>
bool test_alistiterator(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_ALISTITERATOR );

   if (!test_node_alistiterator(is_soft)) {
      return is_soft;
   }
   {
      AIO::writeln_warning("Test get_next, has_next, reset, clear");
      AList<s32int> list;
      s32int N = 10;

      for (s32int i = 0; i < N; i += 1) {
         list.push_end(i);
      }
      AIO::writeln_warning("Write list :");
      for (s32int i = 0; i < N; i += 1) {
         AIO::write(list.at(i));
         AIO::write(" ");
      }
      AIO::writeln();

      AListIterator<s32int> li(list);
      AIO::writeln_warning("test list iterator");
      for (s32int i = 0; li.has_next() ; i += 1) {
         if (i != li.get_next()) {
            return is_soft;
         }
      }

      AIO::writeln_warning("test reset iterator");
      li.reset();
      for (s32int i = 0; li.has_next() ; i += 1) {
         if (i != li.get_next()) {
            return is_soft;
         }
      }

      AIO::writeln_warning("test clear");
      li.clear();
      if (li.has_next()) {
         return is_soft;
      }

      AIO::writeln_success("Done");
   }

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_ALISTITERATOR );
   return true;
}
#endif

