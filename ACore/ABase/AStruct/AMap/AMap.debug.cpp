#include "AMap.hpp"

#ifdef DEBUG_AMAP
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_amap() {
	AMap<AString, AString> map;
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_AMAP );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_amap());
		    AIO::writeln("1) help");
            AIO::writeln("2) add");
			AIO::writeln("3) count");
            AIO::writeln("4) get_key");
			AIO::writeln("5) get_data");
            AIO::writeln("6) find");
            AIO::writeln("7) print");
            AIO::writeln("0) stop");	
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_atreemap(com)) {
			return true;
		}
        if (com == "add") {
            AString key = AIO::get_word();
			AString data = AIO::get_word();
            map.add(key, data);
		}
		if (com == "count") {
            AIO::writeln(map.count());
        }
        if (com == "get_key") {
            u64int num = AIO::get_u64int();
            AIO::writeln(map.get_key(num));
        }
		if (com == "get_data") {
            u64int num = AIO::get_u64int();
            AIO::writeln(*map.get_data(num));
        }
        if (com == "find") {
            AString num = AIO::get_word();
            AIO::writeln(map.find(num));
        }
        if (com == "print") {
            map.print();
        }
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_AMAP );
	AIO::write_div_line();
	return false;
}
#endif
