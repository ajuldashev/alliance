#include "AMap.exception.hpp"

AMapException::AMapException() : AllianceException() {
	exception = "Alliance-Exception-AMap";
}

AMapException::AMapException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AMap");
}

AMapException::AMapException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AMap");
}

AMapException::AMapException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AMap");
}

AMapException::AMapException(const AMapException & obj) : AllianceException(obj) {

}

AMapException::~AMapException() {

}
