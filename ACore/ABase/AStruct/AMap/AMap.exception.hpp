#ifndef HPP_AMAPEXCEPTION
#define HPP_AMAPEXCEPTION
#include <alliance.exception.hpp>

class AMapException : public AllianceException {
public:
	AMapException();
	AMapException(std::string text);
	AMapException(AllianceException & e);
	AMapException(AllianceException & e, std::string text);
	AMapException(const AMapException & obj);
	~AMapException();
};

#endif
