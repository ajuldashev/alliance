#ifndef HPP_AMAP
#define HPP_AMAP
#include <ACore/ABase/AMath/AMath.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include "AMap.exception.hpp"

template <typename Key, typename Data>
class AMap {
private:
    class Node;
    Node * root;
    s64int add_rec(Node * node, Key & key, Data & data);
    s64int find_rec(Node * node, Key & key, s64int pos);
    APtr<Data> get(Node * node, Key & key);
    
    bool test_left(Node * node);
    bool test_right(Node * node);
    Node * rotation_left(Node * node);
    Node * rotation_right(Node * node);
public:
    AMap();
    s64int add(Key key, Data data);
    
    Key        get_key (s64int pos);
    APtr<Data> get_data(s64int pos);
    
    APtr<Data> get(Key key);
    
    s64int find(Key key);
    
    u64int count();

    AString ftab(s64int);
    void print(Node * node = NULL, s64int tab = 0, AString stab = "");

    ~AMap();
};

template <typename Key, typename Data> 
class AMap<Key, Data>::Node {
public:
    Node();
    Node * left;
    Node * right;
    Node * parent;
    s64int size;
    Key key;
    APtr<Data> data;
    ~Node();
};

template <typename Key, typename Data>
AMap<Key, Data>::Node::Node() {
    left = NULL;
    right = NULL;
    parent = NULL;
    size = 0;
}

template <typename Key, typename Data>
AMap<Key, Data>::Node::~Node() {
    if (left) {
        delete left;
    }
    if (right) {
        delete right;
    }
    size = 0;
}

template <typename Key, typename Data>
s64int AMap<Key, Data>::add_rec(Node * node, Key & key, Data & data) {
    if (node) {
        if (node->key == key) {
            if (node->left) {
                return node->left->size;
            }
            return 0;
        }
        if (node->key > key) {
            if (node->left == NULL) {
                node->left = new Node();
                node->size += 1;
                node->left->size = 1;
                node->left->key = key;
                node->left->parent = node;
                node->left->data = APtr<Data>::clone(data);
                return 0;
            }
            s64int pos = add_rec(node->left, key, data);
            if (pos != -1) {
                node->size += 1;
                if (test_left(node)) {
                    if (node == root) {
                        node = rotation_left(node);
                        root = node;
                    } else {
                        node = rotation_left(node);
                    }
                }
                if (test_right(node)) {
                    if (node == root) {
                        node = rotation_right(node);
                        root = node;
                    } else {
                        node = rotation_right(node);
                    }
                }
            }
            return pos;
        }
        if (node->key < key) {
            if (node->right == NULL) {
                node->right = new Node();
                node->size += 1;
                node->right->size = 1;
                node->right->key = key;
                node->right->parent = node;
                node->right->data = APtr<Data>::clone(data);
                s64int left_pos = 0;
                if (node->left) {
                    left_pos = node->left->size;
                }
                return left_pos + 1;
            }
            s64int pos = add_rec(node->right, key, data);
            if (pos != -1) {
                node->size += 1;
                if (test_left(node)) {
                    if (node == root) {
                        node = rotation_left(node);
                        root = node;
                    } else {
                        node = rotation_left(node);
                    }
                }
                if (test_right(node)) {
                    if (node == root) {
                        node = rotation_right(node);
                        root = node;
                    } else {
                        node = rotation_right(node);
                    }
                }
            }
            s64int left_pos = 0;
            if (node->left) {
                left_pos = node->left->size;
            }
            return pos + left_pos + 1;
        }
    }
    return -1;
}

template <typename Key, typename Data>
s64int AMap<Key, Data>::find_rec(Node * node, Key & key, s64int pos) {
    if (node) {
        if (node->key == key) {
            if (node->left) {
                pos += node->left->size;
            }
            return pos;
        }
        if (node->key < key) {
            if (node->left) {
                pos += node->left->size;
            }
            return find_rec(node->right, key, pos + 1);
        }
        if (node->key > key) {
            return find_rec(node->left, key, pos);
        }
    }
    return -1;
}

template <typename Key, typename Data>
APtr<Data> AMap<Key, Data>::get(Node * node, Key & key) {
    if (node) {
        if (node->key == key) {
            return node->data;
        }
        if (node->key < key) {
            return get(node->right, key);
        }
        if (node->key > key) {
            return get(node->left, key);
        }
    }
    return APtr<Data>();
}

template <typename Key, typename Data>
Key AMap<Key, Data>::get_key(s64int pos) {
    Key res;
    if (root) {
        if (pos >= 0 && pos < root->size) {
            Node * node = root;
            if (pos == 0 && !node->left) {
                return node->key;
            }
            while (pos >= 0) {
                if (node->left && pos == node->left->size) {
                    return node->key;
                }
                if (node->left && pos < node->left->size) {
                    node = node->left;
                    continue;
                }
                if (node->left && node->right) {
                    pos -= node->left->size + 1;
                    node = node->right;
                    continue;
                }
                if (pos == 0 && !node->left) {
                    return node->key;
                }
                if (node->right) {
                    pos -= 1;
                    node = node->right;
                    continue;
                }
                return node->key;
            }
        }
    }    
    return res;
}

template <typename Key, typename Data>
APtr<Data> AMap<Key, Data>::get_data(s64int pos) {
    if (root) {
        if (pos >= 0 && pos < root->size) {
            Node * node = root;
            if (pos == 0 && !node->left) {
                return node->data;
            }
            while (pos >= 0) {
                if (node->left && pos == node->left->size) {
                    return node->data;
                }
                if (node->left && pos < node->left->size) {
                    node = node->left;
                    continue;
                }
                if (node->left && node->right) {
                    pos -= node->left->size + 1;
                    node = node->right;
                    continue;
                }
                if (pos == 0 && !node->left) {
                    return node->data;
                }
                if (node->right) {
                    pos -= 1;
                    node = node->right;
                    continue;
                }
                return node->data;
            }
        }
    }    
    return APtr<Data>();
}

template <typename Key, typename Data>
APtr<Data> AMap<Key, Data>::get(Key key) {
    return get(root, key);
}

template <typename Key, typename Data>
s64int AMap<Key, Data>::find(Key key) {
    return find_rec(root, key, 0);
}

template <typename Key, typename Data>
bool AMap<Key, Data>::test_left(Node * node) {
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->right && node->right->right) {
            new_right = node->right->right->size;
        }
        new_left = node->size - 1 - new_right;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template <typename Key, typename Data>
bool AMap<Key, Data>::test_right(Node *node) { 
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->left && node->left->left) {
            new_left = node->left->left->size;
        }
        new_right = node->size - 1 - new_left;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template <typename Key, typename Data>
typename AMap<Key, Data>::Node * 
AMap<Key, Data>::rotation_left(Node * node) {
    if (node ) {
        Node * pnode = node; 
        if (pnode->right) {
            s64int size = pnode->size;
            Node * rnode = pnode->right;
            if (rnode->left) {
                rnode->left->parent = pnode;
            }
            pnode->right = rnode->left;
            rnode->left = pnode;
            rnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = rnode;
                } else {
                    pnode->parent->right = rnode;
                }
            }
            pnode->parent = rnode;
            pnode = rnode;
            pnode->size = size;
            pnode->left->size = size - 1;
            if (pnode->right) {
                pnode->left->size -= pnode->right->size;
            }
            node = pnode;
        }
    }
    return node;
}

template <typename Key, typename Data>
typename AMap<Key, Data>::Node * 
AMap<Key, Data>::rotation_right(Node * node) { 
    if (node) {
        Node * pnode = node; 
        if (pnode->left) {
            s64int size = pnode->size;
            Node * lnode = pnode->left;
            if (lnode->right) {
                lnode->right->parent = pnode;
            }
            pnode->left = lnode->right;
            lnode->right = pnode;
            lnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = lnode;
                } else {
                    pnode->parent->right = lnode;
                }
            }
            pnode->parent = lnode;
            pnode = lnode;
            pnode->size = size;
            pnode->right->size = size - 1;
            if (pnode->left) {
                pnode->right->size -= pnode->left->size;
            }
            node = pnode;
        }
    }
    return node;
}

template <typename Key, typename Data>
AMap<Key, Data>::AMap() {
    root = NULL;
}

template <typename Key, typename Data>
s64int AMap<Key, Data>::add(Key key, Data data) {
    if (root == NULL) {
        root = new Node();
        root->size += 1;
        root->key = key;
        root->data = APtr<Data>::clone(data);
        return 0;
    }
    return add_rec(root, key, data);
}

template <typename Key, typename Data>
AString AMap<Key, Data>::ftab(s64int tab) {
    AString res = "";
    for (s64int i = 0; i < tab; i += 1) {
        res = res + "  ";
    }
    return res;
}

template <typename Key, typename Data>
void AMap<Key, Data>::print(Node * node, s64int tab, AString stab) {
    if (!node) {
        node = root;
    }
    if (node) {
        AIO::write(ftab(tab));
        AIO::write("Size ");
        AIO::write(node->size); 
        AIO::write( " : Key "); 
        AIO::write( node->key);
        AIO::write( " : Data "); 
        AIO::write(*node->data);
        if (node->left || node->right) {
            AIO::writeln(" {");
            if (node->left) {
                AIO::writeln(ftab(tab) + "Left : ");
                print(node->left, tab + 1);
            }
            if (node->right) {
                AIO::writeln(ftab(tab) + "Right : ");
                print(node->right, tab + 1);
            }
            AIO::writeln(ftab(tab) + "}");
        } else {
            AIO::writeln();
        }
        return;
    }  
}

template <typename Key, typename Data>
u64int AMap<Key, Data>::count() {
    if (root) {
        return root->size;
    }
    return 0;
}

template <typename Key, typename Data>
AMap<Key, Data>::~AMap() {
    if (root) {
        delete root;
    }
}

#endif
