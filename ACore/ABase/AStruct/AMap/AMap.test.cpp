#include "AMap.hpp"

#ifdef TEST_AMAP
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		AMap<AString, AString> map;
		u64int N = 1000;
		for (u64int i = N/10; i < N; i += 1) {
			if (map.add(AStringLibs::to_string(i) + "key", AStringLibs::to_string(i) + "data") == -1) {
				/** <doc-test-info>
				 *
				 */
				TEST_ALERT(false, 1)		
			}
		}
		for (u64int i = 0; i < N - N/10; i += 1) {
			AString key = map.get_key(i);
			/** <doc-test-info>
			 *
			 */
			TEST_ALERT(key == (AStringLibs::to_string(i + N/10) + "key"), 2)
			AString data = *map.get_data(i);
			/** <doc-test-info>
			 *
			 */
			TEST_ALERT(data == (AStringLibs::to_string(i + N/10) + "data"), 3)
		}
		TEST_ALERT(map.count() == N - N/10, 4)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
		AMap<AString, AString> map;
		
		map.add("key1", "data1");
		map.add("key2", "data2");
		map.add("key3", "data3");
		
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(map.count() == 3, 1)

		APtr<AString> string;

		string = map.get("key2");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(*string == "data2", 2)

		string = map.get("key4");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(string.is_empty(), 3)

	}
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(amap)

	TEST_CALL(1)
	TEST_CALL(2)

TEST_FUNCTION_MAIN_END(AMAP)

#endif
