#include "AStack.exception.hpp"

AStackException::AStackException() : AllianceException() {
	exception = "Alliance-Exception-AStack";
}

AStackException::AStackException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStack");
}

AStackException::AStackException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStack");
}

AStackException::AStackException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStack");
}

AStackException::AStackException(const AStackException & obj) : AllianceException(obj) {
	
}

AStackException::~AStackException() {

}

