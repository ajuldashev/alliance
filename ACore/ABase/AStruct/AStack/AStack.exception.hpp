#ifndef HPP_ASTACKEXCEPTION
#define HPP_ASTACKEXCEPTION
#include <alliance.exception.hpp>

class AStackException : public AllianceException {
public:
	AStackException();
	AStackException(std::string text);
	AStackException(AllianceException & e);
	AStackException(AllianceException & e, std::string text);
	AStackException(const AStackException & obj);
	~AStackException();
};

#endif

