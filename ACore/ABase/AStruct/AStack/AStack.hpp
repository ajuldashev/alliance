#ifndef HPP_ASTACK
#define HPP_ASTACK
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "AStack.exception.hpp"

template <typename T> 
class AStack {
	class Node;
private:
	APtr<Node> head;
	APtr<s64int> size;
public:
	AStack();
	AStack(const AStack<T> & stack);

	s64int length();
	void push(T data);
	void pop();
	T    get();
	T&   at();
	void clear();
	void swap();
	AStack clone();
	AStack & operator=(AStack stack);

	~AStack();	
};

template <typename T> 
class AStack<T>::Node {
public:
	APtr<Node> next;
	T data;
};

template <typename T> 
AStack<T>::AStack() {
	size = APtr<s64int>::make();
	*size = 0;
}

template <typename T> 
AStack<T>::AStack(const AStack<T> & stack) {
	head = stack.head;
	size = stack.size;
}

template <typename T>
s64int AStack<T>::length() {
	return *size;
}

template <typename T>
void AStack<T>::push(T data) {
	if (!head) {
		head = APtr<Node>::make();
		head->data = data;
		*size = 1;
		return;
	}
	APtr<Node>temp = APtr<Node>::make();
	temp->data = data;
	*size += 1;
	temp->next = head;
	head = temp;
}

template <typename T> 
T & AStack<T>::at() {
	try {
		return head->data;
	} catch (AllianceException & e) {
		throw AStackException(e, "at()");
	}
}

template <typename T> 
T AStack<T>::get() {
	try {
		return head->data;
	} catch (AllianceException & e) {
		throw AStackException(e, "get()");
	}
}

template <typename T> 
void AStack<T>::pop() {
	try {
		head = head->next;
		*size -= 1;
	} catch (AllianceException & e) {
		throw AStackException(e, "pop()");
	}
}

template <typename T> 
void AStack<T>::clear() {
	head.reset();
	size = APtr<s64int>::make();
	*size = 0;
}

template <typename T>
void AStack<T>::swap() {
	try {
		APtr<Node> new_head;
		while (head) {
			APtr<Node> temp = head;
			head = head->next;
			temp->next = new_head;
			new_head = temp;
		}
		head = new_head;
	} catch (AllianceException & e) {
		throw AStackException(e, "swap()");
	}
}

template <typename T>
AStack<T> AStack<T>::clone() {
	AStack<T> res;
	for (APtr<Node> i = head; i; i = i->next) {
		res.push(i->data);
	}
	res.swap();
	return res;
}

template <typename T>
AStack<T> & AStack<T>::operator=(AStack stack) {
	head = stack.head;
	size = stack.size;
	return *this;
}

template <typename T> 
AStack<T>::~AStack() {
	clear();
}

#endif
