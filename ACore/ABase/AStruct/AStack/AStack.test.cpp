#include "AStack.hpp"

#ifdef TEST_ASTACK
#include <ACore/ABase/AIO/AIO.hpp>
bool test_astack(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_ASTACK );

   if (!test_node_astack(is_soft)) {
      return is_soft;
   }

   {
      AIO::writeln_warning("Test AStack() && length() == 0");
      AStack<s32int> stack;
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test push(17)");
      AStack<s32int> stack;
      stack.push(17);
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test push() 17, 1, 441 && pop()");
      AStack<s32int> stack;
      stack.push(17);
      stack.push(1);
      stack.push(441);
      if (
         stack.length() != 3 || 
         stack.at()  != 441   || 
         stack.get() != 441
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (
         stack.length() != 2 || 
         stack.at()  != 1   || 
         stack.get() != 1
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 3 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test clear() 17, 1, 441");
      AStack<s32int> stack;
      stack.push(17);
      stack.push(1);
      stack.push(441);
      if (stack.length() != 3) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.clear();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test swap() 17, 1, 441");
      AStack<s32int> stack;
      stack.push(441);
      stack.push(1);
      stack.push(17);
      stack.swap();
      if (
         stack.length() != 3 || 
         stack.at()  != 441   || 
         stack.get() != 441
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (
         stack.length() != 2 || 
         stack.at()  != 1   || 
         stack.get() != 1
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 3 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test swap() 17, 1");
      AStack<s32int> stack;
      stack.push(1);
      stack.push(17);
      stack.swap();
      if (
         stack.length() != 2 || 
         stack.at()  != 1   || 
         stack.get() != 1
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test swap() 17");
      AStack<s32int> stack;
      stack.push(17);
      stack.swap();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test swap() NULL");
      AStack<s32int> stack;
      stack.swap();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test clone() 17, 1, 441");
      AStack<s32int> stack_origin;
      stack_origin.push(17);
      stack_origin.push(1);
      stack_origin.push(441);
      AStack<s32int> stack = stack_origin.clone();
      if (
         stack.length() != 3 || 
         stack.at()  != 441   || 
         stack.get() != 441
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (
         stack.length() != 2 || 
         stack.at()  != 1   || 
         stack.get() != 1
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 3 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test clone() 17, 1");
      AStack<s32int> stack_origin;
      stack_origin.push(17);
      stack_origin.push(1);
      AStack<s32int> stack = stack_origin.clone();
      if (
         stack.length() != 2 || 
         stack.at()  != 1   || 
         stack.get() != 1
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test clone() 17");
      AStack<s32int> stack_origin;
      stack_origin.push(17);
      AStack<s32int> stack = stack_origin.clone();
      if (
         stack.length() != 1 || 
         stack.at()  != 17   || 
         stack.get() != 17
      ) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      stack.pop();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test clone() NULL");
      AStack<s32int> stack_origin;
      AStack<s32int> stack = stack_origin.clone();
      if (stack.length() != 0) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   {
      AIO::writeln_warning("Test operator = () 17, 1, 441");
      AStack<s32int> stack_origin;
      stack_origin.push(17);
      stack_origin.push(1);
      stack_origin.push(441);
      if (stack_origin.length() != 3) {
         return is_soft;
      }
      AIO::writeln_success("step 1 done");
      AStack<s32int> stack;
      stack = stack_origin;
      if (stack_origin.length() != 3 || stack.length() != 3) {
         return is_soft;
      }
      AIO::writeln_success("step 2 done");
      stack.pop();
      if (stack_origin.length() != 2 || stack.length() != 2) {
         return is_soft;
      }
      AIO::writeln_success("step 3 done");
      stack_origin.pop();
      if (stack_origin.length() != 1 || stack.length() != 1) {
         return is_soft;
      }
      AIO::writeln_warning("Done");
   }

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_ASTACK );
   return true;
}
#endif