#include "AStruct.hpp"

#ifdef DEBUG_ASTRUCT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_astruct() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRUCT );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_astruct());
			AIO::writeln(" 1) help");
			AIO::writeln("10) stop");
			AIO::writeln(" e) exit");	
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_astruct(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ASTRUCT );
	AIO::write_div_line();
	return false;
}
#endif

