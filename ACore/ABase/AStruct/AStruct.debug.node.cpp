#include "AStruct.hpp"

#ifdef DEBUG_ASTRUCT
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_astruct(AString com) {
	#ifdef DEBUG_AARRAY
	if (com == "aarray") {
		if (debug_aarray()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ALIST
	if (com == "alist") {
		if (debug_alist()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMAP
	if (com == "amap") {
		if (debug_amap()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTACK
	if (com == "astack") {
		if (debug_astack()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRUCTITERATOR
	if (com == "astructiterator") {
		if (debug_astructiterator()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATREE
	if (com == "atree") {
		if (debug_atree()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATREEBINARY
	if (com == "atreebinary") {
		if (debug_atreebinary()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATREEMAP
	if (com == "atreemap") {
		if (debug_atreemap()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATREENAV
	if (com == "atreenav") {
		if (debug_atreenav()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATREESEARCH
	if (com == "atreesearch") {
		if (debug_atreesearch()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AVECTOR
	if (com == "avector") {
		if (debug_avector()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_astruct() {
	AString text;
	#ifdef DEBUG_AARRAY
	text += "d) aarray\n";
	#endif
	#ifdef DEBUG_ALIST
	text += "d) alist\n";
	#endif
	#ifdef DEBUG_AMAP
	text += "d) amap\n";
	#endif
	#ifdef DEBUG_ASTACK
	text += "d) astack\n";
	#endif
	#ifdef DEBUG_ASTRUCTITERATOR
	text += "d) astructiterator\n";
	#endif
	#ifdef DEBUG_ATREE
	text += "d) atree\n";
	#endif
	#ifdef DEBUG_ATREEBINARY
	text += "d) atreebinary\n";
	#endif
	#ifdef DEBUG_ATREEMAP
	text += "d) atreemap\n";
	#endif
	#ifdef DEBUG_ATREENAV
	text += "d) atreenav\n";
	#endif
	#ifdef DEBUG_ATREESEARCH
	text += "d) atreesearch\n";
	#endif
	#ifdef DEBUG_AVECTOR
	text += "d) avector\n";
	#endif
	return text;
}

#endif
