#include "AStruct.exception.hpp"

AStructException::AStructException() : AllianceException() {
	exception = "Alliance-Exception-AStruct";
}

AStructException::AStructException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStruct");
}

AStructException::AStructException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStruct");
}

AStructException::AStructException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStruct");
}

AStructException::AStructException(const AStructException & obj) : AllianceException(obj) {
	
}

AStructException::~AStructException() {

}

