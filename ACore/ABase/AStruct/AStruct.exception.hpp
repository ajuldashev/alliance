#ifndef HPP_ASTRUCTEXCEPTION
#define HPP_ASTRUCTEXCEPTION
#include <alliance.exception.hpp>

class AStructException : public AllianceException {
public:
	AStructException();
	AStructException(std::string text);
	AStructException(AllianceException & e);
	AStructException(AllianceException & e, std::string text);
	AStructException(const AStructException & obj);
	~AStructException();
};

#endif

