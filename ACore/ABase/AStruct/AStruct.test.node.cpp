#include "AStruct.hpp"

#ifdef TEST_ASTRUCT
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_astruct(bool is_soft) {
	#ifdef TEST_AARRAY
	if (!test_aarray(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AARRAY );
		return is_soft;
	}
	#endif
	#ifdef TEST_ALIST
	if (!test_alist(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ALIST );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMAP
	if (!test_amap(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMAP );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTACK
	if (!test_astack(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTACK );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRUCTITERATOR
	if (!test_astructiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRUCTITERATOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATREE
	if (!test_atree(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATREE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATREEBINARY
	if (!test_atreebinary(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATREEBINARY );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATREEMAP
	if (!test_atreemap(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATREEMAP );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATREENAV
	if (!test_atreenav(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATREENAV );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATREESEARCH
	if (!test_atreesearch(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATREESEARCH );
		return is_soft;
	}
	#endif
	#ifdef TEST_AVECTOR
	if (!test_avector(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AVECTOR );
		return is_soft;
	}
	#endif
	return true;
}
#endif
