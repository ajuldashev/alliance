#include "AStructIterator.hpp"

#ifdef DEBUG_ASTRUCTITERATOR
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_astructiterator() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRUCTITERATOR );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astructiterator());           
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astructiterator(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRUCTITERATOR );
    AIO::write_div_line();
    return false;
}
#endif

