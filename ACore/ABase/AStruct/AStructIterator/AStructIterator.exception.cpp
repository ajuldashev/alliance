#include "AStructIterator.exception.hpp"

AStructIteratorException::AStructIteratorException() : AllianceException() {
	exception = "Alliance-Exception-AStructIterator";
}

AStructIteratorException::AStructIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStructIterator");
}

AStructIteratorException::AStructIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStructIterator");
}

AStructIteratorException::AStructIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStructIterator");
}

AStructIteratorException::AStructIteratorException(const AStructIteratorException & obj) : AllianceException(obj) {
	
}

AStructIteratorException::~AStructIteratorException() {

}

