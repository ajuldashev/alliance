#ifndef HPP_ASTRUCTITERATOREXCEPTION
#define HPP_ASTRUCTITERATOREXCEPTION
#include <alliance.exception.hpp>

class AStructIteratorException : public AllianceException {
public:
	AStructIteratorException();
	AStructIteratorException(std::string text);
	AStructIteratorException(AllianceException & e);
	AStructIteratorException(AllianceException & e, std::string text);
	AStructIteratorException(const AStructIteratorException & obj);
	~AStructIteratorException();
};

#endif

