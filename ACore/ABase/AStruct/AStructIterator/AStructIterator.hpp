#ifndef HPP_ASTRUCTITERATOR
#define HPP_ASTRUCTITERATOR
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include "AStructIterator.exception.hpp"

template <typename T>
class AStructIterator {
protected:
    bool (*vfunc_has_next)(AStructIterator<T> & i);
    T    (*vfunc_get_next)(AStructIterator<T> & i);
    void (*vfunc_reset)(AStructIterator<T> & i);
    void (*vfunc_clear)(AStructIterator<T> & i);
public:
    AStructIterator();
    bool has_next();
    T    get_next();
    void reset();
    void clear();
    ~AStructIterator();
};

template <typename T>
static bool func_has_next_astructiterator(AStructIterator<T> & i) {
    throw AStructIteratorException("has_next() : NULL");
}

template <typename T>
static T func_get_next_astructiterator(AStructIterator<T> & i) {
    throw AStructIteratorException("get_next() : NULL");
}

template <typename T>
static void func_reset_astructiterator(AStructIterator<T> & i) {
    throw AStructIteratorException("reset() : NULL");
}

template <typename T>
static void func_clear_astructiterator(AStructIterator<T> & i) {
    throw AStructIteratorException("clear() : NULL");
}

template <typename T>
AStructIterator<T>::AStructIterator() {
    vfunc_has_next = func_has_next_astructiterator<T>;
    vfunc_get_next = func_get_next_astructiterator<T>;
    vfunc_reset = func_reset_astructiterator<T>;
    vfunc_clear = func_clear_astructiterator<T>;
}

template <typename T>
bool AStructIterator<T>::has_next() {
    return vfunc_has_next(*this);
}

template <typename T>
T AStructIterator<T>::get_next() {
    return vfunc_get_next(*this);
}

template <typename T>
void AStructIterator<T>::reset() {
    vfunc_reset(*this);
}

template <typename T>
void AStructIterator<T>::clear() {
    vfunc_clear(*this);
}

template <typename T>
AStructIterator<T>::~AStructIterator() {

}

#endif