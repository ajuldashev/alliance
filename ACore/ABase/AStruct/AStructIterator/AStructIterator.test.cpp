#include "AStructIterator.hpp"

#ifdef TEST_ASTRUCTITERATOR
#include <ACore/ABase/AIO/AIO.hpp>
bool test_astructiterator(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_ASTRUCTITERATOR );

   if (!test_node_astructiterator(is_soft)) {
      return is_soft;
   }
     
   AIO::writeln_warning("Test has_next()");
   try {
      AStructIterator<s32int> iterator;
      iterator.has_next();
   } catch(AStructIteratorException e) {
		AIO::writeln_warning(e.get_text().c_str());
	} catch(...) {
      return is_soft;
   }
   AIO::writeln_success("Done");
   
   AIO::writeln_warning("Test get_next()");
   try {
      AStructIterator<s32int> iterator;
      iterator.get_next();
   } catch(AStructIteratorException e) {
		AIO::writeln_warning(e.get_text().c_str());
	} catch(...) {
      return is_soft;
   }
   AIO::writeln_success("Done");

   AIO::writeln_warning("Test reset()");
   try {
      AStructIterator<s32int> iterator;
      iterator.reset();
   } catch(AStructIteratorException e) {
		AIO::writeln_warning(e.get_text().c_str());
	} catch(...) {
      return is_soft;
   }
   AIO::writeln_success("Done");

   AIO::writeln_warning("Test clear()");
   try {
      AStructIterator<s32int> iterator;
      iterator.clear();
   } catch(AStructIteratorException e) {
		AIO::writeln_warning(e.get_text().c_str());
	} catch(...) {
      return is_soft;
   }
   AIO::writeln_success("Done");

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_ASTRUCTITERATOR );
   return true;
}
#endif

