#include "ATree.hpp"

#ifdef DEBUG_ATREE
#include <ACore/ABase/AIO/AIO.hpp>

void debug_print(APtrWeak<ATree<s32int>> tree, AString tab = "") {
    if (tree->is_empty()) {
        return;
    }
    if (tree->has_data()) {
        AIO::write(tab);
        AIO::write(tree->get());
    }
    if (tree->has_children()) {
        AIO::writeln(" {");
        for (s32int i = 0; i < tree->length(); i += 1) {
            debug_print(tree->at(i), tab + "  ");
        }
        AIO::writeln(tab + "}");
    } else {
        AIO::writeln();
    }
}

bool debug_atree() {
    ATree<s32int> tree;
    APtrWeak<ATree<s32int>> current = & tree;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATREE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_atree());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) add");
            AIO::writeln(" 3) add_child");
            AIO::writeln(" 4) insert");
            AIO::writeln(" 5) replace");
            AIO::writeln(" 6) remove");
            AIO::writeln(" 7) remove_children");
            AIO::writeln(" 8) has_data");
            AIO::writeln(" 9) has_parent");
            AIO::writeln("10) has_children");
            AIO::writeln("11) is_empty");
            AIO::writeln("12) at_pos");
            AIO::writeln("13) at_parent");
            AIO::writeln("14) at");
            AIO::writeln("15) get");
            AIO::writeln("16) length");
            AIO::writeln("17) free_data");
            AIO::writeln("18) free");
            AIO::writeln("19) status");            
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_atree(com)) {
			return true;
		}
        if (com == "add") {
            s32int val = AIO::get_s32int();
            current->add(val);
        }
        if (com == "add_child") {
            s32int val = AIO::get_s32int();
            current->add_child(val);
        }
        if (com == "insert") {
            s32int val = AIO::get_s32int();
            s32int pos = AIO::get_s32int();
            current->insert(val, pos);
        }
        if (com == "replace") {
            s32int val = AIO::get_s32int();
            s32int pos = AIO::get_s32int();
            current->replace(val, pos);
        }
        if (com == "remove") {
            s32int pos = AIO::get_s32int();
            current->remove(pos);
        }
        if (com == "remove_children") {
            current->remove_children();
        }
        if (com == "has_data") {
            AIO::writeln(current->has_data());
        }
        if (com == "has_parent") {
            AIO::writeln(current->has_parent());
        }
        if (com == "has_children") {
            AIO::writeln(current->has_children());
        }
        if (com == "is_empty") {
            AIO::writeln(current->is_empty());
        }
        if (com == "at_pos") {
            current = current->at(AIO::get_u32int());
        }
        if (com == "at_parent") {
            current = current->at_parent();
        }
        if (com == "at") {
            AIO::writeln(current->at());
        }
        if (com == "get") {
            AIO::writeln(current->get());
        }
        if (com == "length") {
            AIO::writeln(current->length());
        }
        if (com == "free_data") {
            current->free_data();
        }
        if (com == "free") {
            current->free();
        }
        if (com == "status") {
            debug_print(current);
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATREE );
    AIO::write_div_line();
    return false;
}
#endif

