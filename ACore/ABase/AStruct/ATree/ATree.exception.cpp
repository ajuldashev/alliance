#include "ATree.exception.hpp"

ATreeException::ATreeException() : AllianceException() {
	exception = "Alliance-Exception-ATree";
}

ATreeException::ATreeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATree");
}

ATreeException::ATreeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATree");
}

ATreeException::ATreeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATree");
}

ATreeException::ATreeException(const ATreeException & obj) : AllianceException(obj) {
	
}

ATreeException::~ATreeException() {

}

