#ifndef HPP_ATREEEXCEPTION
#define HPP_ATREEEXCEPTION
#include <alliance.exception.hpp>

class ATreeException : public AllianceException {
public:
	ATreeException();
	ATreeException(std::string text);
	ATreeException(AllianceException & e);
	ATreeException(AllianceException & e, std::string text);
	ATreeException(const ATreeException & obj);
	~ATreeException();
};

#endif

