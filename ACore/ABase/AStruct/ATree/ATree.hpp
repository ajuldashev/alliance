#ifndef HPP_ATREE
#define HPP_ATREE
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include "ATree.exception.hpp"

template <typename T>
class ATree {
private:
    AList<ATree> children;
    APtrWeak<ATree> parent;
    APtrUnique<T> data;
public:
    ATree();
    ATree(T & obj);
    void add(T & obj);
    void add_child(T & obj);
    void insert(T & obj, s64int pos);
    void replace(T & obj, s64int pos);
    void remove(s64int pos);
    void remove_children();
    bool has_data();
    bool has_parent();
    bool has_children();
    bool is_empty();
    APtrWeak<ATree<T>> at(s64int pos);
    APtrWeak<ATree<T>> at_parent();
    T & at();
    T  get();
    s64int length();
    void free_data();
    void free();
    ~ATree();
};

template <typename T>
ATree<T>::ATree() {

}

template <typename T>
ATree<T>::ATree(T & obj) {
    add(obj);
}

template <typename T>
void ATree<T>::add(T & obj) {
    if (data.is_empty()) {
        data = APtrUnique<T>::make();
    }
    *data = obj;
}

template <typename T>
void ATree<T>::add_child(T & obj) {
    ATree<T> tree(obj);
    tree.parent = APtrWeak<ATree<T>>(this);
    children.push_end(tree);
}

template <typename T>
void ATree<T>::insert(T & obj, s64int pos) {
    ATree<T> tree(obj);
    children.insert(tree, pos);
}

template <typename T>
void ATree<T>::replace(T & obj, s64int pos) {
    children.set(obj, pos);
}

template <typename T>
void ATree<T>::remove(s64int pos) {
    children.remove(pos, 1);
}

template <typename T>
void ATree<T>::remove_children() {
    children.clear();
}

template <typename T>
bool ATree<T>::has_data() {
    return !data.is_empty();
}

template <typename T>
bool ATree<T>::has_parent() {
    return !parent.is_empty();
}

template <typename T>
bool ATree<T>::has_children() {
    return children.length() > 0;
}

template <typename T>
bool ATree<T>::is_empty() {
    return !(has_data() || has_children());
}

template <typename T>
APtrWeak<ATree<T>> ATree<T>::at(s64int pos) {
    return APtrWeak<ATree<T>>(&children.at(pos));
}

template <typename T>
APtrWeak<ATree<T>> ATree<T>::at_parent() {
    return parent;
}

template <typename T>
T & ATree<T>::at() {
    return *data;
}

template <typename T>
T ATree<T>::get() {
    return *data;
}

template <typename T>
s64int ATree<T>::length() {
    return children.length();
}

template <typename T>
void ATree<T>::free_data() {
    data.reset();
}

template <typename T>
void ATree<T>::free() {
    remove_children();
    free_data();
}

template <typename T>
ATree<T>::~ATree() {
    free();
}

#endif