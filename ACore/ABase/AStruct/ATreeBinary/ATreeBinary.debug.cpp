#include "ATreeBinary.hpp"

#ifdef DEBUG_ATREEBINARY
#include <ACore/ABase/AIO/AIO.hpp>

void debug_print(ATreeBinary<s32int> & tree, AString tab = "") {
    if (!tree.is_empty()) {
        AIO::write(tab + " ");
        AIO::write(tree.get());
        AIO::write(" {");
        if (tree.goto_left()) {
            AIO::writeln();
            debug_print(tree, tab + "  ");
            tree.goto_parent();
        }
        if (tree.goto_right()) {
            AIO::writeln();
            debug_print(tree, tab + "  ");
            tree.goto_parent();
            AIO::write(tab);
        }
        AIO::write("}");
    }
    AIO::writeln();
}

bool debug_atreebinary() {
    ATreeBinary<s32int> tree;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATREEBINARY );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_atreebinary());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) get");
            AIO::writeln(" 3) set");
            AIO::writeln(" 4) add_right");
            AIO::writeln(" 5) add_left");
            AIO::writeln(" 6) goto_root");
            AIO::writeln(" 7) goto_parent");
            AIO::writeln(" 8) goto_right");
            AIO::writeln(" 9) goto_left");
            AIO::writeln("10) is_empty");
            AIO::writeln("11) has_parent");
            AIO::writeln("12) has_right");
            AIO::writeln("13) has_left");
            AIO::writeln("14) free_current");
            AIO::writeln("15) free_right");
            AIO::writeln("16) free_left");
            AIO::writeln("17) free");
            AIO::writeln("18) status");
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_atreebinary(com)) {
			return true;
		}
        if (com == "get") {
            AIO::writeln(tree.get());
        }
        if (com == "set") {
            tree.set(AIO::get_s32int());
        }
        if (com == "add_right") {
            tree.add_right(AIO::get_s32int());
        }
        if (com == "add_left") {
            tree.add_left(AIO::get_s32int());
        }
        if (com == "goto_root") {
            tree.goto_root();
        }
        if (com == "goto_parent") {
            tree.goto_parent();
        }
        if (com == "goto_right") {
            tree.goto_right();
        }
        if (com == "goto_left") {
            tree.goto_left();
        }
        if (com == "is_empty") {
            AIO::writeln(tree.is_empty());
        }
        if (com == "has_parent") {
            AIO::writeln(tree.has_parent());
        }
        if (com == "has_right") {
            AIO::writeln(tree.has_right());
        }
        if (com == "has_left") {
            AIO::writeln(tree.has_left());
        }
        if (com == "free_current") {
            tree.free_current();
        }
        if (com == "free_right") {
            tree.free_right();
        }
        if (com == "free_left") {
            tree.free_left();
        }
        if (com == "free") {
            tree.free();
        }
        if (com == "status") {
            debug_print(tree);
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATREEBINARY );
    AIO::write_div_line();
    return false;
}
#endif

