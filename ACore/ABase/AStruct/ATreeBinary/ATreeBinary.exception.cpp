#include "ATreeBinary.exception.hpp"

ATreeBinaryException::ATreeBinaryException() : AllianceException() {
	exception = "Alliance-Exception-ATreeBinary";
}

ATreeBinaryException::ATreeBinaryException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATreeBinary");
}

ATreeBinaryException::ATreeBinaryException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATreeBinary");
}

ATreeBinaryException::ATreeBinaryException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATreeBinary");
}

ATreeBinaryException::ATreeBinaryException(const ATreeBinaryException & obj) : AllianceException(obj) {
	
}

ATreeBinaryException::~ATreeBinaryException() {

}

