#ifndef HPP_ATREEBINARYEXCEPTION
#define HPP_ATREEBINARYEXCEPTION
#include <alliance.exception.hpp>

class ATreeBinaryException : public AllianceException {
public:
	ATreeBinaryException();
	ATreeBinaryException(std::string text);
	ATreeBinaryException(AllianceException & e);
	ATreeBinaryException(AllianceException & e, std::string text);
	ATreeBinaryException(const ATreeBinaryException & obj);
	~ATreeBinaryException();
};

#endif

