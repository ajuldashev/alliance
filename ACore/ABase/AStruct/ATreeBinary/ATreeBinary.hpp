#ifndef HPP_ATREEBINARYBINARY
#define HPP_ATREEBINARYBINARY
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "ATreeBinary.exception.hpp"

template <typename T>
class ATreeBinary{
private:
    class Node;
    APtrUnique<Node> head;
    APtrWeak<Node> current;
public:
    ATreeBinary();

    T & get();
    void set(T);
    void add_right(T);
    void add_left(T);

    bool goto_root();
    bool goto_parent();
    bool goto_right();
    bool goto_left();

    bool is_empty();
    bool has_parent();
    bool has_right();
    bool has_left();

    void free_current();
    void free_right();
    void free_left();
    void free();

    ~ATreeBinary();
};

template <typename T> 
class ATreeBinary<T>::Node {
public:
    APtrWeak<Node> parent;
	APtrUnique<Node> left;
	APtrUnique<Node> right;
	T obj;
};

template <typename T>
ATreeBinary<T>::ATreeBinary() {
    
}

template <typename T>
void ATreeBinary<T>::add_right(T obj) {
    if (current) {
        if (!current->right) {
            current->right = APtrUnique<Node>::make();
            current->right->parent = current;
            current->right->obj = obj;
            return;
        }
        throw ATreeBinaryException("add_right -> Error add obj, right is exist");
    }
    throw ATreeBinaryException("add_right -> Error add obj, current is NULL");
}

template <typename T>
void ATreeBinary<T>::add_left(T obj) {
    if (current) {
        if (!current->left) {
            current->left = APtrUnique<Node>::make();
            current->left->parent = current;
            current->left->obj = obj;
            return;
        }
        throw ATreeBinaryException("add_left -> Error add obj, left is exist");
    }
    throw ATreeBinaryException("add_left -> Error add obj, current is NULL");
}

template <typename T>
bool ATreeBinary<T>::goto_root() {
    current = head;
}

template <typename T>
bool ATreeBinary<T>::goto_parent() {
    if (current && current->parent) {
        current = current->parent;
        return true;
    }
    return false;
}

template <typename T>
bool ATreeBinary<T>::goto_right() {
    if (current && current->right) {
        current = current->right;
        return true;
    }
    return false;
}

template <typename T>
bool ATreeBinary<T>::goto_left() {
    if (current && current->left) {
        current = current->left;
        return true;
    }
    return false;
}

template <typename T>
bool ATreeBinary<T>::is_empty() {
    return current.is_empty();
}

template <typename T>
bool ATreeBinary<T>::has_parent() {
    if (current && current->parent) {
        return true;
    }
    return false;
}

template <typename T>
bool ATreeBinary<T>::has_right() {
    if (current && current->right) {
        return true;
    }
    return false;
}

template <typename T>
bool ATreeBinary<T>::has_left() {
    if (current && current->left) {
        return true;
    }
    return false;
}

template <typename T>
void ATreeBinary<T>::free_current() {
    if (current && current->parent) {
        APtrWeak<Node> temp = current;
        current = current->parent;
        if (current->left == temp) {
            current->left.reset();
        } else {
            current->right.reset();
        }
        return;
    }
    if (current) {
        head.reset();
        current.reset();
    }
}

template <typename T>
void ATreeBinary<T>::free_right() {
    if (current && current->right) {
        current->right.reset();
    }
}

template <typename T>
void ATreeBinary<T>::free_left() {
    if (current && current->left) {
        current->left.reset();
    }
}

template <typename T>
void ATreeBinary<T>::free() {
    head.reset();
    current.reset();
}

template <typename T>
T & ATreeBinary<T>::get() {
    if (current) {
        return current->obj;
    }
    throw ATreeBinaryException("get -> Error get obj, current is NULL");
}

template <typename T>
void ATreeBinary<T>::set(T obj) {
    if (!current) {
        head = APtrUnique<Node>::make();
        current = head;
    }
    head->obj = obj;
}

template <typename T>
ATreeBinary<T>::~ATreeBinary() {
	free();
}

#endif
