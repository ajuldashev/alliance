#include "ATreeMap.exception.hpp"

ATreeMapException::ATreeMapException() : AllianceException() {
	exception = "Alliance-Exception-ATreeMap";
}

ATreeMapException::ATreeMapException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATreeMap");
}

ATreeMapException::ATreeMapException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATreeMap");
}

ATreeMapException::ATreeMapException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATreeMap");
}

ATreeMapException::ATreeMapException(const ATreeMapException & obj) : AllianceException(obj) {
	
}

ATreeMapException::~ATreeMapException() {

}