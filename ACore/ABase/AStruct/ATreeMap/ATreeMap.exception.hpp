#ifndef HPP_ATREEMAPEXCEPTION
#define HPP_ATREEMAPEXCEPTION
#include <alliance.exception.hpp>

class ATreeMapException : public AllianceException {
public:
	ATreeMapException();
	ATreeMapException(std::string text);
	ATreeMapException(AllianceException & e);
	ATreeMapException(AllianceException & e, std::string text);
	ATreeMapException(const ATreeMapException & obj);
	~ATreeMapException();
};

#endif