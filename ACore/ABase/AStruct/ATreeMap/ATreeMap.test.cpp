#include "ATreeMap.hpp"

#ifdef TEST_ATREEMAP
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
bool test_atreemap(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_ATREEMAP );

   if (!test_node_atreemap(is_soft)) {
      return is_soft;
   }

   ATreeMap tree;
   s32int N = 1000;
   AString buff;
   {
      AIO::writeln_warning("Test add [1 .. 1000]");
      for (s32int i = N/10; i < N; i += 1) {
         AStringLibs::to_string(buff, i);
         tree.add(buff);
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test find [1 .. 1000]");
      for (s32int i = N/10; i < N; i += 1) {
         AStringLibs::to_string(buff, i);
         if (tree.find(buff) != i - N/10) {
            AIO::writeln_error("i              : ");
            AIO::writeln(i);
            AIO::writeln_error("tree.find(str) : ");
            AIO::writeln(tree.find(buff));
            return is_soft;
         }
      }
      AIO::writeln_success("Done");
   }

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_ATREEMAP );
   return true;
}
#endif

