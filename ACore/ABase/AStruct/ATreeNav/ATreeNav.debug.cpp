#include "ATreeNav.hpp"

#ifdef DEBUG_ATREENAV
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_atreenav() {
    ATreeNav<s32int> tree;
    APtrWeak<ATreeNav<s32int>> current = & tree;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATREENAV );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_atreenav());
            AIO::writeln(" 1) help");       
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_atreenav(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATREENAV );
    AIO::write_div_line();
    return false;
}
#endif

