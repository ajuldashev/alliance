#include "ATreeNav.exception.hpp"

ATreeNavException::ATreeNavException() : AllianceException() {
	exception = "Alliance-Exception-ATreeNav";
}

ATreeNavException::ATreeNavException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATreeNav");
}

ATreeNavException::ATreeNavException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATreeNav");
}

ATreeNavException::ATreeNavException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATreeNav");
}

ATreeNavException::ATreeNavException(const ATreeNavException & obj) : AllianceException(obj) {
	
}

ATreeNavException::~ATreeNavException() {

}

