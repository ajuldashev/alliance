#ifndef HPP_ATREENAVEXCEPTION
#define HPP_ATREENAVEXCEPTION
#include <alliance.exception.hpp>

class ATreeNavException : public AllianceException {
public:
	ATreeNavException();
	ATreeNavException(std::string text);
	ATreeNavException(AllianceException & e);
	ATreeNavException(AllianceException & e, std::string text);
	ATreeNavException(const ATreeNavException & obj);
	~ATreeNavException();
};

#endif

