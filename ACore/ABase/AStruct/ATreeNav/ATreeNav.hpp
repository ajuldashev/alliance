#ifndef HPP_ATREENAV
#define HPP_ATREENAV
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include <ACore/ABase/AStruct/ATree/ATree.hpp>
#include "ATreeNav.exception.hpp"

template <typename T>
class ATreeNav {
private:
    APtrWeak<ATree<T>> tree_head;
    APtrWeak<ATree<T>> tree_current;
public:
    ATreeNav();
    ATreeNav(ATree<T> &);
    ATreeNav(ATreeNav &);
    ATreeNav(APtrWeak<ATree<T>> &);

    bool goto_parent();
    bool goto_child(s64int);
    bool goto_root();

    void set(T & obj);
    void add(T & obj);

    void insert(T & obj, s64int pos);
    void replace(T & obj, s64int pos);
    
    void remove(s64int pos);
    void remove();
    
    bool has_data();
    bool has_parent();
    bool has_children();
    
    bool is_empty();

    T & at();
    T & at(s64int);
    T get();
    T get(s64int);

    ATreeNav & operator=(ATreeNav&);
    ATreeNav & operator=(ATree<T>&);

    s64int length();
    
    void free_data();
    void free();

    void reset();

    ~ATreeNav();
};

template <typename T>
ATreeNav<T>::ATreeNav() {

}

template <typename T>
ATreeNav<T>::ATreeNav(ATree<T> & tree) {
    tree_head = &tree;
    tree_current = &tree;
}

template <typename T>
ATreeNav<T>::ATreeNav(ATreeNav & tree) {
    tree_head = tree.tree_head;
    tree_current = tree.tree_current;
}

template <typename T>
ATreeNav<T>::ATreeNav(APtrWeak<ATree<T>> & tree) {
    tree_head = tree;
    tree_current = tree;
}

template <typename T>
bool ATreeNav<T>::goto_parent() {
    if (!is_empty() && tree_current->has_parent()) {
        tree_current = tree_current->at_parent();
        return true;
    }
    return false;
}

template <typename T>
bool ATreeNav<T>::goto_child(s64int pos) {
    if (!is_empty() && tree_current->has_children()) {
        tree_current = tree_current->at(pos);
        return true;
    }
    return false;
}

template <typename T>
bool ATreeNav<T>::goto_root() {
    if (tree_head) {
        tree_current = tree_head;
        return true;
    }
    return false;
}

template <typename T>
void ATreeNav<T>::set(T & obj) {
    tree_current->add(obj);
}

template <typename T>
void ATreeNav<T>::add(T & obj) {
    tree_current->add_child(obj);
}

template <typename T>
void ATreeNav<T>::insert(T & obj, s64int pos) {
    tree_current->insert(obj, pos);
}

template <typename T>
void ATreeNav<T>::replace(T & obj, s64int pos) {
    tree_current->replace(obj, pos);
}

template <typename T>
void ATreeNav<T>::remove(s64int pos) {
    tree_current->remove(pos);
}

template <typename T>
void ATreeNav<T>::remove() {
    tree_current->remove_children();
}

template <typename T>
bool ATreeNav<T>::has_data() {
    return tree_current->has_data();
}

template <typename T>
bool ATreeNav<T>::has_parent() {
    return tree_current->has_parent();
}

template <typename T>
bool ATreeNav<T>::has_children() {
    return tree_current->has_children();
}

template <typename T>
bool ATreeNav<T>::is_empty() {
    return tree_current.is_empty();
}

template <typename T>
T & ATreeNav<T>::at() {
    return tree_current->at(); 
}

template <typename T>
T & ATreeNav<T>::at(s64int pos) {
    return tree_current->at(pos)->at(); 
}

template <typename T>
T ATreeNav<T>::get() {
    return tree_current->get(); 
}

template <typename T>
T ATreeNav<T>::get(s64int pos) {
    return tree_current->at(pos)->get();
}

template <typename T>
ATreeNav<T> & ATreeNav<T>::operator=(ATreeNav & tree) {
    tree_head = tree.tree_head;
    tree_current = tree.tree_current;
    return *this;
}

template <typename T>
ATreeNav<T> & ATreeNav<T>::operator=(ATree<T> & tree) {
    tree_head = &tree;
    tree_current = &tree;
    return *this;
}

template <typename T>
s64int ATreeNav<T>::length() {
    return tree_current->length();
}
    
template <typename T>
void ATreeNav<T>::free_data() {
    tree_current->free_data();
}
    
template <typename T>
void ATreeNav<T>::free() {
    tree_current->free();
}

template <typename T>
void ATreeNav<T>::reset() {
    tree_head.reset();
    tree_current.reset();
}

template <typename T>
ATreeNav<T>::~ATreeNav() {
    reset();
}

#endif