#include "ATreeSearch.hpp"

#ifdef DEBUG_ATREESEARCH
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_atreesearch() {
    ATreeSearch<s64int> tree;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATREESEARCH );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_atreesearch());
            AIO::writeln("1) help");
            AIO::writeln("2) add");
            AIO::writeln("3) get");
            AIO::writeln("4) find");
            AIO::writeln("5) print");
            AIO::writeln("0) stop");	
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_atreesearch(com)) {
			return true;
		}
        if (com == "add") {
            s64int num = AIO::get_s64int();
            tree.add(num);
        }
        if (com == "get") {
            s64int num = AIO::get_s64int();
            AIO::writeln(tree.get(num));
        }
        if (com == "find") {
            s64int num = AIO::get_s64int();
            AIO::writeln(tree.find(num));
        }
        if (com == "print") {
            tree.print();
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATREESEARCH );
    AIO::write_div_line();
    return false;
}
#endif

