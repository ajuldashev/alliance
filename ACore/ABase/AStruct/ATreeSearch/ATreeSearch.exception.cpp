#include "ATreeSearch.exception.hpp"

ATreeSearchException::ATreeSearchException() : AllianceException() {
	exception = "Alliance-Exception-ATreeSearch";
}

ATreeSearchException::ATreeSearchException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATreeSearch");
}

ATreeSearchException::ATreeSearchException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATreeSearch");
}

ATreeSearchException::ATreeSearchException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATreeSearch");
}

ATreeSearchException::ATreeSearchException(const ATreeSearchException & obj) : AllianceException(obj) {
	
}

ATreeSearchException::~ATreeSearchException() {

}

