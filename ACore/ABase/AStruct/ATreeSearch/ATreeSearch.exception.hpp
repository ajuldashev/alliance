#ifndef HPP_ATREESEARCHEXCEPTION
#define HPP_ATREESEARCHEXCEPTION
#include <alliance.exception.hpp>

class ATreeSearchException : public AllianceException {
public:
	ATreeSearchException();
	ATreeSearchException(std::string text);
	ATreeSearchException(AllianceException & e);
	ATreeSearchException(AllianceException & e, std::string text);
	ATreeSearchException(const ATreeSearchException & obj);
	~ATreeSearchException();
};

#endif

