#ifndef HPP_ATREESEARCH
#define HPP_ATREESEARCH
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AIO/AIO.hpp>
#include "ATreeSearch.exception.hpp"

template <typename T>
class ATreeSearch{
private:
    class Node;
    Node * root;
    s64int add_rec(Node *, T &);
    s64int find_rec(Node *, T &, s64int);
    AString ftab(s64int);
    bool test_left(Node *);
    bool test_right(Node *);
    Node * rotation_left(Node *);
    Node * rotation_right(Node *);
    template <typename R>
    bool abs(R a) {
        return (a < 0)? -a : a;
    }
public:
    ATreeSearch();
    s64int add(T &);
    T get(s64int);
    s64int find(T);
    void print(Node * node = NULL, s64int tab = 0, AString stab = "");
    ~ATreeSearch();
};

template <typename T> 
class ATreeSearch<T>::Node {
public:
    Node();
    Node * left;
    Node * right;
    Node * parent;
    s64int size;
    T data;
    ~Node();
};

template <typename T>
ATreeSearch<T>::Node::Node() {
    left = NULL;
    right = NULL;
    parent = NULL;
    size = 0;
}

template <typename T>
ATreeSearch<T>::Node::~Node() {
    if (left) {
        delete left;
    }
    if (right) {
        delete right;
    }
    size = 0;
}

template <typename T>
s64int ATreeSearch<T>::add_rec(Node * node, T & data) {
    if (node) {
        if (node->data == data) {
            if (node->left) {
                return node->left->size;
            }
            return 0;
        }
        if (node->data > data) {
            if (node->left == NULL) {
                node->left = new Node();
                node->size += 1;
                node->left->size = 1;
                node->left->data = data;
                node->left->parent = node;
                return 0;
            }
            s64int pos = add_rec(node->left, data);
            if (pos != -1) {
                node->size += 1;
                if (test_left(node)) {
                    if (node == root) {
                        node = rotation_left(node);
                        root = node;
                    } else {
                        node = rotation_left(node);
                    }
                }
                if (test_right(node)) {
                    if (node == root) {
                        node = rotation_right(node);
                        root = node;
                    } else {
                        node = rotation_right(node);
                    }
                }
            }
            return pos;
        }
        if (node->data < data) {
            if (node->right == NULL) {
                node->right = new Node();
                node->size += 1;
                node->right->size = 1;
                node->right->data = data;
                node->right->parent = node;
                s64int left_pos = 0;
                if (node->left) {
                    left_pos = node->left->size;
                }
                return left_pos + 1;
            }
            s64int pos = add_rec(node->right, data);
            if (pos != -1) {
                node->size += 1;
                if (test_left(node)) {
                    if (node == root) {
                        node = rotation_left(node);
                        root = node;
                    } else {
                        node = rotation_left(node);
                    }
                }
                if (test_right(node)) {
                    if (node == root) {
                        node = rotation_right(node);
                        root = node;
                    } else {
                        node = rotation_right(node);
                    }
                }
            }
            s64int left_pos = 0;
            if (node->left) {
                left_pos = node->left->size;
            }
            return pos + left_pos + 1;
        }
    }
    return -1;
}

template <typename T>
s64int ATreeSearch<T>::find_rec(Node * node, T & data, s64int pos) {
    if (node) {
        if (node->data == data) {
            if (node->left) {
                pos += node->left->size;
            }
            return pos;
        }
        if (node->data < data) {
            if (node->left) {
                pos += node->left->size;
            }
            return find_rec(node->right, data, pos + 1);
        }
        if (node->data > data) {
            return find_rec(node->left, data, pos);
        }
    }
    return -1;
}

template <typename T>
T ATreeSearch<T>::get(s64int pos) {
    T res;
    if (root) {
        if (pos >= 0 && pos < root->size) {
            Node * node = root;
            if (pos == 0 && !node->left) {
                return node->data;
            }
            while (pos >= 0) {
                if (node->left && pos == node->left->size) {
                    return node->data;
                }
                if (node->left && pos < node->left->size) {
                    node = node->left;
                    continue;
                }
                if (node->left && node->right) {
                    pos -= node->left->size + 1;
                    node = node->right;
                    continue;
                }
                if (pos == 0 && !node->left) {
                    return node->data;
                }
                if (node->right) {
                    pos -= 1;
                    node = node->right;
                    continue;
                }
                return node->data;
            }
        }
    }    
    return res;
}

template <typename T>
s64int ATreeSearch<T>::find(T data) {
    return find_rec(root, data, 0);
}

template <typename T>
AString ATreeSearch<T>::ftab(s64int tab) {
    AString res = "";
    for (s64int i = 0; i < tab; i += 1) {
        res = res + "  ";
    }
    return res;
}

template <typename T>
bool ATreeSearch<T>::test_left(Node * node) {
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->right && node->right->right) {
            new_right = node->right->right->size;
        }
        new_left = node->size - 1 - new_right;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template <typename T>
bool ATreeSearch<T>::test_right(Node *node) { 
    if (node) {
        s64int old_left = 0;
        s64int old_right = 0;
        if (node->left) {
            old_left = node->left->size;
        }
        if (node->right) {
            old_left = node->right->size;
        }
        s64int new_left = 0;
        s64int new_right = 0;
        if (node->left && node->left->left) {
            new_left = node->left->left->size;
        }
        new_right = node->size - 1 - new_left;
        if ((abs(new_right - new_left)) < (abs(old_right - old_left))) {
            return true;
        }
    }
    return false;
}

template <typename T>
typename ATreeSearch<T>::Node * 
ATreeSearch<T>::rotation_left(Node * node) {
    if (node ) {
        Node * pnode = node; 
        if (pnode->right) {
            s64int size = pnode->size;
            Node * rnode = pnode->right;
            if (rnode->left) {
                rnode->left->parent = pnode;
            }
            pnode->right = rnode->left;
            rnode->left = pnode;
            rnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = rnode;
                } else {
                    pnode->parent->right = rnode;
                }
            }
            pnode->parent = rnode;
            pnode = rnode;
            pnode->size = size;
            pnode->left->size = size - 1;
            if (pnode->right) {
                pnode->left->size -= pnode->right->size;
            }
            node = pnode;
        }
    }
    return node;
}

template <typename T>
typename ATreeSearch<T>::Node * 
ATreeSearch<T>::rotation_right(Node * node) { 
    if (node) {
        Node * pnode = node; 
        if (pnode->left) {
            s64int size = pnode->size;
            Node * lnode = pnode->left;
            if (lnode->right) {
                lnode->right->parent = pnode;
            }
            pnode->left = lnode->right;
            lnode->right = pnode;
            lnode->parent = pnode->parent;
            if (pnode->parent) {
                if (pnode->parent->left == pnode) {
                    pnode->parent->left = lnode;
                } else {
                    pnode->parent->right = lnode;
                }
            }
            pnode->parent = lnode;
            pnode = lnode;
            pnode->size = size;
            pnode->right->size = size - 1;
            if (pnode->left) {
                pnode->right->size -= pnode->left->size;
            }
            node = pnode;
        }
    }
    return node;
}

template <typename T>
ATreeSearch<T>::ATreeSearch() {
    root = NULL;
}

template <typename T>
s64int ATreeSearch<T>::add(T & data) {
    if (root == NULL) {
        root = new Node();
        root->size += 1;
        root->data = data;
        return 0;
    }
    return add_rec(root, data);
}

template <typename T>
void ATreeSearch<T>::print(Node * node, s64int tab, AString stab) {
    if (!node) {
        node = root;
    }
    if (node) {
        AIO::write(ftab(tab));
        AIO::write("Size ");
        AIO::write(node->size); 
        AIO::write( " : Data "); 
        AIO::write( node->data);
        if (node->left || node->right) {
            AIO::writeln(" {");
            if (node->left) {
                AIO::writeln(ftab(tab) + "Left : ");
                print(node->left, tab + 1);
            }
            if (node->right) {
                AIO::writeln(ftab(tab) + "Right : ");
                print(node->right, tab + 1);
            }
            AIO::writeln(ftab(tab) + "}");
        } else {
            AIO::writeln();
        }
        return;
    }  
}

template <typename T>
ATreeSearch<T>::~ATreeSearch() {
    if (root) {
        delete root;
    }
}

#endif