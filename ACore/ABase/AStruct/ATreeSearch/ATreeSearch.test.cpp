#include "ATreeSearch.hpp"

#ifdef TEST_ATREESEARCH
#include <ACore/ABase/AIO/AIO.hpp>
bool test_atreesearch(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_ATREESEARCH );

   if (!test_node_atreesearch(is_soft)) {
      return is_soft;
   }

   ATreeSearch<s32int> tree;
   s32int N = 1000;
   {
      AIO::writeln_warning("Test add [1 .. 1000]");
      for (s32int i = 0; i < N; i += 1) {
         tree.add(i);
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test find [1 .. 1000]");
      for (s32int i = 0; i < N; i += 1) {
         if (tree.find(i) != i) {
            return is_soft;
         }
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test get [1 .. 1000]");
      for (s32int i = 0; i < N; i += 1) {
         if (tree.get(i) != i) {
            AIO::writeln_error("i          : ");
            AIO::writeln(i);
            AIO::writeln_error("get(i)     : ");
            AIO::writeln(tree.get(i));
            return is_soft;
         }
      }
      AIO::writeln_success("Done");
   }
   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_ATREESEARCH );
   return true;
}
#endif

