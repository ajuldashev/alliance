#include "AVector.hpp"

#ifdef DEBUG_AVECTOR
#include <ACore/ABase/AIO/AIO.hpp>

void write_vector(AVector<s32int> vector) {
    AIO::write("size   : ");
    AIO::writeln(vector.size());
    AIO::write("onset  : ");
    AIO::writeln(vector.onset());
    AIO::write("length : ");
    AIO::writeln(vector.length());
    for(s32int idx = 0; idx < vector.onset(); idx += 1) {
        AIO::writeln("value: null");
    }
    for(s32int idx = 0; idx < vector.length(); idx += 1) {
        AIO::write("value: ");
        AIO::writeln(vector[idx]);
    }
    for(s32int idx = vector.onset() + vector.length(); idx < vector.size(); idx += 1) {
        AIO::writeln("value: null");
    }
}

bool debug_avector() {
    AVector<s32int> vector;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AVECTOR );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_avector());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) to_use");
            AIO::writeln(" 3) to_x2");
            AIO::writeln(" 4) to_normal");
            AIO::writeln(" 5) push_beg");
            AIO::writeln(" 6) push_end");
            AIO::writeln(" 7) insert");
            AIO::writeln(" 8) insert_arr");
            AIO::writeln(" 9) pop_beg");
            AIO::writeln("10) pop_end");
            AIO::writeln("11) get");
            AIO::writeln("12) pop");
            AIO::writeln("13) shr");
            AIO::writeln("14) shl");
            AIO::writeln("15) get_vec");
            AIO::writeln("16) sub_vec");
            AIO::writeln("17) remove");
            AIO::writeln("18) clear");
            AIO::writeln("19) status");
            AIO::writeln("20) =");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "to_use") {
            vector.to_use();
        }
        if (com == "to_x2") {
            vector.to_x2();
        }
        if (com == "to_normal") {
            vector.to_normal();
        }
        if (com == "push_beg") {
            s32int value = AIO::get_s32int();
            vector.push_beg(value);
        }
        if (com == "push_end") {
            s32int value = AIO::get_s32int();
            vector.push_end(value);
        }
        if (com == "insert") {
            s32int value = AIO::get_s32int();
            vector.insert(value, AIO::get_s32int());
        }
        if (com == "insert_arr") {
            s32int size = AIO::get_s32int();
            AVector<s32int> arr;
            for (s32int i = 0; i < size; i += 1) {
                s32int value = AIO::get_s32int();
                arr.push_end(value);
            }
            s32int value = vector.get(AIO::get_s32int());
            vector.insert(arr, value);
        }
        if (com == "pop_beg") {
            s32int value = vector.pop_beg();
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "pop_end") {
            s32int value = vector.pop_end();
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "get") {
            s32int value = vector.get(AIO::get_s32int());
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "pop") {
            s32int value = vector.get(AIO::get_s32int());
            AIO::write("value: ");
            AIO::writeln(value);
        }
        if (com == "shr") {
            vector.shr(AIO::get_s32int());
        }
        if (com == "shl") {
            vector.shl(AIO::get_s32int());
        }
        if (com == "get_vec") {
            s64int arg_1 = AIO::get_s32int();
            s64int arg_2 = AIO::get_s32int();
            AVector<s32int> vec = vector.get(arg_1, arg_2);
            write_vector(vec);
        }
        if (com == "sub_vec") {
            s64int arg_1 = AIO::get_s32int();
            s64int arg_2 = AIO::get_s32int();
            AVector<s32int> vec = vector.sub(arg_1, arg_2);
            write_vector(vec);
        }
        if (com == "remove") {
            s64int arg_1 = AIO::get_s32int();
            s64int arg_2 = AIO::get_s32int();
            vector.remove(arg_1, arg_2);
        }
        if (com == "clear") {
            vector.clear();
        }
        if (com == "status") {
            write_vector(vector);
        }
        if (com == "=") {
            AVector<AString> str1;
            AVector<AString> str2;
            std::cout << "Begin operator = " << std::endl;
            str1 = str2;
            std::cout << "End operator = " << std::endl; 
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_avector(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AVECTOR );
    AIO::write_div_line();
    return false;
}
#endif

