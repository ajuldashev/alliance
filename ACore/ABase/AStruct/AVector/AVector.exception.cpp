#include "AVector.exception.hpp"

AVectorException::AVectorException() : AllianceException() {
	exception = "Alliance-Exception-AVector";
}

AVectorException::AVectorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AVector");
}

AVectorException::AVectorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AVector");
}

AVectorException::AVectorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AVector");
}

AVectorException::AVectorException(const AVectorException & obj) : AllianceException(obj) {
	
}

AVectorException::~AVectorException() {

}

