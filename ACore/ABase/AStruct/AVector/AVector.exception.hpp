#ifndef HPP_AVECTOREXCEPTION
#define HPP_AVECTOREXCEPTION
#include <alliance.exception.hpp>

class AVectorException : public AllianceException {
public:
	AVectorException();
	AVectorException(std::string text);
	AVectorException(AllianceException & e);
	AVectorException(AllianceException & e, std::string text);
	AVectorException(const AVectorException & obj);
	~AVectorException();
};

#endif

