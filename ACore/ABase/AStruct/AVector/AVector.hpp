#ifndef HPP_AVECTOR
#define HPP_AVECTOR
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtrArray/APtrArray.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "AVector.exception.hpp"

template<typename T> 
class AVector { 
private:
    mutable APtrArray<T> array;
	s64int begin;
	s64int array_size;
	s64int current_size;
	void insert_before(T);
	void insert_in(T, s64int);
	void insert_after(T);
public:
	AVector();
    AVector(s64int);
	AVector(const AVector<T> &);

	void copy(AVector<T> &);

	s64int length() const;
	s64int onset()  const;
	s64int size()   const;

    void to_use();
	void to_x2();
	void to_normal();

	bool shr(s64int offset = 1);
	bool shl(s64int offset = 1);

	void insert(T, s64int);
	void insert(AVector<T> & , s64int);
	void push_beg(T); 
	void push_end(T);

	T & at(s64int);
	T get(s64int);
	T pop(s64int);
	AVector<T> get(s64int, s64int);
	AVector<T> sub(s64int, s64int);
	void remove(s64int, s64int);

	T pop_beg();
	T pop_end();

    T & operator[](s64int);
	AVector<T> & operator=(AVector<T>&);

	void clear();

	~AVector();
};

template<typename T> 
AVector<T>::AVector() {
	begin = 0;
    array_size = 1;
    current_size = 0;
	APtrArray<T> temp(array_size);
	array = temp;
}

template<typename T> 
AVector<T>::AVector(s64int size) {
	begin = 0;
    array_size = size;
    current_size = 0;
    APtrArray<T> temp(array_size);
	array = temp;
}

template<typename T>
AVector<T>::AVector(const AVector<T> & arr) {
	begin = arr.begin;
	array_size = arr.array_size;
	current_size = arr.current_size;
	APtrArray<T> ptr(array_size);
	array = ptr;
	for (s64int i = 0; i < current_size; i += 1) {
		array[begin + i] = arr.array[begin + i];
	}
}

template<typename T>
void AVector<T>::copy(AVector<T> & arr) {
	begin = arr.begin;
	array_size = arr.array_size;
	current_size = arr.current_size;
	APtrArray<T> ptr(array_size);
	array = ptr;
	for (s64int i = 0; i < current_size; i += 1) {
		array[begin + i] = arr[i];
	}
}

template<typename T>
s64int AVector<T>::length() const {
    return current_size;
}

template<typename T>
s64int AVector<T>::onset() const {
    return begin;
}

template<typename T>
s64int AVector<T>::size() const {
    return array_size;
}

template<typename T>
void AVector<T>::to_use() {
	if (current_size == 0) {
		current_size = 1;
		array_size = 1;
		begin = 0;
		APtrArray<T> temp(current_size);
		return;
	} 
    APtrArray<T> temp(current_size);
    for (s64int i = 0; i < current_size; i += 1) {
        temp[i] = array[begin + i];
    }
    array = temp;
    begin = 0;
	array_size = current_size;
}

template<typename T>
void AVector<T>::to_x2() {
	s64int new_size = 2*array_size;
	s64int new_begin = (new_size - current_size)/2;
	APtrArray<T> temp(new_size);
	for (s64int i = 0; i < current_size; i += 1) {
		temp[new_begin + i] = array[begin + i];
	}
	array = temp;
	begin = new_begin;
	array_size = new_size;
}

template<typename T>
void AVector<T>::to_normal() {
	s64int new_begin = (array_size - current_size)/2;
	if (begin > new_begin) {
		for (s64int i = 0; i < current_size; i += 1) {
			array[new_begin + i] = array[begin + i];
		}
	} else 
	if (begin < new_begin) { 
		for (s64int i = current_size - 1; i >= 0; i -= 1) {
			array[new_begin + i] = array[begin + i]; 
		}
	}
	begin = new_begin;
}

template<typename T>
bool AVector<T>::shr(s64int offset) {
	if (array_size - (begin + current_size) < offset) {
		return false;
	}
	s64int new_begin = begin + offset;
	for (s64int i = current_size - 1; i >= 0; i -= 1) {
		array[new_begin + i] = array[begin + i];
	}
	begin = new_begin;
	return true;
}

template<typename T>
bool AVector<T>::shl(s64int offset) {
	if (offset > begin) {
		return false;
	}
	s64int new_begin = begin - offset;
	for (s64int i = 0; i < current_size; i += 1) {
		array[new_begin + i] = array[begin + i];
	}
	begin = new_begin;
	return true;
}

template<typename T>
void AVector<T>::insert_before(T mem) {
	if (begin == 0) {
		to_x2();
		insert_before(mem);
		return;
	}
	begin -= 1;
	array[begin] = mem;
	current_size += 1;
}


template<typename T>
void AVector<T>::insert_in(T mem, s64int pos) {
	if (pos < current_size/2) {
		if (begin == 0) {
			to_x2();
		}
		for (s64int i = 0; i <= pos; i += 1) {
			array[begin + i - 1] = array[begin + i];
		}
		begin -= 1;	
		array[begin + pos] = mem;
		current_size += 1;
	} else {
		if (array_size == (begin + current_size)) {
			to_x2();
		}
		for (s64int i = current_size; i >= pos; i -= 1) {
			array[begin + i] = array[begin + i - 1];
		}
		array[begin + pos] = mem;
		current_size += 1;
	}
}

template<typename T>
void AVector<T>::insert_after(T mem) {
	if (array_size == (begin + current_size)) {
		to_x2();
	}
	array[begin + current_size] = mem;
	current_size += 1;
}

template<typename T>
void AVector<T>::insert(T mem, s64int pos) {
    if (current_size == 0) {
        array[begin] = mem;
        current_size += 1;
		return;
    }
    if (begin + current_size == array_size) {
        to_x2();
    }
	if (pos <= 0) {
		insert_before(mem);
		return;
	}
	if (pos >= current_size) {
		insert_after(mem);
		return;
	}
	insert_in(mem ,pos);
}

template<typename T>
void AVector<T>::insert(AVector<T> & arr, s64int pos) {
    if (current_size == 0) {
        copy(arr);
        return;
    }
    AVector<T> res(current_size + arr.length());
	if (pos <= 0) {
		for (s64int i = 0; i < arr.length(); ++i) {
			res.push_end(arr.at(i));
		}
		for (s64int i = 0; i < current_size; ++i) {
			res.push_end(at(i));
		}
	} else
	if (pos >= current_size) {
		for (s64int i = 0; i < current_size; ++i) {
			res.push_end(at(i));
		}
		for (s64int i = 0; i < arr.length(); ++i) {
			res.push_end(arr.at(i));
		}
	} else {
		for (s64int i = 0; i < pos; ++i) {
				res.push_end(at(i));
		}
		for (s64int i = 0; i < arr.length(); ++i) {
			res.push_end(arr.at(i));
		} 
		for (s64int i = pos; i < current_size; ++i) {
				res.push_end(at(i));
		}
	}
	copy(res);
}

template<typename T>
void AVector<T>::push_beg(T mem) {
	insert(mem, 0);
}

template<typename T>
void AVector<T>::push_end(T mem) {
	insert(mem, current_size);
}

template<typename T>
T & AVector<T>::at(s64int pos) {
	if (current_size == 0) {
		return array[begin];
	}
	if (pos <= 0) {
		return array[begin];
	}
	if (pos >= current_size) {
		return array[begin + current_size - 1];
	}
	return array[begin + pos];
}

template<typename T>
T AVector<T>::get(s64int pos) {
	if (current_size == 0) {
		return T(array[begin]);
	}
	if (pos <= 0) {
		return T(array[begin]);
	}
	if (pos >= current_size) {
		return T(array[begin + current_size - 1]);
	}
	return T(array[begin + pos]);
}

template<typename T>
T AVector<T>::pop(s64int pos) {
	if (current_size == 0) {
		return array[0];
	}
	s64int i;
	if (pos <= 0) {
		T res = array[begin];
		if (current_size > 1) {
			begin += 1;
		}
		current_size -= 1;
		if (current_size >= array_size/2 && current_size != 0) {
			to_use();
		}
		return res;
	}
	if (pos >= current_size - 1) {
		T res = array[begin + current_size - 1];
		current_size -= 1;
		if (current_size >= array_size/2 && current_size != 0) {
			to_use();
		}
		return res;
	}
	T res = array[begin + pos];
	if (pos < current_size/2) {
		for (s64int i = pos; i > 0; i -= 1) {
			array[begin + i] = array[begin + i - 1];
		}
		begin += 1;
	} else {
		for (s64int i = pos; i < current_size - 1; i += 1) {
			array[begin + i] = array[begin + i + 1];
		}
	}
	current_size -= 1;
	if (current_size >= array_size/2 && current_size != 0) {
		to_use();
	}
	return res;
}

template<typename T>
AVector<T> AVector<T>::sub(s64int pos, s64int len){
	AVector<T> res = get(pos, len);
	remove(pos, len);
	return res;
}

template<typename T>
AVector<T> AVector<T>::get(s64int pos, s64int len){
	if (len <= 0 || pos >= current_size) {
		AVector<T> res;
		return res;
	}
	if (pos == 0 && len >= current_size) {
		AVector<T> res;
		res.copy(*this);
		return res;
	}
	if (pos <= 0) {
		AVector<T> res(len);
		for (s64int i = 0; i < len; i += 1) {
			res.push_end(get(i));
		}
		return res;
	}
	if  (pos + len >= current_size) {
		AVector<T> res(current_size - pos);
		for (s64int i = pos; i < current_size; i += 1) {
			res.push_end(get(i));
		}
		return res;
	}
	AVector<T> res(len);
	for (s64int i = pos; i < pos + len; i += 1) {
		res.push_end(get(i));
	}
	return res;
}

template<typename T>
void AVector<T>::remove(s64int pos, s64int len){
	if (len <= 0 || pos >= current_size) {
		return;
	}
	if (pos == 0 && len >= current_size) {
		clear();
		return;
	}
	if (pos <= 0) {
		begin += len;
		current_size = (current_size <= pos + len)? pos : current_size - len ;
		if (current_size >= array_size/2 && current_size != 0) {
			to_use();
		}
		return;
	}
	if  (pos + len >= current_size) {
		current_size = pos;
		if (current_size >= array_size/2 && current_size != 0) {
			to_use();
		}
		return;
	}
	s64int l1 = pos;
	s64int l2 = current_size - (pos + len);
	if  (l1 < l2) {
		for (s64int i = 0; i < l1 ; i += 1) {
			array[begin + pos + len - i - 1] = array[begin + pos - i - 1];
		}
		begin += len;
	} else {
		for (s64int i = 0; i < l2; i += 1) {
			array[begin + pos + i] = array[begin + pos + i + len];
		}
	}
	current_size = (current_size <= pos + len)? pos : current_size - len ;
	if (current_size >= array_size/2 && current_size != 0) {
		to_use();
	}
}

template<typename T>
T AVector<T>::pop_beg() {
	return pop(0);
}

template<typename T>
T AVector<T>::pop_end() {
	return pop(current_size - 1);
}

template<typename T>
T & AVector<T>::operator[](s64int pos) {
	if (current_size == 0) {
		return array[begin];
	}
	if (pos <= 0) {
		return array[begin];
	}
	if (pos >= current_size) {
		return array[begin + current_size - 1];
	}
	return array[begin + pos];
}

template<typename T>
AVector<T> & AVector<T>::operator=(AVector<T> & arr) {
	copy(arr);
	return *this;
}

template<typename T>
void AVector<T>::clear() {
	begin = 0;
	array_size = 1;
    current_size = 0;
	APtrArray<T> temp(array_size);
	array = temp;
}

template<typename T>
AVector<T>::~AVector() {

}

#endif
