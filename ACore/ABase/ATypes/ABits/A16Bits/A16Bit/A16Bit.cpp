#include "A16Bit.hpp"
#include "A16Bit.test.cpp"
#include "A16Bit.test.node.cpp"
#include "A16Bit.debug.cpp"
#include "A16Bit.debug.node.cpp"
#include "A16Bit.exception.cpp"


A16Bit A16Bit::unpack(u8int l, u8int h) {
	A16Bit res;
	res.l = l;
	res.h = h;
	return res;
}

A16Bit A16Bit::unpack(u16int word) {
	A16Bit res;
	res.l = (word     ) & 0xFF;
	res.h = (word >> 8) & 0xFF;
	return res;
}

A16Bit A16Bit::unpack(A16Bit _pack) {
	return _pack;
}

u16int A16Bit::pack(u8int l, u8int h) {
	u16int res = 0;
	res = res | h;
	res = res << 8;
	res = res | l;
	return res;
}

u16int A16Bit::pack(u16int _unpack) {
	return _unpack;
}

u16int A16Bit::pack(A16Bit pack) {
	u16int res = 0;
	res = res | pack.h & 0xFF;
	res = res << 8;
	res = res | pack.l & 0xFF;
	return res;
}

bool A16Bit::test(AString str) {
	u64int len = str.length();
	if (len == 0) {
		return false;
	}
	if (len % 2 != 0) {
		return false;
	}
	for (int i = 0; i < len && i < 4; i += 1) {
		if (!((str[i] >= '0' && str[i] <= '9')
			|| (str[i] >= 'a' && str[i] <= 'f')
			|| (str[i] >= 'A' && str[i] <= 'F')) 
		) {
			return false;
		}
	}
	return true;
}

A16Bit A16Bit::from_text(AString text) {
	A16Bit res;
	if (test(text)) {
		u64int len = text.length();
		res.l = 0;
		res.h = 0;
		if (len > 3) {
			res.h |= AStringLibs::from_hex_num(text[3]);
		} 
		if (len > 2) {
			res.h |= (AStringLibs::from_hex_num(text[2]) << 4);
		}  
		if (len > 1) {
			res.l |= AStringLibs::from_hex_num(text[1]);
		}  
		if (len > 0) {
			res.l |= (AStringLibs::from_hex_num(text[0]) << 4);
		}
	}
	return res;
}

AString A16Bit::to_text(A16Bit _pack) {
	AString res = "0000";
	res[0] = AStringLibs::to_hex_num((_pack.l >> 4) & 0xF);
	res[1] = AStringLibs::to_hex_num((_pack.l     ) & 0xF);
	res[2] = AStringLibs::to_hex_num((_pack.h >> 4) & 0xF);
	res[3] = AStringLibs::to_hex_num((_pack.h     ) & 0xF);
	return res;
}

AString A16Bit::to_text(u16int _pack) {
	return to_text(pack(_pack));
}