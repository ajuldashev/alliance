#include "A16Bit.hpp"

#ifdef DEBUG_A16BIT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a16bit() {
    A16Bit pack;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A16BIT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a16bit());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) unpack-8");
            AIO::writeln(" 3) unpack-16");
            AIO::writeln(" 4) unpack-p16");
            AIO::writeln(" 5) pack-8");
            AIO::writeln(" 6) pack-16");
            AIO::writeln(" 7) pack-p16");
            AIO::writeln(" 8) from_text");
            AIO::writeln(" 9) to_text-pack");
            AIO::writeln("10) to_text");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a16bit(com)) {
			return true;
		}
        if (com == "pack-8") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            pack = A16Bit::unpack(p.l, p.h);
        }
        if (com == "pack-16") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            u16int num = A16Bit::pack(p);
            pack = A16Bit::unpack(num);
        }
        if (com == "pack-p16") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            pack = A16Bit::unpack(p);
        }
        if (com == "unpack-8") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            pack = A16Bit::unpack(A16Bit::pack(p.l, p.h));
        }
        if (com == "unpack-16") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            u16int num = A16Bit::pack(p);
            pack = A16Bit::unpack(A16Bit::pack(num));
        }
        if (com == "unpack-p16") {
            AString text = AIO::get_word();
            A16Bit p = A16Bit::from_text(text);
            pack = A16Bit::unpack(A16Bit::pack(p));
        }
        if (com == "from_text") {
            AString text = AIO::get_word();
            pack = A16Bit::from_text(text);
        }
        if (com == "to_text-pack") {
            AIO::writeln(A16Bit::to_text(pack));
        }
        if (com == "to_text") {
            u16int num = A16Bit::pack(pack);
            AIO::writeln(A16Bit::to_text(num));
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_A16BIT );
    AIO::write_div_line();
    return false;
}
#endif

