#include "A16Bit.exception.hpp"

A16BitException::A16BitException() : AllianceException() {
	exception = "Alliance-Exception-A16Bit";
}

A16BitException::A16BitException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A16Bit");
}

A16BitException::A16BitException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A16Bit");
}

A16BitException::A16BitException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A16Bit");
}

A16BitException::A16BitException(const A16BitException & obj) : AllianceException(obj) {
	
}

A16BitException::~A16BitException() {

}

