#ifndef HPP_A16BITEXCEPTION
#define HPP_A16BITEXCEPTION
#include <alliance.exception.hpp>

class A16BitException : public AllianceException {
public:
	A16BitException();
	A16BitException(std::string text);
	A16BitException(AllianceException & e);
	A16BitException(AllianceException & e, std::string text);
	A16BitException(const A16BitException & obj);
	~A16BitException();
};

#endif

