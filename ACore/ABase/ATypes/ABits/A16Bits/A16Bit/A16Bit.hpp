#ifndef HPP_A16BIT
#define HPP_A16BIT
#include <alliance.config.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include "A16Bit.exception.hpp"

struct A16Bit {
    u8int l;
    u8int h;
    static A16Bit unpack(u8int, u8int);
    static A16Bit unpack(u16int);
    static A16Bit unpack(A16Bit);
    static u16int pack(u8int, u8int);
    static u16int pack(u16int);
    static u16int pack(A16Bit);
    static bool test(AString);
    static A16Bit from_text(AString);
    static AString to_text(A16Bit);
    static AString to_text(u16int);
};

#endif
  