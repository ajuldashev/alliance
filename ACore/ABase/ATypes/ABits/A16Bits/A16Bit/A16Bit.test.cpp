#include "A16Bit.hpp"

#ifdef TEST_A16BIT
#include <ACore/ABase/AIO/AIO.hpp>
bool test_a16bit(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_A16BIT );

   if (!test_node_a16bit(is_soft)) {
      return is_soft;
   }

   {
      AIO::writeln_warning("Test pack(u8int, u8int)");
      AIO::writeln_warning("Test pack(0x1, 0x2)");
      A16Bit p1 = A16Bit::unpack(0x1, 0x2);
      u16int t1 = 0x0201;
      if (t1 != A16Bit::pack(p1)) {
         AIO::writeln(A16Bit::pack(p1));
         AIO::writeln(t1);
         return is_soft;
      }
      AIO::writeln_warning("Test pack(0xF7, 0x15)");
      A16Bit p2 = A16Bit::unpack(0x15, 0xF7);
      u16int t2 = 0xF715;
      if (t2 != A16Bit::pack(p2)) {
         AIO::writeln(A16Bit::pack(p2));
         AIO::writeln(t2);
         return is_soft;
      }  
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test pack(u16int)");
      A16Bit p1 = A16Bit::unpack(0xF713);
      u16int t1 = 0xF713;
      if (t1 != A16Bit::pack(p1)) {
         AIO::writeln(A16Bit::pack(p1));
         AIO::writeln(t1);
         return is_soft;
      }
      
      AIO::writeln_success("Done");
   }

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_A16BIT );
   return true;
}
#endif
