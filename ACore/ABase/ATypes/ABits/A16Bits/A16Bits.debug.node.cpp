#include "A16Bits.hpp"

#ifdef DEBUG_A16BITS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_a16bits(AString com) {
	#ifdef DEBUG_A16BIT
	if (com == "a16bit") {
		if (debug_a16bit()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A16BITSWEAK
	if (com == "a16bitsweak") {
		if (debug_a16bitsweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_a16bits() {
	AString text;
	#ifdef DEBUG_A16BIT
	text += "d) a16bit\n";
	#endif
	#ifdef DEBUG_A16BITSWEAK
	text += "d) a16bitsweak\n";
	#endif
	return text;
}

#endif
