#include "A16Bits.exception.hpp"

A16BitsException::A16BitsException() : AllianceException() {
	exception = "Alliance-Exception-A16Bits";
}

A16BitsException::A16BitsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A16Bits");
}

A16BitsException::A16BitsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A16Bits");
}

A16BitsException::A16BitsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A16Bits");
}

A16BitsException::A16BitsException(const A16BitsException & obj) : AllianceException(obj) {
	
}

A16BitsException::~A16BitsException() {

}

