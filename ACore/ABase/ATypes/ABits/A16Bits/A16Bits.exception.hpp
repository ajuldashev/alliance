#ifndef HPP_A16BITSEXCEPTION
#define HPP_A16BITSEXCEPTION
#include <alliance.exception.hpp>

class A16BitsException : public AllianceException {
public:
	A16BitsException();
	A16BitsException(std::string text);
	A16BitsException(AllianceException & e);
	A16BitsException(AllianceException & e, std::string text);
	A16BitsException(const A16BitsException & obj);
	~A16BitsException();
};

#endif

