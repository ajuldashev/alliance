#include "A16Bits.hpp"

#ifdef TEST_A16BITS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_a16bits(bool is_soft) {
	#ifdef TEST_A16BIT
	if (!test_a16bit(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A16BIT );
		return is_soft;
	}
	#endif
	#ifdef TEST_A16BITSWEAK
	if (!test_a16bitsweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A16BITSWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
