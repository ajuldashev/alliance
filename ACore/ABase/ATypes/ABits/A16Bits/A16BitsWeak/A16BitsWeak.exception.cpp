#include "A16BitsWeak.exception.hpp"

A16BitsWeakException::A16BitsWeakException() : AllianceException() {
	exception = "Alliance-Exception-A16BitsWeak";
}

A16BitsWeakException::A16BitsWeakException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A16BitsWeak");
}

A16BitsWeakException::A16BitsWeakException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A16BitsWeak");
}

A16BitsWeakException::A16BitsWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A16BitsWeak");
}

A16BitsWeakException::A16BitsWeakException(const A16BitsWeakException & obj) : AllianceException(obj) {
	
}

A16BitsWeakException::~A16BitsWeakException() {

}

