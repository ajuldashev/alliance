#ifndef HPP_A16BITSWEAKEXCEPTION
#define HPP_A16BITSWEAKEXCEPTION
#include <alliance.exception.hpp>

class A16BitsWeakException : public AllianceException {
public:
	A16BitsWeakException();
	A16BitsWeakException(std::string text);
	A16BitsWeakException(AllianceException & e);
	A16BitsWeakException(AllianceException & e, std::string text);
	A16BitsWeakException(const A16BitsWeakException & obj);
	~A16BitsWeakException();
};

#endif

