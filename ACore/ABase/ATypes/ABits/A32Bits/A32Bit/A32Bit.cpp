#include "A32Bit.hpp"
#include "A32Bit.test.cpp"
#include "A32Bit.test.node.cpp"
#include "A32Bit.debug.cpp"
#include "A32Bit.debug.node.cpp"
#include "A32Bit.exception.cpp"

A32Bit A32Bit::unpack(u8int ll, u8int lh, u8int hl, u8int hh) {
	A32Bit res;
	res.l = A16Bit::unpack(ll, lh);
	res.h = A16Bit::unpack(hl, hh);
	return res;
}

A32Bit A32Bit::unpack(u16int l, u16int h) {
	A32Bit res;
	res.l = A16Bit::unpack(l);
	res.h = A16Bit::unpack(h);
	return res;
}

A32Bit A32Bit::unpack(u32int dword) {
	A32Bit res;
	res.l.l = (dword      ) & 0xFF;
	res.l.h = (dword >> 8 ) & 0xFF;
	res.h.l = (dword >> 16) & 0xFF;
	res.h.h = (dword >> 24) & 0xFF;
	return res;
}

A32Bit A32Bit::unpack(A16Bit l, A16Bit h) {
	A32Bit res;
	res.l = l;
	res.h = h;
	return res;
}

A32Bit A32Bit::unpack(A32Bit _pack) {
	return _pack;
}


u32int A32Bit::pack(u8int ll, u8int lh, u8int hl, u8int hh) {
	u32int res = 0;
	res = res | hh;
	res = res << 8;
	res = res | hl;
	res = res << 8;
	res = res | lh;
	res = res << 8;
	res = res | ll;
	return res;
}

u32int A32Bit::pack(u16int l, u16int h) {
	u32int res = 0;
	res = res | h;
	res = res << 16;
	res = res | l;
	return res;
}

u32int A32Bit::pack(u32int _unpack) {
	return _unpack;
}

u32int A32Bit::pack(A16Bit l, A16Bit h) {
	u32int res = 0;
	res = res | h.h;
	res = res << 8;
	res = res | h.l;
	res = res << 8;
	res = res | l.h;
	res = res << 8;
	res = res | l.l;
	return res;
}

u32int A32Bit::pack(A32Bit _pack) {
	u32int res = 0;
	res = res | _pack.h.h;
	res = res << 8;
	res = res | _pack.h.l;
	res = res << 8;
	res = res | _pack.l.h;
	res = res << 8;
	res = res | _pack.l.l;
	return res;
}

bool A32Bit::test(AString str) {
	u64int len = str.length();
	if (len == 0) {
		return false;
	}
	if (len % 2 != 0) {
		return false;
	}
	for (int i = 0; i < len && i < 8; i += 1) {
		if (!((str[i] >= '0' && str[i] <= '9')
			|| (str[i] >= 'a' && str[i] <= 'f')
			|| (str[i] >= 'A' && str[i] <= 'F')) 
		) {
			return false;
		}
	}
	return true;
}

A32Bit A32Bit::from_text(AString text) {
	A32Bit res;
	if (test(text)) {
		u64int len = text.length();
		res.l.l = 0;
		res.l.h = 0;
		res.h.l = 0;
		res.h.h = 0;
		if (len > 7) {
			res.h.h |= AStringLibs::from_hex_num(text[7]);
		}
		if (len > 6) {
			res.h.h |= (AStringLibs::from_hex_num(text[6]) << 4);
		}
		if (len > 5) {
			res.h.l |= AStringLibs::from_hex_num(text[5]);
		}
		if (len > 4) {
			res.h.l |= (AStringLibs::from_hex_num(text[4]) << 4);
		}
		if (len > 3) {
			res.l.h |= AStringLibs::from_hex_num(text[3]);
		} 
		if (len > 2) {
			res.l.h |= (AStringLibs::from_hex_num(text[2]) << 4);
		}  
		if (len > 1) {
			res.l.l |= AStringLibs::from_hex_num(text[1]);
		}  
		if (len > 0) {
			res.l.l |= (AStringLibs::from_hex_num(text[0]) << 4);
		}
	}
	return res;
}

AString A32Bit::to_text(A32Bit _pack) {
	AString res = "00000000";
	res[0] = AStringLibs::to_hex_num((_pack.l.l >> 4) & 0xF);
	res[1] = AStringLibs::to_hex_num((_pack.l.l     ) & 0xF);
	res[2] = AStringLibs::to_hex_num((_pack.l.h >> 4) & 0xF);
	res[3] = AStringLibs::to_hex_num((_pack.l.h     ) & 0xF);
	res[4] = AStringLibs::to_hex_num((_pack.h.l >> 4) & 0xF);
	res[5] = AStringLibs::to_hex_num((_pack.h.l     ) & 0xF);
	res[6] = AStringLibs::to_hex_num((_pack.h.h >> 4) & 0xF);
	res[7] = AStringLibs::to_hex_num((_pack.h.h     ) & 0xF);
	return res;
}

AString A32Bit::to_text(u32int _pack) {
	return to_text(pack(_pack));
}