#include "A32Bit.hpp"

#ifdef DEBUG_A32BIT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a32bit() {
    A32Bit pack;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A32BIT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a32bit());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) unpack-8");
            AIO::writeln(" 3) unpack-16");
            AIO::writeln(" 4) unpack-32");
            AIO::writeln(" 5) unpack-p16");
            AIO::writeln(" 6) unpack-p32");
            AIO::writeln(" 7) pack-8");
            AIO::writeln(" 8) pack-16");
            AIO::writeln(" 9) pack-32");
            AIO::writeln("10) pack-p16");
            AIO::writeln("11) pack-p32");
            AIO::writeln("12) from_text");
            AIO::writeln("13) to_text-pack");
            AIO::writeln("14) to_text");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a32bit(com)) {
			return true;
		}
        if (com == "pack-8") {
            AString text = AIO::get_word();
            A32Bit p = A32Bit::from_text(text);
            pack = A32Bit::unpack(p.l.l, p.l.h, p.h.l, p.h.h);
        }
        if (com == "pack-16") {
            A16Bit l = A16Bit::from_text(AIO::get_word());
            u16int num_l = A16Bit::pack(l);
            A16Bit h = A16Bit::from_text(AIO::get_word());
            u16int num_h = A16Bit::pack(h);
            pack = A32Bit::unpack(num_l, num_h);
        }
        if (com == "pack-32") {
            AString text = AIO::get_word();
            A32Bit p = A32Bit::from_text(text);
            u32int num = A32Bit::pack(p);
            pack = A32Bit::unpack(num);
        }
        if (com == "pack-p16") {
            A16Bit l = A16Bit::from_text(AIO::get_word());
            A16Bit h = A16Bit::from_text(AIO::get_word());
            pack = A32Bit::unpack(l, h);
        }
        if (com == "pack-p32") {
            A32Bit p = A32Bit::from_text(AIO::get_word());
            pack = A32Bit::unpack(p);
        }
        if (com == "unpack-8") {
            AString text = AIO::get_word();
            A32Bit p = A32Bit::from_text(text);
            pack = A32Bit::unpack(A32Bit::pack(p.l.l, p.l.h, p.h.l, p.h.h));
        }
        if (com == "unpack-16") {
            A16Bit l = A16Bit::from_text(AIO::get_word());
            u16int num_l = A16Bit::pack(l);
            A16Bit h = A16Bit::from_text(AIO::get_word());
            u16int num_h = A16Bit::pack(h);
            pack = A32Bit::unpack(A32Bit::pack(num_l, num_h));
        }
        if (com == "unpack-32") {
            AString text = AIO::get_word();
            A32Bit p = A32Bit::from_text(text);
            u32int num = A32Bit::pack(p);
            pack = A32Bit::unpack(A32Bit::pack(num));
        }
        if (com == "unpack-p16") {
            A16Bit l = A16Bit::from_text(AIO::get_word());
            A16Bit h = A16Bit::from_text(AIO::get_word());
            pack = A32Bit::unpack(A32Bit::pack(l, h));
        }
        if (com == "unpack-p32") {
            A32Bit p = A32Bit::from_text(AIO::get_word());
            pack = A32Bit::unpack(A32Bit::pack(p));
        }
        if (com == "from_text") {
            AString text = AIO::get_word();
            pack = A32Bit::from_text(text);
        }
        if (com == "to_text-pack") {
            AIO::writeln(A32Bit::to_text(pack));
        }
        if (com == "to_text") {
            u32int num = A32Bit::pack(pack);
            AIO::writeln(A32Bit::to_text(num));
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_A32BIT );
    AIO::write_div_line();
    return false;
}
#endif

