#include "A32Bit.exception.hpp"

A32BitException::A32BitException() : AllianceException() {
	exception = "Alliance-Exception-A32Bit";
}

A32BitException::A32BitException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A32Bit");
}

A32BitException::A32BitException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A32Bit");
}

A32BitException::A32BitException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A32Bit");
}

A32BitException::A32BitException(const A32BitException & obj) : AllianceException(obj) {
	
}

A32BitException::~A32BitException() {

}

