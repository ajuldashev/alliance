#ifndef HPP_A8BITSEXCEPTION
#define HPP_A8BITSEXCEPTION
#include <alliance.exception.hpp>

class A32BitException : public AllianceException {
public:
	A32BitException();
	A32BitException(std::string text);
	A32BitException(AllianceException & e);
	A32BitException(AllianceException & e, std::string text);
	A32BitException(const A32BitException & obj);
	~A32BitException();
};

#endif

