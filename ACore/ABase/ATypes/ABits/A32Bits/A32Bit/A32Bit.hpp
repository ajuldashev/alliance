#ifndef HPP_A8BITSPACK32
#define HPP_A8BITSPACK32
#include <ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.hpp>
#include "A32Bit.exception.hpp"

struct A32Bit {
    A16Bit l;
    A16Bit h;
    static A32Bit unpack(u8int, u8int, u8int, u8int);
    static A32Bit unpack(u16int, u16int);
    static A32Bit unpack(u32int);
    static A32Bit unpack(A16Bit, A16Bit);
    static A32Bit unpack(A32Bit);
    static u32int pack(u8int, u8int, u8int, u8int);
    static u32int pack(u16int, u16int);
    static u32int pack(u32int);
    static u32int pack(A16Bit, A16Bit);
    static u32int pack(A32Bit);
    static bool test(AString);
    static A32Bit from_text(AString);
    static AString to_text(A32Bit);
    static AString to_text(u32int);
};

#endif
