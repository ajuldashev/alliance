#include "A32Bits.hpp"
#include "A32Bits.test.cpp"
#include "A32Bits.test.node.cpp"
#include "A32Bits.debug.cpp"
#include "A32Bits.debug.node.cpp"
#include "A32Bits.exception.cpp"
#include <ACore/ABase/AIO/AIO.hpp>

A32Bits::A32Bits() {
	mode = A32BitsMode::variable;
	offset = 0;
	memory.insert(0, 0);
}

A32Bits::A32Bits(A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	memory.insert(0, 0);
}

A32Bits::A32Bits(u8int num) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u8int num, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u16int num) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u16int num, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u32int num) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u32int num, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u64int num) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(u64int num, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(num);
}

A32Bits::A32Bits(const A32Bits & mem) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(mem);
}

A32Bits::A32Bits(const A32Bits & mem, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(mem);
}

A32Bits::A32Bits(AString str) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(str);
}

A32Bits::A32Bits(AString str, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(str);
}

A32Bits::A32Bits(const char * cstr) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(cstr);
}

A32Bits::A32Bits(const char * cstr, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(cstr);
}

A32Bits::A32Bits(const char * cstr, u64int size) {
	mode = A32BitsMode::variable;
	offset = 0;
	set(cstr, size);
}

A32Bits::A32Bits(const char * cstr, u64int size, A32BitsMode _mode) {
	mode = _mode;
	offset = 0;
	set(cstr, size);
}

A32Bits::A32Bits(u64int size, u8int byte) {
	mode = A32BitsMode::variable;
	offset = 0;
	AArray<u8int> new_memory = alloc(size, byte).memory;
	memory = new_memory;
}

A32Bits::A32Bits(u64int size, u8int byte, A32BitsMode _mode) {
	mode = A32BitsMode::variable;
	offset = 0;
	AArray<u8int> new_memory = alloc(size, byte).memory;
	memory = new_memory;
}

void A32Bits::set_mode(A32BitsMode _mode) {
	mode = _mode;
}

A32BitsMode A32Bits::get_mode() {
	return mode;
}

void A32Bits::set_offset(u64int _offset){
	offset = _offset;
}

u64int A32Bits::get_offset(){
	return offset;
}

void A32Bits::set(u8int num) {
	memory.clear();
	memory.insert(num, 0);
}

void A32Bits::set(u16int num) {
	memory.clear();
	u8int byte_1 = num & 0xff;
	num = num >> 8;
	u8int byte_2 = num & 0xff;
	memory.insert(byte_2, 1);
	memory.insert(byte_1, 2);
}

void A32Bits::set(u32int num) {
	memory.clear();
	u8int byte_1 = num & 0xff;
	num = num >> 8;
	u8int byte_2 = num & 0xff;
	num = num >> 8;
	u8int byte_3 = num & 0xff;
	num = num >> 8;
	u8int byte_4 = num & 0xff;
	memory.insert(byte_4, 1);
	memory.insert(byte_3, 2);
	memory.insert(byte_2, 3);
	memory.insert(byte_1, 4);
}

void A32Bits::set(u64int num) {
	memory.clear();
	u8int byte_1 = num & 0xff;
	num = num >> 8;
	u8int byte_2 = num & 0xff;
	num = num >> 8;
	u8int byte_3 = num & 0xff;
	num = num >> 8;
	u8int byte_4 = num & 0xff;
	num = num >> 8;
	u8int byte_5 = num & 0xff;
	num = num >> 8;
	u8int byte_6 = num & 0xff;
	num = num >> 8;
	u8int byte_7 = num & 0xff;
	num = num >> 8;
	u8int byte_8 = num & 0xff;
	memory.insert(byte_8, 1);
	memory.insert(byte_7, 2);
	memory.insert(byte_6, 3);
	memory.insert(byte_5, 4);
	memory.insert(byte_4, 5);
	memory.insert(byte_3, 6);
	memory.insert(byte_2, 7);
	memory.insert(byte_1, 8);
}

void A32Bits::set(const A32Bits & mem) {
	memory.clear();
	for (u32int i = 0; i < mem.memory.length(); i += 1) {
		memory.push_end(mem.memory[i]);
	}
}

void A32Bits::set(AString str) {
	memory.clear();
	u64int len = str.length();
	for (u64int i = 0; i < len; i += 1) {
		memory.push_end(str[i]);
	}
}

void A32Bits::set(const char * str) {
	AString res(str);
	set(res);
}

void A32Bits::set(const char * cstr, u64int size) {
	AArray<u8int> new_memory(size);
	memory = new_memory;
	for (u64int i = 0; i < size; i += 1) {
		memory.push_end(cstr[i]);
	}
}

A32Bits A32Bits::alloc(u64int size, u8int byte = 0) {
	A32Bits res;
	AArray<u8int> new_memory(size);
	res.memory = new_memory;
	for (u64int i = 0; i < res.size(); i += 1) {
		res.memory.push_end(byte);
	}
	return res;
}


bool A32Bits::from_text(AString str) {
	if (!test_memory(str)) {
		return false;
	}
	memory.clear();
	u64int len = str.length();
	for (u64int i = 0; i < len/2; i += 1) {
		char ah = str[i*2];
		char al = str[i*2 + 1];
		u8int byte_h = AStringLibs::from_hex_num(ah);
		u8int byte_l = AStringLibs::from_hex_num(al);
		u8int byte = (byte_h << 4) + byte_l;
		memory.push_end(byte);
	}
	return true;
}

bool A32Bits::from_text(const char * str) {
	AString res(str);
	return from_text(res);
}

u8int A32Bits::to_u8int() {
	if (A32BitsMode::variable == mode) {
		to_size_block(sizeof(u8int));
		return memory.at(memory.length() - 1);
	}
	if (A32BitsMode::array == mode) {
		if (offset*sizeof(u8int) < memory.length()) {
			to_size_block(sizeof(u8int));
			return memory.at(offset);
		}
	}
	if (A32BitsMode::foreach == mode) {
		to_size_block(sizeof(u8int));
		return memory.at(0);
	}
	return 0;
}

u16int A32Bits::to_u16int() {
	if (A32BitsMode::variable == mode) {
		to_size_block(sizeof(u16int));
		return A16Bit::pack(memory.at(memory.length() - 2), 
										memory.at(memory.length() - 1)
										);
	}
	if (A32BitsMode::array == mode) {
		if (offset*sizeof(u16int) < memory.length()) {
			to_size_block(sizeof(u16int));
			return A16Bit::pack(memory.at(offset), 
											memory.at(offset + 1)
											);
		}
	}
	if (A32BitsMode::foreach == mode) {
		to_size_block(sizeof(u16int));
		return A16Bit::pack(memory.at(0), memory.at(1));
	}
	return 0;
}

u32int A32Bits::to_u32int() {
	if (A32BitsMode::variable == mode) {
		to_size_block(sizeof(u32int));
		return A32Bit::pack(memory.at(memory.length() - 4),
										memory.at(memory.length() - 3),
										memory.at(memory.length() - 2), 
										memory.at(memory.length() - 1)
										);
	}
	if (A32BitsMode::array == mode) {
		if (offset*sizeof(u32int) < memory.length()) {
			to_size_block(sizeof(u32int));
			return A32Bit::pack(memory.at(offset), 
											memory.at(offset + 1), 
											memory.at(offset + 2), 
											memory.at(offset + 3)
											);
		}
	}
	if (A32BitsMode::foreach == mode) {
		to_size_block(sizeof(u32int));
		return A32Bit::pack(memory.at(0), 
										memory.at(1),
										memory.at(2),
										memory.at(3)
										);
	}
	return 0;
}

u64int A32Bits::to_u64int() {
	if (A32BitsMode::variable == mode) {
		to_size_block(sizeof(u64int));
		return A64Bit::pack(memory.at(memory.length() - 8),
										memory.at(memory.length() - 7),
										memory.at(memory.length() - 6),
										memory.at(memory.length() - 5),
										memory.at(memory.length() - 4),
										memory.at(memory.length() - 3),
										memory.at(memory.length() - 2), 
										memory.at(memory.length() - 1)
										);
	}
	if (A32BitsMode::array == mode) {
		if (offset*sizeof(u64int) < memory.length()) {
			to_size_block(sizeof(u64int));
			return A64Bit::pack(memory.at(offset), 
											memory.at(offset + 1), 
											memory.at(offset + 2),
											memory.at(offset + 3),
											memory.at(offset + 4),
											memory.at(offset + 5),
											memory.at(offset + 6), 
											memory.at(offset + 7)
											);
		}
	}
	if (A32BitsMode::foreach == mode) {
		to_size_block(sizeof(u64int));
		return A64Bit::pack(memory.at(0), 
										memory.at(1),
										memory.at(2),
										memory.at(3),
										memory.at(4),
										memory.at(5),
										memory.at(6),
										memory.at(7)
										);
	}
	return 0;
}

AString A32Bits::to_string() {
	AString res;
	for (u64int i = 0; i < memory.length() && memory[i]; i += 1) {
		res = res + AString(memory[i]);
	}
	return res;
}

void A32Bits::to_size_block(u64int _size, u8int byte) {
	u64int _mod_size = length() % _size;
	for (u64int i = 0; i < (_size - _mod_size) && _mod_size != 0; i += 1) {
		push_end(byte);
	}
}

void A32Bits::to_use() {

}

AString A32Bits::to_text() {
	AString res;
	for (int i = 0; i < memory.length(); i += 1) {
		u8int byte = memory[i];
		char ah = ((byte & 0xf0) >> 4);
		char al =  (byte & 0x0f);
		res = res + 
			AString(AStringLibs::to_hex_num(ah)) + 
			AString(AStringLibs::to_hex_num(al));
	}
	return res;
}

u64int A32Bits::size() {
	return 0;
}

u64int A32Bits::length() {
	return memory.length();
}

void A32Bits::add(A32Bits mem) {
	for (u64int i = 0; i < mem.length(); i += 1) {
		push_end(mem[i]);
	}
}

void A32Bits::add(u8int num) {
	A32Bits mem(num);
	add(mem);
}

void A32Bits::add(u16int num) {
	A32Bits mem(num);
	add(mem);
}

void A32Bits::add(u32int num) {
	A32Bits mem(num);
	add(mem);
}

void A32Bits::add(u64int num) {
	A32Bits mem(num);
	add(mem);
}

void A32Bits::add(AString str) {
	A32Bits mem(str);
	add(mem);
}

void A32Bits::add(const char * cstr) {
	A32Bits mem(cstr);
	add(mem);
}

A32Bits A32Bits::sub(u64int pos, u64int len) {
	A32Bits res;
	//res.memory = memory.sub(pos, len);
	return res;
}
    
A32Bits A32Bits::get(u64int pos, u64int len) {
	A32Bits res = alloc(len);
	for (u64int i = pos; (i < (pos + len + 1)) && (i < length()); i += 1) {
		res[i - pos] = memory[i];
	}
	return res;
}

void A32Bits::insert(A32Bits mem, u64int pos) {

}

void A32Bits::insert(u8int num, u64int pos) {
	memory.insert(num, pos);
}

void A32Bits::insert(u16int num, u64int pos) {
	A32Bits mem(num);
	insert(mem, pos);
}

void A32Bits::insert(u32int num, u64int pos) {
	A32Bits mem(num);
	insert(mem, pos);
}

void A32Bits::insert(u64int num, u64int pos) {
	A32Bits mem(num);
	insert(mem, pos);
}

void A32Bits::insert(AString str, u64int pos) {
	A32Bits mem(str);
	insert(mem, pos);
}

void A32Bits::insert(const char * str, u64int pos) {
	A32Bits mem(str);
	insert(mem, pos);
}

u8int & A32Bits::operator[](u64int pos) {
	return memory[pos];
}

A32Bits A32Bits::operator+(A32Bits mem) {
	A32Bits res(*this);
	for (u64int i = 0; i < mem.length(); i += 1) {
		res.push_end(mem[i]);
	}
	return res;
}

A32Bits A32Bits::operator+(u8int num) {
	A32Bits res(*this);
	A32Bits mem(num);
	return res + mem;
}

A32Bits A32Bits::operator+(u16int num) {
	A32Bits res(*this);
	A32Bits mem(num);
	return res + mem;
}

A32Bits A32Bits::operator+(u32int num) {
	A32Bits res(*this);
	A32Bits mem(num);
	return res + mem;
}

A32Bits A32Bits::operator+(u64int num) {
	A32Bits res(*this);
	A32Bits mem(num);
	return res + mem;
}

A32Bits A32Bits::operator+(AString str) {
	A32Bits res(*this);
	A32Bits mem(str);
	return res + mem;
}

A32Bits A32Bits::operator+(const char * cstr) {
	A32Bits res(*this);
	A32Bits mem(cstr);
	return res + mem;
}

A32Bits & A32Bits::operator=(A32Bits mem) {
	memory.clear();
	set(mem);
	return *this;
}

A32Bits & A32Bits::operator=(u8int num) {
	memory.clear();
	set(num);
	return *this;
}

A32Bits & A32Bits::operator=(u16int num) {
	memory.clear();
	set(num);
	return *this;
}

A32Bits & A32Bits::operator=(u32int num) {
	memory.clear();
	set(num);
	return *this;
}

A32Bits & A32Bits::operator=(u64int num) {
	memory.clear();
	set(num);
	return *this;
}

A32Bits & A32Bits::operator=(AString str) {
	memory.clear();
	set(str);
	return *this;
}

A32Bits & A32Bits::operator=(const char * cstr) {
	memory.clear();
	set(cstr);
	return *this;
}

A32Bits A32Bits::operator|(A32Bits mem) {
	u64int len = length();
	u64int mem_len = mem.length();
	A32Bits res;
	if (len >= mem_len) {
		res.set(mem);
		for (u64int i = 0; i < len - mem_len; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < len; i += 1) {
			res[i] = res[i] | at(i); 
		}
	} else {
		res.set(*this);
		for (u64int i = 0; i < mem_len - len ; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < mem_len; i += 1) {
			res[i] = res[i] | mem[i]; 
		}
	}
	return res;
}

A32Bits A32Bits::operator|(u8int num) {
	A32Bits res(num);
	return *this | res;
}

A32Bits A32Bits::operator|(u16int num) {
	A32Bits res(num);
	return *this | res;
}

A32Bits A32Bits::operator|(u32int num) {
	A32Bits res(num);
	return *this | res;
}

A32Bits A32Bits::operator|(u64int num) {
	A32Bits res(num);
	return *this | res;
}

A32Bits A32Bits::operator|(AString str) {
	A32Bits res(str);
	return *this | res;
}

A32Bits A32Bits::operator|(const char * cstr) {
	A32Bits res(cstr);
	return *this | res;
}

A32Bits A32Bits::operator&(A32Bits mem) {
	u64int len = length();
	u64int mem_len = mem.length();
	A32Bits res;
	if (len >= mem_len) {
		res.set(mem);
		for (u64int i = 0; i < len - mem_len; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < len; i += 1) {
			res[i] = res[i] & at(i); 
		}
	} else {
		res.set(*this);
		for (u64int i = 0; i < mem_len - len ; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < mem_len; i += 1) {
			res[i] = res[i] & mem[i]; 
		}
	}
	return res;
}

A32Bits A32Bits::operator&(u8int num) {
	A32Bits res(num);
	return *this & res;
}

A32Bits A32Bits::operator&(u16int num) {
	A32Bits res(num);
	return *this & res;
}

A32Bits A32Bits::operator&(u32int num) {
	A32Bits res(num);
	return *this & res;
}

A32Bits A32Bits::operator&(u64int num) {
	A32Bits res(num);
	return *this & res;
}

A32Bits A32Bits::operator&(AString str) {
	A32Bits res(str);
	return *this & res;
}

A32Bits A32Bits::operator&(const char * cstr) {
	A32Bits res(cstr);
	return *this & res;
}

A32Bits A32Bits::operator^(A32Bits mem) {
	u64int len = length();
	u64int mem_len = mem.length();
	A32Bits res;
	if (len >= mem_len) {
		res.set(mem);
		for (u64int i = 0; i < len - mem_len; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < len; i += 1) {
			res[i] = res[i] ^ at(i); 
		}
	} else {
		res.set(*this);
		for (u64int i = 0; i < mem_len - len ; i += 1) {
			u8int byte = 0;
			res.push_beg(byte);
		}
		for (u64int i = 0; i < mem_len; i += 1) {
			res[i] = res[i] ^ mem[i]; 
		}
	}
	return res;
}

A32Bits A32Bits::operator^(u8int num) {
	A32Bits res(num);
	return *this ^ res;
}

A32Bits A32Bits::operator^(u16int num) {
	A32Bits res(num);
	return *this ^ res;
}

A32Bits A32Bits::operator^(u32int num) {
	A32Bits res(num);
	return *this ^ res;
}

A32Bits A32Bits::operator^(u64int num) {
	A32Bits res(num);
	return *this ^ res;
}

A32Bits A32Bits::operator^(AString str) {
	A32Bits res(str);
	return *this ^ res;
}

A32Bits A32Bits::operator^(const char * cstr) {
	A32Bits res(cstr);
	return *this ^ res;
}

A32Bits A32Bits::operator~() {
	A32Bits res(*this);
	for (u64int i = 0; i < memory.length(); i += 1) {
		res[i] = ~memory[i];
	}
	return res;
}

A32Bits A32Bits::operator>>(u64int offset) {
	u8int  offset_bit  = offset%8;
	u64int offset_byte = offset/8;
	A32Bits res(*this);
	for (u64int i = 0; i < offset_byte; i += 1) {
		u8int byte = 0;
		res.push_beg(byte);
		res.pop_end();
	}
	if (res.length() > 0 && offset_bit != 0) {
		u8int mask = ((0x1 << (offset_bit)) - 1);
		u8int prev = 0;
		for (u64int i = 0; i < res.length(); i += 1) {
			res[i] = (res[i] >> offset_bit) | (prev << (7 - offset_bit));
			prev = res[i] & mask;
		}
	}
	return res;
}

A32Bits A32Bits::operator<<(u64int offset) {
	u8int  offset_bit  = offset%8;
	u64int offset_byte = offset/8;
	A32Bits res(*this);
	for (u64int i = 0; i < offset_byte; i += 1) {
		u8int byte = 0;
		res.push_end(byte);
		res.pop_beg();
	}
	if (res.length() > 0 && offset_bit != 0) {
		u8int mask = ~((0x1 << (8 - offset_bit)) - 1);
		u8int prev = 0;
		for (s64int i = res.length() - 1; i >= 0; i -= 1) {
			res[i] = (res[i] << (offset_bit)) | (prev >> (7 - offset_bit));
			prev = res[i] & mask;
		}
	}
	return res;
}

bool A32Bits::test_memory(AString str) {
	u64int len = str.length();
	if (len == 0) {
		return false;
	}
	if (len % 2 != 0) {
		return false;
	}
	for (int i = 0; i < len; i += 1) {
		if (!((str[i] >= '0' && str[i] <= '9')
			|| (str[i] >= 'a' && str[i] <= 'f')
			|| (str[i] >= 'A' && str[i] <= 'F')) 
		) {
			return false;
		}
	}
	return true;
}

bool A32Bits::bit_set(u64int bit) {
	u64int n_byte = bit/8;
	if (n_byte >= memory.length()) {
		return false;
	}
	u8int offset = bit%8;
	u8int byte = memory[n_byte];
	byte = byte | (0x1 << (offset));
	memory[n_byte] = byte;
	return true;
}

bool A32Bits::bit_test(u64int bit) {
	u64int n_byte = bit/8;
	if (n_byte >= memory.length()) {
		return false;
	}
	u8int offset = bit%8;
	u8int byte = memory[n_byte];
	byte = byte & (0x1 << (offset));
	return byte;
}

bool A32Bits::bit_clear(u64int bit) {
	u64int n_byte = bit/8;
	if (n_byte >= memory.length()) {
		return false;
	}
	u8int offset = bit%8;
	u8int byte = memory[n_byte];
	byte = byte & ~(0x1 << (offset));
	memory[n_byte] = byte;
	return true;
}

void A32Bits::push_beg(u8int byte) {
	memory.push_beg(byte);
} 

void A32Bits::push_end(u8int byte) {
	memory.push_end(byte);
}

u8int & A32Bits::at(u64int n_byte) {
	return memory.at(n_byte);
}

u8int A32Bits::get(u64int n_byte) {
	return memory.get(n_byte);
}

u8int A32Bits::pop_beg() {
	return memory.pop_beg();
}

u8int A32Bits::pop_end() {
	return memory.pop_end();
}

void A32Bits::clear() {
	memory.clear();
}

A32Bits::~A32Bits() {

}