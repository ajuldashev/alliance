#include "A32Bits.hpp"

#ifdef DEBUG_A32BITS
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a32bits() {
    A32Bits mem;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A32BITS );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a32bits());
            AIO::writeln(" 1) help");
            AIO::writeln(" 5) status");
            AIO::writeln(" 6) to_use");
            AIO::writeln(" 7) set_8");
            AIO::writeln(" 8) set_16");
            AIO::writeln(" 9) set_32");
            AIO::writeln(" 0) set_64");
            AIO::writeln("10) set_mem");
            AIO::writeln("11) set_str");
            AIO::writeln("12) set_cstr");
            AIO::writeln("13) set_cstr_size");
            AIO::writeln("14) from_str");
            AIO::writeln("15) from_cstr");
            AIO::writeln("16) bit_set");
            AIO::writeln("17) bit_test");
            AIO::writeln("18) bit_clear");
            AIO::writeln("19) +");
            AIO::writeln("20) insert");
            AIO::writeln("21) push_beg");
            AIO::writeln("22) push_end");
            AIO::writeln("23) at");
            AIO::writeln("24) get");
            AIO::writeln("25) pop_beg");
            AIO::writeln("26) pop_end");
            AIO::writeln("27) ~");
            AIO::writeln("28) >>");
            AIO::writeln("29) <<");
            AIO::writeln("30) |");
            AIO::writeln("31) &");
            AIO::writeln("32) ^");
            AIO::writeln("33) alloc");
            // AIO::writeln("34) to_8");
            // AIO::writeln("35) to_16");
            // AIO::writeln("36) to_32");
            // AIO::writeln("37) to_64");
            AIO::writeln("38) to_string");
            AIO::writeln("39) to_size_block");
            AIO::writeln("40) set_mode");
            AIO::writeln("41) get_mode");
            AIO::writeln("42) set_offset");
            AIO::writeln("43) get_offset");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a32bits(com)) {
			return true;
		}
        if (com == "status") {
            AIO::write("Size        : ");
            AIO::writeln(mem.size());
            AIO::write("Length      : ");
            AIO::writeln(mem.length());
            AIO::write("Memory byte : ");
            AIO::writeln(mem.to_text());	
        }
        if (com == "set_8") {
            u8int byte = AIO::get_u8int();
            mem.set(byte);
        }
        if (com == "set_16") {
            u16int byte = AIO::get_u16int();
            mem.set(byte);
        }
        if (com == "set_32") {
            u32int byte = AIO::get_u32int();
            mem.set(byte);
        }
        if (com == "set_64") {
            u64int byte = AIO::get_u64int();
            mem.set(byte);
        }
        if (com == "set_str") {
            AString word = AIO::get_word();
            mem.set(word);
        }
        if (com == "set_cstr") {
            AString word = AIO::get_word();
            mem.set(word.to_cstring().const_data());
        }
        if (com == "set_cstr_size") {
            char test[] = {1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0};
            mem.set((char *)test, 16);
        }
        if (com == "from_str") {
            AString word = AIO::get_word();
            mem.from_text(word);
        }
        if (com == "from_cstr") {
            AString word = AIO::get_word();
            mem.from_text(word.to_cstring().const_data());
        }
        if (com == "to_use") {
            mem.to_use();
        }
        if (com == "bit_set") {
            u64int bit = AIO::get_u64int();
            AIO::writeln(mem.bit_set(bit));
        }
        if (com == "bit_test") {
            u64int bit = AIO::get_u64int();
            AIO::writeln(mem.bit_test(bit));
        }
        if (com == "bit_clear") {
            u64int bit = AIO::get_u64int();
            AIO::writeln(mem.bit_clear(bit));
        }
        if (com == "+") {
            AString word = AIO::get_word();
            A32Bits two_mem;
            two_mem.from_text(word);
            mem = mem + two_mem;
        }
        if (com == "insert") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            u64int n_byte = AIO::get_u64int();
            mem.insert(res[0], n_byte);
        }
        if (com == "push_beg") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            mem.push_beg(res[0]);
        }
        if (com == "push_end") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            mem.push_end(res[0]);
        }
        if (com == "at") {
            u64int n_byte = AIO::get_u64int();
            A32Bits res(mem.at(n_byte));
            AIO::writeln(res.to_text());
        }
        if (com == "get") {
            u64int n_byte = AIO::get_u64int();
            A32Bits res(mem.get(n_byte));
            AIO::writeln(res.to_text());
        }
        if (com == "pop_beg") {
            A32Bits res(mem.pop_beg());
            AIO::writeln(res.to_text());
        }
        if (com == "pop_end") {
            A32Bits res(mem.pop_end());
            AIO::writeln(res.to_text());
        }
        if (com == "~") {
            mem = ~mem;
        }
        if (com == ">>") {
            u64int n_bit = AIO::get_u64int();
            mem = mem >> n_bit;
        }
        if (com == "<<") {
            u64int n_bit = AIO::get_u64int();
            mem = mem << n_bit;
        }
        if (com == "|") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            mem = mem | res;
        }
        if (com == "&") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            mem = mem & res;
        }
        if (com == "^") {
            AString byte = AIO::get_word();
            A32Bits res;
            res.from_text(byte);
            mem = mem ^ res;
        }
        if (com == "alloc") {
            u64int size = AIO::get_u64int();
            u8int byte = AIO::get_char();
            mem = A32Bits::alloc(size, byte);
        }
        // if (com == "to_8") {
        //     u8int byte = mem.to_u8int();
        //     AIO::write("Number : ");
        //     AIO::writeln(byte);
        // }
        // if (com == "to_16") {
        //     u16int byte = mem.to_u16int();
        //     AIO::write("Number : ");
        //     AIO::writeln(byte);
        // }
        // if (com == "to_32") {
        //     u32int byte = mem.to_u32int();
        //     AIO::write("Number : ");
        //     AIO::writeln(byte);
        // }
        // if (com == "to_64") {
        //     u64int byte = mem.to_u64int();
        //     AIO::write("Number : ");
        //     AIO::writeln(byte);
        // }
        if (com == "to_string") {
            AString res = mem.to_string();
            AIO::writeln(AString("Res : ") + res);
        }
        if (com == "to_size_block") {
            AIO::write("Write size block : ");
            u64int size = AIO::get_u64int();
            mem.to_size_block(size);
        }
        if (com == "set_mode") {
            AIO::write("Mode : variable, array, foreach -> ");
            AString mode = AIO::get_word();
            if (mode == "variable") {
                mem.set_mode(A32BitsMode::variable);
            }
            if (mode == "array") {
                mem.set_mode(A32BitsMode::array);
            }
            if (mode == "foreach") {
                mem.set_mode(A32BitsMode::foreach);
            }
        }
        if (com == "get_mode") {
            A32BitsMode mode = mem.get_mode();
            if (mode == A32BitsMode::variable) {
                AIO::writeln("Mode : variable");
            }
            if (mode == A32BitsMode::array) {
                AIO::writeln("Mode : array");
            }
            if (mode == A32BitsMode::foreach) {
                AIO::writeln("Mode : foreach");
            }
        }
        if (com == "set_offset") {
            u64int offset = AIO::get_u64int();
            mem.set_offset(offset);
        }
        if (com == "get_offset") {
            AIO::write("Offset : ");
            AIO::writeln(mem.get_offset());
        }
        AIO::write_div_line();
com = AIO::get_command();
   }
   AIO::writeln("End debug the module " MODULE_DEBUG_A32BITS );
   AIO::write_div_line();
   return false;
}
#endif

