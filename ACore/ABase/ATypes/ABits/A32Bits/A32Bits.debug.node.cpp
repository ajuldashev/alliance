#include "A32Bits.hpp"

#ifdef DEBUG_A32BITS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_a32bits(AString com) {
	#ifdef DEBUG_A32BIT
	if (com == "a32bit") {
		if (debug_a32bit()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A32BITSWEAK
	if (com == "a32bitsweak") {
		if (debug_a32bitsweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_a32bits() {
	AString text;
	#ifdef DEBUG_A32BIT
	text += "d) a32bit\n";
	#endif
	#ifdef DEBUG_A32BITSWEAK
	text += "d) a32bitsweak\n";
	#endif
	return text;
}

#endif
