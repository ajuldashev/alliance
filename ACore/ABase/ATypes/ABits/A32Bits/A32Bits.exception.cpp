#include "A32Bits.exception.hpp"

A32BitsException::A32BitsException() : AllianceException() {
	exception = "Alliance-Exception-A32Bits";
}

A32BitsException::A32BitsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A32Bits");
}

A32BitsException::A32BitsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A32Bits");
}

A32BitsException::A32BitsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A32Bits");
}

A32BitsException::A32BitsException(const A32BitsException & obj) : AllianceException(obj) {
	
}

A32BitsException::~A32BitsException() {

}

