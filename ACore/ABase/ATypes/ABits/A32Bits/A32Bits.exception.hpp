#ifndef HPP_A32BITSEXCEPTION
#define HPP_A32BITSEXCEPTION
#include <alliance.exception.hpp>

class A32BitsException : public AllianceException {
public:
	A32BitsException();
	A32BitsException(std::string text);
	A32BitsException(AllianceException & e);
	A32BitsException(AllianceException & e, std::string text);
	A32BitsException(const A32BitsException & obj);
	~A32BitsException();
};

#endif

