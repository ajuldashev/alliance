#ifndef HPP_A32BITS
#define HPP_A32BITS
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AStruct/AArray/AArray.hpp>
#include <ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.hpp>
#include "A32Bits.exception.hpp"

enum class A32BitsMode {
    variable,
    array,
    foreach
};

class A32Bits {
private:
    mutable AArray<u8int> memory;
    A32BitsMode mode;
    u64int offset;
public:
    A32Bits();
    A32Bits(A32BitsMode);
    A32Bits(u8int);
    A32Bits(u8int, A32BitsMode);
    A32Bits(u16int);
    A32Bits(u16int, A32BitsMode);
    A32Bits(u32int);
    A32Bits(u32int, A32BitsMode);
    A32Bits(u64int);
    A32Bits(u64int, A32BitsMode);
    A32Bits(const A32Bits &);
    A32Bits(const A32Bits &, A32BitsMode);
    A32Bits(AString);
    A32Bits(AString, A32BitsMode);
    A32Bits(const char *);
    A32Bits(const char *, A32BitsMode);
    A32Bits(const char *, u64int);
    A32Bits(const char *, u64int, A32BitsMode);
    A32Bits(u64int, u8int);
    A32Bits(u64int, u8int, A32BitsMode);

    void set_mode(A32BitsMode);
    A32BitsMode get_mode();
    void   set_offset(u64int);
    u64int get_offset();

    void set(u8int);
    void set(u16int);
    void set(u32int);
    void set(u64int);
    void set(const A32Bits &);
    void set(AString);
    void set(const char *);
    void set(const char *, u64int);

    static A32Bits alloc(u64int, u8int);

    void to_use();
    AString to_text();
    bool from_text(AString);
    bool from_text(const char *);

    u8int  to_u8int ();
    u16int to_u16int();
    u32int to_u32int();
    u64int to_u64int();
    AString to_string();
    
    void to_size_block(u64int, u8int byte = 0x00);

    u64int size();
    u64int length();

    void add(A32Bits);
    void add(u8int);
    void add(u16int);
    void add(u32int);
    void add(u64int);
    void add(AString);
    void add(const char *);

    A32Bits sub(u64int, u64int);
    A32Bits get(u64int, u64int);

    void insert(A32Bits, u64int);
    void insert(u8int, u64int);
    void insert(u16int, u64int);
    void insert(u32int, u64int);
    void insert(u64int, u64int);
    void insert(AString, u64int);
    void insert(const char *, u64int);

    void logical_or (A32Bits);
    void logical_or (u8int);
    void logical_or (u16int);
    void logical_or (u32int);
    void logical_or (u64int);
    void logical_or (AString);
    void logical_or (const char *);

    void logical_xor(A32Bits);
    void logical_xor(u8int);
    void logical_xor(u16int);
    void logical_xor(u32int);
    void logical_xor(u64int);
    void logical_xor(AString);
    void logical_xor(const char *);

    void logical_and(A32Bits);
    void logical_and(u8int);
    void logical_and(u16int);
    void logical_and(u32int);
    void logical_and(u64int);
    void logical_and(AString);
    void logical_and(const char *);

    void logical_not(A32Bits);
    void logical_not(u8int);
    void logical_not(u16int);
    void logical_not(u32int);
    void logical_not(u64int);
    void logical_not(AString);
    void logical_not(const char *);

    void logical_shr(A32Bits);
    void logical_shr(u8int);
    void logical_shr(u16int);
    void logical_shr(u32int);
    void logical_shr(u64int);
    void logical_shr(AString);
    void logical_shr(const char *);

    void logical_shl(A32Bits);
    void logical_shl(u8int);
    void logical_shl(u16int);
    void logical_shl(u32int);
    void logical_shl(u64int);
    void logical_shl(AString);
    void logical_shl(const char *);

    u8int & operator[](u64int);
    
    A32Bits operator+(A32Bits);
    A32Bits operator+(u8int);
    A32Bits operator+(u16int);
    A32Bits operator+(u32int);
    A32Bits operator+(u64int);
    A32Bits operator+(AString);
    A32Bits operator+(const char *);

    A32Bits & operator=(A32Bits);
    A32Bits & operator=(u8int);
    A32Bits & operator=(u16int);
    A32Bits & operator=(u32int);
    A32Bits & operator=(u64int);
    A32Bits & operator=(AString);
    A32Bits & operator=(const char *);

    A32Bits operator|(A32Bits);
    A32Bits operator|(u8int);
    A32Bits operator|(u16int);
    A32Bits operator|(u32int);
    A32Bits operator|(u64int);
    A32Bits operator|(AString);
    A32Bits operator|(const char *);

    A32Bits operator&(A32Bits);
    A32Bits operator&(u8int);
    A32Bits operator&(u16int);
    A32Bits operator&(u32int);
    A32Bits operator&(u64int);
    A32Bits operator&(AString);
    A32Bits operator&(const char *);

    A32Bits operator^(A32Bits);
    A32Bits operator^(u8int);
    A32Bits operator^(u16int);
    A32Bits operator^(u32int);
    A32Bits operator^(u64int);
    A32Bits operator^(AString);
    A32Bits operator^(const char *);

    A32Bits operator~();
    
    A32Bits operator>>(u64int);
    A32Bits operator<<(u64int);
    

    bool test_memory(AString);

    bool bit_set(u64int);
    bool bit_test(u64int);
    bool bit_clear(u64int);

	void push_beg(u8int); 
	void push_end(u8int);

	u8int & at(u64int);
	u8int get(u64int);

	u8int pop_beg();
	u8int pop_end();

    void clear();
    
    ~A32Bits();
};

#endif
