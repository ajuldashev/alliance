#include "A32Bits.hpp"

#ifdef TEST_A32BITS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_a32bits(bool is_soft) {
	#ifdef TEST_A32BIT
	if (!test_a32bit(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A32BIT );
		return is_soft;
	}
	#endif
	#ifdef TEST_A32BITSWEAK
	if (!test_a32bitsweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A32BITSWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
