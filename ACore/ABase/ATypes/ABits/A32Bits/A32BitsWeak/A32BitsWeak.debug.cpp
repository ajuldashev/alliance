#include "A32BitsWeak.hpp"

#ifdef DEBUG_A32BITSWEAK
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a32bitsweak() {
    A32BitsWeak pack;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A32BITSWEAK );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a32bitsweak());
            AIO::writeln("1) help");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a32bitsweak(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_A32BITSWEAK );
    AIO::write_div_line();
    return false;
}
#endif

