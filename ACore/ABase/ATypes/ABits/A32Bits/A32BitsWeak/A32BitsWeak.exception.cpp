#include "A32BitsWeak.exception.hpp"

A32BitsWeakException::A32BitsWeakException() : AllianceException() {
	exception = "Alliance-Exception-A32BitsWeak";
}

A32BitsWeakException::A32BitsWeakException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A32BitsWeak");
}

A32BitsWeakException::A32BitsWeakException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A32BitsWeak");
}

A32BitsWeakException::A32BitsWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A32BitsWeak");
}

A32BitsWeakException::A32BitsWeakException(const A32BitsWeakException & obj) : AllianceException(obj) {
	
}

A32BitsWeakException::~A32BitsWeakException() {

}

