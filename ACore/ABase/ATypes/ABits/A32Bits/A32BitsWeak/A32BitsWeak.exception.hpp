#ifndef HPP_A32BITSWEAKEXCEPTION
#define HPP_A32BITSWEAKEXCEPTION
#include <alliance.exception.hpp>

class A32BitsWeakException : public AllianceException {
public:
	A32BitsWeakException();
	A32BitsWeakException(std::string text);
	A32BitsWeakException(AllianceException & e);
	A32BitsWeakException(AllianceException & e, std::string text);
	A32BitsWeakException(const A32BitsWeakException & obj);
	~A32BitsWeakException();
};

#endif

