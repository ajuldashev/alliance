#include "A64Bit.hpp"
#include "A64Bit.test.cpp"
#include "A64Bit.test.node.cpp"
#include "A64Bit.debug.cpp"
#include "A64Bit.debug.node.cpp"
#include "A64Bit.exception.cpp"

A64Bit A64Bit::unpack(u8int lll, 
								u8int llh, 
								u8int lhl, 
								u8int lhh, 
								u8int hll, 
								u8int hlh, 
								u8int hhl, 
								u8int hhh
) {
	A64Bit res;
	res.l = A32Bit::unpack(lll, llh, lhl, lhh);
	res.h = A32Bit::unpack(hll, hlh, hhl, hhh);
	return res;
}

A64Bit A64Bit::unpack(u16int ll, u16int lh, u16int hl, u16int hh) {
	A64Bit res;
	res.l = A32Bit::unpack(ll, lh);
	res.h = A32Bit::unpack(hl, hh);
	return res;
}

A64Bit A64Bit::unpack(u32int l, u32int h) {
	A64Bit res;
	res.l = A32Bit::unpack(l);
	res.h = A32Bit::unpack(h);
	return res;
}

A64Bit A64Bit::unpack(u64int qword) {
	A64Bit res;
	res.l.l.l = (qword      ) & 0xFF;
	res.l.l.h = (qword >> 8 ) & 0xFF;
	res.l.h.l = (qword >> 16) & 0xFF;
	res.l.h.h = (qword >> 24) & 0xFF;
	res.h.l.l = (qword >> 32) & 0xFF;
	res.h.l.h = (qword >> 40) & 0xFF;
	res.h.h.l = (qword >> 48) & 0xFF;
	res.h.h.h = (qword >> 56) & 0xFF;
	return res;
}

A64Bit A64Bit::unpack(A16Bit ll, A16Bit lh, A16Bit hl, A16Bit hh) {
	A64Bit res;
	res.l = A32Bit::unpack(ll, lh);
	res.h = A32Bit::unpack(hl, hh);
	return res;
}

A64Bit A64Bit::unpack(A32Bit l, A32Bit h) {
	A64Bit res;
	res.l = l;
	res.h = h;
	return res;
}

A64Bit A64Bit::unpack(A64Bit _pack) {
	return _pack;
}

u64int A64Bit::pack(u8int lll, 
							u8int llh, 
							u8int lhl, 
							u8int lhh, 
							u8int hll, 
							u8int hlh, 
							u8int hhl, 
							u8int hhh
) {
	u64int res = 0;
	res = res | hhh;
	res = res << 8;
	res = res | hhl;
	res = res << 8;
	res = res | hlh;
	res = res << 8;
	res = res | hll;
	res = res << 8;
	res = res | lhh;
	res = res << 8;
	res = res | lhl;
	res = res << 8;
	res = res | llh;
	res = res << 8;
	res = res | lll;
	return res;
}

u64int A64Bit::pack(u16int ll, u16int lh, u16int hl, u16int hh) {
	u64int res = 0;
	res = res | hh;
	res = res << 16;
	res = res | hl;
	res = res << 16;
	res = res | lh;
	res = res << 16;
	res = res | ll;
	return res;
}

u64int A64Bit::pack(u32int l, u32int h) {
	u64int res = 0;
	res = res | h;
	res = res << 32;
	res = res | l;
	return res;
}

u64int A64Bit::pack(u64int _unpack) {
	return _unpack;
}

u64int A64Bit::pack(A16Bit ll, A16Bit lh, A16Bit hl, A16Bit hh) {
	u64int res = 0;
	res = res | hh.h;
	res = res << 8;
	res = res | hh.l;
	res = res << 8;
	res = res | hl.h;
	res = res << 8;
	res = res | hl.l;
	res = res << 8;
	res = res | lh.h;
	res = res << 8;
	res = res | lh.l;
	res = res << 8;
	res = res | ll.h;
	res = res << 8;
	res = res | ll.l;
	return res;
}

u64int A64Bit::pack(A32Bit l, A32Bit h) {
	u64int res = 0;
	res = res | h.h.h;
	res = res << 8;
	res = res | h.h.l;
	res = res << 8;
	res = res | h.l.h;
	res = res << 8;
	res = res | h.l.l;
	res = res << 8;
	res = res | l.h.h;
	res = res << 8;
	res = res | l.h.l;
	res = res << 8;
	res = res | l.l.h;
	res = res << 8;
	res = res | l.l.l;
	return res;
}

u64int A64Bit::pack(A64Bit _pack) {
	u64int res = 0;
	res = res | _pack.h.h.h;
	res = res << 8;
	res = res | _pack.h.h.l;
	res = res << 8;
	res = res | _pack.h.l.h;
	res = res << 8;
	res = res | _pack.h.l.l;
	res = res << 8;
	res = res | _pack.l.h.h;
	res = res << 8;
	res = res | _pack.l.h.l;
	res = res << 8;
	res = res | _pack.l.l.h;
	res = res << 8;
	res = res | _pack.l.l.l;
	return res;
}

bool A64Bit::test(AString str) {
	u64int len = str.length();
	if (len == 0) {
		return false;
	}
	if (len % 2 != 0) {
		return false;
	}
	for (int i = 0; i < len && i < 16; i += 1) {
		if (!((str[i] >= '0' && str[i] <= '9')
			|| (str[i] >= 'a' && str[i] <= 'f')
			|| (str[i] >= 'A' && str[i] <= 'F')) 
		) {
			return false;
		}
	}
	return true;
}

A64Bit A64Bit::from_text(AString text) {
	A64Bit res;
	if (test(text)) {
		u64int len = text.length();
		res.l.l.l = 0;
		res.l.l.h = 0;
		res.l.h.l = 0;
		res.l.h.h = 0;
		res.h.l.l = 0;
		res.h.l.h = 0;
		res.h.h.l = 0;
		res.h.h.h = 0;
		if (len > 15) {
			res.h.h.h |= AStringLibs::from_hex_num(text[15]);
		}
		if (len > 14) {
			res.h.h.h |= (AStringLibs::from_hex_num(text[14]) << 4);
		}
		if (len > 13) {
			res.h.h.l |= AStringLibs::from_hex_num(text[13]);
		}
		if (len > 12) {
			res.h.h.l |= (AStringLibs::from_hex_num(text[12]) << 4);
		}
		if (len > 11) {
			res.h.l.h |= AStringLibs::from_hex_num(text[11]);
		} 
		if (len > 10) {
			res.h.l.h |= (AStringLibs::from_hex_num(text[10]) << 4);
		}  
		if (len > 9) {
			res.h.l.l |= AStringLibs::from_hex_num(text[9]);
		}  
		if (len > 8) {
			res.h.l.l |= (AStringLibs::from_hex_num(text[8]) << 4);
		}
		if (len > 7) {
			res.l.h.h |= AStringLibs::from_hex_num(text[7]);
		}
		if (len > 6) {
			res.l.h.h |= (AStringLibs::from_hex_num(text[6]) << 4);
		}
		if (len > 5) {
			res.l.h.l |= AStringLibs::from_hex_num(text[5]);
		}
		if (len > 4) {
			res.l.h.l |= (AStringLibs::from_hex_num(text[4]) << 4);
		}
		if (len > 3) {
			res.l.l.h |= AStringLibs::from_hex_num(text[3]);
		} 
		if (len > 2) {
			res.l.l.h |= (AStringLibs::from_hex_num(text[2]) << 4);
		}  
		if (len > 1) {
			res.l.l.l |= AStringLibs::from_hex_num(text[1]);
		}  
		if (len > 0) {
			res.l.l.l |= (AStringLibs::from_hex_num(text[0]) << 4);
		}
	}
	return res;
}

AString A64Bit::to_text(A64Bit _pack) {
	AString res = "0000000000000000";
	res[ 0] = AStringLibs::to_hex_num((_pack.l.l.l >> 4) & 0xF);
	res[ 1] = AStringLibs::to_hex_num((_pack.l.l.l     ) & 0xF);
	res[ 2] = AStringLibs::to_hex_num((_pack.l.l.h >> 4) & 0xF);
	res[ 3] = AStringLibs::to_hex_num((_pack.l.l.h     ) & 0xF);
	res[ 4] = AStringLibs::to_hex_num((_pack.l.h.l >> 4) & 0xF);
	res[ 5] = AStringLibs::to_hex_num((_pack.l.h.l     ) & 0xF);
	res[ 6] = AStringLibs::to_hex_num((_pack.l.h.h >> 4) & 0xF);
	res[ 7] = AStringLibs::to_hex_num((_pack.l.h.h     ) & 0xF);
	res[ 8] = AStringLibs::to_hex_num((_pack.h.l.l >> 4) & 0xF);
	res[ 9] = AStringLibs::to_hex_num((_pack.h.l.l     ) & 0xF);
	res[10] = AStringLibs::to_hex_num((_pack.h.l.h >> 4) & 0xF);
	res[11] = AStringLibs::to_hex_num((_pack.h.l.h     ) & 0xF);
	res[12] = AStringLibs::to_hex_num((_pack.h.h.l >> 4) & 0xF);
	res[13] = AStringLibs::to_hex_num((_pack.h.h.l     ) & 0xF);
	res[14] = AStringLibs::to_hex_num((_pack.h.h.h >> 4) & 0xF);
	res[15] = AStringLibs::to_hex_num((_pack.h.h.h     ) & 0xF);
	return res;
}

AString A64Bit::to_text(u64int _pack) {
	return to_text(pack(_pack));
}