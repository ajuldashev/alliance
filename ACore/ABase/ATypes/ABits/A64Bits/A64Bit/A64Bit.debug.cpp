#include "A64Bit.hpp"

#ifdef DEBUG_A64BIT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a64bit() {
    A64Bit pack;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A64BIT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a64bit());
            AIO::writeln(" 1) help");
            AIO::writeln(" 2) unpack-8");
            AIO::writeln(" 3) unpack-16");
            AIO::writeln(" 4) unpack-32");
            AIO::writeln(" 5) unpack-64");
            AIO::writeln(" 6) unpack-p16");
            AIO::writeln(" 7) unpack-p32");
            AIO::writeln(" 8) unpack-p64");
            AIO::writeln(" 9) pack-8");
            AIO::writeln("10) pack-16");
            AIO::writeln("11) pack-32");
            AIO::writeln("12) pack-64");
            AIO::writeln("13) pack-p16");
            AIO::writeln("14) pack-p32");
            AIO::writeln("15) pack-p64");
            AIO::writeln("16) from_text");
            AIO::writeln("17) to_text-pack");
            AIO::writeln("18) to_text");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a64bit(com)) {
			return true;
		}
        if (com == "pack-8") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            pack = A64Bit::unpack(p.l.l.l, p.l.l.h, p.l.h.l, p.l.h.h, p.h.l.l, p.h.l.h, p.h.h.l, p.h.h.h);
        }
        if (com == "pack-16") {
            A16Bit ll = A16Bit::from_text(AIO::get_word());
            u16int num_ll = A16Bit::pack(ll);
            A16Bit lh = A16Bit::from_text(AIO::get_word());
            u16int num_lh = A16Bit::pack(lh);
            A16Bit hl = A16Bit::from_text(AIO::get_word());
            u16int num_hl = A16Bit::pack(hl);
            A16Bit hh = A16Bit::from_text(AIO::get_word());
            u16int num_hh = A16Bit::pack(hh);
            pack = A64Bit::unpack(num_ll, num_lh, num_hl, num_hh);
        }
        if (com == "pack-32") {
            A32Bit l = A32Bit::from_text(AIO::get_word());
            u32int num_l = A32Bit::pack(l);
            A32Bit h = A32Bit::from_text(AIO::get_word());
            u32int num_h = A32Bit::pack(h);
            pack = A64Bit::unpack(num_l, num_h);
        }
        if (com == "pack-64") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            u64int num = A64Bit::pack(p);
            pack = A64Bit::unpack(num);
        }
        if (com == "pack-p16") {
            A16Bit ll = A16Bit::from_text(AIO::get_word());
            A16Bit lh = A16Bit::from_text(AIO::get_word());
            A16Bit hl = A16Bit::from_text(AIO::get_word());
            A16Bit hh = A16Bit::from_text(AIO::get_word());
            pack = A64Bit::unpack(ll, lh, hl, hh);
        }
        if (com == "pack-p32") {
            A32Bit l = A32Bit::from_text(AIO::get_word());
            A32Bit h = A32Bit::from_text(AIO::get_word());
            pack = A64Bit::unpack(l, h);
        }
        if (com == "pack-p64") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            pack = A64Bit::unpack(p);
        }
        if (com == "unpack-8") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            pack = A64Bit::unpack(A64Bit::unpack(p.l.l.l, p.l.l.h, p.l.h.l, p.l.h.h, p.h.l.l, p.h.l.h, p.h.h.l, p.h.h.h));
        }
        if (com == "unpack-16") {
            A16Bit ll = A16Bit::from_text(AIO::get_word());
            u16int num_ll = A16Bit::pack(ll);
            A16Bit lh = A16Bit::from_text(AIO::get_word());
            u16int num_lh = A16Bit::pack(lh);
            A16Bit hl = A16Bit::from_text(AIO::get_word());
            u16int num_hl = A16Bit::pack(hl);
            A16Bit hh = A16Bit::from_text(AIO::get_word());
            u16int num_hh = A16Bit::pack(hh);
            pack = A64Bit::unpack(A64Bit::pack(num_ll, num_lh, num_hl, num_hh));
        }
        if (com == "unpack-32") {
            A32Bit l = A32Bit::from_text(AIO::get_word());
            u32int num_l = A32Bit::pack(l);
            A32Bit h = A32Bit::from_text(AIO::get_word());
            u32int num_h = A32Bit::pack(h);
            pack = A64Bit::unpack(A64Bit::pack(num_l, num_h));
        }
        if (com == "unpack-64") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            u64int num = A64Bit::pack(p);
            pack = A64Bit::unpack(A64Bit::pack(num));
        }
        if (com == "unpack-p16") {
            A16Bit ll = A16Bit::from_text(AIO::get_word());
            A16Bit lh = A16Bit::from_text(AIO::get_word());
            A16Bit hl = A16Bit::from_text(AIO::get_word());
            A16Bit hh = A16Bit::from_text(AIO::get_word());
            pack = A64Bit::unpack(A64Bit::pack(ll, lh, hl, hh));
        }
        if (com == "unpack-p32") {
            A32Bit l = A32Bit::from_text(AIO::get_word());
            A32Bit h = A32Bit::from_text(AIO::get_word());
            pack = A64Bit::unpack(A64Bit::pack(l, h));
        }
        if (com == "unpack-p64") {
            AString text = AIO::get_word();
            A64Bit p = A64Bit::from_text(text);
            pack = A64Bit::unpack(A64Bit::pack(p));
        }
        if (com == "from_text") {
            AString text = AIO::get_word();
            pack = A64Bit::from_text(text);
        }
        if (com == "to_text-pack") {
            AIO::writeln(A64Bit::to_text(pack));
        }
        if (com == "to_text") {
            u64int num = A64Bit::pack(pack);
            AIO::writeln(A64Bit::to_text(num));
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_A64BIT );
    AIO::write_div_line();
    return false;
}
#endif

