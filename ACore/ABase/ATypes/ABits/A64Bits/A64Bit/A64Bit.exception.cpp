#include "A64Bit.exception.hpp"

A64BitException::A64BitException() : AllianceException() {
	exception = "Alliance-Exception-A64Bit";
}

A64BitException::A64BitException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A64Bit");
}

A64BitException::A64BitException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A64Bit");
}

A64BitException::A64BitException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A64Bit");
}

A64BitException::A64BitException(const A64BitException & obj) : AllianceException(obj) {
	
}

A64BitException::~A64BitException() {

}

