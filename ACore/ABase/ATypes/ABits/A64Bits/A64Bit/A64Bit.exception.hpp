#ifndef HPP_A64BITEXCEPTION
#define HPP_A64BITEXCEPTION
#include <alliance.exception.hpp>

class A64BitException : public AllianceException {
public:
	A64BitException();
	A64BitException(std::string text);
	A64BitException(AllianceException & e);
	A64BitException(AllianceException & e, std::string text);
	A64BitException(const A64BitException & obj);
	~A64BitException();
};

#endif

