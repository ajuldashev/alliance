#ifndef HPP_A64BIT
#define HPP_A64BIT
#include <ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.hpp>
#include "A64Bit.exception.hpp"

struct A64Bit {
    A32Bit l;
    A32Bit h;
    static A64Bit unpack(u8int, u8int, u8int, u8int, u8int, u8int, u8int, u8int);
    static A64Bit unpack(u16int, u16int, u16int, u16int);
    static A64Bit unpack(u32int, u32int);
    static A64Bit unpack(u64int);
    static A64Bit unpack(A16Bit, A16Bit, A16Bit, A16Bit);
    static A64Bit unpack(A32Bit, A32Bit);
    static A64Bit unpack(A64Bit);
    static u64int pack(u8int, u8int, u8int, u8int, u8int, u8int, u8int, u8int);
    static u64int pack(u16int, u16int, u16int, u16int);
    static u64int pack(u32int, u32int);
    static u64int pack(u64int);
    static u64int pack(A16Bit, A16Bit, A16Bit, A16Bit);
    static u64int pack(A32Bit, A32Bit);
    static u64int pack(A64Bit);
    static bool test(AString);
    static A64Bit from_text(AString);
    static AString to_text(A64Bit);
    static AString to_text(u64int);
};

#endif
