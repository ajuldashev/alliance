#include "A64Bits.hpp"

#ifdef DEBUG_A64BITS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_a64bits(AString com) {
	#ifdef DEBUG_A64BIT
	if (com == "a64bit") {
		if (debug_a64bit()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A64BITSWEAK
	if (com == "a64bitsweak") {
		if (debug_a64bitsweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_a64bits() {
	AString text;
	#ifdef DEBUG_A64BIT
	text += "d) a64bit\n";
	#endif
	#ifdef DEBUG_A64BITSWEAK
	text += "d) a64bitsweak\n";
	#endif
	return text;
}

#endif
