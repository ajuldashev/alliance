#include "A64Bits.exception.hpp"

A64BitsException::A64BitsException() : AllianceException() {
	exception = "Alliance-Exception-A64Bits";
}

A64BitsException::A64BitsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A64Bits");
}

A64BitsException::A64BitsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A64Bits");
}

A64BitsException::A64BitsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A64Bits");
}

A64BitsException::A64BitsException(const A64BitsException & obj) : AllianceException(obj) {
	
}

A64BitsException::~A64BitsException() {

}

