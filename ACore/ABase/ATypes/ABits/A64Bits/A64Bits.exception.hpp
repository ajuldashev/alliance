#ifndef HPP_A64BITSEXCEPTION
#define HPP_A64BITSEXCEPTION
#include <alliance.exception.hpp>

class A64BitsException : public AllianceException {
public:
	A64BitsException();
	A64BitsException(std::string text);
	A64BitsException(AllianceException & e);
	A64BitsException(AllianceException & e, std::string text);
	A64BitsException(const A64BitsException & obj);
	~A64BitsException();
};

#endif

