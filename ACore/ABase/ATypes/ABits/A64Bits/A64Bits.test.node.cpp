#include "A64Bits.hpp"

#ifdef TEST_A64BITS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_a64bits(bool is_soft) {
	#ifdef TEST_A64BIT
	if (!test_a64bit(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A64BIT );
		return is_soft;
	}
	#endif
	#ifdef TEST_A64BITSWEAK
	if (!test_a64bitsweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A64BITSWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
