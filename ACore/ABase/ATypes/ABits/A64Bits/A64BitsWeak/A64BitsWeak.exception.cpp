#include "A64BitsWeak.exception.hpp"

A64BitsWeakException::A64BitsWeakException() : AllianceException() {
	exception = "Alliance-Exception-A64BitsWeak";
}

A64BitsWeakException::A64BitsWeakException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A64BitsWeak");
}

A64BitsWeakException::A64BitsWeakException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A64BitsWeak");
}

A64BitsWeakException::A64BitsWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A64BitsWeak");
}

A64BitsWeakException::A64BitsWeakException(const A64BitsWeakException & obj) : AllianceException(obj) {
	
}

A64BitsWeakException::~A64BitsWeakException() {

}

