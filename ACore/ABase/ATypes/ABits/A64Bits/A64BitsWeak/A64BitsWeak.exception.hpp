#ifndef HPP_A64BITSWEAKEXCEPTION
#define HPP_A64BITSWEAKEXCEPTION
#include <alliance.exception.hpp>

class A64BitsWeakException : public AllianceException {
public:
	A64BitsWeakException();
	A64BitsWeakException(std::string text);
	A64BitsWeakException(AllianceException & e);
	A64BitsWeakException(AllianceException & e, std::string text);
	A64BitsWeakException(const A64BitsWeakException & obj);
	~A64BitsWeakException();
};

#endif

