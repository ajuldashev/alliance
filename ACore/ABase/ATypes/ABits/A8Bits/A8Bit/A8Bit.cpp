#include "A8Bit.hpp"
#include "A8Bit.test.cpp"
#include "A8Bit.test.node.cpp"
#include "A8Bit.debug.cpp"
#include "A8Bit.debug.node.cpp"
#include "A8Bit.exception.cpp"


A8Bit A8Bit::unpack(u8int l, u8int h) {
	A8Bit res;
	res.l = (l) & 0xF;
	res.h = (h) & 0xF;
	return res;
}


A8Bit A8Bit::unpack(u8int byte) {
	A8Bit res;
	res.l = (byte     ) & 0xF;
	res.h = (byte >> 4) & 0xF;
	return res;
}

A8Bit A8Bit::unpack(A8Bit byte) {
	A8Bit res;
	res.l = byte.l;
	res.h = byte.h;
	return res;
}

u8int A8Bit::pack(u8int l, u8int h) {
	u8int byte = 0;
	byte |= h & 0xF;
	byte <<= 4;
	byte |= l & 0xF;
	return byte;
}

u8int A8Bit::pack(u8int byte) {
	return byte;
}

u8int A8Bit::pack(A8Bit byte) {
	u8int res = 0;
	res = res | byte.h & 0xFF;
	res = res << 4;
	res = res | byte.l & 0xFF;
	return res;
}

bool A8Bit::test(AString str) {
	u64int len = str.length();
	if (len == 0) {
		return false;
	}
	if (len % 2 != 0) {
		return false;
	}
	for (int i = 0; i < len && i < 2; i += 1) {
		if (!((str[i] >= '0' && str[i] <= '9')
			|| (str[i] >= 'a' && str[i] <= 'f')
			|| (str[i] >= 'A' && str[i] <= 'F')) 
		) {
			return false;
		}
	}
	return true;
}

A8Bit A8Bit::from_text(AString text) {
	A8Bit res;
	if (test(text)) {
		u64int len = text.length();
		res.h = 0;
		res.l = 0;
		if (len > 1) {
			res.h = AStringLibs::from_hex_num(text[0]);
		}  
		if (len > 0) {
			res.l = AStringLibs::from_hex_num(text[1]);
		}
	}
	return res;
}

AString A8Bit::to_text(A8Bit byte) {
	AString res = "00";
	res[0] = AStringLibs::to_hex_num((byte.h) & 0xF);
	res[1] = AStringLibs::to_hex_num((byte.l) & 0xF);
	return res;
}

AString A8Bit::to_text(u8int byte) {
	return to_text(unpack(byte));
}

void A8Bit::bit_set(u8int & byte, u8int bit) {
	byte = byte | (0x1 << bit);
}

bool A8Bit::bit_test(u8int & byte, u8int bit) {
	return byte & (0x1 << bit);
}

void A8Bit::bit_clear(u8int & byte, u8int bit) {
	byte = byte & ~(0x1 << bit);
}