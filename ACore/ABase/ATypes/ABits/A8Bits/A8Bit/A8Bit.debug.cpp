#include "A8Bit.hpp"

#ifdef DEBUG_A8BIT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_a8bit() {
    A8Bit pack;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_A8BIT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_a8bit());
            AIO::writeln(" 1) help");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_a8bit(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_A8BIT );
    AIO::write_div_line();
    return false;
}
#endif

