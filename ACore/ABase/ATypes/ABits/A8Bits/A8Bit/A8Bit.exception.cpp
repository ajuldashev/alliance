#include "A8Bit.exception.hpp"

A8BitException::A8BitException() : AllianceException() {
	exception = "Alliance-Exception-A8Bit";
}

A8BitException::A8BitException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A8Bit");
}

A8BitException::A8BitException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A8Bit");
}

A8BitException::A8BitException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A8Bit");
}

A8BitException::A8BitException(const A8BitException & obj) : AllianceException(obj) {
	
}

A8BitException::~A8BitException() {

}

