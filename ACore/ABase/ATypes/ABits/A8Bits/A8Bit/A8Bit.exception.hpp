#ifndef HPP_A8BITEXCEPTION
#define HPP_A8BITEXCEPTION
#include <alliance.exception.hpp>

class A8BitException : public AllianceException {
public:
	A8BitException();
	A8BitException(std::string text);
	A8BitException(AllianceException & e);
	A8BitException(AllianceException & e, std::string text);
	A8BitException(const A8BitException & obj);
	~A8BitException();
};

#endif

