#ifndef HPP_A8BIT
#define HPP_A8BIT
#include <alliance.config.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include "A8Bit.exception.hpp"

struct A8Bit {
    u8int l;
    u8int h;
    static A8Bit unpack(u8int l, u8int h);
    static A8Bit unpack(u8int byte);
    static A8Bit unpack(A8Bit byte);
    static u8int pack(u8int l, u8int h);
    static u8int pack(u8int byte);
    static u8int pack(A8Bit byte);   
    static bool test(AString text);
    static A8Bit from_text(AString text);
    static AString to_text(u8int byte);
    static AString to_text(A8Bit byte);
    static void bit_set(u8int & byte, u8int bit);
    static bool bit_test(u8int & byte, u8int bit);
    static void bit_clear(u8int & byte, u8int bit);
};

#endif
  