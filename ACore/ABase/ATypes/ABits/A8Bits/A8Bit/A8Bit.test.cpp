#include "A8Bit.hpp"

#ifdef TEST_A8BIT
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *  Test A8Bit unpack(u8int, u8int)
 */
TEST_FUNCTION_BEGIN(1)
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x00, 0x00)
       * step 1
       * step 2
       */
      A8Bit pack = A8Bit::unpack(0x00, 0x00);
      TEST_ALERT(pack.l == 0x00, 1)
      TEST_ALERT(pack.h == 0x00, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x01, 0x07)
       * step 3
       * step 4
       */
      A8Bit pack = A8Bit::unpack(0x01, 0x07);
      TEST_ALERT(pack.l == 0x01, 3)
      TEST_ALERT(pack.h == 0x07, 4)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x0F, 0x0A)
       * step 5
       * step 6
       */
      A8Bit pack = A8Bit::unpack(0x0F, 0x0A);
      TEST_ALERT(pack.l == 0x0F, 5)
      TEST_ALERT(pack.h == 0x0A, 6)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x0F, 0x0F)
       * step 7
       * step 8
       */
      A8Bit pack = A8Bit::unpack(0x0F, 0x0F);
      TEST_ALERT(pack.l == 0x0F, 7)
      TEST_ALERT(pack.h == 0x0F, 8)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test A8Bit unpack(u8int)
 */
TEST_FUNCTION_BEGIN(2)
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x00)
       * step 1
       * step 2
       */
      A8Bit pack = A8Bit::unpack(0x00);
      TEST_ALERT(pack.l == 0x00, 1)
      TEST_ALERT(pack.h == 0x00, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0x17)
       * step 3
       * step 4
       */
      A8Bit pack = A8Bit::unpack(0x17);
      TEST_ALERT(pack.l == 0x07, 3)
      TEST_ALERT(pack.h == 0x01, 4)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0xFA)
       * step 5
       * step 6
       */
      A8Bit pack = A8Bit::unpack(0xFA);
      TEST_ALERT(pack.l == 0x0A, 5)
      TEST_ALERT(pack.h == 0x0F, 6)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(0xFF)
       * step 7
       * step 8
       */
      A8Bit pack = A8Bit::unpack(0xFF);
      TEST_ALERT(pack.l == 0x0F, 7)
      TEST_ALERT(pack.h == 0x0F, 8)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test A8Bit unpack(A8Bit)
 */
TEST_FUNCTION_BEGIN(3)
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(unpack(0x00))
       * step 1
       * step 2
       */
      A8Bit pack = A8Bit::unpack(A8Bit::unpack(0x00));
      TEST_ALERT(pack.l == 0x00, 1)
      TEST_ALERT(pack.h == 0x00, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(unpack(0x17))
       * step 3
       * step 4
       */
      A8Bit pack = A8Bit::unpack(A8Bit::unpack(0x17));
      TEST_ALERT(pack.l == 0x07, 3)
      TEST_ALERT(pack.h == 0x01, 4)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(unpack(0xFA))
       * step 5
       * step 6
       */
      A8Bit pack = A8Bit::unpack(A8Bit::unpack(0xFA));
      TEST_ALERT(pack.l == 0x0A, 5)
      TEST_ALERT(pack.h == 0x0F, 6)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit unpack(unpack(0xFF))
       * step 7
       * step 8
       */
      A8Bit pack = A8Bit::unpack(A8Bit::unpack(0xFF));
      TEST_ALERT(pack.l == 0x0F, 7)
      TEST_ALERT(pack.h == 0x0F, 8)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test A8Bit pack(u8int, u8int)
 */
TEST_FUNCTION_BEGIN(4)
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x00, 0x00)
       * step 1
       */
      u8int pack = A8Bit::pack(0x00, 0x00);
      TEST_ALERT(pack == 0x00, 1)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x01, 0x07)
       * step 2
       */
      u8int pack = A8Bit::pack(0x01, 0x07);
      TEST_ALERT(pack == 0x71, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x0F, 0x0A)
       * step 3
       */
      u8int pack = A8Bit::pack(0x0F, 0x0A);
      TEST_ALERT(pack == 0xAF, 3)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x0F, 0x0F)
       * step 4
       */
      u8int pack = A8Bit::pack(0x0F, 0x0F);
      TEST_ALERT(pack == 0xFF, 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test A8Bit pack(0x00)
 */
TEST_FUNCTION_BEGIN(5)
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x00)
       * step 1
       */
      u8int pack = A8Bit::pack(0x00);
      TEST_ALERT(pack == 0x00, 1)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0x17)
       * step 2
       */
      u8int pack = A8Bit::pack(0x17);
      TEST_ALERT(pack == 0x17, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0xFA)
       * step 3
       */
      u8int pack = A8Bit::pack(0xFA);
      TEST_ALERT(pack == 0xFA, 3)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(0xFF)
       * step 4
       */
      u8int pack = A8Bit::pack(0xFF);
      TEST_ALERT(pack == 0xFF, 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test A8Bit pack(A8Bit)
 */
TEST_FUNCTION_BEGIN(6)
   {
      /** <doc-test-info> 
       * Test A8Bit pack(unpack(0x00))
       * step 1
       */
      u8int pack = A8Bit::pack(A8Bit::unpack(0x00));
      TEST_ALERT(pack == 0x00, 1)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(unpack(0x17))
       * step 2
       */
      u8int pack = A8Bit::pack(A8Bit::unpack(0x17));
      TEST_ALERT(pack == 0x17, 2)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(unpack(0xFA))
       * step 3
       */
      u8int pack = A8Bit::pack(A8Bit::unpack(0xFA));
      TEST_ALERT(pack == 0xFA, 3)
   }
   {
      /** <doc-test-info> 
       * Test A8Bit pack(unpack(0xFF))
       * step 4
       */
      u8int pack = A8Bit::pack(A8Bit::unpack(0xFF));
      TEST_ALERT(pack == 0xFF, 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test test(AString)
 */
TEST_FUNCTION_BEGIN(7)
   {
      /** <doc-test-info> 
       * Test test(\"\")
       * step 1
       */
      TEST_ALERT(!A8Bit::test(""), 1)
      /** <doc-test-info> 
       * Test test(\"00\")
       * step 2
       */
      TEST_ALERT(A8Bit::test("00"), 2)
      /** <doc-test-info> 
       * Test test(\"000\")
       * step 3
       */
      TEST_ALERT(!A8Bit::test("000"), 3)
      /** <doc-test-info> 
       * Test test(\"0123456789ABCDEF\")
       * step 4
       */
      TEST_ALERT(A8Bit::test("0123456789ABCDEF"), 4)
      /** <doc-test-info> 
       * Test test(\"0123456789ABCDEV\")
       * step 5
       */
      TEST_ALERT(A8Bit::test("0123456789ABCDEV"), 5)
      /** <doc-test-info> 
       * Test test(\"0V23456789ABCDEV\")
       * step 6
       */
      TEST_ALERT(!A8Bit::test("0V23456789ABCDEV"), 6)
      /** <doc-test-info> 
       * Test test(\"0123456789ABCDE\")
       * step 7
       */
      TEST_ALERT(!A8Bit::test("0123456789ABCDE"), 7)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test from_text(AString)
 */
TEST_FUNCTION_BEGIN(8)
   {
      /** <doc-test-info> 
       * Test pack(from_text(\"00\"))
       * step 1
       */
      TEST_ALERT(A8Bit::pack(A8Bit::from_text("00")) == 0x00, 1)
      /** <doc-test-info> 
       * Test pack(from_text(\"17\"))
       * step 2
       */
      TEST_ALERT(A8Bit::pack(A8Bit::from_text("17")) == 0x17, 2)
      /** <doc-test-info> 
       * Test pack(from_text(\"FA\"))
       * step 3
       */
      TEST_ALERT(A8Bit::pack(A8Bit::from_text("FA")) == 0xFA, 3)
      /** <doc-test-info> 
       * Test pack(from_text(\"FF\"))
       * step 4
       */
      TEST_ALERT(A8Bit::pack(A8Bit::from_text("FF")) == 0xFF, 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test to_text(u8int)
 */
TEST_FUNCTION_BEGIN(9)
   {
      /** <doc-test-info> 
       * Test to_text(0x00)
       * step 1
       */
      TEST_ALERT(A8Bit::to_text(0x00) == "00", 1)
      /** <doc-test-info> 
       * Test to_text(0x17)
       * step 2
       */
      TEST_ALERT(A8Bit::to_text(0x17) == "17", 2)
      /** <doc-test-info> 
       * Test to_text(0xFA)
       * step 3
       */
      TEST_ALERT(A8Bit::to_text(0xFA) == "FA", 3)
      /** <doc-test-info> 
       * Test to_text(0xFF)
       * step 4
       */
      TEST_ALERT(A8Bit::to_text(0xFF) == "FF", 4)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test bit_set(u8int & byte, u8int bit)
 */
TEST_FUNCTION_BEGIN(10)
   {
      u8int byte = 0;
      A8Bit::bit_set(byte, 0);
      /** <doc-test-info> 
       * Test bit_set(0, 0)
       * step 1
       */
      TEST_ALERT(byte == 0x1, 1)
      A8Bit::bit_set(byte, 0);
      /** <doc-test-info> 
       * Test bit_set(1, 0)
       * step 1
       */
      TEST_ALERT(byte == 0x1, 2)
      A8Bit::bit_set(byte, 3);
      /** <doc-test-info> 
       * Test bit_set(1, 3)
       * step 1
       */
      TEST_ALERT(byte == 0x9, 3)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test bit_test(u8int & byte, u8int bit)
 */
TEST_FUNCTION_BEGIN(11)
   {
      u8int byte = 0xF0;
      /** <doc-test-info> 
       * Test bit_test(0xF0, 0)
       * step 1
       */
      TEST_ALERT(!A8Bit::bit_test(byte, 0), 1)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 1)
       * step 2
       */
      TEST_ALERT(!A8Bit::bit_test(byte, 1), 2)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 2)
       * step 3
       */
      TEST_ALERT(!A8Bit::bit_test(byte, 2), 3)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 3)
       * step 4
       */
      TEST_ALERT(!A8Bit::bit_test(byte, 3), 4)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 4)
       * step 5
       */
      TEST_ALERT(A8Bit::bit_test(byte, 4), 5)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 5)
       * step 6
       */
      TEST_ALERT(A8Bit::bit_test(byte, 5), 6)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 6)
       * step 7
       */
      TEST_ALERT(A8Bit::bit_test(byte, 6), 7)
      /** <doc-test-info> 
       * Test bit_test(0xF0, 7)
       * step 8
       */
      TEST_ALERT(A8Bit::bit_test(byte, 7), 8)
   }
TEST_FUNCTION_END

/** <doc-test-static-function>
 *  Test bit_clear(u8int & byte, u8int bit)
 */
TEST_FUNCTION_BEGIN(12)
   {
      u8int byte = 0xFF;
      /** <doc-test-info> 
       * Test bit_clear(0xFF, 7)
       * step 1
       */
      A8Bit::bit_clear(byte, 7);
      TEST_ALERT(byte == 0x7F, 1)
      /** <doc-test-info> 
       * Test bit_clear(0x7F, 6)
       * step 2
       */
      A8Bit::bit_clear(byte, 6);
      TEST_ALERT(byte == 0x3F, 2)
      /** <doc-test-info> 
       * Test bit_clear(0x3F, 5)
       * step 3
       */
      A8Bit::bit_clear(byte, 5);
      TEST_ALERT(byte == 0x1F, 3)
      /** <doc-test-info> 
       * Test bit_clear(0x1F, 4)
       * step 4
       */
      A8Bit::bit_clear(byte, 4);
      TEST_ALERT(byte == 0x0F, 4)
      /** <doc-test-info> 
       * Test bit_clear(0x0F, 3)
       * step 5
       */
      A8Bit::bit_clear(byte, 3);
      TEST_ALERT(byte == 0x07, 5)
      /** <doc-test-info> 
       * Test bit_clear(0x07, 2)
       * step 6
       */
      A8Bit::bit_clear(byte, 2);
      TEST_ALERT(byte == 0x03, 6)
      /** <doc-test-info> 
       * Test bit_clear(0x03, 1)
       * step 7
       */
      A8Bit::bit_clear(byte, 1);
      TEST_ALERT(byte == 0x01, 7)
      /** <doc-test-info> 
       * Test bit_clear(0x01, 0)
       * step 8
       */
      A8Bit::bit_clear(byte, 0);
      TEST_ALERT(byte == 0x00, 8)
   }
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(a8bit)
   
   TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)
   TEST_CALL(7)
   TEST_CALL(8)
   TEST_CALL(9)
   TEST_CALL(10)
   TEST_CALL(11)

TEST_FUNCTION_MAIN_END(A8BIT)

#endif