#include "A8Bits.hpp"
#include <ACore/ABase/AIO/AIO.hpp>
#include "A8Bits.test.cpp"
#include "A8Bits.test.node.cpp"
#include "A8Bits.debug.cpp"
#include "A8Bits.debug.node.cpp"
#include "A8Bits.exception.cpp"

A8Bits::A8Bits() {

}

A8Bits::A8Bits(u8int byte) {
	set(byte);
}

A8Bits::A8Bits(AString string) {
	set(string);
}

A8Bits::A8Bits(u64int len, u8int num) {
	set(len, num);
}

A8Bits::A8Bits(const char * string, u64int len) {
	set(string, len);
}

A8Bits::A8Bits(const A8Bits & bits) {
	set(bits);
}

void A8Bits::set(u8int num) {
	bytes = APtrArray<u8int>::make(1);
	bytes[0] = num;
}

void A8Bits::set(AString string) {
	bytes = APtrArray<u8int>::make(string.length());
	for (u64int i = 0; i < string.length(); i += 1) {
		bytes[i] = string[i];
	}
}

void A8Bits::set(u64int len, u8int num) {
	bytes = APtrArray<u8int>::make(len);
	for (u64int i = 0; i < len; i += 1) {
		bytes[i] = num;
	}
}

void A8Bits::set(const char * str, u64int len) {
	bytes = APtrArray<u8int>::make(len);
	for (u64int i = 0; i < len; i += 1) {
		bytes[i] = str[i];
	}
}

void A8Bits::set(const A8Bits & bits) {
	bytes = bits.bytes;
}

A8Bits A8Bits::make(u64int size, u8int byte) {
	A8Bits bits;
	bits.bytes = APtrArray<u8int>::make(size);
	for (s64int i = 0; i < size; i += 1) {
		bits[i] = byte;
	}
	return bits;
}

u64int A8Bits::length() {
	return bytes.length();
}

void A8Bits::merge(A8Bits bits) {
	u64int new_length = bytes.length() + bits.length();
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(new_length);
	for (u64int i = 0; i < bytes.length(); i += 1) {
		new_bytes[i] = bytes[i];
	}
	for (u64int i = bytes.length(); i < new_length; i += 1) {
		new_bytes[i] = bits[i - bytes.length()];
	}
}

void A8Bits::insert(A8Bits bits, u64int pos) {
	
	u64int pos_1 = 0;
	u64int pos_2 = pos;
	u64int pos_3 = pos + bits.length();
	
	u64int offset_1 = 0;
	u64int offset_2 = pos_2;
	u64int offset_3 = bits.length();
	
	u64int new_length = bytes.length() + bits.length();
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(new_length);
	
	for (u64int i = pos_1; i < pos_2; i += 1) {
		new_bytes[i] = bytes[i - offset_1];
	}
	
	for (u64int i = pos_2; i < pos_3; i += 1) {
		new_bytes[i] = bits[i - offset_2];
	}
	
	for (u64int i = pos_3; i < new_length; i += 1) {
		new_bytes[i] = bytes[i - offset_3];
	}

	bytes = new_bytes;
}

u8int & A8Bits::at(u64int idx) {
	return bytes[idx];
}

u8int A8Bits::get(u64int idx) {
	u8int res = bytes[idx];
	return res;
}

u8int A8Bits::pop(u64int idx) {
	u8int res = bytes[idx];
	if (bytes.length() == 1) {
		clear();
		return res;
	}
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(bytes.length() - 1);
	for (u64int i = 0; i < idx; i += 1) {
		new_bytes[i] = bytes[i];
	}
	for (u64int i = idx + 1; i < bytes.length(); i += 1) {
		new_bytes[i - 1] = bytes[i];
	}
	bytes = new_bytes;
	return res;
}

void A8Bits::resize(s64int size, u8int byte) {
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(size);
	for (u64int i = 0; i < bytes.length(); i += 1) {
		new_bytes[i] = bytes[i];
	}
	for (u64int i = bytes.length(); i < size; i += 1) {
		new_bytes[i] = byte;
	}
}

A8Bits A8Bits::get(s64int pos, s64int size) {
	A8Bits res;
	res.bytes = APtrArray<u8int>::make(size);
	for (s64int i = pos; i < pos + size; i += 1) {
		res[i - pos] = bytes[i];
	}
	return res;
}

A8Bits A8Bits::sub(s64int pos, s64int size) {

}

void A8Bits::remove(s64int pos, s64int size) {
	if (pos == 0 && size == bytes.length()) {
		clear();
	}

	u64int pos_1 = 0;
	u64int pos_2 = pos + size;
	u64int len_1 = pos;
	u64int len_2 = bytes.length();
	u64int offset_1 = 0;
	u64int offset_2 = pos_2;
	
	u64int new_length = bytes.length() + size;
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(new_length);
	
	for (u64int i = pos_1; i < len_1; i += 1) {
		new_bytes[i - offset_1] = bytes[i];
	}

	for (u64int i = pos_2; i < len_2; i += 1) {
		new_bytes[i - offset_2] = bytes[i];
	}

	bytes = new_bytes;
}

void A8Bits::push_beg(u8int byte) {
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(bytes.length() + 1);
	new_bytes[0] = byte;
	for (u64int i = 1; i < new_bytes.length(); i += 1) {
		new_bytes[i] = bytes[i - 1];
	}
	bytes = new_bytes;
}

void A8Bits::push_end(u8int byte) {
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(bytes.length() + 1);
	new_bytes[bytes.length()] = byte;
	for (u64int i = 0; i < bytes.length(); i += 1) {
		new_bytes[i] = bytes[i];
	}
	bytes = new_bytes;
}

u8int A8Bits::pop_beg() {
	u8int res = bytes[0];
	if (bytes.length() == 1) {
		clear();
		return res;
	}
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(bytes.length() - 1);
	for (u64int i = 1; i < bytes.length(); i += 1) {
		new_bytes[i - 1] = bytes[i];
	}
	bytes = new_bytes;
	return res;
}

u8int A8Bits::pop_end() {
	u8int res = bytes[bytes.length() - 1];
	if (bytes.length() == 1) {
		clear();
		return res;
	}
	APtrArray<u8int> new_bytes = APtrArray<u8int>::make(bytes.length() - 1);
	for (u64int i = 0; i < bytes.length() - 1; i += 1) {
		new_bytes[i] = bytes[i];
	}
	bytes = new_bytes;
	return res;
}

AString A8Bits::to_text() {
	A8BitsWeak weak(bytes);
	return weak.to_text();
}

bool A8Bits::from_text(AString text) {
	bytes = APtrArray<u8int>::make(text.length()/2);
	for (u64int i = 0; i < text.length()/2; i += 1) {
		AString hex = text.get(i*2, 2);
		bytes[i] = A8Bit::pack(A8Bit::from_text(hex));
	}
}

bool A8Bits::test_format(AString text) {
	return A8BitsWeak::test_format(text);
}

void A8Bits::logical_or(A8Bits bits, u64int pos) {
	A8BitsWeak weak(bytes);
	weak.logical_or(bits.bytes, pos);
}

void A8Bits::logical_xor(A8Bits bits, u64int pos) {
	A8BitsWeak weak(bytes);
	weak.logical_xor(bits.bytes, pos);
}

void A8Bits::logical_and(A8Bits bits, u64int pos) {
	A8BitsWeak weak(bytes);
	weak.logical_and(bits.bytes, pos);
}

void A8Bits::logical_not(u64int pos, u64int size) {
	A8BitsWeak weak(bytes);
	weak.logical_not(pos, size);
}

void A8Bits::logical_shr(u64int offset) {
	A8BitsWeak weak(bytes);
	weak.logical_shr(offset);
}

void A8Bits::logical_shl(u64int offset) {
	A8BitsWeak weak(bytes);
	weak.logical_shl(offset);
}

u8int & A8Bits::operator[](u64int idx) {
	return bytes[idx];
}

void A8Bits::operator ~() {
	A8BitsWeak weak(bytes);
	~weak;
}

void A8Bits::operator>>(u64int offset) {
	logical_shr(offset);
}

void A8Bits::operator<<(u64int offset) {
	logical_shl(offset);
}

void A8Bits::bit_set(u64int bit) {
	A8BitsWeak weak(bytes);
	weak.bit_set(bit);
}

bool A8Bits::bit_test(u64int bit) {
	A8BitsWeak weak(bytes);
	return weak.bit_test(bit);
}

void A8Bits::bit_clear(u64int bit) {
	A8BitsWeak weak(bytes);
	weak.bit_clear(bit);
}

A8Bits & A8Bits::operator=(A8Bits bits) {
	bytes = bits.bytes;
}

void A8Bits::clear() {
	bytes.reset();
}

A8Bits::~A8Bits() {
	clear();
}