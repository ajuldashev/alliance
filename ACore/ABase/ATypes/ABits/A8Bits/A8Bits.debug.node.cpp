#include "A8Bits.hpp"

#ifdef DEBUG_A8BITS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_a8bits(AString com) {
	#ifdef DEBUG_A8BIT
	if (com == "a8bit") {
		if (debug_a8bit()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A8BITSWEAK
	if (com == "a8bitsweak") {
		if (debug_a8bitsweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_a8bits() {
	AString text;
	#ifdef DEBUG_A8BIT
	text += "d) a8bit\n";
	#endif
	#ifdef DEBUG_A8BITSWEAK
	text += "d) a8bitsweak\n";
	#endif
	return text;
}

#endif
