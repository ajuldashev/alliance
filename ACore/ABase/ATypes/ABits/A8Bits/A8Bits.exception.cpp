#include "A8Bits.exception.hpp"

A8BitsException::A8BitsException() : AllianceException() {
	exception = "Alliance-Exception-A8Bits";
}

A8BitsException::A8BitsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A8Bits");
}

A8BitsException::A8BitsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A8Bits");
}

A8BitsException::A8BitsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A8Bits");
}

A8BitsException::A8BitsException(const A8BitsException & obj) : AllianceException(obj) {
	
}

A8BitsException::~A8BitsException() {

}

