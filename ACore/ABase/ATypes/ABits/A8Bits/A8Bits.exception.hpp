#ifndef HPP_A8BITSEXCEPTION
#define HPP_A8BITSEXCEPTION
#include <alliance.exception.hpp>

class A8BitsException : public AllianceException {
public:
	A8BitsException();
	A8BitsException(std::string text);
	A8BitsException(AllianceException & e);
	A8BitsException(AllianceException & e, std::string text);
	A8BitsException(const A8BitsException & obj);
	~A8BitsException();
};

#endif

