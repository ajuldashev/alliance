#ifndef HPP_A8BITS
#define HPP_A8BITS
#include "A8BitsWeak/A8BitsWeak.hpp"
#include "A8Bits.exception.hpp"

class A8Bits {
private:
    APtrArray<u8int> bytes;
public:
    A8Bits();
    A8Bits(u8int);
    A8Bits(AString);
    A8Bits(u64int, u8int);
    A8Bits(const char *, u64int);
    A8Bits(const A8Bits &);

    void set(u8int);
    void set(AString);
    void set(u64int, u8int);
    void set(const char *, u64int);
    void set(const A8Bits &);

    static A8Bits make(u64int, u8int);
    void resize(s64int, u8int byte = 0);

    u64int length();

    void merge(A8Bits);

    void insert(A8Bits, u64int);

	u8int & at(u64int);
	u8int get(u64int);
    u8int pop(u64int);

    A8Bits get(s64int, s64int);
    A8Bits sub(s64int, s64int);
    void   remove(s64int, s64int);

    void push_beg(u8int);
	void push_end(u8int);

    u8int pop_beg();
	u8int pop_end();

    AString to_text();
    bool from_text(AString);
    static bool test_format(AString);

    void logical_or (A8Bits, u64int);
    void logical_xor(A8Bits, u64int);
    void logical_and(A8Bits, u64int);
    
    void logical_not(u64int, u64int);

    void logical_shr(u64int);
    void logical_shl(u64int);

    u8int & operator[](u64int);

    void operator ~();

    void operator>>(u64int);
    void operator<<(u64int);

    void bit_set(u64int);
    bool bit_test(u64int);
    void bit_clear(u64int);

    A8Bits & operator=(A8Bits);
    void clear();
    
    ~A8Bits();
};

#endif
