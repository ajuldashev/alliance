#include "A8Bits.hpp"

#ifdef TEST_A8BITS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_a8bits(bool is_soft) {
	#ifdef TEST_A8BIT
	if (!test_a8bit(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A8BIT );
		return is_soft;
	}
	#endif
	#ifdef TEST_A8BITSWEAK
	if (!test_a8bitsweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A8BITSWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
