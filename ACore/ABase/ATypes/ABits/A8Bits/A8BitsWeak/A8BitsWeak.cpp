#include "A8BitsWeak.hpp"
#include "A8BitsWeak.test.cpp"
#include "A8BitsWeak.test.node.cpp"
#include "A8BitsWeak.debug.cpp"
#include "A8BitsWeak.debug.node.cpp"
#include "A8BitsWeak.exception.cpp"

A8BitsWeak::A8BitsWeak() {

}

A8BitsWeak::A8BitsWeak(APtrArray<u8int> ptr) {
    bytes = ptr;
}

A8BitsWeak::A8BitsWeak(APtrArrayWeak<u8int> ptr) {
    bytes = ptr;
}

A8BitsWeak::A8BitsWeak(const A8BitsWeak & bits) {
    bytes = bits.bytes;
}

u64int A8BitsWeak::length() {
    return bytes.length();
}

void A8BitsWeak::logical_or(A8BitsWeak bits, u64int pos) {
	for (u64int i = 0; i < bits.length(); i += 1) {
		bytes[i + pos] = bytes[i + pos] | bits[i];
	}
}

void A8BitsWeak::logical_xor(A8BitsWeak bits, u64int pos) {
	for (u64int i = 0; i < bits.length(); i += 1) {
		bytes[i + pos] = bytes[i + pos] ^ bits[i];
	}
}

void A8BitsWeak::logical_and(A8BitsWeak bits, u64int pos) {
	for (u64int i = 0; i < bits.length(); i += 1) {
		bytes[i + pos] = bytes[i + pos] & bits[i];
	}
}

void A8BitsWeak::logical_not(u64int pos, u64int size) {
	for (u64int i = 0; i < size; i += 1) {
		bytes[i + pos] = !bytes[i + pos];
	}
}

void A8BitsWeak::logical_shr(u64int offset) {
	u64int offset_byte = offset / 8;
	u64int offset_bit  = offset % 8;
	for (s64int i = bytes.length() - 1; i >= offset_byte; i -= 1) {
		bytes[i] = bytes[i - offset];
	}
	for (s64int i = 0; i < offset_byte; i += 1) {
		bytes[i] = 0;
	}
	u8int mask = 0xFF >> (8 - offset_bit);
	u8int temp_low = 0;
	u8int temp_hight = 0;
	for (u64int i = offset_byte; i < bytes.length(); i += 1) {
		temp_low = bytes[i] & mask;
		bytes[i] = (bytes[i] >> offset_bit) | temp_hight;
		temp_hight = temp_low << (8 - offset_bit);
	}
}

void A8BitsWeak::logical_shl(u64int offset) {
	u64int offset_byte = offset / 8;
	u64int offset_bit  = offset % 8;
	for (s64int i = offset_byte; i < bytes.length(); i += 1) {
		bytes[i - offset] = bytes[i];
	}
	for (s64int i = bytes.length() - offset_byte; i < bytes.length(); i += 1) {
		bytes[i] = 0;
	}
	u8int mask = 0xFF << (8 - offset_bit);
	u8int temp_low = 0;
	u8int temp_hight = 0;
	for (u64int i = bytes.length() - offset_byte; i >= 0; i -= 1) {
		temp_hight = bytes[i] & mask;
		bytes[i] = (bytes[i] << offset_bit) | temp_low;
		temp_low = temp_hight >> (8 - offset_bit);
	}
}

void A8BitsWeak::bit_set(u64int bit) {
	u64int byte = bit / 8;
	A8Bit::bit_set(bytes[byte], bit % 8);
}

bool A8BitsWeak::bit_test(u64int bit) {
	u64int byte = bit / 8;
	return A8Bit::bit_test(bytes[byte], bit % 8);
}

void A8BitsWeak::bit_clear(u64int bit) {
	u64int byte = bit / 8;
	A8Bit::bit_clear(bytes[byte], bit % 8);
}

AString A8BitsWeak::to_text() {
	AString res = AString::make(bytes.length() * 2);
	for (u64int i = 0; i < bytes.length(); i += 1) {
		AString hex = A8Bit::to_text(bytes[i]);
		res[i * 2 + 0] = hex[0];
		res[i * 2 + 1] = hex[1];
	}
}

bool A8BitsWeak::test_format(AString text) {
	if (text.length() % 2 != 0) {
		return false;
	}
	for (u64int i = 0; i < text.length(); i += 1) {
		if (!(
			(text[i] >= '0' && text[i] <= '9') ||
			(text[i] >= 'A' && text[i] <= 'F') ||
			(text[i] >= 'a' && text[i] <= 'f')
		)) {
			return false;
		}
	}
	return true;
}

u8int & A8BitsWeak::operator[](u64int idx) {
    return bytes[idx];
}

void A8BitsWeak::operator ~() {
    for (u64int i = 0; i < bytes.length(); i += 1) {
		bytes[i] = ~bytes[i];
	}
}

void A8BitsWeak::operator>>(u64int offset) {
    logical_shr(offset);
}

void A8BitsWeak::operator<<(u64int offset) {
    logical_shl(offset);
}

A8BitsWeak::~A8BitsWeak() {
    bytes.reset();
}
