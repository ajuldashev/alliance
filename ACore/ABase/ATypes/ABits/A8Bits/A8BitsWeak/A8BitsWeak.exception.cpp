#include "A8BitsWeak.exception.hpp"

A8BitsWeakException::A8BitsWeakException() : AllianceException() {
	exception = "Alliance-Exception-A8BitsWeak";
}

A8BitsWeakException::A8BitsWeakException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-A8BitsWeak");
}

A8BitsWeakException::A8BitsWeakException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-A8BitsWeak");
}

A8BitsWeakException::A8BitsWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-A8BitsWeak");
}

A8BitsWeakException::A8BitsWeakException(const A8BitsWeakException & obj) : AllianceException(obj) {
	
}

A8BitsWeakException::~A8BitsWeakException() {

}

