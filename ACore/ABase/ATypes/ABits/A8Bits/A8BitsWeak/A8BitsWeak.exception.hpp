#ifndef HPP_A8BITSWEAKEXCEPTION
#define HPP_A8BITSWEAKEXCEPTION
#include <alliance.exception.hpp>

class A8BitsWeakException : public AllianceException {
public:
	A8BitsWeakException();
	A8BitsWeakException(std::string text);
	A8BitsWeakException(AllianceException & e);
	A8BitsWeakException(AllianceException & e, std::string text);
	A8BitsWeakException(const A8BitsWeakException & obj);
	~A8BitsWeakException();
};

#endif

