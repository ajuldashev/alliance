#ifndef HPP_A8BITSWEAK
#define HPP_A8BITSWEAK
#include <alliance.config.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp>
#include <ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.hpp>
#include "A8BitsWeak.exception.hpp"

class A8BitsWeak {
private:
    APtrArrayWeak<u8int> bytes;
public:
    A8BitsWeak();
    A8BitsWeak(APtrArray<u8int>);
    A8BitsWeak(APtrArrayWeak<u8int>);
    A8BitsWeak(const A8BitsWeak &);

    u64int length();

    void logical_or (A8BitsWeak, u64int);
    void logical_xor(A8BitsWeak, u64int);
    void logical_and(A8BitsWeak, u64int);
    
    void logical_not(u64int, u64int);

    void logical_shr(u64int);
    void logical_shl(u64int);

    void bit_set(u64int);
    bool bit_test(u64int);
    void bit_clear(u64int);

    AString to_text();
    static bool test_format(AString);

    u8int & operator[](u64int);

    void operator ~();

    void operator>>(u64int);
    void operator<<(u64int);

    ~A8BitsWeak();
};

#endif
  