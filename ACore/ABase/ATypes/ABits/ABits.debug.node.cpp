#include "ABits.hpp"

#ifdef DEBUG_ABITS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_abits(AString com) {
	#ifdef DEBUG_A16BITS
	if (com == "a16bits") {
		if (debug_a16bits()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A32BITS
	if (com == "a32bits") {
		if (debug_a32bits()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A64BITS
	if (com == "a64bits") {
		if (debug_a64bits()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_A8BITS
	if (com == "a8bits") {
		if (debug_a8bits()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_abits() {
	AString text;
	#ifdef DEBUG_A16BITS
	text += "d) a16bits\n";
	#endif
	#ifdef DEBUG_A32BITS
	text += "d) a32bits\n";
	#endif
	#ifdef DEBUG_A64BITS
	text += "d) a64bits\n";
	#endif
	#ifdef DEBUG_A8BITS
	text += "d) a8bits\n";
	#endif
	return text;
}

#endif
