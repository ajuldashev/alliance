#include "ABits.exception.hpp"

ABitsException::ABitsException() : AllianceException() {
	exception = "Alliance-Exception-ABits";
}

ABitsException::ABitsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ABits");
}

ABitsException::ABitsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ABits");
}

ABitsException::ABitsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ABits");
}

ABitsException::ABitsException(const ABitsException & obj) : AllianceException(obj) {
	
}

ABitsException::~ABitsException() {

}

