#ifndef HPP_ABitsEXCEPTION
#define HPP_ABitsEXCEPTION
#include <alliance.exception.hpp>

class ABitsException : public AllianceException {
public:
	ABitsException();
	ABitsException(std::string text);
	ABitsException(AllianceException & e);
	ABitsException(AllianceException & e, std::string text);
	ABitsException(const ABitsException & obj);
	~ABitsException();
};

#endif

