#include "ABits.hpp"

#ifdef TEST_ABITS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_abits(bool is_soft) {
	#ifdef TEST_A16BITS
	if (!test_a16bits(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A16BITS );
		return is_soft;
	}
	#endif
	#ifdef TEST_A32BITS
	if (!test_a32bits(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A32BITS );
		return is_soft;
	}
	#endif
	#ifdef TEST_A64BITS
	if (!test_a64bits(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A64BITS );
		return is_soft;
	}
	#endif
	#ifdef TEST_A8BITS
	if (!test_a8bits(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_A8BITS );
		return is_soft;
	}
	#endif
	return true;
}
#endif
