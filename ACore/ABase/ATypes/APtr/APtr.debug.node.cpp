#include "APtr.hpp"

#ifdef DEBUG_APTR
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aptr(AString com) {
	#ifdef DEBUG_APTRUNIQUE
	if (com == "aptrunique") {
		if (debug_aptrunique()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_APTRWEAK
	if (com == "aptrweak") {
		if (debug_aptrweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aptr() {
	AString text;
	#ifdef DEBUG_APTRUNIQUE
	text += "d) aptrunique\n";
	#endif
	#ifdef DEBUG_APTRWEAK
	text += "d) aptrweak\n";
	#endif
	return text;
}

#endif
