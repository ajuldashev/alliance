#include "APtr.exception.hpp"

APtrException::APtrException() : AllianceException() {
	exception = "Alliance-Exception-APtr";
}

APtrException::APtrException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-APtr");
}

APtrException::APtrException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-APtr");
}

APtrException::APtrException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtr");
}

APtrException::APtrException(const APtrException & obj) : AllianceException(obj) {
	
}

APtrException::~APtrException() {

}

