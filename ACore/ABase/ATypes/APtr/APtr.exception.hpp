#ifndef HPP_APTREXCEPTION
#define HPP_APTREXCEPTION
#include <alliance.exception.hpp>

class APtrException : public AllianceException {
public:
	APtrException();
	APtrException(std::string text);
	APtrException(AllianceException & e);
	APtrException(AllianceException & e, std::string text);
	APtrException(const APtrException & obj);
	~APtrException();
};

#endif

