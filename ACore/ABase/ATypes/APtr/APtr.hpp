#ifndef HPP_APTR
#define HPP_APTR
#include <alliance.config.hpp>
#include <cstddef>
#include <iostream>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "APtr.exception.hpp"

#ifndef HPP_FRIEND_APTRWEAK
#define HPP_FRIEND_APTRWEAK
template <typename T>
class APtrWeak;
#endif

/** <doc-class-info>
 * Шаблонный класс, автоматический указатель, для управления динамической памяти путем подсчета ссылок на нее. 
 * Создает указатель на данные с помощь оператора NEW, НЕ МАССИВЫ, для них используется другой класс.
 */
template <typename T>
class APtr {
	/** <doc-class-friend>
	 * Дружественный шаблонный класс
	 * Нужен для того чтобы можно было сравнивать хранимые внутри указатели на равенство
	 */
	friend APtrWeak<T>;
private:
	// <doc-private-field> Указатель на данные
	T      * pointer       = NULL;
	// <doc-private-field> Указатель на счетчик ссылок на данные
	u64int * pointer_count = NULL;
	/** <doc-private-method> 
	 * Метод для удаления текущего указателя
	 * При вызыве, pointer_count уменьшается на 1
	 * Данные удаляются если pointer_count равен нулю
	 */
	void remove();
public:

	/** <doc-public-method> 
	 * Конструктор
	 * Инициализирует поля по умолчанию
	 */
	APtr();

	/** <doc-public-method> 
	 * Конструктор
	 * arg 1 : ptr - указатель на данные, которые были выделены при помощи оператора new
	 */
	APtr(T * ptr);

	/** <doc-public-method> 
	 * Конструктор копирования 
	 * arg 1 : aptr - объект
	 * Если pointer не равен NULL, то он копируется, а pointer_count увеличиывется на 1 
	 */
	APtr(const APtr & aptr);
	
	/** <doc-public-method> 
	 * Метод сброса указателя
	 * При вызове, вызывается remove и обнуляются поля 
	 */
	void reset();

	/** <doc-public-method> 
	 * Метод обнавление данных хранимых в указателе 
	 */
	void update(T data);

	/** <doc-public-method> 
	 * Метод проверки указателя на NULL
	 * return - true если pointer == NULL 
	 */
	bool is_empty() const;

	/** <doc-public-method> 
	 * Метод проверки указателя на NULL
	 * return - true если pointer != NULL 
	 */
	bool is_present() const;

	/** <doc-public-method> 
	 * Метод проверки указателя на NULL
	 * return - возвращает указатель на константные данные хранимые данные  
	 */
	const T * at() const;
	
	/** <doc-public-operator> 
	 * Оператор присваения
	 * arg 1 : aptr - присваевыемый объект
	 * Если pointer не равен NULL, то он копируется, а pointer_count увеличиывется на 1 
	 * return - возвращает ссылку на себя  
	 */
	APtr & operator=(APtr aptr);
	
	/** <doc-public-operator> 
	 * Оператор разыменования указателя
	 * return - возвращает ссылку на хранимые данные 
	 * <doc-warning> Если pointer == NULL, при вызове выкинет исключение APtrException
	 */
	T& operator*();

	/** <doc-public-operator> 
	 * Оператор разыменования указателя
	 * return - возвращает указателя на хранимые данные 
	 * <doc-warning> Если pointer == NULL, при вызове выкинет исключение APtrException
	 */
	T* operator->();
	
	/** <doc-public-operator> 
	 * Оператор сравнения равно
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалети pointer равны между собой 
	 */
	bool operator==(APtr & aptr);

	/** <doc-public-operator> 
	 * Оператор сравнения НЕ равно
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалети pointer НЕ равны между собой 
	 */
	bool operator!=(APtr & aptr);

	/** <doc-public-operator> 
	 * Оператор сравнения меньше
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалеть pointer у this меньше чем у aptr 
	 */
	bool operator <(APtr & aptr);

	/** <doc-public-operator> 
	 * Оператор сравнения больше
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалеть pointer у this больше чем у aptr 
	 */
	bool operator >(APtr & aptr);

	/** <doc-public-operator> 
	 * Оператор сравнения больше или равно
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалеть pointer у this больше или равен aptr.pointer 
	 */
	bool operator>=(APtr & aptr);

	/** <doc-public-operator> 
	 * Оператор сравнения меньше или равно
	 * arg 1 : aptr - объект с которым сравнивают текущий
	 * return - true если указалеть pointer у this меньше или равен aptr.pointer
	 */
	bool operator<=(APtr & aptr);
	
	/** <doc-friend-operator> 
	 * Дружественный оператор сравнения равно
	 * arg 1 : aptr_left  - объект с лева  от знака сравнения
	 * arg 2 : aptr_right - объект с права от знака сравнения
	 * return - true если  aptr_left.pointer == aptr_right.pointer
	 */
	template<typename R>
	friend bool operator==(APtr<R> & aptr_left, APtrWeak<R> & aptr_right);
	
	/** <doc-friend-operator> 
	 * Дружественный оператор сравнения не равно
	 * arg 1 : aptr_left  - объект с лева  от знака сравнения
	 * arg 2 : aptr_right - объект с права от знака сравнения
	 * return - true если  aptr_left.pointer != aptr_right.pointer
	 */
	template<typename R>
	friend bool operator!=(APtr<R> & aptr_left, APtrWeak<R> & aptr_right);

	/** <doc-public-operator> 
	 * Оператор приведения к логическому типу bool
	 * return - true если pointer != NULL
	 */
	operator bool();

	// <doc-template-friend-function> Дружественная шаблонная функция для создания объекта
	template<typename L, typename R>
	friend APtr<L> aptr_make();
	// <doc-template-friend-function> Дружественная шаблонная функция для приведения типа
	template<typename L, typename R>
	friend APtr<R> aptr_cast(APtr<L> & aptr);

	/** <doc-static-public-method> 
	 * Статический метод для динамического создания объекта
	 * return - Указатель на данные с типом указанном в шаблоне с помощью операции NEW, НЕ МАССИВЫ
	 */
	static APtr<T> make();

	/** <doc-static-public-method> 
	 * Статический метод для динамического клонирования объекта
	 * arg 1 : data - данные которые будут сохранениы в указатель
	 * return - Указатель на данные с типом указанном в шаблоне с помощью операции NEW, НЕ МАССИВЫ
	 */
	static APtr<T> clone(T data);

	/** <doc-public-method> 
	 * Деструктор уменьшает pointer_count с помощью вызова метода remove
	 */
	~APtr();
};

template <typename T> 
APtr<T>::APtr() {
	pointer = NULL;
	pointer_count = NULL;
}

template <typename T> 
APtr<T>::APtr(T * ptr) {
	if (ptr == NULL) {
		pointer = NULL;
		pointer_count = NULL;
		return;
	}
	pointer = ptr;
	pointer_count = new u64int;
	*pointer_count = 1;
}

template <typename T> 
APtr<T>::APtr(const APtr & aptr) {
	if (aptr.pointer == NULL) {
		pointer = NULL;
		pointer_count = NULL;
		return;
	}
	pointer = aptr.pointer;
	pointer_count = aptr.pointer_count;
	(*pointer_count) += 1;
}

template <typename T> 
void APtr<T>::remove() {
	if (pointer_count == NULL) {
		return;
	}
	(*pointer_count) -= 1;
	if (*pointer_count == 0) {
		delete pointer;
		delete pointer_count;
		pointer = NULL;
		pointer_count = NULL;
	}
}

template <typename T> 
void APtr<T>::reset() {
	remove();
	if (pointer) {
		pointer = NULL;
		pointer_count = NULL;
	}
}

template <typename T> 
void APtr<T>::update(T data) {
	if (pointer) {
		*pointer = data;
	}
}

template <typename T> 
bool APtr<T>::is_empty() const {
	return pointer == NULL;
}

template <typename T> 
bool APtr<T>::is_present() const {
	return pointer != NULL;
}

template <typename T> 
const T * APtr<T>::at() const {
	return pointer;
}

template <typename T> 
APtr<T>& APtr<T>::operator=(APtr<T> aptr) {
	if (this != &aptr) {
		if (pointer_count && *pointer_count > 0) {
			remove();
		}
		pointer = aptr.pointer;
		pointer_count = aptr.pointer_count;
		if(pointer_count) {
			(*pointer_count) += 1;
		}
	}
	return *this;
}

template <typename T> 
T& APtr<T>::operator*() {
	if (pointer) {
		return *pointer;
	}
	throw APtrException("operator*()");
}

template <typename T> 
T * APtr<T>::operator->() {
	if (pointer) {
		return pointer;
	}
	throw APtrException("operator->()");
}


template <typename T> 
bool APtr<T>::operator==(APtr<T> & aptr) {
	if(pointer == aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtr<T>::operator!=(APtr<T> & aptr) {
	if(pointer != aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtr<T>::operator<(APtr<T> & aptr) {
	if(pointer < aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtr<T>::operator>(APtr<T> & aptr) {
	if(pointer > aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtr<T>::operator<=(APtr<T> & aptr) {
	if(pointer <= aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtr<T>::operator>=(APtr<T> & aptr) {
	if(pointer >= aptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
APtr<T>::operator bool() {
	return !is_empty();
}

template <typename T> 
APtr<T> APtr<T>::make() {
	APtr<T> res(new T);
	return res;
}

template <typename T> 
APtr<T> APtr<T>::clone(T data) {
	APtr<T> res(new T);
	res.update(data);
	return res;
}

/** <doc-template-function> 
 * Дружественная шаблонная функция для создания объекта
 * type L : Шаблон с таким типом данных вернет функция
 * type R : Данные с таким типом создаст функция
 * return - Функция создает данные типа R, помещает их в объект шаблон типа L и возвращает его.
 */
template<typename L, typename R>
APtr<L> aptr_make() {
	return APtr<L>(static_cast<L*>(new R));
}

/** <doc-template-function> 
 * Дружественная шаблонная функция для приведения типа
 * type L : Исходный тип данных 
 * type R : Тип данных к которым будут приведены данные 
 * arg 1  : Объект APtr<L>, указатель которого будет приведен к типу R 
 * return - Функция приводит pointer типа L к R, при этом pointer_count увеличивается на 1 и возвращает их в APtr<R>
 */
template<typename L, typename R>
APtr<R> aptr_cast(APtr<L> & aptr) {
	APtr<R> res;
	res.pointer = static_cast<R*>(aptr.pointer);
	res.pointer_count = aptr.pointer_count;
	if(res.pointer_count) {
		(*res.pointer_count) += 1;
	}
	return res;
}

template <typename T> 
APtr<T>::~APtr() {
	remove();
}

#endif