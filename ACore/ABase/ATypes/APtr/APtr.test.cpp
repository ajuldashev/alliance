#include "APtr.hpp"

#ifdef TEST_APTR
#include <ACore/ABase/AIO/AIO.hpp>

namespace APtrTest {
   class A {
   public:
      virtual void f() {
         AIO::writeln("A");
      }
   };

   class B : public A {
   public:
      virtual void f() {
         AIO::writeln("B");
      }
      void f2() {
         AIO::writeln("F2");
      }
   };
};

bool test_aptr(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_APTR );
   
   if (!test_node_aptr(is_soft)) {
      return is_soft;
   }
   
   {
      AIO::writeln_warning("Test aptr()");
      APtr<APtrTest::A> ptr;
      if (!ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test aptr(A *)");
      APtr<APtrTest::A> ptr(new APtrTest::A);
      if (ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test aptr(const A *)");
      APtr<APtrTest::A> ptr_1(new APtrTest::A);
      if (ptr_1.is_empty()) {
         return is_soft;
      }
      APtr<APtrTest::A> ptr_2(ptr_1);
      if (ptr_2.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test reset");
      APtr<APtrTest::A> ptr(new APtrTest::A);
      if (ptr.is_empty()) {
         return is_soft;
      }
      ptr.reset();
      if (!ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test make");
      APtr<APtrTest::A> ptr = APtr<APtrTest::A>::make();
      if (ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test bool");
      APtr<APtrTest::A> ptr = APtr<APtrTest::A>::make();
      if (!ptr) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test ptr = aptr_make<A, A>()");
      APtr<APtrTest::A> ptr = aptr_make<APtrTest::A, APtrTest::A>();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr -> F()");
      ptr->f();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_a = aptr_make<A, B>()");
      APtr<APtrTest::A> ptr_a = aptr_make<APtrTest::A, APtrTest::B>();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_a -> F()");
      ptr_a->f();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_b = aptr_cast<A, B>(ptr_a)");
      APtr<APtrTest::B> ptr_b = aptr_cast<APtrTest::A, APtrTest::B>(ptr_a);
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_b -> F2()");
      ptr_b->f2();
      AIO::writeln_success("Done");
   }

   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_APTR );
   return true;
}
#endif

