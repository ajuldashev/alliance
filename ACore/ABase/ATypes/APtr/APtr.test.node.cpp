#include "APtr.hpp"

#ifdef TEST_APTR
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aptr(bool is_soft) {
	#ifdef TEST_APTRUNIQUE
	if (!test_aptrunique(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APTRUNIQUE );
		return is_soft;
	}
	#endif
	#ifdef TEST_APTRWEAK
	if (!test_aptrweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APTRWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
