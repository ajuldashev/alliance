#include "APtrUnique.exception.hpp"

APtrUniqueException::APtrUniqueException() : AllianceException() {
	exception = "Alliance-Exception-APtrUnique";
}

APtrUniqueException::APtrUniqueException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-APtrUnique");
}

APtrUniqueException::APtrUniqueException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-APtrUnique");
}

APtrUniqueException::APtrUniqueException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrUnique");
}

APtrUniqueException::APtrUniqueException(const APtrUniqueException & obj) : AllianceException(obj) {
	
}

APtrUniqueException::~APtrUniqueException() {

}

