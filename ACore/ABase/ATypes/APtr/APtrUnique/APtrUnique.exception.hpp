#ifndef HPP_APTRUNIQUEEXCEPTION
#define HPP_APTRUNIQUEEXCEPTION
#include <alliance.exception.hpp>

class APtrUniqueException : public AllianceException {
public:
	APtrUniqueException();
	APtrUniqueException(std::string text);
	APtrUniqueException(AllianceException & e);
	APtrUniqueException(AllianceException & e, std::string text);
	APtrUniqueException(const APtrUniqueException & obj);
	~APtrUniqueException();
};

#endif

