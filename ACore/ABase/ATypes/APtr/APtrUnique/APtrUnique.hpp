#ifndef HPP_APTRUNIQUE
#define HPP_APTRUNIQUE
#include <alliance.config.hpp>
#include <cstddef>
#include <iostream>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "APtrUnique.exception.hpp"

#ifndef HPP_FRIEND_APTRWEAK
#define HPP_FRIEND_APTRWEAK
template <typename T>
class APtrWeak;
#endif

template <typename T>
class APtrUnique {
	friend APtrWeak<T>;
private:
	mutable T * pointer = NULL;
public:
	APtrUnique();
	APtrUnique(const APtrUnique &);
	void reset();

	bool is_empty() const;
	const T * at() const;
	
	APtrUnique & operator=(APtrUnique);
	
	T& operator*();
	T* operator->();
	
	bool operator==(APtrUnique &);
	bool operator!=(APtrUnique &);
	bool operator <(APtrUnique &);
	bool operator >(APtrUnique &);
	bool operator>=(APtrUnique &);
	bool operator<=(APtrUnique &);
	
	template<typename R>
	friend bool operator==(APtrUnique<R> &, APtrWeak<R> &);
	template<typename R>
	friend bool operator!=(APtrUnique<R> &, APtrWeak<R> &);

	operator bool();

	template<typename L, typename R>
	friend APtrUnique<L> aptr_unique_make();
	template<typename L, typename R>
	friend APtrUnique<R> aptr_unique_cast(APtrUnique<L> & obj);

	static APtrUnique<T> make();

	~APtrUnique();
};

template <typename T> 
APtrUnique<T>::APtrUnique() {
	pointer = NULL;
}

template <typename T> 
APtrUnique<T>::APtrUnique(const APtrUnique & sptr) {
	pointer = sptr.pointer;
	sptr.pointer = NULL;
}

template <typename T> 
void APtrUnique<T>::reset() {
	if (pointer == NULL) {
		return;
	}
	delete pointer;
	pointer = NULL;
}

template <typename T> 
bool APtrUnique<T>::is_empty() const {
	return pointer == NULL;
}

template <typename T> 
const T * APtrUnique<T>::at() const {
	return pointer;
}

template <typename T> 
APtrUnique<T>& APtrUnique<T>::operator=(APtrUnique<T> sptr) {
	if (this != &sptr) {
		if (pointer) {
			delete pointer;
		}
		pointer = sptr.pointer;
		sptr.pointer = NULL;
	}
	return *this;
}

template <typename T> 
T& APtrUnique<T>::operator*() {
	if (!pointer) {
		throw APtrUniqueException("operator*()");
	}
	return *pointer;
}

template <typename T> 
T * APtrUnique<T>::operator->() {
	if (!pointer) {
		throw APtrUniqueException("operator->()");
	}
	return pointer;
}


template <typename T> 
bool APtrUnique<T>::operator==(APtrUnique<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrUnique<T>::operator!=(APtrUnique<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrUnique<T>::operator<(APtrUnique<T> & sptr) {
	if(pointer < sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrUnique<T>::operator>(APtrUnique<T> & sptr) {
	if(pointer > sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrUnique<T>::operator<=(APtrUnique<T> & sptr) {
	if(pointer <= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrUnique<T>::operator>=(APtrUnique<T> & sptr) {
	if(pointer >= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
APtrUnique<T>::operator bool() {
	return !is_empty();
}

template <typename T> 
APtrUnique<T> APtrUnique<T>::make() {
	APtrUnique<T> res;
	res.pointer = new T;
	return res;
}

template<typename L, typename R>
APtrUnique<L> aptr_unique_make() {
	APtrUnique<L> res;
	res.pointer = static_cast<L*>(new R);
	return res;
}

template<typename L, typename R>
APtrUnique<R> aptr_unique_cast(APtrUnique<L> & obj) {
	APtrUnique<R> res;
	res.pointer = static_cast<R*>(obj.pointer);
	obj.pointer = NULL;
	return res;
}

template <typename T> 
APtrUnique<T>::~APtrUnique() {
	reset();
}

#endif