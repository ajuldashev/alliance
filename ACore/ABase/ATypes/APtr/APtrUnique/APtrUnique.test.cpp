#include "APtrUnique.hpp"

#ifdef TEST_APTRUNIQUE
#include <ACore/ABase/AIO/AIO.hpp>

namespace APtrUniqueTest {
   class A {
   public:
      virtual void f() {
         AIO::writeln("A");
      }
   };

   class B : public A {
   public:
      virtual void f() {
         AIO::writeln("B");
      }
      void f2() {
         AIO::writeln("F2");
      }
   };
};

bool test_aptrunique(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_APTRUNIQUE );

   if (!test_node_aptrunique(is_soft)) {
      return is_soft;
   }

   {
      AIO::writeln_warning("Test aptrunique()");
      APtrUnique<s32int> ptr;
      if (!ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test make && operator = ");
      APtrUnique<s32int> ptr = APtrUnique<s32int>::make();
      if (ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test aptrunique(const APtrUnique &)");
      APtrUnique<s32int> ptr = APtrUnique<s32int>::make();
      if (ptr.is_empty()) {
         return is_soft;
      }
      APtrUnique<s32int> ptr2(ptr);
      if (!ptr.is_empty() && ptr2.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }

   {
      AIO::writeln_warning("Test ptr = aptr_unique_make<A, A>()");
      APtrUnique<APtrUniqueTest::A> ptr = aptr_unique_make<APtrUniqueTest::A, APtrUniqueTest::A>();
      if (ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr -> F()");
      ptr->f();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_a = aptr_unique_make<A, B>()");
      APtrUnique<APtrUniqueTest::A> ptr_a = aptr_unique_make<APtrUniqueTest::A, APtrUniqueTest::B>();
      if (ptr_a.is_empty() && !ptr.is_empty() ) {
         return is_soft;
      }
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_a -> F()");
      ptr_a->f();
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_b = aptr_unique_cast<A, B>(ptr_a)");
      APtrUnique<APtrUniqueTest::B> ptr_b = aptr_unique_cast<APtrUniqueTest::A, APtrUniqueTest::B>(ptr_a);
      if (ptr_b.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");

      AIO::writeln_warning("Test ptr_b -> F2()");
      ptr_b->f2();
      AIO::writeln_success("Done");
   }
   {
      AIO::writeln_warning("Test operator = ");
      APtrUnique<s32int> ptr = APtrUnique<s32int>::make();
      if (ptr.is_empty()) {
         return is_soft;
      }
      APtrUnique<s32int> ptr_2;
      if (!ptr_2.is_empty()) {
         return is_soft;
      }
      ptr_2 = ptr;
      if (ptr_2.is_empty() && !ptr.is_empty()) {
         return is_soft;
      }
      AIO::writeln_success("Done");
   }
   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_APTRUNIQUE );
   return true;
}
#endif

