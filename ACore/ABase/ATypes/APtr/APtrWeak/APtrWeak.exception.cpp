#include "APtrWeak.exception.hpp"

APtrWeakException::APtrWeakException() : AllianceException() {
	exception = "Alliance-Exception-APtrWeak";
}

APtrWeakException::APtrWeakException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-APtrWeak");
}

APtrWeakException::APtrWeakException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-APtrWeak");
}

APtrWeakException::APtrWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrWeak");
}

APtrWeakException::APtrWeakException(const APtrWeakException & obj) : AllianceException(obj) {
	
}

APtrWeakException::~APtrWeakException() {

}

