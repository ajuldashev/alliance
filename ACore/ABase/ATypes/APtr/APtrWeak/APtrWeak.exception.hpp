#ifndef HPP_APTRWEAKEXCEPTION
#define HPP_APTRWEAKEXCEPTION
#include <alliance.exception.hpp>

class APtrWeakException : public AllianceException {
public:
	APtrWeakException();
	APtrWeakException(std::string text);
	APtrWeakException(AllianceException & e);
	APtrWeakException(AllianceException & e, std::string text);
	APtrWeakException(const APtrWeakException & obj);
	~APtrWeakException();
};

#endif

