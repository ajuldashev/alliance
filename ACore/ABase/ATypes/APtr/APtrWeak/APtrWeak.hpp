#ifndef HPP_APTRWEAK
#define HPP_APTRWEAK
#include <alliance.config.hpp>
#include <cstddef>
#include <iostream>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include <ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp>
#include "APtrWeak.exception.hpp"

template <typename T>
class APtrWeak {
protected:
	T * pointer;
public:
	APtrWeak();
	APtrWeak(T *);
	APtrWeak(APtr<T> &);
	APtrWeak(APtrWeak<T> &);
	APtrWeak(const APtrWeak &);
	void reset();

	bool is_empty() const;
	
	const T * at() const;
	
	APtrWeak & operator=(APtr<T> &);
	APtrWeak & operator=(APtrWeak);
	APtrWeak & operator=(APtrUnique<T> &);
	
	T& operator*();
	T* operator->();
	
	bool operator==(APtrWeak &);
	bool operator!=(APtrWeak &);
	bool operator <(APtrWeak &);
	bool operator >(APtrWeak &);
	bool operator>=(APtrWeak &);
	bool operator<=(APtrWeak &);
	
	bool operator==(APtr<T> &);
	bool operator!=(APtr<T> &);
	
	bool operator==(APtrUnique<T> &);
	bool operator!=(APtrUnique<T> &);

	template<typename R>
	friend bool operator==(APtr<R> &, APtrWeak<R> &);
	template<typename R>
	friend bool operator!=(APtr<R> &, APtrWeak<R> &);

	template<typename R>
	friend bool operator==(APtrUnique<R> &, APtrWeak<R> &);
	template<typename R>
	friend bool operator!=(APtrUnique<R> &, APtrWeak<R> &);

	operator bool();

	~APtrWeak();
};

template <typename T> 
APtrWeak<T>::APtrWeak() {
	pointer = 0;
}

template <typename T> 
APtrWeak<T>::APtrWeak(T * sptr) {
	pointer = sptr;
}

template <typename T> 
APtrWeak<T>::APtrWeak(APtr<T> & sptr) {
	pointer = sptr.pointer;
}

template <typename T> 
APtrWeak<T>::APtrWeak(APtrWeak & sptr) {
	pointer = sptr.pointer;
}

template <typename T> 
APtrWeak<T>::APtrWeak(const APtrWeak & sptr) {
	pointer = sptr.pointer;
}

template <typename T> 
void APtrWeak<T>::reset() {
	pointer = NULL;
}

template <typename T> 
bool APtrWeak<T>::is_empty() const {
	return pointer == NULL;
}

template <typename T> 
const T * APtrWeak<T>::at() const {
	return pointer;
}

template <typename T> 
APtrWeak<T>& APtrWeak<T>::operator=(APtrWeak<T> sptr) {
	pointer = sptr.pointer;
	return *this;
}

template <typename T> 
APtrWeak<T>& APtrWeak<T>::operator=(APtr<T> & sptr) {
	pointer = sptr.pointer;
	return *this;
}

template <typename T> 
APtrWeak<T>& APtrWeak<T>::operator=(APtrUnique<T> & sptr) {
	pointer = sptr.pointer;
	return *this;
}

template <typename T> 
T& APtrWeak<T>::operator*() {
	if (pointer) {
		return *pointer;
	} else {
		throw APtrWeakException("operator*()");
	}
}

template <typename T> 
T * APtrWeak<T>::operator->() {
	if (pointer) {
		return pointer;
	} else {
		throw APtrWeakException("operator->()");
	}
}


template <typename T> 
bool APtrWeak<T>::operator==(APtrWeak<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator!=(APtrWeak<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator<(APtrWeak<T> & sptr) {
	if(pointer < sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator>(APtrWeak<T> & sptr) {
	if(pointer > sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator<=(APtrWeak<T> & sptr) {
	if(pointer <= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator>=(APtrWeak<T> & sptr) {
	if(pointer >= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator==(APtr<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator!=(APtr<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator==(APtrUnique<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrWeak<T>::operator!=(APtrUnique<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename R> 
bool operator==(APtr<R> & sptr1, APtrWeak<R> & sptr2) {
	if(sptr1.pointer == sptr2.pointer) {
		return true;
	}
	return false;
}

template <typename R> 
bool operator!=(APtr<R> & sptr1, APtrWeak<R> & sptr2) {
	if(sptr1.pointer != sptr2.pointer) {
		return true;
	}
	return false;
}

template <typename R> 
bool operator==(APtrUnique<R> & sptr1, APtrWeak<R> & sptr2) {
	if(sptr1.pointer == sptr2.pointer) {
		return true;
	}
	return false;
}

template <typename R> 
bool operator!=(APtrUnique<R> & sptr1, APtrWeak<R> & sptr2) {
	if(sptr1.pointer != sptr2.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
APtrWeak<T>::operator bool() {
	return !is_empty();
}

template <typename T> 
APtrWeak<T>::~APtrWeak() {
	
}

#endif