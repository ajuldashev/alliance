#include "APtrWeak.hpp"

#ifdef TEST_APTRWEAK
#include <ACore/ABase/AIO/AIO.hpp>
bool test_aptrweak(bool is_soft) {
   AIO::write_div_line();
   AIO::writeln("Begin test the module " MODULE_TEST_APTRWEAK );
   
   if (!test_node_aptrweak(is_soft)) {
      return is_soft;
   }

   APtr<s32int> ptr = APtr<s32int>::make();
   APtrWeak<s32int> ptr_weak = ptr;
   AIO::writeln_warning("Test make APtr");
   if (ptr) {
      AIO::writeln_success("APtr is exist");
   } else {
      AIO::writeln_error("APtr not exist");
      return is_soft;
   }
   AIO::writeln_warning("Test make APtrWeak");
   if (ptr_weak) {
      AIO::writeln_success("APtrWeak is exist");
   } else {
      AIO::writeln_error("APtrWeak not exist");
      return is_soft;
   }
   AIO::writeln_warning("Test ptr = 5");
   *ptr = 5;
   if (*ptr_weak == 5) {
      AIO::write_success("*ptr_weak == *ptr == ");
      AIO::writeln(*ptr_weak);
   } else {
      AIO::writeln_error("APtrWeak != APtr");
      return is_soft;
   }
   AIO::writeln_warning("Test ptr_weak = 7");
   *ptr_weak = 7;
   if (*ptr == 7) {
      AIO::write_success("*ptr == *ptr_weak == ");
      AIO::writeln(*ptr);
   } else {
      AIO::writeln_error("APtrWeak != APtr");
      return is_soft;
   }
   AIO::writeln_warning("Test ptr = ptr_weak");
   if (ptr == ptr_weak) {
      AIO::writeln_success("ptr == ptr_weak");
   } else {
      AIO::writeln_error("ptr != ptr_weak");
      return is_soft;
   }
   AIO::writeln_warning("Test ptr != ptr2");
   APtr<s32int> ptr2 = APtr<s32int>::make();
   if (ptr != ptr2) {
      AIO::writeln_success("ptr != ptr2");
   } else {
      AIO::writeln_error("ptr == ptr2");
      return is_soft;
   }
   AIO::writeln_warning("Test ptr2 = ptr_weak");
   if (ptr2 != ptr_weak) {
      AIO::writeln_success("ptr2 != ptr_weak");
   } else {
      AIO::writeln_error("ptr2 == ptr_weak");
      return is_soft;
   }
   AIO::write_div_line();
   AIO::writeln_success("Done  test the module " MODULE_TEST_APTRWEAK );
   return true;
}
#endif

