#include "APtrArray.hpp"

#ifdef DEBUG_APTRARRAY
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aptrarray() {
    APtrArray<s32int> ptr;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_APTRARRAY );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aptrarray());
            AIO::writeln("1) help");
            AIO::writeln("2) make");
            AIO::writeln("3) []");
            AIO::writeln("4) remove");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aptrarray(com)) {
			return true;
		}
        if (com == "make") {
            ptr = APtrArray<s32int>::make(AIO::get_s32int());
        }
        if (com == "[]") {
            try {
                ptr[AIO::get_s32int()];
            }
            catch (APtrArrayException e) {
                AIO::writeln_error(e.get_text().c_str());
            }
        }
        if (com == "clear") {
            ptr.remove();
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_APTRARRAY );
    AIO::write_div_line();
    return false;
}
#endif

