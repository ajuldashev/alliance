#include "APtrArray.hpp"

#ifdef DEBUG_APTRARRAY
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aptrarray(AString com) {
	#ifdef DEBUG_APTRARRAYWEAK
	if (com == "aptrarrayweak") {
		if (debug_aptrarrayweak()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aptrarray() {
	AString text;
	#ifdef DEBUG_APTRARRAYWEAK
	text += "d) aptrarrayweak\n";
	#endif
	return text;
}

#endif
