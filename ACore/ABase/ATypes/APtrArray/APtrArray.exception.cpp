#include "APtrArray.exception.hpp"

APtrArrayException::APtrArrayException() : AllianceException() {
	index = 0;
	exception = "Alliance-Exception-APtrArray";
}

APtrArrayException::APtrArrayException(std::string text) : AllianceException(text) {
	index = 0;
	add_text("Alliance-Exception-APtrArray");
}

APtrArrayException::APtrArrayException(std::string text, u64int i) {
	index = i;
	text += std::string(" | index : ") + std::to_string(index);
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArray");
}

APtrArrayException::APtrArrayException(AllianceException & e) : AllianceException(e) {
	index = 0;
	add_text("Alliance-Exception-APtrArray");
}

APtrArrayException::APtrArrayException(AllianceException & e, std::string text) : AllianceException(e) {
	index = 0;
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArray");
}

APtrArrayException::APtrArrayException(AllianceException & e, std::string text, u64int i) : AllianceException(e) {
	index = i;
	text += std::string(" | index : ") + std::to_string(index);
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArray");
}

APtrArrayException::APtrArrayException(const APtrArrayException & obj) : AllianceException(obj) {
	index = 0;
}

u64int APtrArrayException::get_index() {
	return index;
}

APtrArrayException::~APtrArrayException() {

}

