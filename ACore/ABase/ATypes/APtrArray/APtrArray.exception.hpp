#ifndef HPP_APTRARRAYEXCEPTION
#define HPP_APTRARRAYEXCEPTION
#include <alliance.exception.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>

class APtrArrayException : public AllianceException {
private:
	u64int index;
public:
	APtrArrayException();
	APtrArrayException(std::string text);
	APtrArrayException(std::string text, u64int i);
	APtrArrayException(AllianceException & e);
	APtrArrayException(AllianceException & e, std::string text);
	APtrArrayException(AllianceException & e, std::string text, u64int i);
	APtrArrayException(const APtrArrayException & obj);
	u64int get_index();
	~APtrArrayException();
};

#endif

