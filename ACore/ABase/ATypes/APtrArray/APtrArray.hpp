#ifndef HPP_APTRARRAY
#define HPP_APTRARRAY
#include <alliance.config.hpp>
#include <cstddef>
#include <iostream>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include "APtrArray.exception.hpp"

#ifndef HPP_FRIEND_APTRARRAYWEAK
#define HPP_FRIEND_APTRARRAYWEAK
template <typename T>
class APtrArrayWeak;
#endif

template <typename T>
class APtrArray {
	friend APtrArrayWeak<T>;
protected:
	T 	   * pointer = NULL;
	u64int * pointer_count = NULL;
	u64int   pointer_length = 0;
public:
	APtrArray();
	APtrArray(u64int);
	APtrArray(const APtrArray &);
	u64int length() const;
	void remove();
	void reset();

	bool is_unique() const;
	bool is_empty() const;
	const T * at() const;
	
	APtrArray clone();
	APtrArray & operator=(APtrArray);
	
	T& operator*();
	T& operator[](u64int);
	T* operator->();
	
	bool operator==(APtrArray &);
	bool operator!=(APtrArray &);
	bool operator <(APtrArray &);
	bool operator >(APtrArray &);
	bool operator>=(APtrArray &);
	bool operator<=(APtrArray &);
	
	operator bool();

	template<typename L, typename R>
	friend APtrArray<R> aptr_cast(APtrArray<L> & obj);
	static APtrArray<T> make(u64int);

	~APtrArray();
};

template <typename T> 
APtrArray<T>::APtrArray() {
	pointer = NULL;
	pointer_count = NULL;
	pointer_length = 0;
}

template <typename T> 
APtrArray<T>::APtrArray(u64int size) {
	if (size == 0) {
		throw APtrArrayException("APtrArray(u64int size) : Failed create new array with length 0");
	}
	pointer = new T[size];
	pointer_count = new u64int;
	*pointer_count = 1;
	pointer_length = size;
}

template <typename T> 
APtrArray<T>::APtrArray(const APtrArray & sptr) {
	if (sptr.pointer == NULL) {
		pointer = NULL;
		pointer_count = NULL;
		pointer_length = 0;
		return;
	}
	pointer = sptr.pointer;
	pointer_count = sptr.pointer_count;
	pointer_length = sptr.pointer_length;
	++(*pointer_count);
}

template <typename T> 
u64int APtrArray<T>::length() const {
	return pointer_length;
}

template <typename T> 
void APtrArray<T>::remove() {
	if (pointer_count == NULL) {
		return;
	}
	--(*pointer_count);
	if (*pointer_count == 0) {
		delete [] pointer;
		delete pointer_count;
		pointer = NULL;
		pointer_length = 0;
		pointer_count = NULL;
	}
}

template <typename T> 
void APtrArray<T>::reset() {
	remove();
	if (pointer) {
		pointer = NULL;
		pointer_length = 0;
		pointer_count = NULL;
	}
}

template <typename T> 
bool APtrArray<T>::is_unique() const {
	return (pointer_count && *pointer_count == 1);
}

template <typename T> 
bool APtrArray<T>::is_empty() const {
	return pointer == NULL;
}

template <typename T> 
const T * APtrArray<T>::at() const {
	return pointer;
}

template <typename T> 
APtrArray<T> APtrArray<T>::clone() {
	APtrArray<T> res;
	if (!is_empty()) {
		res = APtrArray::make(pointer_length);
		for (u64int i = 0; i < pointer_length; i += 1) {
			res[i] = pointer[i];
		}
	}
	return res;
}

template <typename T> 
APtrArray<T>& APtrArray<T>::operator=(APtrArray<T> sptr) {
	if (this != &sptr) {
		if (pointer_count && *pointer_count > 0) {
			remove();
		}
		pointer = sptr.pointer;
		pointer_count = sptr.pointer_count;
		pointer_length = sptr.pointer_length;
		if (pointer_count) {
			++(*pointer_count);
		}
	}
	return *this;
}

template <typename T> 
T& APtrArray<T>::operator*() {
	if (!pointer) {
		throw APtrArrayException("operator*() : This object is NULL");
	}
	return *pointer;
}

template <typename T>
T& APtrArray<T>::operator[](u64int p) {
	if (pointer) {
		if (p >= 0 && p < pointer_length) {
			return pointer[p];		
		} else {
			throw APtrArrayException("operator[] : Failed get object by index in array", p);
		}
	}
	throw APtrArrayException("operator[](u64int) : This object is NULL");
}

template <typename T> 
T * APtrArray<T>::operator->() {
	if (!pointer) {
		throw APtrArrayException("operator->() : This object is NULL");
	}
	return pointer;
}


template <typename T> 
bool APtrArray<T>::operator==(APtrArray<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArray<T>::operator!=(APtrArray<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArray<T>::operator<(APtrArray<T> & sptr) {
	if(pointer < sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArray<T>::operator>(APtrArray<T> & sptr) {
	if(pointer > sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArray<T>::operator<=(APtrArray<T> & sptr) {
	if(pointer <= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArray<T>::operator>=(APtrArray<T> & sptr) {
	if(pointer >= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
APtrArray<T>::operator bool() {
	return !is_empty();
}

template <typename T> 
APtrArray<T> APtrArray<T>::make(u64int size) {
	if (size == 0) {
		throw APtrArrayException("make(u64int size) : Failed create new array with length 0");
	}
	APtrArray<T> res;
	res.pointer = new T[size];
	res.pointer_count = new u64int;
	*(res.pointer_count) = 1;
	res.pointer_length = size;
	return res;
}


template<typename L, typename R>
APtrArray<R> aptr_cast(APtrArray<L> & obj) {
	APtrArray<R> res;
	res.pointer = reinterpret_cast<R*>(obj.pointer);
	res.pointer_count  = obj.pointer_count;
	res.pointer_length = obj.pointer_length * (sizeof(L)/sizeof(R));
	if(res.pointer_count) {
		++(*res.pointer_count);
	}
	return res;
}

template <typename T> 
APtrArray<T>::~APtrArray() {
	remove();
}

#endif