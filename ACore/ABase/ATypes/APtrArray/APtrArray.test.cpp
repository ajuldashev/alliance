#include "APtrArray.hpp"

#ifdef TEST_APTRARRAY
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
      APtrArray<u64int> ptr(1);
      ptr[0] = 0x0102030405060708;

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ptr.length() == 1, 1)

      APtrArray<u8int> ptr2 = aptr_cast<u64int, u8int>(ptr);

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ptr2.length() == 8, 1)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(aptrarray)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(APTRARRAY)

#endif

