#include "APtrArray.hpp"

#ifdef TEST_APTRARRAY
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aptrarray(bool is_soft) {
	#ifdef TEST_APTRARRAYWEAK
	if (!test_aptrarrayweak(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APTRARRAYWEAK );
		return is_soft;
	}
	#endif
	return true;
}
#endif
