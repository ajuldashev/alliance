#include "APtrArrayWeak.hpp"

#ifdef DEBUG_APTRARRAYWEAK
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aptrarrayweak() {
    APtrArrayWeak<s32int> ptr;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_APTRARRAYWEAK );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_aptrarrayweak());
            AIO::writeln("1) help");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_aptrarrayweak(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_APTRARRAYWEAK );
    AIO::write_div_line();
    return false;
}
#endif

