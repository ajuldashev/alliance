#include "APtrArrayWeak.exception.hpp"

APtrArrayWeakException::APtrArrayWeakException() : AllianceException() {
	index = 0;
	exception = "Alliance-Exception-APtrArrayWeak";
}

APtrArrayWeakException::APtrArrayWeakException(std::string text) : AllianceException(text) {
	index = 0;
	add_text("Alliance-Exception-APtrArrayWeak");
}

APtrArrayWeakException::APtrArrayWeakException(std::string text, u64int i) {
	index = i;
	text += std::string(" | index : ") + std::to_string(index);
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArrayWeak");
}

APtrArrayWeakException::APtrArrayWeakException(AllianceException & e) : AllianceException(e) {
	index = 0;
	add_text("Alliance-Exception-APtrArrayWeak");
}

APtrArrayWeakException::APtrArrayWeakException(AllianceException & e, std::string text) : AllianceException(e) {
	index = 0;
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArrayWeak");
}

APtrArrayWeakException::APtrArrayWeakException(AllianceException & e, std::string text, u64int i) : AllianceException(e) {
	index = i;
	text += std::string(" | index : ") + std::to_string(index);
	add_text(" -- " + text);
	add_text("Alliance-Exception-APtrArrayWeak");
}

APtrArrayWeakException::APtrArrayWeakException(const APtrArrayWeakException & obj) : AllianceException(obj) {
	index = 0;
}

u64int APtrArrayWeakException::get_index() {
	return index;
}

APtrArrayWeakException::~APtrArrayWeakException() {

}

