#ifndef HPP_APTRARRAYWEAKEXCEPTION
#define HPP_APTRARRAYWEAKEXCEPTION
#include <alliance.exception.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>

class APtrArrayWeakException : public AllianceException {
private:
	u64int index;
public:
	APtrArrayWeakException();
	APtrArrayWeakException(std::string text);
	APtrArrayWeakException(std::string text, u64int i);
	APtrArrayWeakException(AllianceException & e);
	APtrArrayWeakException(AllianceException & e, std::string text);
	APtrArrayWeakException(AllianceException & e, std::string text, u64int i);
	APtrArrayWeakException(const APtrArrayWeakException & obj);
	u64int get_index();
	~APtrArrayWeakException();
};

#endif

