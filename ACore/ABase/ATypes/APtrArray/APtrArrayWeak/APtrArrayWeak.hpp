#ifndef HPP_APTRARRAYWEAK
#define HPP_APTRARRAYWEAK
#include <alliance.config.hpp>
#include <cstddef>
#include <iostream>
#include <ACore/ABase/ATypes/APtrArray/APtrArray.hpp>
#include "APtrArrayWeak.exception.hpp"

template <typename T>
class APtrArrayWeak {
protected:
	T 	   * pointer = NULL;
	u64int   pointer_length = 0;
public:
	APtrArrayWeak();
	APtrArrayWeak(T *, u64int);
	APtrArrayWeak(APtrArray<T> &);
	APtrArrayWeak(const APtrArrayWeak &);
	u64int length() const;
	void reset();

	bool is_empty() const;
	const T * at() const;
	
	APtrArrayWeak & operator=(APtrArrayWeak);
	
	T& operator*();
	T& operator[](u64int);
	T* operator->();
	
	bool operator==(APtrArrayWeak &);
	bool operator!=(APtrArrayWeak &);
	bool operator <(APtrArrayWeak &);
	bool operator >(APtrArrayWeak &);
	bool operator>=(APtrArrayWeak &);
	bool operator<=(APtrArrayWeak &);
	
	operator bool();

	template<typename L, typename R>
	friend APtrArrayWeak<R> aptr_cast(APtrArrayWeak<L> & obj);

	~APtrArrayWeak();
};

template <typename T> 
APtrArrayWeak<T>::APtrArrayWeak() {
	pointer = NULL;
	pointer_length = 0;
}

template <typename T> 
APtrArrayWeak<T>::APtrArrayWeak(T * pointer, u64int pointer_length) {
	this->pointer = pointer;
	this->pointer_length = pointer_length;
}

template <typename T> 
APtrArrayWeak<T>::APtrArrayWeak(APtrArray<T> & sptr) {
	pointer = sptr.pointer;
	pointer_length = sptr.pointer_length;
}

template <typename T> 
APtrArrayWeak<T>::APtrArrayWeak(const APtrArrayWeak & sptr) {
	pointer = sptr.pointer;
	pointer_length = sptr.pointer_length;
}

template <typename T> 
u64int APtrArrayWeak<T>::length() const {
	return pointer_length;
}

template <typename T> 
void APtrArrayWeak<T>::reset() {
	pointer = NULL;
	pointer_length = 0;
}

template <typename T> 
bool APtrArrayWeak<T>::is_empty() const {
	return pointer == NULL;
}

template <typename T> 
const T * APtrArrayWeak<T>::at() const {
	return pointer;
}

template <typename T> 
APtrArrayWeak<T>& APtrArrayWeak<T>::operator=(APtrArrayWeak<T> sptr) {
	if (this != &sptr) {
		pointer = sptr.pointer;
		pointer_length = sptr.pointer_length;
	}
	return *this;
}

template <typename T> 
T& APtrArrayWeak<T>::operator*() {
	if (!pointer) {
		throw APtrArrayWeakException("operator*() : This object is NULL");
	}
	return *pointer;
}

template <typename T>
T& APtrArrayWeak<T>::operator[](u64int p) {
	if (pointer) {
		if (p >= 0 && p < pointer_length) {
			return pointer[p];		
		} else {
			throw APtrArrayWeakException("operator[] : Failed get object by index in array", p);
		}
	}
	throw APtrArrayWeakException("operator[](u64int) : This object is NULL");
}

template <typename T> 
T * APtrArrayWeak<T>::operator->() {
	if (!pointer) {
		throw APtrArrayWeakException("operator->() : This object is NULL");
	}
	return pointer;
}


template <typename T> 
bool APtrArrayWeak<T>::operator==(APtrArrayWeak<T> & sptr) {
	if(pointer == sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArrayWeak<T>::operator!=(APtrArrayWeak<T> & sptr) {
	if(pointer != sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArrayWeak<T>::operator<(APtrArrayWeak<T> & sptr) {
	if(pointer < sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArrayWeak<T>::operator>(APtrArrayWeak<T> & sptr) {
	if(pointer > sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArrayWeak<T>::operator<=(APtrArrayWeak<T> & sptr) {
	if(pointer <= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
bool APtrArrayWeak<T>::operator>=(APtrArrayWeak<T> & sptr) {
	if(pointer >= sptr.pointer) {
		return true;
	}
	return false;
}

template <typename T> 
APtrArrayWeak<T>::operator bool() {
	return !is_empty();
}

template<typename L, typename R>
APtrArrayWeak<R> aptr_cast(APtrArrayWeak<L> & obj) {
	APtrArrayWeak<R> res;
	res.pointer = reinterpret_cast<R*>(obj.pointer);
	res.pointer_length = obj.pointer_length * (sizeof(L)/sizeof(R));
	return res;
}

template <typename T> 
APtrArrayWeak<T>::~APtrArrayWeak() {

}

#endif