#include "AString.hpp"
#include "AString.test.cpp"
#include "AString.test.node.cpp"
#include "AString.debug.cpp"
#include "AString.debug.node.cpp"
#include "AString.exception.cpp"


AString::AString() {

}

AString::AString(char str) {
   string.insert(str, 0);
}

AString::AString(char * str) {
    s64int l = length(str);
    if (l == 0) {
        return;
    }
    string = AStringBase<char>::make(l);
    for (s64int i = 0; i < l; i += 1) {
        string[i] = str[i];
    }
}

AString::AString(const char * str) {
    s64int l = length(str);
    if (l == 0) {
        return;
    }
    string = AStringBase<char>::make(l);
    for (s64int i = 0; i < l; i += 1) {
        string[i] = str[i];
    }
}

AString::AString(const AString & str) {
    string = str.string;
}

AString AString::make(s64int size) {
    try {
        AString res;
        res.string = AStringBase<char>::make(size);
        return res;
    } catch (AllianceException & e) {
        throw AStringException(e, "make(s64int size)");
    }
}

s64int AString::length() {
    return string.length();
}

s64int AString::length(char * str) {
    if (str == NULL) {
        return 0;
    }
    s64int res = 0;
    for (s64int i = 0; str[i]; i += 1) {
        ++res;
    }
    return res;
}

s64int AString::length(char const * str) {
    if (str == NULL) {
        return 0;
    }
    s64int res = 0;
    for (s64int i = 0; str[i]; i += 1) {
        ++res;
    }
    return res;
}

void AString::copy(char * str) {
    if (str == NULL) {
        return;
    }
    string.clear();
    s64int len = length(str);
    if (len == 0) {
        return;
    }
    AStringBase<char> string_base = AStringBase<char>::make(len);
    for (s64int i = 0; i < string_base.length(); i += 1){
        string_base[i] = str[i];
    }
    string = string_base;
}

void AString::copy(char const * str) {
    if (str == NULL) {
        return;
    }
    string.clear();
    s64int len = length(str);
    if (len == 0) {
        return;
    }
    AStringBase<char> string_base = AStringBase<char>::make(len);
    for (s64int i = 0; i < string_base.length(); i += 1){
        string_base[i] = str[i];
    }
    string = string_base;
}

void AString::copy(AString str) {
    string = str.string.copy();
}

AString AString::get_copy() {
    AString res;
    res.string = string.copy();
    return res;
}

AString AString::get_copy(const char * str) {
    AString res(str);
    return res;
}

bool AString::operator <(AString str) {
    if (str.is_empty() && string.is_empty()) {
        return false;
    }
    if (string.is_empty()) {
        return true;
    }
    s64int len = (string.length() < str.length())? string.length() : str.length();
    bool f = false;
    s64int i = 0;
    for (i = 0; i < len; i += 1) {
        if (string[i] != str[i]) {
            f = true;
            break;
        }
    }
    if (f) {
        return string[i] < str[i];
    }
    if (!f && string.length() > str.length()) {
        return false;
    }
    if (!f && string.length() < str.length()) {
        return true;
    }
    return false;
}

bool AString::operator >(AString str) {
    if (str.is_empty() && string.is_empty()) {
        return false;
    }
    if (str.is_empty()) {
        return true;
    }
    s64int len = (string.length() < str.length())? string.length() : str.length();
    bool f = false;
    s64int i = 0;
    for (i = 0; i < len; i += 1) {
        if (string[i] != str[i]) {
            f = true;
            break;
        }
    }
    if (f) {
        return string[i] > str[i];
    }
    if (!f && string.length() < str.length()) {
        return false;
    }
    if (!f && string.length() > str.length()) {
        return true;
    }
    return false;
}

bool AString::operator<=(AString str) {
    return !(*this > str);
}

bool AString::operator>=(AString str) {
    return !(*this < str);
}

bool AString::operator!=(AString str) {
    return !(this->operator==(str));
}

bool AString::operator==(AString str) {
    if (str.is_empty() && string.is_empty()) {
        return true;
    }
    if (str.is_empty() || string.is_empty()) {
        return false;
    }
    if (string.length() !=  str.length()){
        return false;
    }
    for (s64int i = 0; i < length(); i += 1) {
        if (string[i] != str[i]) {
            return false;
        }
    }
    return true;
}

AString & AString::operator+=(AString str) {
    *this = *this + str;
    return *this;
}

AString & AString::operator=(AString str) {
    string = str.string.copy();
    return *this;
}

AString & AString::operator=(const char * str) {
    copy(str);
    return *this;
}

AString AString::to_cstring() {
    AString res(*this);
    res.string.insert('\0', res.length());
    return res;
}

const char * AString::const_data() const {
    return string.const_data();
}

s64int AString::find(AString str,  s64int pos) {
    return string.find(str.string, pos);
}

AString AString::get(s64int pos, s64int len = 1) {
    try {
        AString res;
        AStringBase<char> string_base = string.get(pos, len);
        res.string = string_base;
        return res;
    } catch (AllianceException & e) {
        throw AStringException(e, "get(s64int pos, s64int len)");
    }
}

AString AString::sub(s64int pos, s64int len = 1) {
    try {
        AString res;
        AStringBase<char> string_base = string.sub(pos, len);
        res.string = string_base;
        return res;
    } catch (AllianceException & e) {
        throw AStringException(e, "sub(s64int pos, s64int len)");
    }
}

void AString::remove(s64int pos, s64int len = 1) {
    try {
        string.remove(pos, len);
    } catch (AllianceException & e) {
        throw AStringException(e, "remove(s64int pos, s64int len)");
    }
}

AString AString::insert(char ch, s64int pos) {
    string.insert(ch, pos);
    return *this;
}

AString AString::insert(const char * cstr, s64int pos) {
    AString str(cstr);
    string.insert(str.string, pos);
    return *this;
}

AString AString::insert(AString str, s64int pos) {
    string.insert(str.string, pos);
    return *this;
}

AString AString::operator+(char ch) {
    AString res(*this);
    res.insert(ch, string.length());
    return res;
}

AString AString::operator+(char const * str) {
    AString res(*this);
    res.insert(str, string.length());
    return res;
}

AString AString::operator+(AString str) {
    AString res(*this);
    res.insert(str, string.length());
    return res;
}

AString operator+(char const * cstr, AString str) {
    AString nstr(cstr);
    return nstr + str;
}

char AString::to_lower(char symb) {
    if (symb >= 'A' && symb <= 'Z') {
        return symb + 'a' - 'A';
    }
    return symb;
}

char AString::to_upper(char symb) {
    if (symb >= 'a' && symb <= 'z') {
        return symb + 'A' - 'a';
    }
    return symb;
}

AString AString::to_lower(const char * str) {
    AString res(get_copy(str));
    for (s32int i = 0; i < res.length(); i++) {
        if (res[i] >= 'A' && res[i] <= 'Z') {
            res[i] = res[i] + 'a' - 'A';
        }    
    }
    return res;
}

AString AString::to_upper(const char * str) {
    AString res(get_copy(str));
    for (s32int i = 0; i < res.length(); i++) {
        if (res[i] >= 'a' && res[i] <= 'z') {
            res[i] = res[i] + 'A' - 'a';
        }    
    }
    return res;
}

AString AString::to_lower(AString str) {
    return to_lower(str.to_cstring().const_data());
}

AString AString::to_upper(AString str) {
    return to_upper(str.to_cstring().const_data());
}

AString AString::to_lower() {
    return to_lower(to_cstring().const_data());
}

AString AString::to_upper() {
    return to_upper(to_cstring().const_data());
}

char & AString::operator[](s64int p) {
    try {
        return string[p];
    }
    catch (AllianceException & e) {
        throw AStringException(e, "operator[](s64int p)");
    }
}

bool AString::is_empty() {
    return string.is_empty();
}

AString::~AString() {

}