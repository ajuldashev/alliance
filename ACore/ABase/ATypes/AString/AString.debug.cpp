#include "AString.hpp"

#ifdef DEBUG_ASTRING
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_astring() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRING );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astring());
            AIO::writeln(" 1) help");
            AIO::writeln(" 3) cmp");
            AIO::writeln(" 4) assigned");
            AIO::writeln(" 5) find");
            AIO::writeln(" 6) get");
            AIO::writeln(" 7) sub");
            AIO::writeln(" 8) remove");
            AIO::writeln(" 9) insert_symbol");
            AIO::writeln("10) insert_str");
            AIO::writeln("11) to_lower_symb");
            AIO::writeln("12) to_upper_symb");
            AIO::writeln("13) to_lower_char");
            AIO::writeln("14) to_upper_char");
            AIO::writeln("15) to_lower_AString");
            AIO::writeln("16) to_upper_AString");
            AIO::writeln("17) to_lower_num");
            AIO::writeln("18) to_upper_num");
            AIO::writeln("19) []");
            AIO::writeln("20) >");
            AIO::writeln("21) <");
            AIO::writeln("22) ==");
            AIO::writeln("23) !=");
            AIO::writeln("24) >=");
            AIO::writeln("25) <=");
            AIO::writeln(" 0) stop");
            AIO::writeln(" e) exit");	
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astring(com)) {
			return true;
		}
        if (com == "cmp") {
            AString op;
            op = AIO::get_word();
            AString aword_1(AIO::get_word());
            AString aword_2(AIO::get_word());
            if (op == ">") {
                AIO::writeln(((aword_1 > aword_2)? "true" : "false"));
            }
            if (op == "<") {
                AIO::writeln(((aword_1 < aword_2)? "true" : "false"));
            }
            if (op == ">=") {
                AIO::writeln(((aword_1 >= aword_2)? "true" : "false"));
            }
            if (op == "<=") {
                AIO::writeln(((aword_1 <= aword_2)? "true" : "false"));
            }
            if (op == "==") {
                AIO::writeln(((aword_1 == aword_2)? "true" : "false"));
            }
            if (op == "!=") {
                AIO::writeln(((aword_1 != aword_2)? "true" : "false"));
            }
        }
        if (com == "assigned") {
            AString aword_1 = AIO::get_word();
            AIO::write(aword_1.to_cstring().const_data());
            AIO::write("  ");
            AString aword_2;
            aword_2 = aword_1;
            AIO::writeln(aword_2.to_cstring().const_data());
        }
        if (com == "find") {
            AString aword_1 = AIO::get_word();
            AString aword_2 = AIO::get_word();
            AIO::writeln(aword_1.find(aword_2, AIO::get_s32int()));
        }
        if (com == "sub") {
            AString aword_1 = AIO::get_word();
            AIO::writeln(aword_1.sub(AIO::get_s32int(), AIO::get_s32int()).to_cstring().const_data());
        }
        if (com == "insert") {
            AString aword_1 = AIO::get_word();
            AString aword_2 = AIO::get_word();
            aword_1.insert(aword_2, AIO::get_s32int());
            AIO::writeln(aword_1.to_cstring().const_data());
        }
        if (com == "to_lower_symb") {
            AString aword_1;
            AIO::writeln(aword_1.to_lower(AIO::get_word()));
        }
        if (com == "to_upper_symb") {
            AString aword_1;
            AIO::writeln(aword_1.to_upper(AIO::get_word()));
        }
        if (com == "to_lower_char") {
            AString aword_1;
            AIO::writeln((aword_1.to_lower(AIO::get_word())).to_cstring().const_data());
        }
        if (com == "to_upper_char") {
            AString aword_1;
            AIO::writeln((aword_1.to_upper(AIO::get_word())).to_cstring().const_data());
        }
        if (com == "to_lower_AString") {
            AString aword = AIO::get_word();
            AString aword_1;
            AIO::writeln((aword_1.to_lower(aword)).to_cstring().const_data());
        }
        if (com == "to_upper_AString") {
            AString aword = AIO::get_word();
            AString aword_1;
            AIO::writeln((aword_1.to_upper(aword)).to_cstring().const_data());
        }
        if (com == "to_lower") {
            AString aword_1 = AIO::get_word();
            AIO::writeln((aword_1.to_lower()).to_cstring().const_data());
        }
        if (com == "to_upper") {
            AString aword_1 = AIO::get_word();
            AIO::writeln((aword_1.to_upper()).to_cstring().const_data());
        }
        if (com == "[]") {
            AString aword = AIO::get_word();
            AIO::writeln(aword[AIO::get_s32int()]);
        }
        if (com == ">") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 > word_2);
        }
        if (com == "<") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 < word_2);
        }
        if (com == "==") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 == word_2);
        }
        if (com == "!=") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 != word_2);
        }
        if (com == ">=") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 >= word_2);
        }
        if (com == "<=") {
            AString word_1 = AIO::get_word();
            AString word_2 = AIO::get_word();
            AIO::writeln(word_1 <= word_2);
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRING );
    AIO::write_div_line();
    return false;
}
#endif

