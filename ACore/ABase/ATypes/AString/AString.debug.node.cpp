#include "AString.hpp"

#ifdef DEBUG_ASTRING
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_astring(AString com) {
	#ifdef DEBUG_ASTRINGBASE
	if (com == "astringbase") {
		if (debug_astringbase()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_astring() {
	AString text;
	#ifdef DEBUG_ASTRINGBASE
	text += "d) astringbase\n";
	#endif
	return text;
}

#endif
