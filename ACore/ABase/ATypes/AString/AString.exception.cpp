#include "AString.exception.hpp"

AStringException::AStringException() : AllianceException() {
	exception = "Alliance-Exception-AString";
}

AStringException::AStringException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AString");
}

AStringException::AStringException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AString");
}

AStringException::AStringException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AString");
}

AStringException::AStringException(const AStringException & obj) : AllianceException(obj) {
	
}

AStringException::~AStringException() {

}

