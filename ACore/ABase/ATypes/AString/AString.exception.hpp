#ifndef HPP_ASTRINGEXCEPTION
#define HPP_ASTRINGEXCEPTION
#include <alliance.exception.hpp>

class AStringException : public AllianceException {
public:
	AStringException();
	AStringException(std::string text);
	AStringException(AllianceException & e);
	AStringException(AllianceException & e, std::string text);
	AStringException(const AStringException & obj);
	~AStringException();
};

#endif

