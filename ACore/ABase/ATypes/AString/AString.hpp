#ifndef HPP_ASTRING
#define HPP_ASTRING
#include "AStringBase/AStringBase.hpp"
#include "AString.exception.hpp"

class AString {
private:
    AStringBase<char> string;
public:
    AString();
    AString(char);
    AString(char *);
    AString(const char *);
    AString(const AString &);
    
    static AString make(s64int);

    s64int length();
    static s64int length(char *);
    static s64int length(char const *);

    void copy(char *);
    void copy(char const *);
    void copy(AString);

    AString get_copy();
    static AString get_copy(const char *);

    bool operator <(AString);
    bool operator >(AString);
    bool operator<=(AString);
    bool operator>=(AString);
    bool operator!=(AString);
    bool operator==(AString);

    AString & operator+=(AString);
    AString & operator=(AString);
    AString & operator=(const char *);

    AString to_cstring();
    const char * const_data() const;

    s64int find(AString, s64int pos = 0);

    AString get(s64int, s64int);
    AString sub(s64int, s64int);
    void remove(s64int, s64int);

    AString insert(char, s64int);
    AString insert(const char *, s64int);
    AString insert(AString, s64int);

    AString operator+(char);
    AString operator+(char const *);
    AString operator+(AString);


    friend AString operator+(char const *, AString);

    static char to_lower(char);
    static char to_upper(char);
    static AString to_lower(const char *);
    static AString to_upper(const char *);
    static AString to_lower(AString);
    static AString to_upper(AString);
    AString to_lower();
    AString to_upper();

    char & operator[](s64int);

    bool is_empty();

    ~AString();
};

#endif