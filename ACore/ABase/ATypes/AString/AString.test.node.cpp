#include "AString.hpp"

#ifdef TEST_ASTRING
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_astring(bool is_soft) {
	#ifdef TEST_ASTRINGBASE
	if (!test_astringbase(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRINGBASE );
		return is_soft;
	}
	#endif
	return true;
}
#endif
