#include "AStringBase.hpp"

#ifdef DEBUG_ASTRINGBASE
#include <ACore/ABase/AIO/AIO.hpp>

static s64int debug_length(const char * string){
    s64int length = 0;
    if (string) {
        while (string && *string) {
            length += 1;
            string += 1;
        }
    }
    return length;
}

static void debug_set(AStringBase<char> & string_base, const char * string) {
    s64int length = debug_length(string);
    AStringBase<char> temp = AStringBase<char>::make(length);
    for (s64int i = 0; i < length; i += 1) {
        temp.at(i) = string[i];
    }
    string_base.copy(temp);
}

static void debug_print(AStringBase<char> debug_string) {
    if (debug_string.length() > 0) {
        const char * string = debug_string.const_data();
        AIO::write(string);
    }
}

static AStringBase<char> debug_base() {
    AString str = AIO::get_word();
    AStringBase<char> string_char;
    debug_set(string_char, str.to_cstring().const_data());
    return string_char;
}

bool debug_astringbase() {
    AStringBase<char> str;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ASTRINGBASE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_astringbase());
            AIO::writeln("1) help");
            AIO::writeln("2) test_read");
            AIO::writeln("3) []");
            AIO::writeln("0) stop");
            AIO::writeln("e) exit");
        }
        if (com == "test_read") {
            str = debug_base();
            debug_print(str);
            AIO::writeln("");
        }
        if (com == "exit") {
            return true;
        }
        if (debug_node_astringbase(com)) {
			return true;
		}
        if (com == "[]") {
            try {
                str[AIO::get_s32int()];
            }
            catch (AStringBaseException e) {
                AIO::writeln_error(e.get_text().c_str());
            }
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ASTRINGBASE );
    AIO::write_div_line();
    return false;
}
#endif

