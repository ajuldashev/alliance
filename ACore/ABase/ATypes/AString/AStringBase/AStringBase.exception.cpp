#include "AStringBase.exception.hpp"

AStringBaseException::AStringBaseException() : AllianceException() {
	exception = "Alliance-Exception-AStringBase";
}

AStringBaseException::AStringBaseException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AStringBase");
}

AStringBaseException::AStringBaseException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AStringBase");
}

AStringBaseException::AStringBaseException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AStringBase");
}

AStringBaseException::AStringBaseException(const AStringBaseException & obj) : AllianceException(obj) {
	
}

AStringBaseException::~AStringBaseException() {

}

