#ifndef HPP_ASTRINGBASEEXCEPTION
#define HPP_ASTRINGBASEEXCEPTION
#include <alliance.exception.hpp>

class AStringBaseException : public AllianceException {
public:
	AStringBaseException();
	AStringBaseException(std::string text);
	AStringBaseException(AllianceException & e);
	AStringBaseException(AllianceException & e, std::string text);
	AStringBaseException(const AStringBaseException & obj);
	~AStringBaseException();
};

#endif

