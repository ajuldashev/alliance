#ifndef HPP_ASTRINGBASE
#define HPP_ASTRINGBASE
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/APtrArray/APtrArray.hpp>
#include "AStringBase.exception.hpp"


/** <doc-template-class-info>
 *  Базовый шаблонный класс строка
 *  Реализует базовые алгоритмы для работы со строками
 *  Symbol - Тип символа с которым ведется работа
 *  <doc-info>
 *  На одну и туже строку могут ссылаться несколько объектов,
 *  но при попытке изменить ее, сначало создается новая строка,
 *  и меняется ее содержимое, а исходная не изменяется !
 */
template<typename Symbol>
class AStringBase {
private:
    /** <doc-private-field>
     *  Указатель на массив символов, который является строкой
     */
    APtrArray<Symbol> string;
public:
    /** <doc-public-method>
     *  Конструктор по умолчанию
     */
    AStringBase();

    /** <doc-public-method>
     *  Конструктор с параметром
     *  arg 1 : symbol
     *  Создается сткока из одного символа и ему присваивается значение symbol
     */
    AStringBase(Symbol symbol);

    /** <doc-public-method>
     *  Конструктор копирования
     *  arg 1 : string_base
     *  Копируется указатель на строку astringbase.string
     *  при этом сама строка не копируеться
     */
    AStringBase(const AStringBase & string_base);

    /** <doc-static-public-method>
     *  Статический метод для создания строки
     *  arg 1 : length - длина с которой будет создана новая строка в символах
     *  return возвращает строку длиной length
     *  <doc-warning>
     *  Строка которую возращает метод, имеет произвольное содержимое !
     *  <doc-error>
     *  При попытке создать строку length == 0, выкинет исключение
     */
    static AStringBase make(s64int length);

    /** <doc-public-method>
     *  Метод создания копии
     *  return возвращает КОПИЮ текущей строки
     */
    AStringBase copy();

    /** <doc-public-method>
     *  Метод копии другой строки 
     *  arg 1 : string_base - строка которую будет копировать
     *  Метод копирует саму строку а не указатель !
     */
    void copy(AStringBase & strign_base);

    /** <doc-public-method>
     *  Метод получения длины строки 
     *  return длина строки в символах, которая храниться в указателе
     */
    s64int length() const;

    /** <doc-public-method>
     *  Метод получения ссылки на символ по позиции 
     *  arg 1 : pos - позиция сомвола в строке
     *  return ссылка на символ
     *  При получении символа по ссылке, мы можем менять исходный символ в строке
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    Symbol &at(s64int pos);

    /** <doc-public-method>
     *  Метод получения символа по позиции 
     *  arg 1 : pos - позиция символа в строке
     *  return символ
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    Symbol get(s64int pos);

    /** <doc-public-method>
     *  Метод получения подстроки по позиции и длине
     *  arg 1 : pos - позиция подстроки в строке
     *  arg 2 : length - длина подстроки
     *  return подстрока
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    AStringBase get(s64int pos, s64int length);

    /** <doc-public-method>
     *  Метод получения подстроки по позиции и длине и удаления ее из исходной строки
     *  arg 1 : pos - позиция подстроки в строке
     *  arg 2 : length - длина подстроки
     *  return подстрока
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    AStringBase sub(s64int pos, s64int length);

    /** <doc-public-method>
     *  Метод удаления подстроки по позиции и длине из исходной
     *  arg 1 : pos - позиция подстроки в строке
     *  arg 2 : length - длина подстроки
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    void remove(s64int pos, s64int length);

    /** <doc-public-method>
     *  Метод символа в строку
     *  arg 1 : symbol - символ который нужно вставить
     *  arg 2 : pos - позиция в строке
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    void insert(Symbol symbol, s64int pos);

    /** <doc-public-method>
     *  Метод строки в строку
     *  arg 1 : string_base - строка которую нужно вставить
     *  arg 2 : pos - позиция в строке
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    void insert(AStringBase & string_base, s64int pos);
    
    /** <doc-public-method>
     *  Метод поиск построки в строке
     *  arg 1 : symbol - символ который нужно найти
     *  arg 2 : pos - позиция в строке с котрой начинается поиск
     *  return позиция в строке если найдено совпадение и -1 если не найдено
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    s64int find(Symbol symbol, s64int pos);
    
    /** <doc-public-method>
     *  Метод поиск построки в строке
     *  arg 1 : string_base - строка которую нужно найти
     *  arg 2 : pos - позиция в строке с котрой начинается поиск
     *  return позиция в строке если найдено совпадение и -1 если не найдено
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    s64int find(AStringBase & string_base, s64int pos);

    /** <doc-public-operator>
     *  Оператор получения ссылки на символ по позиции 
     *  arg 1 : pos - позиция сомвола в строке
     *  return ссылка на символ
     *  При получении символа по ссылке, мы можем менять исходный символ в строке
     *  <doc-error>
     *  При выходе позиции за границы строки, выкинет исключение
     */
    Symbol & operator[](s64int pos);

    /** <doc-public-operator>
     *  Оператор присваения 
     *  arg 1 : string_base
     *  return ссылка на себя
     *  Копируется указатель на строку astringbase.string
     *  при этом сама строка не копируеться
     */
    AStringBase & operator=(AStringBase string_base);
    
    /** <doc-public-method>
     *  Метод получние указателя на константные данные  
     *  return указатель на данные
     */
    const Symbol * const_data() const;
    
    /** <doc-public-method>
     *  Метод проверки на уникальность строки  
     *  return true - если на строку указывает ровно один указатель 
     */
    bool is_unique();
    
    /** <doc-public-method>
     *  Метод проверки на пустую строку  
     *  return true - длина строки равно 0 
     */
    bool is_empty();
    
    /** <doc-public-method>
     *  Метод отчистки строки  
     *  Отчищает строку от данных путем сброса указателя
     */
    void clear();

    /** <doc-public-method>
     *  Диструктор  
     *  Вызывает clear
     */
    ~AStringBase();
};

template<typename Symbol>
AStringBase<Symbol>::AStringBase() {

}

template<typename Symbol>
AStringBase<Symbol>::AStringBase(Symbol symbol) {
    string = APtrArray<Symbol>::make(1);
    string[0] = symbol;
}

template<typename Symbol>
AStringBase<Symbol>::AStringBase(const AStringBase & string_base) {
    try {
        string = string_base.string;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "AStringBase(const AStringBase & string_base) : Error create ABaseString");
	}
}

template<typename Symbol>
AStringBase<Symbol> AStringBase<Symbol>::make(s64int length) {
    AStringBase<Symbol> res;
    try {
        res.string = APtrArray<Symbol>::make(length);
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "make(s64int length) : Error make ABaseString");
	}
    return res;
}

template<typename Symbol>
AStringBase<Symbol> AStringBase<Symbol>::copy() {
    AStringBase res;
    res.string = string.clone();
    return res;
}

template<typename Symbol>
void AStringBase<Symbol>::copy(AStringBase<Symbol> & string_base) {
    string = string_base.string.clone();
}

template<typename Symbol>
s64int AStringBase<Symbol>::length() const {
    return string.length();
}

template<typename Symbol>
Symbol & AStringBase<Symbol>::at(s64int pos) {
    try {
        return string[pos];
    } catch (AllianceException & e) {
        throw AStringBaseException("at(s64int pos)");
    }
}

template<typename Symbol>
Symbol AStringBase<Symbol>::get(s64int pos) {
    try {
        return string[pos];
    } catch (AllianceException & e) {
        throw AStringBaseException("get(s64int pos)");
    }
}

template<typename Symbol>
AStringBase<Symbol> AStringBase<Symbol>::get(s64int pos, s64int length) {
    AStringBase<Symbol> res;
    try {
        res = make(length);
        for (s64int i = 0; i < length; i += 1) {
            res.at(i) = string[i + pos];
        }
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "get(s64int pos, s64int length) : Error get AStringBase");
	} 
    return res;
}

template<typename Symbol>
AStringBase<Symbol> AStringBase<Symbol>::sub(s64int pos, s64int length) {
    try {
        AStringBase<Symbol> res = get(pos, length);
        remove(pos, length);
        return res;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "sub(s64int pos, s64int length)");
	}
}

template<typename Symbol>
void AStringBase<Symbol>::remove(s64int pos, s64int length) {
    try {
        if (string.length() - length == 0) {
            clear();
            return;
        }
        APtrArray<Symbol> new_string = APtrArray<Symbol>::make(string.length() - length);
        s64int new_idx = 0;
        s64int idx = 0;
        for (; idx < pos; idx += 1, new_idx += 1) {
            new_string[new_idx] = string[idx];
        }
        idx = pos + length;
        for (; idx < string.length(); idx += 1, new_idx += 1) {
            new_string[new_idx] = string[idx];
        }
        string = new_string;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "remove(s64int pos, s64int length) : Error remove AStringBase");
	}
}

template<typename Symbol>
void AStringBase<Symbol>::insert(Symbol symbol, s64int pos) {
    try {
        APtrArray<Symbol> new_string = APtrArray<Symbol>::make(string.length() + 1);
        for (s64int i = 0; i < pos; i += 1) {
            new_string[i] = string[i];
        }
        new_string[pos] = symbol;
        for (s64int i = pos + 1; i <= string.length(); i += 1) {
            new_string[i] = string[i - 1];
        }
        string = new_string;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "insert(Symbol symbol, s64int pos) : Error insert symbol");
	}
}

template<typename Symbol>
void AStringBase<Symbol>::insert(AStringBase & string_base, s64int pos) {
    try {
        if (string_base.is_empty()) {
            return;
        }
        APtrArray<Symbol> new_string = APtrArray<Symbol>::make(string.length() + string_base.length());
        s64int new_idx = 0;
        s64int idx = 0;
        for (; idx < pos; idx += 1, new_idx += 1) {
            new_string[new_idx] = string[idx];
        }
        for (s64int ids = 0; ids < string_base.length(); ids += 1, new_idx += 1) {
            new_string[new_idx] = string_base[ids];
        }
        for (; idx < string.length(); idx += 1, new_idx += 1) {
            new_string[new_idx] = string[idx];
        }
        string = new_string;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "insert(AStringBase & string_base, s64int pos) : Error insert AStringBase");
	}
}

template<typename Symbol>
s64int AStringBase<Symbol>::find(Symbol symbol, s64int pos) {
    try {
        s64int res = -1;
        for (s64int i = pos; i < string.length(); i += 1) {
            if (string[i] == symbol) {
                res = i;
                break;
            }
        }
        return res;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "find(Symbol symbol, s64int pos)");
	}
}

template<typename Symbol>
s64int AStringBase<Symbol>::find(AStringBase & string_base, s64int pos) {
    try {
        s64int res = -1;
        if (string.length() < string_base.length()) {
            return res;
        }
        s64int length_for_find = string.length() - string_base.length() + 1;
        for (s64int i = pos; i < length_for_find; i += 1) {
            bool found = true;
            for (s64int j = 0; j < string_base.length(); j += 1) {
                if (string[i + j] != string_base[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                res = i;
                break;
            }
        }
        return res;
    } catch (AllianceException & e) {
		throw AStringBaseException(e, "find(Symbol symbol, s64int pos)");
	}
}

template<typename Symbol>
Symbol & AStringBase<Symbol>::operator[](s64int pos) {
    try {
        if (!string.is_unique()) {
            string = string.clone();
        }
        return string[pos];
    } 
    catch (AllianceException & e) {
        throw AStringBaseException(e, "operator[](s64int pos) : Error get symbol by index in operator []");
    }
}

template<typename Symbol>
AStringBase<Symbol> & AStringBase<Symbol>::operator=(AStringBase<Symbol> string_base) {
    string = string_base.string;
    return *this;
}

template<typename Symbol>
const Symbol * AStringBase<Symbol>::const_data() const {
    return string.at();
}

template<typename Symbol>
bool AStringBase<Symbol>::is_unique() {
    return string.is_unique();
}

template<typename Symbol>
bool AStringBase<Symbol>::is_empty() {
    return string.is_empty();
}

template<typename Symbol>
void AStringBase<Symbol>::clear() {
    string.reset();
}

template<typename Symbol>
AStringBase<Symbol>::~AStringBase() {
    clear();
}

#endif