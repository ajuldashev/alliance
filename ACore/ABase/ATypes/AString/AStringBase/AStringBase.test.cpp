#include "AStringBase.hpp"

#ifdef TEST_ASTRINGBASE
#include <ACore/ABase/AIO/AIO.hpp>

static s64int test_char(const char * str) {
   if (!str) {
      return 0;
   } 
   s64int res = 0;
   while (*str) {
      res += 1;
      str += 1;
   }
   return res;
}

static bool test_equal(AStringBase<char> & str1, AStringBase<char> & str2) {
   if (str1.length() == str2.length()) {
      for (s64int i = 0; i < str1.length(); i += 1) {
         if (str1[i] != str2[i]) {
            return false;
         }
      }
      return true;
   }
   return false;
}

static AStringBase<char> test_string(const char * str) {
   AStringBase<char> res;
   if (str) {
      res = AStringBase<char>::make(test_char(str));
      for (s64int i = 0; i < res.length(); i += 1) {
         res[i] = str[i];
      }
   }
   return res;
}

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
      AStringBase<char> str1 = test_string("ASD");
      AStringBase<char> str2 = test_string("ASD");
      AStringBase<char> str3 = test_string("ASE");

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(test_equal(str1, str2), 1)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(!test_equal(str1, str3), 2)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str1.is_unique(), 3)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str2.is_unique(), 4)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str3.is_unique(), 5)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
      AStringBase<char> str;
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_empty(), 1)
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(!str.is_unique(), 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
      AStringBase<char> str('F');

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_empty() == false, 1)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_unique() == true, 2)
      
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.length() == 1, 3)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str[0] == 'F', 4)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
      AStringBase<char> str = test_string("Test Const Constructor Copy");
      
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_empty() == false, 1)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_unique(), 2)

      AStringBase<char> str_copy(str);

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str_copy.is_empty() == false, 3)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_unique() == false, 4)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(test_equal(str, str_copy), 5)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
      AStringBase<char> str = test_string("Test Copy");
      
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_empty() == false, 1)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_unique() == true, 2)
 
      AStringBase<char> str_copy_1 = str.copy();
      
      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(str.is_unique() == true, 3)

      /** <doc-test-info>
		 *
		 */
		TEST_ALERT(test_equal(str, str_copy_1), 4)

      AStringBase<char> str_copy_2;
      str_copy_2.copy(str);

       /** <doc-test-info>
		 *
		 */
		TEST_ALERT(test_equal(str, str_copy_2), 5)

	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(true, 1)
      AStringBase<char> str = test_string("Test at");
      const char * str_source = "Test at";
      for (s64int i = 0; i < str.length(); i += 1) {
         TEST_ALERT(str.at(i) == str_source[i], 1)
      }
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(7)
	{
		/** <doc-test-info>
		 *
		 */
      AStringBase<char> str = test_string("Test get");
      const char * str_source = "Test get";
      for (s64int i = 0; i < str.length(); i += 1) {
         TEST_ALERT(str.get(i) == str_source[i], 1)
      }
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(astringbase)

	TEST_CALL(1)
   TEST_CALL(2)
   TEST_CALL(3)
   TEST_CALL(4)
   TEST_CALL(5)
   TEST_CALL(6)
   TEST_CALL(7)

TEST_FUNCTION_MAIN_END(ASTRINGBASE)

#endif

