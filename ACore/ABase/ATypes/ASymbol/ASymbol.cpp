#include "ASymbol.hpp"
#include "ASymbol.test.cpp"
#include "ASymbol.test.node.cpp"
#include "ASymbol.debug.cpp"
#include "ASymbol.debug.node.cpp"
#include "ASymbol.exception.cpp"

ASymbol::ASymbol() {

}

ASymbol::ASymbol(char symbol) {
    this->symbol = AString(symbol);
}

ASymbol::ASymbol(AString string) {
    try {
        set(string);
    } catch (AllianceException & e) {
        throw ASymbolException(e, "ASymbol(AString string)");
    }
}

ASymbol::ASymbol(const char * cstring) {
    try {
        AString string(cstring);
        set(string);
    } catch (AllianceException & e) {
        throw ASymbolException(e, "ASymbol(const char * cstring)");
    }
}

ASymbol::ASymbol(const ASymbol & symbol) {
    try {
        AString string(symbol.symbol);
        set(string);
    } catch (AllianceException & e) {
        throw ASymbolException(e, "ASymbol(const ASymbol & symbol)");
    }
}

void ASymbol::set(ASymbol symbol) {
    try {
        set(symbol.symbol);
    } catch (AllianceException & e) {
        throw ASymbolException(e, "set(ASymbol symbol)");
    }
}

void ASymbol::set(AString string) {
    try {
        u8int length_symbol = test_length_symbol(string);
        if (length_symbol == 0) {
            return;
        }
        symbol = string.get(0, length_symbol);
    } catch (AllianceException & e) {
        throw ASymbolException(e, "set(AString string)");
    }
}

u64int ASymbol::set(AString string, u64int index) {
    try {
        u64int base_index = fix_index(string, index);
        u8int length_symbol = test_length_symbol(string, base_index);
        if (length_symbol == 0) {
            return base_index;
        }
        symbol = string.get(base_index, length_symbol);
        return base_index;
    } catch(AllianceException & e) {
        throw ASymbolException("set(AString string, u64int index)");
    }
}

bool ASymbol::operator <(ASymbol symbol) {
    return this->symbol < symbol.symbol;
}

bool ASymbol::operator >(ASymbol symbol) {
    return this->symbol > symbol.symbol;
}

bool ASymbol::operator<=(ASymbol symbol) {
    return this->symbol <= symbol.symbol;
}

bool ASymbol::operator>=(ASymbol symbol) {
    return this->symbol >= symbol.symbol;
}

bool ASymbol::operator!=(ASymbol symbol) {
    return this->symbol != symbol.symbol;
}

bool ASymbol::operator==(ASymbol symbol) {
    return this->symbol == symbol.symbol;
}

ASymbol & ASymbol::operator=(ASymbol symbol) {
    try {
        set(symbol);
        return *this;
    } catch (AllianceException & e) {
        throw ASymbolException(e, "operator=(ASymbol symbol)");
    }
}

AString ASymbol::to_string() {
    return symbol;
}

u64int ASymbol::length() {
    return symbol.length();
}

u64int ASymbol::fix_index(AString string, u64int index) {
    try {
        u64int base_index = index;
        while (is_intermediate_byte((u8int)string[base_index]) && base_index > 0) {
            base_index -= 1;
        }
        if (is_intermediate_byte((u8int)string[base_index])) {
            throw ASymbolException("Fail utf-8 intermediate symbol");
        }
        return base_index;
    } catch(AllianceException & e) {
        throw ASymbolException("fix_index(AString string, u64int index)");
    }
}

u8int ASymbol::test_length_symbol(AString string, u64int index) {
    try {
        if (string.length() == 0 ||
            string.length() == index) {
            return 0;
        }
        u8int test_symbol = string[index];
        if ((test_symbol & 0x80) == 0x00) {
            return 1;
        }
        if ((test_symbol & 0xE0) == 0xC0) {
            return 2;
        }
        if ((test_symbol & 0xF0) == 0xE0) {
            return 3;
        }
        if ((test_symbol & 0xF8) == 0xF0) {
            return 4;
        }
        if ((test_symbol & 0xFC) == 0xF8) {
            return 5;
        }
        if ((test_symbol & 0xFE) == 0xFC) {
            return 6;
        }
        return 0;
    } catch (AllianceException & e) {
        throw ASymbolException(e, "test_length_symbol(AString string, u64int index)");
    }
}

bool ASymbol::is_intermediate_byte(u8int byte) {
    return (byte & 0xC0) == 0x80;
}

ASymbol::~ASymbol() {

}
