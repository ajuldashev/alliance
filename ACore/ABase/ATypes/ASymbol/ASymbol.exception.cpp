#include "ASymbol.exception.hpp"

ASymbolException::ASymbolException() : AllianceException() {
	exception = "Alliance-Exception-ASymbol";
}

ASymbolException::ASymbolException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ASymbol");
}

ASymbolException::ASymbolException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ASymbol");
}

ASymbolException::ASymbolException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ASymbol");
}

ASymbolException::ASymbolException(const ASymbolException & obj) : AllianceException(obj) {

}

ASymbolException::~ASymbolException() {

}
