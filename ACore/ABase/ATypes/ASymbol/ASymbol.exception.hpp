#ifndef HPP_ASYMBOLEXCEPTION
#define HPP_ASYMBOLEXCEPTION
#include <alliance.exception.hpp>

class ASymbolException : public AllianceException {
public:
	ASymbolException();
	ASymbolException(std::string text);
	ASymbolException(AllianceException & e);
	ASymbolException(AllianceException & e, std::string text);
	ASymbolException(const ASymbolException & obj);
	~ASymbolException();
};

#endif
