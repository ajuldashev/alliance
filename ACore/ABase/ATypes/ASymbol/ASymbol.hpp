#ifndef HPP_ASYMBOL
#define HPP_ASYMBOL
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.hpp>
#include "ASymbol.exception.hpp"

class ASymbol {
private:
    AString symbol;
public:
    ASymbol();
    ASymbol(char symbol);
    ASymbol(AString string);
    ASymbol(const char * cstring);
    ASymbol(const ASymbol & symbol);

    void set(ASymbol symbol);
    void set(AString string);

    u64int set(AString string, u64int index);

    bool operator <(ASymbol symbol);
    bool operator >(ASymbol symbol);
    bool operator<=(ASymbol symbol);
    bool operator>=(ASymbol symbol);
    bool operator!=(ASymbol symbol);
    bool operator==(ASymbol symbol);

    ASymbol & operator=(ASymbol symbol);

    AString to_string();

    u64int length();

    static u64int fix_index(AString string, u64int index);

    static u8int test_length_symbol(AString string, u64int index = 0);

    static bool is_intermediate_byte(u8int byte);

    ~ASymbol();
};

#endif
