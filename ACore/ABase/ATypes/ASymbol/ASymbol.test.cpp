#include "ASymbol.hpp"

#ifdef TEST_ASYMBOL
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
		ASymbol symbol("1");
		TEST_ALERT(symbol.length() == 1, 1)
		TEST_ALERT(symbol.to_string() == "1", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
		/** <doc-test-info>
		 *
		 */
		ASymbol symbol("A");
		TEST_ALERT(symbol.length() == 1, 1)
		TEST_ALERT(symbol.to_string() == "A", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
		/** <doc-test-info>
		 *
		 */
		ASymbol symbol("Ф");
		TEST_ALERT(symbol.length() == 2, 1)
		TEST_ALERT(symbol.to_string() == "Ф", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
		/** <doc-test-info>
		 *
		 */
		ASymbol symbol("ё");
		TEST_ALERT(symbol.length() == 2, 1)
		TEST_ALERT(symbol.to_string() == "ё", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
		/** <doc-test-info>
		 *
		 */
		ASymbol symbol;
		TEST_ALERT(symbol.is_intermediate_byte(0xB9), 1)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
		ASymbol symbol;
		u64int index = symbol.set("цй", 0);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.length() == 2, 1)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.to_string() == "ц", 2)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(index == 0, 3)
	}
	{
		ASymbol symbol;
		u64int index = symbol.set("цй", 1);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.length() == 2, 4)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.to_string() == "ц", 5)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(index == 0, 6)
	}
	{
		ASymbol symbol;
		u64int index = symbol.set("цй", 2);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.length() == 2, 7)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.to_string() == "й", 8)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(index == 2, 9)
	}
	{
		ASymbol symbol;
		u64int index = symbol.set("цй", 3);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.length() == 2, 10)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(symbol.to_string() == "й", 11)
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(index == 2, 12)
	}
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(asymbol)

	TEST_CALL(1)
	TEST_CALL(2)
	TEST_CALL(3)
	TEST_CALL(4)
	TEST_CALL(5)
	TEST_CALL(6)

TEST_FUNCTION_MAIN_END(ASYMBOL)

#endif
