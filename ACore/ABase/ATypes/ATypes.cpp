#include "ATypes.hpp"
#include "ATypes.test.cpp"
#include "ATypes.test.node.cpp"
#include "ATypes.debug.cpp"
#include "ATypes.debug.node.cpp"
#include "ATypes.exception.cpp"
#include "climits"
#include "cfloat"

template<>
bool ATypes<bool>::min() {
    return false;
}

template<>
bool ATypes<bool>::max() {
    return true;
}

template<>
s8int ATypes<s8int>::min() {
    return SCHAR_MIN;
}

template<>
s8int ATypes<s8int>::max() {
    return SCHAR_MAX;
}

template<>
u8int ATypes<u8int>::min() {
    return 0;
}

template<>
u8int ATypes<u8int>::max() {
    return UCHAR_MAX;
}

template<>
s16int ATypes<s16int>::min() {
    return SHRT_MIN;
}

template<>
s16int ATypes<s16int>::max() {
    return SHRT_MAX;
}

template<>
u16int ATypes<u16int>::min() {
    return 0;
}

template<>
u16int ATypes<u16int>::max() {
    return USHRT_MAX;
}

template<>
s32int ATypes<s32int>::min() {
    return INT_MIN;
}

template<>
s32int ATypes<s32int>::max() {
    return INT_MAX;
}

template<>
u32int ATypes<u32int>::min() {
    return 0;
}

template<>
u32int ATypes<u32int>::max() {
    return UINT_MAX;
}

template<>
s64int ATypes<s64int>::min() {
    return LONG_MIN;
}

template<>
s64int ATypes<s64int>::max() {
    return LONG_MAX;
}

template<>
u64int ATypes<u64int>::min() {
    return 0;
}

template<>
u64int ATypes<u64int>::max() {
    return ULONG_MAX;
}

template<>
float ATypes<float>::min() {
    return FLT_MIN;
}

template<>
float ATypes<float>::max() {
    return FLT_MAX;
}

template<>
double ATypes<double>::min() {
    return DBL_MIN;
}

template<>
double ATypes<double>::max() {
    return DBL_MAX;
}
