#include "ATypes.hpp"

#ifdef DEBUG_ATYPES
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_atypes(AString com) {
	#ifdef DEBUG_ABITS
	if (com == "abits") {
		if (debug_abits()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_APTR
	if (com == "aptr") {
		if (debug_aptr()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_APTRARRAY
	if (com == "aptrarray") {
		if (debug_aptrarray()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASTRING
	if (com == "astring") {
		if (debug_astring()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ASYMBOL
	if (com == "asymbol") {
		if (debug_asymbol()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_atypes() {
	AString text;
	#ifdef DEBUG_ABITS
	text += "d) abits\n";
	#endif
	#ifdef DEBUG_APTR
	text += "d) aptr\n";
	#endif
	#ifdef DEBUG_APTRARRAY
	text += "d) aptrarray\n";
	#endif
	#ifdef DEBUG_ASTRING
	text += "d) astring\n";
	#endif
	#ifdef DEBUG_ASYMBOL
	text += "d) asymbol\n";
	#endif
	return text;
}

#endif
