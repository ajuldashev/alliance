#include "ATypes.exception.hpp"

ATypesException::ATypesException() : AllianceException() {
	exception = "Alliance-Exception-ATypes";
}

ATypesException::ATypesException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATypes");
}

ATypesException::ATypesException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATypes");
}

ATypesException::ATypesException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATypes");
}

ATypesException::ATypesException(const ATypesException & obj) : AllianceException(obj) {
	
}

ATypesException::~ATypesException() {

}

