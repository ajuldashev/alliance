#ifndef HPP_ATYPESEXCEPTION
#define HPP_ATYPESEXCEPTION
#include <alliance.exception.hpp>

class ATypesException : public AllianceException {
public:
	ATypesException();
	ATypesException(std::string text);
	ATypesException(AllianceException & e);
	ATypesException(AllianceException & e, std::string text);
	ATypesException(const ATypesException & obj);
	~ATypesException();
};

#endif

