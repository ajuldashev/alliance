#ifndef HPP_ATYPES
#define HPP_ATYPES
#include <alliance.config.hpp>
#include "ATypes.exception.hpp"

/** <doc-type-info> 
 *  Тип обозначающий что функция или метод не возвращают ничего
 *  <doc-type-name> void
 */

/** <doc-type-info> 
 *  Логический тип данных, может принимать значние - true/false
 *  <doc-type-name> bool
 */

/** <doc-type-info> 
 *  Указатель на стандартную строку в стиле СИ
 */
typedef char * APString;

/** <doc-type-info> 
 *  Указатель на константную строку в стиле СИ
 */
typedef const char * ACString;

/** <doc-type-info> 
 *  Целое число, размеров в 1 байт в диапазоне [-128, 127]
 */
typedef signed char s8int;

/** <doc-type-info> 
 *  Целое без знаковое число, размеров в 1 байт в диапазоне [0, 255]
 */
typedef unsigned char u8int;

/** <doc-type-info> 
 *  Целое число, размеров в 2 байт в диапазоне [-32768, 32767] 
 */
typedef signed short s16int;

/** <doc-type-info> 
 *  Целое без знаковое число, размеров в 2 байт в диапазоне [0, 65535]
 */
typedef unsigned short u16int;

/** <doc-type-info> 
 *  Целое число, размеров в 4 байт в диапазоне [-2147483648, 2147483647]
 */
typedef signed int s32int;

/** <doc-type-info> 
 *  Целое без знаковое число, размеров в 4 байт в диапазоне [0, 4294967295]
 */
typedef unsigned int u32int;

/** <doc-type-info> 
 *  Целое число, размеров в 8 байт в диапазоне [−9223372036854775808 , 9223372036854775807] 
 */
typedef signed long s64int;

/** <doc-type-info> 
 *  Целое без знаковое число, размеров в 8 байт в диапазоне [0, 18446744073709551615]
 */
typedef unsigned long u64int;

/** <doc-type-info> 
 *  Число одинарной точности с плавающей запятой обеспечивают относительную точность 7-8 десятичных цифр в диапазоне [10^-38, 10^38] 
 *  <doc-type-name> float
 */

/** <doc-type-info> 
 *  Число двойной точности с плавающей запятой обеспечивают относительную точность 15-17 десятичных цифр в диапазоне [10^-308, 10^308] 
 *  <doc-type-name> double
 */


/** <doc-class-info>
 * Класс предоставляет информацию о типа
 */
template<typename T>
class ATypes {
public:
    /** <doc-static-public-method>
     * return - миниму который может принимать переменная имеющая тип заданный в шаблоне
     */
    static T min();
    
    /** <doc-static-public-method>
     * return - максимум который может принимать переменная имеющая тип заданный в шаблоне
     */
    static T max();
};

template<>
bool ATypes<bool>::min();

template<>
bool ATypes<bool>::max();

template<>
s8int ATypes<s8int>::min();

template<>
s8int ATypes<s8int>::max();

template<>
u8int ATypes<u8int>::min();

template<>
u8int ATypes<u8int>::max();

template<>
s16int ATypes<s16int>::min();

template<>
s16int ATypes<s16int>::max();

template<>
u16int ATypes<u16int>::min();

template<>
u16int ATypes<u16int>::max();

template<>
s32int ATypes<s32int>::min();

template<>
s32int ATypes<s32int>::max();

template<>
u32int ATypes<u32int>::min();

template<>
u32int ATypes<u32int>::max();

template<>
s64int ATypes<s64int>::min();

template<>
s64int ATypes<s64int>::max();

template<>
u64int ATypes<u64int>::min();

template<>
u64int ATypes<u64int>::max();

template<>
float ATypes<float>::min();

template<>
float ATypes<float>::max();

template<>
double ATypes<double>::min();

template<>
double ATypes<double>::max();

template<typename T>
T ATypes<T>::min() {
    throw ATypesException("min() : Not defined");
}

template<typename T>
T ATypes<T>::max() {
    throw ATypesException("max() : Not defined");
}

#endif