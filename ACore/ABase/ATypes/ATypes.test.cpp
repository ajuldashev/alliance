#include "ATypes.hpp"
#include "climits"
#include "cfloat"

#ifdef TEST_ATYPES
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
        try {
            ATypes<void>::min();
            TEST_ALERT(false, 1)
        } catch (ATypesException e){
            TEST_ALERT(true, 1)
        } 
	}
    {
		/** <doc-test-info>
		 *
		 */
        try {
            ATypes<void>::max();
            TEST_ALERT(false, 2)
        } catch (ATypesException e){
            TEST_ALERT(true, 2)
        } 
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<bool>::min() == false, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<bool>::max() == true, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s8int>::min() == CHAR_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s8int>::max() == CHAR_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u8int>::min() == 0, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u8int>::max() == UCHAR_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s16int>::min() == SHRT_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s16int>::max() == SHRT_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u16int>::min() == 0, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u16int>::max() == USHRT_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(7)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s32int>::min() == INT_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s32int>::max() == INT_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(8)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u32int>::min() == 0, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u32int>::max() == UINT_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(9)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s64int>::min() == LONG_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<s64int>::max() == LONG_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(10)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u64int>::min() == 0, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<u64int>::max() == ULONG_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(11)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<float>::min() == FLT_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<float>::max() == FLT_MAX, 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(12)
	{
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<double>::min() == DBL_MIN, 1)
        /** <doc-test-info>
		 *
		 */
		TEST_ALERT(ATypes<double>::max() == DBL_MAX, 2)
	}
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(atypes)

	TEST_CALL(1)
    TEST_CALL(2)
    TEST_CALL(3)
    TEST_CALL(4)
    TEST_CALL(5)
    TEST_CALL(6)
    TEST_CALL(7)
    TEST_CALL(8)
    TEST_CALL(9)
    TEST_CALL(10)
    TEST_CALL(11)
    TEST_CALL(12)

TEST_FUNCTION_MAIN_END(ATYPES)

#endif