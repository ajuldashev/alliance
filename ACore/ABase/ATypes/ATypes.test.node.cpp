#include "ATypes.hpp"

#ifdef TEST_ATYPES
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_atypes(bool is_soft) {
	#ifdef TEST_ABITS
	if (!test_abits(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ABITS );
		return is_soft;
	}
	#endif
	#ifdef TEST_APTR
	if (!test_aptr(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APTR );
		return is_soft;
	}
	#endif
	#ifdef TEST_APTRARRAY
	if (!test_aptrarray(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APTRARRAY );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASTRING
	if (!test_astring(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASTRING );
		return is_soft;
	}
	#endif
	#ifdef TEST_ASYMBOL
	if (!test_asymbol(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ASYMBOL );
		return is_soft;
	}
	#endif
	return true;
}
#endif
