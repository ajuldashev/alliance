#include "ACore.hpp"

#ifdef DEBUG_ACORE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_acore(AString com) {
	#ifdef DEBUG_ABASE
	if (com == "abase") {
		if (debug_abase()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATOOLS
	if (com == "atools") {
		if (debug_atools()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_acore() {
	AString text;
	#ifdef DEBUG_ABASE
	text += "d) abase\n";
	#endif
	#ifdef DEBUG_ATOOLS
	text += "d) atools\n";
	#endif
	return text;
}

#endif
