#include "ACore.exception.hpp"

ACoreException::ACoreException() : AllianceException() {
	exception = "Alliance-Exception-ACore";
}

ACoreException::ACoreException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACore");
}

ACoreException::ACoreException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACore");
}

ACoreException::ACoreException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACore");
}

ACoreException::ACoreException(const ACoreException & obj) : AllianceException(obj) {
	
}

ACoreException::~ACoreException() {

}

