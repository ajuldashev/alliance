#ifndef ACOREEXCEPTION
#define ACOREEXCEPTION
#include <alliance.exception.hpp>

class ACoreException : public AllianceException {
public:
	ACoreException();
	ACoreException(std::string text);
	ACoreException(AllianceException & e);
	ACoreException(AllianceException & e, std::string text);
	ACoreException(const ACoreException & obj);
	~ACoreException();
};

#endif

