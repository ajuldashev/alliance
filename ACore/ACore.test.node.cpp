#include "ACore.hpp"

#ifdef TEST_ACORE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_acore(bool is_soft) {
	#ifdef TEST_ABASE
	if (!test_abase(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ABASE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATOOLS
	if (!test_atools(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATOOLS );
		return is_soft;
	}
	#endif
	return true;
}
#endif
