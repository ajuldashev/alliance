#include "ACppImage.hpp"

#ifdef DEBUG_ACPPIMAGE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_acppimage(AString com) {
	#ifdef DEBUG_ACPPITEM
	if (com == "acppitem") {
		if (debug_acppitem()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_acppimage() {
	AString text;
	#ifdef DEBUG_ACPPITEM
	text += "d) acppitem\n";
	#endif
	return text;
}

#endif
