#include "ACppImage.exception.hpp"

ACppImageException::ACppImageException() : AllianceException() {
	exception = "Alliance-Exception-ACppImage";
}

ACppImageException::ACppImageException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppImage");
}

ACppImageException::ACppImageException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppImage");
}

ACppImageException::ACppImageException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppImage");
}

ACppImageException::ACppImageException(const ACppImageException & obj) : AllianceException(obj) {

}

ACppImageException::~ACppImageException() {

}
