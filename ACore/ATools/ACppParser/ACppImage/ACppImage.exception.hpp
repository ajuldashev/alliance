#ifndef HPP_ACPPIMAGEEXCEPTION
#define HPP_ACPPIMAGEEXCEPTION
#include <alliance.exception.hpp>

class ACppImageException : public AllianceException {
public:
	ACppImageException();
	ACppImageException(std::string text);
	ACppImageException(AllianceException & e);
	ACppImageException(AllianceException & e, std::string text);
	ACppImageException(const ACppImageException & obj);
	~ACppImageException();
};

#endif
