#include "ACppImage.hpp"

#ifdef TEST_ACPPIMAGE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_acppimage(bool is_soft) {
	#ifdef TEST_ACPPITEM
	if (!test_acppitem(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPITEM );
		return is_soft;
	}
	#endif
	return true;
}
#endif
