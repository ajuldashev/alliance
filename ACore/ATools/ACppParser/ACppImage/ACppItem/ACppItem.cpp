#include "ACppItem.hpp"
#include "ACppItem.test.cpp"
#include "ACppItem.test.node.cpp"
#include "ACppItem.debug.cpp"
#include "ACppItem.debug.node.cpp"
#include "ACppItem.exception.cpp"

ACppItem::ACppItem() {

}

void ACppItem::add_propertie(AString propertie, AString value) {
    properties.add(propertie, value);
}

void ACppItem::add_item(APtr<ACppItem> item) {
    items.push_end(item);
}

AString ACppItem::get_propertie(AString propertie) {
    APtr<AString> value = properties.get(propertie);
    return (value.is_present())? *value : "undefined";
}

APtr<ACppItem> ACppItem::get_item(u64int index) {
    return items.get(index);
}

u64int ACppItem::count_items() {
    return items.length();
}

static AString make_offset(u64int count) {
    AString tab = "";
    for (u64int i = 0; i < count; i += 1) {
        tab += "\t";
    }
    return tab;
}

AString ACppItem::to_text(u64int offset) {
    AString res;
    for (u64int i = 0; i < properties.count(); i += 1) {
        res += make_offset(offset) + "\"" + properties.get_key(i) + "\":\"" + *properties.get_data(i) + "\";\n";
    }
    for (u64int i = 0; i < items.length(); i += 1) {
        res += make_offset(offset) + "{\n" + items.at(i)->to_text(offset + 1) + make_offset(offset) + "}\n";
    }
    return res;
}

ACppItem::~ACppItem() {

}