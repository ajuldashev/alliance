#include "ACppItem.exception.hpp"

ACppItemException::ACppItemException() : AllianceException() {
	exception = "Alliance-Exception-ACppItem";
}

ACppItemException::ACppItemException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppItem");
}

ACppItemException::ACppItemException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppItem");
}

ACppItemException::ACppItemException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppItem");
}

ACppItemException::ACppItemException(const ACppItemException & obj) : AllianceException(obj) {

}

ACppItemException::~ACppItemException() {

}
