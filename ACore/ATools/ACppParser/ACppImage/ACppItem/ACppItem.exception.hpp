#ifndef HPP_ACPPITEMEXCEPTION
#define HPP_ACPPITEMEXCEPTION
#include <alliance.exception.hpp>

class ACppItemException : public AllianceException {
public:
	ACppItemException();
	ACppItemException(std::string text);
	ACppItemException(AllianceException & e);
	ACppItemException(AllianceException & e, std::string text);
	ACppItemException(const ACppItemException & obj);
	~ACppItemException();
};

#endif
