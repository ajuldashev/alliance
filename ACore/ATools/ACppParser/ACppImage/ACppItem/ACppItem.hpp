#ifndef HPP_ACPPITEM
#define HPP_ACPPITEM
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include <ACore/ABase/AStruct/AList/AList.hpp>
#include <ACore/ABase/AStruct/AMap/AMap.hpp>
#include <ACore/ABase/ATypes/APtr/APtr.hpp>
#include "ACppItem.exception.hpp"

class ACppItem {
private:
    AMap<AString, AString> properties;
    AList<APtr<ACppItem>> items;
public:
    ACppItem();

    void add_propertie(AString propertie, AString value);
    void add_item(APtr<ACppItem> item);

    AString get_propertie(AString propertie);

    APtr<ACppItem> get_item(u64int index);
    u64int count_items();

    AString to_text(u64int offset = 0);

    ~ACppItem();
};

#endif
