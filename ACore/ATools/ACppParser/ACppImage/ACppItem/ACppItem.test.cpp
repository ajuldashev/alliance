#include "ACppItem.hpp"

#ifdef TEST_ACPPITEM
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppItem item;
		item.add_propertie("name", "item");
		item.add_propertie("args", "0");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(item.get_propertie("name") == "item", 1)

		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(item.get_propertie("args") == "0", 2)

		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(item.get_propertie("arg_1") == "undefined", 3)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acppitem)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPITEM)

#endif
