#include "ACppParser.hpp"
#include "ACppParser.test.cpp"
#include "ACppParser.test.node.cpp"
#include "ACppParser.debug.cpp"
#include "ACppParser.debug.node.cpp"
#include "ACppParser.exception.cpp"

void ACppParser::remove_space_and_empty_lines(ATextIterator & text_iterator) {
    try {
        while (try_parse_space(text_iterator)
            || try_parse_empty_line(text_iterator)
        );   
    } catch (AllianceException & e) {
        throw ACppParserException(e, "remove_space_and_empty_lines(ATextIterator & text_iterator)");
    }
}

bool ACppParser::try_parse_empty_line(ATextIterator & text_iterator) {
    try {

        if (text_iterator.can_get()) {
            AStringIterator string_iterator = text_iterator.get_iterator();
            if (!string_iterator.can_get()) {
                text_iterator.update(string_iterator);
                return true;
            }
        }

        return false;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_empty_line(ATextIterator & text_iterator)");
    }
}

bool ACppParser::try_parse_space(ATextIterator & text_iterator) {
    try {

        if (!(
            token.try_parse_text(text_iterator, ACppToken::Token::cpp_space)
        )) {
            return false; 
        }

        return true;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_space(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_commands(ATextIterator & text_iterator) {
    try {
        APtr<ACppItem> item;
        APtr<ACppItem> command_item = try_parse_command(text_iterator);

        while (command_item.is_present()) {
            if (item.is_empty()) {
                item = APtr<ACppItem>::make();
                item->add_propertie("type","commands");
            }
            item->add_item(command_item);
            command_item = try_parse_command(text_iterator);
        }

        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_commands(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_command(ATextIterator & text_iterator) {
    try {
        APtr<ACppItem> item;

        remove_space_and_empty_lines(text_iterator);

        item = try_parse_ifdef(text_iterator);
        if (item.is_present()) {
            return item;
        }

        item = try_parse_ifndef(text_iterator);
        if (item.is_present()) {
            return item;
        }

        item = try_parse_define(text_iterator);
        if (item.is_present()) {
            return item;
        }

        item = try_parse_include(text_iterator);
        if (item.is_present()) {
            return item;
        }

        /*for test*/
        item = try_parse_type_name(text_iterator);
        if (item.is_present()) {
            return item;
        }

        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_command(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_ifdef(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "ifdef")
        )) {
            return item; 
        } 

        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_space);
        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_all_line);
        
        if (token.get_token() == ACppToken::Token::cpp_error) {
            throw ACppParserException("Failed parse value of #ifdef");
        }

        item = APtr<ACppItem>::make();
        item->add_propertie("type", "def");
        item->add_propertie("name", "#ifdef");
        item->add_propertie("value", token.get_text());

        APtr<ACppItem> item_commands = try_parse_commands(local_text_iterator);

        if (item_commands) {
            item->add_item(item_commands);
        }

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "endif")
        )) {
            throw ACppParserException("Not found #endif");
        }

        text_iterator = local_text_iterator;
        
        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_ifdef(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_ifndef(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "ifndef")
        )) {
            return item; 
        } 

        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_space);
        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_all_line);
        
        if (token.get_token() == ACppToken::Token::cpp_error) {
            throw ACppParserException("Failed parse value of #ifdef");
        }

        item = APtr<ACppItem>::make();
        item->add_propertie("type", "def");
        item->add_propertie("name", "#ifndef");
        item->add_propertie("value", token.get_text());

        APtr<ACppItem> item_commands = try_parse_commands(local_text_iterator);

        if (item_commands) {
            item->add_item(item_commands);
        }

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "endif")
        )) {
            throw ACppParserException("Not found #endif");
        }

        text_iterator = local_text_iterator;
        
        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_ifndef(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_define(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "define")
        )) {
            return item; 
        } 

        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_space);
        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_all_line);
        
        if (token.get_token() == ACppToken::Token::cpp_error) {
            throw ACppParserException("Failed parse value of #define");
        }

        item = APtr<ACppItem>::make();
        item->add_propertie("type", "def");
        item->add_propertie("name", "#define");
        item->add_propertie("value", token.get_text());

        text_iterator = local_text_iterator;
        
        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_define(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_include(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        if (!(
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "#")
        &&  token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name      , "include")
        )) {
            return item; 
        } 

        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_space);
        token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_all_line);
        
        if (token.get_token() == ACppToken::Token::cpp_error) {
            throw ACppParserException("Failed parse value of #include");
        }

        item = APtr<ACppItem>::make();
        item->add_propertie("type", "def");
        item->add_propertie("name", "#include");
        item->add_propertie("value", token.get_text());

        text_iterator = local_text_iterator;
        
        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_include(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_type_name(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        remove_space_and_empty_lines(local_text_iterator);

        if (token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_type)) {

            item = APtr<ACppItem>::make();
            item->add_propertie("type", "keytype");
            item->add_propertie("name", token.get_text());
            
            text_iterator = local_text_iterator;

            return item;
        }

        if (token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_word, "typename")) {
            remove_space_and_empty_lines(local_text_iterator);
        }

        if ( 
            ! token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name)
            || token.match(token.get_text(), ACppToken::Token::cpp_key_word)
        ) {
            return item; 
        }

        AString current_type_name = token.get_text();

        if ( ! current_type_name.is_empty()) {
            item = APtr<ACppItem>::make();
            item->add_propertie("type", "type");
        } else {
            return item;
        }

        while( ! current_type_name.is_empty()) {
            
            APtr<ACppItem> item_template_args = try_parse_template_args_in_type_name(local_text_iterator);

            remove_space_and_empty_lines(local_text_iterator);
            if ( ! token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "::")) {
                item->add_propertie("name", current_type_name);
                if (item_template_args.is_present()) {
                    item->add_item(item_template_args);
                    item_template_args.reset();
                }
                break;
            }

            APtr<ACppItem> item_namespace = APtr<ACppItem>::make();
            item_namespace->add_propertie("type", "namespace");
            item_namespace->add_propertie("name", current_type_name);

            if (item_template_args.is_present()) {
                item_namespace->add_item(item_template_args);
                item_template_args.reset();
            }
            item->add_item(item_namespace);

            remove_space_and_empty_lines(local_text_iterator);
            token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_name);
            current_type_name = token.get_text();
        }

        if (current_type_name.is_empty()) {
            throw ACppParserException("Failed parse type name");
        }

        text_iterator = local_text_iterator;

        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_type_name(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_template_args_in_type_name(ATextIterator & text_iterator) {
    try {
        ATextIterator local_text_iterator = text_iterator;
        APtr<ACppItem> item;

        remove_space_and_empty_lines(local_text_iterator);
        if ( ! token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, "<")) {
            return item;
        }

        remove_space_and_empty_lines(local_text_iterator);
        APtr<ACppItem> item_type = try_parse_type_name(local_text_iterator);
        while (item_type.is_present()) {
            if (item.is_empty()) {
                item = APtr<ACppItem>::make();
                item->add_propertie("type", "template_args");
            }
            item->add_item(item_type);

            remove_space_and_empty_lines(local_text_iterator);
            if ( ! token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, ",")) {
                break;
            }

            remove_space_and_empty_lines(local_text_iterator);
            item_type = try_parse_type_name(local_text_iterator);
        }

        if (item_type.is_empty()) {
            throw ACppParserException("Failed parse type in template args");
        }

        remove_space_and_empty_lines(local_text_iterator);
        if ( ! token.try_parse_text(local_text_iterator, ACppToken::Token::cpp_key_symbol, ">")) {
            throw ACppParserException("Not found sumbol >");
        }

        text_iterator = local_text_iterator;
        
        return item;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "try_parse_template_args_in_type_name(ATextIterator & text_iterator)");
    }
}

APtr<ACppItem> ACppParser::try_parse_field_name(ATextIterator & text_iterator) {

}

APtr<ACppItem> ACppParser::try_parse_variable_name(ATextIterator & text_iterator) {

}

void ACppParser::throw_exception_failed_parse_token(ATextIterator & text_iterator) {
    try {
        if (token.try_parse_text(text_iterator)) {
            throw ACppParserException(AStringFormat::format("Failed token \"%ss\" text \"%ss\" in line %ul index of %ul", 
                token.to_text_token(), token.get_text(), text_iterator.get_index_line(), text_iterator.get_iterator().get_index()).to_cstring().const_data());
        } else {
            throw ACppParserException(AStringFormat::format("Failed in line %ul index of %ul", 
                text_iterator.get_index_line(), text_iterator.get_iterator().get_index()).to_cstring().const_data());
        }
    } catch (AllianceException & e) {
        throw ACppParserException(e, "throw_exception_failed_parse_token(ATextIterator & text_iterator)");
    }
}

ACppParser::ACppParser() {

}

void ACppParser::set_text(AText text) {
    try {
        this->text = text;
    } catch (AllianceException & e) {
        throw ACppParserException(e, "set_text(AText text)");
    }
}

void ACppParser::load_text(AString file_name) {
    try {
        ATextFile file(file_name);
        file.load();
        this->text = file.get_text();
    } catch (AllianceException & e) {
        throw ACppParserException(e, "load_text(AString file_name)");
    }
}

void ACppParser::parse() {
    try {
        ATextIterator text_iterator;
        text_iterator.set_text(text);
        APtr<ACppItem> item = try_parse_commands(text_iterator);
        if (item) {
            AIO::writeln(item->to_text());
        } else {
            AIO::writeln("FAIL");
        }
    } catch (AllianceException & e) {
        throw ACppParserException(e, "parse()");
    }
}

ACppImage ACppParser::get_image() {
    return image;
}

ACppParser::~ACppParser() {

}