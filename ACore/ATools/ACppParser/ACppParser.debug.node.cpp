#include "ACppParser.hpp"

#ifdef DEBUG_ACPPPARSER
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_acppparser(AString com) {
	#ifdef DEBUG_ACPPIMAGE
	if (com == "acppimage") {
		if (debug_acppimage()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKEN
	if (com == "acpptoken") {
		if (debug_acpptoken()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_acppparser() {
	AString text;
	#ifdef DEBUG_ACPPIMAGE
	text += "d) acppimage\n";
	#endif
	#ifdef DEBUG_ACPPTOKEN
	text += "d) acpptoken\n";
	#endif
	return text;
}

#endif
