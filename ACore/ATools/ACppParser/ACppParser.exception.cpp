#include "ACppParser.exception.hpp"

ACppParserException::ACppParserException() : AllianceException() {
	exception = "Alliance-Exception-ACppParser";
}

ACppParserException::ACppParserException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppParser");
}

ACppParserException::ACppParserException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppParser");
}

ACppParserException::ACppParserException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppParser");
}

ACppParserException::ACppParserException(const ACppParserException & obj) : AllianceException(obj) {

}

ACppParserException::~ACppParserException() {

}
