#ifndef HPP_ACPPPARSEREXCEPTION
#define HPP_ACPPPARSEREXCEPTION
#include <alliance.exception.hpp>

class ACppParserException : public AllianceException {
public:
	ACppParserException();
	ACppParserException(std::string text);
	ACppParserException(AllianceException & e);
	ACppParserException(AllianceException & e, std::string text);
	ACppParserException(const ACppParserException & obj);
	~ACppParserException();
};

#endif
