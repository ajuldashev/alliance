#ifndef HPP_ACPPPARSER
#define HPP_ACPPPARSER
#include <ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.hpp>
#include <ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.hpp>
#include <ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.hpp>
#include "ACppToken/ACppToken.hpp"
#include "ACppImage/ACppImage.hpp"
#include "ACppParser.exception.hpp"

class ACppParser {
private:
    AText text;
    ACppImage image;
    ACppToken token;

    void remove_space_and_empty_lines(ATextIterator & text_iterator);    
    bool try_parse_empty_line(ATextIterator & text_iterator);
    bool try_parse_space(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_commands(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_command(ATextIterator & text_iterator);
    
    APtr<ACppItem> try_parse_ifdef (ATextIterator & text_iterator);
    APtr<ACppItem> try_parse_ifndef(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_define(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_include(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_type_name(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_template_args_in_type_name(ATextIterator & text_iterator);
    
    APtr<ACppItem> try_parse_field_name(ATextIterator & text_iterator);

    APtr<ACppItem> try_parse_variable_name(ATextIterator & text_iterator);

    void throw_exception_failed_parse_token(ATextIterator & text_iterator);

public:
    ACppParser();

    void set_text(AText text);
    void load_text(AString file_name);

    void parse();

    ACppImage get_image();

    ~ACppParser();
};

#endif
