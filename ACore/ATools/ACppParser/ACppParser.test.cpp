#include "ACppParser.hpp"

#ifdef TEST_ACPPPARSER
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		/** <doc-test-info>
		 *
		 */
		ACppParser parser;
		parser.load_text("alliance.copy.cpp");
		parser.parse();
		TEST_ALERT(true, 1)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acppparser)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPPARSER)

#endif
