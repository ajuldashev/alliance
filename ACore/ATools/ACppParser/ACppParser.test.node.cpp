#include "ACppParser.hpp"

#ifdef TEST_ACPPPARSER
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_acppparser(bool is_soft) {
	#ifdef TEST_ACPPIMAGE
	if (!test_acppimage(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPIMAGE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKEN
	if (!test_acpptoken(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKEN );
		return is_soft;
	}
	#endif
	return true;
}
#endif
