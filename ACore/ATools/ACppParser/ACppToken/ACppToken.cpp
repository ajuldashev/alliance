#include "ACppToken.hpp"
#include "ACppToken.test.cpp"
#include "ACppToken.test.node.cpp"
#include "ACppToken.debug.cpp"
#include "ACppToken.debug.node.cpp"
#include "ACppToken.exception.cpp"

AStringIterator ACppToken::parse_space(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_space->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_space;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_space(AStringIterator iterator)");
    }
}
    
AStringIterator ACppToken::parse_comment_line(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_comment_line->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_comment_line;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_comment_line(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_comment_inline(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_comment_inline->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_comment_inline;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_comment_inline(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_comment_begin(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_comment_begin->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_comment_begin;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_comment_begin(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_commnet_end(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_comment_end->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_comment_end;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_comment_end(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_key_symbol(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_key_symbol->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_key_symbol;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_key_symbol(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_key_type(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_key_type->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_key_type;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_key_type(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_key_word(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_key_word->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_key_word;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_key_word(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_name(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_name->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_name;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_name(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_number_hex(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_number_hex->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_number_hex;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_number_hex(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_number_double(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_number_double->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_number_double;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_number_double(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_number_integer(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_number_integer->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_number_integer;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_number_integer(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_char(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_char->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_char;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_char(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_string(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_string->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_string;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_string(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::parse_all_line(AStringIterator iterator) {
    try {
        ARegexPart part = pattern_all_line->matches_part(iterator);
        APtr<AStringList> matches = part.get_matches();
        if (matches) {
            text = matches->get(0);
            token = Token::cpp_all_line;
        } else {
            token = Token::cpp_error;
        }
        return part.get_finish();
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "parse_all_line(AStringIterator iterator)");
    }
}


ACppToken::ACppToken() {
    
}

AStringIterator ACppToken::set(AStringIterator iterator) {
    try {

        iterator = parse_space(iterator);
        
        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_comment_line(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_comment_inline(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_comment_begin(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_commnet_end(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_key_symbol(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_key_type(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_key_word(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_name(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_number_hex(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_number_double(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_number_integer(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_char(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_string(iterator);

        if (token != Token::cpp_error) {
            return iterator;
        }

        iterator = parse_all_line(iterator);

        return iterator;
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "set(AStringIterator iterator)");
    }
}

AStringIterator ACppToken::set(AStringIterator iterator, Token token) {
    try {
        switch (token) {
            case Token::cpp_space : {
                return parse_space(iterator);
            }
            case Token::cpp_comment_line : {
                return parse_comment_line(iterator);
            }
            case Token::cpp_comment_inline : {
                return parse_comment_inline(iterator);
            }
            case Token::cpp_comment_begin : {
                return parse_comment_begin(iterator);
            }
            case Token::cpp_comment_end : {
                return parse_commnet_end(iterator);
            }
            case Token::cpp_key_symbol : {
                return parse_key_symbol(iterator);
            }
            case Token::cpp_key_type : {
                return parse_key_type(iterator);
            }
            case Token::cpp_key_word : {
                return parse_key_word(iterator);
            }
            case Token::cpp_name : {
                return parse_name(iterator);
            }
            case Token::cpp_number_hex : {
                return parse_number_hex(iterator);
            }
            case Token::cpp_number_double : {
                return parse_number_double(iterator);
            }
            case Token::cpp_number_integer : {
                return parse_number_integer(iterator);
            }
            case Token::cpp_char : {
                return parse_char(iterator);
            }
            case Token::cpp_string : {
                return parse_string(iterator);
            }
            case Token::cpp_all_line : {
                return parse_all_line(iterator);
            }
            default : {
                token = Token::cpp_error;
                return iterator;
            }
        }
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "set(AStringIterator iterator, Token token)");
    }
    return iterator;
}

ACppToken::Token ACppToken::get_token() {
    return token;
}

AString ACppToken::get_text() {
    return text;
}

bool ACppToken::try_parse_text(ATextIterator & text_iterator) {
    try {
        if (text_iterator.can_get()) {
            AStringIterator string_iterator = set(text_iterator.get_iterator());
            if (get_token() == Token::cpp_error) {
                return false;
            }
            text_iterator.update(string_iterator);
            return true;
        }
        return false;
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "try_parse_text(ATextIterator & text_iterator)");
    }
}

bool ACppToken::try_parse_text(ATextIterator & text_iterator, Token token) {
    try {
        if (text_iterator.can_get()) {
            AStringIterator string_iterator = set(text_iterator.get_iterator(), token);
            if (get_token() == Token::cpp_error) {
                return false;
            }
            text_iterator.update(string_iterator);
            return true;
        }
        return false;
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "try_parse_text(ATextIterator & text_iterator, Token token)");
    }
}

bool ACppToken::try_parse_text(ATextIterator & text_iterator, Token token, AString value) {
    try {
        if (text_iterator.can_get()) {
            AStringIterator string_iterator = set(text_iterator.get_iterator(), token);
            if (get_token() == Token::cpp_error
                || get_text() != value
            ) {
                return false;
            }
            text_iterator.update(string_iterator);
            return true;
        }
        return false;
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "try_parse_text(ATextIterator & text_iterator, Token token, AString value)");
    }
}

bool ACppToken::match(AStringIterator string_iterator, Token token) {
    try {
        switch (token) {
            case Token::cpp_space : {
                return pattern_space->match(string_iterator);
            }
            case Token::cpp_comment_line : {
                return pattern_comment_line->match(string_iterator);
            }
            case Token::cpp_comment_inline : {
                return pattern_comment_inline->match(string_iterator);
            }
            case Token::cpp_comment_begin : {
                return pattern_comment_begin->match(string_iterator);
            }
            case Token::cpp_comment_end : {
                return pattern_comment_end->match(string_iterator);
            }
            case Token::cpp_key_symbol : {
                return pattern_key_symbol->match(string_iterator);
            }
            case Token::cpp_key_type : {
                return pattern_key_type->match(string_iterator);
            }
            case Token::cpp_key_word : {
                return pattern_key_word->match(string_iterator);
            }
            case Token::cpp_name : {
                return pattern_name->match(string_iterator);
            }
            case Token::cpp_number_hex : {
                return pattern_number_hex->match(string_iterator);
            }
            case Token::cpp_number_double : {
                return pattern_number_double->match(string_iterator);
            }
            case Token::cpp_number_integer : {
                return pattern_number_integer->match(string_iterator);
            }
            case Token::cpp_char : {
                return pattern_char->match(string_iterator);
            }
            case Token::cpp_string : {
                return pattern_string->match(string_iterator);
            }
            case Token::cpp_all_line : {
                return pattern_all_line->match(string_iterator);
            }
            default : {
                token = Token::cpp_error;
                return false;
            }
        }
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "set(AStringIterator iterator, Token token)");
    }
}

AString ACppToken::to_text_token() {
    try {
        switch (token) {
            case Token::cpp_space : {
                return "cpp_space";
            }
            case Token::cpp_comment_line : {
                return "cpp_comment_line";
            }
            case Token::cpp_comment_inline : {
                return "cpp_comment_inline";
            }
            case Token::cpp_comment_begin : {
                return "cpp_comment_begin";
            }
            case Token::cpp_comment_end : {
                return "cpp_comment_end";
            }
            case Token::cpp_key_symbol : {
                return "cpp_key_symbol";
            }
            case Token::cpp_key_type : {
                return "cpp_key_type";
            }
            case Token::cpp_key_word : {
                return "cpp_key_word";
            }
            case Token::cpp_name : {
                return "cpp_name";
            }
            case Token::cpp_number_hex : {
                return "cpp_number_hex";
            }
            case Token::cpp_number_double : {
                return "cpp_number_double";
            }
            case Token::cpp_number_integer : {
                return "cpp_number_integer";
            }
            case Token::cpp_char : {
                return "cpp_char";
            }
            case Token::cpp_string : {
                return "cpp_string";
            }
            case Token::cpp_all_line : {
                return "cpp_all_line";
            }
            default : {
                return "error";
            }
        }
    } catch (AllianceException & e) {
        throw ACppTokenException(e, "token_to_text(Token toke)");
    }
}

ACppToken::~ACppToken() {

}