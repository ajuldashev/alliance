#include "ACppToken.hpp"

#ifdef DEBUG_ACPPTOKEN
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_acpptoken(AString com) {
	#ifdef DEBUG_ACPPTOKENALLLINE
	if (com == "acpptokenallline") {
		if (debug_acpptokenallline()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENCHAR
	if (com == "acpptokenchar") {
		if (debug_acpptokenchar()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTBEGIN
	if (com == "acpptokencommentbegin") {
		if (debug_acpptokencommentbegin()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTEND
	if (com == "acpptokencommentend") {
		if (debug_acpptokencommentend()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTINLINE
	if (com == "acpptokencommentinline") {
		if (debug_acpptokencommentinline()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTLINE
	if (com == "acpptokencommentline") {
		if (debug_acpptokencommentline()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENKEYSYMBOL
	if (com == "acpptokenkeysymbol") {
		if (debug_acpptokenkeysymbol()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENKEYTYPE
	if (com == "acpptokenkeytype") {
		if (debug_acpptokenkeytype()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENKEYWORD
	if (com == "acpptokenkeyword") {
		if (debug_acpptokenkeyword()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENNAME
	if (com == "acpptokenname") {
		if (debug_acpptokenname()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERDOUBLE
	if (com == "acpptokennumberdouble") {
		if (debug_acpptokennumberdouble()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERHEX
	if (com == "acpptokennumberhex") {
		if (debug_acpptokennumberhex()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERINTEGER
	if (com == "acpptokennumberinteger") {
		if (debug_acpptokennumberinteger()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENSPACE
	if (com == "acpptokenspace") {
		if (debug_acpptokenspace()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ACPPTOKENSTRING
	if (com == "acpptokenstring") {
		if (debug_acpptokenstring()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_acpptoken() {
	AString text;
	#ifdef DEBUG_ACPPTOKENALLLINE
	text += "d) acpptokenallline\n";
	#endif
	#ifdef DEBUG_ACPPTOKENCHAR
	text += "d) acpptokenchar\n";
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTBEGIN
	text += "d) acpptokencommentbegin\n";
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTEND
	text += "d) acpptokencommentend\n";
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTINLINE
	text += "d) acpptokencommentinline\n";
	#endif
	#ifdef DEBUG_ACPPTOKENCOMMENTLINE
	text += "d) acpptokencommentline\n";
	#endif
	#ifdef DEBUG_ACPPTOKENKEYSYMBOL
	text += "d) acpptokenkeysymbol\n";
	#endif
	#ifdef DEBUG_ACPPTOKENKEYTYPE
	text += "d) acpptokenkeytype\n";
	#endif
	#ifdef DEBUG_ACPPTOKENKEYWORD
	text += "d) acpptokenkeyword\n";
	#endif
	#ifdef DEBUG_ACPPTOKENNAME
	text += "d) acpptokenname\n";
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERDOUBLE
	text += "d) acpptokennumberdouble\n";
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERHEX
	text += "d) acpptokennumberhex\n";
	#endif
	#ifdef DEBUG_ACPPTOKENNUMBERINTEGER
	text += "d) acpptokennumberinteger\n";
	#endif
	#ifdef DEBUG_ACPPTOKENSPACE
	text += "d) acpptokenspace\n";
	#endif
	#ifdef DEBUG_ACPPTOKENSTRING
	text += "d) acpptokenstring\n";
	#endif
	return text;
}

#endif
