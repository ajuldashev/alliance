#include "ACppToken.exception.hpp"

ACppTokenException::ACppTokenException() : AllianceException() {
	exception = "Alliance-Exception-ACppToken";
}

ACppTokenException::ACppTokenException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppToken");
}

ACppTokenException::ACppTokenException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppToken");
}

ACppTokenException::ACppTokenException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppToken");
}

ACppTokenException::ACppTokenException(const ACppTokenException & obj) : AllianceException(obj) {

}

ACppTokenException::~ACppTokenException() {

}
