#ifndef HPP_ACPPTOKENEXCEPTION
#define HPP_ACPPTOKENEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenException : public AllianceException {
public:
	ACppTokenException();
	ACppTokenException(std::string text);
	ACppTokenException(AllianceException & e);
	ACppTokenException(AllianceException & e, std::string text);
	ACppTokenException(const ACppTokenException & obj);
	~ACppTokenException();
};

#endif
