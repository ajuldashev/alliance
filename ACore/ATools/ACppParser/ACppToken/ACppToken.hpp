#ifndef HPP_ACPPTOKEN
#define HPP_ACPPTOKEN

#include <ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.hpp>

#include "ACppTokenSpace/ACppTokenSpace.hpp"

#include "ACppTokenCommentBegin/ACppTokenCommentBegin.hpp"
#include "ACppTokenCommentEnd/ACppTokenCommentEnd.hpp"
#include "ACppTokenCommentInline/ACppTokenCommentInline.hpp"
#include "ACppTokenCommentLine/ACppTokenCommentLine.hpp"

#include "ACppTokenKeySymbol/ACppTokenKeySymbol.hpp"
#include "ACppTokenKeyType/ACppTokenKeyType.hpp"
#include "ACppTokenKeyWord/ACppTokenKeyWord.hpp"
#include "ACppTokenName/ACppTokenName.hpp"

#include "ACppTokenNumberHex/ACppTokenNumberHex.hpp"
#include "ACppTokenNumberInteger/ACppTokenNumberInteger.hpp"
#include "ACppTokenNumberDouble/ACppTokenNumberDouble.hpp"

#include "ACppTokenChar/ACppTokenChar.hpp"
#include "ACppTokenString/ACppTokenString.hpp"

#include "ACppTokenAllLine/ACppTokenAllLine.hpp"

#include "ACppToken.exception.hpp"

class ACppToken {
public:
    enum class Token {
        // same error
        cpp_error,
        // \\s
        cpp_space,
        // // comment
        cpp_comment_line,
        // /*comment*/
        cpp_comment_inline,
        // /* comment begin
        cpp_comment_begin,
        // comment end */
        cpp_comment_end,
        // #, {, [, (, <, ...
        cpp_key_symbol,
        // enum, ...
        cpp_key_word,
        // int, float, ...
        cpp_key_type,
        // name
        cpp_name,
        // 0xff
        cpp_number_hex,
        // 1234.1234
        cpp_number_double,
        // 1234
        cpp_number_integer,
        // "string"
        cpp_string,
        // 's'
        cpp_char,
        // all line
        cpp_all_line
    };
private:
    Token token;
    AString text;

    ACppTokenSpace pattern_space;
    
    ACppTokenCommentLine   pattern_comment_line;
    ACppTokenCommentInline pattern_comment_inline;
    ACppTokenCommentBegin  pattern_comment_begin;
    ACppTokenCommentEnd    pattern_comment_end;

    ACppTokenKeySymbol pattern_key_symbol;
    ACppTokenKeyType   pattern_key_type;
    ACppTokenKeyWord   pattern_key_word;
    ACppTokenName      pattern_name;

    ACppTokenNumberHex     pattern_number_hex;
    ACppTokenNumberDouble  pattern_number_double;
    ACppTokenNumberInteger pattern_number_integer;

    ACppTokenChar   pattern_char;
    ACppTokenString pattern_string;

    ACppTokenAllLine pattern_all_line;

    AStringIterator parse_space(AStringIterator iterator);
    
    AStringIterator parse_comment_line(AStringIterator iterator);
    AStringIterator parse_comment_inline(AStringIterator iterator);
    AStringIterator parse_comment_begin(AStringIterator iterator);
    AStringIterator parse_commnet_end(AStringIterator iterator);

    AStringIterator parse_key_symbol(AStringIterator iterator);
    AStringIterator parse_key_type(AStringIterator iterator);
    AStringIterator parse_key_word(AStringIterator iterator);
    AStringIterator parse_name(AStringIterator iterator);

    AStringIterator parse_number_hex(AStringIterator iterator);
    AStringIterator parse_number_double(AStringIterator iterator);
    AStringIterator parse_number_integer(AStringIterator iterator);

    AStringIterator parse_char(AStringIterator iterator);
    AStringIterator parse_string(AStringIterator iterator);

    AStringIterator parse_all_line(AStringIterator iterator);

public:
    ACppToken();

    AStringIterator set(AStringIterator iterator);

    AStringIterator set(AStringIterator iterator, Token token);

    ACppToken::Token get_token();

    AString get_text();

    bool try_parse_text(ATextIterator & text_iterator);

    bool try_parse_text(ATextIterator & text_iterator, Token token);

    bool try_parse_text(ATextIterator & text_iterator, Token token, AString value);

    bool match(AStringIterator string_iterator, Token token);

    AString to_text_token();

    ~ACppToken();
};

#endif