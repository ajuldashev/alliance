#include "ACppToken.hpp"

#ifdef TEST_ACPPTOKEN
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_acpptoken(bool is_soft) {
	#ifdef TEST_ACPPTOKENALLLINE
	if (!test_acpptokenallline(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENALLLINE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENCHAR
	if (!test_acpptokenchar(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENCHAR );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENCOMMENTBEGIN
	if (!test_acpptokencommentbegin(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENCOMMENTBEGIN );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENCOMMENTEND
	if (!test_acpptokencommentend(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENCOMMENTEND );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENCOMMENTINLINE
	if (!test_acpptokencommentinline(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENCOMMENTINLINE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENCOMMENTLINE
	if (!test_acpptokencommentline(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENCOMMENTLINE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENKEYSYMBOL
	if (!test_acpptokenkeysymbol(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENKEYSYMBOL );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENKEYTYPE
	if (!test_acpptokenkeytype(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENKEYTYPE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENKEYWORD
	if (!test_acpptokenkeyword(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENKEYWORD );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENNAME
	if (!test_acpptokenname(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENNAME );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENNUMBERDOUBLE
	if (!test_acpptokennumberdouble(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENNUMBERDOUBLE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENNUMBERHEX
	if (!test_acpptokennumberhex(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENNUMBERHEX );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENNUMBERINTEGER
	if (!test_acpptokennumberinteger(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENNUMBERINTEGER );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENSPACE
	if (!test_acpptokenspace(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENSPACE );
		return is_soft;
	}
	#endif
	#ifdef TEST_ACPPTOKENSTRING
	if (!test_acpptokenstring(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPTOKENSTRING );
		return is_soft;
	}
	#endif
	return true;
}
#endif
