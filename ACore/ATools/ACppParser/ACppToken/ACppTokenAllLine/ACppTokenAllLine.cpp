#include "ACppTokenAllLine.hpp"
#include "ACppTokenAllLine.test.cpp"
#include "ACppTokenAllLine.test.node.cpp"
#include "ACppTokenAllLine.debug.cpp"
#include "ACppTokenAllLine.debug.node.cpp"
#include "ACppTokenAllLine.exception.cpp"

ACppTokenAllLine::ACppTokenAllLine() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .any().any_number()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenAllLineException(e, "ACppTokenAllLine()");
    }
}

ARegexPattern * ACppTokenAllLine::operator->() {
    return &pattern;
}

ACppTokenAllLine::~ACppTokenAllLine() {

}
