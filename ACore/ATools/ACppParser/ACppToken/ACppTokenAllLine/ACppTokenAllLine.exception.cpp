#include "ACppTokenAllLine.exception.hpp"

ACppTokenAllLineException::ACppTokenAllLineException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenAllLine";
}

ACppTokenAllLineException::ACppTokenAllLineException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenAllLine");
}

ACppTokenAllLineException::ACppTokenAllLineException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenAllLine");
}

ACppTokenAllLineException::ACppTokenAllLineException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenAllLine");
}

ACppTokenAllLineException::ACppTokenAllLineException(const ACppTokenAllLineException & obj) : AllianceException(obj) {

}

ACppTokenAllLineException::~ACppTokenAllLineException() {

}
