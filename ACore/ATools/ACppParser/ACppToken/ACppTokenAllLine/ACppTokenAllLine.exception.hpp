#ifndef HPP_ACPPTOKENALLLINEEXCEPTION
#define HPP_ACPPTOKENALLLINEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenAllLineException : public AllianceException {
public:
	ACppTokenAllLineException();
	ACppTokenAllLineException(std::string text);
	ACppTokenAllLineException(AllianceException & e);
	ACppTokenAllLineException(AllianceException & e, std::string text);
	ACppTokenAllLineException(const ACppTokenAllLineException & obj);
	~ACppTokenAllLineException();
};

#endif
