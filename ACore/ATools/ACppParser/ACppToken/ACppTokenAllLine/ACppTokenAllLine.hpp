#ifndef HPP_ACPPTOKENALLLINE
#define HPP_ACPPTOKENALLLINE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenAllLine.exception.hpp"

class ACppTokenAllLine {
private:
    ARegexPattern pattern;
public:
    ACppTokenAllLine();
    ARegexPattern * operator->();
    ~ACppTokenAllLine();
};

#endif
