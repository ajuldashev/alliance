#include "ACppTokenAllLine.hpp"

#ifdef TEST_ACPPTOKENALLLINE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenAllLine token;
		AStringIterator iterator("12341234"); 
		
		ARegexPart part = token->matches_part(iterator);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "12341234", 1)

		iterator.next();
		iterator.next();
		
		part = token->matches_part(iterator);
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "341234", 2)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenallline)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENALLLINE)

#endif
