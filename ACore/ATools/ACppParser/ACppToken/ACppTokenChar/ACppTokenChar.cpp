#include "ACppTokenChar.hpp"
#include "ACppTokenChar.test.cpp"
#include "ACppTokenChar.test.node.cpp"
#include "ACppTokenChar.debug.cpp"
#include "ACppTokenChar.debug.node.cpp"
#include "ACppTokenChar.exception.cpp"

ACppTokenChar::ACppTokenChar() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .symbol("'")
            .text("\\\\\\\\").may_be_present()
            .any()
            .symbol("'")
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenCharException(e, "ARegexPattern()");
    }
}

ARegexPattern * ACppTokenChar::operator->() {
    return &pattern;
}

ACppTokenChar::~ACppTokenChar() {

}