#include "ACppTokenChar.exception.hpp"

ACppTokenCharException::ACppTokenCharException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenChar";
}

ACppTokenCharException::ACppTokenCharException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenChar");
}

ACppTokenCharException::ACppTokenCharException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenChar");
}

ACppTokenCharException::ACppTokenCharException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenChar");
}

ACppTokenCharException::ACppTokenCharException(const ACppTokenCharException & obj) : AllianceException(obj) {

}

ACppTokenCharException::~ACppTokenCharException() {

}
