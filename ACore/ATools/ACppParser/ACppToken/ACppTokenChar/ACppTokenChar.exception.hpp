#ifndef HPP_ACPPTOKENCHAREXCEPTION
#define HPP_ACPPTOKENCHAREXCEPTION
#include <alliance.exception.hpp>

class ACppTokenCharException : public AllianceException {
public:
	ACppTokenCharException();
	ACppTokenCharException(std::string text);
	ACppTokenCharException(AllianceException & e);
	ACppTokenCharException(AllianceException & e, std::string text);
	ACppTokenCharException(const ACppTokenCharException & obj);
	~ACppTokenCharException();
};

#endif
