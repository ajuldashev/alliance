#ifndef HPP_ACPPTOKENCHAR
#define HPP_ACPPTOKENCHAR
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenChar.exception.hpp"

class ACppTokenChar {
private:
    ARegexPattern pattern;
public:
    ACppTokenChar();
    ARegexPattern * operator->();
    ~ACppTokenChar();
};

#endif
