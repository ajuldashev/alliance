#include "ACppTokenChar.hpp"

#ifdef TEST_ACPPTOKENCHAR
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenChar token;
		ARegexPart part = token->matches_part("'a''b''c''\\n'");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "'a'", 1)

		part = token->matches_part(part.get_finish());
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "'b'", 2)

		part = token->matches_part(part.get_finish());
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "'c'", 3)

		part = token->matches_part(part.get_finish());
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "'\\n'", 4)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenchar)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENCHAR)

#endif
