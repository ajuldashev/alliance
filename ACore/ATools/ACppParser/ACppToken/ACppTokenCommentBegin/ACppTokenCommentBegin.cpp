#include "ACppTokenCommentBegin.hpp"
#include "ACppTokenCommentBegin.test.cpp"
#include "ACppTokenCommentBegin.test.node.cpp"
#include "ACppTokenCommentBegin.debug.cpp"
#include "ACppTokenCommentBegin.debug.node.cpp"
#include "ACppTokenCommentBegin.exception.cpp"

ACppTokenCommentBegin::ACppTokenCommentBegin() {
    try {
        pattern  = ARegexPattern(ARegexConstructor::make()
            .regex("/\\*")
            .any().any_number()
            .to_string()
        );
    } catch(AllianceException & e) {
        throw ACppTokenCommentBeginException(e, "ACppTokenCommentBegin()");
    }
}

ARegexPattern * ACppTokenCommentBegin::operator->() {
    return &pattern;
}

ACppTokenCommentBegin::~ACppTokenCommentBegin() {

}
