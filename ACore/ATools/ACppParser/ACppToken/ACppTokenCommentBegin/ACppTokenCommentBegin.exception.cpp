#include "ACppTokenCommentBegin.exception.hpp"

ACppTokenCommentBeginException::ACppTokenCommentBeginException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenCommentBegin";
}

ACppTokenCommentBeginException::ACppTokenCommentBeginException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenCommentBegin");
}

ACppTokenCommentBeginException::ACppTokenCommentBeginException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenCommentBegin");
}

ACppTokenCommentBeginException::ACppTokenCommentBeginException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenCommentBegin");
}

ACppTokenCommentBeginException::ACppTokenCommentBeginException(const ACppTokenCommentBeginException & obj) : AllianceException(obj) {

}

ACppTokenCommentBeginException::~ACppTokenCommentBeginException() {

}
