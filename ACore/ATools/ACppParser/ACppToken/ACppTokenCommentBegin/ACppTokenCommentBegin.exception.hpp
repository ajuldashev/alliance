#ifndef HPP_ACPPTOKENCOMMENTBEGINEXCEPTION
#define HPP_ACPPTOKENCOMMENTBEGINEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenCommentBeginException : public AllianceException {
public:
	ACppTokenCommentBeginException();
	ACppTokenCommentBeginException(std::string text);
	ACppTokenCommentBeginException(AllianceException & e);
	ACppTokenCommentBeginException(AllianceException & e, std::string text);
	ACppTokenCommentBeginException(const ACppTokenCommentBeginException & obj);
	~ACppTokenCommentBeginException();
};

#endif
