#ifndef HPP_ACPPTOKENCOMMENTBEGIN
#define HPP_ACPPTOKENCOMMENTBEGIN
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenCommentBegin.exception.hpp"

class ACppTokenCommentBegin {
private:
    ARegexPattern pattern;
public:
    ACppTokenCommentBegin();
    ARegexPattern * operator->();
    ~ACppTokenCommentBegin();
};

#endif
