#include "ACppTokenCommentBegin.hpp"

#ifdef TEST_ACPPTOKENCOMMENTBEGIN
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenCommentBegin token;
		ARegexPart part = token->matches_part("/* world");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "/* world", 1)
		
		part = token->matches_part("/ * world");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokencommentbegin)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENCOMMENTBEGIN)

#endif
