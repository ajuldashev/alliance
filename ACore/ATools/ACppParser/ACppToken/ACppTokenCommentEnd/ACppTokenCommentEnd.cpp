#include "ACppTokenCommentEnd.hpp"
#include "ACppTokenCommentEnd.test.cpp"
#include "ACppTokenCommentEnd.test.node.cpp"
#include "ACppTokenCommentEnd.debug.cpp"
#include "ACppTokenCommentEnd.debug.node.cpp"
#include "ACppTokenCommentEnd.exception.cpp"

ACppTokenCommentEnd::ACppTokenCommentEnd() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .any().any_number()
            .group_begin()
                .retrospective_check_negative()
                .regex("//")
            .group_end()
            .regex("\\*/")
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenCommentEndException(e, "ACppTokenCommentEnd()");
    }
}

ARegexPattern * ACppTokenCommentEnd::operator->() {
    return &pattern;
}

ACppTokenCommentEnd::~ACppTokenCommentEnd() {

}
