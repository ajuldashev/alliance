#include "ACppTokenCommentEnd.exception.hpp"

ACppTokenCommentEndException::ACppTokenCommentEndException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenCommentEnd";
}

ACppTokenCommentEndException::ACppTokenCommentEndException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenCommentEnd");
}

ACppTokenCommentEndException::ACppTokenCommentEndException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenCommentEnd");
}

ACppTokenCommentEndException::ACppTokenCommentEndException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenCommentEnd");
}

ACppTokenCommentEndException::ACppTokenCommentEndException(const ACppTokenCommentEndException & obj) : AllianceException(obj) {

}

ACppTokenCommentEndException::~ACppTokenCommentEndException() {

}
