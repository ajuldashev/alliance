#ifndef HPP_ACPPTOKENCOMMENTENDEXCEPTION
#define HPP_ACPPTOKENCOMMENTENDEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenCommentEndException : public AllianceException {
public:
	ACppTokenCommentEndException();
	ACppTokenCommentEndException(std::string text);
	ACppTokenCommentEndException(AllianceException & e);
	ACppTokenCommentEndException(AllianceException & e, std::string text);
	ACppTokenCommentEndException(const ACppTokenCommentEndException & obj);
	~ACppTokenCommentEndException();
};

#endif
