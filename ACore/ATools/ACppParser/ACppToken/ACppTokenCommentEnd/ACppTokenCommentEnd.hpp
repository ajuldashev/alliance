#ifndef HPP_ACPPTOKENCOMMENTEND
#define HPP_ACPPTOKENCOMMENTEND
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenCommentEnd.exception.hpp"

class ACppTokenCommentEnd {
private:
    ARegexPattern pattern;
public:
    ACppTokenCommentEnd();
    ARegexPattern * operator->();
    ~ACppTokenCommentEnd();
};

#endif
