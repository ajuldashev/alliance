#include "ACppTokenCommentInline.hpp"
#include "ACppTokenCommentInline.test.cpp"
#include "ACppTokenCommentInline.test.node.cpp"
#include "ACppTokenCommentInline.debug.cpp"
#include "ACppTokenCommentInline.debug.node.cpp"
#include "ACppTokenCommentInline.exception.cpp"

ACppTokenCommentInline::ACppTokenCommentInline() {
    try {
    pattern  = ARegexPattern(ARegexConstructor::make()
        .regex("/\\*")
        .any().any_number()
        .group_begin()
            .retrospective_check_negative()
            .regex("//")
        .group_end()
        .regex("\\*/")
        .to_string()
    );
    } catch (AllianceException & e) {
        throw ACppTokenCommentInlineException(e, "ACppTokenCommentInline()");
    }
}

ARegexPattern * ACppTokenCommentInline::operator->() {
    return &pattern;
}

ACppTokenCommentInline::~ACppTokenCommentInline() {

}
