#include "ACppTokenCommentInline.exception.hpp"

ACppTokenCommentInlineException::ACppTokenCommentInlineException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenCommentInline";
}

ACppTokenCommentInlineException::ACppTokenCommentInlineException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenCommentInline");
}

ACppTokenCommentInlineException::ACppTokenCommentInlineException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenCommentInline");
}

ACppTokenCommentInlineException::ACppTokenCommentInlineException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenCommentInline");
}

ACppTokenCommentInlineException::ACppTokenCommentInlineException(const ACppTokenCommentInlineException & obj) : AllianceException(obj) {

}

ACppTokenCommentInlineException::~ACppTokenCommentInlineException() {

}
