#ifndef HPP_ACPPTOKENCOMMENTINLINEEXCEPTION
#define HPP_ACPPTOKENCOMMENTINLINEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenCommentInlineException : public AllianceException {
public:
	ACppTokenCommentInlineException();
	ACppTokenCommentInlineException(std::string text);
	ACppTokenCommentInlineException(AllianceException & e);
	ACppTokenCommentInlineException(AllianceException & e, std::string text);
	ACppTokenCommentInlineException(const ACppTokenCommentInlineException & obj);
	~ACppTokenCommentInlineException();
};

#endif
