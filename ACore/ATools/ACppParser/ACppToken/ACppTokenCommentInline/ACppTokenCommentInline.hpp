#ifndef HPP_ACPPTOKENCOMMENTINLINE
#define HPP_ACPPTOKENCOMMENTINLINE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenCommentInline.exception.hpp"

class ACppTokenCommentInline {
private:
    ARegexPattern pattern;
public:
    ACppTokenCommentInline();
    ARegexPattern * operator->();
    ~ACppTokenCommentInline();
};

#endif
