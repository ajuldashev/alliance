#include "ACppTokenCommentInline.hpp"

#ifdef TEST_ACPPTOKENCOMMENTINLINE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenCommentInline token;
		ARegexPart part = token->matches_part("/* world */ 1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "/* world */", 1)

		part = token->matches_part("/* world // */ 1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokencommentinline)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENCOMMENTINLINE)

#endif
