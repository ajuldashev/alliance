#include "ACppTokenCommentLine.hpp"
#include "ACppTokenCommentLine.test.cpp"
#include "ACppTokenCommentLine.test.node.cpp"
#include "ACppTokenCommentLine.debug.cpp"
#include "ACppTokenCommentLine.debug.node.cpp"
#include "ACppTokenCommentLine.exception.cpp"

ACppTokenCommentLine::ACppTokenCommentLine() {
    try {
        pattern  = ARegexPattern(ARegexConstructor::make()
            .regex("//")
            .any().any_number()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenCommentLineException(e, "ACppTokenCommentLine()");
    }
}

ARegexPattern * ACppTokenCommentLine::operator->() {
    return &pattern;
}

ACppTokenCommentLine::~ACppTokenCommentLine() {

}
