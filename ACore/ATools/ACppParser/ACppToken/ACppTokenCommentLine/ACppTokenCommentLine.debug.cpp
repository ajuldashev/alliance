#include "ACppTokenCommentLine.hpp"

#ifdef DEBUG_ACPPTOKENCOMMENTLINE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_acpptokencommentline() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ACPPTOKENCOMMENTLINE );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_acpptokencommentline());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_acpptokencommentline(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ACPPTOKENCOMMENTLINE );
	AIO::write_div_line();
	return false;
}
#endif
