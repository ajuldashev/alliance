#include "ACppTokenCommentLine.exception.hpp"

ACppTokenCommentLineException::ACppTokenCommentLineException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenCommentLine";
}

ACppTokenCommentLineException::ACppTokenCommentLineException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenCommentLine");
}

ACppTokenCommentLineException::ACppTokenCommentLineException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenCommentLine");
}

ACppTokenCommentLineException::ACppTokenCommentLineException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenCommentLine");
}

ACppTokenCommentLineException::ACppTokenCommentLineException(const ACppTokenCommentLineException & obj) : AllianceException(obj) {

}

ACppTokenCommentLineException::~ACppTokenCommentLineException() {

}
