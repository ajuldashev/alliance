#ifndef HPP_ACPPTOKENCOMMENTLINEEXCEPTION
#define HPP_ACPPTOKENCOMMENTLINEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenCommentLineException : public AllianceException {
public:
	ACppTokenCommentLineException();
	ACppTokenCommentLineException(std::string text);
	ACppTokenCommentLineException(AllianceException & e);
	ACppTokenCommentLineException(AllianceException & e, std::string text);
	ACppTokenCommentLineException(const ACppTokenCommentLineException & obj);
	~ACppTokenCommentLineException();
};

#endif
