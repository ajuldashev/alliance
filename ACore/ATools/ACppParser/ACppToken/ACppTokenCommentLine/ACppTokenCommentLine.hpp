#ifndef HPP_ACPPTOKENCOMMENTLINE
#define HPP_ACPPTOKENCOMMENTLINE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenCommentLine.exception.hpp"

class ACppTokenCommentLine {
private:
    ARegexPattern pattern;
public:
    ACppTokenCommentLine();
    ARegexPattern * operator->();
    ~ACppTokenCommentLine();
};

#endif
