#include "ACppTokenCommentLine.hpp"

#ifdef TEST_ACPPTOKENCOMMENTLINE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenCommentLine token;
		ARegexPart part = token->matches_part("//1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "//1234", 1)

		part = token->matches_part(" //1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)

		part = token->matches_part("/ /1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 3)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokencommentline)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENCOMMENTLINE)

#endif
