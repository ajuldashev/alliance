#include "ACppTokenKeySymbol.hpp"
#include "ACppTokenKeySymbol.test.cpp"
#include "ACppTokenKeySymbol.test.node.cpp"
#include "ACppTokenKeySymbol.debug.cpp"
#include "ACppTokenKeySymbol.debug.node.cpp"
#include "ACppTokenKeySymbol.exception.cpp"

ACppTokenKeySymbol::ACppTokenKeySymbol() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .group_begin()
                .regex("\\{")
            .group_or()
                .regex("\\[")
            .group_or()
                .regex("\\(")
            .group_or()
                .regex("\\)")
            .group_or()
                .regex("\\]")
            .group_or()
                .regex("\\}")
            .group_or()
                .regex("\\.")
            .group_or()
                .regex("\\,")
            .group_or()
                .regex("\\;")
            .group_or()
                .regex("\\?")
            .group_or()
                .regex("\\:").count(1,2)
            .group_or()
                .regex("\\=").count(1,2)
            .group_or()
                .regex("\\#").count(1,2)
            .group_or()
                .regex("\\/")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\*")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\%")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\!")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\~")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\^")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\<")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\>")
                .regex("\\=").may_be_present()
            .group_or()
                .regex("\\+")
                .group_begin()
                    .regex("\\+")
                .group_or()
                    .regex("\\=")
                .group_end().may_be_present()
            .group_or()
                .regex("\\-")
                .group_begin()
                    .regex("\\-")
                .group_or()
                    .regex("\\>")
                .group_or()
                    .regex("\\=")
                .group_end().may_be_present()
            .group_or()
                .regex("\\|")
                .group_begin()
                    .regex("\\|")
                .group_or()
                    .regex("\\=")
                .group_end().may_be_present()
            .group_or()
                .regex("\\&")
                .group_begin()
                    .regex("\\&")
                .group_or()
                    .regex("\\=")
                .group_end().may_be_present()
            .group_end()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenKeySymbolException(e, "ACppTokenKeySymbol()");
    }
}

ARegexPattern * ACppTokenKeySymbol::operator->() {
    return &pattern;
}

ACppTokenKeySymbol::~ACppTokenKeySymbol() {

}
