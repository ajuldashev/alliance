#include "ACppTokenKeySymbol.exception.hpp"

ACppTokenKeySymbolException::ACppTokenKeySymbolException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenKeySymbol";
}

ACppTokenKeySymbolException::ACppTokenKeySymbolException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenKeySymbol");
}

ACppTokenKeySymbolException::ACppTokenKeySymbolException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenKeySymbol");
}

ACppTokenKeySymbolException::ACppTokenKeySymbolException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenKeySymbol");
}

ACppTokenKeySymbolException::ACppTokenKeySymbolException(const ACppTokenKeySymbolException & obj) : AllianceException(obj) {

}

ACppTokenKeySymbolException::~ACppTokenKeySymbolException() {

}
