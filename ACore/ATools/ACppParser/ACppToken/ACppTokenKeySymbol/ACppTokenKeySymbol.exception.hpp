#ifndef HPP_ACPPTOKENKEYSYMBOLEXCEPTION
#define HPP_ACPPTOKENKEYSYMBOLEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenKeySymbolException : public AllianceException {
public:
	ACppTokenKeySymbolException();
	ACppTokenKeySymbolException(std::string text);
	ACppTokenKeySymbolException(AllianceException & e);
	ACppTokenKeySymbolException(AllianceException & e, std::string text);
	ACppTokenKeySymbolException(const ACppTokenKeySymbolException & obj);
	~ACppTokenKeySymbolException();
};

#endif
