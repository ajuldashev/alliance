#ifndef HPP_ACPPTOKENKEYSYMBOL
#define HPP_ACPPTOKENKEYSYMBOL
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenKeySymbol.exception.hpp"

class ACppTokenKeySymbol {
private:
    ARegexPattern pattern;
public:
    ACppTokenKeySymbol();
    ARegexPattern * operator->();
    ~ACppTokenKeySymbol();
};

#endif
