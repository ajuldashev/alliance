#include "ACppTokenKeySymbol.hpp"

#ifdef TEST_ACPPTOKENKEYSYMBOL
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("##123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "##", 1)

		part = token->matches_part("#123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "#", 2)

		part = token->matches_part("# #123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "#", 3)

		part = token->matches_part("==123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "==", 4)

		part = token->matches_part("=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "=", 5)

		part = token->matches_part("= =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "=", 6)

		part = token->matches_part("::123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "::", 7)

		part = token->matches_part(":123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ":", 8)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(2)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("{}123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "{", 1)

		part = token->matches_part("}{123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "}", 2)

		part = token->matches_part("[]123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "[", 3)

		part = token->matches_part("]]123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "]", 4)

		part = token->matches_part("((123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "(", 5)

		part = token->matches_part(") (123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ")", 6)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(3)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part(".123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ".", 1)

		part = token->matches_part("F123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)

		part = token->matches_part(",123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ",", 3)

		part = token->matches_part(";123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ";", 4)

		part = token->matches_part(" ;123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 5)

		part = token->matches_part("?123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "?", 6)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(4)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("/123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "/", 1)

		part = token->matches_part("/=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "/=", 2)

		part = token->matches_part("/ =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "/", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(5)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("*123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "*", 1)

		part = token->matches_part("*=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "*=", 2)

		part = token->matches_part("* =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "*", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(6)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("%123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "%", 1)

		part = token->matches_part("%=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "%=", 2)

		part = token->matches_part("% =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "%", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(7)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("!123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "!", 1)

		part = token->matches_part("!=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "!=", 2)

		part = token->matches_part("! =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "!", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(8)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("~123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "~", 1)

		part = token->matches_part("~=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "~=", 2)

		part = token->matches_part("~ =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "~", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(9)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("^123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "^", 1)

		part = token->matches_part("^=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "^=", 2)

		part = token->matches_part("^ =123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "^", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(10)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("<123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "<", 1)

		part = token->matches_part("<=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "<=", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(11)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part(">123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ">", 1)

		part = token->matches_part(">=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ">=", 2)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(12)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("+123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "+", 1)

		part = token->matches_part("+=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "+=", 2)

		part = token->matches_part("++123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "++", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(13)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("-123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "-", 1)

		part = token->matches_part("-=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "-=", 2)

		part = token->matches_part("--123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "--", 3)

		part = token->matches_part("->123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "->", 4)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(14)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("|123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "|", 1)

		part = token->matches_part("|=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "|=", 2)

		part = token->matches_part("||123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "||", 3)
	}
TEST_FUNCTION_END

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(15)
	{
		ACppTokenKeySymbol token;
		ARegexPart part = token->matches_part("&123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "&", 1)

		part = token->matches_part("&=123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "&=", 2)

		part = token->matches_part("&&123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "&&", 3)
	}
TEST_FUNCTION_END

TEST_FUNCTION_MAIN_BEGIN(acpptokenkeysymbol)

	TEST_CALL(1)
	TEST_CALL(2)
	TEST_CALL(3)
	TEST_CALL(4)
	TEST_CALL(5)
	TEST_CALL(6)
	TEST_CALL(7)
	TEST_CALL(8)
	TEST_CALL(9)
	TEST_CALL(10)
	TEST_CALL(11)
	TEST_CALL(12)
	TEST_CALL(13)
	TEST_CALL(14)
	TEST_CALL(15)

TEST_FUNCTION_MAIN_END(ACPPTOKENKEYSYMBOL)

#endif
