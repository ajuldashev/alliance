#include "ACppTokenKeyType.hpp"
#include "ACppTokenKeyType.test.cpp"
#include "ACppTokenKeyType.test.node.cpp"
#include "ACppTokenKeyType.debug.cpp"
#include "ACppTokenKeyType.debug.node.cpp"
#include "ACppTokenKeyType.exception.cpp"

ACppTokenKeyType::ACppTokenKeyType() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .group_begin()
                .regex("char")
            .group_or()    
                .regex("double")
            .group_or()    
                .regex("float")
            .group_or()    
                .regex("int")
            .group_or()    
                .regex("long")
            .group_or()    
                .regex("short")
            .group_or()    
                .regex("void")
            .group_or()    
                .regex("bool")  
            .group_or()    
                .regex("wchar_t")
            .group_end()
            .group_begin()
                .advanced_check_negative()
                .begin()
                .regex_class("A-Za-z0-9_")
            .group_end()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenKeyTypeException(e, "ACppTokenKeyType()");
    }
}

ARegexPattern * ACppTokenKeyType::operator->() {
    return &pattern;
}

ACppTokenKeyType::~ACppTokenKeyType() {

}

