#include "ACppTokenKeyType.exception.hpp"

ACppTokenKeyTypeException::ACppTokenKeyTypeException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenKeyType";
}

ACppTokenKeyTypeException::ACppTokenKeyTypeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenKeyType");
}

ACppTokenKeyTypeException::ACppTokenKeyTypeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenKeyType");
}

ACppTokenKeyTypeException::ACppTokenKeyTypeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenKeyType");
}

ACppTokenKeyTypeException::ACppTokenKeyTypeException(const ACppTokenKeyTypeException & obj) : AllianceException(obj) {

}

ACppTokenKeyTypeException::~ACppTokenKeyTypeException() {

}
