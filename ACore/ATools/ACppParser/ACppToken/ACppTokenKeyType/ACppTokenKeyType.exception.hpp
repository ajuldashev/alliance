#ifndef HPP_ACPPTOKENKEYTYPEEXCEPTION
#define HPP_ACPPTOKENKEYTYPEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenKeyTypeException : public AllianceException {
public:
	ACppTokenKeyTypeException();
	ACppTokenKeyTypeException(std::string text);
	ACppTokenKeyTypeException(AllianceException & e);
	ACppTokenKeyTypeException(AllianceException & e, std::string text);
	ACppTokenKeyTypeException(const ACppTokenKeyTypeException & obj);
	~ACppTokenKeyTypeException();
};

#endif
