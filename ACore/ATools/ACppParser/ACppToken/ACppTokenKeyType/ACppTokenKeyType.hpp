#ifndef HPP_ACPPTOKENKEYTYPE
#define HPP_ACPPTOKENKEYTYPE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenKeyType.exception.hpp"

class ACppTokenKeyType {
private:
    ARegexPattern pattern;
public:
    ACppTokenKeyType();
    ARegexPattern * operator->();
    ~ACppTokenKeyType();
};

#endif
