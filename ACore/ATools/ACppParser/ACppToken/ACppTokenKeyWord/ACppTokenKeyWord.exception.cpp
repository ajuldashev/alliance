#include "ACppTokenKeyWord.exception.hpp"

ACppTokenKeyWordException::ACppTokenKeyWordException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenKeyWord";
}

ACppTokenKeyWordException::ACppTokenKeyWordException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenKeyWord");
}

ACppTokenKeyWordException::ACppTokenKeyWordException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenKeyWord");
}

ACppTokenKeyWordException::ACppTokenKeyWordException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenKeyWord");
}

ACppTokenKeyWordException::ACppTokenKeyWordException(const ACppTokenKeyWordException & obj) : AllianceException(obj) {

}

ACppTokenKeyWordException::~ACppTokenKeyWordException() {

}
