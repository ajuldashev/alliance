#ifndef HPP_ACPPTOKENKEYWORDEXCEPTION
#define HPP_ACPPTOKENKEYWORDEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenKeyWordException : public AllianceException {
public:
	ACppTokenKeyWordException();
	ACppTokenKeyWordException(std::string text);
	ACppTokenKeyWordException(AllianceException & e);
	ACppTokenKeyWordException(AllianceException & e, std::string text);
	ACppTokenKeyWordException(const ACppTokenKeyWordException & obj);
	~ACppTokenKeyWordException();
};

#endif
