#ifndef HPP_ACPPTOKENKEYWORD
#define HPP_ACPPTOKENKEYWORD
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenKeyWord.exception.hpp"

class ACppTokenKeyWord {
private:
    ARegexPattern pattern;
public:
    ACppTokenKeyWord();
    ARegexPattern * operator->();
    ~ACppTokenKeyWord();
};

#endif
