#include "ACppTokenKeyWord.hpp"

#ifdef TEST_ACPPTOKENKEYWORD
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenKeyWord token;
		ARegexPart part = token->matches_part("template");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "template", 1)

		part = token->matches_part("template<");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "template", 2)

		part = token->matches_part("template<A>");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "template", 3)

		part = token->matches_part("templateA");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 4)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenkeyword)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENKEYWORD)

#endif
