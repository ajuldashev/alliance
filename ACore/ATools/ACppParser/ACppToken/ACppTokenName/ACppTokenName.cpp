#include "ACppTokenName.hpp"
#include "ACppTokenName.test.cpp"
#include "ACppTokenName.test.node.cpp"
#include "ACppTokenName.debug.cpp"
#include "ACppTokenName.debug.node.cpp"
#include "ACppTokenName.exception.cpp"

ACppTokenName::ACppTokenName() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .regex_class("A-Za-z_")
            .regex_class("A-Za-z0-9_").any_number()
            .group_begin()
                .advanced_check_negative()
                .begin()
                .regex_class("A-Za-z0-9_")
            .group_end()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenNameException(e, "ACppTokenName()");
    }
}

ARegexPattern * ACppTokenName::operator->() {
    return &pattern;
}

ACppTokenName::~ACppTokenName() {

}
