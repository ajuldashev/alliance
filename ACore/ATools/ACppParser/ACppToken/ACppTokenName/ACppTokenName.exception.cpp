#include "ACppTokenName.exception.hpp"

ACppTokenNameException::ACppTokenNameException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenName";
}

ACppTokenNameException::ACppTokenNameException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenName");
}

ACppTokenNameException::ACppTokenNameException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenName");
}

ACppTokenNameException::ACppTokenNameException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenName");
}

ACppTokenNameException::ACppTokenNameException(const ACppTokenNameException & obj) : AllianceException(obj) {

}

ACppTokenNameException::~ACppTokenNameException() {

}
