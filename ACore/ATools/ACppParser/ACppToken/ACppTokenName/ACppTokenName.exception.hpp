#ifndef HPP_ACPPTOKENNAMEEXCEPTION
#define HPP_ACPPTOKENNAMEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenNameException : public AllianceException {
public:
	ACppTokenNameException();
	ACppTokenNameException(std::string text);
	ACppTokenNameException(AllianceException & e);
	ACppTokenNameException(AllianceException & e, std::string text);
	ACppTokenNameException(const ACppTokenNameException & obj);
	~ACppTokenNameException();
};

#endif
