#ifndef HPP_ACPPTOKENNAME
#define HPP_ACPPTOKENNAME
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenName.exception.hpp"

class ACppTokenName {
private:
    ARegexPattern pattern;
public:
    ACppTokenName();
    ARegexPattern * operator->();
    ~ACppTokenName();
};

#endif
