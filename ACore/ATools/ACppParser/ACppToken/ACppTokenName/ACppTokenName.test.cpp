#include "ACppTokenName.hpp"

#ifdef TEST_ACPPTOKENNAME
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenName token;
		ARegexPart part = token->matches_part("name;");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "name", 1)
		
		part = token->matches_part("F21");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "F21", 2)
	
		part = token->matches_part("2F21");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 3)

		part = token->matches_part("get(int i)");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "get", 4)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenname)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENNAME)

#endif
