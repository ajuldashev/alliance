#include "ACppTokenNumberDouble.hpp"
#include "ACppTokenNumberDouble.test.cpp"
#include "ACppTokenNumberDouble.test.node.cpp"
#include "ACppTokenNumberDouble.debug.cpp"
#include "ACppTokenNumberDouble.debug.node.cpp"
#include "ACppTokenNumberDouble.exception.cpp"

ACppTokenNumberDouble::ACppTokenNumberDouble() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .group_begin()
                .digital().at_least_one()
                .regex("\\.")
                .digital().any_number()
            .group_or()
                .digital().any_number()
                .regex("\\.")
                .digital().at_least_one()
            .group_end()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenNumberDoubleException(e, "ACppTokenNumberDouble()");
    }
}

ARegexPattern * ACppTokenNumberDouble::operator->() {
    return &pattern;
}

ACppTokenNumberDouble::~ACppTokenNumberDouble() {

}
