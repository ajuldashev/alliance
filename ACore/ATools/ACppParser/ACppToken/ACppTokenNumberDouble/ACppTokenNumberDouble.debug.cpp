#include "ACppTokenNumberDouble.hpp"

#ifdef DEBUG_ACPPTOKENNUMBERDOUBLE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_acpptokennumberdouble() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ACPPTOKENNUMBERDOUBLE );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_acpptokennumberdouble());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_acpptokennumberdouble(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ACPPTOKENNUMBERDOUBLE );
	AIO::write_div_line();
	return false;
}
#endif
