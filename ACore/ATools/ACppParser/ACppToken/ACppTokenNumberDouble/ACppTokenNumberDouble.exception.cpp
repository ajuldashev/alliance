#include "ACppTokenNumberDouble.exception.hpp"

ACppTokenNumberDoubleException::ACppTokenNumberDoubleException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenNumberDouble";
}

ACppTokenNumberDoubleException::ACppTokenNumberDoubleException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenNumberDouble");
}

ACppTokenNumberDoubleException::ACppTokenNumberDoubleException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenNumberDouble");
}

ACppTokenNumberDoubleException::ACppTokenNumberDoubleException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenNumberDouble");
}

ACppTokenNumberDoubleException::ACppTokenNumberDoubleException(const ACppTokenNumberDoubleException & obj) : AllianceException(obj) {

}

ACppTokenNumberDoubleException::~ACppTokenNumberDoubleException() {

}
