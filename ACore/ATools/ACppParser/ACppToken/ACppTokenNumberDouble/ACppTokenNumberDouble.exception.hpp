#ifndef HPP_ACPPTOKENNUMBERDOUBLEEXCEPTION
#define HPP_ACPPTOKENNUMBERDOUBLEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenNumberDoubleException : public AllianceException {
public:
	ACppTokenNumberDoubleException();
	ACppTokenNumberDoubleException(std::string text);
	ACppTokenNumberDoubleException(AllianceException & e);
	ACppTokenNumberDoubleException(AllianceException & e, std::string text);
	ACppTokenNumberDoubleException(const ACppTokenNumberDoubleException & obj);
	~ACppTokenNumberDoubleException();
};

#endif
