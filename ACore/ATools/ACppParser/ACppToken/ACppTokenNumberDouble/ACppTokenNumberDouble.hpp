#ifndef HPP_ACPPTOKENNUMBERDOUBLE
#define HPP_ACPPTOKENNUMBERDOUBLE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenNumberDouble.exception.hpp"

class ACppTokenNumberDouble {
private:
    ARegexPattern pattern;
public:
    ACppTokenNumberDouble();
    ARegexPattern * operator->();
    ~ACppTokenNumberDouble();
};

#endif
