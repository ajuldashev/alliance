#include "ACppTokenNumberDouble.hpp"

#ifdef TEST_ACPPTOKENNUMBERDOUBLE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenNumberDouble token;
		ARegexPart part = token->matches_part(".123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == ".123", 1)

		part = token->matches_part("123.456");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "123.456", 2)

		part = token->matches_part("123.");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "123.", 3)

		part = token->matches_part("123");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 4)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokennumberdouble)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENNUMBERDOUBLE)

#endif
