#include "ACppTokenNumberHex.hpp"
#include "ACppTokenNumberHex.test.cpp"
#include "ACppTokenNumberHex.test.node.cpp"
#include "ACppTokenNumberHex.debug.cpp"
#include "ACppTokenNumberHex.debug.node.cpp"
#include "ACppTokenNumberHex.exception.cpp"

ACppTokenNumberHex::ACppTokenNumberHex() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .text("0x")
            .regex_class("0-9A-Fa-f").at_least_one()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenNumberHexException(e, "ACppTokenNumberHex()");
    }
}

ARegexPattern * ACppTokenNumberHex::operator->() {
    return &pattern;
}

ACppTokenNumberHex::~ACppTokenNumberHex() {

}
