#include "ACppTokenNumberHex.hpp"

#ifdef DEBUG_ACPPTOKENNUMBERHEX
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_acpptokennumberhex() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ACPPTOKENNUMBERHEX );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_acpptokennumberhex());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_acpptokennumberhex(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ACPPTOKENNUMBERHEX );
	AIO::write_div_line();
	return false;
}
#endif
