#include "ACppTokenNumberHex.exception.hpp"

ACppTokenNumberHexException::ACppTokenNumberHexException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenNumberHex";
}

ACppTokenNumberHexException::ACppTokenNumberHexException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenNumberHex");
}

ACppTokenNumberHexException::ACppTokenNumberHexException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenNumberHex");
}

ACppTokenNumberHexException::ACppTokenNumberHexException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenNumberHex");
}

ACppTokenNumberHexException::ACppTokenNumberHexException(const ACppTokenNumberHexException & obj) : AllianceException(obj) {

}

ACppTokenNumberHexException::~ACppTokenNumberHexException() {

}
