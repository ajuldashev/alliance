#ifndef HPP_ACPPTOKENNUMBERHEXEXCEPTION
#define HPP_ACPPTOKENNUMBERHEXEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenNumberHexException : public AllianceException {
public:
	ACppTokenNumberHexException();
	ACppTokenNumberHexException(std::string text);
	ACppTokenNumberHexException(AllianceException & e);
	ACppTokenNumberHexException(AllianceException & e, std::string text);
	ACppTokenNumberHexException(const ACppTokenNumberHexException & obj);
	~ACppTokenNumberHexException();
};

#endif
