#ifndef HPP_ACPPTOKENNUMBERHEX
#define HPP_ACPPTOKENNUMBERHEX
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenNumberHex.exception.hpp"

class ACppTokenNumberHex {
private:
    ARegexPattern pattern;
public:
    ACppTokenNumberHex();
    ARegexPattern * operator->();
    ~ACppTokenNumberHex();
};

#endif
