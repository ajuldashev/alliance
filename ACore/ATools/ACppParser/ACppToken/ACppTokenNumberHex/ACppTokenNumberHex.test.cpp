#include "ACppTokenNumberHex.hpp"

#ifdef TEST_ACPPTOKENNUMBERHEX
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenNumberHex token;
		ARegexPart part = token->matches_part("0");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 1)

		part = token->matches_part("0x");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)

		part = token->matches_part("0x1");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "0x1", 3)

		part = token->matches_part("0x1f");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "0x1f", 4)

		part = token->matches_part("0x1fG");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "0x1f", 5)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokennumberhex)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENNUMBERHEX)

#endif
