#include "ACppTokenNumberInteger.hpp"
#include "ACppTokenNumberInteger.test.cpp"
#include "ACppTokenNumberInteger.test.node.cpp"
#include "ACppTokenNumberInteger.debug.cpp"
#include "ACppTokenNumberInteger.debug.node.cpp"
#include "ACppTokenNumberInteger.exception.cpp"

ACppTokenNumberInteger::ACppTokenNumberInteger() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .digital().at_least_one()
            .to_string()
        );
    } catch(AllianceException & e) {
        throw ACppTokenNumberIntegerException(e, "ACppTokenNumberInteger()");
    }
}

ARegexPattern * ACppTokenNumberInteger::operator->() {
    return &pattern;
}

ACppTokenNumberInteger::~ACppTokenNumberInteger() {

}
