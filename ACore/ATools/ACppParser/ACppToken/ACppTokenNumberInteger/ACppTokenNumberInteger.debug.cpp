#include "ACppTokenNumberInteger.hpp"

#ifdef DEBUG_ACPPTOKENNUMBERINTEGER
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_acpptokennumberinteger() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ACPPTOKENNUMBERINTEGER );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_acpptokennumberinteger());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_acpptokennumberinteger(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ACPPTOKENNUMBERINTEGER );
	AIO::write_div_line();
	return false;
}
#endif
