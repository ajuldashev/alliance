#include "ACppTokenNumberInteger.exception.hpp"

ACppTokenNumberIntegerException::ACppTokenNumberIntegerException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenNumberInteger";
}

ACppTokenNumberIntegerException::ACppTokenNumberIntegerException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenNumberInteger");
}

ACppTokenNumberIntegerException::ACppTokenNumberIntegerException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenNumberInteger");
}

ACppTokenNumberIntegerException::ACppTokenNumberIntegerException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenNumberInteger");
}

ACppTokenNumberIntegerException::ACppTokenNumberIntegerException(const ACppTokenNumberIntegerException & obj) : AllianceException(obj) {

}

ACppTokenNumberIntegerException::~ACppTokenNumberIntegerException() {

}
