#ifndef HPP_ACPPTOKENNUMBERINTEGEREXCEPTION
#define HPP_ACPPTOKENNUMBERINTEGEREXCEPTION
#include <alliance.exception.hpp>

class ACppTokenNumberIntegerException : public AllianceException {
public:
	ACppTokenNumberIntegerException();
	ACppTokenNumberIntegerException(std::string text);
	ACppTokenNumberIntegerException(AllianceException & e);
	ACppTokenNumberIntegerException(AllianceException & e, std::string text);
	ACppTokenNumberIntegerException(const ACppTokenNumberIntegerException & obj);
	~ACppTokenNumberIntegerException();
};

#endif
