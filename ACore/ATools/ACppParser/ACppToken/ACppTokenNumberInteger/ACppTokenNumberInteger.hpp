#ifndef HPP_ACPPTOKENNUMBERINTEGER
#define HPP_ACPPTOKENNUMBERINTEGER
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenNumberInteger.exception.hpp"

class ACppTokenNumberInteger {
private:
    ARegexPattern pattern;
public:
    ACppTokenNumberInteger();
    ARegexPattern * operator->();
    ~ACppTokenNumberInteger();
};

#endif
