#include "ACppTokenNumberInteger.hpp"

#ifdef TEST_ACPPTOKENNUMBERINTEGER
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenNumberInteger token;
		ARegexPart part = token->matches_part("0");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "0", 1)

		part = token->matches_part("1234");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "1234", 2)

		part = token->matches_part("1234.5678");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "1234", 3)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokennumberinteger)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENNUMBERINTEGER)

#endif
