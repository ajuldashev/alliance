#include "ACppTokenSpace.hpp"
#include "ACppTokenSpace.test.cpp"
#include "ACppTokenSpace.test.node.cpp"
#include "ACppTokenSpace.debug.cpp"
#include "ACppTokenSpace.debug.node.cpp"
#include "ACppTokenSpace.exception.cpp"

ACppTokenSpace::ACppTokenSpace() {
    try {
        pattern = ARegexPattern(
            ARegexConstructor::make()
            .space().at_least_one()
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenSpaceException(e, "ACppTokenSpace()");
    }
}

ARegexPattern * ACppTokenSpace::operator->() {
    return &pattern;
}

ACppTokenSpace::~ACppTokenSpace() {

}
