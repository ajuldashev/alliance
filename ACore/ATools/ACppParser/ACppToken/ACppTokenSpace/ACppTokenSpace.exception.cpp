#include "ACppTokenSpace.exception.hpp"

ACppTokenSpaceException::ACppTokenSpaceException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenSpace";
}

ACppTokenSpaceException::ACppTokenSpaceException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenSpace");
}

ACppTokenSpaceException::ACppTokenSpaceException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenSpace");
}

ACppTokenSpaceException::ACppTokenSpaceException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenSpace");
}

ACppTokenSpaceException::ACppTokenSpaceException(const ACppTokenSpaceException & obj) : AllianceException(obj) {

}

ACppTokenSpaceException::~ACppTokenSpaceException() {

}
