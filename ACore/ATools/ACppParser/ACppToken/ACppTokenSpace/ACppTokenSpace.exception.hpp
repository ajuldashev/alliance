#ifndef HPP_ACPPTOKENSPACEEXCEPTION
#define HPP_ACPPTOKENSPACEEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenSpaceException : public AllianceException {
public:
	ACppTokenSpaceException();
	ACppTokenSpaceException(std::string text);
	ACppTokenSpaceException(AllianceException & e);
	ACppTokenSpaceException(AllianceException & e, std::string text);
	ACppTokenSpaceException(const ACppTokenSpaceException & obj);
	~ACppTokenSpaceException();
};

#endif
