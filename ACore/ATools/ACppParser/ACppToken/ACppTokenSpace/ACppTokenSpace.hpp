#ifndef HPP_ACPPTOKENSPACE
#define HPP_ACPPTOKENSPACE
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenSpace.exception.hpp"

class ACppTokenSpace {
private:
    ARegexPattern pattern;
public:
    ACppTokenSpace();
    ARegexPattern * operator->();
    ~ACppTokenSpace();
};

#endif
