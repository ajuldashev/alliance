#include "ACppTokenSpace.hpp"

#ifdef TEST_ACPPTOKENSPACE
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenSpace token;
		ARegexPart part = token->matches_part(" \t");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == " \t", 1)

		part = token->matches_part("");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 2)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenspace)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENSPACE)

#endif
