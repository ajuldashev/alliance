#include "ACppTokenString.hpp"
#include "ACppTokenString.test.cpp"
#include "ACppTokenString.test.node.cpp"
#include "ACppTokenString.debug.cpp"
#include "ACppTokenString.debug.node.cpp"
#include "ACppTokenString.exception.cpp"

ACppTokenString::ACppTokenString() {
    try {
        pattern = ARegexPattern(ARegexConstructor::make()
            .regex("\"")

            .group_begin()
                .regex_class("^\"")
            .group_or()
                .group_begin().retrospective_check_positive()
                    .group_begin()
                        .regex("\\\\\\\\")
                    .group_end().any_number()
                    .regex("\\\\")
                    .end()
                .group_end()
                .regex("\"")
            .group_end().any_number()
            
            .group_begin().retrospective_check_negative()
                .group_begin()
                    .regex("\\\\\\\\")
                .group_end().any_number()
                .regex("\\\\")
                .end()
            .group_end()
            .regex("\"")
            
            .to_string()
        );
    } catch (AllianceException & e) {
        throw ACppTokenStringException(e, "ACppTokenString()");
    }
}

ARegexPattern * ACppTokenString::operator->() {
    return &pattern;
}

ACppTokenString::~ACppTokenString() {

}
