#include "ACppTokenString.exception.hpp"

ACppTokenStringException::ACppTokenStringException() : AllianceException() {
	exception = "Alliance-Exception-ACppTokenString";
}

ACppTokenStringException::ACppTokenStringException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ACppTokenString");
}

ACppTokenStringException::ACppTokenStringException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ACppTokenString");
}

ACppTokenStringException::ACppTokenStringException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ACppTokenString");
}

ACppTokenStringException::ACppTokenStringException(const ACppTokenStringException & obj) : AllianceException(obj) {

}

ACppTokenStringException::~ACppTokenStringException() {

}
