#ifndef HPP_ACPPTOKENSTRINGEXCEPTION
#define HPP_ACPPTOKENSTRINGEXCEPTION
#include <alliance.exception.hpp>

class ACppTokenStringException : public AllianceException {
public:
	ACppTokenStringException();
	ACppTokenStringException(std::string text);
	ACppTokenStringException(AllianceException & e);
	ACppTokenStringException(AllianceException & e, std::string text);
	ACppTokenStringException(const ACppTokenStringException & obj);
	~ACppTokenStringException();
};

#endif
