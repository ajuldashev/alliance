#ifndef HPP_ACPPTOKENSTRING
#define HPP_ACPPTOKENSTRING
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ACppTokenString.exception.hpp"

class ACppTokenString {
private:
    ARegexPattern pattern;
public:
    ACppTokenString();
    ARegexPattern * operator->();
    ~ACppTokenString();
};

#endif
