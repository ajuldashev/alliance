#include "ACppTokenString.hpp"

#ifdef TEST_ACPPTOKENSTRING
#include <ACore/ABase/AIO/AIO.hpp>

/** <doc-test-static-function>
 *
 */
TEST_FUNCTION_BEGIN(1)
	{
		ACppTokenString token;
		ARegexPart part = token->matches_part("\"Hello world\" not string");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "\"Hello world\"", 1)

		part = token->matches_part("\"Hello world\\\" #2 \" not string");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches()->get(0) == "\"Hello world\\\" #2 \"", 2)

		part = token->matches_part("\"Hello world\\\" #3 not string");
		/** <doc-test-info>
		 *
		 */
		TEST_ALERT(part.get_matches().is_empty(), 3)
	}
TEST_FUNCTION_END


TEST_FUNCTION_MAIN_BEGIN(acpptokenstring)

	TEST_CALL(1)

TEST_FUNCTION_MAIN_END(ACPPTOKENSTRING)

#endif
