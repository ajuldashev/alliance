#include "AHtmlConstructor.hpp"
#include "AHtmlConstructor.test.cpp"
#include "AHtmlConstructor.test.node.cpp"
#include "AHtmlConstructor.debug.cpp"
#include "AHtmlConstructor.debug.node.cpp"
#include "AHtmlConstructor.exception.cpp"

AString AHtmlConstructor::make_space() {
    return make_space(count_tab);
}

AString AHtmlConstructor::make_space(u64int count) {
    AString result;
    for (u64int i = 0; i < count; i += 1) {
        result += "\t";
    }
    return result;
}

void AHtmlConstructor::close_begin_tag() {
    if (!tag_is_done) {
        html += ">\n";
        tag_is_done = true;
    }
}

AString AHtmlConstructor::fix_space(AString text) {
    for (u64int i = 0; i < text.length(); i += 1) {
        if (text[i] == '\n') {
            text = text.insert(make_space(), i + 1);
        }
    }
    return text;
}

AHtmlConstructor::AHtmlConstructor() {
    tag_is_done = true;
    count_tab = 0;
}

AHtmlConstructor AHtmlConstructor::make() {
    return AHtmlConstructor();
}

AHtmlConstructor & AHtmlConstructor::begin(AString tag) {
    close_begin_tag();
    html += make_space() + "<" + tag;
    tag_is_done = false;
    count_tab += 1;
    return *this;
}

AHtmlConstructor & AHtmlConstructor::attribute(AString name, AString value) {
    html += " " + name + "=\"" + value + "\""; 
    return *this;
}

AHtmlConstructor & AHtmlConstructor::text(AString text) {
    close_begin_tag();
    html += make_space() + fix_space(text) + "\n";
    return *this;
}

AHtmlConstructor & AHtmlConstructor::end(AString tag) {
    close_begin_tag();
    count_tab -= 1;
    html += make_space() + "</" + tag + ">\n";
    return *this;
}

AHtmlConstructor & AHtmlConstructor::tag(AString tag) {
    close_begin_tag();
    tag_is_done = false;
    html += make_space() + "<" + tag;
    return *this;
}

AHtmlConstructor & AHtmlConstructor::add_space(u64int count) {
    close_begin_tag();
    html = html.insert(make_space(count), 0);
    for (u64int i = 0; i < html.length(); i += 1) {
        if (html[i] == '\n' && i < html.length() - 1) {
            html = html.insert(make_space(count), i + 1);
        }
    }
    return *this;
}

AString AHtmlConstructor::to_text() {
    close_begin_tag();
    return html;
}

AHtmlConstructor::~AHtmlConstructor() {
    
}
