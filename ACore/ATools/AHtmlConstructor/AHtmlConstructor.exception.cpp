#include "AHtmlConstructor.exception.hpp"

AHtmlConstructorException::AHtmlConstructorException() : AllianceException() {
	exception = "Alliance-Exception-AHtmlConstructor";
}

AHtmlConstructorException::AHtmlConstructorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AHtmlConstructor");
}

AHtmlConstructorException::AHtmlConstructorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AHtmlConstructor");
}

AHtmlConstructorException::AHtmlConstructorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AHtmlConstructor");
}

AHtmlConstructorException::AHtmlConstructorException(const AHtmlConstructorException & obj) : AllianceException(obj) {

}

AHtmlConstructorException::~AHtmlConstructorException() {

}
