#ifndef HPP_AHTMLCONSTRUCTOREXCEPTION
#define HPP_AHTMLCONSTRUCTOREXCEPTION
#include <alliance.exception.hpp>

class AHtmlConstructorException : public AllianceException {
public:
	AHtmlConstructorException();
	AHtmlConstructorException(std::string text);
	AHtmlConstructorException(AllianceException & e);
	AHtmlConstructorException(AllianceException & e, std::string text);
	AHtmlConstructorException(const AHtmlConstructorException & obj);
	~AHtmlConstructorException();
};

#endif
