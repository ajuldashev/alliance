#ifndef HPP_AHTMLCONSTRUCTOR
#define HPP_AHTMLCONSTRUCTOR
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AHtmlConstructor.exception.hpp"

class AHtmlConstructor {
private:
    AString html;
    u64int count_tab;
    bool tag_is_done;
    AString make_space();
    AString make_space(u64int count);
    void close_begin_tag();
    AString fix_space(AString text);
public:
    AHtmlConstructor();

    static AHtmlConstructor make();

    AHtmlConstructor & begin(AString tag);

    AHtmlConstructor & attribute(AString name, AString value);

    AHtmlConstructor & text(AString text);

    AHtmlConstructor & end(AString tag);

    AHtmlConstructor & tag(AString tag);

    AHtmlConstructor & add_space(u64int count);

    AString to_text();

    ~AHtmlConstructor();
};

#endif
