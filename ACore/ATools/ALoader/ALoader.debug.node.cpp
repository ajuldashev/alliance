#include "ALoader.hpp"

#ifdef DEBUG_ALOADER
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aloader(AString com) {
	#ifdef DEBUG_ATEXTMANADGER
	if (com == "atextmanadger") {
		if (debug_atextmanadger()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aloader() {
	AString text;
	#ifdef DEBUG_ATEXTMANADGER
	text += "d) atextmanadger\n";
	#endif
	return text;
}

#endif
