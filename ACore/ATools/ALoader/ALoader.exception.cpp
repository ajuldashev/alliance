#include "ALoader.exception.hpp"

ALoaderException::ALoaderException() : AllianceException() {
	exception = "Alliance-Exception-ALoader";
}

ALoaderException::ALoaderException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ALoader");
}

ALoaderException::ALoaderException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ALoader");
}

ALoaderException::ALoaderException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ALoader");
}

ALoaderException::ALoaderException(const ALoaderException & obj) : AllianceException(obj) {
	
}

ALoaderException::~ALoaderException() {

}

