#ifndef HPP_ALOADEREXCEPTION
#define HPP_ALOADEREXCEPTION
#include <alliance.exception.hpp>

class ALoaderException : public AllianceException {
public:
	ALoaderException();
	ALoaderException(std::string text);
	ALoaderException(AllianceException & e);
	ALoaderException(AllianceException & e, std::string text);
	ALoaderException(const ALoaderException & obj);
	~ALoaderException();
};

#endif

