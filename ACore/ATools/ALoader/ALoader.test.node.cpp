#include "ALoader.hpp"

#ifdef TEST_ALOADER
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aloader(bool is_soft) {
	#ifdef TEST_ATEXTMANADGER
	if (!test_atextmanadger(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATEXTMANADGER );
		return is_soft;
	}
	#endif
	return true;
}
#endif
