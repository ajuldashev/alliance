#include "AText.hpp"
#include "AText.test.cpp"
#include "AText.test.node.cpp"
#include "AText.debug.cpp"
#include "AText.debug.node.cpp"
#include "AText.exception.cpp"

AText::AText() {
	this->text = APtr<AStringList>::make();
}

AText::AText(const AText & text) {
	this->text = text.text;
}

void AText::add(AText text) {
	this->text->insert(*text.text, this->text->length());
}

bool AText::is_present() {
	return text.is_present();
}

bool AText::is_empty() {
	return text.is_empty();
}

APtr<AStringList> AText::operator->() {
	try {
		return text;
	} catch (AllianceException & e) {
		throw ATextException(e, "operator->()");
	}
}

AString & AText::operator[](u64int index) {
	try {
		return text->at(index);
	} catch (AllianceException & e) {
		throw ATextException(e, "operator[](u64int index)");
	}
}

AText & AText::operator=(AText text) {
	this->text = text.text;
	return *this;
}


void AText::clear() {
	
}

AText::~AText() {
	clear();
}