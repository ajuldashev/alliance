#include "AText.hpp"

#ifdef DEBUG_ATEXT
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_atext() {
    AText text;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATEXT );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
			AIO::write(debug_list_node_atext());
            AIO::writeln(" 1) help");
			AIO::writeln(" 0) stop");
			AIO::writeln(" e) exit");	
        }
		if (com == "exit") {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATEXT );
    AIO::write_div_line();
    return false;
}
#endif

