#include "AText.exception.hpp"

ATextException::ATextException() : AllianceException() {
	exception = "Alliance-Exception-AText";
}

ATextException::ATextException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AText");
}

ATextException::ATextException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AText");
}

ATextException::ATextException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AText");
}

ATextException::ATextException(const ATextException & obj) : AllianceException(obj) {
	
}

ATextException::~ATextException() {

}

