#ifndef HPP_ATEXTEXCEPTION
#define HPP_ATEXTEXCEPTION
#include <alliance.exception.hpp>

class ATextException : public AllianceException {
public:
	ATextException();
	ATextException(std::string text);
	ATextException(AllianceException & e);
	ATextException(AllianceException & e, std::string text);
	ATextException(const ATextException & obj);
	~ATextException();
};

#endif

