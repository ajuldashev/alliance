#ifndef HPP_ATEXT
#define HPP_ATEXT
#include <ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.hpp>
#include "AText.exception.hpp"

class AText {
private:
    APtr<AStringList> text;
public:
    AText();
    AText(const AText & text);

    bool is_present();
    bool is_empty();

    void add(AText text);

    APtr<AStringList> operator->();

    AString & operator[](u64int index);
    
    AText & operator=(AText text);

    void clear();

    ~AText();
};

#endif
