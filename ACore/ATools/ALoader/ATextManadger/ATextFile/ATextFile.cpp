#include <iostream>
#include "ATextFile.hpp"
#include "ATextFile.test.cpp"
#include "ATextFile.test.node.cpp"
#include "ATextFile.debug.cpp"
#include "ATextFile.debug.node.cpp"
#include "ATextFile.exception.cpp"

ATextFile::ATextFile() {

}

ATextFile::ATextFile(AString name) {
	this->name = name;
}

ATextFile::ATextFile(const ATextFile & file) {
	this->name = file.name;
	this->text = file.text;
}

AText ATextFile::get_text() {
	return text;
}

void ATextFile::set_name(AString name) {
	this->name = name;
}

AString ATextFile::get_name() {
	return name;
}

u64int ATextFile::length() {
	return text->length();
}

u64int ATextFile::size() {
	u64int res = 0;
	for (s64int i = 0; i < text->length(); i += 1) {
		res += text[i].length();
	}
	return res;
}

bool ATextFile::load() {
	AIFile file(name);
	if (file.is_file()) {
		for (u64int idx = 0; !file.is_eof(); idx += 1) {
			text->push_end(file.get_line());
		}
		return true;
	}
	return false;
}

bool ATextFile::load(AString name) {
	set_name(name);
	load();
}

AString & ATextFile::at(s64int index) {
	return text->at(index);
}

AString ATextFile::get(s64int index) {
	return text->get(index);
}

AString ATextFile::pop(s64int index) {
	return text->pop(index);
}

void ATextFile::insert(AString string, s64int index) {
	text->insert(string, index);
}

void ATextFile::remove(s64int index, s64int length) {
	text->remove(index, length);
}

bool ATextFile::has_text() {
	return length() > 0;
}

void ATextFile::clear() {
	text.clear();
	name = "";	
}

ATextFile::~ATextFile() {
	clear();
}