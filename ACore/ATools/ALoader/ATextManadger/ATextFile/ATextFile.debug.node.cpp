#include "ATextFile.hpp"

#ifdef DEBUG_ATEXTFILE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_atextfile(AString com) {
	#ifdef DEBUG_ATEXT
	if (com == "atext") {
		if (debug_atext()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ATEXTITERATOR
	if (com == "atextiterator") {
		if (debug_atextiterator()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_atextfile() {
	AString text;
	#ifdef DEBUG_ATEXT
	text += "d) atext\n";
	#endif
	#ifdef DEBUG_ATEXTITERATOR
	text += "d) atextiterator\n";
	#endif
	return text;
}

#endif
