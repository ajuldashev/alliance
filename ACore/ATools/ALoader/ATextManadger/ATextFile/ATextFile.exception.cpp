#include "ATextFile.exception.hpp"

ATextFileException::ATextFileException() : AllianceException() {
	exception = "Alliance-Exception-ATextFile";
}

ATextFileException::ATextFileException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATextFile");
}

ATextFileException::ATextFileException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATextFile");
}

ATextFileException::ATextFileException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATextFile");
}

ATextFileException::ATextFileException(const ATextFileException & obj) : AllianceException(obj) {
	
}

ATextFileException::~ATextFileException() {

}

