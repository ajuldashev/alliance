#ifndef HPP_ATEXTFILEEXCEPTION
#define HPP_ATEXTFILEEXCEPTION
#include <alliance.exception.hpp>

class ATextFileException : public AllianceException {
public:
	ATextFileException();
	ATextFileException(std::string text);
	ATextFileException(AllianceException & e);
	ATextFileException(AllianceException & e, std::string text);
	ATextFileException(const ATextFileException & obj);
	~ATextFileException();
};

#endif

