#ifndef HPP_ATEXTFILE
#define HPP_ATEXTFILE
#include <alliance.config.hpp>
#include <ACore/ABase/ATypes/ATypes.hpp>
#include <ACore/ABase/AStruct/AArray/AArray.hpp>
#include <ACore/ABase/AIO/AFile/AIFile/AIFile.hpp>
#include "AText/AText.hpp"
#include "ATextFile.exception.hpp"

class ATextFile {
private:
    AString name;
    AText text;
public:
    ATextFile();
    ATextFile(AString name);
    ATextFile(const ATextFile & text_file);
    AText get_text();
    void    set_name(AString name);
    AString get_name();
    u64int length();
    u64int size();
    bool load();
    bool load(AString name);
    AString& at(s64int index);
    AString get(s64int index);
    AString pop(s64int index);
    void insert(AString string, s64int index);
    void remove(s64int index, s64int length);
    bool has_text();
    void clear();
    ~ATextFile();
};

#endif