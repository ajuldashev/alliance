#include "ATextFile.hpp"

#ifdef TEST_ATEXTFILE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_atextfile(bool is_soft) {
	#ifdef TEST_ATEXT
	if (!test_atext(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATEXT );
		return is_soft;
	}
	#endif
	#ifdef TEST_ATEXTITERATOR
	if (!test_atextiterator(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATEXTITERATOR );
		return is_soft;
	}
	#endif
	return true;
}
#endif
