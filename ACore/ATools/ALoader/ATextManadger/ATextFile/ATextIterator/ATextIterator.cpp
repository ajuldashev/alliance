#include "ATextIterator.hpp"
#include "ATextIterator.test.cpp"
#include "ATextIterator.test.node.cpp"
#include "ATextIterator.debug.cpp"
#include "ATextIterator.debug.node.cpp"
#include "ATextIterator.exception.cpp"

ATextIterator::ATextIterator() {
    index_line = 0;
}

ATextIterator::ATextIterator(const ATextIterator & text_iterator) {
    index_line = text_iterator.index_line;
    iterator = text_iterator.iterator;
    text = text_iterator.text;
}

void ATextIterator::set_text(AText text) {
    this->text = text;
    index_line = 0;
    if (text.is_present()
        && text->length() > 0
    ) {
        iterator.set(text[0]);
    } else {
        iterator.clear();
    }
}

AStringIterator ATextIterator::get_iterator() {
    return iterator;
}

void ATextIterator::update(AStringIterator iterator) {
    this->iterator = iterator;
    if (!iterator.can_get()) {
        if (text.is_present()
            && index_line < text->length()
        ) {
            index_line += 1;
            this->iterator.set(text[index_line]);
        }
    }
}

bool ATextIterator::can_get() {
    return text.is_present()
        && (iterator.can_get() || index_line < text->length());
}

u64int ATextIterator::get_index_line() {
    return index_line;
}

ATextIterator::~ATextIterator() {

}