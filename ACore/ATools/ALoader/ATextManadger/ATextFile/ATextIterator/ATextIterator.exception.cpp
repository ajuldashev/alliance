#include "ATextIterator.exception.hpp"

ATextIteratorException::ATextIteratorException() : AllianceException() {
	exception = "Alliance-Exception-ATextIterator";
}

ATextIteratorException::ATextIteratorException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATextIterator");
}

ATextIteratorException::ATextIteratorException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATextIterator");
}

ATextIteratorException::ATextIteratorException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATextIterator");
}

ATextIteratorException::ATextIteratorException(const ATextIteratorException & obj) : AllianceException(obj) {

}

ATextIteratorException::~ATextIteratorException() {

}
