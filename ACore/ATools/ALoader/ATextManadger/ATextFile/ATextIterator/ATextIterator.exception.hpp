#ifndef HPP_ATEXTITERATOREXCEPTION
#define HPP_ATEXTITERATOREXCEPTION
#include <alliance.exception.hpp>

class ATextIteratorException : public AllianceException {
public:
	ATextIteratorException();
	ATextIteratorException(std::string text);
	ATextIteratorException(AllianceException & e);
	ATextIteratorException(AllianceException & e, std::string text);
	ATextIteratorException(const ATextIteratorException & obj);
	~ATextIteratorException();
};

#endif
