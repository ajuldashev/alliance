#ifndef HPP_ATEXTITERATOR
#define HPP_ATEXTITERATOR
#include <ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp>
#include <ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.hpp>
#include "ATextIterator.exception.hpp"

class ATextIterator {
private:
    AText text;
    u64int index_line;
    AStringIterator iterator;
public:
    ATextIterator();
    ATextIterator(const ATextIterator & text_iterator);

    void set_text(AText text);
    
    AStringIterator get_iterator();

    void update(AStringIterator iterator);

    bool can_get();

    u64int get_index_line();

    ~ATextIterator();
};

#endif
