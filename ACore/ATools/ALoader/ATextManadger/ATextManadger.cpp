#include "ATextManadger.hpp"
#include "ATextManadger.test.cpp"
#include "ATextManadger.test.node.cpp"
#include "ATextManadger.debug.cpp"
#include "ATextManadger.debug.node.cpp"
#include "ATextManadger.exception.cpp"

ATextManadger::ATextManadger() {
	last_index_file = 0;
}

ATextManadger::ATextManadger(const ATextManadger & obj) {
	last_index_file = obj.last_index_file;
	AArray<ATextFile> new_files(obj.files);
	files = new_files;
}

bool ATextManadger::load_file(AString name) {
	ATextFile file;
	file.set_name(name);
	file.load();
	if (!file.has_text()) {
		return false;
	}
	files.push_end(file);
	last_index_file += 1;
	return true;
}

bool ATextManadger::delete_file(AString file_name) {
	for (s64int i = 0; i < files.length(); i += 1) {
		if (files.at(i).get_name() == file_name) {
			files.remove(i, 1);
			return true;
		}
	}
	return false;
}

bool ATextManadger::delete_file(u64int file_index) {
	if (file_index >= 0 && file_index < files.length()) {
		files.at(file_index).clear();
		files.remove(file_index, 1);
		return true;
	}
	return false;
}

ATextFile ATextManadger::get(AString file_name) {
	ATextFile res;
	for (s64int i = 0; i < files.length(); i += 1) {
		if (files.at(i).get_name() == file_name) {
			return files.get(i);
		}
	}
	return res;
}

ATextFile ATextManadger::get(u64int file_index) {
	return files.get(file_index);
}

u64int ATextManadger::length() {
	return files.length();
}

void ATextManadger::clear() {
	for (s64int i = 0; i < files.length(); i += 1) {
		files.at(i).clear();
	}
	files.clear();
}

ATextManadger::~ATextManadger() {
	clear();
	last_index_file = 0;
}