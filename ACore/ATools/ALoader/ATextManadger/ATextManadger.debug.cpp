#include "ATextManadger.hpp"

#ifdef DEBUG_ATEXTMANADGER
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_atextmanadger() {
    ATextManadger manadger;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_ATEXTMANADGER );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_atextmanadger());
            AIO::writeln(" 1) help");
            AIO::writeln(" 0) stop");	
            AIO::writeln(" e) exit");
        }
        if (com == "exit") {
            return true;
        }
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_ATEXTMANADGER );
    AIO::write_div_line();
    return false;
}
#endif

