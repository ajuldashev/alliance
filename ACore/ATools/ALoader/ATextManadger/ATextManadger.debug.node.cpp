#include "ATextManadger.hpp"

#ifdef DEBUG_ATEXTMANADGER
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_atextmanadger(AString com) {
	#ifdef DEBUG_ATEXTFILE
	if (com == "atextfile") {
		if (debug_atextfile()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_atextmanadger() {
	AString text;
	#ifdef DEBUG_ATEXTFILE
	text += "d) atextfile\n";
	#endif
	return text;
}

#endif
