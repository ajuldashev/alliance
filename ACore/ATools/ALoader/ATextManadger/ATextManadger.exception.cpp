#include "ATextManadger.exception.hpp"

ATextManadgerException::ATextManadgerException() : AllianceException() {
	exception = "Alliance-Exception-ATextManadger";
}

ATextManadgerException::ATextManadgerException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATextManadger");
}

ATextManadgerException::ATextManadgerException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATextManadger");
}

ATextManadgerException::ATextManadgerException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATextManadger");
}

ATextManadgerException::ATextManadgerException(const ATextManadgerException & obj) : AllianceException(obj) {
	
}

ATextManadgerException::~ATextManadgerException() {

}

