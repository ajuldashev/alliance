#ifndef HPP_ATEXTMANADGEREXCEPTION
#define HPP_ATEXTMANADGEREXCEPTION
#include <alliance.exception.hpp>

class ATextManadgerException : public AllianceException {
public:
	ATextManadgerException();
	ATextManadgerException(std::string text);
	ATextManadgerException(AllianceException & e);
	ATextManadgerException(AllianceException & e, std::string text);
	ATextManadgerException(const ATextManadgerException & obj);
	~ATextManadgerException();
};

#endif

