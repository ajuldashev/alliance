#ifndef HPP_ATEXTMANADGER
#define HPP_ATEXTMANADGER
#include <alliance.config.hpp>
#include "ATextFile/ATextFile.hpp"
#include "ATextManadger.exception.hpp"

class ATextManadger {
private:
    u64int last_index_file;
    AArray<ATextFile> files;
public:
    ATextManadger();
    ATextManadger(const ATextManadger & text_manadger);
    bool load_file(AString file_name);
    bool delete_file(AString file_name);
    bool delete_file(u64int file_index);
    ATextFile get(AString file_name);
    ATextFile get(u64int file_index);
    u64int length();
    void clear();
    ~ATextManadger();
};

#endif