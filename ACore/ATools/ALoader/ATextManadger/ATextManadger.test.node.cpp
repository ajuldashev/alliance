#include "ATextManadger.hpp"

#ifdef TEST_ATEXTMANADGER
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_atextmanadger(bool is_soft) {
	#ifdef TEST_ATEXTFILE
	if (!test_atextfile(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ATEXTFILE );
		return is_soft;
	}
	#endif
	return true;
}
#endif
