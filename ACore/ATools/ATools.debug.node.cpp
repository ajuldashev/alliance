#include "ATools.hpp"

#ifdef DEBUG_ATOOLS
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_atools(AString com) {
	#ifdef DEBUG_ACPPPARSER
	if (com == "acppparser") {
		if (debug_acppparser()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AHTMLCONSTRUCTOR
	if (com == "ahtmlconstructor") {
		if (debug_ahtmlconstructor()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ALOADER
	if (com == "aloader") {
		if (debug_aloader()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_atools() {
	AString text;
	#ifdef DEBUG_ACPPPARSER
	text += "d) acppparser\n";
	#endif
	#ifdef DEBUG_AHTMLCONSTRUCTOR
	text += "d) ahtmlconstructor\n";
	#endif
	#ifdef DEBUG_ALOADER
	text += "d) aloader\n";
	#endif
	return text;
}

#endif
