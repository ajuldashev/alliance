#include "ATools.exception.hpp"

AToolsException::AToolsException() : AllianceException() {
	exception = "Alliance-Exception-ATools";
}

AToolsException::AToolsException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ATools");
}

AToolsException::AToolsException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ATools");
}

AToolsException::AToolsException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ATools");
}

AToolsException::AToolsException(const AToolsException & obj) : AllianceException(obj) {
	
}

AToolsException::~AToolsException() {

}

