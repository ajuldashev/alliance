#ifndef ATOOLSEXCEPTION
#define ATOOLSEXCEPTION
#include <alliance.exception.hpp>

class AToolsException : public AllianceException {
public:
	AToolsException();
	AToolsException(std::string text);
	AToolsException(AllianceException & e);
	AToolsException(AllianceException & e, std::string text);
	AToolsException(const AToolsException & obj);
	~AToolsException();
};

#endif

