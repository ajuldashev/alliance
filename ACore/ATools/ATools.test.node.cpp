#include "ATools.hpp"

#ifdef TEST_ATOOLS
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_atools(bool is_soft) {
	#ifdef TEST_ACPPPARSER
	if (!test_acppparser(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACPPPARSER );
		return is_soft;
	}
	#endif
	#ifdef TEST_AHTMLCONSTRUCTOR
	if (!test_ahtmlconstructor(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AHTMLCONSTRUCTOR );
		return is_soft;
	}
	#endif
	#ifdef TEST_ALOADER
	if (!test_aloader(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ALOADER );
		return is_soft;
	}
	#endif
	return true;
}
#endif
