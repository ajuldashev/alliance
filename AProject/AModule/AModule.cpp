#include "AModule.hpp"
#include "AModule.test.cpp"
#include "AModule.test.node.cpp"
#include "AModule.debug.cpp"
#include "AModule.debug.node.cpp"
#include "AModule.exception.cpp"

AModule::AModule() {

}

AModule::AModule(AFile file) {
    set_info(file);
}

AFile AModule::get_info() {
    return info;
}

void AModule::set_info(AFile file) {
    info = file;
}

AString AModule::to_text(ATreeNav<AModule> modules, AString tab) {
    AString res;
    ATreeNav<AModule> nav(modules);
    res = res + tab + info.get_name() + "\n";
    for (s64int i = 0; i < nav.length(); i += 1) {
        nav.goto_child(i);
        res = res + nav.at().to_text(nav, tab + "  ");
        nav.goto_parent();
    }
    return res;
}

void AModule::load(ATreeNav<AModule> modules) {
    ADir dir(info.full_name());
    dir.open();
    dir.sort();
    for (s64int i = 0; i < dir.dirs().length(); i += 1) {
        AModule module(dir.dirs()[i]);
        if (module.is_module()) {
            modules.add(module);
            modules.goto_child(modules.length() - 1);
            module.load(modules);
            modules.goto_parent();
        }
    }
}

bool AModule::is_module() {
    if (info.is_exist() && info.is_dir()); {
        AString name = info.get_name();
        AString path = info.full_name();
        AString module_cpp = path + "/" + name + ".cpp";
        return AFileInfo::is_exist(module_cpp);
    }
    return false;
}

AString AModule::to_text_module() {
    AString name = info.get_name();
    name = fix_path(name);
    for (s64int i = 0; i < name.length(); i += 1) {
        if (name[i] == '/') {
            name[i] = '_';
        }
    }
    return name;
}

AString AModule::to_full_name_module() {
    AString name = info.full_name();
    name = fix_path(name);
    for (s64int i = 0; i < name.length(); i += 1) {
        if (name[i] == '/') {
            name[i] = '-';
        }
    }
    return name;
}

AString AModule::fix_path(AString path) {
    if (path.length() > 1 && path[0] == '.' && path[1] == '/') {
        path.remove(0, 2);
    }
    return path;
}

AModule::~AModule() {

}