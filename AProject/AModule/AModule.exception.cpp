#include "AModule.exception.hpp"

AModuleException::AModuleException() : AllianceException() {
	exception = "Alliance-Exception-AModule";
}

AModuleException::AModuleException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModule");
}

AModuleException::AModuleException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModule");
}

AModuleException::AModuleException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModule");
}

AModuleException::AModuleException(const AModuleException & obj) : AllianceException(obj) {
	
}

AModuleException::~AModuleException() {

}

