#ifndef HPP_AMODULEEXCEPTION
#define HPP_AMODULEEXCEPTION
#include <alliance.exception.hpp>

class AModuleException : public AllianceException {
public:
	AModuleException();
	AModuleException(std::string text);
	AModuleException(AllianceException & e);
	AModuleException(AllianceException & e, std::string text);
	AModuleException(const AModuleException & obj);
	~AModuleException();
};

#endif

