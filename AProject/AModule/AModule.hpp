#ifndef HPP_AMODULE
#define HPP_AMODULE
#include <alliance.config.hpp>
#include <ACore/ABase/AIO/ADir/ADir.hpp>
#include <ACore/ABase/AIO/AFileInfo/AFileInfo.hpp>
#include <ACore/ABase/AStruct/ATreeNav/ATreeNav.hpp>
#include "AModule.exception.hpp"

class AModule {
private:
    AFile info;
public:
    AModule();
    AModule(AFile);
    AFile get_info();
    void  set_info(AFile file);
    AString to_text(ATreeNav<AModule> modules, AString tab);
    void load(ATreeNav<AModule> modules);
    bool is_module();
    AString to_text_module();
    AString to_full_name_module();
    static AString fix_path(AString);
    ~AModule();
};

#endif