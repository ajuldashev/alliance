#include "AModuleDebugHeader.hpp"
#include "AModuleDebugHeader.test.cpp"
#include "AModuleDebugHeader.test.node.cpp"
#include "AModuleDebugHeader.debug.cpp"
#include "AModuleDebugHeader.debug.node.cpp"
#include "AModuleDebugHeader.exception.cpp"

AModuleDebugHeader::AModuleDebugHeader() {

}

AModuleDebugHeader::AModuleDebugHeader(ATreeNav<AModule> modules) {
    this->modules = modules;
}

AString AModuleDebugHeader::to_text() {
    AString text;
    text += header();
    text += body();
    text += end();
    return text;
}

AString AModuleDebugHeader::header() {
    AString text;
    text += "#ifndef DEBUG_FILE\n";
    text += "#define DEBUG_FILE\n";
    text += "#define DEBUG_ALLIANCE\n";
    text += "#ifdef  DEBUG_ALLIANCE\n\n";
    text += "#include <iostream>\n";
    text += "#define df(str) std::cout << (str) << std::endl;\n\n";
    text += "class AString;\n\n";
    text += "bool debug_alliance();\n";
    text += "bool debug_node_alliance(AString com);\n";
    text += "AString debug_list_node_alliance();\n";
    return text;
}

AString AModuleDebugHeader::body_rec(ATreeNav<AModule> modules, AString tab) {
    AString text;
    if (modules.has_data()) {
        AString deff = "DEBUG_" + modules.at().to_text_module().to_upper();
        AString func = "debug_" + modules.at().to_text_module().to_lower() + "();";
        AString func_node = "debug_node_" + modules.at().to_text_module().to_lower() + "(AString com);";
        AString func_list_node = "debug_list_node_" + modules.at().to_text_module().to_lower() + "();";
        AString deff_full_name = "\"" + modules.at().to_full_name_module() + "\"";
        text += tab + "#define " + deff + "\n";
        text += tab + "#ifdef  " + deff + "\n";
        text += tab + "\tbool " + func + "\n";
        text += tab + "\tbool " + func_node + "\n";
        text += tab + "\tAString " + func_list_node + "\n";
        text += tab + "\t#define MODULE_" + deff + " " + deff_full_name + "\n";
    }
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body_rec(modules, tab + "\t");
        modules.goto_parent();
    }
    if (modules.has_data()) {
        text += tab + "#endif" + "\n";
    }
    return text;
}

AString AModuleDebugHeader::body() {
    AString text;
    text += body_rec(modules, "");
    return text;
}

AString AModuleDebugHeader::end() {
    AString text;
    text += "#endif\n";
    text += "#endif\n";
    return text;
}

AModuleDebugHeader::~AModuleDebugHeader() {

}