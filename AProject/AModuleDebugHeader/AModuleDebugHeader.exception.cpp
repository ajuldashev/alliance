#include "AModuleDebugHeader.exception.hpp"

AModuleDebugHeaderException::AModuleDebugHeaderException() : AllianceException() {
	exception = "Alliance-Exception-AModuleDebugHeader";
}

AModuleDebugHeaderException::AModuleDebugHeaderException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleDebugHeader");
}

AModuleDebugHeaderException::AModuleDebugHeaderException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleDebugHeader");
}

AModuleDebugHeaderException::AModuleDebugHeaderException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleDebugHeader");
}

AModuleDebugHeaderException::AModuleDebugHeaderException(const AModuleDebugHeaderException & obj) : AllianceException(obj) {
	
}

AModuleDebugHeaderException::~AModuleDebugHeaderException() {

}

