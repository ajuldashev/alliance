#ifndef HPP_AMODULEDEBUGHEADEREXCEPTION
#define HPP_AMODULEDEBUGHEADEREXCEPTION
#include <alliance.exception.hpp>

class AModuleDebugHeaderException : public AllianceException {
public:
	AModuleDebugHeaderException();
	AModuleDebugHeaderException(std::string text);
	AModuleDebugHeaderException(AllianceException & e);
	AModuleDebugHeaderException(AllianceException & e, std::string text);
	AModuleDebugHeaderException(const AModuleDebugHeaderException & obj);
	~AModuleDebugHeaderException();
};

#endif

