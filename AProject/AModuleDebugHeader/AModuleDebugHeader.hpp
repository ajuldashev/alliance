#ifndef HPP_AMODULEDEBUGHEADER
#define HPP_AMODULEDEBUGHEADER
#include <alliance.config.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleDebugHeader.exception.hpp"

class AModuleDebugHeader {
private:
    ATreeNav<AModule> modules;
    AString body_rec(ATreeNav<AModule> modules, AString tab);
public:
    AModuleDebugHeader();
    AModuleDebugHeader(ATreeNav<AModule> modules);
    
    AString to_text();
    AString header();
    AString body();
    AString end();

    ~AModuleDebugHeader();
};

#endif