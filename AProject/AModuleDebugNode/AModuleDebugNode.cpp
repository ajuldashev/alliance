#include "AModuleDebugNode.hpp"
#include "AModuleDebugNode.test.cpp"
#include "AModuleDebugNode.test.node.cpp"
#include "AModuleDebugNode.debug.cpp"
#include "AModuleDebugNode.debug.node.cpp"
#include "AModuleDebugNode.exception.cpp"

AModuleDebugNode::AModuleDebugNode() {

}

AModuleDebugNode::AModuleDebugNode(ATreeNav<AModule> modules) {
    this->modules = modules;
}

void AModuleDebugNode::start() {
    body_rec(modules);
}

void AModuleDebugNode::save_file(ATreeNav<AModule> modules) {
    AString module_cpp = "alliance.debug.node.cpp";
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        module_cpp = path + "/" + name + ".debug.node.cpp";
    }
    AIO::to_file(to_text(modules), module_cpp);
}

AString AModuleDebugNode::to_text(ATreeNav<AModule> modules) {
    AString text;
    text += header_match_command(modules);
    for (s64int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body_match_command(modules);
        modules.goto_parent();
    }
    text += end_match_command(modules);
    text += header_list_node(modules);
    for (s64int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body_list_node(modules);
        modules.goto_parent();
    }
    text += end_list_node(modules);
    return text;
}

AString AModuleDebugNode::header_match_command(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    text += "#include \"";
    text += name_module;
    text += ".hpp\"\n";
    text += "\n";
    text += "#ifdef DEBUG_";
    text += name_module.to_upper() + "\n";
    text += "#include <ACore/ABase/AIO/AIO.hpp>\n\n";
    text += "bool debug_node_";
    text += name_module.to_lower();
    text += "(AString com) {\n";
    return text;
}

void AModuleDebugNode::body_rec(ATreeNav<AModule> modules) {
    save_file(modules);
    for (s64int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        body_rec(modules);
        modules.goto_parent();
    }
}

AString AModuleDebugNode::body_match_command(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    if (modules.has_data()) {
        text += "\t#ifdef DEBUG_" + name_module.to_upper() + "\n";
        text += "\tif (com == \"";
        text += name_module.to_lower();
        text += "\") {\n";
        text += "\t\tif (debug_";
        text += name_module.to_lower();
        text += "()) {\n";
        text += "\t\t\treturn true;\n";
        text += "\t\t}\n";
        text += "\t}\n";
        text += "\t#endif\n";
    }
    return text;
}

AString AModuleDebugNode::end_match_command(ATreeNav<AModule> modules) {
    AString text;
    text += "\treturn false;\n";
    text += "}\n";
    text += "\n";
    return text;
}

AString AModuleDebugNode::header_list_node(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    text += "AString debug_list_node_";
    text += name_module.to_lower();
    text += "() {\n";
    text += "\tAString text;\n";
    return text;
}

AString AModuleDebugNode::body_list_node(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    if (modules.has_data()) {
        text += "\t#ifdef DEBUG_" + name_module.to_upper() + "\n";
        text += "\ttext += \"d) ";
        text += name_module.to_lower();
        text += "\\n\";\n";
        text += "\t#endif\n";
    }
    return text;
}

AString AModuleDebugNode::end_list_node(ATreeNav<AModule> modules) {
    AString text;
    text += "\treturn text;\n";
    text += "}\n\n";
    text += "#endif\n";
    return text;
}


AModuleDebugNode::~AModuleDebugNode() {

}