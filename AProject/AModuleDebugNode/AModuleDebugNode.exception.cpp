#include "AModuleDebugNode.exception.hpp"

AModuleDebugNodeException::AModuleDebugNodeException() : AllianceException() {
	exception = "Alliance-Exception-AModuleDebugNode";
}

AModuleDebugNodeException::AModuleDebugNodeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleDebugNode");
}

AModuleDebugNodeException::AModuleDebugNodeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleDebugNode");
}

AModuleDebugNodeException::AModuleDebugNodeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleDebugNode");
}

AModuleDebugNodeException::AModuleDebugNodeException(const AModuleDebugNodeException & obj) : AllianceException(obj) {
	
}

AModuleDebugNodeException::~AModuleDebugNodeException() {

}

