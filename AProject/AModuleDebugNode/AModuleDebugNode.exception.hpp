#ifndef HPP_AMODULEDEBUGNODEEXCEPTION
#define HPP_AMODULEDEBUGNODEEXCEPTION
#include <alliance.exception.hpp>

class AModuleDebugNodeException : public AllianceException {
public:
	AModuleDebugNodeException();
	AModuleDebugNodeException(std::string text);
	AModuleDebugNodeException(AllianceException & e);
	AModuleDebugNodeException(AllianceException & e, std::string text);
	AModuleDebugNodeException(const AModuleDebugNodeException & obj);
	~AModuleDebugNodeException();
};

#endif

