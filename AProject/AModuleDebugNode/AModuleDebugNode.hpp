#ifndef HPP_AMODULEDEBUGNODE
#define HPP_AMODULEDEBUGNODE
#include <alliance.config.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleDebugNode.exception.hpp"

class AModuleDebugNode {
private:
    ATreeNav<AModule> modules;
    void body_rec(ATreeNav<AModule> modules);
public:
    AModuleDebugNode();
    AModuleDebugNode(ATreeNav<AModule> modules);
    
    void start();
    void save_file(ATreeNav<AModule> modules);
    
    AString to_text(ATreeNav<AModule> modules);

    AString header_match_command(ATreeNav<AModule> modules);
    AString body_match_command(ATreeNav<AModule> modules);
    AString end_match_command(ATreeNav<AModule> modules);

    AString header_list_node(ATreeNav<AModule> modules);
    AString body_list_node(ATreeNav<AModule> modules);
    AString end_list_node(ATreeNav<AModule> modules);

    ~AModuleDebugNode();
};

#endif