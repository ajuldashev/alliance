#include "ADocHtmlParserCPP.hpp"
#include "ADocHtmlParserCPP.test.cpp"
#include "ADocHtmlParserCPP.test.node.cpp"
#include "ADocHtmlParserCPP.debug.cpp"
#include "ADocHtmlParserCPP.debug.node.cpp"
#include "ADocHtmlParserCPP.exception.cpp"

ADocHtmlParserCPP::ADocHtmlParserCPP() {

}

ADocHtmlParserCPP::ADocHtmlParserCPP(ATreeNav<AModule> modules) {
    this->modules = modules;
}

 AHtmlConstructor ADocHtmlParserCPP::parse() {
     return AHtmlConstructor::make()
        .begin("div")
            .begin("h3")
                .text("Детали реализации ")
            .end("h3")
            .begin("div")
                .attribute("class", "block-info")
                .text("Отсутствуют")
            .end("div")
        .end("div")
        ;
 }

ADocHtmlParserCPP::~ADocHtmlParserCPP() {

}