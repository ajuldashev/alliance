#include "ADocHtmlParserCPP.exception.hpp"

ADocHtmlParserCPPException::ADocHtmlParserCPPException() : AllianceException() {
	exception = "Alliance-Exception-ADocHtmlParserCPP";
}

ADocHtmlParserCPPException::ADocHtmlParserCPPException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ADocHtmlParserCPP");
}

ADocHtmlParserCPPException::ADocHtmlParserCPPException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ADocHtmlParserCPP");
}

ADocHtmlParserCPPException::ADocHtmlParserCPPException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ADocHtmlParserCPP");
}

ADocHtmlParserCPPException::ADocHtmlParserCPPException(const ADocHtmlParserCPPException & obj) : AllianceException(obj) {

}

ADocHtmlParserCPPException::~ADocHtmlParserCPPException() {

}
