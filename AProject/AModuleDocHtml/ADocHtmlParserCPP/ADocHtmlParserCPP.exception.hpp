#ifndef HPP_ADOCHTMLPARSERCPPEXCEPTION
#define HPP_ADOCHTMLPARSERCPPEXCEPTION
#include <alliance.exception.hpp>

class ADocHtmlParserCPPException : public AllianceException {
public:
	ADocHtmlParserCPPException();
	ADocHtmlParserCPPException(std::string text);
	ADocHtmlParserCPPException(AllianceException & e);
	ADocHtmlParserCPPException(AllianceException & e, std::string text);
	ADocHtmlParserCPPException(const ADocHtmlParserCPPException & obj);
	~ADocHtmlParserCPPException();
};

#endif
