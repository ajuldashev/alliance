#ifndef HPP_ADOCHTMLPARSERCPP
#define HPP_ADOCHTMLPARSERCPP
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <AProject/AModule/AModule.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ADocHtmlParserCPP.exception.hpp"

class ADocHtmlParserCPP {
private:
    ATreeNav<AModule> modules;
public:
    ADocHtmlParserCPP();
    ADocHtmlParserCPP(ATreeNav<AModule> modules);

    AHtmlConstructor parse();

    ~ADocHtmlParserCPP();
};

#endif
