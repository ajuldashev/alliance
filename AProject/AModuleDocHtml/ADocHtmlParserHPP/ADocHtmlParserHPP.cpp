#include "ADocHtmlParserHPP.hpp"
#include "ADocHtmlParserHPP.test.cpp"
#include "ADocHtmlParserHPP.test.node.cpp"
#include "ADocHtmlParserHPP.debug.cpp"
#include "ADocHtmlParserHPP.debug.node.cpp"
#include "ADocHtmlParserHPP.exception.cpp"

void ADocHtmlParserHPP::init() {
    //"\\s*class\\s+([A-Za-z0-9_]+)\\s*(?::\\s*(?:public)?\\s+([A-Za-z0-9_]+))?\\s*\\{?"

    // Паттерн описания определения класса например "class A : public B {"
    pattern_class = ARegexPattern(
        ARegexConstructor::make()
        // В начале стоит ключевое слово class, отделенное пробелами
        .space().any_number() 
        .text("class")
        .space().at_least_one()
        // Затем идет имя самого класса, попадает в первую группу      
        .group_begin()
            .regex_class("A-Za-z0-9_").at_least_one()
        .group_end()
        // Дальше может идти любое кол-во пробелов
        .space().any_number()
        // Далее идет не обязательная группа, наследнование других классов
        .group_begin()
            .unknown()
            // наследование начинается с двоеточия
            .symbol(":")
            .space().any_number()
            // Далее может идти ключевое слово public
            .group_begin()
                .unknown()
                .text("public")
            .group_end().may_be_present()
            // любое кол-во пробелов
            .space().at_least_one()
            // Имя класса от которого наследуемся
            .group_begin()
                .regex_class("A-Za-z0-9_").at_least_one()
            .group_end()

        .group_end().may_be_present()
        
        // любое кол-во пробелов
        .space().any_number()
        // Может оканчиватся открывающей фигурной скобкой
        .symbol("{").may_be_present()

        .to_string()
    );
}

void ADocHtmlParserHPP::load_file() {
    AString path = modules.at().get_info().get_path() + modules.at().get_info().get_name();
    AString name = modules.at().get_info().get_name() + ".hpp";
    file.set_name(path + "/" + name);
    file.load();
}

void ADocHtmlParserHPP::parse_info() {
    has_info = false;
    u64int index = 0;
    for (; index < file.length(); index += 1) {
        APtr<AStringList> matches = pattern_class.matches(file.get(index));
        if (matches) {
            if (matches->get(1) == modules.at().get_info().get_name()) {
                html_class = html_class
                .begin("h3")
                    .text("Модуль " + matches->get(1))
                .end("h3")
                ;
            } else {
                html_class = html_class
                .begin("h3")
                    .text("Класс " + matches->get(1))
                .end("h3")
                ;
            }
            has_info = true;
        }
    }
}

AString ADocHtmlParserHPP::parse_result() {
    if (has_info) {
        return AHtmlConstructor::make()
        .text(html_class.to_text())
        .text(html_other.to_text())
        .to_text(); 
    }
    return AHtmlConstructor::make()
        .begin("div")
            .attribute("class", "block-info")
            .text("Отсутствует")
        .end("div")
        .to_text(); 
}

ADocHtmlParserHPP::ADocHtmlParserHPP() {
    has_info = false;
    init();
}

ADocHtmlParserHPP::ADocHtmlParserHPP(ATreeNav<AModule> modules) {
    this->modules = modules;
    has_info = false;
    init();
}

 AHtmlConstructor ADocHtmlParserHPP::parse() {
    load_file();
    parse_info();
    return AHtmlConstructor::make()
        .begin("div")
            .begin("h3")
                .text("Основная информация ")
            .end("h3")
            .text(parse_result())
        .end("div")
        ; 
}

ADocHtmlParserHPP::~ADocHtmlParserHPP() {

}