#include "ADocHtmlParserHPP.exception.hpp"

ADocHtmlParserHPPException::ADocHtmlParserHPPException() : AllianceException() {
	exception = "Alliance-Exception-ADocHtmlParserHPP";
}

ADocHtmlParserHPPException::ADocHtmlParserHPPException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ADocHtmlParserHPP");
}

ADocHtmlParserHPPException::ADocHtmlParserHPPException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ADocHtmlParserHPP");
}

ADocHtmlParserHPPException::ADocHtmlParserHPPException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ADocHtmlParserHPP");
}

ADocHtmlParserHPPException::ADocHtmlParserHPPException(const ADocHtmlParserHPPException & obj) : AllianceException(obj) {

}

ADocHtmlParserHPPException::~ADocHtmlParserHPPException() {

}
