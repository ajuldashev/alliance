#ifndef HPP_ADOCHTMLPARSERHPPEXCEPTION
#define HPP_ADOCHTMLPARSERHPPEXCEPTION
#include <alliance.exception.hpp>

class ADocHtmlParserHPPException : public AllianceException {
public:
	ADocHtmlParserHPPException();
	ADocHtmlParserHPPException(std::string text);
	ADocHtmlParserHPPException(AllianceException & e);
	ADocHtmlParserHPPException(AllianceException & e, std::string text);
	ADocHtmlParserHPPException(const ADocHtmlParserHPPException & obj);
	~ADocHtmlParserHPPException();
};

#endif
