#ifndef HPP_ADOCHTMLPARSERHPP
#define HPP_ADOCHTMLPARSERHPP
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <AProject/AModule/AModule.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include <ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.hpp>
#include "ADocHtmlParserHPP.exception.hpp"

class ADocHtmlParserHPP {
private:
    ATextFile file;
    
    ATreeNav<AModule> modules;
    
    AHtmlConstructor html_class;
    AHtmlConstructor html_other;

    ARegexPattern pattern_class;

    bool has_info;

    void init();

    void load_file();
    void parse_info();
    AString parse_result();
public:
    ADocHtmlParserHPP();
    ADocHtmlParserHPP(ATreeNav<AModule> modules);

    AHtmlConstructor parse();

    ~ADocHtmlParserHPP();
};

#endif
