#include "ADocHtmlParserTEST.hpp"
#include "ADocHtmlParserTEST.test.cpp"
#include "ADocHtmlParserTEST.test.node.cpp"
#include "ADocHtmlParserTEST.debug.cpp"
#include "ADocHtmlParserTEST.debug.node.cpp"
#include "ADocHtmlParserTEST.exception.cpp"

ADocHtmlParserTEST::ADocHtmlParserTEST() {

}

ADocHtmlParserTEST::ADocHtmlParserTEST(ATreeNav<AModule> modules) {
    this->modules = modules;
}

 AHtmlConstructor ADocHtmlParserTEST::parse() {
     return AHtmlConstructor::make()
        .begin("div")
            .begin("h3")
                .text("Информация о тестировании ")
            .end("h3")
            .begin("div")
                .attribute("class", "block-info")
                .text("Отсутствует")
            .end("div")
        .end("div")
        ;
 }

ADocHtmlParserTEST::~ADocHtmlParserTEST() {

}