#include "ADocHtmlParserTEST.hpp"

#ifdef DEBUG_ADOCHTMLPARSERTEST
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_adochtmlparsertest() {
	AString com;
	AIO::write_div_line();
	AIO::writeln("Begin debug the module " MODULE_DEBUG_ADOCHTMLPARSERTEST );
	com = AIO::get_command();
	while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_adochtmlparsertest());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_adochtmlparsertest(com)) {
			return true;
		}
		AIO::write_div_line();
		com = AIO::get_command();
	}
	AIO::writeln("End debug the module " MODULE_DEBUG_ADOCHTMLPARSERTEST );
	AIO::write_div_line();
	return false;
}
#endif
