#include "ADocHtmlParserTEST.exception.hpp"

ADocHtmlParserTESTException::ADocHtmlParserTESTException() : AllianceException() {
	exception = "Alliance-Exception-ADocHtmlParserTEST";
}

ADocHtmlParserTESTException::ADocHtmlParserTESTException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-ADocHtmlParserTEST");
}

ADocHtmlParserTESTException::ADocHtmlParserTESTException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-ADocHtmlParserTEST");
}

ADocHtmlParserTESTException::ADocHtmlParserTESTException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-ADocHtmlParserTEST");
}

ADocHtmlParserTESTException::ADocHtmlParserTESTException(const ADocHtmlParserTESTException & obj) : AllianceException(obj) {

}

ADocHtmlParserTESTException::~ADocHtmlParserTESTException() {

}
