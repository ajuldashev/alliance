#ifndef HPP_ADOCHTMLPARSERTESTEXCEPTION
#define HPP_ADOCHTMLPARSERTESTEXCEPTION
#include <alliance.exception.hpp>

class ADocHtmlParserTESTException : public AllianceException {
public:
	ADocHtmlParserTESTException();
	ADocHtmlParserTESTException(std::string text);
	ADocHtmlParserTESTException(AllianceException & e);
	ADocHtmlParserTESTException(AllianceException & e, std::string text);
	ADocHtmlParserTESTException(const ADocHtmlParserTESTException & obj);
	~ADocHtmlParserTESTException();
};

#endif
