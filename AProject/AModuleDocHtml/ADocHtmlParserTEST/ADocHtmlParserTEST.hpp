#ifndef HPP_ADOCHTMLPARSERTEST
#define HPP_ADOCHTMLPARSERTEST
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <AProject/AModule/AModule.hpp>
#include <ACore/ABase/ALibs/ARegex/ARegex.hpp>
#include "ADocHtmlParserTEST.exception.hpp"

class ADocHtmlParserTEST {
private:
    ATreeNav<AModule> modules;
public:
    ADocHtmlParserTEST();
    ADocHtmlParserTEST(ATreeNav<AModule> modules);

    AHtmlConstructor parse();

    ~ADocHtmlParserTEST();
};

#endif
