#include "AModuleDocHtml.hpp"
#include "AModuleDocHtml.test.cpp"
#include "AModuleDocHtml.test.node.cpp"
#include "AModuleDocHtml.debug.cpp"
#include "AModuleDocHtml.debug.node.cpp"
#include "AModuleDocHtml.exception.cpp"

AHtmlConstructor AModuleDocHtml::get_html_hpp(ATreeNav<AModule> modules) {
    ADocHtmlParserHPP parser(modules);
    return AHtmlConstructor(parser.parse());
}

AHtmlConstructor AModuleDocHtml::get_html_cpp(ATreeNav<AModule> modules) {
    ADocHtmlParserCPP parser(modules);
    return AHtmlConstructor(parser.parse());
}

AHtmlConstructor AModuleDocHtml::get_html_test(ATreeNav<AModule> modules) {
    ADocHtmlParserTEST parser(modules);
    return AHtmlConstructor(parser.parse());
}

AModuleDocHtml::AModuleDocHtml() {

}

AModuleDocHtml::AModuleDocHtml(ATreeNav<AModule> modules) {
    this->modules = modules;
}

void AModuleDocHtml::start() {
    body_rec(modules);
}

void AModuleDocHtml::save_file(ATreeNav<AModule> modules) {
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        AString module_html = path + "/" + name + ".doc.html";
        AIO::to_file(to_text(modules), module_html);
    }
}

AString AModuleDocHtml::to_text(ATreeNav<AModule> modules) {

    AHtmlConstructor html_hpp  = get_html_hpp  (modules);
    AHtmlConstructor html_cpp  = get_html_cpp  (modules);
    AHtmlConstructor html_test = get_html_test (modules);

    return AHtmlConstructor::make()

        .tag("!DOCTYPE html")

        .begin("html")
            .begin("head")
                .tag("meta")
                    .attribute("charset", "utf-8")
                .begin("title")
                    .text("Документация по проекту Alliance")
                .end("title")
                .tag("link")
                    .attribute("rel", "stylesheet")
                    .attribute("href", "style.css")
            .end("head")
            .begin("body")
                .attribute("id", "body-status-id")
                .attribute("class", "status-module-stable")
                .begin("div")
                    .attribute("class", "main-block")

                    .begin("div")
                        .attribute("class", "head-block")
                        .begin("h1")
                            .text(modules.at().get_info().get_name())
                        .end("h1")
                    .end("div")

                    .begin("div")
                        .attribute("class", "hrefs-block")
                        .attribute("id", "hrefs-block-id")
                        .begin("ul")
                            .begin("li")
                                .begin("a")
                                    .attribute("href", "alliance.doc.html")
                                    .text("На главную")
                                .end("a")
                            .end("li")
                            .begin("li")
                                .begin("a")
                                    .attribute("onclick", "javascript:history.back(-2); return false;")
                                    .text("Назад")
                                .end("a")
                            .end("li")    
                        .end("ul")
                    .end("div")

                    .begin("div")
                        .attribute("class", "body-block")
                        .text(html_hpp.to_text())
                        .tag("/br")
                        .text(html_cpp.to_text())
                        .tag("/br")
                        .text(html_test.to_text())
                    .end("div")

                .end("div")
            .end("body")
        .end("html")
        .to_text();
}

AString AModuleDocHtml::header(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;

    return text;
}

void AModuleDocHtml::body_rec(ATreeNav<AModule> modules) {
    save_file(modules);
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        body_rec(modules);
        modules.goto_parent();
    }
}

AString AModuleDocHtml::body(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    if (modules.has_data()) {
        
    }
    return text;
}

AString AModuleDocHtml::end(ATreeNav<AModule> modules) {
    AString text;

    return text;
}

AModuleDocHtml::~AModuleDocHtml() {

}