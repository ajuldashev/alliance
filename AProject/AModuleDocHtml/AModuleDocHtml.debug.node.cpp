#include "AModuleDocHtml.hpp"

#ifdef DEBUG_AMODULEDOCHTML
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_amoduledochtml(AString com) {
	#ifdef DEBUG_ADOCHTMLPARSERCPP
	if (com == "adochtmlparsercpp") {
		if (debug_adochtmlparsercpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ADOCHTMLPARSERHPP
	if (com == "adochtmlparserhpp") {
		if (debug_adochtmlparserhpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_ADOCHTMLPARSERTEST
	if (com == "adochtmlparsertest") {
		if (debug_adochtmlparsertest()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_amoduledochtml() {
	AString text;
	#ifdef DEBUG_ADOCHTMLPARSERCPP
	text += "d) adochtmlparsercpp\n";
	#endif
	#ifdef DEBUG_ADOCHTMLPARSERHPP
	text += "d) adochtmlparserhpp\n";
	#endif
	#ifdef DEBUG_ADOCHTMLPARSERTEST
	text += "d) adochtmlparsertest\n";
	#endif
	return text;
}

#endif
