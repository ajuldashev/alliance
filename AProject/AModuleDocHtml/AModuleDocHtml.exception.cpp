#include "AModuleDocHtml.exception.hpp"

AModuleDocHtmlException::AModuleDocHtmlException() : AllianceException() {
	exception = "Alliance-Exception-AModuleDocHtml";
}

AModuleDocHtmlException::AModuleDocHtmlException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleDocHtml");
}

AModuleDocHtmlException::AModuleDocHtmlException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleDocHtml");
}

AModuleDocHtmlException::AModuleDocHtmlException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleDocHtml");
}

AModuleDocHtmlException::AModuleDocHtmlException(const AModuleDocHtmlException & obj) : AllianceException(obj) {
	
}

AModuleDocHtmlException::~AModuleDocHtmlException() {

}

