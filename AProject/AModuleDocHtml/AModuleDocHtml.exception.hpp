#ifndef HPP_AMODULEDOCHTMLEXCEPTION
#define HPP_AMODULEDOCHTMLEXCEPTION
#include <alliance.exception.hpp>

class AModuleDocHtmlException : public AllianceException {
public:
	AModuleDocHtmlException();
	AModuleDocHtmlException(std::string text);
	AModuleDocHtmlException(AllianceException & e);
	AModuleDocHtmlException(AllianceException & e, std::string text);
	AModuleDocHtmlException(const AModuleDocHtmlException & obj);
	~AModuleDocHtmlException();
};

#endif

