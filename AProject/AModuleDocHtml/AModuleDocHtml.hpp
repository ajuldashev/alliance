#ifndef HPP_AMODULEDOCHTML
#define HPP_AMODULEDOCHTML
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.hpp>
#include <AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.hpp>
#include <AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.hpp>
#include "AModuleDocHtml.exception.hpp"

class AModuleDocHtml {
private:
    ATreeNav<AModule> modules;
    void body_rec(ATreeNav<AModule> modules);

    AHtmlConstructor get_html_hpp(ATreeNav<AModule> modules);
    AHtmlConstructor get_html_cpp(ATreeNav<AModule> modules);
    AHtmlConstructor get_html_test(ATreeNav<AModule> modules);

public:
    AModuleDocHtml();
    AModuleDocHtml(ATreeNav<AModule> modules);
    
    void start();
    void save_file(ATreeNav<AModule> modules);
    
    AString to_text(ATreeNav<AModule> modules);
    AString header(ATreeNav<AModule> modules);
    AString body(ATreeNav<AModule> modules);
    AString end(ATreeNav<AModule> modules);

    ~AModuleDocHtml();
};

#endif