#include "AModuleDocHtml.hpp"

#ifdef TEST_AMODULEDOCHTML
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_amoduledochtml(bool is_soft) {
	#ifdef TEST_ADOCHTMLPARSERCPP
	if (!test_adochtmlparsercpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ADOCHTMLPARSERCPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_ADOCHTMLPARSERHPP
	if (!test_adochtmlparserhpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ADOCHTMLPARSERHPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_ADOCHTMLPARSERTEST
	if (!test_adochtmlparsertest(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ADOCHTMLPARSERTEST );
		return is_soft;
	}
	#endif
	return true;
}
#endif
