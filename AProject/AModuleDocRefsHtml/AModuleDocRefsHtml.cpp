#include "AModuleDocRefsHtml.hpp"
#include "AModuleDocRefsHtml.test.cpp"
#include "AModuleDocRefsHtml.test.node.cpp"
#include "AModuleDocRefsHtml.debug.cpp"
#include "AModuleDocRefsHtml.debug.node.cpp"
#include "AModuleDocRefsHtml.exception.cpp"

AModuleDocRefsHtml::AModuleDocRefsHtml() {

}

AModuleDocRefsHtml::AModuleDocRefsHtml(ATreeNav<AModule> modules) {
    this->modules = modules;
}

void AModuleDocRefsHtml::start() {
    body_rec(modules, "");
}

AString AModuleDocRefsHtml::to_text(ATreeNav<AModule> modules, AString path) {
    AString text;
    text += header(modules, path);
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body(modules, path);
        modules.goto_parent();
    }
    text += end(modules, path);
    return text;
}

AString AModuleDocRefsHtml::header(ATreeNav<AModule> modules, AString path) {
    AString text;
    text += "\t\t\t\t<ul>\n";
    return text;
}

void AModuleDocRefsHtml::body_rec(ATreeNav<AModule> modules, AString path) {
    replase_html_refs(modules, path);
    replace_css_ref(modules, path);
    replase_status(modules, path);
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        body_rec(modules, path + "../");
        modules.goto_parent();
    }
}

AString AModuleDocRefsHtml::body(ATreeNav<AModule> modules, AString path) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    if (modules.has_data()) {
        text = AHtmlConstructor::make()
            
            .begin("li")
                .begin("a")
                    .attribute("href", name_module + "/" + name_module + ".doc.html")
                    .text(name_module)
                .end("a")
            .end("li")

            .add_space(4)
            .to_text();
    }
    return text;
}

AString AModuleDocRefsHtml::end(ATreeNav<AModule> modules, AString path) {
    return AHtmlConstructor::make()
            
            .begin("li")
                .begin("a")
                    .attribute("href", path + "alliance.doc.html")
                    .text("На главную")
                .end("a")
            .end("li")

            .begin("li")
                .begin("a")
                    .attribute("onclick", "javascript:history.back(-2); return false;")
                    .text("Назад")
                .end("a")
            .end("li")
        
            .add_space(1)
            
            .tag("/ul")

            .add_space(4)

            .to_text();
}

void AModuleDocRefsHtml::replace_css_ref(ATreeNav<AModule> modules, AString path_call_back) {
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        AString path_to_html_file = path + "/" + name + ".doc.html";
        AString new_tag_css = "\t\t<link rel=\"stylesheet\" href=\"" + path_call_back + "style.css\">\n";
        AIFile file(path_to_html_file);
        AString buff = "";
        AString line = file.get_line();
        while (!file.is_eof()) {
            s64int pos = line.find("<link rel=\"stylesheet\" href=\"");
            if (pos != -1) {
                buff += new_tag_css;
                line = file.get_line();
                break;
            } else {
                buff += line + "\n";
            }
            line = file.get_line();
        }
        while (!file.is_eof()) {
            buff += line + "\n";
            line = file.get_line();
        }
        file.close();
        AIO::to_file(buff, path_to_html_file);
    }
}

void AModuleDocRefsHtml::replase_html_refs(ATreeNav<AModule> modules, AString path_call_back) {
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        AString path_to_html_file = path + "/" + name + ".doc.html";
        AString new_refs = to_text(modules, path_call_back);
        AIFile file(path_to_html_file);
        AString buff = "";
        AString line = file.get_line();
        while (!file.is_eof()) {
            s64int pos = line.find("id=\"hrefs-block-id\"");
            if (pos != -1) {
                buff += line + "\n";
                buff += new_refs;
                line = file.get_line();
                s64int pos_end_div = line.find("</div");
                while (!file.is_eof() && pos_end_div == -1) {
                    line = file.get_line();
                    pos_end_div = line.find("</div");
                }
                break;
            } else {
                buff += line + "\n";
            }
            line = file.get_line();
        }
        while (!file.is_eof()) {
            buff += line + "\n";
            line = file.get_line();
        }
        file.close();
        AIO::to_file(buff, path_to_html_file);
    }
}

void AModuleDocRefsHtml::replase_status(ATreeNav<AModule> modules, AString path_call_back) {
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        AString path_to_html_file = path + "/" + name + ".doc.html";
        AString path_to_status_file = path + "/" + name + ".status";
        AString new_status = "\t<body id=\"body-status-id\" class=\"" + AIO::from_file(path_to_status_file) + "\">\n";
        AIFile file(path_to_html_file);
        AString buff = "";
        AString line = file.get_line();
        while (!file.is_eof()) {
            s64int pos = line.find("<body id=\"body-status-id\" class=\"");
            if (pos != -1) {
                buff += new_status;
                line = file.get_line();
                break;
            } else {
                buff += line + "\n";
            }
            line = file.get_line();
        }
        while (!file.is_eof()) {
            buff += line + "\n";
            line = file.get_line();
        }
        file.close();
        AIO::to_file(buff, path_to_html_file);
    }
}

AModuleDocRefsHtml::~AModuleDocRefsHtml() {

}