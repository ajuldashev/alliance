#include "AModuleDocRefsHtml.hpp"

#ifdef DEBUG_AMODULEDOCREFSHTML
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_amoduledocrefshtml() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AMODULEDOCREFSHTML );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_amoduledocrefshtml());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");	
		}
		if (com == "exit") {
			return true;
		}
        if (debug_node_amoduledocrefshtml(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AMODULEDOCREFSHTML );
    AIO::write_div_line();
    return false;
}
#endif

