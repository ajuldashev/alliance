#include "AModuleDocRefsHtml.exception.hpp"

AModuleDocRefsHtmlException::AModuleDocRefsHtmlException() : AllianceException() {
	exception = "Alliance-Exception-AModuleDocRefsHtml";
}

AModuleDocRefsHtmlException::AModuleDocRefsHtmlException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleDocRefsHtml");
}

AModuleDocRefsHtmlException::AModuleDocRefsHtmlException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleDocRefsHtml");
}

AModuleDocRefsHtmlException::AModuleDocRefsHtmlException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleDocRefsHtml");
}

AModuleDocRefsHtmlException::AModuleDocRefsHtmlException(const AModuleDocRefsHtmlException & obj) : AllianceException(obj) {
	
}

AModuleDocRefsHtmlException::~AModuleDocRefsHtmlException() {

}

