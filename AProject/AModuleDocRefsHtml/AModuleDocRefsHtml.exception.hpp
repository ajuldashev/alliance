#ifndef HPP_AMODULEDOCREFSHTMLEXCEPTION
#define HPP_AMODULEDOCREFSHTMLEXCEPTION
#include <alliance.exception.hpp>

class AModuleDocRefsHtmlException : public AllianceException {
public:
	AModuleDocRefsHtmlException();
	AModuleDocRefsHtmlException(std::string text);
	AModuleDocRefsHtmlException(AllianceException & e);
	AModuleDocRefsHtmlException(AllianceException & e, std::string text);
	AModuleDocRefsHtmlException(const AModuleDocRefsHtmlException & obj);
	~AModuleDocRefsHtmlException();
};

#endif

