#ifndef HPP_AMODULEDOCREFSHTML
#define HPP_AMODULEDOCREFSHTML
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <ACore/ABase/AIO/AFile/AIFile/AIFile.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleDocRefsHtml.exception.hpp"

class AModuleDocRefsHtml {
private:
    ATreeNav<AModule> modules;
    void body_rec(ATreeNav<AModule> modules, AString path_call_back);
public:
    AModuleDocRefsHtml();
    AModuleDocRefsHtml(ATreeNav<AModule> modules);
    
    void start();
    
    AString to_text(ATreeNav<AModule> modules, AString path_call_back);
    AString header(ATreeNav<AModule> modules, AString path_call_back);
    AString body(ATreeNav<AModule> modules, AString path_call_back);
    AString end(ATreeNav<AModule> modules, AString path_call_back);

    void replace_css_ref(ATreeNav<AModule> modules, AString path_call_back);
    void replase_html_refs(ATreeNav<AModule> modules, AString path_call_back);
    void replase_status(ATreeNav<AModule> modules, AString path_call_back);

    ~AModuleDocRefsHtml();
};

#endif