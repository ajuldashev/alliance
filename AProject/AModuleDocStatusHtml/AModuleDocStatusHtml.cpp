#include "AModuleDocStatusHtml.hpp"
#include "AModuleDocStatusHtml.test.cpp"
#include "AModuleDocStatusHtml.test.node.cpp"
#include "AModuleDocStatusHtml.debug.cpp"
#include "AModuleDocStatusHtml.debug.node.cpp"
#include "AModuleDocStatusHtml.exception.cpp"

AModuleDocStatusHtml::AModuleDocStatusHtml() {

}

AModuleDocStatusHtml::AModuleDocStatusHtml(ATreeNav<AModule> modules) {
    this->modules = modules;
}

AString AModuleDocStatusHtml::to_text() {
    return AHtmlConstructor::make()

        .tag("!DOCTYPE html")

        .begin("html")
            .begin("head")
                .tag("meta")
                    .attribute("charset", "utf-8")
                .begin("title")
                    .text("Документация по проекту Alliance")
                .end("title")
                .tag("link")
                    .attribute("rel", "stylesheet")
                    .attribute("href", "style.css")
            .end("head")
            .begin("body")
                .attribute("id", "body-status-id")
                .attribute("class", "status-module-stable")
                .begin("div")
                    .attribute("class", "main-block")

                    .begin("div")
                        .attribute("class", "head-block")
                        .begin("h1")
                            .text("Status")
                        .end("h1")
                    .end("div")

                    .begin("div")
                        .attribute("class", "hrefs-block")
                        .begin("ul")
                            .begin("li")
                                .begin("a")
                                    .attribute("href", "alliance.doc.html")
                                    .text("На главную")
                                .end("a")
                            .end("li")
                            .begin("li")
                                .begin("a")
                                    .attribute("onclick", "javascript:history.back(-2); return false;")
                                    .text("Назад")
                                .end("a")
                            .end("li")    
                        .end("ul")
                    .end("div")

                    .begin("div")
                        .attribute("class", "body-block")
                        .tag("br")
                            .attribute("class", "tab-1")
                        .text(body())
                    .end("div")

                .end("div")
            .end("body")
        .end("html")
        .to_text();
}

AString AModuleDocStatusHtml::header() {
    AString text;
    return text;
}

AString AModuleDocStatusHtml::body_rec(ATreeNav<AModule> modules, s64int tab) {
    AString text;
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".doc.html";
        AString file_status = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".status";
        AString status = get_status(file_status);
        text += AHtmlConstructor::make()
            .begin("a")
                .attribute("href", full_name)
                .attribute("class", "text-" + status + " tab-" + AStringLibs::to_string(tab))
                .text(name)
            .end("a")
            .tag("br")
            .to_text();
    }
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body_rec(modules, tab + 1);
        modules.goto_parent();
    }
    
    return text;
}

AString AModuleDocStatusHtml::body() {
    AString text;
    AModuleSizeInfo info(modules);
    text += info.to_text();
    text += body_rec(modules, 0);
    return text;
}

AString AModuleDocStatusHtml::end() {
    AString text;
    return text;
}

AString AModuleDocStatusHtml::get_status(AString name) {
    AString text = AIO::from_file(name);
    
    if (
        text != "status-module-not-ready" &&
        text != "status-module-in-progress" &&
        text != "status-module-ready" &&
        text != "status-module-stable"
    ) {
        text = "status-module-not-ready";
    }
    return text;
}

AModuleDocStatusHtml::~AModuleDocStatusHtml() {

}
