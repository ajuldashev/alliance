#include "AModuleDocStatusHtml.hpp"

#ifdef DEBUG_AMODULEDOCSTATUSHTML
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_amoduledocstatushtml(AString com) {
	#ifdef DEBUG_AMODULESIZEINFO
	if (com == "amodulesizeinfo") {
		if (debug_amodulesizeinfo()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_amoduledocstatushtml() {
	AString text;
	#ifdef DEBUG_AMODULESIZEINFO
	text += "d) amodulesizeinfo\n";
	#endif
	return text;
}

#endif
