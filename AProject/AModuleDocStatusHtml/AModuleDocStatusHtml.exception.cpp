#include "AModuleDocStatusHtml.exception.hpp"

AModuleDocStatusHtmlException::AModuleDocStatusHtmlException() : AllianceException() {
	exception = "Alliance-Exception-AModuleDocStatusHtml";
}

AModuleDocStatusHtmlException::AModuleDocStatusHtmlException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleDocStatusHtml");
}

AModuleDocStatusHtmlException::AModuleDocStatusHtmlException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleDocStatusHtml");
}

AModuleDocStatusHtmlException::AModuleDocStatusHtmlException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleDocStatusHtml");
}

AModuleDocStatusHtmlException::AModuleDocStatusHtmlException(const AModuleDocStatusHtmlException & obj) : AllianceException(obj) {

}

AModuleDocStatusHtmlException::~AModuleDocStatusHtmlException() {

}
