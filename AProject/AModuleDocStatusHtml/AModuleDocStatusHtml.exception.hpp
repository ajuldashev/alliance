#ifndef HPP_AMODULEDOCSTATUSHTMLEXCEPTION
#define HPP_AMODULEDOCSTATUSHTMLEXCEPTION
#include <alliance.exception.hpp>

class AModuleDocStatusHtmlException : public AllianceException {
public:
	AModuleDocStatusHtmlException();
	AModuleDocStatusHtmlException(std::string text);
	AModuleDocStatusHtmlException(AllianceException & e);
	AModuleDocStatusHtmlException(AllianceException & e, std::string text);
	AModuleDocStatusHtmlException(const AModuleDocStatusHtmlException & obj);
	~AModuleDocStatusHtmlException();
};

#endif
