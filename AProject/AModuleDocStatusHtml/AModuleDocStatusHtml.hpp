#ifndef HPP_AMODULEDOCSTATUSHTML
#define HPP_AMODULEDOCSTATUSHTML
#include <AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.hpp>
#include "AModuleDocStatusHtml.exception.hpp"

class AModuleDocStatusHtml {
private:
    ATreeNav<AModule> modules;
    AString body_rec(ATreeNav<AModule> modules, s64int tab);
public:
    AModuleDocStatusHtml();
    AModuleDocStatusHtml(ATreeNav<AModule> modules);
    
    AString to_text();
    AString header();
    AString body();
    AString end();

    AString get_status(AString);

    ~AModuleDocStatusHtml();
};

#endif
