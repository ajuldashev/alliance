#include "AModuleDocStatusHtml.hpp"

#ifdef TEST_AMODULEDOCSTATUSHTML
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_amoduledocstatushtml(bool is_soft) {
	#ifdef TEST_AMODULESIZEINFO
	if (!test_amodulesizeinfo(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULESIZEINFO );
		return is_soft;
	}
	#endif
	return true;
}
#endif
