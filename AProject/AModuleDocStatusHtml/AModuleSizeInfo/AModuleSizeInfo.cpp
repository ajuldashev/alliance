#include "AModuleSizeInfo.hpp"
#include "AModuleSizeInfo.test.cpp"
#include "AModuleSizeInfo.test.node.cpp"
#include "AModuleSizeInfo.debug.cpp"
#include "AModuleSizeInfo.debug.node.cpp"
#include "AModuleSizeInfo.exception.cpp"

void AModuleSizeInfo::collect_count_lines_rec(ATreeNav<AModule> modules) {
    if (modules.has_data()) {
        count_lines_hpp += get_count_lines_hpp(modules);
        count_lines_cpp += get_count_lines_cpp(modules);
        count_lines_test_cpp += get_count_lines_test_cpp(modules);
        count_lines_test_node_cpp += get_count_lines_test_node_cpp(modules);
        count_lines_debug_cpp += get_count_lines_debug_cpp(modules);
        count_lines_debug_node_cpp += get_count_lines_debug_node_cpp(modules);
        count_lines_exception_hpp += get_count_lines_exception_hpp(modules);
        count_lines_exception_cpp += get_count_lines_exception_cpp(modules);
        count_lines_doc_html += get_count_lines_doc_html(modules);
        count_lines_status += get_count_lines_status(modules); 
    }
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        collect_count_lines_rec(modules);
        modules.goto_parent();
    }
}

void AModuleSizeInfo::collect_count_lines() {

    count_lines_config_hpp = get_count_lines("alliance.config.hpp");
    count_lines_hpp = 0;
    count_lines_cpp = 0;
    count_lines_test_cpp = 0;
    count_lines_test_node_cpp = 0;
    count_lines_test_macros_cpp = get_count_lines("alliance.test.macros.hpp");
    count_lines_debug_cpp = 0;
    count_lines_debug_node_cpp = 0;
    count_lines_exception_hpp = 0;
    count_lines_exception_cpp = 0;
    count_lines_doc_tutorial_html = get_count_lines("alliance.doc.tutorial.html");
    count_lines_doc_status_html = get_count_lines("alliance.doc.status.html");
    count_lines_doc_html = 0;
    count_lines_status = 0;
    count_lines_makefile = get_count_lines("makefile");
    count_lines_style_css = get_count_lines("style.css");

    collect_count_lines_rec(modules);
    
    count_lines_hpp += get_count_lines("alliance.hpp");
    count_lines_cpp += get_count_lines("alliance.cpp");
    count_lines_test_cpp += get_count_lines("alliance.test.cpp");
    count_lines_test_node_cpp += get_count_lines("alliance.test.node.cpp");
    count_lines_debug_cpp += get_count_lines("alliance.debug.cpp");
    count_lines_debug_node_cpp += get_count_lines("alliance.debug.node.cpp");
    count_lines_exception_hpp += get_count_lines("alliance.exception.hpp");
    count_lines_exception_cpp += get_count_lines("alliance.exception.cpp");
    count_lines_doc_html += get_count_lines("alliance.doc.html");
    count_lines_status += get_count_lines("alliance.status");

    count_lines = count_lines_config_hpp
        + count_lines_hpp
        + count_lines_cpp
        + count_lines_test_cpp
        + count_lines_test_node_cpp
        + count_lines_test_macros_cpp
        + count_lines_debug_cpp
        + count_lines_debug_node_cpp
        + count_lines_exception_hpp
        + count_lines_exception_cpp
        + count_lines_doc_tutorial_html
        + count_lines_doc_status_html
        + count_lines_doc_html
        + count_lines_status
        + count_lines_makefile
        + count_lines_style_css;
}

AModuleSizeInfo::AModuleSizeInfo() {

}

AModuleSizeInfo::AModuleSizeInfo(ATreeNav<AModule> modules) {
    this->modules = modules;
}

AString AModuleSizeInfo::to_text_count_line(AString title, u64int count) {
    return AHtmlConstructor::make()
        .begin("div")
            .attribute("class", "code-row")
            .begin("div")
                .attribute("class", "code-cell-type")
                .text(title)
            .end("div")
            .begin("div")
                .attribute("class", "code-cell-name")
                .text(AStringLibs::to_string(count))
            .end("div")
        .end("div")
        .to_text();
}

AString AModuleSizeInfo::to_text() {
    collect_count_lines();
    return AHtmlConstructor::make()
        .begin("div")
            .attribute("class", "code-table")
            .text(body())
        .end("div")
        .tag("br")
            .attribute("class", "tab-1")
        .to_text();
}

AString AModuleSizeInfo::header() {
    AString text;
    return text;
}

AString AModuleSizeInfo::body() {
    AString text;
    
    text += to_text_count_line("Всего строк", count_lines);

    text += to_text_count_line("*.hpp", count_lines_hpp);
    text += to_text_count_line("*.cpp", count_lines_cpp);
    text += to_text_count_line("*.test.cpp", count_lines_test_cpp);
    text += to_text_count_line("*.test.node.cpp", count_lines_test_node_cpp);

    text += to_text_count_line("*.debug.cpp", count_lines_debug_cpp);
    text += to_text_count_line("*.debug.node.cpp", count_lines_debug_node_cpp);
    text += to_text_count_line("*.exception.hpp", count_lines_exception_hpp);
    text += to_text_count_line("*.exception.cpp", count_lines_exception_cpp);

    text += to_text_count_line("*.doc.html", count_lines_doc_html);
    text += to_text_count_line("*.status", count_lines_status);
    text += to_text_count_line("makefile", count_lines_makefile);
    text += to_text_count_line("style.css", count_lines_style_css);

    text += to_text_count_line("alliance.config.hpp", count_lines_config_hpp);
    text += to_text_count_line("alliance.test.macros.hpp", count_lines_test_macros_cpp);
    text += to_text_count_line("alliance.doc.tutorial.html", count_lines_doc_tutorial_html);
    text += to_text_count_line("alliance.doc.status.html", count_lines_doc_status_html);

    return text;
}

AString AModuleSizeInfo::end() {
    AString text;
    return text;
}

u64int AModuleSizeInfo::get_count_lines(AString file_name) {
    AIFile file(file_name);
    u64int count = 0;
    while (!file.is_eof()) {
        file.get_line();
        count += 1;
    }
    return count;
}

u64int AModuleSizeInfo::get_count_lines_hpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".hpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_test_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".test.cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_test_node_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".test.node.cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_debug_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".debug.cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_debug_node_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".debug.node.cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_exception_hpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".exception.hpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_exception_cpp(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".exception.cpp";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_doc_html(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".doc.html";
    return get_count_lines(full_name);
}

u64int AModuleSizeInfo::get_count_lines_status(ATreeNav<AModule> modules) {
    AString full_name = modules.at().get_info().full_name() + "/" + modules.at().get_info().get_name() + ".status";
    return get_count_lines(full_name);
}

AModuleSizeInfo::~AModuleSizeInfo() {

}