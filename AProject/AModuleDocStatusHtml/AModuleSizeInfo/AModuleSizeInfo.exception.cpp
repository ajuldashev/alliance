#include "AModuleSizeInfo.exception.hpp"

AModuleSizeInfoException::AModuleSizeInfoException() : AllianceException() {
	exception = "Alliance-Exception-AModuleSizeInfo";
}

AModuleSizeInfoException::AModuleSizeInfoException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleSizeInfo");
}

AModuleSizeInfoException::AModuleSizeInfoException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleSizeInfo");
}

AModuleSizeInfoException::AModuleSizeInfoException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleSizeInfo");
}

AModuleSizeInfoException::AModuleSizeInfoException(const AModuleSizeInfoException & obj) : AllianceException(obj) {
	
}

AModuleSizeInfoException::~AModuleSizeInfoException() {

}

