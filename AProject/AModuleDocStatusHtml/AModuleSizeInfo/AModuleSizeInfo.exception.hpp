#ifndef HPP_AMODULESIZEINFOEXCEPTION
#define HPP_AMODULESIZEINFOEXCEPTION
#include <alliance.exception.hpp>

class AModuleSizeInfoException : public AllianceException {
public:
	AModuleSizeInfoException();
	AModuleSizeInfoException(std::string text);
	AModuleSizeInfoException(AllianceException & e);
	AModuleSizeInfoException(AllianceException & e, std::string text);
	AModuleSizeInfoException(const AModuleSizeInfoException & obj);
	~AModuleSizeInfoException();
};

#endif

