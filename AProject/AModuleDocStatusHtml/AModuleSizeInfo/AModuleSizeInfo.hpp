#ifndef HPP_AMODULESIZEINFO
#define HPP_AMODULESIZEINFO
#include <ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp>
#include <ACore/ABase/AIO/AFile/AIFile/AIFile.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleSizeInfo.exception.hpp"

class AModuleSizeInfo {
private:
    ATreeNav<AModule> modules;

    u64int count_lines;

    u64int count_lines_config_hpp;

    u64int count_lines_hpp;
    u64int count_lines_cpp;

    u64int count_lines_test_cpp;
    u64int count_lines_test_node_cpp;
    u64int count_lines_test_macros_cpp;

    u64int count_lines_debug_cpp;
    u64int count_lines_debug_node_cpp;

    u64int count_lines_exception_hpp;
    u64int count_lines_exception_cpp;

    u64int count_lines_doc_tutorial_html;
    u64int count_lines_doc_status_html;
    u64int count_lines_doc_html;
    
    u64int count_lines_status;

    u64int count_lines_makefile;

    u64int count_lines_style_css;

    void collect_count_lines_rec(ATreeNav<AModule> modules);
    void collect_count_lines();
public:
    AModuleSizeInfo();
    AModuleSizeInfo(ATreeNav<AModule> modules);

    AString to_text_count_line(AString title, u64int count);    
    AString to_text();
    AString header();
    AString body();
    AString end();

    u64int get_count_lines(AString file_name);

    u64int get_count_lines_hpp(ATreeNav<AModule> modules);
    u64int get_count_lines_cpp(ATreeNav<AModule> modules);

    u64int get_count_lines_test_cpp(ATreeNav<AModule> modules);
    u64int get_count_lines_test_node_cpp(ATreeNav<AModule> modules);

    u64int get_count_lines_debug_cpp(ATreeNav<AModule> modules);
    u64int get_count_lines_debug_node_cpp(ATreeNav<AModule> modules);

    u64int get_count_lines_exception_hpp(ATreeNav<AModule> modules);
    u64int get_count_lines_exception_cpp(ATreeNav<AModule> modules);

    u64int get_count_lines_doc_html(ATreeNav<AModule> modules);
    u64int get_count_lines_status(ATreeNav<AModule> modules); 

    ~AModuleSizeInfo();
};

#endif