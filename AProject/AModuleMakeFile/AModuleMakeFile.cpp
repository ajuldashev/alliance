#include "AModuleMakeFile.hpp"
#include "AModuleMakeFile.test.cpp"
#include "AModuleMakeFile.test.node.cpp"
#include "AModuleMakeFile.debug.cpp"
#include "AModuleMakeFile.debug.node.cpp"
#include "AModuleMakeFile.exception.cpp"

AModuleMakeFile::AModuleMakeFile() {

}

AModuleMakeFile::AModuleMakeFile(ATreeNav<AModule> modules) {
	this->modules = modules;
}

AString AModuleMakeFile::to_text() {
	AString text;
	text += header();
	text += body();
	text += end();
	return text;
}

AString AModuleMakeFile::header() {
	AString text;
	
	return text;
}

AString AModuleMakeFile::body_rec(ATreeNav<AModule> modules, AString tab) {
	AString text;
	AString local_dir = "out/";
	AString name_module = modules.at().to_text_module().to_lower();
	AString name_modules = name_module + "_modules.o";
	AString line_1 = local_dir + name_modules + " : ";
	AString line_2 = "\tld -r ";
	AString target = modules.at().to_text_module().to_lower();
	AString target_name = modules.at().get_info().get_name();
	AString target_file = AModule::fix_path(modules.at().get_info().full_name()) + "/" + target_name;
	AString file_hpp = target_file + ".hpp ";
	AString file_cpp = target_file + ".cpp ";
	AString file_test_cpp = target_file + ".test.cpp ";
	AString file_debug_cpp = target_file + ".debug.cpp ";
	AString file_exception_h = target_file + ".exception.hpp ";
	AString file_exception_cpp = target_file + ".exception.cpp ";
	AString line_files = file_hpp + file_cpp + file_test_cpp + file_debug_cpp + file_exception_h + file_exception_cpp;
	AString target_line_1 = local_dir + target + ".o " + " : " + line_files + " \n";
	AString target_line_2 = "\tg++ -I. -std=c++11 -c " + target_file + ".cpp -o " + local_dir + target + ".o \n";
	line_1 += local_dir + target + ".o";
	line_2 += local_dir + target + ".o";
	text += target_line_1;
	text += target_line_2;
	text += "\n";
	for (int i = 0; i < modules.length(); i += 1) {
		line_1 += " " + local_dir + modules.at(i).to_text_module().to_lower() + "_modules.o";
		line_2 += " " + local_dir + modules.at(i).to_text_module().to_lower() + "_modules.o";
		modules.goto_child(i);
		text += body_rec(modules, tab + "\t");
		modules.goto_parent();
	}
	text += line_1 + "\n";
	text += line_2 + " -o " + local_dir + name_modules + "\n";
	text += "\n";
	return text;
}

AString AModuleMakeFile::body() {
	AString text;
	AString local_dir = "out/";
	AString target = "alliance";
	text += "all : " + target + "\n\n";
	AString path_to_file = target + ".cpp";
	AString line_1 = target + " : ";
	AString line_2 = "\tg++ -I. -std=c++11 ";
	if (AFileInfo::is_exist(path_to_file)) {
		AString file_hpp = target + ".hpp ";
		AString file_cpp = target + ".cpp ";
		AString file_test_cpp = target + ".test.cpp ";
		AString file_debug_cpp = target + ".debug.cpp ";
		AString file_exception_h = target + ".exception.hpp ";
		AString file_exception_cpp = target + ".exception.cpp ";
		AString line_files = 
			file_hpp + 
			file_cpp + 
			file_test_cpp + 
			file_debug_cpp + 
			file_exception_h + 
			file_exception_cpp;
		AString target_line_1 = target + ".o " + " : " + line_files + " \n";
		AString target_line_2 = "\tg++ -I. -std=c++11 -c " + target + ".cpp -o " + target + ".o \n";
		line_1 += target + ".o";
		line_2 += target + ".o";
		text += target_line_1;
		text += target_line_2;
		text += "\n";
	}
	for (int i = 0; i < modules.length(); i += 1) {
		line_1 += " " + local_dir + modules.at(i).to_text_module().to_lower() + "_modules.o";
		line_2 += " " + local_dir + modules.at(i).to_text_module().to_lower() + "_modules.o";
		modules.goto_child(i);
		text += body_rec(modules, "");
		modules.goto_parent();
	}
	text += line_1 + "\n";
	text += line_2 + " -o " + target + "\n";
	return text;
}

AString AModuleMakeFile::end() {
	AString text;
	text += "\n";
	text += "clean : \n";
	text += "\trm *.o alliance out/*.o\n";
	text += "\n";
	text += "memtest : \n";
	text += "\tvalgrind -v --tool=memcheck --leak-check=full ./alliance\n";
	text += "save : alliance\n";
	text += "\tcp alliance alliance.save\n";
	text += "config : alliance.save\n";
	text += "\t./config.sh\n";
	text += "start : alliance\n";
	text += "\t./alliance\n";
	return text;
}

AModuleMakeFile::~AModuleMakeFile() {

}
