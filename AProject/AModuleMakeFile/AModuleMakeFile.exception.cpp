#include "AModuleMakeFile.exception.hpp"

AModuleMakeFileException::AModuleMakeFileException() : AllianceException() {
	exception = "Alliance-Exception-AModuleMakeFile";
}

AModuleMakeFileException::AModuleMakeFileException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleMakeFile");
}

AModuleMakeFileException::AModuleMakeFileException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleMakeFile");
}

AModuleMakeFileException::AModuleMakeFileException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleMakeFile");
}

AModuleMakeFileException::AModuleMakeFileException(const AModuleMakeFileException & obj) : AllianceException(obj) {
	
}

AModuleMakeFileException::~AModuleMakeFileException() {

}

