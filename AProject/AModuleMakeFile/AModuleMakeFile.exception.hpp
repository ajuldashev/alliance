#ifndef HPP_AMODULEMAKEFILEEXCEPTION
#define HPP_AMODULEMAKEFILEEXCEPTION
#include <alliance.exception.hpp>

class AModuleMakeFileException : public AllianceException {
public:
	AModuleMakeFileException();
	AModuleMakeFileException(std::string text);
	AModuleMakeFileException(AllianceException & e);
	AModuleMakeFileException(AllianceException & e, std::string text);
	AModuleMakeFileException(const AModuleMakeFileException & obj);
	~AModuleMakeFileException();
};

#endif

