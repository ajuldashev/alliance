#ifndef HPP_AMODULEMAKEFILE
#define HPP_AMODULEMAKEFILE
#include <alliance.config.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleMakeFile.exception.hpp"

class AModuleMakeFile {
private:
    ATreeNav<AModule> modules;
    AString body_rec(ATreeNav<AModule> modules, AString tab);
public:
    AModuleMakeFile();
    AModuleMakeFile(ATreeNav<AModule> modules);
    
    AString to_text();
    AString header();
    AString body();
    AString end();

    ~AModuleMakeFile();
};

#endif