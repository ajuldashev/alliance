#include "AModuleTestHeader.hpp"
#include "AModuleTestHeader.test.cpp"
#include "AModuleTestHeader.test.node.cpp"
#include "AModuleTestHeader.debug.cpp"
#include "AModuleTestHeader.debug.node.cpp"
#include "AModuleTestHeader.exception.cpp"

AModuleTestHeader::AModuleTestHeader() {

}

AModuleTestHeader::AModuleTestHeader(ATreeNav<AModule> modules) {
    this->modules = modules;
}

AString AModuleTestHeader::to_text() {
    AString text;
    text += header();
    text += body();
    text += end();
    return text;
}

AString AModuleTestHeader::header() {
    AString text;
    text += "#ifndef HPP_TEST_FILE\n";
    text += "#define HPP_TEST_FILE\n";
    text += "#define TEST_ALLIANCE\n";
    text += "#ifdef  TEST_ALLIANCE\n";
    text += "bool test_alliance(bool is_soft = false);\n";
    text += "bool test_node_alliance(bool is_soft = false);\n";
    return text;
}

AString AModuleTestHeader::body_rec(ATreeNav<AModule> modules, AString tab) {
    AString text;
    if (modules.has_data()) {
        AString deff = "TEST_" + modules.at().to_text_module().to_upper();
        AString func = "test_" + modules.at().to_text_module().to_lower() + "(bool is_soft = false);";
        AString func_node = "test_node_" + modules.at().to_text_module().to_lower() + "(bool is_soft = false);";
        AString deff_full_name = "\"" + modules.at().to_full_name_module() + "\"";
        text += tab + "#define " + deff + "\n";
        text += tab + "#ifdef  " + deff + "\n";
        text += tab + "\tbool " + func + "\n";
        text += tab + "\tbool " + func_node + "\n";
        text += tab + "\t#define MODULE_" + deff + " " + deff_full_name + "\n";
    }
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body_rec(modules, tab + "\t");
        modules.goto_parent();
    }
    if (modules.has_data()) {
        text += tab + "#endif" + "\n";
    }
    return text;
}

AString AModuleTestHeader::body() {
    AString text;
    text += body_rec(modules, "");
    return text;
}

AString AModuleTestHeader::end() {
    AString text;
    text += "#endif\n";
    text += "#endif\n";
    return text;
}

AModuleTestHeader::~AModuleTestHeader() {

}