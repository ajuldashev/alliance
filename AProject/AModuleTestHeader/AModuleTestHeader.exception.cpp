#include "AModuleTestHeader.exception.hpp"

AModuleTestHeaderException::AModuleTestHeaderException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTestHeader";
}

AModuleTestHeaderException::AModuleTestHeaderException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTestHeader");
}

AModuleTestHeaderException::AModuleTestHeaderException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTestHeader");
}

AModuleTestHeaderException::AModuleTestHeaderException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTestHeader");
}

AModuleTestHeaderException::AModuleTestHeaderException(const AModuleTestHeaderException & obj) : AllianceException(obj) {
	
}

AModuleTestHeaderException::~AModuleTestHeaderException() {

}

