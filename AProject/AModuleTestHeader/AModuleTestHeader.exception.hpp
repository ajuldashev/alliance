#ifndef HPP_AMODULETESTHEADEREXCEPTION
#define HPP_AMODULETESTHEADEREXCEPTION
#include <alliance.exception.hpp>

class AModuleTestHeaderException : public AllianceException {
public:
	AModuleTestHeaderException();
	AModuleTestHeaderException(std::string text);
	AModuleTestHeaderException(AllianceException & e);
	AModuleTestHeaderException(AllianceException & e, std::string text);
	AModuleTestHeaderException(const AModuleTestHeaderException & obj);
	~AModuleTestHeaderException();
};

#endif

