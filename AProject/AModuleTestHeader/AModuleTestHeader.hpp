#ifndef HPP_AMODULETESTHEADER
#define HPP_AMODULETESTHEADER
#include <alliance.config.hpp>
#include <AProject/AModule/AModule.hpp>
#include "AModuleTestHeader.exception.hpp"

class AModuleTestHeader {
private:
    ATreeNav<AModule> modules;
    AString body_rec(ATreeNav<AModule> modules, AString tab);
public:
    AModuleTestHeader();
    AModuleTestHeader(ATreeNav<AModule> modules);

    AString to_text();
    AString header();
    AString body();
    AString end();

    ~AModuleTestHeader();
};

#endif