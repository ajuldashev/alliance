#include "AModuleTestNode.hpp"
#include "AModuleTestNode.test.cpp"
#include "AModuleTestNode.test.node.cpp"
#include "AModuleTestNode.debug.cpp"
#include "AModuleTestNode.debug.node.cpp"
#include "AModuleTestNode.exception.cpp"

AModuleTestNode::AModuleTestNode() {

}

AModuleTestNode::AModuleTestNode(ATreeNav<AModule> modules) {
    this->modules = modules;
}

void AModuleTestNode::start() {
    body_rec(modules);
}

void AModuleTestNode::save_file(ATreeNav<AModule> modules) {
    AString module_cpp = "alliance.test.node.cpp";
    if (modules.has_data()) {
        AString name = modules.at().get_info().get_name();
        AString path = modules.at().get_info().full_name();
        module_cpp = path + "/" + name + ".test.node.cpp";
    }
    AIO::to_file(to_text(modules), module_cpp);
}

AString AModuleTestNode::to_text(ATreeNav<AModule> modules) {
    AString text;
    text += header(modules);
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        text += body(modules);
        modules.goto_parent();
    }
    text += end(modules);
    return text;
}

AString AModuleTestNode::header(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    text += "#include \"";
    text += name_module;
    text += ".hpp\"\n";
    text += "\n";
    text += "#ifdef TEST_";
    text += name_module.to_upper() + "\n";
    text += "#include <ACore/ABase/AIO/AIO.hpp>\n";
    text += "bool test_node_";
    text += name_module.to_lower();
    text += "(bool is_soft) {\n";
    return text;
}

void AModuleTestNode::body_rec(ATreeNav<AModule> modules) {
    save_file(modules);
    for (int i = 0; i < modules.length(); i += 1) {
        modules.goto_child(i);
        body_rec(modules);
        modules.goto_parent();
    }
}

AString AModuleTestNode::body(ATreeNav<AModule> modules) {
    AString name_module = "alliance";
    if (modules.has_data()) {
        name_module = modules.at().to_text_module();
    }
    AString text;
    if (modules.has_data()) {
        text += "\t#ifdef TEST_";
        text += name_module.to_upper() + "\n";
        text += "\tif (!test_";
        text += name_module.to_lower();
        text += "(is_soft)) {\n";
        text += "\t\tAIO::writeln_error(\"FAIL test the module \"";
        text += " MODULE_TEST_";
        text += name_module.to_upper();
        text += " );\n";
        text += "\t\treturn is_soft;\n";
        text += "\t}\n";
        text += "\t#endif\n";
    }
    return text;
}

AString AModuleTestNode::end(ATreeNav<AModule> modules) {
    AString text;
    text += "\treturn true;\n";
    text += "}\n";
    text += "#endif\n";
    return text;
}

AModuleTestNode::~AModuleTestNode() {

}