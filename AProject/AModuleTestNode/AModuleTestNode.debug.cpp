#include "AModuleTestNode.hpp"

#ifdef DEBUG_AMODULETESTNODE
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_amoduletestnode() {
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_AMODULETESTNODE );
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
            AIO::write(debug_list_node_amoduletestnode());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");	
		}
		if (com == "exit") {
			return true;
		}
        if (debug_node_amoduletestnode(com)) {
			return true;
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_AMODULETESTNODE );
    AIO::write_div_line();
    return false;
}
#endif

