#include "AModuleTestNode.exception.hpp"

AModuleTestNodeException::AModuleTestNodeException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTestNode";
}

AModuleTestNodeException::AModuleTestNodeException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTestNode");
}

AModuleTestNodeException::AModuleTestNodeException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTestNode");
}

AModuleTestNodeException::AModuleTestNodeException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTestNode");
}

AModuleTestNodeException::AModuleTestNodeException(const AModuleTestNodeException & obj) : AllianceException(obj) {
	
}

AModuleTestNodeException::~AModuleTestNodeException() {

}

