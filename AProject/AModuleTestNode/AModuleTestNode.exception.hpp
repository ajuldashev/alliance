#ifndef HPP_AMODULETESTNODEEXCEPTION
#define HPP_AMODULETESTNODEEXCEPTION
#include <alliance.exception.hpp>

class AModuleTestNodeException : public AllianceException {
public:
	AModuleTestNodeException();
	AModuleTestNodeException(std::string text);
	AModuleTestNodeException(AllianceException & e);
	AModuleTestNodeException(AllianceException & e, std::string text);
	AModuleTestNodeException(const AModuleTestNodeException & obj);
	~AModuleTestNodeException();
};

#endif

