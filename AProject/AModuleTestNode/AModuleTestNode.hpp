#ifndef HPP_AMODULETESTNODE
#define HPP_AMODULETESTNODE
#include <AProject/AModule/AModule.hpp>
#include "AModuleTestNode.exception.hpp"

class AModuleTestNode {
private:
    ATreeNav<AModule> modules;
    void body_rec(ATreeNav<AModule> modules);
public:
    AModuleTestNode();
    AModuleTestNode(ATreeNav<AModule> modules);
    
    void start();
    void save_file(ATreeNav<AModule> modules);
    
    AString to_text(ATreeNav<AModule> modules);
    AString header(ATreeNav<AModule> modules);
    AString body(ATreeNav<AModule> modules);
    AString end(ATreeNav<AModule> modules);

    ~AModuleTestNode();
};

#endif