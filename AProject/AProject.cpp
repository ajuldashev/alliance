#include "AProject.hpp"
#include "AProject.test.cpp"
#include "AProject.test.node.cpp"
#include "AProject.debug.cpp"
#include "AProject.debug.node.cpp"
#include "AProject.exception.cpp"

AProject::AProject() {
    path = ".";
}

void AProject::load() {
    ADir dir(path);
    dir.open();
    dir.sort();
    ATreeNav<AModule> nav(modules);
    for (s64int i = 0; i < dir.dirs().length(); i += 1) {
        AModule module(dir.dirs()[i]);
        if (module.is_module()) {
            nav.add(module);
            nav.goto_child(nav.length() - 1);
            module.load(nav);
            nav.goto_parent();
        }
    }
}

AString AProject::to_text(AString tab) {
    AString res;
    ATreeNav<AModule> nav(modules);
    for (s64int i = 0; i < nav.length(); i += 1) {
        nav.goto_child(i);
        res = res + nav.at().to_text(nav, tab);
        nav.goto_parent();
    }
    return res;
}

AString AProject::to_text_debug_header() {
    AString text;
    ATreeNav<AModule> nav(modules);
    AModuleDebugHeader temp(nav);
    text = text + temp.to_text();
    return text;
}

AString AProject::to_text_make_file() {
    AString text;
    ATreeNav<AModule> nav(modules);
    AModuleMakeFile temp(nav);
    text = text + temp.to_text();
    return text;
}

AString AProject::to_text_test_header() {
    AString text;
    ATreeNav<AModule> nav(modules);
    AModuleTestHeader temp(nav);
    text = text + temp.to_text();
    return text;
}

AString AProject::to_text_doc_status_html() {
    AString text;
    ATreeNav<AModule> nav(modules);
    AModuleDocStatusHtml temp(nav);
    text = text + temp.to_text();
    return text;
}

void AProject::start_make_test_node() {
    ATreeNav<AModule> nav(modules);
    AModuleTestNode temp(nav);
    temp.start();
}

void AProject::start_make_debug_node() {
    ATreeNav<AModule> nav(modules);
    AModuleDebugNode temp(nav);
    temp.start();
}

void AProject::start_make_doc_html() {
    ATreeNav<AModule> nav(modules);
    AModuleDocHtml temp(nav);
    temp.start();
}

void AProject::start_make_doc_refs_html() {
    ATreeNav<AModule> nav(modules);
    AModuleDocRefsHtml temp(nav);
    temp.start();
}

void AProject::start() {
    AIO::writeln_warning("Start configuration alliance project");
    AIO::writeln_warning("loading ...");
    load();
    AIO::writeln_warning("make makefile file");
    AIO::to_file(to_text_make_file(),  "makefile");
    AIO::writeln_warning("make alliance.debug.hpp file");
    AIO::to_file(to_text_debug_header(),  "alliance.debug.hpp");
    AIO::writeln_warning("make alliance.test.hpp file");
    AIO::to_file(to_text_test_header(), "alliance.test.hpp");
    AIO::writeln_warning("make alliance.doc.status.html file");
    AIO::to_file(to_text_doc_status_html(), "alliance.doc.status.html");
    AIO::writeln_warning("start make test node");
    start_make_test_node();
    AIO::writeln_warning("start make debug node");
    start_make_debug_node();
    AIO::writeln_warning("start make doc html");
    start_make_doc_html();
    AIO::writeln_warning("start make doc refs html");
    start_make_doc_refs_html();
    AIO::writeln_success("Done");
}

void AProject::com() {
    ATreeNav<AModule> nav(modules);
    AProjectCom com(nav);
    com.start();
}

AProject::~AProject() {
    
}