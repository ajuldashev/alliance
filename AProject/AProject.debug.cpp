#include "AProject.hpp"

#ifdef DEBUG_APROJECT
#include <ACore/ABase/AIO/AIO.hpp>
bool debug_aproject() {
	AProject make;
    AString com;
    AIO::write_div_line();
    AIO::writeln("Begin debug the module " MODULE_DEBUG_APROJECT);
    com = AIO::get_command();
    while (com != "stop") {
        if  (com == "help") {
			AIO::write(debug_list_node_aproject());
			AIO::writeln(" 1) help");
			AIO::writeln(" 2) load");
			AIO::writeln(" 3) status");
			AIO::writeln(" 4) write_debug_header");
			AIO::writeln(" 5) write_make_file");
			AIO::writeln(" 6) write_text_header");
			AIO::writeln(" 7) start_make_test_node");
			AIO::writeln(" 8) start_make_doc_refs_html");
			AIO::writeln(" 9) start");
			AIO::writeln("10) com");
			AIO::writeln(" 0) stop");
			AIO::writeln(" e) exit");	
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_aproject(com)) {
			return true;
		}
		if (com == "load") {
			make.load();
		}
		if (com == "status") {
			AIO::writeln(make.to_text());
		}
		if (com == "write_debug_header") {
			AIO::writeln(make.to_text_debug_header());
		}
		if (com == "write_make_file") {
			AIO::writeln(make.to_text_make_file());
		}
		if (com == "write_test_header") {
			AIO::writeln(make.to_text_test_header());
		}
		if (com == "start_make_test_node") {
			make.start_make_test_node();
		}
		if (com == "start_make_doc_refs_html") {
			make.start_make_doc_refs_html();
		}
		if (com == "start") {
			make.start();
		}
		if (com == "com") {
			make.load();
			make.com();
		}
        AIO::write_div_line();
        com = AIO::get_command();
    }
    AIO::writeln("End debug the module " MODULE_DEBUG_APROJECT);
    AIO::write_div_line();
    return false;
}
#endif

