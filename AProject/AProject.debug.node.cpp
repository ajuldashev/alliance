#include "AProject.hpp"

#ifdef DEBUG_APROJECT
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aproject(AString com) {
	#ifdef DEBUG_AMODULE
	if (com == "amodule") {
		if (debug_amodule()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEDEBUGHEADER
	if (com == "amoduledebugheader") {
		if (debug_amoduledebugheader()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEDEBUGNODE
	if (com == "amoduledebugnode") {
		if (debug_amoduledebugnode()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEDOCHTML
	if (com == "amoduledochtml") {
		if (debug_amoduledochtml()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEDOCREFSHTML
	if (com == "amoduledocrefshtml") {
		if (debug_amoduledocrefshtml()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEDOCSTATUSHTML
	if (com == "amoduledocstatushtml") {
		if (debug_amoduledocstatushtml()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULEMAKEFILE
	if (com == "amodulemakefile") {
		if (debug_amodulemakefile()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETESTHEADER
	if (com == "amoduletestheader") {
		if (debug_amoduletestheader()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETESTNODE
	if (com == "amoduletestnode") {
		if (debug_amoduletestnode()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_APROJECTCOM
	if (com == "aprojectcom") {
		if (debug_aprojectcom()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aproject() {
	AString text;
	#ifdef DEBUG_AMODULE
	text += "d) amodule\n";
	#endif
	#ifdef DEBUG_AMODULEDEBUGHEADER
	text += "d) amoduledebugheader\n";
	#endif
	#ifdef DEBUG_AMODULEDEBUGNODE
	text += "d) amoduledebugnode\n";
	#endif
	#ifdef DEBUG_AMODULEDOCHTML
	text += "d) amoduledochtml\n";
	#endif
	#ifdef DEBUG_AMODULEDOCREFSHTML
	text += "d) amoduledocrefshtml\n";
	#endif
	#ifdef DEBUG_AMODULEDOCSTATUSHTML
	text += "d) amoduledocstatushtml\n";
	#endif
	#ifdef DEBUG_AMODULEMAKEFILE
	text += "d) amodulemakefile\n";
	#endif
	#ifdef DEBUG_AMODULETESTHEADER
	text += "d) amoduletestheader\n";
	#endif
	#ifdef DEBUG_AMODULETESTNODE
	text += "d) amoduletestnode\n";
	#endif
	#ifdef DEBUG_APROJECTCOM
	text += "d) aprojectcom\n";
	#endif
	return text;
}

#endif
