#include "AProject.exception.hpp"

AProjectException::AProjectException() : AllianceException() {
	exception = "Alliance-Exception-AProject";
}

AProjectException::AProjectException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AProject");
}

AProjectException::AProjectException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AProject");
}

AProjectException::AProjectException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AProject");
}

AProjectException::AProjectException(const AProjectException & obj) : AllianceException(obj) {
	
}

AProjectException::~AProjectException() {

}

