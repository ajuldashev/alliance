#ifndef APROJECTEXCEPTION
#define APROJECTEXCEPTION
#include <alliance.exception.hpp>

class AProjectException : public AllianceException {
public:
	AProjectException();
	AProjectException(std::string text);
	AProjectException(AllianceException & e);
	AProjectException(AllianceException & e, std::string text);
	AProjectException(const AProjectException & obj);
	~AProjectException();
};

#endif

