#ifndef HPP_APROJECT
#define HPP_APROJECT
#include <alliance.config.hpp>
#include "AModule/AModule.hpp"
#include "AModuleDebugHeader/AModuleDebugHeader.hpp"
#include "AModuleDebugNode/AModuleDebugNode.hpp"
#include "AModuleMakeFile/AModuleMakeFile.hpp"
#include "AModuleTestHeader/AModuleTestHeader.hpp"
#include "AModuleTestNode/AModuleTestNode.hpp"
#include "AModuleDocHtml/AModuleDocHtml.hpp"
#include "AModuleDocStatusHtml/AModuleDocStatusHtml.hpp"
#include "AModuleDocRefsHtml/AModuleDocRefsHtml.hpp"
#include "AProjectCom/AProjectCom.hpp"
#include "AProject.exception.hpp"

// <doc-version> 3.0

class AProject {
private:
    AString path;
    ATree<AModule> modules;
public:
    AProject();
    void load();
    AString to_text(AString tab = "");
    AString to_text_debug_header();
    AString to_text_make_file();
    AString to_text_test_header();
    AString to_text_doc_status_html();
    void start_make_test_node();
    void start_make_debug_node();
    void start_make_doc_html();
    void start_make_doc_refs_html();
    void start();
    void com();
    ~AProject();
};

#endif