#include "AProject.hpp"

#ifdef TEST_APROJECT
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aproject(bool is_soft) {
	#ifdef TEST_AMODULE
	if (!test_amodule(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEDEBUGHEADER
	if (!test_amoduledebugheader(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEDEBUGHEADER );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEDEBUGNODE
	if (!test_amoduledebugnode(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEDEBUGNODE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEDOCHTML
	if (!test_amoduledochtml(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEDOCHTML );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEDOCREFSHTML
	if (!test_amoduledocrefshtml(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEDOCREFSHTML );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEDOCSTATUSHTML
	if (!test_amoduledocstatushtml(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEDOCSTATUSHTML );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULEMAKEFILE
	if (!test_amodulemakefile(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULEMAKEFILE );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETESTHEADER
	if (!test_amoduletestheader(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETESTHEADER );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETESTNODE
	if (!test_amoduletestnode(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETESTNODE );
		return is_soft;
	}
	#endif
	#ifdef TEST_APROJECTCOM
	if (!test_aprojectcom(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APROJECTCOM );
		return is_soft;
	}
	#endif
	return true;
}
#endif
