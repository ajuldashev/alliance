#include "AModuleTemplateCpp.hpp"
#include "AModuleTemplateCpp.test.cpp"
#include "AModuleTemplateCpp.test.node.cpp"
#include "AModuleTemplateCpp.debug.cpp"
#include "AModuleTemplateCpp.debug.node.cpp"
#include "AModuleTemplateCpp.exception.cpp"

AString AModuleTemplateCpp::to_template(AString module) {
    AString text;
    text += "#include \"" + module + ".hpp\"\n";
    text += "#include \"" + module + ".test.cpp\"\n";
    text += "#include \"" + module + ".test.node.cpp\"\n";
    text += "#include \"" + module + ".debug.cpp\"\n";
    text += "#include \"" + module + ".debug.node.cpp\"\n";
    text += "#include \"" + module + ".exception.cpp\"\n";
    return text;
}