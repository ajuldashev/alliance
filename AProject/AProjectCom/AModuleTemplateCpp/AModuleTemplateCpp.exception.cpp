#include "AModuleTemplateCpp.exception.hpp"

AModuleTemplateCppException::AModuleTemplateCppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateCpp";
}

AModuleTemplateCppException::AModuleTemplateCppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateCpp");
}

AModuleTemplateCppException::AModuleTemplateCppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateCpp");
}

AModuleTemplateCppException::AModuleTemplateCppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateCpp");
}

AModuleTemplateCppException::AModuleTemplateCppException(const AModuleTemplateCppException & obj) : AllianceException(obj) {

}

AModuleTemplateCppException::~AModuleTemplateCppException() {

}
