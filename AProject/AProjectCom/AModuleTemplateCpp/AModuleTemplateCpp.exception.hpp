#ifndef HPP_AMODULETEMPLATECPPEXCEPTION
#define HPP_AMODULETEMPLATECPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateCppException : public AllianceException {
public:
	AModuleTemplateCppException();
	AModuleTemplateCppException(std::string text);
	AModuleTemplateCppException(AllianceException & e);
	AModuleTemplateCppException(AllianceException & e, std::string text);
	AModuleTemplateCppException(const AModuleTemplateCppException & obj);
	~AModuleTemplateCppException();
};

#endif
