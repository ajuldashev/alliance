#ifndef HPP_AMODULETEMPLATECPP
#define HPP_AMODULETEMPLATECPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateCpp.exception.hpp"

class AModuleTemplateCpp {
public:
    static AString to_template(AString module);
};

#endif
