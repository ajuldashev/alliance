#include "AModuleTemplateDebugCpp.hpp"
#include "AModuleTemplateDebugCpp.test.cpp"
#include "AModuleTemplateDebugCpp.test.node.cpp"
#include "AModuleTemplateDebugCpp.debug.cpp"
#include "AModuleTemplateDebugCpp.debug.node.cpp"
#include "AModuleTemplateDebugCpp.exception.cpp"

AString AModuleTemplateDebugCpp::to_template(AString module) {
    AString text;
    text += "#include \"" + module + ".hpp\"\n";
    text += "\n";
    text += "#ifdef DEBUG_" + module.to_upper() + "\n";
    text += "#include <ACore/ABase/AIO/AIO.hpp>\n";
    text += "bool debug_" + module.to_lower() + "() {\n";
    text += "\tAString com;\n";
    text += "\tAIO::write_div_line();\n";
    text += "\tAIO::writeln(\"Begin debug the module \" MODULE_DEBUG_" + module.to_upper() + " );\n";
    text += "\tcom = AIO::get_command();\n";
    text += "\twhile (com != \"stop\") {\n";
    text += "\t\tif  (com == \"help\") {\n";
    text += "\t\t\tAIO::write(debug_list_node_" + module.to_lower() + "());\n";
    text += "\t\t\tAIO::writeln(\"1) help\");\n";
    text += "\t\t\tAIO::writeln(\"0) stop\");\n";
    text += "\t\t\tAIO::writeln(\"e) exit\");\n";
    text += "\t\t}\n";
    text += "\t\tif (com == \"exit\") {\n";
    text += "\t\t\treturn true;\n";
    text += "\t\t}\n";
    text += "\t\tif (debug_node_" + module.to_lower() + "(com)) {\n";
    text += "\t\t\treturn true;\n";
    text += "\t\t}\n";
    text += "\t\tAIO::write_div_line();\n";
    text += "\t\tcom = AIO::get_command();\n";
    text += "\t}\n";
    text += "\tAIO::writeln(\"End debug the module \" MODULE_DEBUG_" + module.to_upper() + " );\n";
    text += "\tAIO::write_div_line();\n";
    text += "\treturn false;\n";
    text += "}\n";
    text += "#endif\n";
    return text;
}