#include "AModuleTemplateDebugCpp.exception.hpp"

AModuleTemplateDebugCppException::AModuleTemplateDebugCppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateDebugCpp";
}

AModuleTemplateDebugCppException::AModuleTemplateDebugCppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateDebugCpp");
}

AModuleTemplateDebugCppException::AModuleTemplateDebugCppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateDebugCpp");
}

AModuleTemplateDebugCppException::AModuleTemplateDebugCppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateDebugCpp");
}

AModuleTemplateDebugCppException::AModuleTemplateDebugCppException(const AModuleTemplateDebugCppException & obj) : AllianceException(obj) {

}

AModuleTemplateDebugCppException::~AModuleTemplateDebugCppException() {

}
