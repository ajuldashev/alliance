#ifndef HPP_AMODULETEMPLATEDEBUGCPPEXCEPTION
#define HPP_AMODULETEMPLATEDEBUGCPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateDebugCppException : public AllianceException {
public:
	AModuleTemplateDebugCppException();
	AModuleTemplateDebugCppException(std::string text);
	AModuleTemplateDebugCppException(AllianceException & e);
	AModuleTemplateDebugCppException(AllianceException & e, std::string text);
	AModuleTemplateDebugCppException(const AModuleTemplateDebugCppException & obj);
	~AModuleTemplateDebugCppException();
};

#endif
