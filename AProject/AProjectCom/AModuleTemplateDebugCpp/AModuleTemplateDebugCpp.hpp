#ifndef HPP_AMODULETEMPLATEDEBUGCPP
#define HPP_AMODULETEMPLATEDEBUGCPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateDebugCpp.exception.hpp"

class AModuleTemplateDebugCpp {
public:
    static AString to_template(AString module);
};

#endif
