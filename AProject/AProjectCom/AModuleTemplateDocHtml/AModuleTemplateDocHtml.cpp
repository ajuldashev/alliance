#include "AModuleTemplateDocHtml.hpp"
#include "AModuleTemplateDocHtml.test.cpp"
#include "AModuleTemplateDocHtml.test.node.cpp"
#include "AModuleTemplateDocHtml.debug.cpp"
#include "AModuleTemplateDocHtml.debug.node.cpp"
#include "AModuleTemplateDocHtml.exception.cpp"

AString AModuleTemplateDocHtml::to_template(AString module) {
    AString text;
    text += "<!DOCTYPE html>\n";
    text += "<html>\n";
    text += "<head>\n";
    text += "\t<meta charset=\"utf-8\">\n";
    text += "\t<title>Документация по проекту Alliance</title>\n";
    text += "\t<link rel=\"stylesheet\" href=\"./style.css\">\n";
    text += "</head>\n";
    text += "<body id=\"body-status-id\" class=\"status-module-in-progress\">\n";
    text += "\t<div class=\"main-block\">\n";
    text += "\t\t<div class=\"head-block\">\n";
    text += "\t\t\t<h1>" + module + "</h1>\n";
    text += "\t\t</div>\n";
    text += "\t\t<div class=\"hrefs-block\" id=\"hrefs-block-id\">\n";
    text += "\t\t\t<ul>\n";
    text += "\t\t\t\t<li>\n";
    text += "\t\t\t\t\t<a href=\"./alliance.doc.html\"> На главную </a>\n";
    text += "\t\t\t\t</li>\n";
    text += "\t\t\t\t<li>\n";
    text += "\t\t\t\t\t<a onclick=\"javascript:history.back(-2); return false;\">Назад</a>\n";
    text += "\t\t\t\t</li>\n";
    text += "\t\t\t</ul>\n";
    text += "\t\t</div>\n";
    text += "\t\t<div class=\"body-block\">\n";
    text += "\t\t</div>\n";
    text += "\t</div>\n";
    text += "</body>\n";
    text += "</html>\n";
    return text;
}