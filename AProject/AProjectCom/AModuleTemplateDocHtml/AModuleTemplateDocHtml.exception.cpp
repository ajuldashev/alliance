#include "AModuleTemplateDocHtml.exception.hpp"

AModuleTemplateDocHtmlException::AModuleTemplateDocHtmlException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateDocHtml";
}

AModuleTemplateDocHtmlException::AModuleTemplateDocHtmlException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateDocHtml");
}

AModuleTemplateDocHtmlException::AModuleTemplateDocHtmlException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateDocHtml");
}

AModuleTemplateDocHtmlException::AModuleTemplateDocHtmlException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateDocHtml");
}

AModuleTemplateDocHtmlException::AModuleTemplateDocHtmlException(const AModuleTemplateDocHtmlException & obj) : AllianceException(obj) {

}

AModuleTemplateDocHtmlException::~AModuleTemplateDocHtmlException() {

}
