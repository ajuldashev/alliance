#ifndef HPP_AMODULETEMPLATEDOCHTMLEXCEPTION
#define HPP_AMODULETEMPLATEDOCHTMLEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateDocHtmlException : public AllianceException {
public:
	AModuleTemplateDocHtmlException();
	AModuleTemplateDocHtmlException(std::string text);
	AModuleTemplateDocHtmlException(AllianceException & e);
	AModuleTemplateDocHtmlException(AllianceException & e, std::string text);
	AModuleTemplateDocHtmlException(const AModuleTemplateDocHtmlException & obj);
	~AModuleTemplateDocHtmlException();
};

#endif
