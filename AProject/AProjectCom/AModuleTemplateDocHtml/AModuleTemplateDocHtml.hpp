#ifndef HPP_AMODULETEMPLATEDOCHTML
#define HPP_AMODULETEMPLATEDOCHTML
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateDocHtml.exception.hpp"

class AModuleTemplateDocHtml {
public:
    static AString to_template(AString module);
};

#endif
