#include "AModuleTemplateExceptionCpp.hpp"
#include "AModuleTemplateExceptionCpp.test.cpp"
#include "AModuleTemplateExceptionCpp.test.node.cpp"
#include "AModuleTemplateExceptionCpp.debug.cpp"
#include "AModuleTemplateExceptionCpp.debug.node.cpp"
#include "AModuleTemplateExceptionCpp.exception.cpp"

AString AModuleTemplateExceptionCpp::to_template(AString module) {
    AString text;
    text += "#include \"" + module + ".exception.hpp\"\n";
    text += "\n";
    text += "" + module + "Exception::" + module + "Exception() : AllianceException() {\n";
    text += "\texception = \"Alliance-Exception-" + module + "\";\n";
    text += "}\n";
    text += "\n";
    text += "" + module + "Exception::" + module + "Exception(std::string text) : AllianceException(text) {\n";
    text += "\tadd_text(\"Alliance-Exception-" + module + "\");\n";
    text += "}\n";
    text += "\n";
    text += "" + module + "Exception::" + module + "Exception(AllianceException & e) : AllianceException(e) {\n";
    text += "\tadd_text(\"Alliance-Exception-" + module + "\");\n";
    text += "}\n";
    text += "\n";
    text += "" + module + "Exception::" + module + "Exception(AllianceException & e, std::string text) : AllianceException(e) {\n";
    text += "\tadd_text(\" -- \" + text);\n";
    text += "\tadd_text(\"Alliance-Exception-" + module + "\");\n";
    text += "}\n";
    text += "\n";
    text += "" + module + "Exception::" + module + "Exception(const " + module + "Exception & obj) : AllianceException(obj) {\n";
    text += "\n";      
    text += "}\n";
    text += "\n";
    text += "" + module + "Exception::~" + module + "Exception() {\n";
    text += "\n";
    text += "}\n";
    return text;
}