#include "AModuleTemplateExceptionCpp.exception.hpp"

AModuleTemplateExceptionCppException::AModuleTemplateExceptionCppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateExceptionCpp";
}

AModuleTemplateExceptionCppException::AModuleTemplateExceptionCppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateExceptionCpp");
}

AModuleTemplateExceptionCppException::AModuleTemplateExceptionCppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateExceptionCpp");
}

AModuleTemplateExceptionCppException::AModuleTemplateExceptionCppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateExceptionCpp");
}

AModuleTemplateExceptionCppException::AModuleTemplateExceptionCppException(const AModuleTemplateExceptionCppException & obj) : AllianceException(obj) {

}

AModuleTemplateExceptionCppException::~AModuleTemplateExceptionCppException() {

}
