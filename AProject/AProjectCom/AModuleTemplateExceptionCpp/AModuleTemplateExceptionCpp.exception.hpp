#ifndef HPP_AMODULETEMPLATEEXCEPTIONCPPEXCEPTION
#define HPP_AMODULETEMPLATEEXCEPTIONCPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateExceptionCppException : public AllianceException {
public:
	AModuleTemplateExceptionCppException();
	AModuleTemplateExceptionCppException(std::string text);
	AModuleTemplateExceptionCppException(AllianceException & e);
	AModuleTemplateExceptionCppException(AllianceException & e, std::string text);
	AModuleTemplateExceptionCppException(const AModuleTemplateExceptionCppException & obj);
	~AModuleTemplateExceptionCppException();
};

#endif
