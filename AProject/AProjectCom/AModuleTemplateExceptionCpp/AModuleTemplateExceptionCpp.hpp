#ifndef HPP_AMODULETEMPLATEEXCEPTIONCPP
#define HPP_AMODULETEMPLATEEXCEPTIONCPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateExceptionCpp.exception.hpp"

class AModuleTemplateExceptionCpp {
public:
    static AString to_template(AString module);
};

#endif
