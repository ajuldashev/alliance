#include "AModuleTemplateExceptionHpp.hpp"
#include "AModuleTemplateExceptionHpp.test.cpp"
#include "AModuleTemplateExceptionHpp.test.node.cpp"
#include "AModuleTemplateExceptionHpp.debug.cpp"
#include "AModuleTemplateExceptionHpp.debug.node.cpp"
#include "AModuleTemplateExceptionHpp.exception.cpp"

AString AModuleTemplateExceptionHpp::to_template(AString module) {
    AString text;
    text += "#ifndef HPP_" + module.to_upper() + "EXCEPTION\n";
    text += "#define HPP_" + module.to_upper() + "EXCEPTION\n";
    text += "#include <alliance.exception.hpp>\n";
    text += "\n";
    text += "class " + module + "Exception : public AllianceException {\n";
    text += "public:\n";
    text += "\t" + module + "Exception();\n";
    text += "\t" + module + "Exception(std::string text);\n";
    text += "\t" + module + "Exception(AllianceException & e);\n";
    text += "\t" + module + "Exception(AllianceException & e, std::string text);\n";
    text += "\t" + module + "Exception(const " + module + "Exception & obj);\n";
    text += "\t~" + module + "Exception();\n";
    text += "};\n";
    text += "\n";
    text += "#endif\n";
    return text;
}