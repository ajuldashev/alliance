#include "AModuleTemplateExceptionHpp.exception.hpp"

AModuleTemplateExceptionHppException::AModuleTemplateExceptionHppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateExceptionHpp";
}

AModuleTemplateExceptionHppException::AModuleTemplateExceptionHppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateExceptionHpp");
}

AModuleTemplateExceptionHppException::AModuleTemplateExceptionHppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateExceptionHpp");
}

AModuleTemplateExceptionHppException::AModuleTemplateExceptionHppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateExceptionHpp");
}

AModuleTemplateExceptionHppException::AModuleTemplateExceptionHppException(const AModuleTemplateExceptionHppException & obj) : AllianceException(obj) {

}

AModuleTemplateExceptionHppException::~AModuleTemplateExceptionHppException() {

}
