#ifndef HPP_AMODULETEMPLATEEXCEPTIONHPPEXCEPTION
#define HPP_AMODULETEMPLATEEXCEPTIONHPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateExceptionHppException : public AllianceException {
public:
	AModuleTemplateExceptionHppException();
	AModuleTemplateExceptionHppException(std::string text);
	AModuleTemplateExceptionHppException(AllianceException & e);
	AModuleTemplateExceptionHppException(AllianceException & e, std::string text);
	AModuleTemplateExceptionHppException(const AModuleTemplateExceptionHppException & obj);
	~AModuleTemplateExceptionHppException();
};

#endif
