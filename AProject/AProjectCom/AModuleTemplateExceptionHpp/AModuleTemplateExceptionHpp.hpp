#ifndef HPP_AMODULETEMPLATEEXCEPTIONHPP
#define HPP_AMODULETEMPLATEEXCEPTIONHPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateExceptionHpp.exception.hpp"

class AModuleTemplateExceptionHpp {
public:
    static AString to_template(AString module);
};

#endif
