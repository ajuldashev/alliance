#include "AModuleTemplateHpp.hpp"
#include "AModuleTemplateHpp.test.cpp"
#include "AModuleTemplateHpp.test.node.cpp"
#include "AModuleTemplateHpp.debug.cpp"
#include "AModuleTemplateHpp.debug.node.cpp"
#include "AModuleTemplateHpp.exception.cpp"

AString AModuleTemplateHpp::to_template(AString module) {
    AString text;
    text += "#ifndef HPP_" + module.to_upper() + "\n";
    text += "#define HPP_" + module.to_upper() + "\n";
    text += "#include <alliance.config.hpp>\n";
    text += "#include \"" + module + ".exception.hpp\"\n";
    text += "\n";
    text += "class " + module +" {\n";
    text += "\n";
    text += "};\n";
    text += "\n";
    text += "#endif\n";
    return text;
}