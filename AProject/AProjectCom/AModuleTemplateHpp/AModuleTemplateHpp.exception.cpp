#include "AModuleTemplateHpp.exception.hpp"

AModuleTemplateHppException::AModuleTemplateHppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateHpp";
}

AModuleTemplateHppException::AModuleTemplateHppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateHpp");
}

AModuleTemplateHppException::AModuleTemplateHppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateHpp");
}

AModuleTemplateHppException::AModuleTemplateHppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateHpp");
}

AModuleTemplateHppException::AModuleTemplateHppException(const AModuleTemplateHppException & obj) : AllianceException(obj) {

}

AModuleTemplateHppException::~AModuleTemplateHppException() {

}
