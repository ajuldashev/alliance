#ifndef HPP_AMODULETEMPLATEHPPEXCEPTION
#define HPP_AMODULETEMPLATEHPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateHppException : public AllianceException {
public:
	AModuleTemplateHppException();
	AModuleTemplateHppException(std::string text);
	AModuleTemplateHppException(AllianceException & e);
	AModuleTemplateHppException(AllianceException & e, std::string text);
	AModuleTemplateHppException(const AModuleTemplateHppException & obj);
	~AModuleTemplateHppException();
};

#endif
