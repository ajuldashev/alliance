#ifndef HPP_AMODULETEMPLATEHPP
#define HPP_AMODULETEMPLATEHPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateHpp.exception.hpp"

class AModuleTemplateHpp {
public:
    static AString to_template(AString module);
};

#endif
