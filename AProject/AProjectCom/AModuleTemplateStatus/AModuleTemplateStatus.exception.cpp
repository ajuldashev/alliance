#include "AModuleTemplateStatus.exception.hpp"

AModuleTemplateStatusException::AModuleTemplateStatusException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateStatus";
}

AModuleTemplateStatusException::AModuleTemplateStatusException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateStatus");
}

AModuleTemplateStatusException::AModuleTemplateStatusException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateStatus");
}

AModuleTemplateStatusException::AModuleTemplateStatusException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateStatus");
}

AModuleTemplateStatusException::AModuleTemplateStatusException(const AModuleTemplateStatusException & obj) : AllianceException(obj) {

}

AModuleTemplateStatusException::~AModuleTemplateStatusException() {

}
