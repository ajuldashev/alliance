#ifndef HPP_AMODULETEMPLATESTATUSEXCEPTION
#define HPP_AMODULETEMPLATESTATUSEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateStatusException : public AllianceException {
public:
	AModuleTemplateStatusException();
	AModuleTemplateStatusException(std::string text);
	AModuleTemplateStatusException(AllianceException & e);
	AModuleTemplateStatusException(AllianceException & e, std::string text);
	AModuleTemplateStatusException(const AModuleTemplateStatusException & obj);
	~AModuleTemplateStatusException();
};

#endif
