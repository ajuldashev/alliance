#ifndef HPP_AMODULETEMPLATESTATUS
#define HPP_AMODULETEMPLATESTATUS
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateStatus.exception.hpp"

class AModuleTemplateStatus {
public:
    static AString to_template(AString module);
};

#endif
