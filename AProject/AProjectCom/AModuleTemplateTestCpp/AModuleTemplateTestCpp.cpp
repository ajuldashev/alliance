#include "AModuleTemplateTestCpp.hpp"
#include "AModuleTemplateTestCpp.test.cpp"
#include "AModuleTemplateTestCpp.test.node.cpp"
#include "AModuleTemplateTestCpp.debug.cpp"
#include "AModuleTemplateTestCpp.debug.node.cpp"
#include "AModuleTemplateTestCpp.exception.cpp"

AString AModuleTemplateTestCpp::to_template(AString module) {
    AString text;
    text += "#include \"" + module + ".hpp\"\n";
    text += "\n";
    text += "#ifdef TEST_" + module.to_upper() + "\n";
    text += "#include <ACore/ABase/AIO/AIO.hpp>\n";
    text += "\n";
    text += "/** <doc-test-static-function>\n";
    text += " *\n"; 
    text += " */\n";
    text += "TEST_FUNCTION_BEGIN(1)\n";
    text += "\t{\n";
    text += "\t\t/** <doc-test-info>\n";
    text += "\t\t *\n";
    text += "\t\t */\n";
    text += "\t\tTEST_ALERT(true, 1)\n";
    text += "\t}\n";
    text += "TEST_FUNCTION_END\n";
    text += "\n";
    text += "\n";
    text += "TEST_FUNCTION_MAIN_BEGIN(" + module.to_lower() + ")\n";
    text += "\n";
    text += "\tTEST_CALL(1)\n";
    text += "\n";
    text += "TEST_FUNCTION_MAIN_END(" + module.to_upper() + ")\n";
    text += "\n";
    text += "#endif\n";
    return text;
}