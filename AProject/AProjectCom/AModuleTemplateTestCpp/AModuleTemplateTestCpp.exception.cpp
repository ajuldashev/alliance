#include "AModuleTemplateTestCpp.exception.hpp"

AModuleTemplateTestCppException::AModuleTemplateTestCppException() : AllianceException() {
	exception = "Alliance-Exception-AModuleTemplateTestCpp";
}

AModuleTemplateTestCppException::AModuleTemplateTestCppException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AModuleTemplateTestCpp");
}

AModuleTemplateTestCppException::AModuleTemplateTestCppException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AModuleTemplateTestCpp");
}

AModuleTemplateTestCppException::AModuleTemplateTestCppException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AModuleTemplateTestCpp");
}

AModuleTemplateTestCppException::AModuleTemplateTestCppException(const AModuleTemplateTestCppException & obj) : AllianceException(obj) {

}

AModuleTemplateTestCppException::~AModuleTemplateTestCppException() {

}
