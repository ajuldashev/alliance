#ifndef HPP_AMODULETEMPLATETESTCPPEXCEPTION
#define HPP_AMODULETEMPLATETESTCPPEXCEPTION
#include <alliance.exception.hpp>

class AModuleTemplateTestCppException : public AllianceException {
public:
	AModuleTemplateTestCppException();
	AModuleTemplateTestCppException(std::string text);
	AModuleTemplateTestCppException(AllianceException & e);
	AModuleTemplateTestCppException(AllianceException & e, std::string text);
	AModuleTemplateTestCppException(const AModuleTemplateTestCppException & obj);
	~AModuleTemplateTestCppException();
};

#endif
