#ifndef HPP_AMODULETEMPLATETESTCPP
#define HPP_AMODULETEMPLATETESTCPP
#include <ACore/ABase/ATypes/AString/AString.hpp>
#include "AModuleTemplateTestCpp.exception.hpp"

class AModuleTemplateTestCpp {
public:
    static AString to_template(AString module);
};

#endif
