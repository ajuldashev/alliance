#include "AProjectCom.hpp"
#include "AProjectCom.test.cpp"
#include "AProjectCom.test.node.cpp"
#include "AProjectCom.debug.cpp"
#include "AProjectCom.debug.node.cpp"
#include "AProjectCom.exception.cpp"

void AProjectCom::change_dir_to(AString path) {
    if (path.length() == 0) {
        modules.goto_root();
        return;
    }
    if (path == ".") {
        return;
    }
    if (path == "..") {
        if (modules.has_parent()) {
            modules.goto_parent();
        } else {
            AIO::writeln_error("This is root dir");
        }
        return;
    }
    s64int index = find_index(path);
    if (index == -1) {
        AIO::write_warning("Error change dir to ");
        AIO::writeln_error(path);
    } else {
        modules.goto_child(index);
    }
    return;
}

AString AProjectCom::get_first_path(AString path) {
    s64int found = path.find("/");
    if (found == -1) {
        return path;
    }
    if (found == 0) {
        return AString();
    }
    return path.get(0, found);
}

AString AProjectCom::remove_first_path(AString path) {
    s64int found = path.find("/");
    if (found == -1) {
        return AString();
    }
    if (found == 0 && path.length() == 1) {
        return AString();
    }
    if (found == 0) {
        return path.get(1, path.length() - 1); 
    }
    if (found == path.length() - 1) {
        return AString();
    }
    return path.get(found + 1, path.length() - (found + 1));
}

AProjectCom::AProjectCom() {

}

AProjectCom::AProjectCom(ATreeNav<AModule> modules) {
    this->modules = modules;
}

void AProjectCom::start() {
    AString command;
    command = AIO::get_command();
    while (command != "stop") {
        if  (command == "help") {
			AIO::writeln("1) help");
            AIO::writeln("2) ls");
            AIO::writeln("3) cd");
            AIO::writeln("4) mk");
			AIO::writeln("0) stop");
        } else
        if (command == "ls") {
            ls();
        } else
        if (command == "cd") {
            cd(AIO::get_word());
        } else
        if (command == "mk") {
            mk(AIO::get_word());
        }
        command = AIO::get_command();
    }
}

void AProjectCom::ls() {
    AIO::write_div_line();
    AIO::writeln("./");
    if (modules.has_parent()) {
        AIO::writeln("../");
    }
    for (u64int i = 0; i < modules.length(); i += 1) {
        AIO::writeln(modules.at(i).get_info().get_name());
    }
    AIO::write_div_line();
}

void AProjectCom::cd(AString path) {
    while (path.length() > 0) {
        AString dir = get_first_path(path); 
        change_dir_to(dir);
        path = remove_first_path(path);
    }
}

void AProjectCom::mk(AString module) {
    try {
        if (find_index(module) != -1) {
            AIO::write_error("Error make module ");
            AIO::writeln_error(module);
            return;
        }
        AString dir;
        if (modules.has_parent()) {
            dir = modules.at().get_info().get_path() + "/" + modules.at().get_info().get_name() + "/" + module;
        } else {
            dir = "./" + module;
        }
        if (!ADir::make(dir)) {
            AIO::write_error("Error make dir ");
            AIO::writeln_error(dir);
            return;
        }

        AOFile file;

        AString file_hpp = dir + "/" + module + ".hpp";
        file.open(file_hpp);
        file.write(AModuleTemplateHpp::to_template(module));

        AString file_cpp = dir + "/" + module + ".cpp";
        file.open(file_cpp);
        file.write(AModuleTemplateCpp::to_template(module));
        
        AString file_test_cpp = dir + "/" + module + ".test.cpp";
        file.open(file_test_cpp);
        file.write(AModuleTemplateTestCpp::to_template(module));

        AString file_test_node_cpp = dir + "/" + module + ".test.node.cpp";
        file.open(file_test_node_cpp);
        file.write("\n");
        
        AString file_debug_cpp = dir + "/" + module + ".debug.cpp";
        file.open(file_debug_cpp);
        file.write(AModuleTemplateDebugCpp::to_template(module));

        AString file_debug_node_cpp = dir + "/" + module + ".debug.node.cpp";
        file.open(file_debug_node_cpp);
        file.write("\n");

        AString file_exception_hpp = dir + "/" + module + ".exception.hpp";
        file.open(file_exception_hpp);
        file.write(AModuleTemplateExceptionHpp::to_template(module));
        
        AString file_exception_cpp = dir + "/" + module + ".exception.cpp";
        file.open(file_exception_cpp);
        file.write(AModuleTemplateExceptionCpp::to_template(module));
        
        AString file_doc_html = dir + "/" + module + ".doc.html";
        file.open(file_doc_html);
        file.write(AModuleTemplateDocHtml::to_template(module));
        
        AString file_status = dir + "/" + module + ".status";
        file.open(file_status);
        file.write(AModuleTemplateStatus::to_template(module));

    } catch (AllianceException & e) {
        throw AProjectComException(e, "mk(AString module)");
    }
}

s64int AProjectCom::find_index(AString module) {
    s64int index = -1;

    for (u64int i = 0; i < modules.length(); i += 1) {
        if (modules.at(i).get_info().get_name() == module) {
            index = i;
            break;
        }
    }

    return index;
}

AProjectCom::~AProjectCom() {

}