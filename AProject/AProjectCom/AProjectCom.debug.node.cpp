#include "AProjectCom.hpp"

#ifdef DEBUG_APROJECTCOM
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_aprojectcom(AString com) {
	#ifdef DEBUG_AMODULETEMPLATECPP
	if (com == "amoduletemplatecpp") {
		if (debug_amoduletemplatecpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATEDEBUGCPP
	if (com == "amoduletemplatedebugcpp") {
		if (debug_amoduletemplatedebugcpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATEDOCHTML
	if (com == "amoduletemplatedochtml") {
		if (debug_amoduletemplatedochtml()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATEEXCEPTIONCPP
	if (com == "amoduletemplateexceptioncpp") {
		if (debug_amoduletemplateexceptioncpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATEEXCEPTIONHPP
	if (com == "amoduletemplateexceptionhpp") {
		if (debug_amoduletemplateexceptionhpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATEHPP
	if (com == "amoduletemplatehpp") {
		if (debug_amoduletemplatehpp()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATESTATUS
	if (com == "amoduletemplatestatus") {
		if (debug_amoduletemplatestatus()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_AMODULETEMPLATETESTCPP
	if (com == "amoduletemplatetestcpp") {
		if (debug_amoduletemplatetestcpp()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_aprojectcom() {
	AString text;
	#ifdef DEBUG_AMODULETEMPLATECPP
	text += "d) amoduletemplatecpp\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATEDEBUGCPP
	text += "d) amoduletemplatedebugcpp\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATEDOCHTML
	text += "d) amoduletemplatedochtml\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATEEXCEPTIONCPP
	text += "d) amoduletemplateexceptioncpp\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATEEXCEPTIONHPP
	text += "d) amoduletemplateexceptionhpp\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATEHPP
	text += "d) amoduletemplatehpp\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATESTATUS
	text += "d) amoduletemplatestatus\n";
	#endif
	#ifdef DEBUG_AMODULETEMPLATETESTCPP
	text += "d) amoduletemplatetestcpp\n";
	#endif
	return text;
}

#endif
