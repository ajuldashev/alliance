#include "AProjectCom.exception.hpp"

AProjectComException::AProjectComException() : AllianceException() {
	exception = "Alliance-Exception-AProjectCom";
}

AProjectComException::AProjectComException(std::string text) : AllianceException(text) {
	add_text("Alliance-Exception-AProjectCom");
}

AProjectComException::AProjectComException(AllianceException & e) : AllianceException(e) {
	add_text("Alliance-Exception-AProjectCom");
}

AProjectComException::AProjectComException(AllianceException & e, std::string text) : AllianceException(e) {
	add_text(" -- " + text);
	add_text("Alliance-Exception-AProjectCom");
}

AProjectComException::AProjectComException(const AProjectComException & obj) : AllianceException(obj) {
	
}

AProjectComException::~AProjectComException() {

}

