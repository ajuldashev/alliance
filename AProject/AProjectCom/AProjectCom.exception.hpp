#ifndef HPP_APROJECTCOMEXCEPTION
#define HPP_APROJECTCOMEXCEPTION
#include <alliance.exception.hpp>

class AProjectComException : public AllianceException {
public:
	AProjectComException();
	AProjectComException(std::string text);
	AProjectComException(AllianceException & e);
	AProjectComException(AllianceException & e, std::string text);
	AProjectComException(const AProjectComException & obj);
	~AProjectComException();
};

#endif

