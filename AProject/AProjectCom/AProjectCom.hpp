#ifndef HPP_APROJECTCOM
#define HPP_APROJECTCOM
#include <ACore/ABase/AIO/AIO.hpp>
#include <ACore/ABase/AIO/AFile/AOFile/AOFile.hpp>

#include <AProject/AModule/AModule.hpp>

#include <AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.hpp>
#include <AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.hpp>
#include <AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.hpp>

#include "AProjectCom.exception.hpp"

class AProjectCom {
private:
    ATreeNav<AModule> modules;
    void change_dir_to(AString path);
    AString get_first_path(AString path);
    AString remove_first_path(AString path);
public:
    AProjectCom();
    AProjectCom(ATreeNav<AModule> modules);

    void start();
    void ls();
    void cd(AString path);
    void mk(AString module);

    s64int find_index(AString module);

    ~AProjectCom();
};

#endif