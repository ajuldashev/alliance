#include "AProjectCom.hpp"

#ifdef TEST_APROJECTCOM
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_aprojectcom(bool is_soft) {
	#ifdef TEST_AMODULETEMPLATECPP
	if (!test_amoduletemplatecpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATECPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATEDEBUGCPP
	if (!test_amoduletemplatedebugcpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATEDEBUGCPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATEDOCHTML
	if (!test_amoduletemplatedochtml(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATEDOCHTML );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATEEXCEPTIONCPP
	if (!test_amoduletemplateexceptioncpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATEEXCEPTIONCPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATEEXCEPTIONHPP
	if (!test_amoduletemplateexceptionhpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATEEXCEPTIONHPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATEHPP
	if (!test_amoduletemplatehpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATEHPP );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATESTATUS
	if (!test_amoduletemplatestatus(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATESTATUS );
		return is_soft;
	}
	#endif
	#ifdef TEST_AMODULETEMPLATETESTCPP
	if (!test_amoduletemplatetestcpp(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_AMODULETEMPLATETESTCPP );
		return is_soft;
	}
	#endif
	return true;
}
#endif
