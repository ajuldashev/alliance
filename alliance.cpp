#include "alliance.hpp"
#include "alliance.test.cpp"
#include "alliance.test.node.cpp"
#include "alliance.debug.cpp"
#include "alliance.debug.node.cpp"
#include "alliance.exception.cpp"

int main(int argc, char ** argv){
	try {
		AConsoleIBuffer::init(argc, argv);
		AIO::write_div_line();
		AIO::writeln("|-{> Starting the Alliance-System <}-|");
		#ifdef DEBUG_ALLIANCE
		debug_alliance();
		#endif
	} catch(AllianceException & e) {
		AIO::writeln_error(e.get_text().c_str());
	}
	return 0;
}
