#include "alliance.hpp"
#ifdef DEBUG_ALLIANCE

#include <ACore/ABase/AIO/AIO.hpp>
bool debug_alliance() {
   AString com;
   AIO::write_div_line();
   AIO::writeln("Begin debug");
   com = AIO::get_command();
   while (com != "stop") {
		if  (com == "help") {
			AIO::write(debug_list_node_alliance());
			AIO::writeln("1) help");
			AIO::writeln("0) stop");
			AIO::writeln("e) exit");	
			AIO::writeln("t) test");
			AIO::writeln("t) test_soft");	
		}
		if (com == "exit") {
			return true;
		}
		if (debug_node_alliance(com)) {
			return true;
		}
		if (com == "test") {
			test_alliance();
		}
		if (com == "test_soft") {
			test_alliance(true);
		}
		AIO::write_div_line();
		com = AIO::get_command();
   }
   AIO::writeln("End debug");
   AIO::write_div_line();
   return false;
}
#endif

