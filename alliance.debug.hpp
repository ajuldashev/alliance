#ifndef DEBUG_FILE
#define DEBUG_FILE
#define DEBUG_ALLIANCE
#ifdef  DEBUG_ALLIANCE

#include <iostream>
#define df(str) std::cout << (str) << std::endl;

class AString;

bool debug_alliance();
bool debug_node_alliance(AString com);
AString debug_list_node_alliance();
	#define DEBUG_ACORE
	#ifdef  DEBUG_ACORE
		bool debug_acore();
		bool debug_node_acore(AString com);
		AString debug_list_node_acore();
		#define MODULE_DEBUG_ACORE "ACore"
		#define DEBUG_ABASE
		#ifdef  DEBUG_ABASE
			bool debug_abase();
			bool debug_node_abase(AString com);
			AString debug_list_node_abase();
			#define MODULE_DEBUG_ABASE "ACore-ABase"
			#define DEBUG_AENCRYPTION
			#ifdef  DEBUG_AENCRYPTION
				bool debug_aencryption();
				bool debug_node_aencryption(AString com);
				AString debug_list_node_aencryption();
				#define MODULE_DEBUG_AENCRYPTION "ACore-ABase-AEncryption"
				#define DEBUG_ACRC16
				#ifdef  DEBUG_ACRC16
					bool debug_acrc16();
					bool debug_node_acrc16(AString com);
					AString debug_list_node_acrc16();
					#define MODULE_DEBUG_ACRC16 "ACore-ABase-AEncryption-ACrc16"
				#endif
				#define DEBUG_ACRC32
				#ifdef  DEBUG_ACRC32
					bool debug_acrc32();
					bool debug_node_acrc32(AString com);
					AString debug_list_node_acrc32();
					#define MODULE_DEBUG_ACRC32 "ACore-ABase-AEncryption-ACrc32"
				#endif
				#define DEBUG_ACRC64
				#ifdef  DEBUG_ACRC64
					bool debug_acrc64();
					bool debug_node_acrc64(AString com);
					AString debug_list_node_acrc64();
					#define MODULE_DEBUG_ACRC64 "ACore-ABase-AEncryption-ACrc64"
				#endif
				#define DEBUG_ASHA1
				#ifdef  DEBUG_ASHA1
					bool debug_asha1();
					bool debug_node_asha1(AString com);
					AString debug_list_node_asha1();
					#define MODULE_DEBUG_ASHA1 "ACore-ABase-AEncryption-ASha1"
				#endif
				#define DEBUG_ASHA256
				#ifdef  DEBUG_ASHA256
					bool debug_asha256();
					bool debug_node_asha256(AString com);
					AString debug_list_node_asha256();
					#define MODULE_DEBUG_ASHA256 "ACore-ABase-AEncryption-ASha256"
				#endif
				#define DEBUG_ASHA3
				#ifdef  DEBUG_ASHA3
					bool debug_asha3();
					bool debug_node_asha3(AString com);
					AString debug_list_node_asha3();
					#define MODULE_DEBUG_ASHA3 "ACore-ABase-AEncryption-ASha3"
				#endif
				#define DEBUG_ASHA512
				#ifdef  DEBUG_ASHA512
					bool debug_asha512();
					bool debug_node_asha512(AString com);
					AString debug_list_node_asha512();
					#define MODULE_DEBUG_ASHA512 "ACore-ABase-AEncryption-ASha512"
				#endif
			#endif
			#define DEBUG_AIO
			#ifdef  DEBUG_AIO
				bool debug_aio();
				bool debug_node_aio(AString com);
				AString debug_list_node_aio();
				#define MODULE_DEBUG_AIO "ACore-ABase-AIO"
				#define DEBUG_ACONSOLE
				#ifdef  DEBUG_ACONSOLE
					bool debug_aconsole();
					bool debug_node_aconsole(AString com);
					AString debug_list_node_aconsole();
					#define MODULE_DEBUG_ACONSOLE "ACore-ABase-AIO-AConsole"
					#define DEBUG_ACONSOLEIBUFFER
					#ifdef  DEBUG_ACONSOLEIBUFFER
						bool debug_aconsoleibuffer();
						bool debug_node_aconsoleibuffer(AString com);
						AString debug_list_node_aconsoleibuffer();
						#define MODULE_DEBUG_ACONSOLEIBUFFER "ACore-ABase-AIO-AConsole-AConsoleIBuffer"
					#endif
				#endif
				#define DEBUG_ADIR
				#ifdef  DEBUG_ADIR
					bool debug_adir();
					bool debug_node_adir(AString com);
					AString debug_list_node_adir();
					#define MODULE_DEBUG_ADIR "ACore-ABase-AIO-ADir"
				#endif
				#define DEBUG_AFILE
				#ifdef  DEBUG_AFILE
					bool debug_afile();
					bool debug_node_afile(AString com);
					AString debug_list_node_afile();
					#define MODULE_DEBUG_AFILE "ACore-ABase-AIO-AFile"
					#define DEBUG_AIFILE
					#ifdef  DEBUG_AIFILE
						bool debug_aifile();
						bool debug_node_aifile(AString com);
						AString debug_list_node_aifile();
						#define MODULE_DEBUG_AIFILE "ACore-ABase-AIO-AFile-AIFile"
					#endif
					#define DEBUG_AOFILE
					#ifdef  DEBUG_AOFILE
						bool debug_aofile();
						bool debug_node_aofile(AString com);
						AString debug_list_node_aofile();
						#define MODULE_DEBUG_AOFILE "ACore-ABase-AIO-AFile-AOFile"
					#endif
				#endif
				#define DEBUG_AFILEINFO
				#ifdef  DEBUG_AFILEINFO
					bool debug_afileinfo();
					bool debug_node_afileinfo(AString com);
					AString debug_list_node_afileinfo();
					#define MODULE_DEBUG_AFILEINFO "ACore-ABase-AIO-AFileInfo"
				#endif
			#endif
			#define DEBUG_ALIBS
			#ifdef  DEBUG_ALIBS
				bool debug_alibs();
				bool debug_node_alibs(AString com);
				AString debug_list_node_alibs();
				#define MODULE_DEBUG_ALIBS "ACore-ABase-ALibs"
				#define DEBUG_ABITSLIBS
				#ifdef  DEBUG_ABITSLIBS
					bool debug_abitslibs();
					bool debug_node_abitslibs(AString com);
					AString debug_list_node_abitslibs();
					#define MODULE_DEBUG_ABITSLIBS "ACore-ABase-ALibs-ABitsLibs"
					#define DEBUG_A16BITSITERATOR
					#ifdef  DEBUG_A16BITSITERATOR
						bool debug_a16bitsiterator();
						bool debug_node_a16bitsiterator(AString com);
						AString debug_list_node_a16bitsiterator();
						#define MODULE_DEBUG_A16BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A16BitsIterator"
					#endif
					#define DEBUG_A32BITSITERATOR
					#ifdef  DEBUG_A32BITSITERATOR
						bool debug_a32bitsiterator();
						bool debug_node_a32bitsiterator(AString com);
						AString debug_list_node_a32bitsiterator();
						#define MODULE_DEBUG_A32BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A32BitsIterator"
					#endif
					#define DEBUG_A64BITSITERATOR
					#ifdef  DEBUG_A64BITSITERATOR
						bool debug_a64bitsiterator();
						bool debug_node_a64bitsiterator(AString com);
						AString debug_list_node_a64bitsiterator();
						#define MODULE_DEBUG_A64BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A64BitsIterator"
					#endif
					#define DEBUG_A8BITSITERATOR
					#ifdef  DEBUG_A8BITSITERATOR
						bool debug_a8bitsiterator();
						bool debug_node_a8bitsiterator(AString com);
						AString debug_list_node_a8bitsiterator();
						#define MODULE_DEBUG_A8BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A8BitsIterator"
					#endif
					#define DEBUG_ABITSCONVERTOR
					#ifdef  DEBUG_ABITSCONVERTOR
						bool debug_abitsconvertor();
						bool debug_node_abitsconvertor(AString com);
						AString debug_list_node_abitsconvertor();
						#define MODULE_DEBUG_ABITSCONVERTOR "ACore-ABase-ALibs-ABitsLibs-ABitsConvertor"
					#endif
				#endif
				#define DEBUG_AREGEX
				#ifdef  DEBUG_AREGEX
					bool debug_aregex();
					bool debug_node_aregex(AString com);
					AString debug_list_node_aregex();
					#define MODULE_DEBUG_AREGEX "ACore-ABase-ALibs-ARegex"
					#define DEBUG_AREGEXCONSTRUCTOR
					#ifdef  DEBUG_AREGEXCONSTRUCTOR
						bool debug_aregexconstructor();
						bool debug_node_aregexconstructor(AString com);
						AString debug_list_node_aregexconstructor();
						#define MODULE_DEBUG_AREGEXCONSTRUCTOR "ACore-ABase-ALibs-ARegex-ARegexConstructor"
					#endif
					#define DEBUG_AREGEXPATTERN
					#ifdef  DEBUG_AREGEXPATTERN
						bool debug_aregexpattern();
						bool debug_node_aregexpattern(AString com);
						AString debug_list_node_aregexpattern();
						#define MODULE_DEBUG_AREGEXPATTERN "ACore-ABase-ALibs-ARegex-ARegexPattern"
						#define DEBUG_AREGEXGROUP
						#ifdef  DEBUG_AREGEXGROUP
							bool debug_aregexgroup();
							bool debug_node_aregexgroup(AString com);
							AString debug_list_node_aregexgroup();
							#define MODULE_DEBUG_AREGEXGROUP "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup"
							#define DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE
							#ifdef  DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE
								bool debug_aregexgroupadvancedchecknegative();
								bool debug_node_aregexgroupadvancedchecknegative(AString com);
								AString debug_list_node_aregexgroupadvancedchecknegative();
								#define MODULE_DEBUG_AREGEXGROUPADVANCEDCHECKNEGATIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupAdvancedCheckNegative"
							#endif
							#define DEBUG_AREGEXGROUPADVANCEDCHECKPOSITIVE
							#ifdef  DEBUG_AREGEXGROUPADVANCEDCHECKPOSITIVE
								bool debug_aregexgroupadvancedcheckpositive();
								bool debug_node_aregexgroupadvancedcheckpositive(AString com);
								AString debug_list_node_aregexgroupadvancedcheckpositive();
								#define MODULE_DEBUG_AREGEXGROUPADVANCEDCHECKPOSITIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupAdvancedCheckPositive"
							#endif
							#define DEBUG_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
							#ifdef  DEBUG_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
								bool debug_aregexgroupretrospectivechecknegative();
								bool debug_node_aregexgroupretrospectivechecknegative(AString com);
								AString debug_list_node_aregexgroupretrospectivechecknegative();
								#define MODULE_DEBUG_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupRetrospectiveCheckNegative"
							#endif
							#define DEBUG_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
							#ifdef  DEBUG_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
								bool debug_aregexgroupretrospectivecheckpositive();
								bool debug_node_aregexgroupretrospectivecheckpositive(AString com);
								AString debug_list_node_aregexgroupretrospectivecheckpositive();
								#define MODULE_DEBUG_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupRetrospectiveCheckPositive"
							#endif
							#define DEBUG_AREGEXGROUPTREE
							#ifdef  DEBUG_AREGEXGROUPTREE
								bool debug_aregexgrouptree();
								bool debug_node_aregexgrouptree(AString com);
								AString debug_list_node_aregexgrouptree();
								#define MODULE_DEBUG_AREGEXGROUPTREE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupTree"
							#endif
						#endif
						#define DEBUG_AREGEXNODE
						#ifdef  DEBUG_AREGEXNODE
							bool debug_aregexnode();
							bool debug_node_aregexnode(AString com);
							AString debug_list_node_aregexnode();
							#define MODULE_DEBUG_AREGEXNODE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode"
							#define DEBUG_AREGEXNODEANYNUMBER
							#ifdef  DEBUG_AREGEXNODEANYNUMBER
								bool debug_aregexnodeanynumber();
								bool debug_node_aregexnodeanynumber(AString com);
								AString debug_list_node_aregexnodeanynumber();
								#define MODULE_DEBUG_AREGEXNODEANYNUMBER "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeAnyNumber"
							#endif
							#define DEBUG_AREGEXNODEATLEASTONE
							#ifdef  DEBUG_AREGEXNODEATLEASTONE
								bool debug_aregexnodeatleastone();
								bool debug_node_aregexnodeatleastone(AString com);
								AString debug_list_node_aregexnodeatleastone();
								#define MODULE_DEBUG_AREGEXNODEATLEASTONE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeAtLeastOne"
							#endif
							#define DEBUG_AREGEXNODEBEGINLINE
							#ifdef  DEBUG_AREGEXNODEBEGINLINE
								bool debug_aregexnodebeginline();
								bool debug_node_aregexnodebeginline(AString com);
								AString debug_list_node_aregexnodebeginline();
								#define MODULE_DEBUG_AREGEXNODEBEGINLINE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeBeginLine"
							#endif
							#define DEBUG_AREGEXNODEENDLINE
							#ifdef  DEBUG_AREGEXNODEENDLINE
								bool debug_aregexnodeendline();
								bool debug_node_aregexnodeendline(AString com);
								AString debug_list_node_aregexnodeendline();
								#define MODULE_DEBUG_AREGEXNODEENDLINE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeEndLine"
							#endif
							#define DEBUG_AREGEXNODEMAYBEPRESENT
							#ifdef  DEBUG_AREGEXNODEMAYBEPRESENT
								bool debug_aregexnodemaybepresent();
								bool debug_node_aregexnodemaybepresent(AString com);
								AString debug_list_node_aregexnodemaybepresent();
								#define MODULE_DEBUG_AREGEXNODEMAYBEPRESENT "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeMayBePresent"
							#endif
						#endif
						#define DEBUG_AREGEXPARSER
						#ifdef  DEBUG_AREGEXPARSER
							bool debug_aregexparser();
							bool debug_node_aregexparser(AString com);
							AString debug_list_node_aregexparser();
							#define MODULE_DEBUG_AREGEXPARSER "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexParser"
							#define DEBUG_AREGEXPARSERTOKEN
							#ifdef  DEBUG_AREGEXPARSERTOKEN
								bool debug_aregexparsertoken();
								bool debug_node_aregexparsertoken(AString com);
								AString debug_list_node_aregexparsertoken();
								#define MODULE_DEBUG_AREGEXPARSERTOKEN "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexParser-ARegexParserToken"
							#endif
						#endif
						#define DEBUG_AREGEXPART
						#ifdef  DEBUG_AREGEXPART
							bool debug_aregexpart();
							bool debug_node_aregexpart(AString com);
							AString debug_list_node_aregexpart();
							#define MODULE_DEBUG_AREGEXPART "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexPart"
						#endif
						#define DEBUG_AREGEXREFS
						#ifdef  DEBUG_AREGEXREFS
							bool debug_aregexrefs();
							bool debug_node_aregexrefs(AString com);
							AString debug_list_node_aregexrefs();
							#define MODULE_DEBUG_AREGEXREFS "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexRefs"
						#endif
						#define DEBUG_AREGEXSYMBOL
						#ifdef  DEBUG_AREGEXSYMBOL
							bool debug_aregexsymbol();
							bool debug_node_aregexsymbol(AString com);
							AString debug_list_node_aregexsymbol();
							#define MODULE_DEBUG_AREGEXSYMBOL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol"
							#define DEBUG_AREGEXSYMBOLANY
							#ifdef  DEBUG_AREGEXSYMBOLANY
								bool debug_aregexsymbolany();
								bool debug_node_aregexsymbolany(AString com);
								AString debug_list_node_aregexsymbolany();
								#define MODULE_DEBUG_AREGEXSYMBOLANY "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolAny"
							#endif
							#define DEBUG_AREGEXSYMBOLCLASS
							#ifdef  DEBUG_AREGEXSYMBOLCLASS
								bool debug_aregexsymbolclass();
								bool debug_node_aregexsymbolclass(AString com);
								AString debug_list_node_aregexsymbolclass();
								#define MODULE_DEBUG_AREGEXSYMBOLCLASS "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass"
								#define DEBUG_AREGEXSYMBOLDIGITAL
								#ifdef  DEBUG_AREGEXSYMBOLDIGITAL
									bool debug_aregexsymboldigital();
									bool debug_node_aregexsymboldigital(AString com);
									AString debug_list_node_aregexsymboldigital();
									#define MODULE_DEBUG_AREGEXSYMBOLDIGITAL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolDigital"
								#endif
								#define DEBUG_AREGEXSYMBOLNOTDIGITAL
								#ifdef  DEBUG_AREGEXSYMBOLNOTDIGITAL
									bool debug_aregexsymbolnotdigital();
									bool debug_node_aregexsymbolnotdigital(AString com);
									AString debug_list_node_aregexsymbolnotdigital();
									#define MODULE_DEBUG_AREGEXSYMBOLNOTDIGITAL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotDigital"
								#endif
								#define DEBUG_AREGEXSYMBOLNOTSPACE
								#ifdef  DEBUG_AREGEXSYMBOLNOTSPACE
									bool debug_aregexsymbolnotspace();
									bool debug_node_aregexsymbolnotspace(AString com);
									AString debug_list_node_aregexsymbolnotspace();
									#define MODULE_DEBUG_AREGEXSYMBOLNOTSPACE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotSpace"
								#endif
								#define DEBUG_AREGEXSYMBOLNOTWORD
								#ifdef  DEBUG_AREGEXSYMBOLNOTWORD
									bool debug_aregexsymbolnotword();
									bool debug_node_aregexsymbolnotword(AString com);
									AString debug_list_node_aregexsymbolnotword();
									#define MODULE_DEBUG_AREGEXSYMBOLNOTWORD "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotWord"
								#endif
								#define DEBUG_AREGEXSYMBOLSPACE
								#ifdef  DEBUG_AREGEXSYMBOLSPACE
									bool debug_aregexsymbolspace();
									bool debug_node_aregexsymbolspace(AString com);
									AString debug_list_node_aregexsymbolspace();
									#define MODULE_DEBUG_AREGEXSYMBOLSPACE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolSpace"
								#endif
								#define DEBUG_AREGEXSYMBOLWORD
								#ifdef  DEBUG_AREGEXSYMBOLWORD
									bool debug_aregexsymbolword();
									bool debug_node_aregexsymbolword(AString com);
									AString debug_list_node_aregexsymbolword();
									#define MODULE_DEBUG_AREGEXSYMBOLWORD "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolWord"
								#endif
							#endif
						#endif
					#endif
				#endif
				#define DEBUG_ASTRINGLIBS
				#ifdef  DEBUG_ASTRINGLIBS
					bool debug_astringlibs();
					bool debug_node_astringlibs(AString com);
					AString debug_list_node_astringlibs();
					#define MODULE_DEBUG_ASTRINGLIBS "ACore-ABase-ALibs-AStringLibs"
					#define DEBUG_ASTRINGFORMAT
					#ifdef  DEBUG_ASTRINGFORMAT
						bool debug_astringformat();
						bool debug_node_astringformat(AString com);
						AString debug_list_node_astringformat();
						#define MODULE_DEBUG_ASTRINGFORMAT "ACore-ABase-ALibs-AStringLibs-AStringFormat"
					#endif
					#define DEBUG_ASTRINGINDEXMAP
					#ifdef  DEBUG_ASTRINGINDEXMAP
						bool debug_astringindexmap();
						bool debug_node_astringindexmap(AString com);
						AString debug_list_node_astringindexmap();
						#define MODULE_DEBUG_ASTRINGINDEXMAP "ACore-ABase-ALibs-AStringLibs-AStringIndexMap"
					#endif
					#define DEBUG_ASTRINGITERATOR
					#ifdef  DEBUG_ASTRINGITERATOR
						bool debug_astringiterator();
						bool debug_node_astringiterator(AString com);
						AString debug_list_node_astringiterator();
						#define MODULE_DEBUG_ASTRINGITERATOR "ACore-ABase-ALibs-AStringLibs-AStringIterator"
					#endif
					#define DEBUG_ASTRINGLIST
					#ifdef  DEBUG_ASTRINGLIST
						bool debug_astringlist();
						bool debug_node_astringlist(AString com);
						AString debug_list_node_astringlist();
						#define MODULE_DEBUG_ASTRINGLIST "ACore-ABase-ALibs-AStringLibs-AStringList"
					#endif
					#define DEBUG_ASTRINGMAP
					#ifdef  DEBUG_ASTRINGMAP
						bool debug_astringmap();
						bool debug_node_astringmap(AString com);
						AString debug_list_node_astringmap();
						#define MODULE_DEBUG_ASTRINGMAP "ACore-ABase-ALibs-AStringLibs-AStringMap"
					#endif
				#endif
			#endif
			#define DEBUG_AMATH
			#ifdef  DEBUG_AMATH
				bool debug_amath();
				bool debug_node_amath(AString com);
				AString debug_list_node_amath();
				#define MODULE_DEBUG_AMATH "ACore-ABase-AMath"
			#endif
			#define DEBUG_ASTRUCT
			#ifdef  DEBUG_ASTRUCT
				bool debug_astruct();
				bool debug_node_astruct(AString com);
				AString debug_list_node_astruct();
				#define MODULE_DEBUG_ASTRUCT "ACore-ABase-AStruct"
				#define DEBUG_AARRAY
				#ifdef  DEBUG_AARRAY
					bool debug_aarray();
					bool debug_node_aarray(AString com);
					AString debug_list_node_aarray();
					#define MODULE_DEBUG_AARRAY "ACore-ABase-AStruct-AArray"
				#endif
				#define DEBUG_ALIST
				#ifdef  DEBUG_ALIST
					bool debug_alist();
					bool debug_node_alist(AString com);
					AString debug_list_node_alist();
					#define MODULE_DEBUG_ALIST "ACore-ABase-AStruct-AList"
					#define DEBUG_ALISTITERATOR
					#ifdef  DEBUG_ALISTITERATOR
						bool debug_alistiterator();
						bool debug_node_alistiterator(AString com);
						AString debug_list_node_alistiterator();
						#define MODULE_DEBUG_ALISTITERATOR "ACore-ABase-AStruct-AList-AListIterator"
					#endif
				#endif
				#define DEBUG_AMAP
				#ifdef  DEBUG_AMAP
					bool debug_amap();
					bool debug_node_amap(AString com);
					AString debug_list_node_amap();
					#define MODULE_DEBUG_AMAP "ACore-ABase-AStruct-AMap"
				#endif
				#define DEBUG_ASTACK
				#ifdef  DEBUG_ASTACK
					bool debug_astack();
					bool debug_node_astack(AString com);
					AString debug_list_node_astack();
					#define MODULE_DEBUG_ASTACK "ACore-ABase-AStruct-AStack"
				#endif
				#define DEBUG_ASTRUCTITERATOR
				#ifdef  DEBUG_ASTRUCTITERATOR
					bool debug_astructiterator();
					bool debug_node_astructiterator(AString com);
					AString debug_list_node_astructiterator();
					#define MODULE_DEBUG_ASTRUCTITERATOR "ACore-ABase-AStruct-AStructIterator"
				#endif
				#define DEBUG_ATREE
				#ifdef  DEBUG_ATREE
					bool debug_atree();
					bool debug_node_atree(AString com);
					AString debug_list_node_atree();
					#define MODULE_DEBUG_ATREE "ACore-ABase-AStruct-ATree"
				#endif
				#define DEBUG_ATREEBINARY
				#ifdef  DEBUG_ATREEBINARY
					bool debug_atreebinary();
					bool debug_node_atreebinary(AString com);
					AString debug_list_node_atreebinary();
					#define MODULE_DEBUG_ATREEBINARY "ACore-ABase-AStruct-ATreeBinary"
				#endif
				#define DEBUG_ATREEMAP
				#ifdef  DEBUG_ATREEMAP
					bool debug_atreemap();
					bool debug_node_atreemap(AString com);
					AString debug_list_node_atreemap();
					#define MODULE_DEBUG_ATREEMAP "ACore-ABase-AStruct-ATreeMap"
				#endif
				#define DEBUG_ATREENAV
				#ifdef  DEBUG_ATREENAV
					bool debug_atreenav();
					bool debug_node_atreenav(AString com);
					AString debug_list_node_atreenav();
					#define MODULE_DEBUG_ATREENAV "ACore-ABase-AStruct-ATreeNav"
				#endif
				#define DEBUG_ATREESEARCH
				#ifdef  DEBUG_ATREESEARCH
					bool debug_atreesearch();
					bool debug_node_atreesearch(AString com);
					AString debug_list_node_atreesearch();
					#define MODULE_DEBUG_ATREESEARCH "ACore-ABase-AStruct-ATreeSearch"
				#endif
				#define DEBUG_AVECTOR
				#ifdef  DEBUG_AVECTOR
					bool debug_avector();
					bool debug_node_avector(AString com);
					AString debug_list_node_avector();
					#define MODULE_DEBUG_AVECTOR "ACore-ABase-AStruct-AVector"
				#endif
			#endif
			#define DEBUG_ATYPES
			#ifdef  DEBUG_ATYPES
				bool debug_atypes();
				bool debug_node_atypes(AString com);
				AString debug_list_node_atypes();
				#define MODULE_DEBUG_ATYPES "ACore-ABase-ATypes"
				#define DEBUG_ABITS
				#ifdef  DEBUG_ABITS
					bool debug_abits();
					bool debug_node_abits(AString com);
					AString debug_list_node_abits();
					#define MODULE_DEBUG_ABITS "ACore-ABase-ATypes-ABits"
					#define DEBUG_A16BITS
					#ifdef  DEBUG_A16BITS
						bool debug_a16bits();
						bool debug_node_a16bits(AString com);
						AString debug_list_node_a16bits();
						#define MODULE_DEBUG_A16BITS "ACore-ABase-ATypes-ABits-A16Bits"
						#define DEBUG_A16BIT
						#ifdef  DEBUG_A16BIT
							bool debug_a16bit();
							bool debug_node_a16bit(AString com);
							AString debug_list_node_a16bit();
							#define MODULE_DEBUG_A16BIT "ACore-ABase-ATypes-ABits-A16Bits-A16Bit"
						#endif
						#define DEBUG_A16BITSWEAK
						#ifdef  DEBUG_A16BITSWEAK
							bool debug_a16bitsweak();
							bool debug_node_a16bitsweak(AString com);
							AString debug_list_node_a16bitsweak();
							#define MODULE_DEBUG_A16BITSWEAK "ACore-ABase-ATypes-ABits-A16Bits-A16BitsWeak"
						#endif
					#endif
					#define DEBUG_A32BITS
					#ifdef  DEBUG_A32BITS
						bool debug_a32bits();
						bool debug_node_a32bits(AString com);
						AString debug_list_node_a32bits();
						#define MODULE_DEBUG_A32BITS "ACore-ABase-ATypes-ABits-A32Bits"
						#define DEBUG_A32BIT
						#ifdef  DEBUG_A32BIT
							bool debug_a32bit();
							bool debug_node_a32bit(AString com);
							AString debug_list_node_a32bit();
							#define MODULE_DEBUG_A32BIT "ACore-ABase-ATypes-ABits-A32Bits-A32Bit"
						#endif
						#define DEBUG_A32BITSWEAK
						#ifdef  DEBUG_A32BITSWEAK
							bool debug_a32bitsweak();
							bool debug_node_a32bitsweak(AString com);
							AString debug_list_node_a32bitsweak();
							#define MODULE_DEBUG_A32BITSWEAK "ACore-ABase-ATypes-ABits-A32Bits-A32BitsWeak"
						#endif
					#endif
					#define DEBUG_A64BITS
					#ifdef  DEBUG_A64BITS
						bool debug_a64bits();
						bool debug_node_a64bits(AString com);
						AString debug_list_node_a64bits();
						#define MODULE_DEBUG_A64BITS "ACore-ABase-ATypes-ABits-A64Bits"
						#define DEBUG_A64BIT
						#ifdef  DEBUG_A64BIT
							bool debug_a64bit();
							bool debug_node_a64bit(AString com);
							AString debug_list_node_a64bit();
							#define MODULE_DEBUG_A64BIT "ACore-ABase-ATypes-ABits-A64Bits-A64Bit"
						#endif
						#define DEBUG_A64BITSWEAK
						#ifdef  DEBUG_A64BITSWEAK
							bool debug_a64bitsweak();
							bool debug_node_a64bitsweak(AString com);
							AString debug_list_node_a64bitsweak();
							#define MODULE_DEBUG_A64BITSWEAK "ACore-ABase-ATypes-ABits-A64Bits-A64BitsWeak"
						#endif
					#endif
					#define DEBUG_A8BITS
					#ifdef  DEBUG_A8BITS
						bool debug_a8bits();
						bool debug_node_a8bits(AString com);
						AString debug_list_node_a8bits();
						#define MODULE_DEBUG_A8BITS "ACore-ABase-ATypes-ABits-A8Bits"
						#define DEBUG_A8BIT
						#ifdef  DEBUG_A8BIT
							bool debug_a8bit();
							bool debug_node_a8bit(AString com);
							AString debug_list_node_a8bit();
							#define MODULE_DEBUG_A8BIT "ACore-ABase-ATypes-ABits-A8Bits-A8Bit"
						#endif
						#define DEBUG_A8BITSWEAK
						#ifdef  DEBUG_A8BITSWEAK
							bool debug_a8bitsweak();
							bool debug_node_a8bitsweak(AString com);
							AString debug_list_node_a8bitsweak();
							#define MODULE_DEBUG_A8BITSWEAK "ACore-ABase-ATypes-ABits-A8Bits-A8BitsWeak"
						#endif
					#endif
				#endif
				#define DEBUG_APTR
				#ifdef  DEBUG_APTR
					bool debug_aptr();
					bool debug_node_aptr(AString com);
					AString debug_list_node_aptr();
					#define MODULE_DEBUG_APTR "ACore-ABase-ATypes-APtr"
					#define DEBUG_APTRUNIQUE
					#ifdef  DEBUG_APTRUNIQUE
						bool debug_aptrunique();
						bool debug_node_aptrunique(AString com);
						AString debug_list_node_aptrunique();
						#define MODULE_DEBUG_APTRUNIQUE "ACore-ABase-ATypes-APtr-APtrUnique"
					#endif
					#define DEBUG_APTRWEAK
					#ifdef  DEBUG_APTRWEAK
						bool debug_aptrweak();
						bool debug_node_aptrweak(AString com);
						AString debug_list_node_aptrweak();
						#define MODULE_DEBUG_APTRWEAK "ACore-ABase-ATypes-APtr-APtrWeak"
					#endif
				#endif
				#define DEBUG_APTRARRAY
				#ifdef  DEBUG_APTRARRAY
					bool debug_aptrarray();
					bool debug_node_aptrarray(AString com);
					AString debug_list_node_aptrarray();
					#define MODULE_DEBUG_APTRARRAY "ACore-ABase-ATypes-APtrArray"
					#define DEBUG_APTRARRAYWEAK
					#ifdef  DEBUG_APTRARRAYWEAK
						bool debug_aptrarrayweak();
						bool debug_node_aptrarrayweak(AString com);
						AString debug_list_node_aptrarrayweak();
						#define MODULE_DEBUG_APTRARRAYWEAK "ACore-ABase-ATypes-APtrArray-APtrArrayWeak"
					#endif
				#endif
				#define DEBUG_ASTRING
				#ifdef  DEBUG_ASTRING
					bool debug_astring();
					bool debug_node_astring(AString com);
					AString debug_list_node_astring();
					#define MODULE_DEBUG_ASTRING "ACore-ABase-ATypes-AString"
					#define DEBUG_ASTRINGBASE
					#ifdef  DEBUG_ASTRINGBASE
						bool debug_astringbase();
						bool debug_node_astringbase(AString com);
						AString debug_list_node_astringbase();
						#define MODULE_DEBUG_ASTRINGBASE "ACore-ABase-ATypes-AString-AStringBase"
					#endif
				#endif
				#define DEBUG_ASYMBOL
				#ifdef  DEBUG_ASYMBOL
					bool debug_asymbol();
					bool debug_node_asymbol(AString com);
					AString debug_list_node_asymbol();
					#define MODULE_DEBUG_ASYMBOL "ACore-ABase-ATypes-ASymbol"
				#endif
			#endif
		#endif
		#define DEBUG_ATOOLS
		#ifdef  DEBUG_ATOOLS
			bool debug_atools();
			bool debug_node_atools(AString com);
			AString debug_list_node_atools();
			#define MODULE_DEBUG_ATOOLS "ACore-ATools"
			#define DEBUG_ACPPPARSER
			#ifdef  DEBUG_ACPPPARSER
				bool debug_acppparser();
				bool debug_node_acppparser(AString com);
				AString debug_list_node_acppparser();
				#define MODULE_DEBUG_ACPPPARSER "ACore-ATools-ACppParser"
				#define DEBUG_ACPPIMAGE
				#ifdef  DEBUG_ACPPIMAGE
					bool debug_acppimage();
					bool debug_node_acppimage(AString com);
					AString debug_list_node_acppimage();
					#define MODULE_DEBUG_ACPPIMAGE "ACore-ATools-ACppParser-ACppImage"
					#define DEBUG_ACPPITEM
					#ifdef  DEBUG_ACPPITEM
						bool debug_acppitem();
						bool debug_node_acppitem(AString com);
						AString debug_list_node_acppitem();
						#define MODULE_DEBUG_ACPPITEM "ACore-ATools-ACppParser-ACppImage-ACppItem"
					#endif
				#endif
				#define DEBUG_ACPPTOKEN
				#ifdef  DEBUG_ACPPTOKEN
					bool debug_acpptoken();
					bool debug_node_acpptoken(AString com);
					AString debug_list_node_acpptoken();
					#define MODULE_DEBUG_ACPPTOKEN "ACore-ATools-ACppParser-ACppToken"
					#define DEBUG_ACPPTOKENALLLINE
					#ifdef  DEBUG_ACPPTOKENALLLINE
						bool debug_acpptokenallline();
						bool debug_node_acpptokenallline(AString com);
						AString debug_list_node_acpptokenallline();
						#define MODULE_DEBUG_ACPPTOKENALLLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenAllLine"
					#endif
					#define DEBUG_ACPPTOKENCHAR
					#ifdef  DEBUG_ACPPTOKENCHAR
						bool debug_acpptokenchar();
						bool debug_node_acpptokenchar(AString com);
						AString debug_list_node_acpptokenchar();
						#define MODULE_DEBUG_ACPPTOKENCHAR "ACore-ATools-ACppParser-ACppToken-ACppTokenChar"
					#endif
					#define DEBUG_ACPPTOKENCOMMENTBEGIN
					#ifdef  DEBUG_ACPPTOKENCOMMENTBEGIN
						bool debug_acpptokencommentbegin();
						bool debug_node_acpptokencommentbegin(AString com);
						AString debug_list_node_acpptokencommentbegin();
						#define MODULE_DEBUG_ACPPTOKENCOMMENTBEGIN "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentBegin"
					#endif
					#define DEBUG_ACPPTOKENCOMMENTEND
					#ifdef  DEBUG_ACPPTOKENCOMMENTEND
						bool debug_acpptokencommentend();
						bool debug_node_acpptokencommentend(AString com);
						AString debug_list_node_acpptokencommentend();
						#define MODULE_DEBUG_ACPPTOKENCOMMENTEND "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentEnd"
					#endif
					#define DEBUG_ACPPTOKENCOMMENTINLINE
					#ifdef  DEBUG_ACPPTOKENCOMMENTINLINE
						bool debug_acpptokencommentinline();
						bool debug_node_acpptokencommentinline(AString com);
						AString debug_list_node_acpptokencommentinline();
						#define MODULE_DEBUG_ACPPTOKENCOMMENTINLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentInline"
					#endif
					#define DEBUG_ACPPTOKENCOMMENTLINE
					#ifdef  DEBUG_ACPPTOKENCOMMENTLINE
						bool debug_acpptokencommentline();
						bool debug_node_acpptokencommentline(AString com);
						AString debug_list_node_acpptokencommentline();
						#define MODULE_DEBUG_ACPPTOKENCOMMENTLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentLine"
					#endif
					#define DEBUG_ACPPTOKENKEYSYMBOL
					#ifdef  DEBUG_ACPPTOKENKEYSYMBOL
						bool debug_acpptokenkeysymbol();
						bool debug_node_acpptokenkeysymbol(AString com);
						AString debug_list_node_acpptokenkeysymbol();
						#define MODULE_DEBUG_ACPPTOKENKEYSYMBOL "ACore-ATools-ACppParser-ACppToken-ACppTokenKeySymbol"
					#endif
					#define DEBUG_ACPPTOKENKEYTYPE
					#ifdef  DEBUG_ACPPTOKENKEYTYPE
						bool debug_acpptokenkeytype();
						bool debug_node_acpptokenkeytype(AString com);
						AString debug_list_node_acpptokenkeytype();
						#define MODULE_DEBUG_ACPPTOKENKEYTYPE "ACore-ATools-ACppParser-ACppToken-ACppTokenKeyType"
					#endif
					#define DEBUG_ACPPTOKENKEYWORD
					#ifdef  DEBUG_ACPPTOKENKEYWORD
						bool debug_acpptokenkeyword();
						bool debug_node_acpptokenkeyword(AString com);
						AString debug_list_node_acpptokenkeyword();
						#define MODULE_DEBUG_ACPPTOKENKEYWORD "ACore-ATools-ACppParser-ACppToken-ACppTokenKeyWord"
					#endif
					#define DEBUG_ACPPTOKENNAME
					#ifdef  DEBUG_ACPPTOKENNAME
						bool debug_acpptokenname();
						bool debug_node_acpptokenname(AString com);
						AString debug_list_node_acpptokenname();
						#define MODULE_DEBUG_ACPPTOKENNAME "ACore-ATools-ACppParser-ACppToken-ACppTokenName"
					#endif
					#define DEBUG_ACPPTOKENNUMBERDOUBLE
					#ifdef  DEBUG_ACPPTOKENNUMBERDOUBLE
						bool debug_acpptokennumberdouble();
						bool debug_node_acpptokennumberdouble(AString com);
						AString debug_list_node_acpptokennumberdouble();
						#define MODULE_DEBUG_ACPPTOKENNUMBERDOUBLE "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberDouble"
					#endif
					#define DEBUG_ACPPTOKENNUMBERHEX
					#ifdef  DEBUG_ACPPTOKENNUMBERHEX
						bool debug_acpptokennumberhex();
						bool debug_node_acpptokennumberhex(AString com);
						AString debug_list_node_acpptokennumberhex();
						#define MODULE_DEBUG_ACPPTOKENNUMBERHEX "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberHex"
					#endif
					#define DEBUG_ACPPTOKENNUMBERINTEGER
					#ifdef  DEBUG_ACPPTOKENNUMBERINTEGER
						bool debug_acpptokennumberinteger();
						bool debug_node_acpptokennumberinteger(AString com);
						AString debug_list_node_acpptokennumberinteger();
						#define MODULE_DEBUG_ACPPTOKENNUMBERINTEGER "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberInteger"
					#endif
					#define DEBUG_ACPPTOKENSPACE
					#ifdef  DEBUG_ACPPTOKENSPACE
						bool debug_acpptokenspace();
						bool debug_node_acpptokenspace(AString com);
						AString debug_list_node_acpptokenspace();
						#define MODULE_DEBUG_ACPPTOKENSPACE "ACore-ATools-ACppParser-ACppToken-ACppTokenSpace"
					#endif
					#define DEBUG_ACPPTOKENSTRING
					#ifdef  DEBUG_ACPPTOKENSTRING
						bool debug_acpptokenstring();
						bool debug_node_acpptokenstring(AString com);
						AString debug_list_node_acpptokenstring();
						#define MODULE_DEBUG_ACPPTOKENSTRING "ACore-ATools-ACppParser-ACppToken-ACppTokenString"
					#endif
				#endif
			#endif
			#define DEBUG_AHTMLCONSTRUCTOR
			#ifdef  DEBUG_AHTMLCONSTRUCTOR
				bool debug_ahtmlconstructor();
				bool debug_node_ahtmlconstructor(AString com);
				AString debug_list_node_ahtmlconstructor();
				#define MODULE_DEBUG_AHTMLCONSTRUCTOR "ACore-ATools-AHtmlConstructor"
			#endif
			#define DEBUG_ALOADER
			#ifdef  DEBUG_ALOADER
				bool debug_aloader();
				bool debug_node_aloader(AString com);
				AString debug_list_node_aloader();
				#define MODULE_DEBUG_ALOADER "ACore-ATools-ALoader"
				#define DEBUG_ATEXTMANADGER
				#ifdef  DEBUG_ATEXTMANADGER
					bool debug_atextmanadger();
					bool debug_node_atextmanadger(AString com);
					AString debug_list_node_atextmanadger();
					#define MODULE_DEBUG_ATEXTMANADGER "ACore-ATools-ALoader-ATextManadger"
					#define DEBUG_ATEXTFILE
					#ifdef  DEBUG_ATEXTFILE
						bool debug_atextfile();
						bool debug_node_atextfile(AString com);
						AString debug_list_node_atextfile();
						#define MODULE_DEBUG_ATEXTFILE "ACore-ATools-ALoader-ATextManadger-ATextFile"
						#define DEBUG_ATEXT
						#ifdef  DEBUG_ATEXT
							bool debug_atext();
							bool debug_node_atext(AString com);
							AString debug_list_node_atext();
							#define MODULE_DEBUG_ATEXT "ACore-ATools-ALoader-ATextManadger-ATextFile-AText"
						#endif
						#define DEBUG_ATEXTITERATOR
						#ifdef  DEBUG_ATEXTITERATOR
							bool debug_atextiterator();
							bool debug_node_atextiterator(AString com);
							AString debug_list_node_atextiterator();
							#define MODULE_DEBUG_ATEXTITERATOR "ACore-ATools-ALoader-ATextManadger-ATextFile-ATextIterator"
						#endif
					#endif
				#endif
			#endif
		#endif
	#endif
	#define DEBUG_APROJECT
	#ifdef  DEBUG_APROJECT
		bool debug_aproject();
		bool debug_node_aproject(AString com);
		AString debug_list_node_aproject();
		#define MODULE_DEBUG_APROJECT "AProject"
		#define DEBUG_AMODULE
		#ifdef  DEBUG_AMODULE
			bool debug_amodule();
			bool debug_node_amodule(AString com);
			AString debug_list_node_amodule();
			#define MODULE_DEBUG_AMODULE "AProject-AModule"
		#endif
		#define DEBUG_AMODULEDEBUGHEADER
		#ifdef  DEBUG_AMODULEDEBUGHEADER
			bool debug_amoduledebugheader();
			bool debug_node_amoduledebugheader(AString com);
			AString debug_list_node_amoduledebugheader();
			#define MODULE_DEBUG_AMODULEDEBUGHEADER "AProject-AModuleDebugHeader"
		#endif
		#define DEBUG_AMODULEDEBUGNODE
		#ifdef  DEBUG_AMODULEDEBUGNODE
			bool debug_amoduledebugnode();
			bool debug_node_amoduledebugnode(AString com);
			AString debug_list_node_amoduledebugnode();
			#define MODULE_DEBUG_AMODULEDEBUGNODE "AProject-AModuleDebugNode"
		#endif
		#define DEBUG_AMODULEDOCHTML
		#ifdef  DEBUG_AMODULEDOCHTML
			bool debug_amoduledochtml();
			bool debug_node_amoduledochtml(AString com);
			AString debug_list_node_amoduledochtml();
			#define MODULE_DEBUG_AMODULEDOCHTML "AProject-AModuleDocHtml"
			#define DEBUG_ADOCHTMLPARSERCPP
			#ifdef  DEBUG_ADOCHTMLPARSERCPP
				bool debug_adochtmlparsercpp();
				bool debug_node_adochtmlparsercpp(AString com);
				AString debug_list_node_adochtmlparsercpp();
				#define MODULE_DEBUG_ADOCHTMLPARSERCPP "AProject-AModuleDocHtml-ADocHtmlParserCPP"
			#endif
			#define DEBUG_ADOCHTMLPARSERHPP
			#ifdef  DEBUG_ADOCHTMLPARSERHPP
				bool debug_adochtmlparserhpp();
				bool debug_node_adochtmlparserhpp(AString com);
				AString debug_list_node_adochtmlparserhpp();
				#define MODULE_DEBUG_ADOCHTMLPARSERHPP "AProject-AModuleDocHtml-ADocHtmlParserHPP"
			#endif
			#define DEBUG_ADOCHTMLPARSERTEST
			#ifdef  DEBUG_ADOCHTMLPARSERTEST
				bool debug_adochtmlparsertest();
				bool debug_node_adochtmlparsertest(AString com);
				AString debug_list_node_adochtmlparsertest();
				#define MODULE_DEBUG_ADOCHTMLPARSERTEST "AProject-AModuleDocHtml-ADocHtmlParserTEST"
			#endif
		#endif
		#define DEBUG_AMODULEDOCREFSHTML
		#ifdef  DEBUG_AMODULEDOCREFSHTML
			bool debug_amoduledocrefshtml();
			bool debug_node_amoduledocrefshtml(AString com);
			AString debug_list_node_amoduledocrefshtml();
			#define MODULE_DEBUG_AMODULEDOCREFSHTML "AProject-AModuleDocRefsHtml"
		#endif
		#define DEBUG_AMODULEDOCSTATUSHTML
		#ifdef  DEBUG_AMODULEDOCSTATUSHTML
			bool debug_amoduledocstatushtml();
			bool debug_node_amoduledocstatushtml(AString com);
			AString debug_list_node_amoduledocstatushtml();
			#define MODULE_DEBUG_AMODULEDOCSTATUSHTML "AProject-AModuleDocStatusHtml"
			#define DEBUG_AMODULESIZEINFO
			#ifdef  DEBUG_AMODULESIZEINFO
				bool debug_amodulesizeinfo();
				bool debug_node_amodulesizeinfo(AString com);
				AString debug_list_node_amodulesizeinfo();
				#define MODULE_DEBUG_AMODULESIZEINFO "AProject-AModuleDocStatusHtml-AModuleSizeInfo"
			#endif
		#endif
		#define DEBUG_AMODULEMAKEFILE
		#ifdef  DEBUG_AMODULEMAKEFILE
			bool debug_amodulemakefile();
			bool debug_node_amodulemakefile(AString com);
			AString debug_list_node_amodulemakefile();
			#define MODULE_DEBUG_AMODULEMAKEFILE "AProject-AModuleMakeFile"
		#endif
		#define DEBUG_AMODULETESTHEADER
		#ifdef  DEBUG_AMODULETESTHEADER
			bool debug_amoduletestheader();
			bool debug_node_amoduletestheader(AString com);
			AString debug_list_node_amoduletestheader();
			#define MODULE_DEBUG_AMODULETESTHEADER "AProject-AModuleTestHeader"
		#endif
		#define DEBUG_AMODULETESTNODE
		#ifdef  DEBUG_AMODULETESTNODE
			bool debug_amoduletestnode();
			bool debug_node_amoduletestnode(AString com);
			AString debug_list_node_amoduletestnode();
			#define MODULE_DEBUG_AMODULETESTNODE "AProject-AModuleTestNode"
		#endif
		#define DEBUG_APROJECTCOM
		#ifdef  DEBUG_APROJECTCOM
			bool debug_aprojectcom();
			bool debug_node_aprojectcom(AString com);
			AString debug_list_node_aprojectcom();
			#define MODULE_DEBUG_APROJECTCOM "AProject-AProjectCom"
			#define DEBUG_AMODULETEMPLATECPP
			#ifdef  DEBUG_AMODULETEMPLATECPP
				bool debug_amoduletemplatecpp();
				bool debug_node_amoduletemplatecpp(AString com);
				AString debug_list_node_amoduletemplatecpp();
				#define MODULE_DEBUG_AMODULETEMPLATECPP "AProject-AProjectCom-AModuleTemplateCpp"
			#endif
			#define DEBUG_AMODULETEMPLATEDEBUGCPP
			#ifdef  DEBUG_AMODULETEMPLATEDEBUGCPP
				bool debug_amoduletemplatedebugcpp();
				bool debug_node_amoduletemplatedebugcpp(AString com);
				AString debug_list_node_amoduletemplatedebugcpp();
				#define MODULE_DEBUG_AMODULETEMPLATEDEBUGCPP "AProject-AProjectCom-AModuleTemplateDebugCpp"
			#endif
			#define DEBUG_AMODULETEMPLATEDOCHTML
			#ifdef  DEBUG_AMODULETEMPLATEDOCHTML
				bool debug_amoduletemplatedochtml();
				bool debug_node_amoduletemplatedochtml(AString com);
				AString debug_list_node_amoduletemplatedochtml();
				#define MODULE_DEBUG_AMODULETEMPLATEDOCHTML "AProject-AProjectCom-AModuleTemplateDocHtml"
			#endif
			#define DEBUG_AMODULETEMPLATEEXCEPTIONCPP
			#ifdef  DEBUG_AMODULETEMPLATEEXCEPTIONCPP
				bool debug_amoduletemplateexceptioncpp();
				bool debug_node_amoduletemplateexceptioncpp(AString com);
				AString debug_list_node_amoduletemplateexceptioncpp();
				#define MODULE_DEBUG_AMODULETEMPLATEEXCEPTIONCPP "AProject-AProjectCom-AModuleTemplateExceptionCpp"
			#endif
			#define DEBUG_AMODULETEMPLATEEXCEPTIONHPP
			#ifdef  DEBUG_AMODULETEMPLATEEXCEPTIONHPP
				bool debug_amoduletemplateexceptionhpp();
				bool debug_node_amoduletemplateexceptionhpp(AString com);
				AString debug_list_node_amoduletemplateexceptionhpp();
				#define MODULE_DEBUG_AMODULETEMPLATEEXCEPTIONHPP "AProject-AProjectCom-AModuleTemplateExceptionHpp"
			#endif
			#define DEBUG_AMODULETEMPLATEHPP
			#ifdef  DEBUG_AMODULETEMPLATEHPP
				bool debug_amoduletemplatehpp();
				bool debug_node_amoduletemplatehpp(AString com);
				AString debug_list_node_amoduletemplatehpp();
				#define MODULE_DEBUG_AMODULETEMPLATEHPP "AProject-AProjectCom-AModuleTemplateHpp"
			#endif
			#define DEBUG_AMODULETEMPLATESTATUS
			#ifdef  DEBUG_AMODULETEMPLATESTATUS
				bool debug_amoduletemplatestatus();
				bool debug_node_amoduletemplatestatus(AString com);
				AString debug_list_node_amoduletemplatestatus();
				#define MODULE_DEBUG_AMODULETEMPLATESTATUS "AProject-AProjectCom-AModuleTemplateStatus"
			#endif
			#define DEBUG_AMODULETEMPLATETESTCPP
			#ifdef  DEBUG_AMODULETEMPLATETESTCPP
				bool debug_amoduletemplatetestcpp();
				bool debug_node_amoduletemplatetestcpp(AString com);
				AString debug_list_node_amoduletemplatetestcpp();
				#define MODULE_DEBUG_AMODULETEMPLATETESTCPP "AProject-AProjectCom-AModuleTemplateTestCpp"
			#endif
		#endif
	#endif
#endif
#endif
