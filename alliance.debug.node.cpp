#include "alliance.hpp"

#ifdef DEBUG_ALLIANCE
#include <ACore/ABase/AIO/AIO.hpp>

bool debug_node_alliance(AString com) {
	#ifdef DEBUG_ACORE
	if (com == "acore") {
		if (debug_acore()) {
			return true;
		}
	}
	#endif
	#ifdef DEBUG_APROJECT
	if (com == "aproject") {
		if (debug_aproject()) {
			return true;
		}
	}
	#endif
	return false;
}

AString debug_list_node_alliance() {
	AString text;
	#ifdef DEBUG_ACORE
	text += "d) acore\n";
	#endif
	#ifdef DEBUG_APROJECT
	text += "d) aproject\n";
	#endif
	return text;
}

#endif
