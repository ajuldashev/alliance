#include "alliance.exception.hpp"

AllianceException::AllianceException() {
    exception = "Alliance-Exception";
}

AllianceException::AllianceException(std::string text) {
    exception = "Alliance-Exception";
    add_text(" -- " + text);
}

AllianceException::AllianceException(AllianceException & obj) {
    set_text(obj.exception);
}

AllianceException::AllianceException(AllianceException & obj, std::string text) {
    add_text(" -- " + text);
	add_text("Alliance-Exception");
}

AllianceException::AllianceException(const AllianceException & obj) {
    set_text(obj.exception);
}

void AllianceException::set_text(std::string text) {
    exception = text;
}

void AllianceException::add_text(std::string text) {
    exception = text + "\n" + exception;
}

std::string AllianceException::get_text() {
    return exception;
}

AllianceException::~AllianceException() {

}