#ifndef ALLIANCEEXCEPTION
#define ALLIANCEEXCEPTION
#include <alliance.config.hpp>
#include <string>

class AllianceException {
protected:
    std::string exception;
public:
    AllianceException();
    AllianceException(std::string text);
    AllianceException(AllianceException &);
    AllianceException(AllianceException &, std::string);
    AllianceException(const AllianceException &);
    void set_text(std::string);
    void add_text(std::string);
    std::string get_text();
    ~AllianceException();
};

#endif


