#include "alliance.hpp"
#ifdef TEST_ALLIANCE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_alliance(bool is_soft) {
    AIO::write_div_line();
    AIO::writeln("Begin test");

    if (!test_node_alliance(is_soft)) {
        return is_soft;
    }

    AIO::write_div_line();
    AIO::writeln_success("Done  test");
    return true;
}
#endif

