#ifndef HPP_TEST_FILE
#define HPP_TEST_FILE
#define TEST_ALLIANCE
#ifdef  TEST_ALLIANCE
bool test_alliance(bool is_soft = false);
bool test_node_alliance(bool is_soft = false);
	#define TEST_ACORE
	#ifdef  TEST_ACORE
		bool test_acore(bool is_soft = false);
		bool test_node_acore(bool is_soft = false);
		#define MODULE_TEST_ACORE "ACore"
		#define TEST_ABASE
		#ifdef  TEST_ABASE
			bool test_abase(bool is_soft = false);
			bool test_node_abase(bool is_soft = false);
			#define MODULE_TEST_ABASE "ACore-ABase"
			#define TEST_AENCRYPTION
			#ifdef  TEST_AENCRYPTION
				bool test_aencryption(bool is_soft = false);
				bool test_node_aencryption(bool is_soft = false);
				#define MODULE_TEST_AENCRYPTION "ACore-ABase-AEncryption"
				#define TEST_ACRC16
				#ifdef  TEST_ACRC16
					bool test_acrc16(bool is_soft = false);
					bool test_node_acrc16(bool is_soft = false);
					#define MODULE_TEST_ACRC16 "ACore-ABase-AEncryption-ACrc16"
				#endif
				#define TEST_ACRC32
				#ifdef  TEST_ACRC32
					bool test_acrc32(bool is_soft = false);
					bool test_node_acrc32(bool is_soft = false);
					#define MODULE_TEST_ACRC32 "ACore-ABase-AEncryption-ACrc32"
				#endif
				#define TEST_ACRC64
				#ifdef  TEST_ACRC64
					bool test_acrc64(bool is_soft = false);
					bool test_node_acrc64(bool is_soft = false);
					#define MODULE_TEST_ACRC64 "ACore-ABase-AEncryption-ACrc64"
				#endif
				#define TEST_ASHA1
				#ifdef  TEST_ASHA1
					bool test_asha1(bool is_soft = false);
					bool test_node_asha1(bool is_soft = false);
					#define MODULE_TEST_ASHA1 "ACore-ABase-AEncryption-ASha1"
				#endif
				#define TEST_ASHA256
				#ifdef  TEST_ASHA256
					bool test_asha256(bool is_soft = false);
					bool test_node_asha256(bool is_soft = false);
					#define MODULE_TEST_ASHA256 "ACore-ABase-AEncryption-ASha256"
				#endif
				#define TEST_ASHA3
				#ifdef  TEST_ASHA3
					bool test_asha3(bool is_soft = false);
					bool test_node_asha3(bool is_soft = false);
					#define MODULE_TEST_ASHA3 "ACore-ABase-AEncryption-ASha3"
				#endif
				#define TEST_ASHA512
				#ifdef  TEST_ASHA512
					bool test_asha512(bool is_soft = false);
					bool test_node_asha512(bool is_soft = false);
					#define MODULE_TEST_ASHA512 "ACore-ABase-AEncryption-ASha512"
				#endif
			#endif
			#define TEST_AIO
			#ifdef  TEST_AIO
				bool test_aio(bool is_soft = false);
				bool test_node_aio(bool is_soft = false);
				#define MODULE_TEST_AIO "ACore-ABase-AIO"
				#define TEST_ACONSOLE
				#ifdef  TEST_ACONSOLE
					bool test_aconsole(bool is_soft = false);
					bool test_node_aconsole(bool is_soft = false);
					#define MODULE_TEST_ACONSOLE "ACore-ABase-AIO-AConsole"
					#define TEST_ACONSOLEIBUFFER
					#ifdef  TEST_ACONSOLEIBUFFER
						bool test_aconsoleibuffer(bool is_soft = false);
						bool test_node_aconsoleibuffer(bool is_soft = false);
						#define MODULE_TEST_ACONSOLEIBUFFER "ACore-ABase-AIO-AConsole-AConsoleIBuffer"
					#endif
				#endif
				#define TEST_ADIR
				#ifdef  TEST_ADIR
					bool test_adir(bool is_soft = false);
					bool test_node_adir(bool is_soft = false);
					#define MODULE_TEST_ADIR "ACore-ABase-AIO-ADir"
				#endif
				#define TEST_AFILE
				#ifdef  TEST_AFILE
					bool test_afile(bool is_soft = false);
					bool test_node_afile(bool is_soft = false);
					#define MODULE_TEST_AFILE "ACore-ABase-AIO-AFile"
					#define TEST_AIFILE
					#ifdef  TEST_AIFILE
						bool test_aifile(bool is_soft = false);
						bool test_node_aifile(bool is_soft = false);
						#define MODULE_TEST_AIFILE "ACore-ABase-AIO-AFile-AIFile"
					#endif
					#define TEST_AOFILE
					#ifdef  TEST_AOFILE
						bool test_aofile(bool is_soft = false);
						bool test_node_aofile(bool is_soft = false);
						#define MODULE_TEST_AOFILE "ACore-ABase-AIO-AFile-AOFile"
					#endif
				#endif
				#define TEST_AFILEINFO
				#ifdef  TEST_AFILEINFO
					bool test_afileinfo(bool is_soft = false);
					bool test_node_afileinfo(bool is_soft = false);
					#define MODULE_TEST_AFILEINFO "ACore-ABase-AIO-AFileInfo"
				#endif
			#endif
			#define TEST_ALIBS
			#ifdef  TEST_ALIBS
				bool test_alibs(bool is_soft = false);
				bool test_node_alibs(bool is_soft = false);
				#define MODULE_TEST_ALIBS "ACore-ABase-ALibs"
				#define TEST_ABITSLIBS
				#ifdef  TEST_ABITSLIBS
					bool test_abitslibs(bool is_soft = false);
					bool test_node_abitslibs(bool is_soft = false);
					#define MODULE_TEST_ABITSLIBS "ACore-ABase-ALibs-ABitsLibs"
					#define TEST_A16BITSITERATOR
					#ifdef  TEST_A16BITSITERATOR
						bool test_a16bitsiterator(bool is_soft = false);
						bool test_node_a16bitsiterator(bool is_soft = false);
						#define MODULE_TEST_A16BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A16BitsIterator"
					#endif
					#define TEST_A32BITSITERATOR
					#ifdef  TEST_A32BITSITERATOR
						bool test_a32bitsiterator(bool is_soft = false);
						bool test_node_a32bitsiterator(bool is_soft = false);
						#define MODULE_TEST_A32BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A32BitsIterator"
					#endif
					#define TEST_A64BITSITERATOR
					#ifdef  TEST_A64BITSITERATOR
						bool test_a64bitsiterator(bool is_soft = false);
						bool test_node_a64bitsiterator(bool is_soft = false);
						#define MODULE_TEST_A64BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A64BitsIterator"
					#endif
					#define TEST_A8BITSITERATOR
					#ifdef  TEST_A8BITSITERATOR
						bool test_a8bitsiterator(bool is_soft = false);
						bool test_node_a8bitsiterator(bool is_soft = false);
						#define MODULE_TEST_A8BITSITERATOR "ACore-ABase-ALibs-ABitsLibs-A8BitsIterator"
					#endif
					#define TEST_ABITSCONVERTOR
					#ifdef  TEST_ABITSCONVERTOR
						bool test_abitsconvertor(bool is_soft = false);
						bool test_node_abitsconvertor(bool is_soft = false);
						#define MODULE_TEST_ABITSCONVERTOR "ACore-ABase-ALibs-ABitsLibs-ABitsConvertor"
					#endif
				#endif
				#define TEST_AREGEX
				#ifdef  TEST_AREGEX
					bool test_aregex(bool is_soft = false);
					bool test_node_aregex(bool is_soft = false);
					#define MODULE_TEST_AREGEX "ACore-ABase-ALibs-ARegex"
					#define TEST_AREGEXCONSTRUCTOR
					#ifdef  TEST_AREGEXCONSTRUCTOR
						bool test_aregexconstructor(bool is_soft = false);
						bool test_node_aregexconstructor(bool is_soft = false);
						#define MODULE_TEST_AREGEXCONSTRUCTOR "ACore-ABase-ALibs-ARegex-ARegexConstructor"
					#endif
					#define TEST_AREGEXPATTERN
					#ifdef  TEST_AREGEXPATTERN
						bool test_aregexpattern(bool is_soft = false);
						bool test_node_aregexpattern(bool is_soft = false);
						#define MODULE_TEST_AREGEXPATTERN "ACore-ABase-ALibs-ARegex-ARegexPattern"
						#define TEST_AREGEXGROUP
						#ifdef  TEST_AREGEXGROUP
							bool test_aregexgroup(bool is_soft = false);
							bool test_node_aregexgroup(bool is_soft = false);
							#define MODULE_TEST_AREGEXGROUP "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup"
							#define TEST_AREGEXGROUPADVANCEDCHECKNEGATIVE
							#ifdef  TEST_AREGEXGROUPADVANCEDCHECKNEGATIVE
								bool test_aregexgroupadvancedchecknegative(bool is_soft = false);
								bool test_node_aregexgroupadvancedchecknegative(bool is_soft = false);
								#define MODULE_TEST_AREGEXGROUPADVANCEDCHECKNEGATIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupAdvancedCheckNegative"
							#endif
							#define TEST_AREGEXGROUPADVANCEDCHECKPOSITIVE
							#ifdef  TEST_AREGEXGROUPADVANCEDCHECKPOSITIVE
								bool test_aregexgroupadvancedcheckpositive(bool is_soft = false);
								bool test_node_aregexgroupadvancedcheckpositive(bool is_soft = false);
								#define MODULE_TEST_AREGEXGROUPADVANCEDCHECKPOSITIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupAdvancedCheckPositive"
							#endif
							#define TEST_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
							#ifdef  TEST_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE
								bool test_aregexgroupretrospectivechecknegative(bool is_soft = false);
								bool test_node_aregexgroupretrospectivechecknegative(bool is_soft = false);
								#define MODULE_TEST_AREGEXGROUPRETROSPECTIVECHECKNEGATIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupRetrospectiveCheckNegative"
							#endif
							#define TEST_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
							#ifdef  TEST_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE
								bool test_aregexgroupretrospectivecheckpositive(bool is_soft = false);
								bool test_node_aregexgroupretrospectivecheckpositive(bool is_soft = false);
								#define MODULE_TEST_AREGEXGROUPRETROSPECTIVECHECKPOSITIVE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupRetrospectiveCheckPositive"
							#endif
							#define TEST_AREGEXGROUPTREE
							#ifdef  TEST_AREGEXGROUPTREE
								bool test_aregexgrouptree(bool is_soft = false);
								bool test_node_aregexgrouptree(bool is_soft = false);
								#define MODULE_TEST_AREGEXGROUPTREE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexGroup-ARegexGroupTree"
							#endif
						#endif
						#define TEST_AREGEXNODE
						#ifdef  TEST_AREGEXNODE
							bool test_aregexnode(bool is_soft = false);
							bool test_node_aregexnode(bool is_soft = false);
							#define MODULE_TEST_AREGEXNODE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode"
							#define TEST_AREGEXNODEANYNUMBER
							#ifdef  TEST_AREGEXNODEANYNUMBER
								bool test_aregexnodeanynumber(bool is_soft = false);
								bool test_node_aregexnodeanynumber(bool is_soft = false);
								#define MODULE_TEST_AREGEXNODEANYNUMBER "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeAnyNumber"
							#endif
							#define TEST_AREGEXNODEATLEASTONE
							#ifdef  TEST_AREGEXNODEATLEASTONE
								bool test_aregexnodeatleastone(bool is_soft = false);
								bool test_node_aregexnodeatleastone(bool is_soft = false);
								#define MODULE_TEST_AREGEXNODEATLEASTONE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeAtLeastOne"
							#endif
							#define TEST_AREGEXNODEBEGINLINE
							#ifdef  TEST_AREGEXNODEBEGINLINE
								bool test_aregexnodebeginline(bool is_soft = false);
								bool test_node_aregexnodebeginline(bool is_soft = false);
								#define MODULE_TEST_AREGEXNODEBEGINLINE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeBeginLine"
							#endif
							#define TEST_AREGEXNODEENDLINE
							#ifdef  TEST_AREGEXNODEENDLINE
								bool test_aregexnodeendline(bool is_soft = false);
								bool test_node_aregexnodeendline(bool is_soft = false);
								#define MODULE_TEST_AREGEXNODEENDLINE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeEndLine"
							#endif
							#define TEST_AREGEXNODEMAYBEPRESENT
							#ifdef  TEST_AREGEXNODEMAYBEPRESENT
								bool test_aregexnodemaybepresent(bool is_soft = false);
								bool test_node_aregexnodemaybepresent(bool is_soft = false);
								#define MODULE_TEST_AREGEXNODEMAYBEPRESENT "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexNode-ARegexNodeMayBePresent"
							#endif
						#endif
						#define TEST_AREGEXPARSER
						#ifdef  TEST_AREGEXPARSER
							bool test_aregexparser(bool is_soft = false);
							bool test_node_aregexparser(bool is_soft = false);
							#define MODULE_TEST_AREGEXPARSER "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexParser"
							#define TEST_AREGEXPARSERTOKEN
							#ifdef  TEST_AREGEXPARSERTOKEN
								bool test_aregexparsertoken(bool is_soft = false);
								bool test_node_aregexparsertoken(bool is_soft = false);
								#define MODULE_TEST_AREGEXPARSERTOKEN "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexParser-ARegexParserToken"
							#endif
						#endif
						#define TEST_AREGEXPART
						#ifdef  TEST_AREGEXPART
							bool test_aregexpart(bool is_soft = false);
							bool test_node_aregexpart(bool is_soft = false);
							#define MODULE_TEST_AREGEXPART "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexPart"
						#endif
						#define TEST_AREGEXREFS
						#ifdef  TEST_AREGEXREFS
							bool test_aregexrefs(bool is_soft = false);
							bool test_node_aregexrefs(bool is_soft = false);
							#define MODULE_TEST_AREGEXREFS "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexRefs"
						#endif
						#define TEST_AREGEXSYMBOL
						#ifdef  TEST_AREGEXSYMBOL
							bool test_aregexsymbol(bool is_soft = false);
							bool test_node_aregexsymbol(bool is_soft = false);
							#define MODULE_TEST_AREGEXSYMBOL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol"
							#define TEST_AREGEXSYMBOLANY
							#ifdef  TEST_AREGEXSYMBOLANY
								bool test_aregexsymbolany(bool is_soft = false);
								bool test_node_aregexsymbolany(bool is_soft = false);
								#define MODULE_TEST_AREGEXSYMBOLANY "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolAny"
							#endif
							#define TEST_AREGEXSYMBOLCLASS
							#ifdef  TEST_AREGEXSYMBOLCLASS
								bool test_aregexsymbolclass(bool is_soft = false);
								bool test_node_aregexsymbolclass(bool is_soft = false);
								#define MODULE_TEST_AREGEXSYMBOLCLASS "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass"
								#define TEST_AREGEXSYMBOLDIGITAL
								#ifdef  TEST_AREGEXSYMBOLDIGITAL
									bool test_aregexsymboldigital(bool is_soft = false);
									bool test_node_aregexsymboldigital(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLDIGITAL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolDigital"
								#endif
								#define TEST_AREGEXSYMBOLNOTDIGITAL
								#ifdef  TEST_AREGEXSYMBOLNOTDIGITAL
									bool test_aregexsymbolnotdigital(bool is_soft = false);
									bool test_node_aregexsymbolnotdigital(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLNOTDIGITAL "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotDigital"
								#endif
								#define TEST_AREGEXSYMBOLNOTSPACE
								#ifdef  TEST_AREGEXSYMBOLNOTSPACE
									bool test_aregexsymbolnotspace(bool is_soft = false);
									bool test_node_aregexsymbolnotspace(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLNOTSPACE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotSpace"
								#endif
								#define TEST_AREGEXSYMBOLNOTWORD
								#ifdef  TEST_AREGEXSYMBOLNOTWORD
									bool test_aregexsymbolnotword(bool is_soft = false);
									bool test_node_aregexsymbolnotword(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLNOTWORD "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolNotWord"
								#endif
								#define TEST_AREGEXSYMBOLSPACE
								#ifdef  TEST_AREGEXSYMBOLSPACE
									bool test_aregexsymbolspace(bool is_soft = false);
									bool test_node_aregexsymbolspace(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLSPACE "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolSpace"
								#endif
								#define TEST_AREGEXSYMBOLWORD
								#ifdef  TEST_AREGEXSYMBOLWORD
									bool test_aregexsymbolword(bool is_soft = false);
									bool test_node_aregexsymbolword(bool is_soft = false);
									#define MODULE_TEST_AREGEXSYMBOLWORD "ACore-ABase-ALibs-ARegex-ARegexPattern-ARegexSymbol-ARegexSymbolClass-ARegexSymbolWord"
								#endif
							#endif
						#endif
					#endif
				#endif
				#define TEST_ASTRINGLIBS
				#ifdef  TEST_ASTRINGLIBS
					bool test_astringlibs(bool is_soft = false);
					bool test_node_astringlibs(bool is_soft = false);
					#define MODULE_TEST_ASTRINGLIBS "ACore-ABase-ALibs-AStringLibs"
					#define TEST_ASTRINGFORMAT
					#ifdef  TEST_ASTRINGFORMAT
						bool test_astringformat(bool is_soft = false);
						bool test_node_astringformat(bool is_soft = false);
						#define MODULE_TEST_ASTRINGFORMAT "ACore-ABase-ALibs-AStringLibs-AStringFormat"
					#endif
					#define TEST_ASTRINGINDEXMAP
					#ifdef  TEST_ASTRINGINDEXMAP
						bool test_astringindexmap(bool is_soft = false);
						bool test_node_astringindexmap(bool is_soft = false);
						#define MODULE_TEST_ASTRINGINDEXMAP "ACore-ABase-ALibs-AStringLibs-AStringIndexMap"
					#endif
					#define TEST_ASTRINGITERATOR
					#ifdef  TEST_ASTRINGITERATOR
						bool test_astringiterator(bool is_soft = false);
						bool test_node_astringiterator(bool is_soft = false);
						#define MODULE_TEST_ASTRINGITERATOR "ACore-ABase-ALibs-AStringLibs-AStringIterator"
					#endif
					#define TEST_ASTRINGLIST
					#ifdef  TEST_ASTRINGLIST
						bool test_astringlist(bool is_soft = false);
						bool test_node_astringlist(bool is_soft = false);
						#define MODULE_TEST_ASTRINGLIST "ACore-ABase-ALibs-AStringLibs-AStringList"
					#endif
					#define TEST_ASTRINGMAP
					#ifdef  TEST_ASTRINGMAP
						bool test_astringmap(bool is_soft = false);
						bool test_node_astringmap(bool is_soft = false);
						#define MODULE_TEST_ASTRINGMAP "ACore-ABase-ALibs-AStringLibs-AStringMap"
					#endif
				#endif
			#endif
			#define TEST_AMATH
			#ifdef  TEST_AMATH
				bool test_amath(bool is_soft = false);
				bool test_node_amath(bool is_soft = false);
				#define MODULE_TEST_AMATH "ACore-ABase-AMath"
			#endif
			#define TEST_ASTRUCT
			#ifdef  TEST_ASTRUCT
				bool test_astruct(bool is_soft = false);
				bool test_node_astruct(bool is_soft = false);
				#define MODULE_TEST_ASTRUCT "ACore-ABase-AStruct"
				#define TEST_AARRAY
				#ifdef  TEST_AARRAY
					bool test_aarray(bool is_soft = false);
					bool test_node_aarray(bool is_soft = false);
					#define MODULE_TEST_AARRAY "ACore-ABase-AStruct-AArray"
				#endif
				#define TEST_ALIST
				#ifdef  TEST_ALIST
					bool test_alist(bool is_soft = false);
					bool test_node_alist(bool is_soft = false);
					#define MODULE_TEST_ALIST "ACore-ABase-AStruct-AList"
					#define TEST_ALISTITERATOR
					#ifdef  TEST_ALISTITERATOR
						bool test_alistiterator(bool is_soft = false);
						bool test_node_alistiterator(bool is_soft = false);
						#define MODULE_TEST_ALISTITERATOR "ACore-ABase-AStruct-AList-AListIterator"
					#endif
				#endif
				#define TEST_AMAP
				#ifdef  TEST_AMAP
					bool test_amap(bool is_soft = false);
					bool test_node_amap(bool is_soft = false);
					#define MODULE_TEST_AMAP "ACore-ABase-AStruct-AMap"
				#endif
				#define TEST_ASTACK
				#ifdef  TEST_ASTACK
					bool test_astack(bool is_soft = false);
					bool test_node_astack(bool is_soft = false);
					#define MODULE_TEST_ASTACK "ACore-ABase-AStruct-AStack"
				#endif
				#define TEST_ASTRUCTITERATOR
				#ifdef  TEST_ASTRUCTITERATOR
					bool test_astructiterator(bool is_soft = false);
					bool test_node_astructiterator(bool is_soft = false);
					#define MODULE_TEST_ASTRUCTITERATOR "ACore-ABase-AStruct-AStructIterator"
				#endif
				#define TEST_ATREE
				#ifdef  TEST_ATREE
					bool test_atree(bool is_soft = false);
					bool test_node_atree(bool is_soft = false);
					#define MODULE_TEST_ATREE "ACore-ABase-AStruct-ATree"
				#endif
				#define TEST_ATREEBINARY
				#ifdef  TEST_ATREEBINARY
					bool test_atreebinary(bool is_soft = false);
					bool test_node_atreebinary(bool is_soft = false);
					#define MODULE_TEST_ATREEBINARY "ACore-ABase-AStruct-ATreeBinary"
				#endif
				#define TEST_ATREEMAP
				#ifdef  TEST_ATREEMAP
					bool test_atreemap(bool is_soft = false);
					bool test_node_atreemap(bool is_soft = false);
					#define MODULE_TEST_ATREEMAP "ACore-ABase-AStruct-ATreeMap"
				#endif
				#define TEST_ATREENAV
				#ifdef  TEST_ATREENAV
					bool test_atreenav(bool is_soft = false);
					bool test_node_atreenav(bool is_soft = false);
					#define MODULE_TEST_ATREENAV "ACore-ABase-AStruct-ATreeNav"
				#endif
				#define TEST_ATREESEARCH
				#ifdef  TEST_ATREESEARCH
					bool test_atreesearch(bool is_soft = false);
					bool test_node_atreesearch(bool is_soft = false);
					#define MODULE_TEST_ATREESEARCH "ACore-ABase-AStruct-ATreeSearch"
				#endif
				#define TEST_AVECTOR
				#ifdef  TEST_AVECTOR
					bool test_avector(bool is_soft = false);
					bool test_node_avector(bool is_soft = false);
					#define MODULE_TEST_AVECTOR "ACore-ABase-AStruct-AVector"
				#endif
			#endif
			#define TEST_ATYPES
			#ifdef  TEST_ATYPES
				bool test_atypes(bool is_soft = false);
				bool test_node_atypes(bool is_soft = false);
				#define MODULE_TEST_ATYPES "ACore-ABase-ATypes"
				#define TEST_ABITS
				#ifdef  TEST_ABITS
					bool test_abits(bool is_soft = false);
					bool test_node_abits(bool is_soft = false);
					#define MODULE_TEST_ABITS "ACore-ABase-ATypes-ABits"
					#define TEST_A16BITS
					#ifdef  TEST_A16BITS
						bool test_a16bits(bool is_soft = false);
						bool test_node_a16bits(bool is_soft = false);
						#define MODULE_TEST_A16BITS "ACore-ABase-ATypes-ABits-A16Bits"
						#define TEST_A16BIT
						#ifdef  TEST_A16BIT
							bool test_a16bit(bool is_soft = false);
							bool test_node_a16bit(bool is_soft = false);
							#define MODULE_TEST_A16BIT "ACore-ABase-ATypes-ABits-A16Bits-A16Bit"
						#endif
						#define TEST_A16BITSWEAK
						#ifdef  TEST_A16BITSWEAK
							bool test_a16bitsweak(bool is_soft = false);
							bool test_node_a16bitsweak(bool is_soft = false);
							#define MODULE_TEST_A16BITSWEAK "ACore-ABase-ATypes-ABits-A16Bits-A16BitsWeak"
						#endif
					#endif
					#define TEST_A32BITS
					#ifdef  TEST_A32BITS
						bool test_a32bits(bool is_soft = false);
						bool test_node_a32bits(bool is_soft = false);
						#define MODULE_TEST_A32BITS "ACore-ABase-ATypes-ABits-A32Bits"
						#define TEST_A32BIT
						#ifdef  TEST_A32BIT
							bool test_a32bit(bool is_soft = false);
							bool test_node_a32bit(bool is_soft = false);
							#define MODULE_TEST_A32BIT "ACore-ABase-ATypes-ABits-A32Bits-A32Bit"
						#endif
						#define TEST_A32BITSWEAK
						#ifdef  TEST_A32BITSWEAK
							bool test_a32bitsweak(bool is_soft = false);
							bool test_node_a32bitsweak(bool is_soft = false);
							#define MODULE_TEST_A32BITSWEAK "ACore-ABase-ATypes-ABits-A32Bits-A32BitsWeak"
						#endif
					#endif
					#define TEST_A64BITS
					#ifdef  TEST_A64BITS
						bool test_a64bits(bool is_soft = false);
						bool test_node_a64bits(bool is_soft = false);
						#define MODULE_TEST_A64BITS "ACore-ABase-ATypes-ABits-A64Bits"
						#define TEST_A64BIT
						#ifdef  TEST_A64BIT
							bool test_a64bit(bool is_soft = false);
							bool test_node_a64bit(bool is_soft = false);
							#define MODULE_TEST_A64BIT "ACore-ABase-ATypes-ABits-A64Bits-A64Bit"
						#endif
						#define TEST_A64BITSWEAK
						#ifdef  TEST_A64BITSWEAK
							bool test_a64bitsweak(bool is_soft = false);
							bool test_node_a64bitsweak(bool is_soft = false);
							#define MODULE_TEST_A64BITSWEAK "ACore-ABase-ATypes-ABits-A64Bits-A64BitsWeak"
						#endif
					#endif
					#define TEST_A8BITS
					#ifdef  TEST_A8BITS
						bool test_a8bits(bool is_soft = false);
						bool test_node_a8bits(bool is_soft = false);
						#define MODULE_TEST_A8BITS "ACore-ABase-ATypes-ABits-A8Bits"
						#define TEST_A8BIT
						#ifdef  TEST_A8BIT
							bool test_a8bit(bool is_soft = false);
							bool test_node_a8bit(bool is_soft = false);
							#define MODULE_TEST_A8BIT "ACore-ABase-ATypes-ABits-A8Bits-A8Bit"
						#endif
						#define TEST_A8BITSWEAK
						#ifdef  TEST_A8BITSWEAK
							bool test_a8bitsweak(bool is_soft = false);
							bool test_node_a8bitsweak(bool is_soft = false);
							#define MODULE_TEST_A8BITSWEAK "ACore-ABase-ATypes-ABits-A8Bits-A8BitsWeak"
						#endif
					#endif
				#endif
				#define TEST_APTR
				#ifdef  TEST_APTR
					bool test_aptr(bool is_soft = false);
					bool test_node_aptr(bool is_soft = false);
					#define MODULE_TEST_APTR "ACore-ABase-ATypes-APtr"
					#define TEST_APTRUNIQUE
					#ifdef  TEST_APTRUNIQUE
						bool test_aptrunique(bool is_soft = false);
						bool test_node_aptrunique(bool is_soft = false);
						#define MODULE_TEST_APTRUNIQUE "ACore-ABase-ATypes-APtr-APtrUnique"
					#endif
					#define TEST_APTRWEAK
					#ifdef  TEST_APTRWEAK
						bool test_aptrweak(bool is_soft = false);
						bool test_node_aptrweak(bool is_soft = false);
						#define MODULE_TEST_APTRWEAK "ACore-ABase-ATypes-APtr-APtrWeak"
					#endif
				#endif
				#define TEST_APTRARRAY
				#ifdef  TEST_APTRARRAY
					bool test_aptrarray(bool is_soft = false);
					bool test_node_aptrarray(bool is_soft = false);
					#define MODULE_TEST_APTRARRAY "ACore-ABase-ATypes-APtrArray"
					#define TEST_APTRARRAYWEAK
					#ifdef  TEST_APTRARRAYWEAK
						bool test_aptrarrayweak(bool is_soft = false);
						bool test_node_aptrarrayweak(bool is_soft = false);
						#define MODULE_TEST_APTRARRAYWEAK "ACore-ABase-ATypes-APtrArray-APtrArrayWeak"
					#endif
				#endif
				#define TEST_ASTRING
				#ifdef  TEST_ASTRING
					bool test_astring(bool is_soft = false);
					bool test_node_astring(bool is_soft = false);
					#define MODULE_TEST_ASTRING "ACore-ABase-ATypes-AString"
					#define TEST_ASTRINGBASE
					#ifdef  TEST_ASTRINGBASE
						bool test_astringbase(bool is_soft = false);
						bool test_node_astringbase(bool is_soft = false);
						#define MODULE_TEST_ASTRINGBASE "ACore-ABase-ATypes-AString-AStringBase"
					#endif
				#endif
				#define TEST_ASYMBOL
				#ifdef  TEST_ASYMBOL
					bool test_asymbol(bool is_soft = false);
					bool test_node_asymbol(bool is_soft = false);
					#define MODULE_TEST_ASYMBOL "ACore-ABase-ATypes-ASymbol"
				#endif
			#endif
		#endif
		#define TEST_ATOOLS
		#ifdef  TEST_ATOOLS
			bool test_atools(bool is_soft = false);
			bool test_node_atools(bool is_soft = false);
			#define MODULE_TEST_ATOOLS "ACore-ATools"
			#define TEST_ACPPPARSER
			#ifdef  TEST_ACPPPARSER
				bool test_acppparser(bool is_soft = false);
				bool test_node_acppparser(bool is_soft = false);
				#define MODULE_TEST_ACPPPARSER "ACore-ATools-ACppParser"
				#define TEST_ACPPIMAGE
				#ifdef  TEST_ACPPIMAGE
					bool test_acppimage(bool is_soft = false);
					bool test_node_acppimage(bool is_soft = false);
					#define MODULE_TEST_ACPPIMAGE "ACore-ATools-ACppParser-ACppImage"
					#define TEST_ACPPITEM
					#ifdef  TEST_ACPPITEM
						bool test_acppitem(bool is_soft = false);
						bool test_node_acppitem(bool is_soft = false);
						#define MODULE_TEST_ACPPITEM "ACore-ATools-ACppParser-ACppImage-ACppItem"
					#endif
				#endif
				#define TEST_ACPPTOKEN
				#ifdef  TEST_ACPPTOKEN
					bool test_acpptoken(bool is_soft = false);
					bool test_node_acpptoken(bool is_soft = false);
					#define MODULE_TEST_ACPPTOKEN "ACore-ATools-ACppParser-ACppToken"
					#define TEST_ACPPTOKENALLLINE
					#ifdef  TEST_ACPPTOKENALLLINE
						bool test_acpptokenallline(bool is_soft = false);
						bool test_node_acpptokenallline(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENALLLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenAllLine"
					#endif
					#define TEST_ACPPTOKENCHAR
					#ifdef  TEST_ACPPTOKENCHAR
						bool test_acpptokenchar(bool is_soft = false);
						bool test_node_acpptokenchar(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENCHAR "ACore-ATools-ACppParser-ACppToken-ACppTokenChar"
					#endif
					#define TEST_ACPPTOKENCOMMENTBEGIN
					#ifdef  TEST_ACPPTOKENCOMMENTBEGIN
						bool test_acpptokencommentbegin(bool is_soft = false);
						bool test_node_acpptokencommentbegin(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENCOMMENTBEGIN "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentBegin"
					#endif
					#define TEST_ACPPTOKENCOMMENTEND
					#ifdef  TEST_ACPPTOKENCOMMENTEND
						bool test_acpptokencommentend(bool is_soft = false);
						bool test_node_acpptokencommentend(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENCOMMENTEND "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentEnd"
					#endif
					#define TEST_ACPPTOKENCOMMENTINLINE
					#ifdef  TEST_ACPPTOKENCOMMENTINLINE
						bool test_acpptokencommentinline(bool is_soft = false);
						bool test_node_acpptokencommentinline(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENCOMMENTINLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentInline"
					#endif
					#define TEST_ACPPTOKENCOMMENTLINE
					#ifdef  TEST_ACPPTOKENCOMMENTLINE
						bool test_acpptokencommentline(bool is_soft = false);
						bool test_node_acpptokencommentline(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENCOMMENTLINE "ACore-ATools-ACppParser-ACppToken-ACppTokenCommentLine"
					#endif
					#define TEST_ACPPTOKENKEYSYMBOL
					#ifdef  TEST_ACPPTOKENKEYSYMBOL
						bool test_acpptokenkeysymbol(bool is_soft = false);
						bool test_node_acpptokenkeysymbol(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENKEYSYMBOL "ACore-ATools-ACppParser-ACppToken-ACppTokenKeySymbol"
					#endif
					#define TEST_ACPPTOKENKEYTYPE
					#ifdef  TEST_ACPPTOKENKEYTYPE
						bool test_acpptokenkeytype(bool is_soft = false);
						bool test_node_acpptokenkeytype(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENKEYTYPE "ACore-ATools-ACppParser-ACppToken-ACppTokenKeyType"
					#endif
					#define TEST_ACPPTOKENKEYWORD
					#ifdef  TEST_ACPPTOKENKEYWORD
						bool test_acpptokenkeyword(bool is_soft = false);
						bool test_node_acpptokenkeyword(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENKEYWORD "ACore-ATools-ACppParser-ACppToken-ACppTokenKeyWord"
					#endif
					#define TEST_ACPPTOKENNAME
					#ifdef  TEST_ACPPTOKENNAME
						bool test_acpptokenname(bool is_soft = false);
						bool test_node_acpptokenname(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENNAME "ACore-ATools-ACppParser-ACppToken-ACppTokenName"
					#endif
					#define TEST_ACPPTOKENNUMBERDOUBLE
					#ifdef  TEST_ACPPTOKENNUMBERDOUBLE
						bool test_acpptokennumberdouble(bool is_soft = false);
						bool test_node_acpptokennumberdouble(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENNUMBERDOUBLE "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberDouble"
					#endif
					#define TEST_ACPPTOKENNUMBERHEX
					#ifdef  TEST_ACPPTOKENNUMBERHEX
						bool test_acpptokennumberhex(bool is_soft = false);
						bool test_node_acpptokennumberhex(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENNUMBERHEX "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberHex"
					#endif
					#define TEST_ACPPTOKENNUMBERINTEGER
					#ifdef  TEST_ACPPTOKENNUMBERINTEGER
						bool test_acpptokennumberinteger(bool is_soft = false);
						bool test_node_acpptokennumberinteger(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENNUMBERINTEGER "ACore-ATools-ACppParser-ACppToken-ACppTokenNumberInteger"
					#endif
					#define TEST_ACPPTOKENSPACE
					#ifdef  TEST_ACPPTOKENSPACE
						bool test_acpptokenspace(bool is_soft = false);
						bool test_node_acpptokenspace(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENSPACE "ACore-ATools-ACppParser-ACppToken-ACppTokenSpace"
					#endif
					#define TEST_ACPPTOKENSTRING
					#ifdef  TEST_ACPPTOKENSTRING
						bool test_acpptokenstring(bool is_soft = false);
						bool test_node_acpptokenstring(bool is_soft = false);
						#define MODULE_TEST_ACPPTOKENSTRING "ACore-ATools-ACppParser-ACppToken-ACppTokenString"
					#endif
				#endif
			#endif
			#define TEST_AHTMLCONSTRUCTOR
			#ifdef  TEST_AHTMLCONSTRUCTOR
				bool test_ahtmlconstructor(bool is_soft = false);
				bool test_node_ahtmlconstructor(bool is_soft = false);
				#define MODULE_TEST_AHTMLCONSTRUCTOR "ACore-ATools-AHtmlConstructor"
			#endif
			#define TEST_ALOADER
			#ifdef  TEST_ALOADER
				bool test_aloader(bool is_soft = false);
				bool test_node_aloader(bool is_soft = false);
				#define MODULE_TEST_ALOADER "ACore-ATools-ALoader"
				#define TEST_ATEXTMANADGER
				#ifdef  TEST_ATEXTMANADGER
					bool test_atextmanadger(bool is_soft = false);
					bool test_node_atextmanadger(bool is_soft = false);
					#define MODULE_TEST_ATEXTMANADGER "ACore-ATools-ALoader-ATextManadger"
					#define TEST_ATEXTFILE
					#ifdef  TEST_ATEXTFILE
						bool test_atextfile(bool is_soft = false);
						bool test_node_atextfile(bool is_soft = false);
						#define MODULE_TEST_ATEXTFILE "ACore-ATools-ALoader-ATextManadger-ATextFile"
						#define TEST_ATEXT
						#ifdef  TEST_ATEXT
							bool test_atext(bool is_soft = false);
							bool test_node_atext(bool is_soft = false);
							#define MODULE_TEST_ATEXT "ACore-ATools-ALoader-ATextManadger-ATextFile-AText"
						#endif
						#define TEST_ATEXTITERATOR
						#ifdef  TEST_ATEXTITERATOR
							bool test_atextiterator(bool is_soft = false);
							bool test_node_atextiterator(bool is_soft = false);
							#define MODULE_TEST_ATEXTITERATOR "ACore-ATools-ALoader-ATextManadger-ATextFile-ATextIterator"
						#endif
					#endif
				#endif
			#endif
		#endif
	#endif
	#define TEST_APROJECT
	#ifdef  TEST_APROJECT
		bool test_aproject(bool is_soft = false);
		bool test_node_aproject(bool is_soft = false);
		#define MODULE_TEST_APROJECT "AProject"
		#define TEST_AMODULE
		#ifdef  TEST_AMODULE
			bool test_amodule(bool is_soft = false);
			bool test_node_amodule(bool is_soft = false);
			#define MODULE_TEST_AMODULE "AProject-AModule"
		#endif
		#define TEST_AMODULEDEBUGHEADER
		#ifdef  TEST_AMODULEDEBUGHEADER
			bool test_amoduledebugheader(bool is_soft = false);
			bool test_node_amoduledebugheader(bool is_soft = false);
			#define MODULE_TEST_AMODULEDEBUGHEADER "AProject-AModuleDebugHeader"
		#endif
		#define TEST_AMODULEDEBUGNODE
		#ifdef  TEST_AMODULEDEBUGNODE
			bool test_amoduledebugnode(bool is_soft = false);
			bool test_node_amoduledebugnode(bool is_soft = false);
			#define MODULE_TEST_AMODULEDEBUGNODE "AProject-AModuleDebugNode"
		#endif
		#define TEST_AMODULEDOCHTML
		#ifdef  TEST_AMODULEDOCHTML
			bool test_amoduledochtml(bool is_soft = false);
			bool test_node_amoduledochtml(bool is_soft = false);
			#define MODULE_TEST_AMODULEDOCHTML "AProject-AModuleDocHtml"
			#define TEST_ADOCHTMLPARSERCPP
			#ifdef  TEST_ADOCHTMLPARSERCPP
				bool test_adochtmlparsercpp(bool is_soft = false);
				bool test_node_adochtmlparsercpp(bool is_soft = false);
				#define MODULE_TEST_ADOCHTMLPARSERCPP "AProject-AModuleDocHtml-ADocHtmlParserCPP"
			#endif
			#define TEST_ADOCHTMLPARSERHPP
			#ifdef  TEST_ADOCHTMLPARSERHPP
				bool test_adochtmlparserhpp(bool is_soft = false);
				bool test_node_adochtmlparserhpp(bool is_soft = false);
				#define MODULE_TEST_ADOCHTMLPARSERHPP "AProject-AModuleDocHtml-ADocHtmlParserHPP"
			#endif
			#define TEST_ADOCHTMLPARSERTEST
			#ifdef  TEST_ADOCHTMLPARSERTEST
				bool test_adochtmlparsertest(bool is_soft = false);
				bool test_node_adochtmlparsertest(bool is_soft = false);
				#define MODULE_TEST_ADOCHTMLPARSERTEST "AProject-AModuleDocHtml-ADocHtmlParserTEST"
			#endif
		#endif
		#define TEST_AMODULEDOCREFSHTML
		#ifdef  TEST_AMODULEDOCREFSHTML
			bool test_amoduledocrefshtml(bool is_soft = false);
			bool test_node_amoduledocrefshtml(bool is_soft = false);
			#define MODULE_TEST_AMODULEDOCREFSHTML "AProject-AModuleDocRefsHtml"
		#endif
		#define TEST_AMODULEDOCSTATUSHTML
		#ifdef  TEST_AMODULEDOCSTATUSHTML
			bool test_amoduledocstatushtml(bool is_soft = false);
			bool test_node_amoduledocstatushtml(bool is_soft = false);
			#define MODULE_TEST_AMODULEDOCSTATUSHTML "AProject-AModuleDocStatusHtml"
			#define TEST_AMODULESIZEINFO
			#ifdef  TEST_AMODULESIZEINFO
				bool test_amodulesizeinfo(bool is_soft = false);
				bool test_node_amodulesizeinfo(bool is_soft = false);
				#define MODULE_TEST_AMODULESIZEINFO "AProject-AModuleDocStatusHtml-AModuleSizeInfo"
			#endif
		#endif
		#define TEST_AMODULEMAKEFILE
		#ifdef  TEST_AMODULEMAKEFILE
			bool test_amodulemakefile(bool is_soft = false);
			bool test_node_amodulemakefile(bool is_soft = false);
			#define MODULE_TEST_AMODULEMAKEFILE "AProject-AModuleMakeFile"
		#endif
		#define TEST_AMODULETESTHEADER
		#ifdef  TEST_AMODULETESTHEADER
			bool test_amoduletestheader(bool is_soft = false);
			bool test_node_amoduletestheader(bool is_soft = false);
			#define MODULE_TEST_AMODULETESTHEADER "AProject-AModuleTestHeader"
		#endif
		#define TEST_AMODULETESTNODE
		#ifdef  TEST_AMODULETESTNODE
			bool test_amoduletestnode(bool is_soft = false);
			bool test_node_amoduletestnode(bool is_soft = false);
			#define MODULE_TEST_AMODULETESTNODE "AProject-AModuleTestNode"
		#endif
		#define TEST_APROJECTCOM
		#ifdef  TEST_APROJECTCOM
			bool test_aprojectcom(bool is_soft = false);
			bool test_node_aprojectcom(bool is_soft = false);
			#define MODULE_TEST_APROJECTCOM "AProject-AProjectCom"
			#define TEST_AMODULETEMPLATECPP
			#ifdef  TEST_AMODULETEMPLATECPP
				bool test_amoduletemplatecpp(bool is_soft = false);
				bool test_node_amoduletemplatecpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATECPP "AProject-AProjectCom-AModuleTemplateCpp"
			#endif
			#define TEST_AMODULETEMPLATEDEBUGCPP
			#ifdef  TEST_AMODULETEMPLATEDEBUGCPP
				bool test_amoduletemplatedebugcpp(bool is_soft = false);
				bool test_node_amoduletemplatedebugcpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATEDEBUGCPP "AProject-AProjectCom-AModuleTemplateDebugCpp"
			#endif
			#define TEST_AMODULETEMPLATEDOCHTML
			#ifdef  TEST_AMODULETEMPLATEDOCHTML
				bool test_amoduletemplatedochtml(bool is_soft = false);
				bool test_node_amoduletemplatedochtml(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATEDOCHTML "AProject-AProjectCom-AModuleTemplateDocHtml"
			#endif
			#define TEST_AMODULETEMPLATEEXCEPTIONCPP
			#ifdef  TEST_AMODULETEMPLATEEXCEPTIONCPP
				bool test_amoduletemplateexceptioncpp(bool is_soft = false);
				bool test_node_amoduletemplateexceptioncpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATEEXCEPTIONCPP "AProject-AProjectCom-AModuleTemplateExceptionCpp"
			#endif
			#define TEST_AMODULETEMPLATEEXCEPTIONHPP
			#ifdef  TEST_AMODULETEMPLATEEXCEPTIONHPP
				bool test_amoduletemplateexceptionhpp(bool is_soft = false);
				bool test_node_amoduletemplateexceptionhpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATEEXCEPTIONHPP "AProject-AProjectCom-AModuleTemplateExceptionHpp"
			#endif
			#define TEST_AMODULETEMPLATEHPP
			#ifdef  TEST_AMODULETEMPLATEHPP
				bool test_amoduletemplatehpp(bool is_soft = false);
				bool test_node_amoduletemplatehpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATEHPP "AProject-AProjectCom-AModuleTemplateHpp"
			#endif
			#define TEST_AMODULETEMPLATESTATUS
			#ifdef  TEST_AMODULETEMPLATESTATUS
				bool test_amoduletemplatestatus(bool is_soft = false);
				bool test_node_amoduletemplatestatus(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATESTATUS "AProject-AProjectCom-AModuleTemplateStatus"
			#endif
			#define TEST_AMODULETEMPLATETESTCPP
			#ifdef  TEST_AMODULETEMPLATETESTCPP
				bool test_amoduletemplatetestcpp(bool is_soft = false);
				bool test_node_amoduletemplatetestcpp(bool is_soft = false);
				#define MODULE_TEST_AMODULETEMPLATETESTCPP "AProject-AProjectCom-AModuleTemplateTestCpp"
			#endif
		#endif
	#endif
#endif
#endif
