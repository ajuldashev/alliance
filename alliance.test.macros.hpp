#ifndef HPP_TEST_MACROS_FILE
#define HPP_TEST_MACROS_FILE

#define TEST_FUNCTION_BEGIN(func)													\
static bool (test_##func)(bool is_soft) {											\
	AIO::write_warning("[");			

#define TEST_FUNCTION_END															\
	AIO::write_success("]");														\
    return true;																	\
}

#define TEST_NODE(node)                 											\
if (!test_node_##node(is_soft)) {       											\
	return is_soft;                     											\
}

#define TEST_FUNCTION_MAIN_BEGIN(func)												\
bool (test_##func)(bool is_soft) {  												\
	TEST_NODE(func)

#define TEST_FUNCTION_MAIN_END(MODULE)												\
	AIO::writeln();																	\
	AIO::writeln_success(" -> " MODULE_TEST_##MODULE ); 							\
	AIO::write_div_line();															\
	return true;																	\
}

#define TEST_CALL(num)   															\
if (!(test_##num)(is_soft)) {														\
	AIO::write_error((", "));           											\
	AIO::write_error("test ");			   											\
	AIO::write_error(AStringLibs::to_string(s64int(num)));							\
	AIO::write_error(("]"));            											\
	if (!is_soft) {                     											\
		AIO::writeln();                 											\
		return false;                   											\
	}                                   											\
}

#define TEST_ALERT(flag, num)  														\
AIO::write_warning(".");															\
if (!(flag)) {                          											\
	AIO::write_error("step ");			   											\
	AIO::write_error(AStringLibs::to_string(s64int(num)));							\
	return false;                       											\
}

#endif