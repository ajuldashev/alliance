#include "alliance.hpp"

#ifdef TEST_ALLIANCE
#include <ACore/ABase/AIO/AIO.hpp>
bool test_node_alliance(bool is_soft) {
	#ifdef TEST_ACORE
	if (!test_acore(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_ACORE );
		return is_soft;
	}
	#endif
	#ifdef TEST_APROJECT
	if (!test_aproject(is_soft)) {
		AIO::writeln_error("FAIL test the module " MODULE_TEST_APROJECT );
		return is_soft;
	}
	#endif
	return true;
}
#endif
