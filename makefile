all : alliance

alliance.o  : alliance.hpp alliance.cpp alliance.test.cpp alliance.debug.cpp alliance.exception.hpp alliance.exception.cpp  
	g++ -I. -std=c++11 -c alliance.cpp -o alliance.o 

out/acore.o  : ACore/ACore.hpp ACore/ACore.cpp ACore/ACore.test.cpp ACore/ACore.debug.cpp ACore/ACore.exception.hpp ACore/ACore.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ACore.cpp -o out/acore.o 

out/abase.o  : ACore/ABase/ABase.hpp ACore/ABase/ABase.cpp ACore/ABase/ABase.test.cpp ACore/ABase/ABase.debug.cpp ACore/ABase/ABase.exception.hpp ACore/ABase/ABase.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ABase.cpp -o out/abase.o 

out/aencryption.o  : ACore/ABase/AEncryption/AEncryption.hpp ACore/ABase/AEncryption/AEncryption.cpp ACore/ABase/AEncryption/AEncryption.test.cpp ACore/ABase/AEncryption/AEncryption.debug.cpp ACore/ABase/AEncryption/AEncryption.exception.hpp ACore/ABase/AEncryption/AEncryption.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/AEncryption.cpp -o out/aencryption.o 

out/acrc16.o  : ACore/ABase/AEncryption/ACrc16/ACrc16.hpp ACore/ABase/AEncryption/ACrc16/ACrc16.cpp ACore/ABase/AEncryption/ACrc16/ACrc16.test.cpp ACore/ABase/AEncryption/ACrc16/ACrc16.debug.cpp ACore/ABase/AEncryption/ACrc16/ACrc16.exception.hpp ACore/ABase/AEncryption/ACrc16/ACrc16.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ACrc16/ACrc16.cpp -o out/acrc16.o 

out/acrc16_modules.o : out/acrc16.o
	ld -r out/acrc16.o -o out/acrc16_modules.o

out/acrc32.o  : ACore/ABase/AEncryption/ACrc32/ACrc32.hpp ACore/ABase/AEncryption/ACrc32/ACrc32.cpp ACore/ABase/AEncryption/ACrc32/ACrc32.test.cpp ACore/ABase/AEncryption/ACrc32/ACrc32.debug.cpp ACore/ABase/AEncryption/ACrc32/ACrc32.exception.hpp ACore/ABase/AEncryption/ACrc32/ACrc32.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ACrc32/ACrc32.cpp -o out/acrc32.o 

out/acrc32_modules.o : out/acrc32.o
	ld -r out/acrc32.o -o out/acrc32_modules.o

out/acrc64.o  : ACore/ABase/AEncryption/ACrc64/ACrc64.hpp ACore/ABase/AEncryption/ACrc64/ACrc64.cpp ACore/ABase/AEncryption/ACrc64/ACrc64.test.cpp ACore/ABase/AEncryption/ACrc64/ACrc64.debug.cpp ACore/ABase/AEncryption/ACrc64/ACrc64.exception.hpp ACore/ABase/AEncryption/ACrc64/ACrc64.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ACrc64/ACrc64.cpp -o out/acrc64.o 

out/acrc64_modules.o : out/acrc64.o
	ld -r out/acrc64.o -o out/acrc64_modules.o

out/asha1.o  : ACore/ABase/AEncryption/ASha1/ASha1.hpp ACore/ABase/AEncryption/ASha1/ASha1.cpp ACore/ABase/AEncryption/ASha1/ASha1.test.cpp ACore/ABase/AEncryption/ASha1/ASha1.debug.cpp ACore/ABase/AEncryption/ASha1/ASha1.exception.hpp ACore/ABase/AEncryption/ASha1/ASha1.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ASha1/ASha1.cpp -o out/asha1.o 

out/asha1_modules.o : out/asha1.o
	ld -r out/asha1.o -o out/asha1_modules.o

out/asha256.o  : ACore/ABase/AEncryption/ASha256/ASha256.hpp ACore/ABase/AEncryption/ASha256/ASha256.cpp ACore/ABase/AEncryption/ASha256/ASha256.test.cpp ACore/ABase/AEncryption/ASha256/ASha256.debug.cpp ACore/ABase/AEncryption/ASha256/ASha256.exception.hpp ACore/ABase/AEncryption/ASha256/ASha256.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ASha256/ASha256.cpp -o out/asha256.o 

out/asha256_modules.o : out/asha256.o
	ld -r out/asha256.o -o out/asha256_modules.o

out/asha3.o  : ACore/ABase/AEncryption/ASha3/ASha3.hpp ACore/ABase/AEncryption/ASha3/ASha3.cpp ACore/ABase/AEncryption/ASha3/ASha3.test.cpp ACore/ABase/AEncryption/ASha3/ASha3.debug.cpp ACore/ABase/AEncryption/ASha3/ASha3.exception.hpp ACore/ABase/AEncryption/ASha3/ASha3.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ASha3/ASha3.cpp -o out/asha3.o 

out/asha3_modules.o : out/asha3.o
	ld -r out/asha3.o -o out/asha3_modules.o

out/asha512.o  : ACore/ABase/AEncryption/ASha512/ASha512.hpp ACore/ABase/AEncryption/ASha512/ASha512.cpp ACore/ABase/AEncryption/ASha512/ASha512.test.cpp ACore/ABase/AEncryption/ASha512/ASha512.debug.cpp ACore/ABase/AEncryption/ASha512/ASha512.exception.hpp ACore/ABase/AEncryption/ASha512/ASha512.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AEncryption/ASha512/ASha512.cpp -o out/asha512.o 

out/asha512_modules.o : out/asha512.o
	ld -r out/asha512.o -o out/asha512_modules.o

out/aencryption_modules.o : out/aencryption.o out/acrc16_modules.o out/acrc32_modules.o out/acrc64_modules.o out/asha1_modules.o out/asha256_modules.o out/asha3_modules.o out/asha512_modules.o
	ld -r out/aencryption.o out/acrc16_modules.o out/acrc32_modules.o out/acrc64_modules.o out/asha1_modules.o out/asha256_modules.o out/asha3_modules.o out/asha512_modules.o -o out/aencryption_modules.o

out/aio.o  : ACore/ABase/AIO/AIO.hpp ACore/ABase/AIO/AIO.cpp ACore/ABase/AIO/AIO.test.cpp ACore/ABase/AIO/AIO.debug.cpp ACore/ABase/AIO/AIO.exception.hpp ACore/ABase/AIO/AIO.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AIO.cpp -o out/aio.o 

out/aconsole.o  : ACore/ABase/AIO/AConsole/AConsole.hpp ACore/ABase/AIO/AConsole/AConsole.cpp ACore/ABase/AIO/AConsole/AConsole.test.cpp ACore/ABase/AIO/AConsole/AConsole.debug.cpp ACore/ABase/AIO/AConsole/AConsole.exception.hpp ACore/ABase/AIO/AConsole/AConsole.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AConsole/AConsole.cpp -o out/aconsole.o 

out/aconsoleibuffer.o  : ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.hpp ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.cpp ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.test.cpp ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.debug.cpp ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.exception.hpp ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AConsole/AConsoleIBuffer/AConsoleIBuffer.cpp -o out/aconsoleibuffer.o 

out/aconsoleibuffer_modules.o : out/aconsoleibuffer.o
	ld -r out/aconsoleibuffer.o -o out/aconsoleibuffer_modules.o

out/aconsole_modules.o : out/aconsole.o out/aconsoleibuffer_modules.o
	ld -r out/aconsole.o out/aconsoleibuffer_modules.o -o out/aconsole_modules.o

out/adir.o  : ACore/ABase/AIO/ADir/ADir.hpp ACore/ABase/AIO/ADir/ADir.cpp ACore/ABase/AIO/ADir/ADir.test.cpp ACore/ABase/AIO/ADir/ADir.debug.cpp ACore/ABase/AIO/ADir/ADir.exception.hpp ACore/ABase/AIO/ADir/ADir.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/ADir/ADir.cpp -o out/adir.o 

out/adir_modules.o : out/adir.o
	ld -r out/adir.o -o out/adir_modules.o

out/afile.o  : ACore/ABase/AIO/AFile/AFile.hpp ACore/ABase/AIO/AFile/AFile.cpp ACore/ABase/AIO/AFile/AFile.test.cpp ACore/ABase/AIO/AFile/AFile.debug.cpp ACore/ABase/AIO/AFile/AFile.exception.hpp ACore/ABase/AIO/AFile/AFile.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AFile/AFile.cpp -o out/afile.o 

out/aifile.o  : ACore/ABase/AIO/AFile/AIFile/AIFile.hpp ACore/ABase/AIO/AFile/AIFile/AIFile.cpp ACore/ABase/AIO/AFile/AIFile/AIFile.test.cpp ACore/ABase/AIO/AFile/AIFile/AIFile.debug.cpp ACore/ABase/AIO/AFile/AIFile/AIFile.exception.hpp ACore/ABase/AIO/AFile/AIFile/AIFile.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AFile/AIFile/AIFile.cpp -o out/aifile.o 

out/aifile_modules.o : out/aifile.o
	ld -r out/aifile.o -o out/aifile_modules.o

out/aofile.o  : ACore/ABase/AIO/AFile/AOFile/AOFile.hpp ACore/ABase/AIO/AFile/AOFile/AOFile.cpp ACore/ABase/AIO/AFile/AOFile/AOFile.test.cpp ACore/ABase/AIO/AFile/AOFile/AOFile.debug.cpp ACore/ABase/AIO/AFile/AOFile/AOFile.exception.hpp ACore/ABase/AIO/AFile/AOFile/AOFile.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AFile/AOFile/AOFile.cpp -o out/aofile.o 

out/aofile_modules.o : out/aofile.o
	ld -r out/aofile.o -o out/aofile_modules.o

out/afile_modules.o : out/afile.o out/aifile_modules.o out/aofile_modules.o
	ld -r out/afile.o out/aifile_modules.o out/aofile_modules.o -o out/afile_modules.o

out/afileinfo.o  : ACore/ABase/AIO/AFileInfo/AFileInfo.hpp ACore/ABase/AIO/AFileInfo/AFileInfo.cpp ACore/ABase/AIO/AFileInfo/AFileInfo.test.cpp ACore/ABase/AIO/AFileInfo/AFileInfo.debug.cpp ACore/ABase/AIO/AFileInfo/AFileInfo.exception.hpp ACore/ABase/AIO/AFileInfo/AFileInfo.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AIO/AFileInfo/AFileInfo.cpp -o out/afileinfo.o 

out/afileinfo_modules.o : out/afileinfo.o
	ld -r out/afileinfo.o -o out/afileinfo_modules.o

out/aio_modules.o : out/aio.o out/aconsole_modules.o out/adir_modules.o out/afile_modules.o out/afileinfo_modules.o
	ld -r out/aio.o out/aconsole_modules.o out/adir_modules.o out/afile_modules.o out/afileinfo_modules.o -o out/aio_modules.o

out/alibs.o  : ACore/ABase/ALibs/ALibs.hpp ACore/ABase/ALibs/ALibs.cpp ACore/ABase/ALibs/ALibs.test.cpp ACore/ABase/ALibs/ALibs.debug.cpp ACore/ABase/ALibs/ALibs.exception.hpp ACore/ABase/ALibs/ALibs.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ALibs.cpp -o out/alibs.o 

out/abitslibs.o  : ACore/ABase/ALibs/ABitsLibs/ABitsLibs.hpp ACore/ABase/ALibs/ABitsLibs/ABitsLibs.cpp ACore/ABase/ALibs/ABitsLibs/ABitsLibs.test.cpp ACore/ABase/ALibs/ABitsLibs/ABitsLibs.debug.cpp ACore/ABase/ALibs/ABitsLibs/ABitsLibs.exception.hpp ACore/ABase/ALibs/ABitsLibs/ABitsLibs.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/ABitsLibs.cpp -o out/abitslibs.o 

out/a16bitsiterator.o  : ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.hpp ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.cpp ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.test.cpp ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.debug.cpp ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.exception.hpp ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/A16BitsIterator/A16BitsIterator.cpp -o out/a16bitsiterator.o 

out/a16bitsiterator_modules.o : out/a16bitsiterator.o
	ld -r out/a16bitsiterator.o -o out/a16bitsiterator_modules.o

out/a32bitsiterator.o  : ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.hpp ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.cpp ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.test.cpp ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.debug.cpp ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.exception.hpp ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/A32BitsIterator/A32BitsIterator.cpp -o out/a32bitsiterator.o 

out/a32bitsiterator_modules.o : out/a32bitsiterator.o
	ld -r out/a32bitsiterator.o -o out/a32bitsiterator_modules.o

out/a64bitsiterator.o  : ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.hpp ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.cpp ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.test.cpp ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.debug.cpp ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.exception.hpp ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/A64BitsIterator/A64BitsIterator.cpp -o out/a64bitsiterator.o 

out/a64bitsiterator_modules.o : out/a64bitsiterator.o
	ld -r out/a64bitsiterator.o -o out/a64bitsiterator_modules.o

out/a8bitsiterator.o  : ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.hpp ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.cpp ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.test.cpp ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.debug.cpp ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.exception.hpp ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/A8BitsIterator/A8BitsIterator.cpp -o out/a8bitsiterator.o 

out/a8bitsiterator_modules.o : out/a8bitsiterator.o
	ld -r out/a8bitsiterator.o -o out/a8bitsiterator_modules.o

out/abitsconvertor.o  : ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.hpp ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.cpp ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.test.cpp ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.debug.cpp ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.exception.hpp ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ABitsLibs/ABitsConvertor/ABitsConvertor.cpp -o out/abitsconvertor.o 

out/abitsconvertor_modules.o : out/abitsconvertor.o
	ld -r out/abitsconvertor.o -o out/abitsconvertor_modules.o

out/abitslibs_modules.o : out/abitslibs.o out/a16bitsiterator_modules.o out/a32bitsiterator_modules.o out/a64bitsiterator_modules.o out/a8bitsiterator_modules.o out/abitsconvertor_modules.o
	ld -r out/abitslibs.o out/a16bitsiterator_modules.o out/a32bitsiterator_modules.o out/a64bitsiterator_modules.o out/a8bitsiterator_modules.o out/abitsconvertor_modules.o -o out/abitslibs_modules.o

out/aregex.o  : ACore/ABase/ALibs/ARegex/ARegex.hpp ACore/ABase/ALibs/ARegex/ARegex.cpp ACore/ABase/ALibs/ARegex/ARegex.test.cpp ACore/ABase/ALibs/ARegex/ARegex.debug.cpp ACore/ABase/ALibs/ARegex/ARegex.exception.hpp ACore/ABase/ALibs/ARegex/ARegex.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegex.cpp -o out/aregex.o 

out/aregexconstructor.o  : ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.hpp ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.cpp ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.test.cpp ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.debug.cpp ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.exception.hpp ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexConstructor/ARegexConstructor.cpp -o out/aregexconstructor.o 

out/aregexconstructor_modules.o : out/aregexconstructor.o
	ld -r out/aregexconstructor.o -o out/aregexconstructor_modules.o

out/aregexpattern.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPattern.cpp -o out/aregexpattern.o 

out/aregexgroup.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroup.cpp -o out/aregexgroup.o 

out/aregexgroupadvancedchecknegative.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckNegative/ARegexGroupAdvancedCheckNegative.cpp -o out/aregexgroupadvancedchecknegative.o 

out/aregexgroupadvancedchecknegative_modules.o : out/aregexgroupadvancedchecknegative.o
	ld -r out/aregexgroupadvancedchecknegative.o -o out/aregexgroupadvancedchecknegative_modules.o

out/aregexgroupadvancedcheckpositive.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupAdvancedCheckPositive/ARegexGroupAdvancedCheckPositive.cpp -o out/aregexgroupadvancedcheckpositive.o 

out/aregexgroupadvancedcheckpositive_modules.o : out/aregexgroupadvancedcheckpositive.o
	ld -r out/aregexgroupadvancedcheckpositive.o -o out/aregexgroupadvancedcheckpositive_modules.o

out/aregexgroupretrospectivechecknegative.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckNegative/ARegexGroupRetrospectiveCheckNegative.cpp -o out/aregexgroupretrospectivechecknegative.o 

out/aregexgroupretrospectivechecknegative_modules.o : out/aregexgroupretrospectivechecknegative.o
	ld -r out/aregexgroupretrospectivechecknegative.o -o out/aregexgroupretrospectivechecknegative_modules.o

out/aregexgroupretrospectivecheckpositive.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupRetrospectiveCheckPositive/ARegexGroupRetrospectiveCheckPositive.cpp -o out/aregexgroupretrospectivecheckpositive.o 

out/aregexgroupretrospectivecheckpositive_modules.o : out/aregexgroupretrospectivecheckpositive.o
	ld -r out/aregexgroupretrospectivecheckpositive.o -o out/aregexgroupretrospectivecheckpositive_modules.o

out/aregexgrouptree.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexGroup/ARegexGroupTree/ARegexGroupTree.cpp -o out/aregexgrouptree.o 

out/aregexgrouptree_modules.o : out/aregexgrouptree.o
	ld -r out/aregexgrouptree.o -o out/aregexgrouptree_modules.o

out/aregexgroup_modules.o : out/aregexgroup.o out/aregexgroupadvancedchecknegative_modules.o out/aregexgroupadvancedcheckpositive_modules.o out/aregexgroupretrospectivechecknegative_modules.o out/aregexgroupretrospectivecheckpositive_modules.o out/aregexgrouptree_modules.o
	ld -r out/aregexgroup.o out/aregexgroupadvancedchecknegative_modules.o out/aregexgroupadvancedcheckpositive_modules.o out/aregexgroupretrospectivechecknegative_modules.o out/aregexgroupretrospectivecheckpositive_modules.o out/aregexgrouptree_modules.o -o out/aregexgroup_modules.o

out/aregexnode.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNode.cpp -o out/aregexnode.o 

out/aregexnodeanynumber.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAnyNumber/ARegexNodeAnyNumber.cpp -o out/aregexnodeanynumber.o 

out/aregexnodeanynumber_modules.o : out/aregexnodeanynumber.o
	ld -r out/aregexnodeanynumber.o -o out/aregexnodeanynumber_modules.o

out/aregexnodeatleastone.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeAtLeastOne/ARegexNodeAtLeastOne.cpp -o out/aregexnodeatleastone.o 

out/aregexnodeatleastone_modules.o : out/aregexnodeatleastone.o
	ld -r out/aregexnodeatleastone.o -o out/aregexnodeatleastone_modules.o

out/aregexnodebeginline.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeBeginLine/ARegexNodeBeginLine.cpp -o out/aregexnodebeginline.o 

out/aregexnodebeginline_modules.o : out/aregexnodebeginline.o
	ld -r out/aregexnodebeginline.o -o out/aregexnodebeginline_modules.o

out/aregexnodeendline.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeEndLine/ARegexNodeEndLine.cpp -o out/aregexnodeendline.o 

out/aregexnodeendline_modules.o : out/aregexnodeendline.o
	ld -r out/aregexnodeendline.o -o out/aregexnodeendline_modules.o

out/aregexnodemaybepresent.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexNode/ARegexNodeMayBePresent/ARegexNodeMayBePresent.cpp -o out/aregexnodemaybepresent.o 

out/aregexnodemaybepresent_modules.o : out/aregexnodemaybepresent.o
	ld -r out/aregexnodemaybepresent.o -o out/aregexnodemaybepresent_modules.o

out/aregexnode_modules.o : out/aregexnode.o out/aregexnodeanynumber_modules.o out/aregexnodeatleastone_modules.o out/aregexnodebeginline_modules.o out/aregexnodeendline_modules.o out/aregexnodemaybepresent_modules.o
	ld -r out/aregexnode.o out/aregexnodeanynumber_modules.o out/aregexnodeatleastone_modules.o out/aregexnodebeginline_modules.o out/aregexnodeendline_modules.o out/aregexnodemaybepresent_modules.o -o out/aregexnode_modules.o

out/aregexparser.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParser.cpp -o out/aregexparser.o 

out/aregexparsertoken.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexParser/ARegexParserToken/ARegexParserToken.cpp -o out/aregexparsertoken.o 

out/aregexparsertoken_modules.o : out/aregexparsertoken.o
	ld -r out/aregexparsertoken.o -o out/aregexparsertoken_modules.o

out/aregexparser_modules.o : out/aregexparser.o out/aregexparsertoken_modules.o
	ld -r out/aregexparser.o out/aregexparsertoken_modules.o -o out/aregexparser_modules.o

out/aregexpart.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexPart/ARegexPart.cpp -o out/aregexpart.o 

out/aregexpart_modules.o : out/aregexpart.o
	ld -r out/aregexpart.o -o out/aregexpart_modules.o

out/aregexrefs.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexRefs/ARegexRefs.cpp -o out/aregexrefs.o 

out/aregexrefs_modules.o : out/aregexrefs.o
	ld -r out/aregexrefs.o -o out/aregexrefs_modules.o

out/aregexsymbol.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbol.cpp -o out/aregexsymbol.o 

out/aregexsymbolany.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolAny/ARegexSymbolAny.cpp -o out/aregexsymbolany.o 

out/aregexsymbolany_modules.o : out/aregexsymbolany.o
	ld -r out/aregexsymbolany.o -o out/aregexsymbolany_modules.o

out/aregexsymbolclass.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolClass.cpp -o out/aregexsymbolclass.o 

out/aregexsymboldigital.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolDigital/ARegexSymbolDigital.cpp -o out/aregexsymboldigital.o 

out/aregexsymboldigital_modules.o : out/aregexsymboldigital.o
	ld -r out/aregexsymboldigital.o -o out/aregexsymboldigital_modules.o

out/aregexsymbolnotdigital.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotDigital/ARegexSymbolNotDigital.cpp -o out/aregexsymbolnotdigital.o 

out/aregexsymbolnotdigital_modules.o : out/aregexsymbolnotdigital.o
	ld -r out/aregexsymbolnotdigital.o -o out/aregexsymbolnotdigital_modules.o

out/aregexsymbolnotspace.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotSpace/ARegexSymbolNotSpace.cpp -o out/aregexsymbolnotspace.o 

out/aregexsymbolnotspace_modules.o : out/aregexsymbolnotspace.o
	ld -r out/aregexsymbolnotspace.o -o out/aregexsymbolnotspace_modules.o

out/aregexsymbolnotword.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolNotWord/ARegexSymbolNotWord.cpp -o out/aregexsymbolnotword.o 

out/aregexsymbolnotword_modules.o : out/aregexsymbolnotword.o
	ld -r out/aregexsymbolnotword.o -o out/aregexsymbolnotword_modules.o

out/aregexsymbolspace.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolSpace/ARegexSymbolSpace.cpp -o out/aregexsymbolspace.o 

out/aregexsymbolspace_modules.o : out/aregexsymbolspace.o
	ld -r out/aregexsymbolspace.o -o out/aregexsymbolspace_modules.o

out/aregexsymbolword.o  : ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.test.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.debug.cpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.exception.hpp ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/ARegex/ARegexPattern/ARegexSymbol/ARegexSymbolClass/ARegexSymbolWord/ARegexSymbolWord.cpp -o out/aregexsymbolword.o 

out/aregexsymbolword_modules.o : out/aregexsymbolword.o
	ld -r out/aregexsymbolword.o -o out/aregexsymbolword_modules.o

out/aregexsymbolclass_modules.o : out/aregexsymbolclass.o out/aregexsymboldigital_modules.o out/aregexsymbolnotdigital_modules.o out/aregexsymbolnotspace_modules.o out/aregexsymbolnotword_modules.o out/aregexsymbolspace_modules.o out/aregexsymbolword_modules.o
	ld -r out/aregexsymbolclass.o out/aregexsymboldigital_modules.o out/aregexsymbolnotdigital_modules.o out/aregexsymbolnotspace_modules.o out/aregexsymbolnotword_modules.o out/aregexsymbolspace_modules.o out/aregexsymbolword_modules.o -o out/aregexsymbolclass_modules.o

out/aregexsymbol_modules.o : out/aregexsymbol.o out/aregexsymbolany_modules.o out/aregexsymbolclass_modules.o
	ld -r out/aregexsymbol.o out/aregexsymbolany_modules.o out/aregexsymbolclass_modules.o -o out/aregexsymbol_modules.o

out/aregexpattern_modules.o : out/aregexpattern.o out/aregexgroup_modules.o out/aregexnode_modules.o out/aregexparser_modules.o out/aregexpart_modules.o out/aregexrefs_modules.o out/aregexsymbol_modules.o
	ld -r out/aregexpattern.o out/aregexgroup_modules.o out/aregexnode_modules.o out/aregexparser_modules.o out/aregexpart_modules.o out/aregexrefs_modules.o out/aregexsymbol_modules.o -o out/aregexpattern_modules.o

out/aregex_modules.o : out/aregex.o out/aregexconstructor_modules.o out/aregexpattern_modules.o
	ld -r out/aregex.o out/aregexconstructor_modules.o out/aregexpattern_modules.o -o out/aregex_modules.o

out/astringlibs.o  : ACore/ABase/ALibs/AStringLibs/AStringLibs.hpp ACore/ABase/ALibs/AStringLibs/AStringLibs.cpp ACore/ABase/ALibs/AStringLibs/AStringLibs.test.cpp ACore/ABase/ALibs/AStringLibs/AStringLibs.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringLibs.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringLibs.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringLibs.cpp -o out/astringlibs.o 

out/astringformat.o  : ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.hpp ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.cpp ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.test.cpp ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringFormat/AStringFormat.cpp -o out/astringformat.o 

out/astringformat_modules.o : out/astringformat.o
	ld -r out/astringformat.o -o out/astringformat_modules.o

out/astringindexmap.o  : ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.hpp ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.cpp ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.test.cpp ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringIndexMap/AStringIndexMap.cpp -o out/astringindexmap.o 

out/astringindexmap_modules.o : out/astringindexmap.o
	ld -r out/astringindexmap.o -o out/astringindexmap_modules.o

out/astringiterator.o  : ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.hpp ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.cpp ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.test.cpp ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringIterator/AStringIterator.cpp -o out/astringiterator.o 

out/astringiterator_modules.o : out/astringiterator.o
	ld -r out/astringiterator.o -o out/astringiterator_modules.o

out/astringlist.o  : ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.hpp ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.cpp ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.test.cpp ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringList/AStringList.cpp -o out/astringlist.o 

out/astringlist_modules.o : out/astringlist.o
	ld -r out/astringlist.o -o out/astringlist_modules.o

out/astringmap.o  : ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.hpp ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.cpp ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.test.cpp ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.debug.cpp ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.exception.hpp ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ALibs/AStringLibs/AStringMap/AStringMap.cpp -o out/astringmap.o 

out/astringmap_modules.o : out/astringmap.o
	ld -r out/astringmap.o -o out/astringmap_modules.o

out/astringlibs_modules.o : out/astringlibs.o out/astringformat_modules.o out/astringindexmap_modules.o out/astringiterator_modules.o out/astringlist_modules.o out/astringmap_modules.o
	ld -r out/astringlibs.o out/astringformat_modules.o out/astringindexmap_modules.o out/astringiterator_modules.o out/astringlist_modules.o out/astringmap_modules.o -o out/astringlibs_modules.o

out/alibs_modules.o : out/alibs.o out/abitslibs_modules.o out/aregex_modules.o out/astringlibs_modules.o
	ld -r out/alibs.o out/abitslibs_modules.o out/aregex_modules.o out/astringlibs_modules.o -o out/alibs_modules.o

out/amath.o  : ACore/ABase/AMath/AMath.hpp ACore/ABase/AMath/AMath.cpp ACore/ABase/AMath/AMath.test.cpp ACore/ABase/AMath/AMath.debug.cpp ACore/ABase/AMath/AMath.exception.hpp ACore/ABase/AMath/AMath.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AMath/AMath.cpp -o out/amath.o 

out/amath_modules.o : out/amath.o
	ld -r out/amath.o -o out/amath_modules.o

out/astruct.o  : ACore/ABase/AStruct/AStruct.hpp ACore/ABase/AStruct/AStruct.cpp ACore/ABase/AStruct/AStruct.test.cpp ACore/ABase/AStruct/AStruct.debug.cpp ACore/ABase/AStruct/AStruct.exception.hpp ACore/ABase/AStruct/AStruct.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AStruct.cpp -o out/astruct.o 

out/aarray.o  : ACore/ABase/AStruct/AArray/AArray.hpp ACore/ABase/AStruct/AArray/AArray.cpp ACore/ABase/AStruct/AArray/AArray.test.cpp ACore/ABase/AStruct/AArray/AArray.debug.cpp ACore/ABase/AStruct/AArray/AArray.exception.hpp ACore/ABase/AStruct/AArray/AArray.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AArray/AArray.cpp -o out/aarray.o 

out/aarray_modules.o : out/aarray.o
	ld -r out/aarray.o -o out/aarray_modules.o

out/alist.o  : ACore/ABase/AStruct/AList/AList.hpp ACore/ABase/AStruct/AList/AList.cpp ACore/ABase/AStruct/AList/AList.test.cpp ACore/ABase/AStruct/AList/AList.debug.cpp ACore/ABase/AStruct/AList/AList.exception.hpp ACore/ABase/AStruct/AList/AList.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AList/AList.cpp -o out/alist.o 

out/alistiterator.o  : ACore/ABase/AStruct/AList/AListIterator/AListIterator.hpp ACore/ABase/AStruct/AList/AListIterator/AListIterator.cpp ACore/ABase/AStruct/AList/AListIterator/AListIterator.test.cpp ACore/ABase/AStruct/AList/AListIterator/AListIterator.debug.cpp ACore/ABase/AStruct/AList/AListIterator/AListIterator.exception.hpp ACore/ABase/AStruct/AList/AListIterator/AListIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AList/AListIterator/AListIterator.cpp -o out/alistiterator.o 

out/alistiterator_modules.o : out/alistiterator.o
	ld -r out/alistiterator.o -o out/alistiterator_modules.o

out/alist_modules.o : out/alist.o out/alistiterator_modules.o
	ld -r out/alist.o out/alistiterator_modules.o -o out/alist_modules.o

out/amap.o  : ACore/ABase/AStruct/AMap/AMap.hpp ACore/ABase/AStruct/AMap/AMap.cpp ACore/ABase/AStruct/AMap/AMap.test.cpp ACore/ABase/AStruct/AMap/AMap.debug.cpp ACore/ABase/AStruct/AMap/AMap.exception.hpp ACore/ABase/AStruct/AMap/AMap.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AMap/AMap.cpp -o out/amap.o 

out/amap_modules.o : out/amap.o
	ld -r out/amap.o -o out/amap_modules.o

out/astack.o  : ACore/ABase/AStruct/AStack/AStack.hpp ACore/ABase/AStruct/AStack/AStack.cpp ACore/ABase/AStruct/AStack/AStack.test.cpp ACore/ABase/AStruct/AStack/AStack.debug.cpp ACore/ABase/AStruct/AStack/AStack.exception.hpp ACore/ABase/AStruct/AStack/AStack.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AStack/AStack.cpp -o out/astack.o 

out/astack_modules.o : out/astack.o
	ld -r out/astack.o -o out/astack_modules.o

out/astructiterator.o  : ACore/ABase/AStruct/AStructIterator/AStructIterator.hpp ACore/ABase/AStruct/AStructIterator/AStructIterator.cpp ACore/ABase/AStruct/AStructIterator/AStructIterator.test.cpp ACore/ABase/AStruct/AStructIterator/AStructIterator.debug.cpp ACore/ABase/AStruct/AStructIterator/AStructIterator.exception.hpp ACore/ABase/AStruct/AStructIterator/AStructIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AStructIterator/AStructIterator.cpp -o out/astructiterator.o 

out/astructiterator_modules.o : out/astructiterator.o
	ld -r out/astructiterator.o -o out/astructiterator_modules.o

out/atree.o  : ACore/ABase/AStruct/ATree/ATree.hpp ACore/ABase/AStruct/ATree/ATree.cpp ACore/ABase/AStruct/ATree/ATree.test.cpp ACore/ABase/AStruct/ATree/ATree.debug.cpp ACore/ABase/AStruct/ATree/ATree.exception.hpp ACore/ABase/AStruct/ATree/ATree.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/ATree/ATree.cpp -o out/atree.o 

out/atree_modules.o : out/atree.o
	ld -r out/atree.o -o out/atree_modules.o

out/atreebinary.o  : ACore/ABase/AStruct/ATreeBinary/ATreeBinary.hpp ACore/ABase/AStruct/ATreeBinary/ATreeBinary.cpp ACore/ABase/AStruct/ATreeBinary/ATreeBinary.test.cpp ACore/ABase/AStruct/ATreeBinary/ATreeBinary.debug.cpp ACore/ABase/AStruct/ATreeBinary/ATreeBinary.exception.hpp ACore/ABase/AStruct/ATreeBinary/ATreeBinary.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/ATreeBinary/ATreeBinary.cpp -o out/atreebinary.o 

out/atreebinary_modules.o : out/atreebinary.o
	ld -r out/atreebinary.o -o out/atreebinary_modules.o

out/atreemap.o  : ACore/ABase/AStruct/ATreeMap/ATreeMap.hpp ACore/ABase/AStruct/ATreeMap/ATreeMap.cpp ACore/ABase/AStruct/ATreeMap/ATreeMap.test.cpp ACore/ABase/AStruct/ATreeMap/ATreeMap.debug.cpp ACore/ABase/AStruct/ATreeMap/ATreeMap.exception.hpp ACore/ABase/AStruct/ATreeMap/ATreeMap.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/ATreeMap/ATreeMap.cpp -o out/atreemap.o 

out/atreemap_modules.o : out/atreemap.o
	ld -r out/atreemap.o -o out/atreemap_modules.o

out/atreenav.o  : ACore/ABase/AStruct/ATreeNav/ATreeNav.hpp ACore/ABase/AStruct/ATreeNav/ATreeNav.cpp ACore/ABase/AStruct/ATreeNav/ATreeNav.test.cpp ACore/ABase/AStruct/ATreeNav/ATreeNav.debug.cpp ACore/ABase/AStruct/ATreeNav/ATreeNav.exception.hpp ACore/ABase/AStruct/ATreeNav/ATreeNav.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/ATreeNav/ATreeNav.cpp -o out/atreenav.o 

out/atreenav_modules.o : out/atreenav.o
	ld -r out/atreenav.o -o out/atreenav_modules.o

out/atreesearch.o  : ACore/ABase/AStruct/ATreeSearch/ATreeSearch.hpp ACore/ABase/AStruct/ATreeSearch/ATreeSearch.cpp ACore/ABase/AStruct/ATreeSearch/ATreeSearch.test.cpp ACore/ABase/AStruct/ATreeSearch/ATreeSearch.debug.cpp ACore/ABase/AStruct/ATreeSearch/ATreeSearch.exception.hpp ACore/ABase/AStruct/ATreeSearch/ATreeSearch.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/ATreeSearch/ATreeSearch.cpp -o out/atreesearch.o 

out/atreesearch_modules.o : out/atreesearch.o
	ld -r out/atreesearch.o -o out/atreesearch_modules.o

out/avector.o  : ACore/ABase/AStruct/AVector/AVector.hpp ACore/ABase/AStruct/AVector/AVector.cpp ACore/ABase/AStruct/AVector/AVector.test.cpp ACore/ABase/AStruct/AVector/AVector.debug.cpp ACore/ABase/AStruct/AVector/AVector.exception.hpp ACore/ABase/AStruct/AVector/AVector.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/AStruct/AVector/AVector.cpp -o out/avector.o 

out/avector_modules.o : out/avector.o
	ld -r out/avector.o -o out/avector_modules.o

out/astruct_modules.o : out/astruct.o out/aarray_modules.o out/alist_modules.o out/amap_modules.o out/astack_modules.o out/astructiterator_modules.o out/atree_modules.o out/atreebinary_modules.o out/atreemap_modules.o out/atreenav_modules.o out/atreesearch_modules.o out/avector_modules.o
	ld -r out/astruct.o out/aarray_modules.o out/alist_modules.o out/amap_modules.o out/astack_modules.o out/astructiterator_modules.o out/atree_modules.o out/atreebinary_modules.o out/atreemap_modules.o out/atreenav_modules.o out/atreesearch_modules.o out/avector_modules.o -o out/astruct_modules.o

out/atypes.o  : ACore/ABase/ATypes/ATypes.hpp ACore/ABase/ATypes/ATypes.cpp ACore/ABase/ATypes/ATypes.test.cpp ACore/ABase/ATypes/ATypes.debug.cpp ACore/ABase/ATypes/ATypes.exception.hpp ACore/ABase/ATypes/ATypes.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ATypes.cpp -o out/atypes.o 

out/abits.o  : ACore/ABase/ATypes/ABits/ABits.hpp ACore/ABase/ATypes/ABits/ABits.cpp ACore/ABase/ATypes/ABits/ABits.test.cpp ACore/ABase/ATypes/ABits/ABits.debug.cpp ACore/ABase/ATypes/ABits/ABits.exception.hpp ACore/ABase/ATypes/ABits/ABits.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/ABits.cpp -o out/abits.o 

out/a16bits.o  : ACore/ABase/ATypes/ABits/A16Bits/A16Bits.hpp ACore/ABase/ATypes/ABits/A16Bits/A16Bits.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bits.test.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bits.debug.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bits.exception.hpp ACore/ABase/ATypes/ABits/A16Bits/A16Bits.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A16Bits/A16Bits.cpp -o out/a16bits.o 

out/a16bit.o  : ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.hpp ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.test.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.debug.cpp ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.exception.hpp ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A16Bits/A16Bit/A16Bit.cpp -o out/a16bit.o 

out/a16bit_modules.o : out/a16bit.o
	ld -r out/a16bit.o -o out/a16bit_modules.o

out/a16bitsweak.o  : ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.hpp ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.cpp ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.test.cpp ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.debug.cpp ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.exception.hpp ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A16Bits/A16BitsWeak/A16BitsWeak.cpp -o out/a16bitsweak.o 

out/a16bitsweak_modules.o : out/a16bitsweak.o
	ld -r out/a16bitsweak.o -o out/a16bitsweak_modules.o

out/a16bits_modules.o : out/a16bits.o out/a16bit_modules.o out/a16bitsweak_modules.o
	ld -r out/a16bits.o out/a16bit_modules.o out/a16bitsweak_modules.o -o out/a16bits_modules.o

out/a32bits.o  : ACore/ABase/ATypes/ABits/A32Bits/A32Bits.hpp ACore/ABase/ATypes/ABits/A32Bits/A32Bits.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bits.test.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bits.debug.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bits.exception.hpp ACore/ABase/ATypes/ABits/A32Bits/A32Bits.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A32Bits/A32Bits.cpp -o out/a32bits.o 

out/a32bit.o  : ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.hpp ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.test.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.debug.cpp ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.exception.hpp ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A32Bits/A32Bit/A32Bit.cpp -o out/a32bit.o 

out/a32bit_modules.o : out/a32bit.o
	ld -r out/a32bit.o -o out/a32bit_modules.o

out/a32bitsweak.o  : ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.hpp ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.cpp ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.test.cpp ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.debug.cpp ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.exception.hpp ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A32Bits/A32BitsWeak/A32BitsWeak.cpp -o out/a32bitsweak.o 

out/a32bitsweak_modules.o : out/a32bitsweak.o
	ld -r out/a32bitsweak.o -o out/a32bitsweak_modules.o

out/a32bits_modules.o : out/a32bits.o out/a32bit_modules.o out/a32bitsweak_modules.o
	ld -r out/a32bits.o out/a32bit_modules.o out/a32bitsweak_modules.o -o out/a32bits_modules.o

out/a64bits.o  : ACore/ABase/ATypes/ABits/A64Bits/A64Bits.hpp ACore/ABase/ATypes/ABits/A64Bits/A64Bits.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bits.test.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bits.debug.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bits.exception.hpp ACore/ABase/ATypes/ABits/A64Bits/A64Bits.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A64Bits/A64Bits.cpp -o out/a64bits.o 

out/a64bit.o  : ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.hpp ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.test.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.debug.cpp ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.exception.hpp ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A64Bits/A64Bit/A64Bit.cpp -o out/a64bit.o 

out/a64bit_modules.o : out/a64bit.o
	ld -r out/a64bit.o -o out/a64bit_modules.o

out/a64bitsweak.o  : ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.hpp ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.cpp ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.test.cpp ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.debug.cpp ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.exception.hpp ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A64Bits/A64BitsWeak/A64BitsWeak.cpp -o out/a64bitsweak.o 

out/a64bitsweak_modules.o : out/a64bitsweak.o
	ld -r out/a64bitsweak.o -o out/a64bitsweak_modules.o

out/a64bits_modules.o : out/a64bits.o out/a64bit_modules.o out/a64bitsweak_modules.o
	ld -r out/a64bits.o out/a64bit_modules.o out/a64bitsweak_modules.o -o out/a64bits_modules.o

out/a8bits.o  : ACore/ABase/ATypes/ABits/A8Bits/A8Bits.hpp ACore/ABase/ATypes/ABits/A8Bits/A8Bits.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bits.test.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bits.debug.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bits.exception.hpp ACore/ABase/ATypes/ABits/A8Bits/A8Bits.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A8Bits/A8Bits.cpp -o out/a8bits.o 

out/a8bit.o  : ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.hpp ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.test.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.debug.cpp ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.exception.hpp ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A8Bits/A8Bit/A8Bit.cpp -o out/a8bit.o 

out/a8bit_modules.o : out/a8bit.o
	ld -r out/a8bit.o -o out/a8bit_modules.o

out/a8bitsweak.o  : ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.hpp ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.cpp ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.test.cpp ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.debug.cpp ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.exception.hpp ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ABits/A8Bits/A8BitsWeak/A8BitsWeak.cpp -o out/a8bitsweak.o 

out/a8bitsweak_modules.o : out/a8bitsweak.o
	ld -r out/a8bitsweak.o -o out/a8bitsweak_modules.o

out/a8bits_modules.o : out/a8bits.o out/a8bit_modules.o out/a8bitsweak_modules.o
	ld -r out/a8bits.o out/a8bit_modules.o out/a8bitsweak_modules.o -o out/a8bits_modules.o

out/abits_modules.o : out/abits.o out/a16bits_modules.o out/a32bits_modules.o out/a64bits_modules.o out/a8bits_modules.o
	ld -r out/abits.o out/a16bits_modules.o out/a32bits_modules.o out/a64bits_modules.o out/a8bits_modules.o -o out/abits_modules.o

out/aptr.o  : ACore/ABase/ATypes/APtr/APtr.hpp ACore/ABase/ATypes/APtr/APtr.cpp ACore/ABase/ATypes/APtr/APtr.test.cpp ACore/ABase/ATypes/APtr/APtr.debug.cpp ACore/ABase/ATypes/APtr/APtr.exception.hpp ACore/ABase/ATypes/APtr/APtr.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/APtr/APtr.cpp -o out/aptr.o 

out/aptrunique.o  : ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.hpp ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.cpp ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.test.cpp ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.debug.cpp ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.exception.hpp ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/APtr/APtrUnique/APtrUnique.cpp -o out/aptrunique.o 

out/aptrunique_modules.o : out/aptrunique.o
	ld -r out/aptrunique.o -o out/aptrunique_modules.o

out/aptrweak.o  : ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.hpp ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.cpp ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.test.cpp ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.debug.cpp ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.exception.hpp ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/APtr/APtrWeak/APtrWeak.cpp -o out/aptrweak.o 

out/aptrweak_modules.o : out/aptrweak.o
	ld -r out/aptrweak.o -o out/aptrweak_modules.o

out/aptr_modules.o : out/aptr.o out/aptrunique_modules.o out/aptrweak_modules.o
	ld -r out/aptr.o out/aptrunique_modules.o out/aptrweak_modules.o -o out/aptr_modules.o

out/aptrarray.o  : ACore/ABase/ATypes/APtrArray/APtrArray.hpp ACore/ABase/ATypes/APtrArray/APtrArray.cpp ACore/ABase/ATypes/APtrArray/APtrArray.test.cpp ACore/ABase/ATypes/APtrArray/APtrArray.debug.cpp ACore/ABase/ATypes/APtrArray/APtrArray.exception.hpp ACore/ABase/ATypes/APtrArray/APtrArray.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/APtrArray/APtrArray.cpp -o out/aptrarray.o 

out/aptrarrayweak.o  : ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.hpp ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.cpp ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.test.cpp ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.debug.cpp ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.exception.hpp ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/APtrArray/APtrArrayWeak/APtrArrayWeak.cpp -o out/aptrarrayweak.o 

out/aptrarrayweak_modules.o : out/aptrarrayweak.o
	ld -r out/aptrarrayweak.o -o out/aptrarrayweak_modules.o

out/aptrarray_modules.o : out/aptrarray.o out/aptrarrayweak_modules.o
	ld -r out/aptrarray.o out/aptrarrayweak_modules.o -o out/aptrarray_modules.o

out/astring.o  : ACore/ABase/ATypes/AString/AString.hpp ACore/ABase/ATypes/AString/AString.cpp ACore/ABase/ATypes/AString/AString.test.cpp ACore/ABase/ATypes/AString/AString.debug.cpp ACore/ABase/ATypes/AString/AString.exception.hpp ACore/ABase/ATypes/AString/AString.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/AString/AString.cpp -o out/astring.o 

out/astringbase.o  : ACore/ABase/ATypes/AString/AStringBase/AStringBase.hpp ACore/ABase/ATypes/AString/AStringBase/AStringBase.cpp ACore/ABase/ATypes/AString/AStringBase/AStringBase.test.cpp ACore/ABase/ATypes/AString/AStringBase/AStringBase.debug.cpp ACore/ABase/ATypes/AString/AStringBase/AStringBase.exception.hpp ACore/ABase/ATypes/AString/AStringBase/AStringBase.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/AString/AStringBase/AStringBase.cpp -o out/astringbase.o 

out/astringbase_modules.o : out/astringbase.o
	ld -r out/astringbase.o -o out/astringbase_modules.o

out/astring_modules.o : out/astring.o out/astringbase_modules.o
	ld -r out/astring.o out/astringbase_modules.o -o out/astring_modules.o

out/asymbol.o  : ACore/ABase/ATypes/ASymbol/ASymbol.hpp ACore/ABase/ATypes/ASymbol/ASymbol.cpp ACore/ABase/ATypes/ASymbol/ASymbol.test.cpp ACore/ABase/ATypes/ASymbol/ASymbol.debug.cpp ACore/ABase/ATypes/ASymbol/ASymbol.exception.hpp ACore/ABase/ATypes/ASymbol/ASymbol.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ABase/ATypes/ASymbol/ASymbol.cpp -o out/asymbol.o 

out/asymbol_modules.o : out/asymbol.o
	ld -r out/asymbol.o -o out/asymbol_modules.o

out/atypes_modules.o : out/atypes.o out/abits_modules.o out/aptr_modules.o out/aptrarray_modules.o out/astring_modules.o out/asymbol_modules.o
	ld -r out/atypes.o out/abits_modules.o out/aptr_modules.o out/aptrarray_modules.o out/astring_modules.o out/asymbol_modules.o -o out/atypes_modules.o

out/abase_modules.o : out/abase.o out/aencryption_modules.o out/aio_modules.o out/alibs_modules.o out/amath_modules.o out/astruct_modules.o out/atypes_modules.o
	ld -r out/abase.o out/aencryption_modules.o out/aio_modules.o out/alibs_modules.o out/amath_modules.o out/astruct_modules.o out/atypes_modules.o -o out/abase_modules.o

out/atools.o  : ACore/ATools/ATools.hpp ACore/ATools/ATools.cpp ACore/ATools/ATools.test.cpp ACore/ATools/ATools.debug.cpp ACore/ATools/ATools.exception.hpp ACore/ATools/ATools.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ATools.cpp -o out/atools.o 

out/acppparser.o  : ACore/ATools/ACppParser/ACppParser.hpp ACore/ATools/ACppParser/ACppParser.cpp ACore/ATools/ACppParser/ACppParser.test.cpp ACore/ATools/ACppParser/ACppParser.debug.cpp ACore/ATools/ACppParser/ACppParser.exception.hpp ACore/ATools/ACppParser/ACppParser.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppParser.cpp -o out/acppparser.o 

out/acppimage.o  : ACore/ATools/ACppParser/ACppImage/ACppImage.hpp ACore/ATools/ACppParser/ACppImage/ACppImage.cpp ACore/ATools/ACppParser/ACppImage/ACppImage.test.cpp ACore/ATools/ACppParser/ACppImage/ACppImage.debug.cpp ACore/ATools/ACppParser/ACppImage/ACppImage.exception.hpp ACore/ATools/ACppParser/ACppImage/ACppImage.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppImage/ACppImage.cpp -o out/acppimage.o 

out/acppitem.o  : ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.hpp ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.cpp ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.test.cpp ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.debug.cpp ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.exception.hpp ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppImage/ACppItem/ACppItem.cpp -o out/acppitem.o 

out/acppitem_modules.o : out/acppitem.o
	ld -r out/acppitem.o -o out/acppitem_modules.o

out/acppimage_modules.o : out/acppimage.o out/acppitem_modules.o
	ld -r out/acppimage.o out/acppitem_modules.o -o out/acppimage_modules.o

out/acpptoken.o  : ACore/ATools/ACppParser/ACppToken/ACppToken.hpp ACore/ATools/ACppParser/ACppToken/ACppToken.cpp ACore/ATools/ACppParser/ACppToken/ACppToken.test.cpp ACore/ATools/ACppParser/ACppToken/ACppToken.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppToken.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppToken.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppToken.cpp -o out/acpptoken.o 

out/acpptokenallline.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenAllLine/ACppTokenAllLine.cpp -o out/acpptokenallline.o 

out/acpptokenallline_modules.o : out/acpptokenallline.o
	ld -r out/acpptokenallline.o -o out/acpptokenallline_modules.o

out/acpptokenchar.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenChar/ACppTokenChar.cpp -o out/acpptokenchar.o 

out/acpptokenchar_modules.o : out/acpptokenchar.o
	ld -r out/acpptokenchar.o -o out/acpptokenchar_modules.o

out/acpptokencommentbegin.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenCommentBegin/ACppTokenCommentBegin.cpp -o out/acpptokencommentbegin.o 

out/acpptokencommentbegin_modules.o : out/acpptokencommentbegin.o
	ld -r out/acpptokencommentbegin.o -o out/acpptokencommentbegin_modules.o

out/acpptokencommentend.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenCommentEnd/ACppTokenCommentEnd.cpp -o out/acpptokencommentend.o 

out/acpptokencommentend_modules.o : out/acpptokencommentend.o
	ld -r out/acpptokencommentend.o -o out/acpptokencommentend_modules.o

out/acpptokencommentinline.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenCommentInline/ACppTokenCommentInline.cpp -o out/acpptokencommentinline.o 

out/acpptokencommentinline_modules.o : out/acpptokencommentinline.o
	ld -r out/acpptokencommentinline.o -o out/acpptokencommentinline_modules.o

out/acpptokencommentline.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenCommentLine/ACppTokenCommentLine.cpp -o out/acpptokencommentline.o 

out/acpptokencommentline_modules.o : out/acpptokencommentline.o
	ld -r out/acpptokencommentline.o -o out/acpptokencommentline_modules.o

out/acpptokenkeysymbol.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenKeySymbol/ACppTokenKeySymbol.cpp -o out/acpptokenkeysymbol.o 

out/acpptokenkeysymbol_modules.o : out/acpptokenkeysymbol.o
	ld -r out/acpptokenkeysymbol.o -o out/acpptokenkeysymbol_modules.o

out/acpptokenkeytype.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenKeyType/ACppTokenKeyType.cpp -o out/acpptokenkeytype.o 

out/acpptokenkeytype_modules.o : out/acpptokenkeytype.o
	ld -r out/acpptokenkeytype.o -o out/acpptokenkeytype_modules.o

out/acpptokenkeyword.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenKeyWord/ACppTokenKeyWord.cpp -o out/acpptokenkeyword.o 

out/acpptokenkeyword_modules.o : out/acpptokenkeyword.o
	ld -r out/acpptokenkeyword.o -o out/acpptokenkeyword_modules.o

out/acpptokenname.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenName/ACppTokenName.cpp -o out/acpptokenname.o 

out/acpptokenname_modules.o : out/acpptokenname.o
	ld -r out/acpptokenname.o -o out/acpptokenname_modules.o

out/acpptokennumberdouble.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenNumberDouble/ACppTokenNumberDouble.cpp -o out/acpptokennumberdouble.o 

out/acpptokennumberdouble_modules.o : out/acpptokennumberdouble.o
	ld -r out/acpptokennumberdouble.o -o out/acpptokennumberdouble_modules.o

out/acpptokennumberhex.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenNumberHex/ACppTokenNumberHex.cpp -o out/acpptokennumberhex.o 

out/acpptokennumberhex_modules.o : out/acpptokennumberhex.o
	ld -r out/acpptokennumberhex.o -o out/acpptokennumberhex_modules.o

out/acpptokennumberinteger.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenNumberInteger/ACppTokenNumberInteger.cpp -o out/acpptokennumberinteger.o 

out/acpptokennumberinteger_modules.o : out/acpptokennumberinteger.o
	ld -r out/acpptokennumberinteger.o -o out/acpptokennumberinteger_modules.o

out/acpptokenspace.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenSpace/ACppTokenSpace.cpp -o out/acpptokenspace.o 

out/acpptokenspace_modules.o : out/acpptokenspace.o
	ld -r out/acpptokenspace.o -o out/acpptokenspace_modules.o

out/acpptokenstring.o  : ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.test.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.debug.cpp ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.exception.hpp ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ACppParser/ACppToken/ACppTokenString/ACppTokenString.cpp -o out/acpptokenstring.o 

out/acpptokenstring_modules.o : out/acpptokenstring.o
	ld -r out/acpptokenstring.o -o out/acpptokenstring_modules.o

out/acpptoken_modules.o : out/acpptoken.o out/acpptokenallline_modules.o out/acpptokenchar_modules.o out/acpptokencommentbegin_modules.o out/acpptokencommentend_modules.o out/acpptokencommentinline_modules.o out/acpptokencommentline_modules.o out/acpptokenkeysymbol_modules.o out/acpptokenkeytype_modules.o out/acpptokenkeyword_modules.o out/acpptokenname_modules.o out/acpptokennumberdouble_modules.o out/acpptokennumberhex_modules.o out/acpptokennumberinteger_modules.o out/acpptokenspace_modules.o out/acpptokenstring_modules.o
	ld -r out/acpptoken.o out/acpptokenallline_modules.o out/acpptokenchar_modules.o out/acpptokencommentbegin_modules.o out/acpptokencommentend_modules.o out/acpptokencommentinline_modules.o out/acpptokencommentline_modules.o out/acpptokenkeysymbol_modules.o out/acpptokenkeytype_modules.o out/acpptokenkeyword_modules.o out/acpptokenname_modules.o out/acpptokennumberdouble_modules.o out/acpptokennumberhex_modules.o out/acpptokennumberinteger_modules.o out/acpptokenspace_modules.o out/acpptokenstring_modules.o -o out/acpptoken_modules.o

out/acppparser_modules.o : out/acppparser.o out/acppimage_modules.o out/acpptoken_modules.o
	ld -r out/acppparser.o out/acppimage_modules.o out/acpptoken_modules.o -o out/acppparser_modules.o

out/ahtmlconstructor.o  : ACore/ATools/AHtmlConstructor/AHtmlConstructor.hpp ACore/ATools/AHtmlConstructor/AHtmlConstructor.cpp ACore/ATools/AHtmlConstructor/AHtmlConstructor.test.cpp ACore/ATools/AHtmlConstructor/AHtmlConstructor.debug.cpp ACore/ATools/AHtmlConstructor/AHtmlConstructor.exception.hpp ACore/ATools/AHtmlConstructor/AHtmlConstructor.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/AHtmlConstructor/AHtmlConstructor.cpp -o out/ahtmlconstructor.o 

out/ahtmlconstructor_modules.o : out/ahtmlconstructor.o
	ld -r out/ahtmlconstructor.o -o out/ahtmlconstructor_modules.o

out/aloader.o  : ACore/ATools/ALoader/ALoader.hpp ACore/ATools/ALoader/ALoader.cpp ACore/ATools/ALoader/ALoader.test.cpp ACore/ATools/ALoader/ALoader.debug.cpp ACore/ATools/ALoader/ALoader.exception.hpp ACore/ATools/ALoader/ALoader.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ALoader/ALoader.cpp -o out/aloader.o 

out/atextmanadger.o  : ACore/ATools/ALoader/ATextManadger/ATextManadger.hpp ACore/ATools/ALoader/ATextManadger/ATextManadger.cpp ACore/ATools/ALoader/ATextManadger/ATextManadger.test.cpp ACore/ATools/ALoader/ATextManadger/ATextManadger.debug.cpp ACore/ATools/ALoader/ATextManadger/ATextManadger.exception.hpp ACore/ATools/ALoader/ATextManadger/ATextManadger.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ALoader/ATextManadger/ATextManadger.cpp -o out/atextmanadger.o 

out/atextfile.o  : ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.test.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.debug.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.exception.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ALoader/ATextManadger/ATextFile/ATextFile.cpp -o out/atextfile.o 

out/atext.o  : ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.test.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.debug.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.exception.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ALoader/ATextManadger/ATextFile/AText/AText.cpp -o out/atext.o 

out/atext_modules.o : out/atext.o
	ld -r out/atext.o -o out/atext_modules.o

out/atextiterator.o  : ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.test.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.debug.cpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.exception.hpp ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.exception.cpp  
	g++ -I. -std=c++11 -c ACore/ATools/ALoader/ATextManadger/ATextFile/ATextIterator/ATextIterator.cpp -o out/atextiterator.o 

out/atextiterator_modules.o : out/atextiterator.o
	ld -r out/atextiterator.o -o out/atextiterator_modules.o

out/atextfile_modules.o : out/atextfile.o out/atext_modules.o out/atextiterator_modules.o
	ld -r out/atextfile.o out/atext_modules.o out/atextiterator_modules.o -o out/atextfile_modules.o

out/atextmanadger_modules.o : out/atextmanadger.o out/atextfile_modules.o
	ld -r out/atextmanadger.o out/atextfile_modules.o -o out/atextmanadger_modules.o

out/aloader_modules.o : out/aloader.o out/atextmanadger_modules.o
	ld -r out/aloader.o out/atextmanadger_modules.o -o out/aloader_modules.o

out/atools_modules.o : out/atools.o out/acppparser_modules.o out/ahtmlconstructor_modules.o out/aloader_modules.o
	ld -r out/atools.o out/acppparser_modules.o out/ahtmlconstructor_modules.o out/aloader_modules.o -o out/atools_modules.o

out/acore_modules.o : out/acore.o out/abase_modules.o out/atools_modules.o
	ld -r out/acore.o out/abase_modules.o out/atools_modules.o -o out/acore_modules.o

out/aproject.o  : AProject/AProject.hpp AProject/AProject.cpp AProject/AProject.test.cpp AProject/AProject.debug.cpp AProject/AProject.exception.hpp AProject/AProject.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProject.cpp -o out/aproject.o 

out/amodule.o  : AProject/AModule/AModule.hpp AProject/AModule/AModule.cpp AProject/AModule/AModule.test.cpp AProject/AModule/AModule.debug.cpp AProject/AModule/AModule.exception.hpp AProject/AModule/AModule.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModule/AModule.cpp -o out/amodule.o 

out/amodule_modules.o : out/amodule.o
	ld -r out/amodule.o -o out/amodule_modules.o

out/amoduledebugheader.o  : AProject/AModuleDebugHeader/AModuleDebugHeader.hpp AProject/AModuleDebugHeader/AModuleDebugHeader.cpp AProject/AModuleDebugHeader/AModuleDebugHeader.test.cpp AProject/AModuleDebugHeader/AModuleDebugHeader.debug.cpp AProject/AModuleDebugHeader/AModuleDebugHeader.exception.hpp AProject/AModuleDebugHeader/AModuleDebugHeader.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDebugHeader/AModuleDebugHeader.cpp -o out/amoduledebugheader.o 

out/amoduledebugheader_modules.o : out/amoduledebugheader.o
	ld -r out/amoduledebugheader.o -o out/amoduledebugheader_modules.o

out/amoduledebugnode.o  : AProject/AModuleDebugNode/AModuleDebugNode.hpp AProject/AModuleDebugNode/AModuleDebugNode.cpp AProject/AModuleDebugNode/AModuleDebugNode.test.cpp AProject/AModuleDebugNode/AModuleDebugNode.debug.cpp AProject/AModuleDebugNode/AModuleDebugNode.exception.hpp AProject/AModuleDebugNode/AModuleDebugNode.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDebugNode/AModuleDebugNode.cpp -o out/amoduledebugnode.o 

out/amoduledebugnode_modules.o : out/amoduledebugnode.o
	ld -r out/amoduledebugnode.o -o out/amoduledebugnode_modules.o

out/amoduledochtml.o  : AProject/AModuleDocHtml/AModuleDocHtml.hpp AProject/AModuleDocHtml/AModuleDocHtml.cpp AProject/AModuleDocHtml/AModuleDocHtml.test.cpp AProject/AModuleDocHtml/AModuleDocHtml.debug.cpp AProject/AModuleDocHtml/AModuleDocHtml.exception.hpp AProject/AModuleDocHtml/AModuleDocHtml.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocHtml/AModuleDocHtml.cpp -o out/amoduledochtml.o 

out/adochtmlparsercpp.o  : AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.hpp AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.cpp AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.test.cpp AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.debug.cpp AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.exception.hpp AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocHtml/ADocHtmlParserCPP/ADocHtmlParserCPP.cpp -o out/adochtmlparsercpp.o 

out/adochtmlparsercpp_modules.o : out/adochtmlparsercpp.o
	ld -r out/adochtmlparsercpp.o -o out/adochtmlparsercpp_modules.o

out/adochtmlparserhpp.o  : AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.hpp AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.cpp AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.test.cpp AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.debug.cpp AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.exception.hpp AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocHtml/ADocHtmlParserHPP/ADocHtmlParserHPP.cpp -o out/adochtmlparserhpp.o 

out/adochtmlparserhpp_modules.o : out/adochtmlparserhpp.o
	ld -r out/adochtmlparserhpp.o -o out/adochtmlparserhpp_modules.o

out/adochtmlparsertest.o  : AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.hpp AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.cpp AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.test.cpp AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.debug.cpp AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.exception.hpp AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocHtml/ADocHtmlParserTEST/ADocHtmlParserTEST.cpp -o out/adochtmlparsertest.o 

out/adochtmlparsertest_modules.o : out/adochtmlparsertest.o
	ld -r out/adochtmlparsertest.o -o out/adochtmlparsertest_modules.o

out/amoduledochtml_modules.o : out/amoduledochtml.o out/adochtmlparsercpp_modules.o out/adochtmlparserhpp_modules.o out/adochtmlparsertest_modules.o
	ld -r out/amoduledochtml.o out/adochtmlparsercpp_modules.o out/adochtmlparserhpp_modules.o out/adochtmlparsertest_modules.o -o out/amoduledochtml_modules.o

out/amoduledocrefshtml.o  : AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.hpp AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.cpp AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.test.cpp AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.debug.cpp AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.exception.hpp AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocRefsHtml/AModuleDocRefsHtml.cpp -o out/amoduledocrefshtml.o 

out/amoduledocrefshtml_modules.o : out/amoduledocrefshtml.o
	ld -r out/amoduledocrefshtml.o -o out/amoduledocrefshtml_modules.o

out/amoduledocstatushtml.o  : AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.hpp AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.cpp AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.test.cpp AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.debug.cpp AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.exception.hpp AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocStatusHtml/AModuleDocStatusHtml.cpp -o out/amoduledocstatushtml.o 

out/amodulesizeinfo.o  : AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.hpp AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.cpp AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.test.cpp AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.debug.cpp AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.exception.hpp AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleDocStatusHtml/AModuleSizeInfo/AModuleSizeInfo.cpp -o out/amodulesizeinfo.o 

out/amodulesizeinfo_modules.o : out/amodulesizeinfo.o
	ld -r out/amodulesizeinfo.o -o out/amodulesizeinfo_modules.o

out/amoduledocstatushtml_modules.o : out/amoduledocstatushtml.o out/amodulesizeinfo_modules.o
	ld -r out/amoduledocstatushtml.o out/amodulesizeinfo_modules.o -o out/amoduledocstatushtml_modules.o

out/amodulemakefile.o  : AProject/AModuleMakeFile/AModuleMakeFile.hpp AProject/AModuleMakeFile/AModuleMakeFile.cpp AProject/AModuleMakeFile/AModuleMakeFile.test.cpp AProject/AModuleMakeFile/AModuleMakeFile.debug.cpp AProject/AModuleMakeFile/AModuleMakeFile.exception.hpp AProject/AModuleMakeFile/AModuleMakeFile.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleMakeFile/AModuleMakeFile.cpp -o out/amodulemakefile.o 

out/amodulemakefile_modules.o : out/amodulemakefile.o
	ld -r out/amodulemakefile.o -o out/amodulemakefile_modules.o

out/amoduletestheader.o  : AProject/AModuleTestHeader/AModuleTestHeader.hpp AProject/AModuleTestHeader/AModuleTestHeader.cpp AProject/AModuleTestHeader/AModuleTestHeader.test.cpp AProject/AModuleTestHeader/AModuleTestHeader.debug.cpp AProject/AModuleTestHeader/AModuleTestHeader.exception.hpp AProject/AModuleTestHeader/AModuleTestHeader.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleTestHeader/AModuleTestHeader.cpp -o out/amoduletestheader.o 

out/amoduletestheader_modules.o : out/amoduletestheader.o
	ld -r out/amoduletestheader.o -o out/amoduletestheader_modules.o

out/amoduletestnode.o  : AProject/AModuleTestNode/AModuleTestNode.hpp AProject/AModuleTestNode/AModuleTestNode.cpp AProject/AModuleTestNode/AModuleTestNode.test.cpp AProject/AModuleTestNode/AModuleTestNode.debug.cpp AProject/AModuleTestNode/AModuleTestNode.exception.hpp AProject/AModuleTestNode/AModuleTestNode.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AModuleTestNode/AModuleTestNode.cpp -o out/amoduletestnode.o 

out/amoduletestnode_modules.o : out/amoduletestnode.o
	ld -r out/amoduletestnode.o -o out/amoduletestnode_modules.o

out/aprojectcom.o  : AProject/AProjectCom/AProjectCom.hpp AProject/AProjectCom/AProjectCom.cpp AProject/AProjectCom/AProjectCom.test.cpp AProject/AProjectCom/AProjectCom.debug.cpp AProject/AProjectCom/AProjectCom.exception.hpp AProject/AProjectCom/AProjectCom.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AProjectCom.cpp -o out/aprojectcom.o 

out/amoduletemplatecpp.o  : AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.hpp AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.cpp AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.test.cpp AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.debug.cpp AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.exception.hpp AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateCpp/AModuleTemplateCpp.cpp -o out/amoduletemplatecpp.o 

out/amoduletemplatecpp_modules.o : out/amoduletemplatecpp.o
	ld -r out/amoduletemplatecpp.o -o out/amoduletemplatecpp_modules.o

out/amoduletemplatedebugcpp.o  : AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.hpp AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.cpp AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.test.cpp AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.debug.cpp AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.exception.hpp AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateDebugCpp/AModuleTemplateDebugCpp.cpp -o out/amoduletemplatedebugcpp.o 

out/amoduletemplatedebugcpp_modules.o : out/amoduletemplatedebugcpp.o
	ld -r out/amoduletemplatedebugcpp.o -o out/amoduletemplatedebugcpp_modules.o

out/amoduletemplatedochtml.o  : AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.hpp AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.cpp AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.test.cpp AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.debug.cpp AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.exception.hpp AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateDocHtml/AModuleTemplateDocHtml.cpp -o out/amoduletemplatedochtml.o 

out/amoduletemplatedochtml_modules.o : out/amoduletemplatedochtml.o
	ld -r out/amoduletemplatedochtml.o -o out/amoduletemplatedochtml_modules.o

out/amoduletemplateexceptioncpp.o  : AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.hpp AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.cpp AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.test.cpp AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.debug.cpp AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.exception.hpp AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateExceptionCpp/AModuleTemplateExceptionCpp.cpp -o out/amoduletemplateexceptioncpp.o 

out/amoduletemplateexceptioncpp_modules.o : out/amoduletemplateexceptioncpp.o
	ld -r out/amoduletemplateexceptioncpp.o -o out/amoduletemplateexceptioncpp_modules.o

out/amoduletemplateexceptionhpp.o  : AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.hpp AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.cpp AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.test.cpp AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.debug.cpp AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.exception.hpp AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateExceptionHpp/AModuleTemplateExceptionHpp.cpp -o out/amoduletemplateexceptionhpp.o 

out/amoduletemplateexceptionhpp_modules.o : out/amoduletemplateexceptionhpp.o
	ld -r out/amoduletemplateexceptionhpp.o -o out/amoduletemplateexceptionhpp_modules.o

out/amoduletemplatehpp.o  : AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.hpp AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.cpp AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.test.cpp AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.debug.cpp AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.exception.hpp AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateHpp/AModuleTemplateHpp.cpp -o out/amoduletemplatehpp.o 

out/amoduletemplatehpp_modules.o : out/amoduletemplatehpp.o
	ld -r out/amoduletemplatehpp.o -o out/amoduletemplatehpp_modules.o

out/amoduletemplatestatus.o  : AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.hpp AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.cpp AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.test.cpp AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.debug.cpp AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.exception.hpp AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateStatus/AModuleTemplateStatus.cpp -o out/amoduletemplatestatus.o 

out/amoduletemplatestatus_modules.o : out/amoduletemplatestatus.o
	ld -r out/amoduletemplatestatus.o -o out/amoduletemplatestatus_modules.o

out/amoduletemplatetestcpp.o  : AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.hpp AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.cpp AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.test.cpp AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.debug.cpp AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.exception.hpp AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.exception.cpp  
	g++ -I. -std=c++11 -c AProject/AProjectCom/AModuleTemplateTestCpp/AModuleTemplateTestCpp.cpp -o out/amoduletemplatetestcpp.o 

out/amoduletemplatetestcpp_modules.o : out/amoduletemplatetestcpp.o
	ld -r out/amoduletemplatetestcpp.o -o out/amoduletemplatetestcpp_modules.o

out/aprojectcom_modules.o : out/aprojectcom.o out/amoduletemplatecpp_modules.o out/amoduletemplatedebugcpp_modules.o out/amoduletemplatedochtml_modules.o out/amoduletemplateexceptioncpp_modules.o out/amoduletemplateexceptionhpp_modules.o out/amoduletemplatehpp_modules.o out/amoduletemplatestatus_modules.o out/amoduletemplatetestcpp_modules.o
	ld -r out/aprojectcom.o out/amoduletemplatecpp_modules.o out/amoduletemplatedebugcpp_modules.o out/amoduletemplatedochtml_modules.o out/amoduletemplateexceptioncpp_modules.o out/amoduletemplateexceptionhpp_modules.o out/amoduletemplatehpp_modules.o out/amoduletemplatestatus_modules.o out/amoduletemplatetestcpp_modules.o -o out/aprojectcom_modules.o

out/aproject_modules.o : out/aproject.o out/amodule_modules.o out/amoduledebugheader_modules.o out/amoduledebugnode_modules.o out/amoduledochtml_modules.o out/amoduledocrefshtml_modules.o out/amoduledocstatushtml_modules.o out/amodulemakefile_modules.o out/amoduletestheader_modules.o out/amoduletestnode_modules.o out/aprojectcom_modules.o
	ld -r out/aproject.o out/amodule_modules.o out/amoduledebugheader_modules.o out/amoduledebugnode_modules.o out/amoduledochtml_modules.o out/amoduledocrefshtml_modules.o out/amoduledocstatushtml_modules.o out/amodulemakefile_modules.o out/amoduletestheader_modules.o out/amoduletestnode_modules.o out/aprojectcom_modules.o -o out/aproject_modules.o

alliance : alliance.o out/acore_modules.o out/aproject_modules.o
	g++ -I. -std=c++11 alliance.o out/acore_modules.o out/aproject_modules.o -o alliance

clean : 
	rm *.o alliance out/*.o

memtest : 
	valgrind -v --tool=memcheck --leak-check=full ./alliance
save : alliance
	cp alliance alliance.save
config : alliance.save
	./config.sh
start : alliance
	./alliance
