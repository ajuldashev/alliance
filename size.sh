#!/bin/bash

let "size= 0"

for file in `find ./ -type f -name "*.hpp"`
do
   size=($size+$(wc -l $file));
done

for file in `find ./ -type f -name "*.cpp"`
do
   size=($size+$(wc -l $file));
done

for file in `find ./ -type f -name "*.acf"`
do
   size=($size+$(wc -l $file));
done

for file in `find ./ -type f -name "*.asf"`
do
   size=($size+$(wc -l $file));
done

for file in `find ./ -type f -name "*.html"`
do
   size=($size+$(wc -l $file));
done

for file in `find ./ -type f -name "makefile"`
do
   size=($size+$(wc -l $file));
done

let "size=$size"
echo "All           | " $size
echo "--------------*-------------"

let "files_h= 0"
for file in `find ./ -type f -name "*.hpp"`
do
   files_h=($files_h+$(wc -l $file));
done
let "files_h=$files_h"
echo "*.h           | " $files_h

let "files_cpp= 0"
for file in `find ./ -type f -name "*.cpp"`
do
   files_cpp=($files_cpp+$(wc -l $file));
done
let "files_cpp=$files_cpp"
echo "*.cpp         | " $files_cpp


let "files_debug= 0"
for file in `find ./ -type f -name "*.debug.cpp"`
do
   files_debug=($files_debug+$(wc -l $file));
done
let "files_debug=$files_debug"
echo "Debug         | " $files_debug


let "files_test= 0"
for file in `find ./ -type f -name "*.test.cpp"`
do
   files_test=($files_test+$(wc -l $file));
done
let "files_test=$files_test"
echo "Test          | " $files_test

let "files_exeption= 0"
for file in `find ./ -type f -name "*exception.hpp"`
do
   files_exeption=($files_exeption+$(wc -l $file));
done
for file in `find ./ -type f -name "*exception.cpp"`
do
   files_exeption=($files_exeption+$(wc -l $file));
done
let "files_exeption=$files_exeption"
echo "Exeption      | " $files_exeption

let "files_doc= 0"
for file in `find ./ -type f -name "*.html"`
do
   files_doc=($files_doc+$(wc -l $file));
done
let "files_doc=$files_doc"
echo "Doc           | " $files_doc

let "files_makefile= 0"
for file in `find ./ -type f -name "makefile"`
do
   files_makefile=($files_makefile+$(wc -l $file));
done
let "files_makefile=$files_makefile"
echo "Makefile      | " $files_makefile
